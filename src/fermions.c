#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include "compatab.h"
#include "fermions.h"
#include "randnorm.h"
#include "move.h"
#include "trial.h"
#include "trialf.h"
#include "utils.h"
#include "memory.h"
#include "randnorm.h"

DOUBLE ***Determinant, ***DeterminantInv;
DOUBLE ***DeterminantT, ***DeterminantTInv;
DOUBLE **DeterminantMoveBackUp, **DeterminantMoveInvBackUp;
int *to_process;
DOUBLE determinantLU;

int orbital_numbers_IFG[ORBITALS_MAX][3]; // (nx, ny, nz) for given alpha
int orbital_numbers_IFG_Norbital[ORBITALS_MAX];
int orbital_numbers_unpaired[ORBITALS_MAX][3]; // (nx, ny, nz) for given alpha
int alpha_polarized = 1;
int orbital_N; // number of orbitals used
DOUBLE orbital_weight[ORBITALS_MAX]; // weights (var. parameters)

int nz_row_index[100][200]; // list with indexes of non zero elements in a given row
int nz_row_number[100]; // number of non zero elements in a given row
int nz_column_index[200][100]; // list with indexes of non zero elements in a given column
int nz_column_number[200]; // number of non zero elements in a given column
// end of variables needed for matrix inversion

// variables needed for matrix inversion
DOUBLE **LU, **AI, *scales, *invb;
int *ps;
// other local arrays
DOUBLE **DeterminantMove, **DeterminantMoveInv;
#pragma omp threadprivate(LU, AI, scales, invb, ps, DeterminantMove, DeterminantMoveInv)

//void *pOMP[16]; //(3) fails
void* Calloc2(const char* name, unsigned length, size_t size) {
  //(1) works OK
  return calloc(length, size);

  // (2) fails
  //void *p;
  //p = calloc(length, size);
  //if(p == NULL) Error("Not enough memory for %s", name);
  //return p;

  //(3) fails
  //int thread_number = omp_get_thread_num();
  //pOMP[thread_number] = calloc(length, size);
  //if(pOMP[thread_number] == NULL) Error("Not enough memory for %s", name);
  //return pOMP[thread_number];
}

void AllocateFermionMatrices(void) {
  int N, i;

  if(verbosity) Message("Memory allocation: allocating fermion matrices ...");
  N = Nmax;
  //Message("serial node %i\n", omp_get_thread_num());

#pragma omp parallel
  {
  LU = (DOUBLE**) Calloc2("LU ", 2*N, sizeof(DOUBLE*));
  for(i=0; i<2*N; i++) LU[i] = (DOUBLE*) Calloc2("LU ", 2*N, sizeof(DOUBLE));

  //Message("node %i pointer %p\n", omp_get_thread_num(), LU);
  AI = (DOUBLE**) Calloc2("AI ", 2*N, sizeof(DOUBLE*));
  for(i=0; i<2*N; i++) AI[i] = (DOUBLE*) Calloc2("AI ", 2*N, sizeof(DOUBLE));

  scales = (DOUBLE*) Calloc2("scales ", N, sizeof(DOUBLE));
  invb = (DOUBLE*) Calloc2("invb ", N, sizeof(DOUBLE));
  ps = (int*) Calloc2("ps ", N, sizeof(int));

  DeterminantMove = (DOUBLE**) Calloc2("DeterminantMove ", N, sizeof(DOUBLE*));
  for(i=0; i<N; i++) DeterminantMove[i] = (DOUBLE*) Calloc2("DeterminantMove[i] ", N, sizeof(DOUBLE));

  DeterminantMoveInv = (DOUBLE**) Calloc2("DeterminantMoveInv ", N, sizeof(DOUBLE*));
  for(i=0; i<N; i++) DeterminantMoveInv[i] = (DOUBLE*) Calloc2("DeterminantMoveInv[i] ", N, sizeof(DOUBLE));
  }
}

/* lu_decompose() decomposes the coefficient matrix A into upper and lower
 * triangular matrices, the composite being the LU matrix.
 *
 * The arguments are:
 *
 *      a - the (n x n) coefficient matrix
 *      n - the order of the matrix
 *
 *  1 is returned if the decomposition was successful,
 *  and 0 is returned if the coefficient matrix is singular.
 */
int LUdecompose(DOUBLE **a, int n) {
  register int i, j, k;
  int pivotindex = 0;
  DOUBLE pivot, biggest, mult, tempf;
  int sign_pivoted = 1; /* parity of pivoting */

  for(i=0; i<n; i++) { /* For each row */
    /* Find the largest element in each row for row equilibration */
    biggest = 0.;

    for(j=0; j<n; j++) {
      if(biggest<(tempf = fabs(LU[i][j] = a[i][j]))) {
        biggest  = tempf;
      }
    }
    if(biggest != 0.)
      scales[i] = 1. / biggest;
    else {
      scales[i] = 0.;
      return(0);      /* Zero row: singular matrix */
    }
    ps[i] = i;          /* Initialize pivot sequence */
  }

  for(k=0; k<n-1; k++) {     /* For each column */
    /* Find the largest element in each column to pivot around */
    biggest = 0.0;
    for(i=k; i<n; i++) {
      if(biggest < (tempf = fabs(LU[ps[i]][k]) * scales[ps[i]])) {
        biggest = tempf;
        pivotindex = i;
      }
    }
    if(biggest == 0.0)
      return(0);      /* Zero column: singular matrix */
    if(pivotindex != k) {      /* Update pivot sequence */
      j = ps[k];
      ps[k] = ps[pivotindex];
      ps[pivotindex] = j;
      sign_pivoted *= -1;
    }

    /* Pivot, eliminating an extra variable each time */
    pivot = LU[ps[k]][k];
    for(i=k+1; i < n; i++) {
      LU[ps[i]][k] = mult = LU[ps[i]][k] / pivot;
      if(mult != 0.0) {
        for (j = k+1; j < n; j++)
          LU[ps[i]][j] -= mult * LU[ps[k]][j];
      }
    }
  }

  /* Calculate determinant of the input matrix */
  determinantLU = sign_pivoted;
  for(i=0; i<n; i++) determinantLU *= LU[ps[i]][i];

  if(LU[ps[n-1]][n-1] == 0.0)
    return(0);          /* Singular matrix */
  return(1);
}

/* lu_solve() solves the linear equation (Ax = b) after the matrix A has
 * been decomposed with lu_decompose() into the lower and upper triangular
 * matrices L and U.
 *
 * The arguments are:
 *
 *      x - the solution vector
 *      b - the constant vector
 *      n - the order of the equation
*/

void LUsolve(DOUBLE *x, DOUBLE *b, int n) {
  register int i, j;
  DOUBLE dot;

  /* Vector reduction using U triangular matrix */
  for(i=0; i<n; i++) {
    dot = 0.0;
    for(j=0; j<i; j++) dot += LU[ps[i]][j] * x[j];
    x[i] = b[ps[i]] - dot;
  }

  /* Back substitution, in L triangular matrix */
  for(i=n-1; i >= 0; i--) {
   dot = 0.0;
    for(j=i+1; j<n; j++) dot += LU[ps[i]][j] * x[j];
    x[i] = (x[i] - dot) / LU[ps[i]][i];
  }
}

int LUinvFull(DOUBLE **A, DOUBLE **Ainv, int n) {
  int i, j;

  // Decompose matrix into L and U triangular matrices
  if(LUdecompose(A, n) == 0) {
    //Warning("Singular matrix, LU decomposition is impossible\n");
    return 0; // Singular case
  }

  // Invert matrix by solving n simultaneous equations n times
  for(i=0; i<n; i++) {
    for(j=0; j<n; j++) invb[j] = 0.;
    invb[i] = 1.0;
    LUsolve(Ainv[i], invb, n);
  }

  return 1;
}

void FindNonZeroElements(DOUBLE **a, int n1, int n2) {
  int i,j;

  // emty arrays
  for(i=0; i<n1; i++) nz_row_number[i] = 0;
  for(i=0; i<n2; i++) nz_column_number[i] = 0;

  for(i=0; i<n1; i++) { // row
    for(j=0; j<n2; j++) { // column
      if(a[i][j] != 0) {
        nz_row_index[i][nz_row_number[i]++] = j;
        nz_column_index[j][nz_column_number[j]++] = i;
      }
    }
  }
}
  int sign_pivoted = 1; // parity of pivoting

void FindLargestElements(void) {
  register int i,j,sparse;
  int pivotindex = 0;
  DOUBLE biggest, tempf;

  for(j=0; j<Nup; j++) { // loop over columns
    biggest = 0.; // Find the largest element in each column for pivoting
    for(sparse=0; sparse<nz_column_number[j]; sparse++) { // loop over elements in a given column
      i = nz_column_index[j][sparse];
      if(biggest<(tempf = fabs(AI[i][j])) && to_process[i]) {
        biggest  = tempf;
        ps[j] = i; // ps[] contains list (rows) corresponding to the largest element
        pivotindex = i;
      }
    }
    if(biggest == 0.) {
      Warning("  LUinvSparse: singular matrix\n");
      return; // zero row: singular matrix
    }
    if(pivotindex != j) sign_pivoted *= -1; // keep track of the permutation parity
    to_process[pivotindex] = OFF;
  }
}

void DirectLoop(void) {
  register int i,j,k,sparse,sparse2,sparse3;
  int i_pivot;
  DOUBLE pivot;
  int update;
  for(k=0; k<Nup; k++) { // pivot, eliminating an extra variable each time (column)
    i_pivot = ps[k]; // number of the row used for pivoting
    to_process[i_pivot] = OFF; // this pivoting row is no longer processed
    for(sparse=0; sparse<nz_column_number[k]; sparse++) { // the column has following elements (rows) to be eliminated
      i = nz_column_index[k][sparse]; // the index of row to be modified
      if(to_process[i]) { // only elements below diagonal has to be removed
        pivot = AI[i][k]/AI[i_pivot][k];
        for(sparse2=0; sparse2<nz_row_number[i_pivot]; sparse2++) { // elements in the row under modification
          j = nz_row_index[i_pivot][sparse2];

          AI[i][j] -= pivot*AI[i_pivot][j];

          // update tables of non zero elements
          if(j == k) { // this element is zero now
            if(AI[i][j] != 0) {
              if(fabs(AI[i][j]) >1e-14)
                Warning("possible problem in LU sparse decomposition. Error %"LE"\n", fabs(AI[i][j]));
              AI[i][j] = 0.;
            }

            if(sparse != nz_column_number[k]-1) { // update column index
              nz_column_index[k][sparse] = nz_column_index[k][nz_column_number[k]-1]; // rewrite this element with the last of the list
              sparse--; // check new element
            }
            nz_column_number[k]--;

            // update row index
            update = ON;
            for(sparse3=0; sparse3<nz_row_number[i] && update; sparse3++)
              if(nz_row_index[i][sparse3] == j)
                update = OFF; 
            sparse3--; // now sparse3 points to A[i][j] element
            if(sparse3 != nz_row_number[i]-1) {
              nz_row_index[i][sparse3] = nz_row_index[i][nz_row_number[i]-1]; // rewrite this element with the last of the list
            }
            nz_row_number[i]--;
          }
          else { // element [i][j] is non zero now
            update = ON; // update column index
            for(sparse3=0; sparse3<nz_column_number[j] && update; sparse3++)
              if(nz_column_index[j][sparse3] == i)
                update = OFF;
            // i.e. this element is absent in the list
            if(update) 
              nz_column_index[j][nz_column_number[j]++] = i;

            update = ON; //update row index
            for(sparse3=0; sparse3<nz_row_number[i]; sparse3++) 
              if(nz_row_index[i][sparse3] == j)
                update = OFF;
            // i.e. this element is absent in the list
            if(update) nz_row_index[i][nz_row_number[i]++] = j;
          }
        }
      }
    }
  }
}

void InverseLoop(void) {
  register int i,j,k,sparse,sparse2,sparse3;
  int i_pivot;
  DOUBLE pivot;
  int update;
  to_process[ps[Nup-1]] = OFF; // this row contains only one term
  for(k=Nup-1; k>=0; k--) { // go back eliminating one column each time
    i_pivot = ps[k]; // number of the row used for subtraction
    to_process[i_pivot] = OFF; // this pivoting row is no longer processed
    for(sparse=0; sparse<nz_column_number[k]; sparse++) { // the column has following elements (rows) to be eliminated
      i = nz_column_index[k][sparse]; // the index of row to be modified
      if(to_process[i]) { // only some elements should be removed
        pivot = AI[i][k]/AI[i_pivot][k];
        for(sparse2=0; sparse2<nz_row_number[i_pivot]; sparse2++) { // elements in the row under modification
          j = nz_row_index[i_pivot][sparse2];

          AI[i][j] -= pivot*AI[i_pivot][j];

          // update tables of non zero elements
          if(j == k) { // this element is zero now
            if(AI[i][j] != 0) {
              if(fabs(AI[i][j]) >1e-14)
                Warning("possible problem in LU sparse decomposition. Error %"LE"\n", fabs(AI[i][j]));
              AI[i][j] = 0.;
            }

            if(sparse != nz_column_number[k]-1) { // update column index
              nz_column_index[k][sparse] = nz_column_index[k][nz_column_number[k]-1]; // rewrite this element with the last of the list
              sparse--; // check new element
            }
            nz_column_number[k]--;

            // update row index
            update = ON;
            for(sparse3=0; sparse3<nz_row_number[i] && update; sparse3++)
              if(nz_row_index[i][sparse3] == j)
                update = OFF; 
            sparse3--; // now sparse3 points to A[i][j] element
            if(sparse3 != nz_row_number[i]-1) {
              nz_row_index[i][sparse3] = nz_row_index[i][nz_row_number[i]-1]; // rewrite this element with the last of the list
            }
            nz_row_number[i]--;
          }
          else { // element [i][j] is non zero now
            update = ON; // update column index
            for(sparse3=0; sparse3<nz_column_number[j] && update; sparse3++)
              if(nz_column_index[j][sparse3] == i)
                update = OFF;
            // i.e. this element is absent in the list
            if(update) 
              nz_column_index[j][nz_column_number[j]++] = i;

            update = ON; //update row index
            for(sparse3=0; sparse3<nz_row_number[i]; sparse3++) 
              if(nz_row_index[i][sparse3] == j)
                update = OFF;
            // i.e. this element is absent in the list
            if(update) nz_row_index[i][nz_row_number[i]++] = j;
          }
        }
      }
    }
  }
}

int LUinvSparse(DOUBLE **A, DOUBLE **Ainv, int N) {
  register int i,j,sparse;
  int i_pivot;
  DOUBLE pivot;

  for(i=0; i<N; i++) // copy A
    for(j=0; j<N; j++)
      AI[i][j] = A[i][j];

  for(i=0; i<N; i++) // add I part to the wide matrix
    for(j=0; j<N; j++)
      AI[i][N+j] = 0;
  for(i=0; i<N; i++) AI[i][N+i] = 1;

  for(i=0; i<N; i++) to_process[i] = ON; // remembers if this row had been already processed

  FindNonZeroElements(AI, N, 2*N); // update table with non zero elements

  FindLargestElements();

  for(i=0; i<N; i++) to_process[i] = ON; // remembers if this row had been already processed

  DirectLoop();

  // Calculate determinant of the matrix
  determinantLU = sign_pivoted;
  for(i=0; i<N; i++) determinantLU *= AI[ps[i]][i];

  if(AI[ps[N-1]][N-1] == 0.) {
    Warning("  LUinvSparse: singular matrix\n");
    return(0); // singular matrix
  }

  InverseLoop();
  // calculate the inverse matrix
  for(i=0; i<N; i++) to_process[i] = ON; // remembers if this row had been already processed

  // divide by the diagonal terms and copy to the inverse matrix
  for(i=0; i<N; i++)
    for(j=0; j<N; j++)
      Ainv[i][j] = 0.;

  for(i=0; i<N; i++) {
    i_pivot = ps[i];
    pivot = 1./AI[i_pivot][i];
    for(sparse=0; sparse<nz_row_number[i_pivot]; sparse++) {
      j = nz_row_index[i_pivot][sparse];
      if(j>= N) {
        Ainv[j-N][i] = pivot * AI[i_pivot][j];
      }
    }
  }

  return 1;
}

void CopyMatrix(DOUBLE **out, DOUBLE **in, int size) {
  int i,j;

  for(i=0; i<size; i++)
    for(j=0; j<size; j++)
      out[i][j] = in[i][j];
}

void SaveMatrix(char *fileout, DOUBLE **matrix, int size1, int size2) {
  FILE *out;
  int i,j;

  out = fopen(fileout, "w");

  for(i=0; i<size1; i++) {
    for(j=0; j<size2; j++) {
      fprintf(out, "%.15"LE" ", matrix[i][j]);
    }
    fprintf(out, "\n");
  }

  fclose(out);
}

int CompareMatrix(DOUBLE **out, DOUBLE **in, int size) {
  int i,j;
  int ret = 0;

  for(i=0; i<size; i++)
    for(j=0; j<size; j++)
      if(out[i][j] != in[i][j]) ret = 1;

  return ret;
}

#ifdef USE_LAPACK
/************************** parity ******************************************/
int parity(int* permutation, int N) {
 int i=0;
 int parity=1;

 for(i=0;i<N;i++){
  if(permutation[i]!=(i+1)){
   parity*=-1;
  }
 }

 return parity;
}

/************************ LU Inv Lapack *************************************/
int LUinvLapack(DOUBLE **A, DOUBLE **Ainv, int N) {
  DOUBLE *m_mat, *work;
  int *m_pivot;
  int info;
  int global_lwork = -1;
  int i,j;

  // allocate matrix continuously for lapack
  m_mat = (DOUBLE*) Calloc("A", (N+1)*(N+1), sizeof(DOUBLE));
  m_pivot = (int*) Calloc("pivot ", N+1, sizeof(int));
  global_lwork = N; // initialize with N
  work = (DOUBLE*)Calloc("work", global_lwork, sizeof(DOUBLE));
  //if(global_lwork == -1) global_lwork = N; // initialize with N

  for(i=0; i<N; i++) {
    for(j=0; j<N; j++) {
      m_mat[i*N+j] = A[i][j];
    }
  }

  dgetrf_(&N, &N, m_mat, &N, m_pivot, &info);
  // now m_mat contains LU decomposition
  determinantLU = parity(m_pivot, N);
  for(i=0;i<Nup;i++) {
    determinantLU*=m_mat[i*(N+1)];
  }

  //and do the inversion for a given decomposition
  dgetri_(&N, m_mat, &N, m_pivot, work, &global_lwork, &info);
  //global_lwork = (int) work[0]; // recommended buffer size

  // inverse matrix is transposed
  for(i=0; i<N; i++) {
    for(j=0; j<N; j++) {
      Ainv[j][i] = m_mat[i*N+j];
    }
  }

  free(m_mat);
  free(m_pivot);
  free(work);

  return 1;
}

  //Message("Lapack package will be used to do matrix inversion\n");
/*************************************** Exit *******************************/

/*int CheckLapack(void) {

int i, j, c1,c2, nrhs, ipiv[size], ok, LDA, LDB ;
DOUBLE matrix[size][size];
DOUBLE b[size] ;
char trans ;
int lwork = size ;
//DOUBLE work[size] ;

c1 = size ;
c2 = 1 ;
LDA = size ;
LDB = size ;
nrhs = 1 ;
trans = 'N' ;

matrix[0][0] = 2. ;
matrix[1][0] = 1. ;
matrix[2][0] = 1. ;
matrix[0][1] = 4. ;
matrix[1][1] = -6. ;
matrix[2][1] = 0. ;
matrix[0][2] = -2. ;
matrix[1][2] = 7. ;
matrix[2][2] = 2. ;

b[0] = 5. ;
b[1] =-2. ;
b[2] = 9. ;

printf("\noriginal matrix \n\n") ;
for (i=0;i<size;i++) { for(j=0;j<size;j++) { printf("%10.4lf",matrix[j][i]) ; } printf("\n") ; } ;
printf("\n\n") ;

dgetrf_(&c1, &c1, matrix, &LDA, ipiv, &ok) ;

printf("\nLU decomposed matrix \n\n") ;
for (i=0;i<size;i++) { for(j=0;j<size;j++) { printf("%10.4lf",matrix[j][i]) ; } printf("\n") ; } ;
printf("\n\n") ;

printf("row order in terms of original row order\n\n") ;
for(j=0;j<size;j++) { printf("%3d",ipiv[j]) ; } printf("\n") ; 

if (ok !=0) printf("\nok = %3d\n",ok) ;

dgetrs_(&trans, &c1, &nrhs, matrix, &LDA, ipiv, b, &LDB, &ok) ;

printf("\nsolution vector\n\n") ;
for (i=0;i<size;i++) { printf("%10.4lf",b[i]) ; } 

printf("\n\n") ;
}*/

/************************** Check Lapack ************************************/
int CheckLapack(void) {
  int i,j;
  //int t, repeat_times=1000000;
  clock_t time_memcpy;
  DOUBLE time1, time2;
  int check_passed = ON;
  DOUBLE **A1, **A1inv;
  DOUBLE **A2, **A2inv;
  DOUBLE m_determinantLU;
  int N;
  DOUBLE delta;
  int repeat_times;

  Message("  Checking Lapack inversion speed ... \n");

  // allocate matrix continuously for lapack
  N = Nup;
  A1 = (DOUBLE**) Calloc("A ", N, sizeof(DOUBLE*));
  for(i=0; i<N; i++) A1[i] = (DOUBLE*) Calloc("AI ", N, sizeof(DOUBLE));
  A1inv = (DOUBLE**) Calloc("A ", N, sizeof(DOUBLE*));
  for(i=0; i<N; i++) A1inv[i] = (DOUBLE*) Calloc("AI ", N, sizeof(DOUBLE));

  A2 = (DOUBLE**) Calloc("A ", N, sizeof(DOUBLE*));
  for(i=0; i<N; i++) A2[i] = (DOUBLE*) Calloc("AI ", N, sizeof(DOUBLE));
  A2inv = (DOUBLE**) Calloc("A ", N, sizeof(DOUBLE*));
  for(i=0; i<N; i++) A2inv[i] = (DOUBLE*) Calloc("AI ", N, sizeof(DOUBLE));

  // generate random matrix
  Randomize();
  for(i=0; i<N; i++) {
    for(j=0; j<N; j++) {
      A1[i][j] = RandomSys();
      A2[i][j] = A1[i][j];
    }
  }

  // do one inversion to get typical time
  time_memcpy = clock();
  LUinvFull(A1, A1inv, N);
  m_determinantLU = determinantLU;
  time1 = (DOUBLE)(clock()-time_memcpy)/(DOUBLE)CLOCKS_PER_SEC;
  if(time1<1e-4) time1 = 1e-4;

  repeat_times = (int) (1./time1); // at least 1 sec
  if(repeat_times<1) repeat_times = 1;

  time_memcpy = clock();
  for(i=0; i<repeat_times; i++) LUinvSparse(A2, A2inv, N);
  time1 = (DOUBLE)(clock()-time_memcpy)/(DOUBLE)CLOCKS_PER_SEC;
  Message("    sparse matrix LU inversion: %"LE" sec\n", time1);

  time_memcpy = clock();
  for(i=0; i<repeat_times; i++) LUinvFull(A2, A2inv, N);
  time2 = (DOUBLE)(clock()-time_memcpy)/(DOUBLE)CLOCKS_PER_SEC;
  Message("    standard LU inversion: %"LE" sec\n", time2);

  time_memcpy = clock();
  for(i=0; i<repeat_times; i++) LUinvLapack(A1, A1inv, N);
  m_determinantLU = determinantLU;
  time1 = (DOUBLE)(clock()-time_memcpy)/(DOUBLE)CLOCKS_PER_SEC;
  Message("    inversion Lapack: %"LE" sec\n", time1);
  Message("    speed gain: %"LG" times\n", time2/time1);

  // check if inverse matrix coincide
  delta = 0.;
  for(i=0; i<N; i++) {
    for(j=0; j<N; j++) {
      delta += (A1inv[i][j]-A2inv[i][j])*(A1inv[i][j]-A2inv[i][j]);
    }
  }
  if(delta>1e-6) {
    Message("\nStandard routine\n");
    for(i=0; i<N; i++) {
      for(j=0; j<N; j++) {
        Message("%lf ", A2inv[i][j]);
     }
     Message("\n");
   }
    Message("determinant %lf\n", determinantLU);

    Message("\nLapack method\n");
    for(i=0; i<N; i++) {
      for(j=0; j<N; j++) {
        Message("%lf ", A1inv[i][j]);
      }
      Message("\n");
    }

    Message("determinant %lf\n", m_determinantLU);
    Error("\nLapack test failed. The inverse matrix is different. Norm differs by %"LE"\n", delta);
  }

  Message("  done\n");

  free(A1);
  free(A2);
  free(A1inv);
  free(A2inv);

  return check_passed;
}

#endif

