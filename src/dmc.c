/*DMC.c*/

#include <math.h>
#include <stdio.h>
#include "main.h"
#include "dmc.h"
#include "randnorm.h"
#include "utils.h"
#include "move.h"
#include "trial.h"
#include "rw.h"
#include "compatab.h"
#include "display.h"
#include "crystal.h"
#include "fermions.h"
#include "trialf.h"
#include "spline.h"
#include "memory.h"
#include "fewbody.h"

#ifdef MPI
# include "parallel.h"
#endif

/**************************** DMC Quadratic Move *********************************/
void DMCQuadraticMove(int w) {
  int go = ON;
  int i,j;
#ifdef CHECK_FORCE_ERROR
  DOUBLE integration_error;
  int integration_reduce_times;
#endif

  if(var_par_array) AdjustVariationalParameter(W[w].varparindex);

#ifdef CHECK_OVERLAPPING
  while(go == ON) {
    go = OFF;
    tried++;
#endif

  // R = R(i-1) + chi, f(chi) = exp(-R^2/4dt) 
  GaussianJump(w, sqrt(dt));

#ifdef CHECK_OVERLAPPING
#ifdef SECURE
  if(Overlapping(W[w].R)) Error("Gaussian jump : impossible situation: (1)");
#endif
#endif

  // R' = R + F(R) dt/2 
    DriftJump(0.5*dt, w);

#ifdef CHECK_OVERLAPPING
  if(Overlapping(W[w].Rp)) {
    overlaped++;
    go = ON; 
    //W[w].status = DEAD;
    //W[w].weight = 0.;
  }
  else {
#endif

  // R" = R + (F(R)+F(R') dt/4
  DriftJump1(0.25*dt, w);

#ifdef CHECK_OVERLAPPING
  if(Overlapping(W[w].Rpp)) {
    overlaped++;
    go = ON; 
    //W[w].status = DEAD;
    //W[w].weight = 0.;
  }
  else {
#endif

  CopyVectorToWalker(&Wp[w], W[w].Rpp);
  if(var_par_array) Wp[w].varparindex = W[w].varparindex;
  Wp[w].w = W[w].w;
  Wp[w].E = WalkerEnergy0(&Wp[w]);
  Wp[w].z2 = W[w].z2;
  Wp[w].r2 = W[w].r2;

#ifdef SECURE
  if(Wp[w].E>Eck) {
    Warning("DMC WARNING: large energy %"LF"\n", Wp[w].E);
    if(branchng_present) {
      overlaped++;
      go = ON; 
      //W[w].status = DEAD;
      //W[w].weight = 0.;
    }
  }
  else if(Wp[w].E<-Eck) {
    Warning("DMC WARNING: negative energy %"LF"\n", Wp[w].E);

    Message("walker coordinates:\n");
    for(j=0; j<Nup; j++) Message("%lf %lf %lf\n", Wp[w].x[j], Wp[w].y[j], Wp[w].z[j]);
    for(j=0; j<Ndn; j++) Message("%lf %lf %lf\n", Wp[w].xdn[j], Wp[w].ydn[j], Wp[w].zdn[j]);

    if(branchng_present) {
      overlaped++;
    }
  }
  else {
#endif
  W[w].Eold = W[w].E;
  W[w].E = Wp[w].E;
  W[w].Epot = Wp[w].Epot;
  W[w].Ekin = Wp[w].Ekin;
  W[w].EFF = Wp[w].EFF;
  W[w].Eext = Wp[w].Eext;
  W[w].z2 = Wp[w].z2;
  W[w].r2 = Wp[w].r2;
  W[w].r2old = Wp[w].r2old;
  W[w].z2old = Wp[w].z2old;

  // R"' = R + F(R") dt
  DriftJump2(dt, w);

#ifdef CHECK_FORCE_ERROR
  // Estimate error of integration by comparison of linear and quadratic formulae
  // 1) R"' = R + F(R) dt,  Force stored in F[]
  // 2) R"' = R + F(R") dt, Force stored in Fpp[]
  integration_error = 0.;
  for(i=0; i<Nmax; i++) for(j=0; j<6; j++) integration_error += (F[i][j]-Fpp[i][j])*(F[i][j]-Fpp[i][j]);
  integration_error = sqrt(integration_error)*dt;
  //Message("%.5e %.5e %.5e %.5e %.5e\n", integration_error, energy_unit*(E+energy_shift), (EFF+energy_shift)*energy_unit, energy_unit*(Wp[w].E)/Nup, energy_unit*(Wp[w].EFF)/Nup);//!

  integration_reduce_times = 0;
  //Message("%.5e %.5e ", integration_error, energy_unit*(Wp[w].E));
  if(integration_error>1e-7) {
    integration_reduce_times = (int) (integration_error*1e7);
    //if(integration_reduce_times>10) integration_reduce_times = 10;
    DMCQuadraticMoveReduceTimestep(w, integration_reduce_times);
    integration_error = 0.;
    for(i=0; i<Nmax; i++) for(j=0; j<6; j++) integration_error += (F[i][j]-Fpp[i][j])*(F[i][j]-Fpp[i][j]);
    integration_error = sqrt(integration_error)*dt/((DOUBLE)integration_reduce_times*2.);
  }
  //Message("%.5e %.5e %i\n", integration_error, energy_unit*(Wp[w].E), integration_reduce_times);
#endif

#ifdef CHECK_OVERLAPPING
  if(!Overlapping(W[w].Rppp)) {
#endif
  CopyVectorToWalker(&W[w], W[w].Rppp);
#ifdef CHECK_OVERLAPPING
    go = OFF;
  }
  else {
    overlaped++;
    go = ON; 
    //W[w].status = DEAD;
    //W[w].weight = 0.;
  }}}}
#endif
#ifdef SECURE
  }
#endif
}

/**************************** DMC Quadratic Move *********************************/
void DMCQuadraticMoveReduceTimestep(int w, int reduce_times_half) {
  int i,j,t;
  DOUBLE dt_reduced;

  dt_reduced = dt / (DOUBLE) (2.*reduce_times_half);

  for(t=0; t<reduce_times_half; t++) {
    // R' = R + F(R) dt/2 
    DriftJump(dt_reduced, w); // 0.5x2
    // R" = R + (F(R)+F(R') dt/4
    DriftJump1(0.5*dt_reduced, w); //0.25x2
    // R"' = R + F(R") dt
    DriftJump2(2.*dt_reduced, w); //1x2

    for(i=0; i<Nmax; i++) for(j=0; j<6; j++) W[w].R[i][j] = W[w].Rppp[i][j];
  }
  CopyVectorToWalker(&Wp[w], W[w].Rpp);
  Wp[w].E = WalkerEnergy0(&Wp[w]);
  Wp[w].z2 = W[w].z2;
  Wp[w].r2 = W[w].r2;

  W[w].Eold = W[w].E;
  W[w].E = Wp[w].E;
  W[w].Epot = Wp[w].Epot;
  W[w].Ekin = Wp[w].Ekin;
  W[w].EFF = Wp[w].EFF;
  W[w].Eext = Wp[w].Eext;
  W[w].z2 = Wp[w].z2;
  W[w].r2 = Wp[w].r2;
  W[w].r2old = Wp[w].r2old;
  W[w].z2old = Wp[w].z2old;

  for(t=0; t<reduce_times_half; t++) {
    // R' = R + F(R) dt/2 
    DriftJump(dt_reduced, w); //0.5x2
    // R" = R + (F(R)+F(R') dt/4
    DriftJump1(0.5*dt_reduced, w); //0.25x2
    // R"' = R + F(R") dt
    DriftJump2(2.*dt_reduced, w); //1x2

    for(i=0; i<Nmax; i++) for(j=0; j<6; j++) W[w].R[i][j] = W[w].Rppp[i][j];
  }
}

/**************************** DMC Simple Move *********************************/
void DMCLinearMoveGaussian(int w) {
  int go = ON;
  int i,j;

  //if(var_par_array) AdjustVariationalParameter(W[w].varparindex);
  while(go == ON) {
    tried++;
    // R = R(i-1) + chi, f(chi) = Exp(-R^2/2dt)
    // Exp(-(x-mu)^2/(2 sigma^2))
    GaussianJump(w, Sqrt(dt)); // R = W[w] + chi, f(chi) = exp(-R^2/4dt)

    CopyVectorToWalker(&W[w], W[w].R);
    ReduceWalkerToTheBox(&W[w]);
    W[w].Eold = W[w].E;
    W[w].E = WalkerEnergy0(&W[w]);

    // R' = R + F(R) dt
    DriftJump(dt, w);

#ifdef HARD_SPHERE
    if(Overlapping(W[w].Rp)) {
      overlaped++;
    }
    else
#endif
    {
      CopyVectorToWalker(&W[w], W[w].Rp);
      ReduceWalkerToTheBox(&W[w]);
      go = OFF;
    }
  }
}

/********************* DMC Linear Middle Move *********************************/
void DMCLinearMoveMiddle(int w) {
  int i,j;

  if(var_par_array) AdjustVariationalParameter(W[w].varparindex);

  // R = R(0) + chi
  GaussianJump(w, sqrt(dt));

  // R' = R + F(R) dt/2 
  DriftJump(0.5*dt, w);

  // Calculate energy
  W[w].Eold = W[w].E;
  CopyVectorToWalker(&W[w], W[w].Rp);
  W[w].E = WalkerEnergy0(&W[w]);

  // R'' = R' + F(R') dt/2 
  for(i=0; i<Nup; i++) { // Drift Force, up
    CaseX(W[w].x[i] += W[w].F[i][0]*m_up_inv*0.5*dt);
    CaseY(W[w].y[i] += W[w].F[i][1]*m_up_inv*0.5*dt);
    CaseZ(W[w].z[i] += W[w].F[i][2]*m_up_inv*0.5*dt);
    if(measure_SD) for(j=0; j<3; j++) W[w].rreal[i][j] += W[w].F[i][j]*m_up_inv*0.5*dt;
  }
  for(i=0; i<Ndn; i++) { // Drift Force, dn
    CaseX(W[w].xdn[i] += W[w].F[i][3]*m_dn_inv*0.5*dt);
    CaseY(W[w].ydn[i] += W[w].F[i][4]*m_dn_inv*0.5*dt);
    CaseZ(W[w].zdn[i] += W[w].F[i][5]*m_dn_inv*0.5*dt);
    if(measure_SD) for(j=3; j<6; j++) W[w].rreal[i][j] += W[w].F[i][3]*m_dn_inv*0.5*dt;
  }
  ReduceWalkerToTheBox(&W[w]);
}

/**************************** DMC Linear Metropolis Move ******************/
// Gaussian move is done with dt
// Drift move is done with dt_all
void  DMCLinearMetropolisMove(int w)  {
  DOUBLE dexp = 0.;
  DOUBLE xi,dx,dy,dz;
  int move_accepted;
  int i,j;
  DOUBLE WE, WEFF, WEpot, WEkin; // store energy calculated in the middle of drift
  DOUBLE dt_drift;

  // dt_drift = 0 - purely Gaussian diffusion
  dt_drift = dt;
  //dt_drift = dt_all;

  if(!W[w].status_end_of_step_initialized) { // first iteration needs to be initialized
    CopyWalkerToVector(W[w].R, W[w]);
    DriftForce(W[w].F, W[w].R);
    for(i=0; i<Nup; i++) { // one step backwards
      CaseX(W[w].dR_drift_old[i][0] = dt_drift*W[w].F[i][0]); // initialize with an approximate value
      CaseY(W[w].dR_drift_old[i][1] = dt_drift*W[w].F[i][1]);
      CaseZ(W[w].dR_drift_old[i][2] = dt_drift*W[w].F[i][2]);
    }
    for(i=0; i<Ndn; i++) {
      CaseX(W[w].dR_drift_old[i][3] = dt_drift*W[w].F[i][3]);
      CaseY(W[w].dR_drift_old[i][4] = dt_drift*W[w].F[i][4]);
      CaseZ(W[w].dR_drift_old[i][5] = dt_drift*W[w].F[i][5]);
    }
    W[w].status_end_of_step_initialized = ON;
  }

  // R = R(i-1) + F[R(i-1)]*dt_drift + chi, f(chi) = const Exp(-R^2/2dt)
  for(i=0; i<Nup; i++) {
    RandomNormal3(&dx, &dy, &dz, 0., sqrt(dt)); // (x,y,z, mu, sigma)
    CaseX(W[w].R[i][0] = W[w].x[i] + W[w].dR_drift_old[i][0] + dx);
    CaseY(W[w].R[i][1] = W[w].y[i] + W[w].dR_drift_old[i][1] + dy);
    CaseZ(W[w].R[i][2] = W[w].z[i] + W[w].dR_drift_old[i][2] + dz);

    ReduceToTheBox(W[w].R[i]);

    CaseX(W[w].dR[i][0] = dx); // (1/2): dR' = chi
    CaseY(W[w].dR[i][1] = dy);
    CaseZ(W[w].dR[i][2] = dz);

    CaseX(dexp += dx*dx); // dR: direct transition probability, P(R->R') = exp(-chi^2/2dt)
    CaseY(dexp += dy*dy);
    CaseZ(dexp += dz*dz);
  }
  for(i=0; i<Ndn; i++) {
    RandomNormal3(&dx, &dy, &dz, 0., sqrt(dt));
    CaseX(W[w].R[i][3] = W[w].xdn[i] + W[w].dR_drift_old[i][3] + dx); 
    CaseY(W[w].R[i][4] = W[w].ydn[i] + W[w].dR_drift_old[i][4] + dy);
    CaseZ(W[w].R[i][5] = W[w].zdn[i] + W[w].dR_drift_old[i][5] + dz);

    ReduceToTheBox(&W[w].R[i][3]);

    CaseX(W[w].dR[i][3] = dx); // (1/3): dR' = chi
    CaseY(W[w].dR[i][4] = dy);
    CaseZ(W[w].dR[i][5] = dz);

    CaseX(dexp += dx*dx); // dR: direct transition probability, P(R->R') = exp(-chi^2/2dt)
    CaseY(dexp += dy*dy);
    CaseZ(dexp += dz*dz);
  }

  // R' = R + F(R) dt
  DriftForce(W[w].F, W[w].R);

  // (2/2): dR' = chi + F(R') dt_drift
  for(i=0; i<Nup; i++) {
    CaseX(dx = dt_drift*W[w].F[i][0]);
    CaseY(dy = dt_drift*W[w].F[i][1]);
    CaseZ(dz = dt_drift*W[w].F[i][2]);

    CaseX(W[w].dR[i][0] += dx); // (2/3) dR' = chi + F(R) dt
    CaseY(W[w].dR[i][1] += dy);
    CaseZ(W[w].dR[i][2] += dz);

    CaseX(W[w].dR_drift[i][0] = dx); // needed for the next step
    CaseY(W[w].dR_drift[i][1] = dy);
    CaseZ(W[w].dR_drift[i][2] = dz);

    CaseX(W[w].Rp[i][0] = W[w].R[i][0] + dx);
    CaseY(W[w].Rp[i][1] = W[w].R[i][1] + dy);
    CaseZ(W[w].Rp[i][2] = W[w].R[i][2] + dz);
#ifdef PARTICLES_NEVER_LEAVE_THE_BOX
    ReduceToTheBox(W[w].Rp[i]);
#endif
  }
  for(i=0; i<Ndn; i++) {
    CaseX(dx = dt_drift*W[w].F[i][3]);
    CaseY(dy = dt_drift*W[w].F[i][4]);
    CaseZ(dz = dt_drift*W[w].F[i][5]);

    CaseX(W[w].dR[i][3] += dx);
    CaseY(W[w].dR[i][4] += dy);
    CaseZ(W[w].dR[i][5] += dz);

    CaseX(W[w].dR_drift[i][3] = dx);
    CaseY(W[w].dR_drift[i][4] = dy);
    CaseZ(W[w].dR_drift[i][5] = dz);

    CaseX(W[w].Rp[i][3] = W[w].R[i][3] + dx);
    CaseY(W[w].Rp[i][4] = W[w].R[i][4] + dy);
    CaseZ(W[w].Rp[i][5] = W[w].R[i][5] + dz);

#ifdef PARTICLES_NEVER_LEAVE_THE_BOX
    ReduceToTheBox(&W[w].Rp[i][3]);
#endif
  }

  // Metropolis move : w = 2[U(R')-U(R)] + [dR^2-dR'^2]/(2dt)
  CopyVectorToWalker(&Wp[w], W[w].R); // calculate new weight in R' (after Gaussian)
  ReduceWalkerToTheBox(&Wp[w]);

  Wp[w].U = U(Wp[w]);

  for(i=0; i<Nup; i++) { // with saved previous displacement:
    CaseX(dx = W[w].dR[i][0] + W[w].dR_drift_old[i][0]); // (3/3): dR' = chi + F(R')dt + F(R)dt
    CaseY(dy = W[w].dR[i][1] + W[w].dR_drift_old[i][1]);
    CaseZ(dz = W[w].dR[i][2] + W[w].dR_drift_old[i][2]);

    CaseX(dexp -= dx*dx); // dR': inverse transition probability P(R'->R)
    CaseY(dexp -= dy*dy);
    CaseZ(dexp -= dz*dz);
  }
  for(i=0; i<Ndn; i++) {
    CaseX(dx = W[w].dR[i][3] + W[w].dR_drift_old[i][3]);
    CaseY(dy = W[w].dR[i][4] + W[w].dR_drift_old[i][4]);
    CaseZ(dz = W[w].dR[i][5] + W[w].dR_drift_old[i][5]);

    CaseX(dexp -= dx*dx);
    CaseY(dexp -= dy*dy);
    CaseZ(dexp -= dz*dz);
  }

  dexp = 2.*(Wp[w].U - W[w].U) + dexp/(2.*dt);

  // The Metropolis code
  if(dexp > 0.) {
    move_accepted = ON;
  }
  else {
    xi = Random();
    if(xi<Exp(dexp)) {
      move_accepted = ON;
    }
    else {
      rejected++;
      return;
    }
  }

  if(move_accepted) {
    accepted++;
    CopyVectorToWalker(&W[w], W[w].R); // move is accepted, update walker coordinates (after Gaussian)
    ReduceWalkerToTheBox(&W[w]);
    CopyVector(W[w].dR_drift_old, W[w].dR_drift); // store the deterministic shift due to the force

    W[w].U = Wp[w].U;
    W[w].Eold = W[w].E;
    W[w].E = WalkerEnergy(&W[w], &W[w].Epot, &W[w].Ekin, &W[w].EFF, &W[w].Eext);
    if(measure_SD) { // update SD with dR = chi + F(R)dt
      for(i=0; i<Nup; i++) {
        CaseX(W[w].rreal[i][0] += W[w].dR[i][0]);
        CaseY(W[w].rreal[i][1] += W[w].dR[i][1]);
        CaseZ(W[w].rreal[i][2] += W[w].dR[i][2]);
      }
      for(i=0; i<Ndn; i++) {
        CaseX(W[w].rreal[i][3] += W[w].dR[i][3]);
        CaseY(W[w].rreal[i][4] += W[w].dR[i][4]);
        CaseZ(W[w].rreal[i][5] += W[w].dR[i][5]);
      }
    }
  }
}

/**************************** DMC Linear Metropolis Move ******************/
void  DMCLinearMetropolisMoveMiddle(int w)  {
  DOUBLE dexp = 0.;
  DOUBLE xi,dx,dy,dz;
  int move_accepted;
  int i,j;
  DOUBLE WE, WEFF, WEpot, WEkin; // store energy calculated in the middle of drift

  if(!W[w].status_end_of_step_initialized) { // first iteration needs to be initialized
    CopyWalkerToVector(W[w].R, W[w]);
    DriftForce(W[w].F, W[w].R);
    for(i=0; i<Nup; i++) { // one step backwards
      CaseX(W[w].dR_drift_old[i][0] = dt*W[w].F[i][0]); // initialize with an approximate value
      CaseY(W[w].dR_drift_old[i][1] = dt*W[w].F[i][1]);
      CaseZ(W[w].dR_drift_old[i][2] = dt*W[w].F[i][2]);
    }
    for(i=0; i<Ndn; i++) {
      CaseX(W[w].dR_drift_old[i][3] = dt*W[w].F[i][3]);
      CaseY(W[w].dR_drift_old[i][4] = dt*W[w].F[i][4]);
      CaseZ(W[w].dR_drift_old[i][5] = dt*W[w].F[i][5]);
    }
    W[w].status_end_of_step_initialized = ON;
  }

  // R = R(i-1) + F[R(i-1)]*dr + chi, f(chi) = const Exp(-R^2/2dt)
  for(i=0; i<Nup; i++) {
    RandomNormal3(&dx, &dy, &dz, 0., sqrt(dt)); //(x,y,z, mu, sigma)
    CaseX(W[w].R[i][0] = W[w].x[i] + W[w].dR_drift_old[i][0] + dx);
    CaseY(W[w].R[i][1] = W[w].y[i] + W[w].dR_drift_old[i][1] + dy);
    CaseZ(W[w].R[i][2] = W[w].z[i] + W[w].dR_drift_old[i][2] + dz);
    ReduceToTheBox(W[w].R[i]);

    CaseX(W[w].dR[i][0] = dx); // (1/2): dR' = chi
    CaseY(W[w].dR[i][1] = dy);
    CaseZ(W[w].dR[i][2] = dz);

    CaseX(dexp += dx*dx); // dR: direct transition probability, P(R->R') = exp(-chi^2/2dt)
    CaseY(dexp += dy*dy);
    CaseZ(dexp += dz*dz);
  }
  for(i=0; i<Ndn; i++) {
    RandomNormal3(&dx, &dy, &dz, 0., sqrt(dt));
    CaseX(W[w].R[i][3] = W[w].xdn[i] + W[w].dR_drift_old[i][3] + dx); 
    CaseY(W[w].R[i][4] = W[w].ydn[i] + W[w].dR_drift_old[i][4] + dy);
    CaseZ(W[w].R[i][5] = W[w].zdn[i] + W[w].dR_drift_old[i][5] + dz);

    ReduceToTheBox(&W[w].R[i][3]);

    CaseX(W[w].dR[i][3] = dx); // (1/2): dR' = chi
    CaseY(W[w].dR[i][4] = dy);
    CaseZ(W[w].dR[i][5] = dz);

    CaseX(dexp += dx*dx); // dR: direct transition probability, P(R->R') = exp(-chi^2/2dt)
    CaseY(dexp += dy*dy);
    CaseZ(dexp += dz*dz);
  }

  // R' = R + F(R) dt
  DriftForce(W[w].F, W[w].R);

  // (2/2): dR' = chi + F(R')dt
  for(i=0; i<Nup; i++) {
    CaseX(dx = dt*W[w].F[i][0]);
    CaseY(dy = dt*W[w].F[i][1]);
    CaseZ(dz = dt*W[w].F[i][2]);

    CaseX(W[w].dR[i][0] += dx); 
    CaseY(W[w].dR[i][1] += dy);
    CaseZ(W[w].dR[i][2] += dz);

    CaseX(W[w].dR_drift[i][0] = dx); // needed for the next step
    CaseY(W[w].dR_drift[i][1] = dy);
    CaseZ(W[w].dR_drift[i][2] = dz);

    CaseX(W[w].Rp[i][0] = W[w].R[i][0] + dx);
    CaseY(W[w].Rp[i][1] = W[w].R[i][1] + dy);
    CaseZ(W[w].Rp[i][2] = W[w].R[i][2] + dz);
//#ifdef PARTICLES_NEVER_LEAVE_THE_BOX
//    ReduceToTheBox(W[w].Rp[i]);
//#endif
  }
  for(i=0; i<Ndn; i++) {
    CaseX(dx = dt*W[w].F[i][3]);
    CaseY(dy = dt*W[w].F[i][4]);
    CaseZ(dz = dt*W[w].F[i][5]);

    CaseX(W[w].dR[i][3] += dx);
    CaseY(W[w].dR[i][4] += dy);
    CaseZ(W[w].dR[i][5] += dz);

    CaseX(W[w].dR_drift[i][3] = dx);
    CaseY(W[w].dR_drift[i][4] = dy);
    CaseZ(W[w].dR_drift[i][5] = dz);

    CaseX(W[w].Rp[i][3] = W[w].R[i][3] + dx);
    CaseY(W[w].Rp[i][4] = W[w].R[i][4] + dy);
    CaseZ(W[w].Rp[i][5] = W[w].R[i][5] + dz);
  }

  // Metropolis move : w = 2[U(R')-U(R)] + [dR^2-dR'^2]/(2dt)
  CopyVectorToWalker(&Wp[w], W[w].R); // calculate new weight in R' (after Gaussian)
  ReduceWalkerToTheBox(&Wp[w]);

  Wp[w].U = U(Wp[w]);

  for(i=0; i<Nup; i++) { // with saved previous displacement:
    CaseX(dx = W[w].dR[i][0] + W[w].dR_drift_old[i][0]); // (3/3): dR' = chi + F(R')dt + F(R)dt
    CaseY(dy = W[w].dR[i][1] + W[w].dR_drift_old[i][1]);
    CaseZ(dz = W[w].dR[i][2] + W[w].dR_drift_old[i][2]);

    CaseX(dexp -= dx*dx); // dR': inverse transition probability P(R'->R)
    CaseY(dexp -= dy*dy);
    CaseZ(dexp -= dz*dz);
  }
  for(i=0; i<Ndn; i++) {
    CaseX(dx = W[w].dR[i][3] + W[w].dR_drift_old[i][3]);
    CaseY(dy = W[w].dR[i][4] + W[w].dR_drift_old[i][4]);
    CaseZ(dz = W[w].dR[i][5] + W[w].dR_drift_old[i][5]);

    CaseX(dexp -= dx*dx);
    CaseY(dexp -= dy*dy);
    CaseZ(dexp -= dz*dz);
  }

  dexp = 2.*(Wp[w].U - W[w].U) + dexp/(2.*dt);

  // The Metropolis code
  if(dexp > 0.) {
    move_accepted = ON;
  }
  else {
    xi = Random();
    if(xi<Exp(dexp)) {
      move_accepted = ON;
    }
    else {
      rejected++;
      return;
    }
  }

  if(move_accepted) {
    accepted++;

    // R' = R + F(R) dt/2 
    for(i=0; i<Nup; i++) {
      CaseX(W[w].x[i] = W[w].R[i][0] + 0.5*W[w].dR_drift[i][0]);
      CaseX(W[w].y[i] = W[w].R[i][1] + 0.5*W[w].dR_drift[i][1]);
      CaseX(W[w].z[i] = W[w].R[i][2] + 0.5*W[w].dR_drift[i][2]);
    }
    for(i=0; i<Ndn; i++) {
      CaseX(W[w].xdn[i] = W[w].R[i][3] + 0.5*W[w].dR_drift[i][3]);
      CaseX(W[w].ydn[i] = W[w].R[i][4] + 0.5*W[w].dR_drift[i][4]);
      CaseX(W[w].zdn[i] = W[w].R[i][5] + 0.5*W[w].dR_drift[i][5]);
    }
    ReduceWalkerToTheBox(&W[w]);

    // Calculate energy
    W[w].Eold = W[w].E;
    W[w].E = WalkerEnergy(&W[w], &W[w].Epot, &W[w].Ekin, &W[w].EFF, &W[w].Eext);

    // proceed
    CopyVectorToWalker(&W[w], W[w].R); // move is accepted, update walker coordinates (after Gaussian)
    ReduceWalkerToTheBox(&W[w]);
    CopyVector(W[w].dR_drift_old, W[w].dR_drift); // store the deterministic shift due to the force
    W[w].U = Wp[w].U;
    if(measure_SD) { // update SD with dR = chi + F(R)dt
      for(i=0; i<Nup; i++) {
        CaseX(W[w].rreal[i][0] += W[w].dR[i][0]);
        CaseY(W[w].rreal[i][1] += W[w].dR[i][1]);
        CaseZ(W[w].rreal[i][2] += W[w].dR[i][2]);
      }
      for(i=0; i<Ndn; i++) {
        CaseX(W[w].rreal[i][3] += W[w].dR[i][3]);
        CaseY(W[w].rreal[i][4] += W[w].dR[i][4]);
        CaseZ(W[w].rreal[i][5] += W[w].dR[i][5]);
      }
    }
  }
}


/**********************************Gaussian Jump *****************************/
// R = W[w] + chi, f(chi) = exp(-R^2/2dt)
void GaussianJump(int w, DOUBLE sigma) {
  int i;
  DOUBLE dx, dy, dz;

  for(i=0; i<Nup; i++) {
    RandomNormal3(&dx, &dy, &dz, 0., sigma); // (x,y,z, mu, sigma)
    CaseX(W[w].R[i][0] = W[w].x[i] + m_up_inv_sqrt*dx);
    CaseY(W[w].R[i][1] = W[w].y[i] + m_up_inv_sqrt*dy);
    CaseZ(W[w].R[i][2] = W[w].z[i] + m_up_inv_sqrt*dz);
    if(measure_SD) {
      CaseX(W[w].rreal[i][0] += m_up_inv_sqrt*dx);
      CaseY(W[w].rreal[i][1] += m_up_inv_sqrt*dy);
      CaseZ(W[w].rreal[i][2] += m_up_inv_sqrt*dz);
    }
    ReduceToTheBox(W[w].R[i]);
  }

  for(i=0; i<Ndn; i++) {
    RandomNormal3(&dx, &dy, &dz, 0., sigma);
    CaseX(W[w].R[i][3] = W[w].xdn[i] + m_dn_inv_sqrt*dx);
    CaseY(W[w].R[i][4] = W[w].ydn[i] + m_dn_inv_sqrt*dy);
    CaseZ(W[w].R[i][5] = W[w].zdn[i] + m_dn_inv_sqrt*dz);
    if(measure_SD) {
      CaseX(W[w].rreal[i][3] += m_dn_inv_sqrt*dx);
      CaseY(W[w].rreal[i][4] += m_dn_inv_sqrt*dy);
      CaseZ(W[w].rreal[i][5] += m_dn_inv_sqrt*dz);
    }
    ReduceToTheBox(&W[w].R[i][3]);
  }
#ifdef CHECK_OVERLAPPING
  if(Overlapping(W[w].R)) {
    tried++;
    overlaped++;
    GaussianJump(w, sigma);
  }
#endif
}

/********************************** Gaussian Jump R ***************************/
// Rp = R + chi, f(chi) = exp(-R^2/4dt)
// add Gaussian move to W[w].R
void GaussianJumpR(int w, DOUBLE sigma) {
  int i;
  DOUBLE dx, dy, dz;

  for(i=0; i<Nup; i++) {
    RandomNormal3(&dx, &dy, &dz, 0., sigma);

    CaseX(W[w].Rp[i][0] = W[w].R[i][0] + m_up_inv_sqrt*dx);
    CaseY(W[w].Rp[i][1] = W[w].R[i][1] + m_up_inv_sqrt*dy);
    CaseZ(W[w].Rp[i][2] = W[w].R[i][2] + m_up_inv_sqrt*dz);
    if(measure_SD) {
      CaseX(W[w].rreal[i][0] += m_up_inv_sqrt*dx);
      CaseY(W[w].rreal[i][1] += m_up_inv_sqrt*dy);
      CaseZ(W[w].rreal[i][2] += m_up_inv_sqrt*dz);
    }
    ReduceToTheBox(W[w].Rp[i]);
  }

  for(i=0; i<Ndn; i++) {
    RandomNormal3(&dx, &dy, &dz, 0., sigma);
    CaseX(W[w].Rp[i][3] = W[w].Rp[i][3] + m_dn_inv_sqrt*dx);
    CaseY(W[w].Rp[i][4] = W[w].Rp[i][4] + m_dn_inv_sqrt*dy);
    CaseZ(W[w].Rp[i][5] = W[w].Rp[i][5] + m_dn_inv_sqrt*dz);
    if(measure_SD) {
      CaseX(W[w].rreal[i][3] += m_dn_inv_sqrt*dx);
      CaseY(W[w].rreal[i][4] += m_dn_inv_sqrt*dy);
      CaseZ(W[w].rreal[i][5] += m_dn_inv_sqrt*dz);
    }
    ReduceToTheBox(&W[w].Rp[i][3]);
  }

#ifdef CHECK_OVERLAPPING
  if(Overlapping(W[w].Rp)) { // rethrow
    tried++;
    overlaped++;
    GaussianJumpR(w, sigma);
  }
#endif
}

/********************************** Drift Jump *******************************/
// R' = R + F(R) dt/2
// Rp = R + F dt
void DriftJump(DOUBLE dt, int w) {
  int i;

  DriftForce(W[w].F, W[w].R);

  for(i=0; i<Nup; i++) {
    CaseX(W[w].Rp[i][0] = W[w].R[i][0] + m_up_inv*dt*W[w].F[i][0]);
    CaseY(W[w].Rp[i][1] = W[w].R[i][1] + m_up_inv*dt*W[w].F[i][1]);
    CaseZ(W[w].Rp[i][2] = W[w].R[i][2] + m_up_inv*dt*W[w].F[i][2]);
    ReduceToTheBox(W[w].Rp[i]);
  }

  for(i=0; i<Ndn; i++) {
    CaseX(W[w].Rp[i][3] = W[w].R[i][3] + m_dn_inv*dt*W[w].F[i][3]);
    CaseY(W[w].Rp[i][4] = W[w].R[i][4] + m_dn_inv*dt*W[w].F[i][4]);
    CaseZ(W[w].Rp[i][5] = W[w].R[i][5] + m_dn_inv*dt*W[w].F[i][5]);
    ReduceToTheBox(&W[w].Rp[i][3]);
  }
}

/********************************** Drift Jump 1 *****************************/
/* R" = R + (F(R)+F(R') dt, dt = t/4 */
void DriftJump1(DOUBLE dt, int w) {
  int i;

  DriftForce(W[w].Fp, W[w].Rp);

  for(i=0; i<Nup; i++) {
    CaseX(W[w].Rpp[i][0] = W[w].R[i][0] + m_up_inv*dt*(W[w].F[i][0] + W[w].Fp[i][0]));
    CaseY(W[w].Rpp[i][1] = W[w].R[i][1] + m_up_inv*dt*(W[w].F[i][1] + W[w].Fp[i][1]));
    CaseZ(W[w].Rpp[i][2] = W[w].R[i][2] + m_up_inv*dt*(W[w].F[i][2] + W[w].Fp[i][2]));
    ReduceToTheBox(W[w].Rpp[i]);
  }

  for(i=0; i<Ndn; i++) {
    CaseX(W[w].Rpp[i][3] = W[w].R[i][3] + m_dn_inv*dt*(W[w].F[i][3] + W[w].Fp[i][3]));
    CaseY(W[w].Rpp[i][4] = W[w].R[i][4] + m_dn_inv*dt*(W[w].F[i][4] + W[w].Fp[i][4]));
    CaseZ(W[w].Rpp[i][5] = W[w].R[i][5] + m_dn_inv*dt*(W[w].F[i][5] + W[w].Fp[i][5]));
    ReduceToTheBox(&W[w].Rpp[i][3]);
  }
}

/********************************** Drift Jump 2 ****************************/
// R(i+1) = R(i) + F(R") dt
void DriftJump2(DOUBLE dt, int w) {
  int i;

  DriftForce(W[w].Fpp, W[w].Rpp);

  for(i=0; i<Nup; i++) {
    CaseX(W[w].Rppp[i][0] = W[w].R[i][0] + m_up_inv*dt*W[w].Fpp[i][0]);
    CaseY(W[w].Rppp[i][1] = W[w].R[i][1] + m_up_inv*dt*W[w].Fpp[i][1]);
    CaseZ(W[w].Rppp[i][2] = W[w].R[i][2] + m_up_inv*dt*W[w].Fpp[i][2]);
    ReduceToTheBox(W[w].Rppp[i]);
    if(measure_SD) {
      CaseX(W[w].rreal[i][0] += m_up_inv*dt*W[w].Fpp[i][0]);
      CaseY(W[w].rreal[i][1] += m_up_inv*dt*W[w].Fpp[i][1]);
      CaseZ(W[w].rreal[i][2] += m_up_inv*dt*W[w].Fpp[i][2]);
    }
  }

  for(i=0; i<Ndn; i++) {
    CaseX(W[w].Rppp[i][3] = W[w].R[i][3] + m_dn_inv*dt*W[w].Fpp[i][3]);
    CaseY(W[w].Rppp[i][4] = W[w].R[i][4] + m_dn_inv*dt*W[w].Fpp[i][4]);
    CaseZ(W[w].Rppp[i][5] = W[w].R[i][5] + m_dn_inv*dt*W[w].Fpp[i][5]);
    ReduceToTheBox(&W[w].Rppp[i][3]);
    if(measure_SD) {
      CaseX(W[w].rreal[i][3] += m_dn_inv*dt*W[w].Fpp[i][3]);
      CaseY(W[w].rreal[i][4] += m_dn_inv*dt*W[w].Fpp[i][4]);
      CaseZ(W[w].rreal[i][5] += m_dn_inv*dt*W[w].Fpp[i][5]);
    }
  }
}

/********************************** Drift Force ******************************/
// F = (\nabla \Psi) / \Psi
// F = (\nabla D) / D + sum_{k!=i} (\nabla_i f(r_ik)) / f(r_ik)
void DriftForce(DOUBLE **F, DOUBLE **R) {
  int i, j, k;
  DOUBLE dr[3], r, r2;
  DOUBLE Force;
  DOUBLE x,y,z;
#ifdef ORBITAL_ANISOTROPIC_ABSOLUTE
  DOUBLE dFxup, dFyup, dFzup, dFxdn, dFydn, dFzdn, dFupup, dFdndn;
#endif
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
  DOUBLE dFx, dFy, dFz;
#endif
#ifdef ORBITAL_POLARIZED_DESCRETE_BCS
#ifndef ORBITAL_ANISOTROPIC_RELATIVE
  DOUBLE dFx, dFy, dFz;
#endif
#endif
#ifdef JASTROW_SAME_SPIN
  DOUBLE dF;
#endif
#ifdef CRYSTAL
  DOUBLE M0, M1[3], e;
#endif
  int alpha;
  DOUBLE Orb[3];

#ifdef FERMIONS
  for(i=0; i<Nmax; i++) for(j=0; j<Nmax; j++) DeterminantMove[i][j] = 0.;

  // Fill determinant
  for(i=0; i<Nup; i++) { // up
    x = R[i][0];
    y = R[i][1];
    z = R[i][2];
    for(j=0; j<Nup; j++) { // down
#ifdef ORBITAL_ANISOTROPIC_ABSOLUTE
       DeterminantMove[i][j] += orbitalFewBody_F(x,y,z,R[i][3],R[i][4],R[i][5],i,j);
#endif
      if(j<Ndn) { // unpolarized
        dr[0] = x - R[j][3];
        dr[1] = y - R[j][4];
        dr[2] = z - R[j][5];
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef ORBITAL_ISOTROPIC
       DeterminantMove[i][j] += orbitalF(sqrt(r2));
#endif
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
        DeterminantMove[i][j] += orbitalDescreteBCS_F(dr[0], dr[1], dr[2]);
#endif
      }
      else { // polarized
#ifdef ORBITAL_POLARIZED_DESCRETE_BCS
        DeterminantMove[i][j] += orbitalDescreteBCS_F(dr[0], dr[1], dr[2]);
#endif
#ifdef ORBITAL_POLARIZED_ALPHA
        alpha = Nup - 1 - j;
        DeterminantMove[i][j] = orbital(R[i][0], orbital_numbers_unpaired[alpha][0])*orbital(R[i][1], orbital_numbers_unpaired[alpha][1])*orbital(R[i][2], orbital_numbers_unpaired[alpha][2]);
#endif
      }
    }
  }
  LUinv(DeterminantMove, DeterminantMoveInv, Nup);
#endif // end FERMIONS

  // set Force array elements to zero
  for(i=0; i<Nmax; i++) for(k=0; k<6; k++) F[i][k] = 0.;

#ifdef ONE_BODY_TRIAL_TERMS // external field force
  for(i=0; i<Nup; i++) OneBodyFp(&F[i][0], &F[i][1], &F[i][2], R[i][0], R[i][1], R[i][2], i, ON);
  for(i=0; i<Ndn; i++) OneBodyFp(&F[i][3], &F[i][4], &F[i][5], R[i][3], R[i][4], R[i][5], i, OFF);
#endif

  for(i=0; i<Nup; i++) {
#ifdef CRYSTAL
#ifdef CRYSTAL_SYMM_SUM_PARTICLE
    M1[0] = M1[1] = M1[2] = 0.; // spin up
    for(j=0; j<Crystal.size; j++) { // fill Mo array
      MoUp[j] = 0.;
      for(k=0; k<Nup; k++) {
        dr[0] = R[k][0] - Crystal.x[j];
        dr[1] = R[k][1] - Crystal.y[j];
        dr[2] = R[k][2] - Crystal.z[j];
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
        //if(CheckInteractionCondition(dr[2], r2)) {
          MoUp[j] += Exp(-Crystal.R*r2);
        //}
      }
    }
    for(j=0; j<Crystal.size; j++) { // calculate the force
      dr[0] = R[i][0] - Crystal.x[j];
      dr[1] = R[i][1] - Crystal.y[j];
      dr[2] = R[i][2] - Crystal.z[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      //if(CheckInteractionCondition(dr[2], r2)) {
        e = Exp(-Crystal.R*r2) / MoUp[j];
        for(k=0; k<3; k++) M1[k] += dr[k]*e;
      //}
    }
    for(k=0; k<3; k++) M1[k] *= -2*Crystal.R;
    for(k=0; k<3; k++) F[i][k] += M1[k];

    M1[0] = M1[1] = M1[2] = 0.; // spin down
    for(j=0; j<Crystal.size; j++) { // fill Mo array
      MoDn[j] = 0.;
      for(k=0; k<Nup; k++) {
        dr[0] = R[k][3] - Crystal.x[j];
        dr[1] = R[k][4] - Crystal.y[j];
        dr[2] = R[k][5] - Crystal.z[j];
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
        //if(CheckInteractionCondition(dr[2], r2)) {
          MoDn[j] += Exp(-Crystal.R*r2);
        //}
      }
    }
    for(j=0; j<Crystal.size; j++) { // calculate the force
      dr[0] = R[i][3] - Crystal.x[j];
      dr[1] = R[i][4] - Crystal.y[j];
      dr[2] = R[i][5] - Crystal.z[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      //if(CheckInteractionCondition(dr[2], r2)) {
        e = Exp(-Crystal.R*r2) / MoDn[j];
        for(k=0; k<3; k++) M1[k] += dr[k]*e;
      //}
    }
    for(k=0; k<3; k++) M1[k] *= -2*Crystal.R;
    for(k=0; k<3; k++) F[i][k+3] += M1[k];
#endif
#endif

    x = R[i][0];
    y = R[i][1];
    z = R[i][2];
    // particle-particle force
    for(j=0; j<Nup; j++) {
#ifdef ORBITAL_ANISOTROPIC_ABSOLUTE
      orbitalFewBody_Fp(&dFxup, &dFyup, &dFzup, &dFxdn, &dFydn, &dFzdn, x, y, z, R[i][3], R[i][4], R[i][5], i, j);
      F[i][0] += DeterminantMoveInv[i][j] * dFxup;
      F[i][1] += DeterminantMoveInv[i][j] * dFyup;
      F[i][2] += DeterminantMoveInv[i][j] * dFzup;
      F[j][3] -= DeterminantMoveInv[i][j] * dFxdn;
      F[j][4] -= DeterminantMoveInv[i][j] * dFydn;
      F[j][5] -= DeterminantMoveInv[i][j] * dFzdn;
#endif
      if(j<Ndn) { // unpolarized
        dr[0] = x - R[j][3];
        dr[1] = y - R[j][4];
        dr[2] = z - R[j][5];
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
        if(CheckInteractionConditionOrbital(dr[2], r2)) {
          r = sqrt(r2);
#ifdef FERMIONS
#ifdef ORBITAL_ISOTROPIC
          Force = DeterminantMoveInv[i][j] * orbitalFp(r)/r;
          F[i][0] += Force * dr[0];
          F[i][1] += Force * dr[1];
          F[i][2] += Force * dr[2];
          F[j][3] -= Force * dr[0];
          F[j][4] -= Force * dr[1];
          F[j][5] -= Force * dr[2];
#endif
#endif
#ifdef JASTROW_OPPOSITE_SPIN // Jastrow MIXED terms
          Force = InterpolateFp12(&G12, r)/r;
          CaseX(F[i][0] += Force * dr[0]);
          CaseY(F[i][1] += Force * dr[1]);
          CaseZ(F[i][2] += Force * dr[2]);
          CaseX(F[j][3] -= Force * dr[0]);
          CaseY(F[j][4] -= Force * dr[1]);
          CaseZ(F[j][5] -= Force * dr[2]);
#endif
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
          orbitalDescreteBCS_Fp(&dFx, &dFy, &dFz, dr[0], dr[1], dr[2]);
          F[i][0] += DeterminantMoveInv[i][j] * dFx;
          F[i][1] += DeterminantMoveInv[i][j] * dFy;
          F[i][2] += DeterminantMoveInv[i][j] * dFz;
          F[j][3] -= DeterminantMoveInv[i][j] * dFx;
          F[j][4] -= DeterminantMoveInv[i][j] * dFy;
          F[j][5] -= DeterminantMoveInv[i][j] * dFz;
#endif
        }
      }
      else { // polarized
#ifdef ORBITAL_POLARIZED_DESCRETE_BCS
        orbitalDescreteBCS_Fp(&dFx, &dFy, &dFz, dr[0], dr[1], dr[2]);
        F[i][0] += DeterminantMoveInv[i][j] * dFx;
        F[i][1] += DeterminantMoveInv[i][j] * dFy;
        F[i][2] += DeterminantMoveInv[i][j] * dFz;
        F[j][3] -= DeterminantMoveInv[i][j] * dFx;
        F[j][4] -= DeterminantMoveInv[i][j] * dFy;
        F[j][5] -= DeterminantMoveInv[i][j] * dFz;
#endif
#ifdef ORBITAL_POLARIZED_ALPHA
        alpha = Nup - 1 - j;
        Orb[0] = orbital(R[i][0], orbital_numbers_unpaired[alpha][0]);
        Orb[1] = orbital(R[i][1], orbital_numbers_unpaired[alpha][1]);
        Orb[2] = orbital(R[i][2], orbital_numbers_unpaired[alpha][2]);
        // first derivative
        F[i][0] += DeterminantMoveInv[i][j] * orbital_dz(R[i][0], orbital_numbers_unpaired[alpha][0]) * Orb[1] * Orb[2];
        F[i][1] += DeterminantMoveInv[i][j] * Orb[0] * orbital_dz(R[i][1], orbital_numbers_unpaired[alpha][1]) * Orb[2];
        F[i][2] += DeterminantMoveInv[i][j] * Orb[0] * Orb[1] * orbital_dz(R[i][2], orbital_numbers_unpaired[alpha][2]);
#endif
      }
    }
  }

#ifdef JASTROW_SAME_SPIN // Jastrow contribution (same spin particle-particle force)
  for(i=0; i<Nup; i++) {
    for(j=i+1; j<Nup; j++) { 
      dr[0] = R[i][0] - R[j][0]; // spin up
      dr[1] = R[i][1] - R[j][1];
      dr[2] = R[i][2] - R[j][2];

      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);

      if(CheckInteractionCondition(dr[2], r2)) {
        r = sqrt(r2);
        Force = InterpolateFp11(&G11, r) / r;
        for(k=0; k<3; k++) {
          dF = Force * dr[k];
          F[i][k] += dF;
          F[j][k] -= dF;
        }
      }
    }
  }

  for(i=0; i<Ndn; i++) {
    for(j=i+1; j<Ndn; j++) {
      dr[0] = R[i][3] - R[j][3]; // spin down
      dr[1] = R[i][4] - R[j][4];
      dr[2] = R[i][5] - R[j][5];

      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);

      if(CheckInteractionCondition(dr[2], r2)) {
        r = sqrt(r2);
        Force = InterpolateFp22(&G22, r) / r;
        for(k=0; k<3; k++) {
          dF = Force * dr[k];
          F[i][k+3] += dF;
          F[j][k+3] -= dF;
        }
      }
    }
  }
#endif

#ifdef SYM_OPPOSITE_SPIN
  DOUBLE p1; // product_{i=1}^{Nup} sum_{j=1}^{Ndn}f(r_ij)
  DOUBLE p2; // product_{j=1}^{Ndn} sum_{i=1}^{Nup}f(r_ij)
  DOUBLE s;
  DOUBLE si;
  DOUBLE sj;
  int l;
  int m;

  //Calculate p1 =  product_{i=1}^{Nup} sum_{j=1}^{Ndn}f(r_ij)
  p1 = 1.;
  for(i = 0; i < Nup; i++) {
    s = 0.;
    CaseX(x = R[i][0]);
    CaseY(y = R[i][1]);
    CaseZ(z = R[i][2]);

    for(j = 0; j < Ndn; j++) {
      CaseX(dr[0] = x - R[j][3]);
      CaseY(dr[1] = y - R[j][4]);
      CaseZ(dr[2] = z - R[j][5]);
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(CheckInteractionConditionOrbital(dr[2], r2)) {
        r = sqrt(r2);
        s += InterpolateSYMF(&GSYM, r);
      }
    }
    p1 *= s;
  }

  //Calculate p2 = product_{j=1}^{Ndn} sum_{i=1}^{Nup}f(r_ij)
  p2 = 1.;
  for(j = 0; j < Ndn; j++) {
    s = 0.;
    CaseX(x = R[j][3]);
    CaseY(y = R[j][4]);
    CaseZ(z = R[j][5]);

    for(i = 0; i < Nup; i++) {
      CaseX(dr[0] = R[i][0] - x);
      CaseY(dr[1] = R[i][1] - y);
      CaseZ(dr[2] = R[i][2] - z);
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if (CheckInteractionConditionOrbital(dr[2], r2)) {
        r = sqrt(r2);
        s += InterpolateSYMF(&GSYM, r);
      }
    }
    p2 *= s;
  }

  //Calculate p1 (A_i) / (p1 + p2)
  // A_i=/S_i
  // S_i = 
  for(i = 0; i < Nup; i++) {
    CaseX(x = R[i][0]);
    CaseY(y = R[i][1]);
    CaseZ(z = R[i][2]);
    si = 0.;
    for (m = 0; m < Ndn; m++) { // Calculate S_i
      CaseX(dr[0] = x - R[m][3]);
      CaseY(dr[1] = y - R[m][4]);
      CaseZ(dr[2] = z - R[m][5]);
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(CheckInteractionConditionOrbital(dr[2], r2)) {
        r = sqrt(r2);
        si += InterpolateSYMF(&GSYM, r);
      }
    }

    for(j = 0; j < Ndn; j++) { // Calculate Fix Fiy Fiz
      CaseX(dr[0] = x - R[j][3]);
      CaseY(dr[1] = y - R[j][4]);
      CaseZ(dr[2] = z - R[j][5]);
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if (CheckInteractionConditionOrbital(dr[2], r2)) {
        r = sqrt(r2);
        Force = InterpolateSYMFp(&GSYM, r) / r;
        CaseX(F[i][0] += (Force * dr[0] / si) * (p1 / (p1 + p2)));
        CaseY(F[i][1] += (Force * dr[1] / si) * (p1 / (p1 + p2)));
        CaseZ(F[i][2] += (Force * dr[2] / si) * (p1 / (p1 + p2)));
        CaseX(F[j][3] -= (Force * dr[0] / si) * (p1 / (p1 + p2)));
        CaseY(F[j][4] -= (Force * dr[1] / si) * (p1 / (p1 + p2)));
        CaseZ(F[j][5] -= (Force * dr[2] / si) * (p1 / (p1 + p2)));
      }
    }

    // Calculate    p2 (B_i) / (p1 + p2)
    // B_i = /S_b
    // S_j = 
    for(j = 0; j < Ndn; j++) { // Calculate Fix Fiy Fiz
      sj = 0.;
      for(l = 0; l < Nup; l++) { // Calculate S_j
        CaseX(dr[0] = R[l][0] - R[j][3]);
        CaseY(dr[1] = R[l][1] - R[j][4]);
        CaseZ(dr[2] = R[l][2] - R[j][5]);
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
        if(CheckInteractionConditionOrbital(dr[2], r2)) {
          r = sqrt(r2);
          sj += InterpolateSYMF(&GSYM, r);
        }
      }

      CaseX(dr[0] = x - R[j][3]);
      CaseY(dr[1] = y - R[j][4]);
      CaseZ(dr[2] = z - R[j][5]);
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(CheckInteractionConditionOrbital(dr[2], r2)) {
        r = sqrt(r2);
        Force = InterpolateSYMFp(&GSYM, r) / r;
        CaseX(F[i][0] += (Force * dr[0] / sj) * (p2 / (p1 + p2)));
        CaseY(F[i][1] += (Force * dr[1] / sj) * (p2 / (p1 + p2)));
        CaseZ(F[i][2] += (Force * dr[2] / sj) * (p2 / (p1 + p2)));
        CaseX(F[j][3] -= (Force * dr[0] / sj) * (p2 / (p1 + p2)));
        CaseY(F[j][4] -= (Force * dr[1] / sj) * (p2 / (p1 + p2)));
        CaseZ(F[j][5] -= (Force * dr[2] / sj) * (p2 / (p1 + p2)));
      }
    }
  }
#endif
}

/******************************* Branching Walker ****************************/
int BranchingWalker(int w) {
  int multiplicity;
  int i, j, wp;
  DOUBLE dE, weight;

#ifdef SECURE
  if(W[w].status != ALIVE) Error("Trying to  replicate a walker which is not alive");
#endif

  //if(SmartMC == DMC_LINEAR_GAUSSIAN)
  //  dE = Eo - W[w].Eold;
  //else

#ifdef BRANCHING_BOUNDARY_CONDITION_WITH_SOURCE
  Eo = 0.; // looking for scattering solution with zero energy
#endif

  dE = Eo - 0.5*(W[w].E + W[w].Eold);

  weight = Exp(dt*dE);
  /*if(Nwalkers < Npop_min)
    weight *= amplify;
  else if(Nwalkers > Npop_max)
    weight *= reduce;*/

   multiplicity = (int) (weight + Random());

   if(!isfinite(W[w].E)) {
     multiplicity = 0;
     Warning("Walker %i has an abnormal energy (%g) and will be killed\n", w, W[w].E);
   }

#ifndef BRANCHING_BOUNDARY_CONDITION_WITH_SOURCE //  in the standard case control the population
  if(Nwalkers > Npop_max)
    multiplicity = (int) ((DOUBLE) multiplicity*reduce + Random());
  else if(Nwalkers < Npop_min)
    multiplicity = (int) ((DOUBLE) multiplicity*amplify + Random());
#endif

  if(multiplicity == 0) {
    W[w].status = KILLED;
    W[w].weight = weight;
  }
  else {
    W[w].weight = weight;// / (DOUBLE) multiplicity;
    for(i=1; i<multiplicity; i++) { // make replicae
      if(Ndead_walkers < 1) {
        Warning("walker %i was killed because it had too many sons (%i)\n", w, multiplicity);
        Message("weight %lf Nwalkers %i, Eo=%lf, E=%lf, Eold=%lf\n", weight, Nwalkers, Eo, W[w].E, W[w].Eold);
        Message("killed walker coordinates:\n");
        for(j=0; j<Nup; j++) Message("%lf %lf %lf\n", W[w].x[j], W[w].y[j], W[w].z[j]);
        for(j=0; j<Ndn; j++) Message("%lf %lf %lf\n", W[w].xdn[j], W[w].ydn[j], W[w].zdn[j]);
        multiplicity = 0;
        W[w].status = DEAD;
        W[w].weight = 0;
      }
      else {
        wp = dead_walkers_storage[Ndead_walkers-1];
#ifdef SECURE
        if(W[wp].status == ALIVE || W[wp].status == REINCARNATION) Error("Branching, walker %i, status %i", wp, W[wp].status);
#endif
        CopyWalker(&W[wp], W[w]);
        W[wp].status = REINCARNATION;
        W[wp].weight = 0.;
        Ndead_walkers--;
      }
    }
  }
  return multiplicity;
}

/********************************** Branching ********************************/
void Branching(void) {
  int w;

  if(SmartMC == DMC_PSEUDOPOTENTIAL) {
    BranchingPseudopotential();
    return;
  }
  else if(SmartMC == DMC_PSEUDOPOTENTIAL_REWEIGHTING) {
    BranchingPseudopotentialReweighting();
    return;
  }

  //amplify = (DOUBLE) Npop_max / (DOUBLE) Nwalkers;
  //reduce  = (DOUBLE) Npop_min / (DOUBLE) Nwalkers;

  if(branchng_present)
    for(w=0; w<Nwalkers; w++)
      if(W[w].status == ALIVE)
        BranchingWalker(w);

  Eo = EnergyO();

  // Measure pure estimators
  if(block>= 0 && iteration_global % Nmeasure == 0) {
    if(measure_R2) SaveMeanR2DMC();
#ifdef TRIAL_1D
    if(measure_TBDM) MeasureTBDM1D();
    if(measure_OBDM) MeasureOBDM1D();
#else
    if(measure_TBDM) MeasureTBDM();
    if(measure_OBDM) MeasureOBDM();
#endif
  }

  Nwalkers = 0;
  Nwalkersw = 0;
  for(w=0; w<NwalkersMax; w++) {
    if(W[w].status == REINCARNATION)
      W[w].status = ALIVE;
    else if(W[w].status == KILLED) {
      W[w].status = DEAD;
      W[w].weight = 0.;
    }
  }

  for(w=0; w<NwalkersMax; w++) {
    if(W[w].status == ALIVE) {
      if(w != Nwalkers) {
        CopyWalker(&W[Nwalkers], W[w]);
        W[w].status = DEAD;
        W[w].weight = 0;
      }
      Nwalkers++;
      Nwalkersw += W[w].weight;
    }
  }

#ifdef BRANCHING_BOUNDARY_CONDITION_WITH_SOURCE // pour walkers from the source
  for(w=Nwalkers-1; w<Npop; w++) {
    W[w].x[0] = 0.5*PD.max;
    W[w].xdn[0] = -0.5*PD.max;
    W[w].y[0] = W[w].y[1] = W[w].z[0] = W[w].zdn[0] = 0;
    W[w].status = ALIVE;
  }
  Nwalkers = Npop;
#endif

  if(Nwalkers == 0) {
    Error("All walkers have died");
  }

  Ndead_walkers = 0;
  for(w=NwalkersMax-1; w>=Nwalkers; w--) {
    dead_walkers_storage[Ndead_walkers++] = w;
#ifdef SECURE
    if(W[w].status != DEAD) Error("Branching (2)");
#endif
  }
}

/********************************* Copy To Walker ****************************/
void CopyVectorToWalker(struct Walker* W, DOUBLE** R) {
  int i;

  for(i=0; i<Nup; i++) {
    CaseX(W->x[i] = R[i][0]);
    CaseY(W->y[i] = R[i][1]);
    CaseZ(W->z[i] = R[i][2]);
  }
  for(i=0; i<Ndn; i++) {
    CaseX(W->xdn[i] = R[i][3]);
    CaseY(W->ydn[i] = R[i][4]);
    CaseZ(W->zdn[i] = R[i][5]);
  }
  //W->weight = 1.;
}

/********************************* Copy To Vector ****************************/
void CopyWalkerToVector(DOUBLE** R, struct Walker W) {
  int i;

  for(i=0; i<Nup; i++) {
    CaseX(R[i][0] = W.x[i]);
    CaseY(R[i][1] = W.y[i]);
    CaseZ(R[i][2] = W.z[i]);
  }
  for(i=0; i<Ndn; i++) {
    CaseX(R[i][3] = W.xdn[i]);
    CaseY(R[i][4] = W.ydn[i]);
    CaseZ(R[i][5] = W.zdn[i]);
  }
}

/********************************* Copy Vector *******************************/
void CopyVector(DOUBLE** out, DOUBLE** in) {
  int i;

  for(i=0; i<Nup; i++) {
    CaseX(out[i][0] = in[i][0]);
    CaseY(out[i][1] = in[i][1]);
    CaseZ(out[i][2] = in[i][2]);
  }
  for(i=0; i<Ndn; i++) {
    CaseX(out[i][3] = in[i][3]);
    CaseY(out[i][4] = in[i][4]);
    CaseZ(out[i][5] = in[i][5]);
  }
}

/********************************** Energy O *********************************/
DOUBLE EnergyO(void) {
  int w;
  DOUBLE E;
  int Nwalkers;

  E = 0.;
  EFF = 0.;
  Eint = 0;
  Epot = 0.;
  Ekin = 0.;
  Eext = 0.;
  Nwalkersw = 0.;
  Nwalkers = 0;
  for(w=0; w<NwalkersMax; w++) {
    if(W[w].status == ALIVE || W[w].status == REINCARNATION) {
      Eint += W[w].E;
      Nwalkers++;
    }
    if(W[w].status == ALIVE || W[w].status == KILLED) {
      W[w].weight = 1.;//!!!
      E += W[w].weight*W[w].E;
      EFF += W[w].weight*W[w].EFF;
      Epot += W[w].weight*W[w].Epot;
      Ekin += W[w].weight*W[w].Ekin;
      Eext += W[w].weight*W[w].Eext;
      Nwalkersw += W[w].weight;
    }
  }
  E /= Nwalkersw;
  EFF /= Nwalkers;
  Eint /= (DOUBLE)(Nwalkers)*Nmean;
  Epot /= (DOUBLE)(Nwalkers)*Nmean;
  Ekin /= (DOUBLE)(Nwalkers)*Nmean;
  Eext /= (DOUBLE)(Nwalkers)*Nmean;

  return E;
}
