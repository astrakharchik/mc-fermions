/*fewbody.h*/

#ifndef _FEWBODY_H_
#define _FEWBODY_H_

#include "main.h"

void DMCPseudopotentialMove(int w);
void BranchingPseudopotential(void);
DOUBLE GreensFunction(DOUBLE **Ro, DOUBLE **R, DOUBLE tau, DOUBLE m_up, DOUBLE m_dn, DOUBLE omega);
void InitializePermutationMatrix(void);
int Factorial(int i);
int Permutations(int Min, int Max, int p, int i);
int Signature(int Min, int Max, int p);
int PermHeavyLight(int p,int i);
int SignatureHeavyLight(int p);
DOUBLE Antider(DOUBLE y, DOUBLE y0, DOUBLE a, DOUBLE mu, DOUBLE tau);
DOUBLE RelWeight(DOUBLE y0, DOUBLE a, DOUBLE mu, DOUBLE tau);
DOUBLE InverseFunction(DOUBLE xi, DOUBLE y0, DOUBLE a, DOUBLE mu, DOUBLE tau);

void DMCPseudopotentialReweightingMove(int w);
void BranchingPseudopotentialReweighting(void);
int BranchingWalkerPseudopotentialReweighting(int w);


#endif

