/*mymath.h*/

#ifndef _MYMATH_H_
#define _MYMATH_H_

#include "main.h"

DOUBLE K0opt(DOUBLE r);
DOUBLE K1opt(DOUBLE r);
DOUBLE K2opt(DOUBLE r);
#define K0(r) K0opt(4./((r)*(r)))
#define K1(r) K1opt(4./((r)*(r)))
#define K2(r) K2opt(4./((r)*(r)))

DOUBLE BesselJ0(DOUBLE x);
DOUBLE BesselJ1(DOUBLE x);
DOUBLE BesselI0(DOUBLE x);
DOUBLE BesselI1(DOUBLE x);
DOUBLE BesselIN(int N, DOUBLE x);
DOUBLE BesselK0(DOUBLE x);
DOUBLE BesselK1(DOUBLE x);
DOUBLE BesselK0Log(DOUBLE x);
DOUBLE BesselY0(DOUBLE x);
DOUBLE BesselY1(DOUBLE x);

DOUBLE Erf(DOUBLE x);
DOUBLE Erfc(DOUBLE x);
DOUBLE Erfcx(DOUBLE x);

#ifndef DBL_EPSILON
#  define DBL_EPSILON 2.2204460492503131e-16
#endif

//MSVC++ 14.0 _MSC_VER == 1900 (Visual Studio 2015)

#if _MSC_VER == 1310 //(Visual Studio 2003)
# define erf_not_defined
#endif

#if _MSC_VER == 1800 // (Visual Studio 2013)
# define erf_not_defined
#endif

#if _MSC_VER == 1700 // (Visual Studio 2012)
# define erf_not_defined
#endif

#if _MSC_VER == 1600 // (Visual Studio 2010)
# define erf_not_defined
#endif

#if _MSC_VER == 1500 // (Visual Studio 2008)
# define erf_not_defined
#endif

#if _MSC_VER == 1400 // (Visual Studio 2005)
# define erf_not_defined
#endif

#ifdef erf_not_defined
  double erf(double);
  double erfc(double);
#endif

#endif
