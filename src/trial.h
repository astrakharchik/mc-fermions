/*trial.h*/

#ifndef __TRIAL_H_
#define __TRIAL_H_

#include "main.h"

struct Grid {
  long int size;    /* number of elements in the grid */
  DOUBLE step;
  DOUBLE I_step;
  DOUBLE min;
  DOUBLE max;
  DOUBLE max2;
  DOUBLE *x; 
  DOUBLE *fp;  /* f'/f  */
  DOUBLE *lnf; /* ln(f) */
  DOUBLE *E;   /* (f"+(D-1)f'/r)/f - (f'/f)^2*/
//#ifdef INTERPOLATE_SPLINE_WF
  DOUBLE *f;   // f
  DOUBLE *fpp; // f''
  DOUBLE k; // sqrt(|E|)
  DOUBLE Atrial, Btrial, Ctrial;
  DOUBLE Apar, Bpar, Rpar;
//#endif
};

void LoadTrialWaveFunction(struct Grid *G, char *file_wf, int normalize_to_one);
void SampleTrialWaveFunction11(struct Grid *G,  DOUBLE min, DOUBLE max, long int size, DOUBLE (*U)(DOUBLE x), DOUBLE (*Fp)(DOUBLE x), DOUBLE (*E)(DOUBLE x));
void SampleTrialWaveFunction12(struct Grid *G,  DOUBLE min, DOUBLE max, long int size, DOUBLE (*U)(DOUBLE x), DOUBLE (*Fp)(DOUBLE x), DOUBLE (*E)(DOUBLE x));
void SampleTrialWaveFunctionOrbital(struct Grid *G, DOUBLE min, DOUBLE max, long int size, DOUBLE (*U)(DOUBLE x), DOUBLE (*Fp)(DOUBLE x), DOUBLE (*E)(DOUBLE x));
void ConstructGridBCS(void);
void ConstructGridLim(struct Grid *G, const DOUBLE xmin, const DOUBLE xmax, const long int size);
void ConstructGridPower(struct Grid *G, const DOUBLE xmin, const DOUBLE xmax, const long int size);
void ConstructGridHS11(struct Grid *G);
void ConstructGridHS12(struct Grid *G);
void ConstructGridHS1D(struct Grid *G, const DOUBLE R, const DOUBLE xi, const DOUBLE D, const int size, const DOUBLE Rmin, const DOUBLE Rmax);
void ConstructGridTonks11(struct Grid *G, const int size, const DOUBLE Rmin, const DOUBLE Rmax);
void ConstructGridTonks12(struct Grid *G, const int size, const DOUBLE Rmin, const DOUBLE Rmax);
void ConstructGridTonks22(struct Grid *G, const int size, const DOUBLE Rmin, const DOUBLE Rmax);
void ConstructGridLieb(struct Grid *G, const DOUBLE R, const DOUBLE Bpar, const int size, const DOUBLE Rmin, const DOUBLE Rmax);
void ConstructGridSS(struct Grid *G, const DOUBLE a, const DOUBLE R, const DOUBLE Rpar, const DOUBLE Bpar, const int size, const DOUBLE Rmin, const DOUBLE Rmax);
void ConstructGridLiebPhonons11(struct Grid *G, const DOUBLE a, const DOUBLE Rpar);
void ConstructGridPhononLuttinger11(struct Grid *G);
void ConstructGridPhononLuttinger12(struct Grid *G);
void ConstructGridPhononLuttingerAuto11(struct Grid *G);
void ConstructGridPhononLuttingerAuto12(struct Grid *G);
void ConstructGridLiebPhonons12(struct Grid *G, const DOUBLE a, const DOUBLE Rpar);
void ConstructGridDipole(struct Grid *G, const DOUBLE Apar, const DOUBLE Bpar, const DOUBLE n);
void ConstructGridZeroRangeUnitaryPhonons(struct Grid *G);
void ConstructAbsent(struct Grid *G);
void ConstructGridSquareWellFreeState(struct Grid *G);
void ConstructGridSquareWellFreeState11(struct Grid *G);
void ConstructGridSquareWellFreeStatePWave11(struct Grid *G);
void ConstructGridSquareWellBoundState(struct Grid *G);
void ConstructGridSquareWellSmooth(struct Grid *G);
void ConstructGridSquareWellFreeStatePhonons11(struct Grid *G);
void ConstructGridSquareWellFreeStatePhonons12(struct Grid *G);
void ConstructGridSquareWellFreeStateGPEPolaron12(struct Grid *G);
void ConstructGridZeroRange_Log_GPE_Polaron(struct Grid *G);
void ConstructGridSh(struct Grid *G);
void ConstructGridHSSimple(struct Grid *G);
void ConstructGridHSSimple11(struct Grid *G);
void ConstructGridPseudopotentialBoundState(struct Grid *G);
void ConstructGridPseudopotentialFreeState(struct Grid *G, const DOUBLE a, const DOUBLE Rpar);
void ConstructGridBCSPseudopotential(void);
void ConstructGridBCSSquareWell(void);
void ConstructGridSoftSphereSquareWell(struct Grid *G);
void ConstructGridSquareWellZeroConst(void);
void ConstructGridSquareWellZeroConst11(void);
void ConstructGridSquareWell2D(void);
void ConstructGridSutherland(struct Grid *G);
void ConstructGridCoulomb1DPhonon(struct Grid *G);
void ConstructGridCoulomb1DPhononEx(struct Grid *G);
void ConstructGridCoulombPhonon(struct Grid *G);
void ConstructGridSquareWellBoundStatePhonons(struct Grid *G);
void ConstructGridSquareWellBoundStatePhonons11(struct Grid *G);
void ConstructGridCh2Phonon11(void);
void ConstructGridCh2Phonon12(void);
void ConstructGridSquareWellFreeStateGauss11(struct Grid *G);
void ConstructGridGauss1122(void);
void ConstructGridDipoleGauss(struct Grid *G);
void ConstructGridDipolePhonon(struct Grid *G);
void ConstructGridDipoleKo11(struct Grid *G);
void ConstructGridDipoleKo22(struct Grid *G);
void ConstructGridSquareWellUnitarity(void);
void ConstructGridZeroRange_K0_Phonons12(struct Grid *G);
void ConstructGridZeroRange_K0_GPE_Polaron(struct Grid *G);
void ConstructGridZeroRange_Unitary_GPE_Polaron(struct Grid *G);

void FindSWBindingEnergy(void);

void CheckTrialWaveFunction(void);

DOUBLE InterpolateExactU11(DOUBLE r);
DOUBLE InterpolateExactFp11(DOUBLE r);
DOUBLE InterpolateExactE11(DOUBLE r);
DOUBLE InterpolateExactU22(DOUBLE r);
DOUBLE InterpolateExactFp22(DOUBLE r);
DOUBLE InterpolateExactE22(DOUBLE r);
DOUBLE InterpolateExactU12(DOUBLE r);
DOUBLE InterpolateExactFp12(DOUBLE r);
DOUBLE InterpolateExactE12(DOUBLE r);
DOUBLE InterpolateExactSymF(DOUBLE r);
DOUBLE InterpolateExactSymFp(DOUBLE r);
DOUBLE InterpolateExactSymE(DOUBLE r);

DOUBLE InterpolateGridU(struct Grid *Grid, DOUBLE r);
DOUBLE InterpolateGridFp(struct Grid *Grid, DOUBLE r);
DOUBLE InterpolateGridE(struct Grid *Grid, DOUBLE r);

DOUBLE InterpolateEG11(DOUBLE r);
DOUBLE InterpolateEG22(DOUBLE r);
DOUBLE InterpolateEG12(DOUBLE r);
DOUBLE IntegrateSimpson(DOUBLE xmin, DOUBLE xmax, DOUBLE (*f)(DOUBLE x), long int Npoints, int dimensionality);

extern DOUBLE trial_Vo, trial11_Vo, trial22_Vo;
extern DOUBLE A1SW, A2SW, A3SW, A4SW, A5SW;
extern DOUBLE A1SD, A2SD, A3SD, A4SD, A5SD;
extern DOUBLE trial_Kappa, trial_Kappa2, trial_k, trial_k2;

void ConstructGridSchiffVerlet(struct Grid *G, const DOUBLE Apar, const DOUBLE Bpar, DOUBLE Rpar, const DOUBLE n);
void ConstructGridMcGuirePBC(void);
void ConstructGridMcGuirePBCRpar(void);
void ConstructGridMcGuirePhonon11(void);
void ConstructGridMcGuirePhonon12(void);
void ConstructGridMcGuireGaussian1D(void);
void ConstructGridMcGuireGaussian3D(void);
void ConstructGridMcGuireFermi(void);
void ConstructGridHS2DFiniteEnergy11(struct Grid *G);
void ConstructGridSquareWell2DZeroEnergyPhonons11(struct Grid *G);
void ConstructGridSquareWell2DZeroEnergyPhonons12(struct Grid *G);
void ConstructGridSoftDisks2DZeroEnergyPhonons11(struct Grid *G);
void ConstructGridSoftDisks2DZeroEnergyPhonons12(struct Grid *G);
void ConstructGridZeroRangeLogPhonons12(struct Grid *G);
void ConstructGridZeroRange_K0_Exp12(struct Grid *G);
void ConstructGridDipoleKoTrap(struct Grid *G);
void ConstructGridSchiffVerlett11_Trap(struct Grid *G);
void ConstructGridSquareWell2DZeroEnergyPhonons12(struct Grid *G);
void ConstructGridSYMGAUSS(struct Grid *G);
void ConstructGridSYMFERMI(struct Grid *G);
void ConstructGridSYMYAROSLAV(struct Grid *G);
void ConstructGridSUMEXP(struct Grid *G);
void ConstructGridSYMTWOBODY(struct Grid *G);
void ConstructGridSquareWell2D_K0_Phonons12(struct Grid *G);
void ConstructGridSquareWell2D_J0_Phonons12(struct Grid *G);
void ConstructGridSoftSpherePhonons11(struct Grid *G, DOUBLE Ro);
void ConstructGridSoftSpherePhonons12(struct Grid *G, DOUBLE Ro);
void ConstructGridSoftSpherePhonons22(struct Grid *G, DOUBLE Ro);
void ConstructGridDarkSoltion12(struct Grid *G);
void ConstructOneBodyGPHLY(void);
void ConstructGridSquareWellTrap(void);
void ConstructGridSquareWellBSTrap(void);
void ConstructGridSquareWellBSLatticePolaron(void);

#endif
