/*crystal.c*/

#include <math.h>
#include <stdio.h>
#include "main.h"
#include "crystal.h"
#include "utils.h"
#include "memory.h"
#include "move.h"
#include "trialf.h"

struct Cryst Crystal;

/************************** Construct Grid Crystal ***************************/
void ConstructGridCrystal(struct Cryst *Crystal, int size) {
  int i,j,k,index;
  int NCrystal;

#ifdef CRYSTAL_WIDTH_ARRAY
  Message("  Parameters Crystal->Rx[i], Ry[i], Rz[i] are independent and will be loaded from file\n");
  Crystal->Rx   = (DOUBLE*) Calloc("Crystal->Rx   ", size, sizeof(DOUBLE));
  Crystal->Ry   = (DOUBLE*) Calloc("Crystal->Ry   ", size, sizeof(DOUBLE));
  Crystal->Rz   = (DOUBLE*) Calloc("Crystal->Rz   ", size, sizeof(DOUBLE));
  generate_crystal_coordinates = OFF;
#endif

#ifdef BC_3DPBC_CUBE
#ifdef LATTICE_3D_CUBIC_MULTIPLE_OCCUPATION
  int multiplicity, m;

  //Nlattice = (int)(pow(Nup, 1./3.)+1e-12);
  Nlattice3 = Nlattice*Nlattice*Nlattice;
  multiplicity = Nup / Nlattice3;
  Message("  cubic lattice with %i x %i x %i lattice sites and multiplicity %i\n", Nlattice, Nlattice, Nlattice, multiplicity);
  if(Nlattice3 * multiplicity != Nup) Error("Cannot construct cubic lattice with %i lattice sites and multiplicity %i", Nup, multiplicity);
#else // usual case
  Nlattice = (int)(pow(Nup, 1./3.)+1e-12);
  Nlattice3 = Nlattice*Nlattice*Nlattice;
  if(Nlattice3 != Nup) Error("Cannot construct cubic lattice with %i lattice sites!", Nup);
#endif
  Message("  Size of the cubic lattice is %ix%ix%i\n", Nlattice, Nlattice, Nlattice);
#endif

  NCrystal = Nup;

#ifdef BC_2DPBC_SQUARE
  Nlattice = (pow(NCrystal, 1./2.)+1e-12);
  Nlattice3 = Nlattice*Nlattice;
  if(Nlattice3 != NCrystal) Error("Cannot construct square lattice with %i lattice sites!", NCrystal);
  Message("  Size of the square lattice is %ix%i\n", Nlattice, Nlattice);
#endif
#ifdef BC_1DPBC_Z
  if(NCrystal<0)
    Nlattice = Nup;
  else
    Nlattice = NCrystal;
  Message("  Size of the square lattice is %i\n", Nlattice);
#endif

  if(verbosity) Message("\nMemory allocation : allocating crystal grid of size %i...\n", size);

  Crystal->x = (DOUBLE*) Calloc("Crystal->x   ", size, sizeof(DOUBLE));
  Crystal->y = (DOUBLE*) Calloc("Crystal->y   ", size, sizeof(DOUBLE));
  Crystal->z = (DOUBLE*) Calloc("Crystal->z   ", size, sizeof(DOUBLE));
  Crystal->xdn = (DOUBLE*) Calloc("Crystal->x   ", size, sizeof(DOUBLE));
  Crystal->ydn = (DOUBLE*) Calloc("Crystal->y   ", size, sizeof(DOUBLE));
  Crystal->zdn = (DOUBLE*) Calloc("Crystal->z   ", size, sizeof(DOUBLE));
  Crystal->size = size;

#ifdef CRYSTAL
  //if(alpha == 0) Error("Incorrect variational parameter, should be alpha>0");

  //Message("  alpha is converted from units of lattice spacing %"LE" -> %"LE"\n", alpha_Rx, alpha_Rx/(L/(DOUBLE)Nlattice*L/(DOUBLE)Nlattice));
  //alpha_Rx /= (L/(DOUBLE)Nlattice*L/(DOUBLE)Nlattice);
#ifdef CRYSTAL_WIDTH_ARRAY
  Message("  One-body term of the crystal trial wavefunction: crystal width array\n");
  Crystal->Rx   = (DOUBLE*) Calloc("Crystal->Rx   ", size, sizeof(DOUBLE));
  Crystal->Ry   = (DOUBLE*) Calloc("Crystal->Ry   ", size, sizeof(DOUBLE));
  Crystal->Rz   = (DOUBLE*) Calloc("Crystal->Rz   ", size, sizeof(DOUBLE));
  Crystal->Rxdn = (DOUBLE*) Calloc("Crystal->Rxdn   ", size, sizeof(DOUBLE));
  Crystal->Rydn = (DOUBLE*) Calloc("Crystal->Rydn   ", size, sizeof(DOUBLE));
  Crystal->Rzdn = (DOUBLE*) Calloc("Crystal->Rzdn   ", size, sizeof(DOUBLE));
#else
  Crystal->R = alpha_Rx;
  //Crystal->Rx = alpha_Rx;
  //Crystal->Ry = alpha_Ry;
  //Crystal->Rz = alpha_Rz;
  Message("  One-body term of the crystal trial wavefunction:\n");
  //Message("    Crystal->Rx = %"LE"\n", Crystal->Rx);
  //Message("    Crystal->Ry = %"LE"\n", Crystal->Ry);
  //Message("    Crystal->Rz = %"LE"\n", Crystal->Rz);
  Message("    Crystal->R = %"LE"\n", Crystal->R);
#endif
#else
  Message("  One-body term is absent\n");
#endif

  index = 0;

#ifdef LATTICE_3D_CUBIC_TWICE // cubic lattice of Na Cl type
  for(i=0; i<Nlattice; i++) {
    for(j=0; j<Nlattice; j++) {
      for(k=0; k<Nlattice; k++) {
        Crystal->x[index] = (0.25+k)*L/(DOUBLE)Nlattice;
        Crystal->y[index] = (0.25+j)*L/(DOUBLE)Nlattice;
        Crystal->z[index] = (0.25+i)*L/(DOUBLE)Nlattice;
        Crystal->xdn[index] = (0.75+k)*L/(DOUBLE)Nlattice;
        Crystal->ydn[index] = (0.75+j)*L/(DOUBLE)Nlattice;
        Crystal->zdn[index] = (0.75+i)*L/(DOUBLE)Nlattice;
        index++;
      };
    };
  };
#endif
#ifdef LATTICE_3D_CUBIC_MULTIPLE_OCCUPATION
  for(m=0; m<multiplicity; m++) {
    for(i=0; i<Nlattice; i++) {
      for(j=0; j<Nlattice; j++) {
        for(k=0; k<Nlattice; k++) {
          Crystal->x[index] = (0.25+k)*L/(DOUBLE)Nlattice;
          Crystal->y[index] = (0.25+j)*L/(DOUBLE)Nlattice;
          Crystal->z[index] = (0.25+i)*L/(DOUBLE)Nlattice;
          Crystal->xdn[index] = (0.75+k)*L/(DOUBLE)Nlattice;
          Crystal->ydn[index] = (0.75+j)*L/(DOUBLE)Nlattice;
          Crystal->zdn[index] = (0.75+i)*L/(DOUBLE)Nlattice;
          index++;
        };
      };
    };
  }
#endif

#ifdef BC_1DPBC_Z // linear crystal
  for(i=0; i<size; i++) {
    Crystal->x[i] = 0.;
    Crystal->y[i] = 0.;
    Crystal->z[i] = L/size*(0.5+i);
  }
  for(i=0; i<size; i++) {
    Crystal->xdn[i] = 0.;
    Crystal->ydn[i] = 0.;
    Crystal->zdn[i] = L/size*(0.5+i);
  }
  index = size;
#endif

  if(verbosity) Message(" done\n");
}

