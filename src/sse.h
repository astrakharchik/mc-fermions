/*sse.h*/

#ifndef _SSE_H_
#define _SSE_H_

#ifdef SSE_OPT
# include <xmmintrin.h>
# include <mm_malloc.h>
#endif

float WalkerEnergySSE(struct Walker *Walker, float *V, float *Ekin, float *EFF, float *Eext);

#endif