/*DMC.h*/

#ifndef _DMC_H_
#define _DMC_H_

void DMCQuadraticMove(int w);
void DMCLinearMoveGaussian(int w);
void DMCLinearMoveMiddle(int w);
void DMCLinearMetropolisMove(int w);
void DMCLinearMetropolisMoveMiddle(int w);

void GaussianJump(int w, DOUBLE dt);
void GaussianJumpR(int w, DOUBLE dt);
void DriftJump(DOUBLE dt, int w);
void DriftJump1(DOUBLE dt, int w);
void DriftJump2(DOUBLE dt, int w);
void DriftForce(DOUBLE **F, DOUBLE **R);
void DriftForceWalker(DOUBLE **F, int w);
int  BranchingWalker(int w);
void Branching(void);

void CopyVectorToWalker(struct Walker* W, DOUBLE** R);
void CopyWalkerToVector(DOUBLE** R, struct Walker W);
void CopyVector(DOUBLE** out, DOUBLE** in);
DOUBLE EnergyO(void);
DOUBLE EnergyODMC(void);

void DMCQuadraticMoveReduceTimestep(int w, int reduce_times);

#endif


