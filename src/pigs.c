/*pigs.c*/
#include <stdio.h>
#include "pigs.h"
#include "main.h"
#include "move.h"
#include "quantities.h"
#include "randnorm.h"
#include "rw.h"
#include "compatab.h"
#include "memory.h"
#include "vmc.h"
#include "mymath.h"
#include <limits.h>
#include <memory.h>

struct Worm worm;

/***************************** PIGS Move All *********************************/
void PIGSMoveAll(void) {
// Moves a single particle in all walkers
  int iloop,i,w,up;
  DOUBLE xi;
  DOUBLE dx,dy,dz;
  int accept;
  DOUBLE du;
  DOUBLE sigma;

  DOUBLE Sold, Snew, dS;
  DOUBLE dSkin, Skinold, Skin;
  DOUBLE dSdelta, Sdeltaold, Sdelta;
  DOUBLE dSwf, Swfold, Swf;

  // Move particle i of the bid
#ifdef MOVE_ONLY_UP_PARTICLES
  for(iloop=0; iloop<Nup; iloop++) { // only particles up are moved
#else // move all particles
  for(iloop=0; iloop<Nup+Ndn; iloop++) { // move i-th particle to the point rm
#endif
    dSkin = dSdelta = dSwf = 0.;

    if(iloop<Nup) { // move particle up
      up = ON;
      i = iloop;
    }
    else { // move particle down
      up = OFF;
      i = iloop-Nup;
    }

    Sold = PIGSPseudopotentialS(&Skinold, &Sdeltaold, &Swfold); // test change

    sigma = sqrt(0.5*dt_all);
    RandomNormal3(&dx, &dy, &dz, 0., sigma);
  
    //if(CheckCoordinatesDimensionality()) Message("dimensionality check not passed");

    //  Move particle np of the walker w
    if(up) {
      for(w=0; w<Npop; w++) {
        CaseX(Wp[w].x[i] = W[w].x[i];) // store old coordinates
        CaseY(Wp[w].y[i] = W[w].y[i];)
        CaseZ(Wp[w].z[i] = W[w].z[i];)
        CaseX(W[w].x[i] += m_up_inv_sqrt*dx;) // move
        CaseY(W[w].y[i] += m_up_inv_sqrt*dy;)
        CaseZ(W[w].z[i] += m_up_inv_sqrt*dz;)
        ReduceToTheBoxXYZ(&W[w].x[i], &W[w].y[i], &W[w].z[i]);
      }
    }
    else {
      for(w=0; w<Npop; w++) {
        CaseX(Wp[w].xdn[i] = W[w].xdn[i];)
        CaseY(Wp[w].ydn[i] = W[w].ydn[i];)
        CaseZ(Wp[w].zdn[i] = W[w].zdn[i];)
        CaseX(W[w].xdn[i] += m_dn_inv_sqrt*dx;)
        CaseY(W[w].ydn[i] += m_dn_inv_sqrt*dy;)
        CaseZ(W[w].zdn[i] += m_dn_inv_sqrt*dz;)
        ReduceToTheBoxXYZ(&W[w].xdn[i], &W[w].ydn[i], &W[w].zdn[i]);
      }
    }

#ifdef CENTER_OF_MASS_IS_NOT_MOVED // propose such movement that CM position is not moved
    Error("  PIGS Move All does not support CENTER_OF_MASS_IS_NOT_MOVED\n");
#endif

    Snew = PIGSPseudopotentialS(&Skin, &Sdelta, &Swf);
    dS = Snew - Sold ; // test change
    dSkin = Skin - Skinold;
    dSdelta = Sdelta - Sdeltaold;
    dSwf = Swf - Swfold;

    if(Random() < Exp(dS)) { // The Metropolis code
      accepted_all++;
    }
    else {
      rejected_all++;
      if(up) {
        for(w=0; w<Npop; w++) {
          CaseX(W[w].x[i] = Wp[w].x[i];) // restore old coordinates
          CaseY(W[w].y[i] = Wp[w].y[i];)
          CaseZ(W[w].z[i] = Wp[w].z[i];)
        }
      }
      else {
        for(w=0; w<Npop; w++) {
          CaseX(W[w].xdn[i] = Wp[w].xdn[i];)
          CaseY(W[w].ydn[i] = Wp[w].ydn[i];)
          CaseZ(W[w].zdn[i] = Wp[w].zdn[i];)
        }
      }
    }
  }
}

/**************************** PIGS Move One By One *****************************/
void PIGSMoveOneByOne(int w) {
// kinetic_link_at_border = OFF - the probability distribution at the border equal to |\psi_T|^2
// kinetic_link_at_border = ON - there is a kinetic energy associated with the move of the particle at the border

  int kinetic_link_at_border = ON;
  int potential_link_at_border = OFF;
  int i,j,iloop;
  int w1,w2; // index of the walker with [w-1] and [w+1]
  DOUBLE dS;
  DOUBLE xi;
  DOUBLE dx,dy,dz;
  DOUBLE x,y,z;
  DOUBLE xp,yp,zp;
  DOUBLE xm,ym,zm; // position of the middle point
  DOUBLE dr[3], r2, r;
  DOUBLE tau;
  DOUBLE dEpot;
  CaseX(DOUBLE xold);
  CaseY(DOUBLE yold);
  CaseZ(DOUBLE zold);
  int chain_edge = OFF;
  int up;
  //DOUBLE sigma;

  x = y = z = 0;
  xm = ym = zm = 0;

  w1 = w-1;
  w2 = w+1;
  //if(w1<0) w1 = Nwalkers-1;
  //if(w2==Nwalkers) w2 = 0;
  if(w == 0) { // treat w=0 and w=Nwalkers-1 separately, according to the trial w.f.
    chain_edge = ON;
    w1 = w2;
  }
  if(w == Nwalkers-1) { // treat w=0 and w=Nwalkers-1 separately
    chain_edge = ON;
    w2 = w1;
  }

  tau = dt; // imaginary time step

  // Move particle i of the bid
  //for(i=0; i<Nup; i++) {
  for(iloop=0; iloop<Nup+Ndn; iloop++) { //  move i-th particle to the point rm
    dS = dEpot = 0.;

    if(iloop<Nup) { // move particle up
      up = ON;
      i = iloop;
    }
    else { // move particle down
      up = OFF;
      i = iloop-Nup;
    }

    if(up) {
      CaseX(x = W[w].x[i]);
      CaseY(y = W[w].y[i]);
      CaseZ(z = W[w].z[i]);
      CaseX(xm = 0.5*(W[w1].x[i] + W[w2].x[i])); // middle point
      CaseY(ym = 0.5*(W[w1].y[i] + W[w2].y[i]));
      CaseZ(zm = 0.5*(W[w1].z[i] + W[w2].z[i]));
    }
    else {
      CaseX(x = W[w].xdn[i]);
      CaseY(y = W[w].ydn[i]);
      CaseZ(z = W[w].zdn[i]);
      CaseX(xm = 0.5*(W[w1].xdn[i] + W[w2].xdn[i])); // middle point
      CaseY(ym = 0.5*(W[w1].ydn[i] + W[w2].ydn[i]));
      CaseZ(zm = 0.5*(W[w1].zdn[i] + W[w2].zdn[i]));
    }

    // Wp is the trial position
    RandomNormal3(&dx, &dy, &dz, 0., Sqrt(dt));
    CaseX(xp = x + dx);
    CaseY(yp = y + dy);
    CaseZ(zp = z + dz);
    ReduceToTheBoxXYZ(&xp, &yp, &zp);

    // new kinetic energy
    CaseX(dr[0] = xp - xm);
    CaseY(dr[1] = yp - ym);
    CaseZ(dr[2] = zp - zm);
    r2 = FindNearestImage(&dr[0], &dr[1], &dr[2]);
    dS -= r2/tau;

    // old kinetic energy
    CaseX(dr[0] = x - xm);
    CaseY(dr[1] = y - ym);
    CaseZ(dr[2] = z - zm);
    r2 = FindNearestImage(&dr[0], &dr[1], &dr[2]);
    dS += r2/tau;

    // weight is given by the trial w.f. + path integral propagator
    if(chain_edge) { // at the edge the weight is given by the trial w.f.
      if(kinetic_link_at_border)
        dS = 0.5*dS; // with kinetic link, kinetic energy was double counted
      else
        dS = 0.; // remove the kinetic link
#ifdef ONE_BODY_TRIAL_TERMS
      dS -= U(W[w]); // subtract the old weight p(R) = psi = Exp(U)
#endif
#ifdef EXTERNAL_POTENTIAL
      if(potential_link_at_border) dEpot -= VextUp(x, y, z, i); // old external potential energy
#endif

      if(up) { // multiply by the new weight p(R') = psi'^2 = Exp(2U')
        CaseX(xold = W[w].x[i]);
        CaseY(yold = W[w].y[i]);
        CaseZ(zold = W[w].z[i]);
        CaseX(W[w].x[i] = xp);
        CaseY(W[w].y[i] = yp);
        CaseZ(W[w].z[i] = zp);
      }
      else {
        CaseX(xold = W[w].xdn[i]);
        CaseY(yold = W[w].ydn[i]);
        CaseZ(zold = W[w].zdn[i]);
        CaseX(W[w].xdn[i] = xp);
        CaseY(W[w].ydn[i] = yp);
        CaseZ(W[w].zdn[i] = zp);
      }
#ifdef ONE_BODY_TRIAL_TERMS
      dS += U(W[w]); // add new weight
#endif
#ifdef EXTERNAL_POTENTIAL
      if(potential_link_at_border) dEpot += VextUp(xp, yp, zp, i); // external potential energy
#endif

      if(up) { // restore coordinates
        CaseX(W[w].x[i] = xold);
        CaseY(W[w].y[i] = yold);
        CaseZ(W[w].z[i] = zold);
      }
      else {
        CaseX(W[w].xdn[i] = xold);
        CaseY(W[w].ydn[i] = yold);
        CaseZ(W[w].zdn[i] = zold);
      }
    }
    else { // weight is given entirely by path integral propagator
      if(up) {
#ifdef EXTERNAL_POTENTIAL
        dEpot += VextUp(xp, yp, zp, i); // external potential energy
#endif
        for(j=0; j<Ndn; j++) { // interaction energy: AB
          CaseX(dr[0] = xp - W->xdn[j]);
          CaseY(dr[1] = yp - W->ydn[j]);
          CaseZ(dr[2] = zp - W->zdn[j]);
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef INTERACTION_SUM_OVER_IMAGES
          dEpot += InteractionEnergySumOverImages12(dr[0],dr[1],dr[2]);
#endif
#ifdef INTERACTION_WITHOUT_IMAGES
          if(CheckInteractionCondition(dr[2], r2)) {
            r = sqrt(r2);
            dEpot += InteractionEnergy12(r);
          }
#endif
        } // loop AB

        for(j=0; j<Nup; j++) { // AA
          if(j != i) {
            CaseX(dr[0] = xp - W->x[j]);
            CaseY(dr[1] = yp - W->y[j]);
            CaseZ(dr[2] = zp - W->z[j]);
            FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef INTERACTION_SUM_OVER_IMAGES
            dEpot += InteractionEnergySumOverImages11(dr[0],dr[1],dr[2]);
#endif
#ifdef INTERACTION_WITHOUT_IMAGES
            if(CheckInteractionCondition(dr[2], r2)) {
              r = sqrt(r2);
              dEpot += InteractionEnergy11(r);
            }
#endif
          }
        } // loop AA
#ifdef EXTERNAL_POTENTIAL
        dEpot -= VextUp(x, y, z, i); // old potential energy
#endif
        for(j=0; j<Ndn; j++) {
          CaseX(dr[0] = W->x[i] - W->xdn[j]);
          CaseY(dr[1] = W->y[i] - W->ydn[j]);
          CaseZ(dr[2] = W->z[i] - W->zdn[j]);
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef INTERACTION_SUM_OVER_IMAGES
          dEpot -= InteractionEnergySumOverImages12(dr[0],dr[1],dr[2]);
#endif
#ifdef INTERACTION_WITHOUT_IMAGES
          if(CheckInteractionCondition(dr[2], r2)) {
            r = sqrt(r2);
            dEpot -= InteractionEnergy12(r);
          }
#endif
        } // loop AB

        for(j=0; j<Nup; j++) { // AA
          if(j != i) {
            CaseX(dr[0] = W->x[i] - W->x[j]);
            CaseY(dr[1] = W->y[i] - W->y[j]);
            CaseZ(dr[2] = W->z[i] - W->z[j]);
            FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef INTERACTION_SUM_OVER_IMAGES
            dEpot -= InteractionEnergySumOverImages11(dr[0],dr[1],dr[2]);
#endif
#ifdef INTERACTION_WITHOUT_IMAGES
            if(CheckInteractionCondition(dr[2], r2)) {
              r = sqrt(r2);
              dEpot -= InteractionEnergy11(r);
            }
#endif
          }
        } // loop AA
      }
      else { //  Move down particle
#ifdef EXTERNAL_POTENTIAL
        dEpot += VextDn(xp, yp, zp, i);
#endif
        for(j=0; j<Nup; j++) { // AB
          CaseX(dr[0] = xp - W->x[j]);
          CaseY(dr[1] = yp - W->y[j]);
          CaseZ(dr[2] = zp - W->z[j]);
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef INTERACTION_SUM_OVER_IMAGES
          dEpot += InteractionEnergySumOverImages12(dr[0],dr[1],dr[2]);
#endif
#ifdef INTERACTION_WITHOUT_IMAGES
          if(CheckInteractionCondition(dr[2], r2)) {
            r = sqrt(r2);
            dEpot += InteractionEnergy12(r);
          }
#endif
        } // loop AB

        for(j=0; j<Ndn; j++) { // BB
          if(j != i) {
            CaseX(dr[0] = xp - W->xdn[j]);
            CaseY(dr[1] = yp - W->ydn[j]);
            CaseZ(dr[2] = zp - W->zdn[j]);
            FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef INTERACTION_SUM_OVER_IMAGES
            dEpot += InteractionEnergySumOverImages22(dr[0],dr[1],dr[2]);
#endif
#ifdef INTERACTION_WITHOUT_IMAGES
            if(CheckInteractionCondition(dr[2], r2)) {
              r = sqrt(r2);
              dEpot += InteractionEnergy22(r);
            }
#endif
          }
        } // loop BB
#ifdef EXTERNAL_POTENTIAL
        dEpot -= VextDn(x, y, z, i);
#endif
        for(j=0; j<Ndn; j++) { // AB
          CaseX(dr[0] = W->x[i] - W->x[j]);
          CaseY(dr[1] = W->y[i] - W->y[j]);
          CaseZ(dr[2] = W->z[i] - W->z[j]);
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef INTERACTION_SUM_OVER_IMAGES
          dEpot -= InteractionEnergySumOverImages12(dr[0],dr[1],dr[2]);
#endif
#ifdef INTERACTION_WITHOUT_IMAGES
          if(CheckInteractionCondition(dr[2], r2)) {
            r = sqrt(r2);
            dEpot -= InteractionEnergy12(r);
          }
#endif
        } // loop AB

        for(j=0; j<Nup; j++) { // BB
          if(j != i) {
            CaseX(dr[0] = W->xdn[i] - W->xdn[j]);
            CaseY(dr[1] = W->ydn[i] - W->ydn[j]);
            CaseZ(dr[2] = W->zdn[i] - W->zdn[j]);
            FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef INTERACTION_SUM_OVER_IMAGES
            dEpot -= InteractionEnergySumOverImages22(dr[0],dr[1],dr[2]);
#endif
#ifdef INTERACTION_WITHOUT_IMAGES
            if(CheckInteractionCondition(dr[2], r2)) {
              r = sqrt(r2);
              dEpot -= InteractionEnergy22(r);
            }
#endif
          }
        } // loop AA
      }
    }

    // potential energy contribution
    dS -= tau*dEpot;

    // The Metropolis code
    // f = Exp(u), p = Exp(up-u)
    if(dS > 0.) {
      if(up) {
        CaseX(W[w].x[i] = xp);
        CaseY(W[w].y[i] = yp);
        CaseZ(W[w].z[i] = zp);
      }
      else {
        CaseX(W[w].xdn[i] = xp);
        CaseY(W[w].ydn[i] = yp);
        CaseZ(W[w].zdn[i] = zp);
      }
      W[w].U -= dS;
      accepted++;
    }
    else {
      xi = Random();
      if(xi<Exp(dS)) {
        if(up) {
          CaseX(W[w].x[i] = xp);
          CaseY(W[w].y[i] = yp);
          CaseZ(W[w].z[i] = zp);
        }
        else {
          CaseX(W[w].xdn[i] = xp);
          CaseY(W[w].ydn[i] = yp);
          CaseZ(W[w].zdn[i] = zp);
        }
        W[w].U -= dS;
        accepted++;
      }
      else {
        rejected++;
      }
    }
  }
}

double PropagatorPseudopotenial1D_ij(double m, double a, double dt, double xi0, double xj0, double xi, double xj) {
// dt - propagation time
// xij0 = x_i(0) - x_j(0), no absolute value
// xij = x_i(dt) - x_j(dt), no absolute value
// m = 2\mu
  double safe;
  double x;
  double xij,xij0,yij;
  double absxij, absxij0;
  double yerfcx, yexp;
  int particles_exchange_sign;

  xij0 = xi0 - xj0;
  xij = xi - xj;

#ifdef BC_1DPBC
  if(xij0> Lhalf) xij0 -= L;
  if(xij > Lhalf) xij -= L;
  if(xij0 < -Lhalf) xij0 += L;
  if(xij < -Lhalf) xij += L;
#ifdef SECURE
  if(xij0<-Lhalf || xij0>Lhalf)
    Warning(" xij0= %lf out of range\n", xij0);
  if(xij<-Lhalf || xij>Lhalf)
    Warning(" xij= %lf out of range\n", xij);
#endif
#endif
  absxij0 = fabs(xij0);
  absxij = fabs(xij);

  if(xij*xij0<0) // there was an exchange between particles
    particles_exchange_sign = -1;
  else // there was no exchange of particles
    particles_exchange_sign = 1;

  //if(absxij>Apar || absxij0>Apar) particles_exchange_sign = 1;
#ifdef BC_1DPBC
  //if(absxij>Lhalf-5.*sqrt(dt) || absxij0>Lhalf-5.*sqrt(dt)) particles_exchange_sign = 1;
  if(absxij>0.75*Lhalf || absxij0>0.75*Lhalf) particles_exchange_sign = 1;
  //if(absxij>5.*sqrt(dt) || absxij0>5.*sqrt(dt)) particles_exchange_sign = 1.;
#endif

  if(a<1e-6 && a>-1.1e-3) { // unitary gas
    //if(particles_exchange_sign == -1) {
    //  safe = log(1. - exp(-m/dt*Apar*Apar));
   // }
   // else 
{
      x = exp(-0.5*m/dt*(1.+(DOUBLE)particles_exchange_sign)*fabs(xij*xij0));
      if(fabs(x) > 1e-10) {
        safe = log(1. - x);
      }
      else { // return log(1-x) expansion
        safe = x + x*x / 2. + x*x*x / 3. + x*x*x*x / 4. + x*x*x*x*x / 5.;
      }
    }
    return safe;
  }

  // erfcx
  yij = sqrt(0.25*m/dt) * (absxij + absxij0) - sqrt(dt/m)/a;
  yerfcx = Erfcx(yij);
  yexp = yerfcx*exp(-0.5*m/dt*(1.+particles_exchange_sign)*fabs(xij*xij0));
  x = yexp * sqrt(PI*dt/m)/a; // a<0

  if(fabs(x) > 1e-10) {
    safe = log(1. + x);
  }
  else { // return log(1+x) expansion
    safe = x - x*x / 2. + x*x*x / 3. - x*x*x*x / 4. + x*x*x*x*x / 5.;
  }

  return safe;
}

double PropagatorLaplacian(double m, double dt, double dr) {
// contribution from the Laplacian + (R(i)-R(i+/-1))^2 / 2

#ifdef BC_1DPBC
  dr = fabs(dr);
  if(dr>Lhalf) dr = L - dr;
#ifdef SECURE
  if(dr<0 || dr>Lhalf) Warning(" dr= %lf out of range\n", dr);
#endif
#endif

  return -0.5 * m * dr*dr / dt;
}

/*************************** PIGS pseudopotential *************************************/
void PIGSPseudopotentialMoveOneByOne(int w) {
  int i,j,iloop;
  int w1,w2; // index of the walker with [w-1] and [w+1]
  DOUBLE dS, dSkin, dSdelta, dSwf;
  DOUBLE dx,dy,dz;
  DOUBLE xp,yp,zp;
  DOUBLE xold, yold, zold;
  DOUBLE tau;
  int chain_edge = OFF;
  int up;
  DOUBLE x, y, z;
  DOUBLE xm, ym, zm; // position of the middle point
  DOUBLE mA, mB, mAB; // i.e. 2 mu
#ifdef EXTERNAL_POTENTIAL
  DOUBLE dEpot;
#endif

  x = y = z = 0.;
  xm = ym = zm = 0.;
  mA = mB = mAB = 1.;

#ifdef INFINITE_MASS_COMPONENT_DN // 2\mu  = m
  mAB = 2.;
#endif

  //tau = dt; // imaginary time step
  tau = beta;

  w1 = w-1;
  w2 = w+1;
  if(w == 0) { // treat w=0 and w=Nwalkers-1 separately, according to the trial w.f.
    chain_edge = ON;
    w1 = w2;
  }
  if(w == Nwalkers-1) { // treat w=0 and w=Nwalkers-1 separately
    chain_edge = ON;
    w2 = w1;
  }

  // Move particle i of the bid
#ifdef MOVE_ONLY_UP_PARTICLES
  for(iloop=0; iloop<Nup; iloop++) { // only particles up are moved
#else // move all particles
  for(iloop=0; iloop<Nup+Ndn; iloop++) { // move i-th particle to the point rm
#endif
    dSkin = dSdelta = dSwf = 0.;
#ifdef EXTERNAL_POTENTIAL
    dEpot = 0.;
#endif

    if(iloop<Nup) { // move particle up
      up = ON;
      i = iloop;
    }
    else { // move particle down
      up = OFF;
      i = iloop-Nup;
    }

    if(up)
      CaseZ(z = W[w].z[i]);
    else 
      CaseZ(z = W[w].zdn[i]);

    // Wp is the trial position
    RandomNormal3(&dx, &dy, &dz, 0., Sqrt(dt));
    //CaseZ(zp = z + dz);
    zp = z + dz;
#ifdef BC_1DPBC
    ReduceToTheBoxXYZ(&xp, &yp, &zp);
#endif
 
    // contribution from the Laplacian, - (R'(i)-R(i+/-1))^2 / 2 + (R(i)-R(i+/-1))^2 / 2
    if(up) {
#ifndef INFINITE_MASS_COMPONENT_UP // mA finite
      dSkin += PropagatorLaplacian(mA, tau, zp - W[w1].z[i]);
      dSkin += PropagatorLaplacian(mA, tau, zp - W[w2].z[i]);
      dSkin -= PropagatorLaplacian(mA, tau, z - W[w1].z[i]);
      dSkin -= PropagatorLaplacian(mA, tau, z - W[w2].z[i]);
#endif
#ifdef EXTERNAL_POTENTIAL
    dEpot += VextUp(xp, yp, zp, i); // new external potential energy
    dEpot -= VextUp(x,  y,  z,  i); // old external potential energy
#endif
    }
    else {
#ifndef INFINITE_MASS_COMPONENT_DN // mB finite
      dSkin += PropagatorLaplacian(mB, tau, zp - W[w1].zdn[i]);
      dSkin += PropagatorLaplacian(mB, tau, zp - W[w2].zdn[i]);
      dSkin -= PropagatorLaplacian(mB, tau, z - W[w1].zdn[i]);
      dSkin -= PropagatorLaplacian(mB, tau, z - W[w2].zdn[i]);
#endif
#ifdef EXTERNAL_POTENTIAL
    dEpot += VextDn(xp, yp, zp, i); // new external potential energy
    dEpot -= VextDn(x,  y,  z,  i); // old external potential energy
#endif
    }

    // contribution from Pseudopotential
    if(up) {
      for(j=0; j<Ndn; j++) { // interaction energy: AB
        dSdelta += PropagatorPseudopotenial1D_ij(mAB, a, tau, zp, W[w].zdn[j], W[w1].z[i], W[w1].zdn[j]);
        dSdelta += PropagatorPseudopotenial1D_ij(mAB, a, tau, zp, W[w].zdn[j], W[w2].z[i], W[w2].zdn[j]);
        dSdelta -= PropagatorPseudopotenial1D_ij(mAB, a, tau, z,  W[w].zdn[j], W[w1].z[i], W[w1].zdn[j]);
        dSdelta -= PropagatorPseudopotenial1D_ij(mAB, a, tau, z,  W[w].zdn[j], W[w2].z[i], W[w2].zdn[j]);
      }      
#ifndef INFINITE_MASS_COMPONENT_UP // mA finite
      for(j=0; j<Nup; j++) if(j != i) { // interaction energy: AA
        dSdelta += PropagatorPseudopotenial1D_ij(mA, aA, tau, zp, W[w].z[j], W[w1].z[i], W[w1].z[j]);
        dSdelta += PropagatorPseudopotenial1D_ij(mA, aA, tau, zp, W[w].z[j], W[w2].z[i], W[w2].z[j]);
        dSdelta -= PropagatorPseudopotenial1D_ij(mA, aA, tau, z,  W[w].z[j], W[w1].z[i], W[w1].z[j]);
        dSdelta -= PropagatorPseudopotenial1D_ij(mA, aA, tau, z,  W[w].z[j], W[w2].z[i], W[w2].z[j]);
      }
#endif
    }
    else { //  Move down particle
      for(j=0; j<Nup; j++) { // interaction energy: AB
        dSdelta += PropagatorPseudopotenial1D_ij(mAB, a, tau, W[w].z[j], zp, W[w1].z[j], W[w1].zdn[i]);
        dSdelta += PropagatorPseudopotenial1D_ij(mAB, a, tau, W[w].z[j], zp, W[w2].z[j], W[w2].zdn[i]);
        dSdelta -= PropagatorPseudopotenial1D_ij(mAB, a, tau, W[w].z[j], z,  W[w1].z[j], W[w1].zdn[i]);
        dSdelta -= PropagatorPseudopotenial1D_ij(mAB, a, tau, W[w].z[j], z,  W[w2].z[j], W[w2].zdn[i]);
      }
#ifndef INFINITE_MASS_COMPONENT_DN // mB finite
      for(j=0; j<Ndn; j++) if(j != i) { // interaction energy: BB
        dSdelta += PropagatorPseudopotenial1D_ij(mB, aB, tau, zp, W[w].zdn[j], W[w1].zdn[i], W[w1].zdn[j]);
        dSdelta += PropagatorPseudopotenial1D_ij(mB, aB, tau, zp, W[w].zdn[j], W[w2].zdn[i], W[w2].zdn[j]);
        dSdelta -= PropagatorPseudopotenial1D_ij(mB, aB, tau, z,  W[w].zdn[j], W[w1].zdn[i], W[w1].zdn[j]);
        dSdelta -= PropagatorPseudopotenial1D_ij(mB, aB, tau, z,  W[w].zdn[j], W[w2].zdn[i], W[w2].zdn[j]);
      }
#endif
    }

    // weight is given by the trial w.f. + path integral propagator
    if(chain_edge) { // at the edge the weight is given by the trial w.f.
      dSkin = 0.5*dSkin; // remove double counting due to w2 == w1
      dSdelta = 0.5*dSdelta; // remove double counting due to w2 == w1
      dSwf -= U(W[w]); // subtract the old weight p(R) = psi = Exp(U), trial wave function
      if(up) { // multiply by the new weight p(R') = psi'^2 = Exp(2U')
        CaseZ(zold = W[w].z[i]);
        CaseZ(W[w].z[i] = zp);
      }
      else {
        CaseZ(zold = W[w].zdn[i]);
        CaseZ(W[w].zdn[i] = zp);
      }
      dSwf += U(W[w]); // add new weight from the trial wave function, calculated with the new coordinates
      if(up) { // restore coordinates
        CaseZ(W[w].z[i] = zold);
      }
      else {
        CaseZ(W[w].zdn[i] = zold);
      }
    }

#ifdef SECURE
    if(isnan(dSdelta)) Warning("NaN weight in PIGS move!\n");
#endif

    dS = dSkin + dSdelta + dSwf; // dSdelta - contribution from delta function, dSwf from the trial w.f.
#ifdef EXTERNAL_POTENTIAL
    dS -= tau*dEpot; // potential energy contribution
#endif

    if(Random() < Exp(dS)) { // The Metropolis code
      if(up) {
        //CaseX(W[w].x[i] = xp);
        //CaseY(W[w].y[i] = yp);
        CaseZ(W[w].z[i] = zp);
      }
      else {
        //CaseX(W[w].xdn[i] = xp);
        //CaseY(W[w].ydn[i] = yp);
        CaseZ(W[w].zdn[i] = zp);
      }
      W[w].U -= dS;

      accepted_one++;
    }
    else {
      rejected_one++;
    }
  }
}

/*************************** PIGS pseudopotential *************************************/
void PIGSPseudopotentialMoveOneByOneOBDM(int w) {
  int i,j,iloop;
  int w1,w2; // index of the walker with [w-1] and [w+1]
  DOUBLE dS, dSkin, dSdelta, dSwf;
  DOUBLE dx,dy,dz;
  DOUBLE xp,yp,zp;
  DOUBLE xold, yold, zold;
  DOUBLE tau;
  int chain_edge = OFF;
  int up;
  DOUBLE x, y, z;
  DOUBLE xm, ym, zm; // position of the middle point
  DOUBLE mA, mB, mAB; // i.e. 2 mu
#ifdef EXTERNAL_POTENTIAL
  DOUBLE dEpot;
#endif
  static int first_time = ON;
  int move_ira = OFF;
  int move_masha = OFF;
  int move_worm = OFF;
  int link_to_previous_level;
  int link_to_next_level;

  if(first_time) {
    worm.w = Nwalkers/2; // open the middle position of the chain
    worm.i = 0; // open particle number i
    worm.z_ira = W[w].z[worm.i]; // at the first moment of time both ends stay at the same particle
    worm.z_masha = W[w].z[worm.i];
    first_time = OFF;
  }

  x = y = z = 0.;
  xm = ym = zm = 0.;
  mA = mB = mAB = 1.;

#ifdef INFINITE_MASS_COMPONENT_DN // 2\mu  = m
  mAB = 2.;
#endif

  //tau = dt; // imaginary time step
  tau = beta;

  w1 = w-1;
  w2 = w+1;
  if(w == 0) chain_edge = ON; // treat w=0 and w=Nwalkers-1 separately, according to the trial w.f.
  if(w == Nwalkers-1) chain_edge = ON; // treat w=0 and w=Nwalkers-1 separately

  // main loop,  move particle i of the bid w
#ifdef MOVE_ONLY_UP_PARTICLES
  for(iloop=0; iloop<Nup; iloop++) { // only particles up are moved
#else // move all particles
  for(iloop=0; iloop<Nup+Ndn; iloop++) { // move i-th particle to the point r -> rp
#endif
    dSkin = dSdelta = dSwf = 0.;
#ifdef EXTERNAL_POTENTIAL
    dEpot = 0.;
#endif
    link_to_next_level = link_to_previous_level = ON;

    if(iloop<Nup) { // move particle up
      up = ON;
      i = iloop;
    }
    else { // move particle down
      up = OFF;
      i = iloop-Nup;
    }

    if(up)
      CaseZ(z = W[w].z[i]);
    else 
      CaseZ(z = W[w].zdn[i]);

    if(w == 0) link_to_previous_level = OFF;
    if(w == Nwalkers-1) link_to_next_level = OFF;

    if(w == worm.w && i == worm.i) {
      move_worm = ON;
      if(rand() % 2 == 0) { // chose randomly which open end will be moved
        move_ira = ON;
        z = worm.z_ira;
        link_to_next_level = OFF;
      }
      else {
        move_masha = ON;
        z = worm.z_masha;
        link_to_previous_level = OFF;
      }
    }
    else {
      move_worm = OFF;
    }

    // Wp is the trial position
    RandomNormal3(&dx, &dy, &dz, 0., Sqrt(dt));
    //CaseZ(zp = z + dz);
    zp = z + dz;
#ifdef BC_1DPBC
    ReduceToTheBoxXYZ(&xp, &yp, &zp);
#endif
 
    if(i == worm.i) { // the open path is moved
      if(w == worm.w-1) { // previous layer, r.h.s. corresponds to ira
        //W[w2].z[i] = worm.z_ira;
        W[worm.w].z[i] = worm.z_ira;
      }
      else if(w == worm.w+1) { // next layer, l.h.s. corresponds to masha
        //W[w1].z[i] = worm.z_masha;
        W[worm.w].z[i] = worm.z_masha;
      }
      else if(w == worm.w) { // open time slice
        if(move_ira) { // current layer corresponds to ira
          W[w].z[i] = worm.z_ira;
        }
        else { // current layer corresponds to masha
          W[w].z[i] = worm.z_masha;
        }
      }
    }

    // contribution from the Laplacian, - (R'(i)-R(i+/-1))^2 / 2 + (R(i)-R(i+/-1))^2 / 2, involves the same particle
    if(up) { // spin up particle is moved
#ifdef FINITE_MASS_COMPONENT_UP // mA finite
      if(link_to_previous_level) dSkin += PropagatorLaplacian(mA, tau, zp - W[w1].z[i]) - PropagatorLaplacian(mA, tau, z - W[w1].z[i]);
      if(link_to_next_level)     dSkin += PropagatorLaplacian(mA, tau, zp - W[w2].z[i]) - PropagatorLaplacian(mA, tau, z - W[w2].z[i]);
#endif
#ifdef EXTERNAL_POTENTIAL
      if(i == worm.i && w == worm.w) {
        dEpot += 0.5*VextUp(xp, yp, zp, i); // new external potential energy for the worm (1/2) weight
        dEpot -= 0.5*VextUp(x,  y,  z,  i); // old external potential energy for the worm (1/2) weight
      }
      else {
        dEpot += VextUp(xp, yp, zp, i); // new external potential energy
        dEpot -= VextUp(x,  y,  z,  i); // old external potential energy
      }
#endif
    }
    else { // spin down particle is moved
#ifdef FINITE_MASS_COMPONENT_DN // mB finite
      if(link_to_previous_level) dSkin += PropagatorLaplacian(mB, tau, zp - W[w1].zdn[i]) - PropagatorLaplacian(mB, tau, z - W[w1].zdn[i]);
      if(link_to_next_level)     dSkin += PropagatorLaplacian(mB, tau, zp - W[w2].zdn[i]) - PropagatorLaplacian(mB, tau, z - W[w2].zdn[i]);
#endif
#ifdef EXTERNAL_POTENTIAL
    dEpot += VextDn(xp, yp, zp, i); // new external potential energy
    dEpot -= VextDn(x,  y,  z,  i); // old external potential energy
#endif
    }

    // contribution from Pseudopotential
    if(up) {
      for(j=0; j<Ndn; j++) { // AB interaction energy: ij
        if(link_to_previous_level) dSdelta += PropagatorPseudopotenial1D_ij(mAB, a, tau, zp, W[w].zdn[j], W[w1].z[i], W[w1].zdn[j])
                                            - PropagatorPseudopotenial1D_ij(mAB, a, tau, z,  W[w].zdn[j], W[w1].z[i], W[w1].zdn[j]);
        if(link_to_next_level)     dSdelta += PropagatorPseudopotenial1D_ij(mAB, a, tau, zp, W[w].zdn[j], W[w2].z[i], W[w2].zdn[j])
                                            - PropagatorPseudopotenial1D_ij(mAB, a, tau, z,  W[w].zdn[j], W[w2].z[i], W[w2].zdn[j]);
      }      
#ifdef FINITE_MASS_COMPONENT_UP // mA finite
      for(j=0; j<Nup; j++) if(j != i) { // AA interaction energy: ij'
        if(link_to_previous_level) dSdelta += PropagatorPseudopotenial1D_ij(mA, aA, tau, zp, W[w].z[j], W[w1].z[i], W[w1].z[j])
                                            - PropagatorPseudopotenial1D_ij(mA, aA, tau, z,  W[w].z[j], W[w1].z[i], W[w1].z[j]);
        if(link_to_next_level)     dSdelta += PropagatorPseudopotenial1D_ij(mA, aA, tau, zp, W[w].z[j], W[w2].z[i], W[w2].z[j])
                                            - PropagatorPseudopotenial1D_ij(mA, aA, tau, z,  W[w].z[j], W[w2].z[i], W[w2].z[j]);
      }
#endif
    }
    else { //  Move down particle
      for(j=0; j<Nup; j++) { // AB interaction energy: ij
        if(link_to_previous_level) dSdelta += PropagatorPseudopotenial1D_ij(mAB, a, tau, W[w].z[j], zp, W[w1].z[j], W[w1].zdn[i])
                                            - PropagatorPseudopotenial1D_ij(mAB, a, tau, W[w].z[j], z,  W[w1].z[j], W[w1].zdn[i]);
        if(link_to_next_level)     dSdelta += PropagatorPseudopotenial1D_ij(mAB, a, tau, W[w].z[j], zp, W[w2].z[j], W[w2].zdn[i])
                                            - PropagatorPseudopotenial1D_ij(mAB, a, tau, W[w].z[j], z,  W[w2].z[j], W[w2].zdn[i]);
      }
#ifdef FINITE_MASS_COMPONENT_DN // mB finite
      for(j=0; j<Ndn; j++) if(j != i) { // BB interaction energy: ij'
        if(link_to_previous_level) dSdelta += PropagatorPseudopotenial1D_ij(mB, aB, tau, zp, W[w].zdn[j], W[w1].zdn[i], W[w1].zdn[j])
                                            - PropagatorPseudopotenial1D_ij(mB, aB, tau, z,  W[w].zdn[j], W[w1].zdn[i], W[w1].zdn[j]);
        if(link_to_next_level)     dSdelta += PropagatorPseudopotenial1D_ij(mB, aB, tau, zp, W[w].zdn[j], W[w2].zdn[i], W[w2].zdn[j])
                                            - PropagatorPseudopotenial1D_ij(mB, aB, tau, z,  W[w].zdn[j], W[w2].zdn[i], W[w2].zdn[j]);
      }
#endif
    }

    if(chain_edge) { // at the edge the weight is given by the trial w.f.
      dSwf -= U(W[w]); // subtract the old weight p(R) = psi = Exp(U), trial wave function
      if(up) { // multiply by the new weight p(R') = psi'^2 = Exp(2U')
        CaseZ(zold = W[w].z[i]);
        CaseZ(W[w].z[i] = zp);
      }
      else {
        CaseZ(zold = W[w].zdn[i]);
        CaseZ(W[w].zdn[i] = zp);
      }
      dSwf += U(W[w]); // add new weight from the trial wave function, calculated with the new coordinates
      if(up) { // restore coordinates
        CaseZ(W[w].z[i] = zold);
      }
      else {
        CaseZ(W[w].zdn[i] = zold);
      }
    }

#ifdef SECURE
    if(isnan(dSdelta)) Warning("NaN weight in PIGS move!\n");
#endif

    dS = dSkin + dSdelta + dSwf; // dSdelta - contribution from delta function, dSwf from the trial w.f.
#ifdef EXTERNAL_POTENTIAL
    dS -= tau*dEpot; // potential energy contribution
#endif

    // The Metropolis code
    if(Random() < Exp(dS)) { // the move is accepted
      if(up) {
        //CaseX(W[w].x[i] = xp);
        //CaseY(W[w].y[i] = yp);
        CaseZ(W[w].z[i] = zp);
      }
      else {
        //CaseX(W[w].xdn[i] = xp);
        //CaseY(W[w].ydn[i] = yp);
        CaseZ(W[w].zdn[i] = zp);
      }
      W[w].U -= dS;

      if(CheckCoordinatesInTheBox())
         Message("check coord not passed");

      if(move_worm) {
        if(move_ira)
          worm.z_ira = zp;
        else
          worm.z_masha = zp;
      }
      accepted_one++;
    }
    else { // the move is rejected
      rejected_one++;
    }

    if(move_worm) { // measure OBDM
      DOUBLE r2,dr[3],r;
      int n;

     //Message("%lf %lf\n", worm.z_ira, worm.z_masha);

      dr[2] = worm.z_ira - worm.z_masha;
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      r = sqrt(r2);
      if(CheckInteractionCondition(dr[2], r2)) {
        n = (int) (r / OBDMpath_integral.step);
        if(n>=OBDMpath_integral.size) n = OBDMpath_integral.size-1;
        OBDMpath_integral.N[n]++;
      }
      OBDMpath_integral.times_measured++;
    }

    move_ira = move_masha = OFF;
  }
}

/*************************** PIGS pseudopotential *************************************/
DOUBLE PIGSPseudopotentialS(DOUBLE *Skincheck, DOUBLE *Sdeltacheck, DOUBLE *Swfcheck) { // test change
//DOUBLE PIGSPseudopotentialS(void) {
//void PIGSPseudopotentialMoveOneChain(int w, int i) {
  int i, j, w, w1, w2; // index of the walker with [w-1] and [w+1]
  DOUBLE S, Skin, Sdelta, Swf;
  DOUBLE mA, mB, mAB; // i.e. 2 mu
  DOUBLE tau;
#ifdef EXTERNAL_POTENTIAL
  DOUBLE Epot = 0.; // potential energy contribution
#endif

  tau = beta;
  mA = mB = mAB = 1.;

#ifdef INFINITE_MASS_COMPONENT_DN // 2\mu  = m
  mAB = 2.;
#endif

  if(SmartMC == PIGS_PSEUDOPOTENTIAL_OBDM) { // initialize with ira position
    W[worm.w].z[worm.i] = worm.z_ira;
  }

  Skin = Sdelta = Swf = 0.;
  for(w=1; w<Nwalkers; w++) {
    w1 = w-1;
#ifdef FINITE_MASS_COMPONENT_UP // mA finite
    for(i=0; i<Nup; i++) Skin += PropagatorLaplacian(mA, tau, W[w1].z[i] - W[w].z[i]);
    for(i=0; i<Nup; i++) for(j=i+1; j<Nup; j++) Sdelta += PropagatorPseudopotenial1D_ij(mA,  aA, tau, W[w].z[i],   W[w].z[j],   W[w1].z[i],   W[w1].z[j]);
#endif
#ifdef FINITE_MASS_COMPONENT_DN // mB finite
    for(i=0; i<Ndn; i++) Skin += PropagatorLaplacian(mB, tau, W[w1].zdn[i] - W[w].zdn[i]);
    for(i=0; i<Ndn; i++) for(j=i+1; j<Ndn; j++) Sdelta += PropagatorPseudopotenial1D_ij(mB,  aB, tau, W[w].zdn[i], W[w].zdn[j], W[w1].zdn[i], W[w1].zdn[j]);
#endif
    for(i=0; i<Nup; i++) for(j=0; j<Ndn; j++)   Sdelta += PropagatorPseudopotenial1D_ij(mAB, a,  tau, W[w].z[i],   W[w].zdn[j], W[w1].z[i],   W[w1].zdn[j]);
#ifdef EXTERNAL_POTENTIAL
    for(i=0; i<Nup; i++) Epot += VextUp(W[w].x[i], W[w].y[i], W[w].z[i], i);
    for(i=0; i<Ndn; i++) Epot += VextDn(W[w].xdn[i], W[w].ydn[i], W[w].zdn[i], i);
#endif
  }
  Swf += U(W[0]);
  Swf += U(W[Nwalkers-1]);
  S = Skin + Sdelta + Swf; // dSdelta - contribution from delta function, dSwf from the trial w.f.
  *Skincheck = Skin;
  *Sdeltacheck = Sdelta;
  *Swfcheck = Swf;

  if(SmartMC == PIGS_PSEUDOPOTENTIAL_OBDM) {
    i = worm.i;
    w = worm.w;
    w2 = w + 1; // next
    Skin -= PropagatorLaplacian(mA, tau, worm.z_ira   - W[w2].z[worm.i]); // this link does not belong to ira
    Skin += PropagatorLaplacian(mA, tau, worm.z_masha - W[w2].z[worm.i]); // this link belongs to masha
    for(j=0; j<Nup; j++) if(j != worm.i) {
      Sdelta -= PropagatorPseudopotenial1D_ij(mA, aA, tau, worm.z_ira,   W[w].z[j], W[w2].z[i], W[w2].z[j]); // remove ira + w2
      Sdelta += PropagatorPseudopotenial1D_ij(mA, aA, tau, worm.z_masha, W[w].z[j], W[w2].z[i], W[w2].z[j]); // add masha + w2
    }
#ifdef EXTERNAL_POTENTIAL // potential energy is divided in two pieces
    Epot -= 0.5*VextUp(W[w].x[i], W[w].y[i], worm.z_ira, i);
    Epot += 0.5*VextUp(W[w].x[i], W[w].y[i], worm.z_ira, i);
#endif
  }

#ifdef EXTERNAL_POTENTIAL
  S -= tau*Epot; // potential energy contribution
#endif

  return S;
}

/************************** Kinetic Energy ************************************/
DOUBLE PIGSKineticEnergy(int w) {
// calculates energy at layer w
  DOUBLE tau;
  DOUBLE Ekin = 0.;
  DOUBLE dr[3], r2 = 0.;
  DOUBLE x, y, z;
  DOUBLE xm, ym, zm;
  int i, w1;
  x = y = z = 0;
  xm = ym = zm = 0;

  w1 = w-1;
  //w2 = w+1;
  tau = dt;

  //CaseX(xm = 0.5*(W[w1].x[i] + W[w2].x[i])); // middle point
  //CaseY(ym = 0.5*(W[w1].y[i] + W[w2].y[i]));
  //CaseZ(zm = 0.5*(W[w1].z[i] + W[w2].z[i]));
  for(i=0; i<Nup; i++) {
    CaseX(dr[0] = W[w].x[i] - W[w1].x[i]);
    CaseY(dr[1] = W[w].y[i] - W[w1].y[i]);
    CaseZ(dr[2] = W[w].z[i] - W[w1].z[i]);
    r2 += FindNearestImage(&dr[0], &dr[1], &dr[2]);
  }
  for(i=0; i<Ndn; i++) {
    CaseX(dr[0] = W[w].xdn[i] - W[w1].xdn[i]);
    CaseY(dr[1] = W[w].ydn[i] - W[w1].ydn[i]);
    CaseZ(dr[2] = W[w].zdn[i] - W[w1].zdn[i]);
    r2 += FindNearestImage(&dr[0], &dr[1], &dr[2]);
  }

  //Ekin = 0.5*Ekin/tau + DIMENSION * 0.5 * (Nup + Ndn) * Log(2.*PI*tau);
  Ekin = (DOUBLE) (Nup + Ndn) * (DOUBLE) DIMENSION / (2.*tau) - r2/(2.*tau*tau);

  return Ekin;
}

/************************ External Energy Tilde ******************************/
// [V, [T, V]]
DOUBLE VextTilde(DOUBLE x, DOUBLE y, DOUBLE z) {

#ifdef TRAP_POTENTIAL // i.e. the anisotropic oscillator
#ifdef TRIAL_1D
  // for 0.5*z*z the commutator gives z^2
  return z*z;
#else // 2D 3D
  Error("  VextTilde not implemented\n");
  return 0.;
#endif
#else  // no oscillator
  return 0.;
#endif
}

/************************ Interaction Energy Tilde ***************************/
// [V, [T, V]]
DOUBLE InteractionEnergyTilde(DOUBLE r) {

#ifdef INTERACTION_ABSENT
  return 0.;
#endif

#ifdef INTERACTION12_SUTHERLAND
  static int first_time = ON;
  static DOUBLE coef;
  static DOUBLE sqE,sqE2;
  DOUBLE V;

  if(first_time) {
    sqE = PI/L;
    sqE2 = sqE*sqE;

    coef = 2.*DIMENSION*(DIMENSION-1.)*sqE2*sqE;

    first_time = OFF;
  }

  V = coef/(Tg(sqE*r)*Sin(sqE*r)*Sin(sqE*r));
  //return V*V;

  return V;
#endif

#ifdef INTERACTION_DIPOLE
  //return 9.*D*D/(r*r*r*r*r*r*r*r);
  //return 3.*D/(r*r*r*r);
  return 3./(r*r*r*r);
#endif

  Error("  Interaction energy not implemented!");
  return 0.;

}

/************************ Walker Potential energy ****************************/
DOUBLE WalkerPotentialEnergyTilde(struct Walker *W) {
  int i,j,k;
  DOUBLE dr[3] = {0,0,0}, r, r2;
  DOUBLE V = 0., dF, Force;

  // one-body terms
  for(i=0; i<Nup; i++) V += VextTilde(W->x[i], W->y[i], W->z[i]);

  // two-body terms
  for(i=0; i<Nup; i++) { 
    for(j=i+1; j<Nup; j++) {
      dr[0] = W->x[i] - W->x[j];
      dr[1] = W->y[i] - W->y[j];
      dr[2] = W->z[i] - W->z[j];
      r2 = FindNearestImage(&dr[0], &dr[1], &dr[2]);
      r = Sqrt(r2);

      Force = InteractionEnergyTilde(r) / r; // returns (V(r))'
      for(k=0; k<3; k++) {
        dF = Force * dr[k];
        W->F[i][k] += dF;
        W->F[j][k] -= dF;
      }
    }
  }

  // two-body terms
  for(i=0; i<Nup; i++) V += W->F[i][0]*W->F[i][0] + W->F[i][1]*W->F[i][1] + W->F[i][2]*W->F[i][2];

  return V;
}

/*************************** Gaussian Jump Walker *****************************/
// R = R + chi, f(chi) = Exp(-R^2/2dt)
void GaussianJumpWalker(int w, DOUBLE sigma) {
  int i;
  DOUBLE dx, dy, dz;

  for(i=0; i<Nup; i++) {
    RandomNormal3(&dx, &dy, &dz, 0., sigma); // (x,y,z, mu, sigma)

    CaseX(W[w].x[i] += dx);
    CaseY(W[w].y[i] += dy);
    CaseZ(W[w].z[i] += dz);

    if(measure_SD) {
      CaseX(W[w].rreal[i][0] += dx);
      CaseY(W[w].rreal[i][1] += dy);
      CaseZ(W[w].rreal[i][2] += dz);
    }
#ifdef PARTICLES_NEVER_LEAVE_THE_BOX
    ReduceWalkerToTheBox(&W[w]);
#endif
  }
}


/**************************** DMC Quartic Move *********************************/
// note that in quadratic algorithm the time step is [2 dt]
// see Eqs. (33-36) in Siu A. Chin and C. R. Chen Gradient symplectic algorithms for solving the Schrodinger equation with time-dependent potentials J. Chem. Phys. 117, 1409 (2002), 
//
void PIGSQuarticMove(int w) {
#ifndef MEMORY_CONTIGUOUS
  int i,j;
#endif
  DOUBLE t1, t2, t3, v0, v1, v2, v3, u0; // time steps
  DOUBLE log_weight = 0.; // logarithm of the branching weight

  // primitive, only two steps:
  //GaussianJumpWalker(w, Sqrt(dt));
  //log_weight += dt*WalkerPotentialEnergy(&W[w]);

  // t1 is a free parameter :
  // algorithm 4A:  t1 = 1/2
  // algorithm 4D:  t1 = 1/3 
  t1 = 0.01; // t1 = 0.28 - 6th order solution for harmonic oscillator

  t3 = t1;
  t2 = 1. - 2.*t1;
  v3 = v0 = (6.*t1*(t1-1.)+1.)/(12.*(t1-1.)*t1);
  v2 = v1 = 0.5 - v0;
  u0 = (1./(6.*t1*(1.-t1)*(1.-t1))-1.)/48.;

  // step 1
  log_weight += v0*dt*WalkerPotentialEnergy(&W[w]);

  // step 2
  GaussianJumpWalker(w, Sqrt(dt*t1));

  // step 3
  //log_weight += v1*dt*(WalkerPotentialEnergy(&W[w]) + u0/v2*dt*dt*WalkerPotentialEnergyTilde(&W[w]));
  log_weight += v1*dt*WalkerPotentialEnergy(&W[w]);
  log_weight += v1*dt*u0/v2*dt*dt*WalkerPotentialEnergyTilde(&W[w]);

  // step 4
  GaussianJumpWalker(w, Sqrt(dt*t2));

  // step 5
  log_weight += v2*dt*(WalkerPotentialEnergy(&W[w]) + u0/v2*dt*dt*WalkerPotentialEnergyTilde(&W[w]));

  // step 6
  GaussianJumpWalker(w, Sqrt(dt*t3));

  // step 7
  log_weight += v3*dt*WalkerPotentialEnergy(&W[w]);

  // invert the sign and subtract the energy shift
  log_weight = dt*Eo-log_weight;
  W[w].weight = Exp(log_weight);

  W[w].E = WalkerEnergy0(&W[w]);
}
