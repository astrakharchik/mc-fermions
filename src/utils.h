/*utils.h*/

#ifndef _ERROR_H
#define _ERROR_H

#include <stdio.h>
#include "main.h"

void Error(const char * format, ...);

#ifndef USE_UTF8
#define Warning(...) Message("WARNING: "##__VA_ARGS__)
void Message(const char * format, ...);
#else // UTF
//#define Message(...) WMessage(L, ## __VA_ARGS__)
#define Message(...) WMessage(L ## __VA_ARGS__)
#define Warning(...) WMessage(L ## "\nWARNING: "##__VA_ARGS__)
void WMessage(const wchar_t * format, ...);
#endif

void CheckMantissa(void);
int CheckMemcpy(void);
int CheckContigiousArray(int **a, int dim1, int dim2);
int CheckEnergyAndDerivatives(void);
int CheckCoordinatesDimensionality(void);
int CheckCoordinatesInTheBox(void);
void MessageTimeElapsed(int sec);

#define ON 1
#define OFF 0

#endif
