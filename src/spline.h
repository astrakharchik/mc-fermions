/*spline.h*/

#ifndef __SPLINE_H_
#define __SPLINE_H_

#include "main.h"

void SaveWaveFunctionSpline(struct Grid *G, char *file_out);
void SplineConstruct(struct Grid *G, DOUBLE yp1, DOUBLE ypn);
void InterpolateSpline(struct Grid *G, DOUBLE r, DOUBLE *y, DOUBLE *yp, DOUBLE *ypp);
DOUBLE InterpolateSplineU(struct Grid *G, DOUBLE r);
DOUBLE InterpolateSplineF(struct Grid *G, DOUBLE r);
DOUBLE InterpolateSplineFp(struct Grid *G, DOUBLE r);
DOUBLE InterpolateSplineE(struct Grid *G, DOUBLE r);
DOUBLE InterpolateSplinef(struct Grid *G, DOUBLE r);
DOUBLE InterpolateSplinefp(struct Grid *G, DOUBLE r);
DOUBLE InterpolateSplinefpp(struct Grid *G, DOUBLE r);

void ConstructGridSpline2Body_ij(struct Grid *G, DOUBLE (*InteractionEnergy)(DOUBLE x), int spin1, int spin2);
DOUBLE SolveScatteringProblem_ij(struct Grid *G, DOUBLE (*InteractionEnergy)(DOUBLE x), DOUBLE E, int *function_was_negative, int flag_lnf_instead_of_f, int spin1, int spin2);
DOUBLE SplineRightHandSide(struct Grid *Grid, DOUBLE x, int spin1, int spin2);

#endif
