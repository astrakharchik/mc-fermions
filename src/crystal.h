/*crystal.h*/
#ifndef _CRYSTAL_H_
#define _CRYSTAL_H_

struct Cryst {
  DOUBLE *x; // coordinates of the cells
  DOUBLE *y; 
  DOUBLE *z; 
  DOUBLE *xdn; // coordinates of the cells
  DOUBLE *ydn; 
  DOUBLE *zdn; 
  int size; // number of grid cells
  // Solid phase
#ifdef CRYSTAL_WIDTH_ARRAY
  DOUBLE *Rx; // Gaussian width in (x,y,z) directions
  DOUBLE *Ry; // 
  DOUBLE *Rz; // 
  DOUBLE *Rxdn; // Gaussian width in (x,y,z) directions
  DOUBLE *Rydn; // 
  DOUBLE *Rzdn; // 
#else
  DOUBLE R; // Gaussian width in (x,y,z) directions
#endif

  // Impurities
  DOUBLE b; // scattering length
  DOUBLE k; // momentum (fixed by Rpar)
  DOUBLE k2; 
};

extern struct Cryst Crystal;

void ConstructGridCrystal(struct Cryst *Crystal, int size);
void ConstructGridImpurity(struct Cryst *Crystal, const DOUBLE R, int size);

#ifdef IMPURITY
DOUBLE ImpurityInterpolateU(DOUBLE x);
DOUBLE ImpurityInterpolateFp(DOUBLE x);
DOUBLE ImpurityInterpolateE(DOUBLE x);
#endif

#endif
