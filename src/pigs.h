/*pigs.h*/
#ifndef _PIGS_H_
#define _PIGS_H_

#include "main.h"

void PIGSMoveOneByOne(int w);
void PIGSMoveAll(void);
void PIGSQuarticMove(int w);
DOUBLE PIGSKineticEnergy(int w);
DOUBLE VextTilde(DOUBLE x, DOUBLE y, DOUBLE z);
DOUBLE WalkerPotentialEnergyTilde(struct Walker *W);
DOUBLE InteractionEnergyTilde(DOUBLE r);
//DOUBLE PIGSPseudopotentialS(void);
DOUBLE PIGSPseudopotentialS(DOUBLE *dSkincheck, DOUBLE *dSdeltacheck, DOUBLE *dSwfcheck);

void PIGSPseudopotentialMoveOneByOne(int w);
void PIGSPseudopotentialMoveOneByOneOBDM(int w);
double PropagatorLaplacian(double m, double dt, double dr);
double PropagatorPseudopotenial1D_ij(double m, double a, double dt, double xi0, double xj0, double xi, double xj);

struct Worm {
  DOUBLE x_ira;
  DOUBLE y_ira;
  DOUBLE z_ira;
  DOUBLE x_masha;
  DOUBLE y_masha;
  DOUBLE z_masha;
  int w;
  int i;
};

#endif
