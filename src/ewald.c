#include "main.h"
#include "mymath.h"
#include "move.h"
#include "utils.h"
#include "ewald.h"
#include <math.h>

DOUBLE Ewald_alpha = 4.;
DOUBLE Ewald_alpha2;

//DOUBLE sum_over_images_cut_off_num = 3.;
int sum_over_images_cut_off_int_x = 5;
int sum_over_images_cut_off_int_y = 5;
int sum_over_images_cut_off_int_z = 5;

//CSM
DOUBLE EwaldPsi(DOUBLE x, DOUBLE y, DOUBLE z) {
  int nx, ny, nz, n2;
  DOUBLE psi = 0.;
  DOUBLE r_n_x, r_n_y, r_n_z, r_n, nr;
  
  for(nx=-sum_over_images_cut_off_int_x; nx<=sum_over_images_cut_off_int_x; nx++) {
    for(ny=-sum_over_images_cut_off_int_y; ny<=sum_over_images_cut_off_int_y; ny++) {
      for(nz=-sum_over_images_cut_off_int_z; nz<=sum_over_images_cut_off_int_z; nz++) {
        r_n_x = (DOUBLE) nx + x;
        r_n_y = (DOUBLE) ny + y;
        r_n_z = (DOUBLE) nz + z;
        r_n = r_n_x*r_n_x+r_n_y*r_n_y+r_n_z*r_n_z;
        psi += Exp(-Ewald_alpha2*r_n) / r_n;
        
        n2 = nx*nx + ny*ny + nz*nz;
        
        if(n2>0) {
          nr = (DOUBLE) nx * x + (DOUBLE) ny * y + (DOUBLE) + nz * z;
          psi += Cos(2*PI*nr)*erfc(PI*sqrt((DOUBLE)n2)/Ewald_alpha)*PI/(DOUBLE) sqrt(n2);
        }
      }
    }
  }
  return psi;
}

DOUBLE EwaldXi(void) {
  int nx, ny, nz, n2;
  DOUBLE xi = 0.;
  
  for(nx=-sum_over_images_cut_off_int_x; nx<=sum_over_images_cut_off_int_x; nx++) {
    for(ny=-sum_over_images_cut_off_int_y; ny<=sum_over_images_cut_off_int_y; ny++) {
      for(nz=-sum_over_images_cut_off_int_z; nz<=sum_over_images_cut_off_int_z; nz++) {
        n2 = nx*nx + ny*ny + nz*nz;
        if(n2>0) {
          xi += Exp(-Ewald_alpha2*(DOUBLE)n2) / (DOUBLE)n2;
        }
      }
    }
  }
  return xi;
}

DOUBLE EwaldSum(struct Walker *Walker) {
  int i,j;
  DOUBLE E = 0;
  DOUBLE Epsi = 0;
  DOUBLE Exi = 0;
  DOUBLE x,y,z,dr[3],r2;
  
  Ewald_alpha2 = Ewald_alpha*Ewald_alpha;
  
  for(i=0; i<Nup; i++) {
    x = Walker->x[i];
    y = Walker->y[i];
    z = Walker->z[i];
    for(j=i+1; j<Nup; j++) {
      dr[0] = x - Walker->x[j];
      dr[1] = y - Walker->y[j];
      dr[2] = z - Walker->z[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      Epsi += EwaldPsi(dr[0]/L, dr[1]/L, dr[2]/L) / (L*L);
    }
  }
  
  Exi += 0.5*(DOUBLE)Nup*EwaldXi()/L*L;
  Exi -= Nup/2.*Ewald_alpha2/L2 - Nup*Nup/(2.*L2) * 2./Ewald_alpha;

  Exi /= Nup;
  Epsi /= Nup;
  
  E += Epsi + Exi;
  
  Message("Ewald: E = %lf\n", E);

  return E;
}

/*// Coulomb
DOUBLE EwaldPsi(DOUBLE x, DOUBLE y, DOUBLE z) {
  int nx, ny, nz, n2;
  DOUBLE psi = 0.;
  DOUBLE r_n_x, r_n_y, r_n_z, r_n, nr;

  for(nx=-sum_over_images_cut_off_int_x; nx<=sum_over_images_cut_off_int_x; nx++) {
    for(ny=-sum_over_images_cut_off_int_y; ny<=sum_over_images_cut_off_int_y; ny++) {
      for(nz=-sum_over_images_cut_off_int_z; nz<=sum_over_images_cut_off_int_z; nz++) {
        r_n_x = (DOUBLE) nx + x;
        r_n_y = (DOUBLE) ny + y;
        r_n_z = (DOUBLE) nz + z;
        r_n = sqrt(r_n_x*r_n_x+r_n_y*r_n_y+r_n_z*r_n_z);
        psi += erfc(Ewald_alpha*r_n) / r_n;

        n2 = nx*nx + ny*ny + nz*nz;

        if(n2>0) {
          nr = (DOUBLE) nx * x + (DOUBLE) ny * y + (DOUBLE) + nz * z;
          psi += Cos(2*PI*nr)*Exp(-PI*PI*(DOUBLE)n2/Ewald_alpha2)/ (PI*(DOUBLE) n2);
        }
      }
    }
  }
  return psi;
}

DOUBLE EwaldXi(void) {
  int nx, ny, nz, n2;
  DOUBLE xi = 0.;
  DOUBLE n;

  for(nx=-sum_over_images_cut_off_int_x; nx<=sum_over_images_cut_off_int_x; nx++) {
    for(ny=-sum_over_images_cut_off_int_y; ny<=sum_over_images_cut_off_int_y; ny++) {
      for(nz=-sum_over_images_cut_off_int_z; nz<=sum_over_images_cut_off_int_z; nz++) {
        n2 = nx*nx + ny*ny + nz*nz;
        if(n2>0) {
          n = sqrt((DOUBLE)n2);
          xi += erfc(Ewald_alpha*n) / n;
          xi += Exp(-PI*PI*n2/Ewald_alpha2)/(PI*n2);
        }
      }
    }
  }
  return xi - 2.*Ewald_alpha/sqrt(PI);
}

DOUBLE EwaldSum(struct Walker *Walker) {
  int i,j;
  DOUBLE E = 0;
  DOUBLE Epsi = 0;
  DOUBLE Exi = 0;
  DOUBLE Echarge = 0;
  DOUBLE x,y,z,dr[3],r2;

  Ewald_alpha2 = Ewald_alpha*Ewald_alpha;

  for(i=0; i<Nup; i++) {
    x = Walker->x[i];
    y = Walker->y[i];
    z = Walker->z[i];
    for(j=i+1; j<Nup; j++) {
      dr[0] = x - Walker->x[j];
      dr[1] = y - Walker->y[j];
      dr[2] = z - Walker->z[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      Epsi += EwaldPsi(dr[0]/L, dr[1]/L, dr[2]/L) / L;
    }
  }

  Exi += 0.5*(DOUBLE)Nup*EwaldXi()/L;
  Exi -= Nup*Nup*PI/(2.*L*Ewald_alpha2);
  
  x = y = z = 0.;
  for(i=0; i<Nup; i++) {
    x += Walker->x[i];
    y += Walker->y[i];
    z += Walker->z[i];
  }
  x -= Nup*Lhalf;
  y -= Nup*Lhalf;
  z -= Nup*Lhalf;
  Echarge += 2.*PI/(3.*L*L*L)*sqrt(x*x+y*y+z*z);

  Exi /= Nup;
  Epsi /= Nup;
  Echarge /= Nup;

  E += Epsi + Exi + Echarge;

  Message("Ewald: E = %lf\n", E/(DOUBLE)Nup);
}*/

/***************** Interaction Self Energy Sum Over Images *******************/
// direct sum of the potential energy over images
#ifndef OPTIMIZED_SELF_ENERGY_SUMMATION
DOUBLE InteractionEnergySumOverImages12(DOUBLE x, DOUBLE y, DOUBLE z) {
  DOUBLE E = 0.;
  int nx, ny, nz;
  DOUBLE r;

  nx = ny = nz = 0;

#ifdef MOVE_IN_X
  for(nx=-(int)sum_over_images_cut_off_num; nx<=(int)sum_over_images_cut_off_num; nx++)
#endif
#ifdef MOVE_IN_Y
    for(ny=-(int)sum_over_images_cut_off_num; ny<=(int)sum_over_images_cut_off_num; ny++)
#endif
#ifdef MOVE_IN_Z
      for(nz=-(int)sum_over_images_cut_off_num; nz<=(int)sum_over_images_cut_off_num; nz++)
#endif
      {
        r = Sqrt((x+nx*Lx)*(x+nx*Lx)+(y+ny*Ly)*(y+ny*Ly)+(z+nz*Lz)*(z+nz*Lz));

        // maximal distance covered by a sphere L/2 + L x sum_over_images_cut_off_num
        if(r<L*(0.5+(int)sum_over_images_cut_off_num))
          E += InteractionEnergy12(r);
      }

  return E;
}

DOUBLE InteractionEnergySumOverImages11(DOUBLE x, DOUBLE y, DOUBLE z) {
  DOUBLE E = 0.;
  static int initialized = OFF;
  static DOUBLE Eself = 0.;
  int nx, ny, nz;
  DOUBLE r;

  nx = ny = nz = 0;

  if(initialized == OFF) { // first time calculate self energy
    Eself = InteractionSelfEnergy11();
    initialized = ON;
  }

#ifdef MOVE_IN_X
  for(nx=-(int)sum_over_images_cut_off_num; nx<=(int)sum_over_images_cut_off_num; nx++)
#endif
#ifdef MOVE_IN_Y
    for(ny=-(int)sum_over_images_cut_off_num; ny<=(int)sum_over_images_cut_off_num; ny++)
#endif
#ifdef MOVE_IN_Z
      for(nz=-(int)sum_over_images_cut_off_num; nz<=(int)sum_over_images_cut_off_num; nz++)
#endif
      {
        r = Sqrt((x+nx*Lx)*(x+nx*Lx)+(y+ny*Ly)*(y+ny*Ly)+(z+nz*Lz)*(z+nz*Lz));

        // maximal distance covered by a sphere L/2 + L x sum_over_images_cut_off_num
        if(r<L*(0.5+(int)sum_over_images_cut_off_num)) E += InteractionEnergy11(r);
      }

  E += Eself;

  return E;
}

DOUBLE InteractionEnergySumOverImages22(DOUBLE x, DOUBLE y, DOUBLE z) {
  DOUBLE E = 0.;
  static int initialized = OFF;
  static DOUBLE Eself = 0.;
  int nx, ny, nz;
  DOUBLE r;

  nx = ny = nz = 0;

  if(initialized == OFF) { // first time calculate self energy
    Eself = InteractionSelfEnergy22();
    initialized = ON;
  }

#ifdef MOVE_IN_X
  for(nx=-(int)sum_over_images_cut_off_num; nx<=(int)sum_over_images_cut_off_num; nx++)
#endif
#ifdef MOVE_IN_Y
    for(ny=-(int)sum_over_images_cut_off_num; ny<=(int)sum_over_images_cut_off_num; ny++)
#endif
#ifdef MOVE_IN_Z
      for(nz=-(int)sum_over_images_cut_off_num; nz<=(int)sum_over_images_cut_off_num; nz++)
#endif
      {
        r = Sqrt((x+nx*Lx)*(x+nx*Lx)+(y+ny*Ly)*(y+ny*Ly)+(z+nz*Lz)*(z+nz*Lz));

        // maximal distance covered by a sphere L/2 + L x sum_over_images_cut_off_num
        if(r<L*(0.5+(int)sum_over_images_cut_off_num)) E += InteractionEnergy22(r);
      }

  E += Eself;

  return E;
}

#else // (dipoles) assumes g2(r)=1 outside of the direct summation radius
DOUBLE InteractionEnergySumOverImages(DOUBLE x, DOUBLE y, DOUBLE z) {
  int nx,ny;
  DOUBLE rr2,rr2_,rr2x,rr2y,s; // Kurbakov
  DOUBLE sum_over_images_cut_off_num_d;
  static int initialized = OFF;
  static DOUBLE Eself = 0.;

  if(initialized == OFF) { // first time calculate self energy

    initialized = ON; // Kurbakov
    if(sum_over_images_cut_off_num < 1.2) { // Kurbakov
      Warning(" Cannot do summation with sum_over_images_cut_off_num<1.2, setting it to 1.2\n"); // Kurbakov
      sum_over_images_cut_off_num = 1.2; // Kurbakov
    } // Kurbakov
    Eself = InteractionSelfEnergy(); // Kurbakov
  }

  s=0;
  sum_over_images_cut_off_num_d = sum_over_images_cut_off_num * Sqrt(Lx*Ly);

  for(nx=-(int)(sum_over_images_cut_off_num_d/Lx)-1; nx<=(int)(sum_over_images_cut_off_num_d/Lx)+1; nx++) { // Kurbakov
    rr2x=(nx*Lx+x)*(nx*Lx+x); // Kurbakov
    for(ny=-(int)(sum_over_images_cut_off_num_d/Ly*1.5)-1; ny<=(int)(sum_over_images_cut_off_num_d/Ly*1.5)+1; ny++) { // Kurbakov
    rr2y=(ny*Ly+y)*(ny*Ly+y); // Kurbakov
      rr2_=rr2x+rr2y*0.5; // Kurbakov
      if(0<rr2_&&rr2_<sum_over_images_cut_off_num_d*sum_over_images_cut_off_num_d) { // Kurbakov
        rr2=rr2x+rr2y; // Kurbakov
#ifdef INTERACTION_QUADRUPOLE
        s += 1./(rr2*rr2*Sqrt(rr2)); // for quadrupoles // Kurbakov
#endif
#ifdef INTERACTION_DIPOLE
        s += 1./(rr2*Sqrt(rr2)); // Kurbakov
#endif
      } // Kurbakov
    } // Kurbakov
  } // Kurbakov
#ifdef INTERACTION_QUADRUPOLE
  return (s+2.*PI*0.66312316693288855/(3.*sum_over_images_cut_off_num_d*sum_over_images_cut_off_num_d*sum_over_images_cut_off_num_d*Lx*Ly)) + Eself; // for quadrupoles // Kurbakov
#endif
#ifdef INTERACTION_DIPOLE
  return (s+2.*PI*0.85984660010223786/(sum_over_images_cut_off_num_d*Lx*Ly)) + Eself; // Kurbakov
#endif
}
#endif

/************************ Interaction Self Energy ****************************/
DOUBLE InteractionSelfEnergy11(void) {
  int nx = 0;
  int ny = 0;
  int nz = 0;
  DOUBLE Eself1, Eself2, Eself, r;
  int nmax = 3;

  // single size
  Eself = 0.;
  nmax = (int) sum_over_images_cut_off_num;
#ifdef MOVE_IN_X
  for(nx=-nmax; nx<=nmax; nx++)
#endif
#ifdef MOVE_IN_Y
    for(ny=-nmax; ny<=nmax; ny++)
#endif
#ifdef MOVE_IN_Z
      for(nz=-nmax; nz<=nmax; nz++)
#endif
      if(nx!=0 || ny!=0 || nz!=0) {
        // calculates up to cut-off radius
        //!!! Eself += InteractionEnergy11(Sqrt((nx*Lx)*(nx*Lx)+(ny*Ly)*(ny*Ly)+(nz*Lz)*(nz*Lz)));
        r = Sqrt((nx*Lx)*(nx*Lx)+(ny*Ly)*(ny*Ly)+(nz*Lz)*(nz*Lz));
        // maximal distance covered by a sphere L/2 + L x sum_over_images_cut_off_num
        if(r<L*(0.5+(int)sum_over_images_cut_off_num)) Eself += InteractionEnergy11(r);
      }
  Eself *= 0.5*(DOUBLE) Nup; // total self energy
  //Eself /= (DOUBLE) N; // energy per particle
  Eself /= (DOUBLE) (Nup*(Nup-1)/2); // to be called N(N-1)/2 times
  Eself2 = Eself;

  // DOUBLE size
  nmax *= 2;
  Eself = 0.;
#ifdef MOVE_IN_X
  for(nx=-nmax; nx<=nmax; nx++)
#endif
#ifdef MOVE_IN_Y
    for(ny=-nmax; ny<=nmax; ny++)
#endif
#ifdef MOVE_IN_Z
      for(nz=-nmax; nz<=nmax; nz++)
#endif
      if(nx!=0 || ny!=0 || nz!=0) {
        //Eself += InteractionEnergy11(Sqrt((nx*Lx)*(nx*Lx)+(ny*Ly)*(ny*Ly)+(nz*Lz)*(nz*Lz)));
        r = Sqrt((nx*Lx)*(nx*Lx)+(ny*Ly)*(ny*Ly)+(nz*Lz)*(nz*Lz));
        // maximal distance covered by a sphere L/2 + L x sum_over_images_cut_off_num
        if(r<L*(0.5+(int)sum_over_images_cut_off_num)) Eself += InteractionEnergy11(r);
      }

  Eself *= 0.5*(DOUBLE) Nup; // total self energy
  //Eself /= (DOUBLE) N; // energy per particle
  Eself /= (DOUBLE) (Nup*(Nup-1)/2); // to be called N(N-1)/2 times
  Eself1 = Eself;

  // extrapolation to infinite size
  Eself = 2.*Eself1-Eself2;
  Message("  Interaction self Energy Sum Over Images11: %"LE" (per particle)\n", Eself*0.5*(DOUBLE)(Nup-1));
  
  //Eself = Ewald_Xi*Ewald_Lk/((DOUBLE)(N-1));

  return Eself;
}

/************************ Interaction Self Energy ****************************/
DOUBLE InteractionSelfEnergy22(void) {
  int nx = 0;
  int ny = 0;
  int nz = 0;
  DOUBLE Eself1, Eself2, Eself, r;
  int nmax = 3;

  // single size
  Eself = 0.;
  nmax = (int) sum_over_images_cut_off_num;
#ifdef MOVE_IN_X
  for(nx=-nmax; nx<=nmax; nx++)
#endif
#ifdef MOVE_IN_Y
    for(ny=-nmax; ny<=nmax; ny++)
#endif
#ifdef MOVE_IN_Z
      for(nz=-nmax; nz<=nmax; nz++)
#endif
      if(nx!=0 || ny!=0 || nz!=0) {
        //Eself += InteractionEnergy22(Sqrt((nx*Lx)*(nx*Lx)+(ny*Ly)*(ny*Ly)+(nz*Lz)*(nz*Lz)));
        r = Sqrt((nx*Lx)*(nx*Lx)+(ny*Ly)*(ny*Ly)+(nz*Lz)*(nz*Lz));
        // maximal distance covered by a sphere L/2 + L x sum_over_images_cut_off_num
        if(r<L*(0.5+(int)sum_over_images_cut_off_num)) Eself += InteractionEnergy22(r);
      }
  Eself *= 0.5*(DOUBLE) Ndn; // total self energy
  //Eself /= (DOUBLE) N; // energy per particle
  Eself /= (DOUBLE) (Ndn*(Ndn-1)/2); // to be called N(N-1)/2 times
  Eself2 = Eself;

  // DOUBLE size
  nmax *= 2;
  Eself = 0.;
#ifdef MOVE_IN_X
  for(nx=-nmax; nx<=nmax; nx++)
#endif
#ifdef MOVE_IN_Y
    for(ny=-nmax; ny<=nmax; ny++)
#endif
#ifdef MOVE_IN_Z
      for(nz=-nmax; nz<=nmax; nz++)
#endif
      if(nx!=0 || ny!=0 || nz!=0) {
        //Eself += InteractionEnergy22(Sqrt((nx*Lx)*(nx*Lx)+(ny*Ly)*(ny*Ly)+(nz*Lz)*(nz*Lz)));
        r = Sqrt((nx*Lx)*(nx*Lx)+(ny*Ly)*(ny*Ly)+(nz*Lz)*(nz*Lz));
        // maximal distance covered by a sphere L/2 + L x sum_over_images_cut_off_num
        if(r<L*(0.5+(int)sum_over_images_cut_off_num)) Eself += InteractionEnergy22(r);
      }

  Eself *= 0.5*(DOUBLE) Ndn; // total self energy
  //Eself /= (DOUBLE) N; // energy per particle
  Eself /= (DOUBLE) (Ndn*(Ndn-1)/2); // to be called N(N-1)/2 times
  Eself1 = Eself;

  // extrapolation to infinite size
  Eself = 2.*Eself1-Eself2;
  Message("  Interaction self Energy Sum Over Images22: %"LE" (per particle)\n", Eself*0.5*(DOUBLE)(Ndn-1));
  
  //Eself = Ewald_Xi*Ewald_Lk/((DOUBLE)(N-1));

  return Eself;
}
