/*parallel.h*/

#ifndef _PARALLEL_H_
#define _PARALLEL_H_

#define MASTER 0

#include "main.h"

#ifdef MPI

#include <mpi.h>

#define MSG_NWALKER 1
#define MSG_WALKER 2
#define MSG_REPLY 3
#define MSG_RADIAL_DISTRIBUTION 4
#define MSG_OBDM 5

extern int MPI_Nslaves;
extern int MPI_myid;

extern int NwalkersAll; // total number rof walkers on all machines
extern char *buffer;
extern int message_size;
extern int *NwalkersSl; // number of walkers at a given machine
extern int *walker_indices; // original indices of walkers distributed to a given machine
extern double time_done_in_parallel;

extern MPI_Status mpi_status; // error status

void ParallelInitializeMPI(int argn, char **argv);
void ParallelInitialize2(void);
void ParallelStop(void);
void CollectAllWalkers(void);
void DistributeAllWalkers(void);

void ParallelSaveBlockDataStart(void);
void ParallelSaveBlockDataEnd(void);
void ParallelSaveEnergyVMC(void);
void ParallelSaveMomentumDistribution(void);

void CollectAllWalkersData(void);
int SendWalkers(int dest, int wmin, int Nwalk, int tag);
int SendWalkersData(int dest, int wmin, int Nwalk, int tag);
void ReceiveWalkers(int src, int wmin, int tag);
void ReceiveWalkersData(int src, int wmin, int tag);

void DistributeWalkersSend(void);
void DistributeWalkersScatter(void);

#endif

/**************************** Open MP ****************************************/
#ifdef _OPENMP

#include <omp.h>
#include "main.h"

extern int OPENMP_myid; // check if this runs is a mastere or a slave
extern int OPENMP_Nslaves;

void ParallelInitializeOpenMP(int argn, char **argv);
DOUBLE RandomCritical(void);

#endif

#endif