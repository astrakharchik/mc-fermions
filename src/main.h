#ifndef __MAIN_H_
#define __MAIN_H_

#define SRC_CODE_DATE "Two-component Fermi gas code. Source code of 27/I/2021\n\n"

/* define SECURE to enable all self-consistent checks */
//#define SECURE
//#define USE_UTF8 // allow UTF characters in the input and output
#define MEMORY_CONTIGUOUS // use static allocation for walkers, otherwise dynamic array
//#define SCALABLE // define for truncation of the orbital at Lhalf/Nc
//#define CHECK_FORCE_ERROR // enable to check difference between first and second order integration schemes
//#define ZERO_ASYMPTOTIC // orbital goes to zero, finite otherwise
//#define BRANCHING_BOUNDARY_CONDITION_WITH_SOURCE // new walkers are added as a source

//#define CHECK_HARDCORE_RADIUS // MoveOneByOne (VMC and Classical) will be regected for overlapping particles
//#define HARDCORE_RADIUS_SQUARED_11 1. // r^2, is used when defined CHECK_HARDCORE_RADIUS
//#define HARDCORE_RADIUS_SQUARED_22 0.
//#define HARDCORE_RADIUS_SQUARED_12 0.
//#define INFINITE_MASS_COMPONENT_DN // infinite mass of the second component
//#define INFINITE_MASS_COMPONENT_UP // infinite mass of the first component
//#define UNIT_MASS_COMPONENT_UP // mass_ratio is used to calculate mass of the second component
//#define UNIT_MASS_COMPONENT_DN // mass_ratio is used to calculate mass of the first component

//#define MPI // define MPI for parallel computations
#define OPENMP_MAX_NPROC 500 // maximal number of nodes which can be used in Open MP; compile with /openmp option

// use Lapack in mkl implementation for doing matrix operations
//#define USE_LAPACK

// define precision (float, double, ...)
//#define DATATYPE_FLOAT
#define DATATYPE_DOUBLE
//#define DATATYPE_LONG_DOUBLE

#define IMAGES_FLOOR
//#define IMAGES_IF_IF
//#define IMAGES_ABS_IF

#define PRECISION 1e-10 // 1e-10 for double, 1e-7 for float

//============================= Interaction potential ====================================
// up + down particles: define ONLY ONE type of the interaction potential (which is not included in Eloc!)
#define INTERACTION_ABSENT // IFG 12
#define INTERACTION11_ABSENT // IFG
#define INTERACTION22_ABSENT // IFG
//#define INTERACTION12_SQUARE_WELL
//#define INTERACTION_DIPOLE
//#define INTERACTION11_DIPOLE
//#define INTERACTION22_DIPOLE
//#define INTERACTION12_DIPOLE_BILAYER
//#define INTERACTION_LENNARD_JONES
//#define INTERACTION11_LENNARD_JONES
//#define INTERACTION11_LENNARD_JONES_6_10
//#define INTERACTION12_LENNARD_JONES_6_10
//#define INTERACTION_CH2_11
//#define INTERACTION_CH2_12 // -Epar/ch^2(r)
//#define INTERACTION11_LiLi
//#define INTERACTION11_HeHe // LJ, is divided by energy_unit
//#define INTERACTION12_LiHe
//#define INTERACTION12_HeHe // LJ, is divided by energy_unit
//#define INTERACTION22_HeHe // LJ, is divided by energy_unit
//#define INTERACTION11_RYDBERG
//#define INTERACTION12_RYDBERG // D/r^6
//#define INTERACTION12_ION // Ion -1/r^4 + regularization, parameters a6,b6,c6
//#define INTERACTION11_RYDBERG_SCREENED // 1/(r^6+a^6)
//#define INTERACTION12_RYDBERG_SCREENED // a/(r^6+b^6)

//#define INTERACTION11_Aziz // uses energy_unit inside InteractionEnergy call, to have [K] as local energy unit
//#define INTERACTION12_Aziz // is divided by energy_unit
//#define INTERACTION22_Aziz // is divided by energy_unit

// up + up, down + down particles: define ONLY ONE type of the interaction potential (which is not included in Eloc!)
//#define INTERACTION11_SQUARE_WELL
//#define INTERACTION22_SQUARE_WELL
//#define INTERACTION12_SUTHERLAND // lambda12
//#define INTERACTION11_SUTHERLAND // lambda1
//#define INTERACTION22_SUTHERLAND // lambda2
//#define INTERACTION11_CALOGERO // Dpar / r^2

//#define INTERACTION11_COULOMB
//#define INTERACTION12_COULOMB_ATTRACTION
//c#define INTERACTION12_COULOMB_REPULSION

//#define HARD_SPHERE12 // size a
//#define HARD_SPHERE11 // size aA
//#define HARD_SPHERE22 // size aB

//#define INTERACTION12_SOFT_SPHERE // Ro12, a
//#define INTERACTION11_SOFT_SPHERE // Ro11, aA
//#define INTERACTION22_SOFT_SPHERE // Ro22, aB
//#define INTERACTION11_HAMILTONIAN_MEAN_FIELD // cos(pi*x/L) [HMF]

//#define INTERACTION_SUM_OVER_IMAGES // enable to sum up to convergence all PBC images, sums up to sum_over_images_cut_off_num

//=============================== Jastrow terms ==========================================
#define JASTROW_OPPOSITE_SPIN // loop in Jastrow term or potential energy
#define JASTROW_SAME_SPIN // loop in Jastrow term or potential energy
//#define SYM_OPPOSITE_SPIN // loop in symmetrized terms, product_{i=1}^{Nup} sum_{j=1}^{Ndn}f(r_ij) + product_{j=1}^{Ndn} sum_{i=1}^{Nup}f(r_ij)

//#define SYMMETRIZE_TRIAL_WF11 // enable to use u11(r) -> u(r) + u(L-r)
//#define SYMMETRIZE_TRIAL_WF22 // enable to use u22(r) -> u22(r) + u22(L-r)
//#define SYMMETRIZE_TRIAL_WF12 // enable to use u12(r) -> u12(r) + u12(L-r)

//Jastrow are interpolated
//#define INTERPOLATE_LOG // interpolate u(r) rather than f(r)
//#define INTERPOLATE_LINEAR_WF11 // linear interpolation Jastrow 11 term
//#define INTERPOLATE_LINEAR_WF12 // linear interpolation Jastrow 12 term
//#define INTERPOLATE_LINEAR_WF22 // linear interpolation Jastrow 22 term
//#define INTERPOLATE_LINEAR_ORBITAL // linear interpolation Orbital term
//#define INTERPOLATE_SPLINE_WF11 // spline interpolation
//#define INTERPOLATE_SPLINE_WF12 // spline interpolation
//#define INTERPOLATE_SPLINE_WF22 // spline interpolation
//#define INTERPOLATE_SPLINE_WF12_SYM // spline interpolation

/* define ONLY ONE type of the Jastrow term */
//#define  TRIAL_ABSENT12
//#define  TRIAL_ABSENT11
//#define  TRIAL_ABSENT22
#define  TRIAL22_SAME_AS_11 // if 22 is not implemented, take it as a copy of 11
//#define  TRIAL12_SAME_AS_11 // if 12 is not implemented, take it as a copy of 11
//#define  TRIAL11_SAME_AS_12 // if 11 is not implemented, take it as a copy of 12
//#define  TRIAL22_SAME_AS_12 // if 22 is not implemented, take it as a copy of 12
//#define  TRIAL_EXTERNAL_WF11 // load wf from file containing following 7 columns: x f lnf fp Eloc E EV
//#define  TRIAL_EXTERNAL_WF12 // load wf from file containing following 7 columns: x f lnf fp Eloc E EV
//#define  TRIAL_EXTERNAL_WF22 // load wf from file containing following 7 columns: x f lnf fp Eloc E EV
//#define  TRIAL_TWO_BODY_SCATTERING_NUMERICAL11 // define INTERPOLATE_SPLINE_WF find two body scattering solution and interpolate it with a spline
//#define  TRIAL_TWO_BODY_SCATTERING_NUMERICAL12 // enable JASTROW_RIGHT_BOUNDARY_FROM_BOUND_STATE_SOLUTION if needed, // for 0<r<Rpar12*L/2  define INTERPOLATE_SPLINE_WF find two body scattering solution and interpolate it with a spline
//#define  TRIAL_TWO_BODY_SCATTERING_NUMERICAL12_SYM
//#define  TRIAL_TWO_BODY_SCATTERING_NUMERICAL22 // define INTERPOLATE_SPLINE_WF find two body scattering solution and interpolate it with a spline
//#define  JASTROW_LEFT_BOUNDARY_ZERO_FUNCTION // choose left boundary condition, enable for f(0) = 0; otherwise f'(0) = 0
//#define  JASTROW_RIGHT_BOUNDARY_ZERO_DERIVATIVE
//#define  JASTROW_RIGHT_BOUNDARY_FROM_BOUND_STATE_SOLUTION // (1D,2D,3D no PBC) self-consistent with energy, unless JASTROW_ENERGY_FROM_EPAR is defined
//#define  JASTROW_ENERGY_FROM_EPAR // use Epar parameter instead of two-body energy E<0
//#define  JASTROW_RIGHT_BOUNDARY_PHONONS // Apar,Rpar (1D,2D,3D) r>Rpar : (3D) exp(-Apar/r^2) (2D) exp(-Apar/r) (1D) |sin(pi*x/L)|^{1/Apar} 
//#define  JASTROW_RIGHT_BOUNDARY_INVERSE_EXPONENTIAL // Apar (1D,2D,3D) r>Rpar : f(r) = exp(-Apar/r + const/r^2)
//#define  JASTROW_RIGHT_BOUNDARY_POLARON_GPE // Apar, Bpar (1D,2D,3D) r>Rpar : f(r) = Atrial*(1 + Bpar exp(-Apar r) + Bpar exp( Apar (r - L)))
//#define  JASTROW_DIMENSIONALITY_FROM_DPAR // use Dpar parameter instead of D=1,2,3
//#define  JASTROW_MASS_FROM_MPAR // use Mpar parameter instead of mu=1/2
//#define  TRIAL_ZERO_RANGE_UNITARY /* (3D) BC absent: 1/r PBC: 1/r, r<Rpar, otherwise phonons exp(-c/r^2-c/(L-r)^2)*/ 
//#define  TRIAL_SS_SW /*BEC! 11,22 - fictitious SS, 12 - Gaussian (3D) A sh(kappa r)/r, r<Rpar; B sin(k r+delta)/r, r>Rpar, i.e. Soft Sphere + Square Well (E>0)*/
//#define  TRIAL_PSEUDOPOT_SIMPLE /* no var par */
//#define  TRIAL_PSEUDOPOT_FS /*(3D) A/r |sin(kr + B)|, no variational parameters */ 
//#define  TRIAL_GAUSS1122 //par: beta, gamma: const - gamma [exp(-beta r^2) + exp(-beta (L-r)^2)]
//#define  TRIAL_GAUSS12 // use SYMMETRIZE 1 - gamma exp(-beta r^2), gamma<1
//#define  TRIAL_GAUSSIAN12 // exp(-beta r^2)
//#define  TRIAL_FERMI12 /* Apar/(1. + exp((r-Epar)/T)) */
//#define  TRIAL_FERMI11 /* Apar/(1. + exp((r-Epar)/T)) */
//#define  TRIAL_MATRIX   /* Load trial wavefuncion (matrix) from 'wf2D.in file'*/
//#define  TRIAL_HS11  /*11,22 (3D) HS + HS solution + exponent in 3D (hard sphere), Rpar11 */
//#define  TRIAL_HS12  /*12 (3D) HS + HS solution + exponent in 3D (hard sphere), Rpar12 */
//#define  TRIAL_HS_SIMPLE  /*12 (3D) HardSphere + HardSphere solution, no variational parameters */
//#define  TRIAL_HS_SIMPLE11  /*11,22 (3D) HardSphere + HardSphere solution, no variational parameters */
//#define  TRIAL_HS1D  /* (1D) HS + HS solution + exponent in 1D (hard rod) */
//#define  TRIAL_HS2D_FINITE_ENERGY11 /* (2D) parameter: a<Rpar<L/2 - matching distance to 1, use Rpar=0 for setting Rpar=L/2*/
//#define  TRIAL_HS2D_ZERO_ENERGY_PHONONS11 // (1D,2D,3D) parameter: aA<Rpar<L/2 [2D phonons] f(r) = A ln(r/aA), r<Rpar; B exp(-C/r + D/r^2)
//#define  TRIAL_SOFT_DISKS_2D_ZERO_ENERGY_PHONONS11 /* aA>0, (2D) SD(E=0) parameter 0<Rpar11<1: Io(kappa r), r<Ro; A ln(r/aA), r<Rpar; B exp(-C/r + D/r^2) */
//#define  TRIAL_SOFT_DISKS_2D_ZERO_ENERGY_PHONONS12 /* a>0, (2D) SD(E=0) parameter 0<Rpar12<1: Io(kappa r), r<Ro; A ln(r/aA), r<Rpar; B exp(-C/r + D/r^2) */
//#define  TRIAL_SOFT_DISKS_2D_ZERO_ENERGY_PHONONS22 /* aB>0, (2D) SD(E=0) parameter 0<Rpar11<1: Io(kappa r), r<Ro; A ln(r/aA), r<Rpar; B exp(-C/r + D/r^2) */
//#define  TRIAL_SS    /* (3D) SS + SS solution + exponent in 3D (hard sphere) */
//#define  TRIAL_SS_EXP11 /*(3D) exp(-Bpar11 r^2) + phonons, Rpar11 */
//#define  TRIAL_SS_EXP22 /*(3D) exp(-Bpar22 r^2) + phonons, Rpar22 */
//#define  TRIAL_SS_PHONONS12 /*(3D) SS + phonons, Rpar12, Bpar12 (scattering momentum), a */
//#define  TRIAL_SS_PHONONS11 /*(3D) SS + phonons, Rpar11, Bpar11 (scattering momentum), aA */
//#define  TRIAL_SS_PHONONS22 /*(3D) SS + phonons, Rpar22, Bpar22 (scattering momentum), aB */
//#define  TRIAL_SS_EXP12 /*(3D) exp(-Bpar12 r^2) + phonons, Rpar12 */
//#define  TRIAL_LIM   /* (1D,2D,3D) 1 + a/r  i.e. limit of 3D HS + HS solution */
//#define  TRIAL_POWER /* (1D,3D,3D ) 1 + a/r^Rpar */
//#define  TRIAL_DIPOLE /* (1D) C exp(-(A/x)^B)*/
//#define  TRIAL_SCHIFF_VERLET /*use SYMMETRIZE_TRIAL_WF for PBC (1D-3D):(1D) exp(-(Apar/(n|z|))^Bpar) (2D, hom) exp{-[Apar/(r/L)]^Bpar} (3D) exp(-Apar/r^Bpar)*/
//#define  TRIAL_SCHIFF_VERLET_PHONON /* (1D-3D):(1D) exp(-0.5 (A/r)^B) + phonons from Rpar (2D, hom) exp{-[Apar/(r/L)]^Bpar} (e^-Cr+e^-C(r-L)) (3D) exp(-Apar/r^Bpar), r<Rpar Lhalf, phonons otherwise*/
//#define  TRIAL_SCHIFF_VERLET_PHONON12 /* Apar12, Bpar12, Rpar12*/
//#define  TRIAL_LIEB  /*11,12,22 (1D) cos(k(|z|-Rpar)); for a<0 VMC only! |a| is the node */
//#define  TRIAL_LIEB_PHONON11 /* Rpar11 [L/2]: aA - s-wave length in 11 channel (1D) A cos(k(x-B)) (x<L*Rpar11), sin^a(pi*x/L), i.e. Lieb + phonons mat */
//#define  TRIAL_LIEB_PHONON12 /* Rpar12 [L/2]: aAB=a - s-wave length in 12 channel (1D) A cos(k(x-B)) (x<L*Rpar12), sin^a(pi*x/L), i.e. Lieb + phonons mat */
//#define  TRIAL_PHONON_LUTTINGER11 // (1D) aA,  Kpar11, Rpar11: A cos(k(x-B)) (x<L*Rpar11), sin^a(pi*x/L), i.e. Lieb + phonons, Rpar11 is taken as it is
//#define  TRIAL_PHONON_LUTTINGER12 // (1D) aAB, Kpar12, Rpar12: A cos(k(x-B)) (x<L*Rpar12), sin^a(pi*x/L), i.e. Lieb + phonons, Rpar12 is taken as it is
//#define  TRIAL_PHONON_LUTTINGER_AUTO11 // (1D) aA,  Kpar11 (automatic Rpar11): A cos(k(x-B)) (x<L*Rpar11), sin^a(pi*x/L), i.e. Lieb + phonons
//#define  TRIAL_PHONON_LUTTINGER_AUTO12 // (1D) aAB, Kpar12 (automatic Rpar12): A cos(k(x-B)) (x<L*Rpar11), sin^a(pi*x/L), i.e. Lieb + phonons
//#define  TRIAL_MCGUIRE_PHONON12 // (1D) exp(x/b); (x<Rpar12), sin^a(pi*x/L) otherwise
//#define  TRIAL_MCGUIRE_PHONON11 // (1D) exp(x/aA); (x<Rpar11), sin^a(pi*x/L) otherwise
//#define  TRIAL_MCGUIRE_PBC12 // (1D) exp(x/B) + exp((2*Rpar12-x)/B) + Apar12 - 2.*exp(Rpar12/B), parameters b>0, Apar12>0, 0<Rpar12<L/2: f'(0)/f(0) = 1/b
//#define  TRIAL_MCGUIRE_PBC12_RPAR // (1D,2D,3D) f(r) = exp(-r/a - Apar12 r^2 + Ctrial r^3)/r, Apar12 - decay rate, Rpar12 - matching point to 1 in units of L/2, f'(0)/f(0) = -1/aAB
//#define  TRIAL_MCGUIRE_PBC22_EPAR // (1D) exp(-Epar(r-L/2)) + exp(-Epar(L/2-r))
//#define  TRIAL_LIEB_RENORMALIZED
//#define  TRIAL_SQ_WELL_ZERO_CONST  /*BCS,normal! 12 (normal) (3D) SS sin(K r)/r [r<RoSW], 1+a/r [r<Rpar], 1. + A*(exp(-Bpar*r)+exp(Bpar*(r-L))) [r<L/2] */
//#define  TRIAL_SQ_WELL_TRAP // 12 free state, a<0 (3D) SS sin(K r)/r [r<RoSW], 1+a/r [r<Rpar]
//#define  TRIAL_SQ_WELL_BS_TRAP // 12 bound state, a>0 (3D) SS sin(K r)/r [r<RoSW], exp(-kr)/r no var. par.
//#define  TRIAL_SQ_WELL_BS_LATTICE_POLARON // (3D) Apar12, Bpar12, Rpar12 sin(K r)/r [r<RoSW], exp(-kr)/r [r < Rpar12] exp(-r/Apar12 + const r^Bpar12 + symm)
//#define  TRIAL_SQ_WELL_UNITARITY
//#define  TRIAL_SQUARE_WELL_SMOOTH /*12 (3D) SS+SS(E<0) A sin(Kappa r)/r, B exp(-k r)/r (1-alpha(r-1)^3) */
//#define  TRIAL_SQUARE_WELL_FS /* (3D) SS+SS(E>0) A sin(Kappa r)/r, B sin(k r + delta) */
//#define  TRIAL_SQUARE_WELL_BS /* define symmetrization! (3D) SS+SS(E<0) A sin(Kappa r)/r, B exp(-k r)/r */
//#define  TRIAL_SQUARE_WELL_BS_PHONONS /* (3D) SW + phonons at Rpar12, C1 sin(Kappa r)/r [r<RoSW], C2 exp(-k r)/r, C3 exp(-c/r^2-c/(L-r)^2)*/
//#define  TRIAL_SQUARE_WELL_FS_PHONONS12 /*(3D) SS E>0, k = Bpar12/(2.*PI*Lwf), 0<Rpar12<1*/
//#define  TRIAL_SQUARE_WELL_FS_GPE_POLARON12 // (3D) SS E>0, k = Bpar12/(2.*PI*Lwf), 0<Rpar12<1; const + exp(-r/Apar) + exp(-(L-r)/Apar)
//#define  TRIAL_SQUARE_WELL_FS_PHONONS11 /*(3D) SS E>0, Bpar11, 0<Rpar11<1*/                             \
//#define  TRIAL11_SQUARE_WELL_FS_GAUSS /* (3D) SS+SS(E>0) A sin(Kappa r)/r, B sin(k r + delta) */
//#define  TRIAL11_SQUARE_WELL_FS_P_WAVE /*(3D) p-wave SW (E>0) A[sin(Kr)/Kr-Cos(Kr)]/r, B[sin(kr+delta)/kr-Cos(kr+delta)]/r, no free parameters */
//#define  TRIAL11_SQUARE_WELL_BS_PHONONS /* (3D) SS+phonons at Rpar, C1 sin(Kappa r)/r, C2 exp(-k r)/r, C3 exp(-c/r^2-c/(L-r)^2) */
//#define  TRIAL11_SQ_WELL_ZERO_CONST  /* s-wave solution to SW (3D) SS sin(K r)/r [r<Ro], 1+a/r [r<Rpar], 1. + A*(exp(-Bpar*r)+exp(Bpar*(r-L))) [r<L/2] */
//#define  TRIAL_SH /*(3D) A th(r)/r */
//#define  TRIAL_SQUARE_WELL2D /* (2D) SW(E>0) Yo(kappa' r), Ko(k r) */
//#define  TRIAL_SQUARE_WELL2D_ZERO_ENERGY_PHONONS12 /* (2D) define TRIAL_SET_NEGATIVE_WF_TO_ZERO for setting f(r)=0 if f(r)<0, repulisive branch a>0, SW(E=0) Jo(kappa r), r<Ro; A ln(r/aA), r<Rpar; B exp(-C/r + D/r^2) */
//#define  TRIAL_SET_NEGATIVE_WF_TO_ZERO
//#define  TRIAL_SQUARE_WELL2D_K0_PHONONS12 /* (2D) attractive branch a>0, SW(E=0) Jo(kappa r), r<Ro; A ln(r/aA), r<Rpar; B exp(-C/r + D/r^2) */
//#define  TRIAL_SQUARE_WELL2D_JO_PHONONS12 /* (2D)  a>0, SW(E=0) Jo(kappa r), r<Ro; A ln(r/aA), r<Rpar; B exp(-C/r + D/r^2) */
//#define  TRIAL_ZERO_RANGE_K0_PHONONS12 // (2D) pseudopotential attractive Ko branch a>0, A Ko(r/a), r<Rpar; B exp(-C/r + D/r^2)
//#define  TRIAL_ZERO_RANGE_K0_EXP12 // (2D) pseudopotential attractive Ko branch a>0, A Ko(r/a), r<Rpar; B exp(-C/r + D/r^2)
//#define  TRIAL_ZERO_RANGE_K0_GPE_POLARON // (2D) pseudopotential attractive Ko branch a>0, A Ko(r/a), r<Rpar; const + exp(-r/Apar) + exp(-(L-r)/Apar)
//#define  TRIAL_ZERO_RANGE_UNITARY_GPE_POLARON // (3D) pseudopotential a=inf, A/r, r<Rpar; const + exp(-r/Apar) + exp(-(L-r)/Apar)
//#define  TRIAL_ZERO_RANGE_LOG_PHONONS12 // (2D) pseudopotential repulsive ln branch,  A ln(r/a), r<Rpar; B exp(-C/r + D/r^2)
//#define  TRIAL_ZERO_RANGE_LOG_GPE_POLARON // (2D) pseudopotential repulsive ln branch,  A ln(r/a), r<Rpar; const + exp(-r/Apar) + exp(-(L-r)/Apar)
//#define  TRIAL_TONKS11 /*aA (1D, edit for 2D and 3D) Tonks w.f. |sin(k(x-a))|, no var. par. */
//#define  TRIAL_TONKS12 /*aAB (1D) Tonks w.f. |sin(k(x-a))|, f'(L/2) = 0, gives correct scattering length conditions f'(0)/f(0) = -1/a in k->0 limit*/
//#define  TRIAL_TONKS22 /*AB (1D) Tonks w.f. |sin(k(x-a))|, f'(L/2) = 0, gives correct scattering length conditions f'(0)/f(0) = -1/a in k->0 limit*/
//#define  TRIAL_DARK_SOLITON12 // (1D) th(Apar12 crd(x,L)), Apar12 in units of xi where xi is the healing length
//#define  TRIAL_SUTHERLAND12 /* (1D) sin^lambda12 (pi*x/L), between up and down */
//#define  TRIAL_SUTHERLAND11 /* (1D) sin^lambda1 (pi*x/L) */
//#define  TRIAL_SUTHERLAND22 /* (1D) sin^lambda2 (pi*x/L) */
//#define  TRIAL_SUTHERLAND_CHANGING_LAMBDA1122 /* (1D) sin[PI x/L]^(1+Apar r + Bpar r^2)
//#define  TRIAL_COULOMB12 // (1D, 2D, 3D) f(r) = exp(-r/2ao)
//#define  TRIAL_COULOMB_3D_PHONON12 /*12: (3D) exp(-r/2ao), r<Rpar, exp(-c/r^2-c/(L-r)^2), r>Rpar*/
//#define  TRIAL_COULOMB12ex // (1D) f(r) = r exp(-r/2ao)
//#define  TRIAL_COULOMB_3D_PHONON11 /*11,22: (3D) I_1(2r^1/2)/r^1/2, r<Rpar, exp(-c/r^2-c/(L-r)^2), r>Rpar*/
//c#define  TRIAL_COULOMB_1D_PHONON /*11,22: (1D) r^1/2 I_1(2r^1/2), r<Rpar, sin^beta(pi r/L), r>Rpar*/
//c#define  TRIAL_COULOMB_1D_PHONON12  /*12: (1D) r^1/2 I_1(2r^1/2), r<Rpar, sin^beta(pi r/L), r>Rpar*/
//#define  TRIAL_COULOMB12exPHONON // (1D) previous with Rpar2 var par
//#define  TRIAL_CH2_PHONON11 // 2/ch^2(r) potential, Rpar11
//#define  TRIAL_CH2_PHONON12 // 2/ch^2(r) potential, Rpar12
//#define  TRIAL_CH2_TRAP12 // 2/ch^2(r) potential
//#define  TRIAL_PSEUDOPOT_BS /* (3D) A exp(-r/a)/r */
//#define  TRIAL_CALOGERO11 // (1D,2D,3D) |r|^Rpar11
//#define  TRIAL_CALOGERO12 // (1D,2D,3D) |r|^Rpar12
//#define  TRIAL_MCGUIRE11 // (1D,3D) 1D: f(r) = exp(-r/aA), 3D: f(r) = exp(-r/aA)/r, bound state of a pseudopotential bound state of a pseudopotential, note that symmetrization changes b.c.
#define  TRIAL_MCGUIRE12 // (1D,3D) 1D: f(r) = exp(-r/aAB), 3D: f(r) = exp(-r/aAB)/r, bound state of a pseudopotential bound state of a pseudopotential, note that symmetrization changes b.c.
//#define  TRIAL_MCGUIRE12_GAUSSIAN1D // (1D) exp(-r / Atrial) + Apar*exp(-(1/2) r^2 / Bpar^2), Atrial defined from the s-wave scattering length a; alters second derivative but not the cusp
//#define  TRIAL_MCGUIRE12_GAUSSIAN3D // (3D) exp(-r/aAB - Apar12 r^2) / r
//#define  TRIAL_MCGUIRE12_FERMI // (1D) Apar, T, Epar
//#define  TRIAL_MCGUIRE11_DIMER // (1D) Apar, Bpar, exp{-r/aA + sqrt(Bpar*a*a+r*r)*(1./aA + 1./a - 1./(2.*Apar))}
//#define  TRIAL_MCGUIRE11_SH // (1D) Apar: dimer-dimer scattering length, sh[r*(1/a-1/2Apar)]-aA*(1/a-1/2Apar)]
//#define  TRIAL_MCGUIRE22_SH // (1D) Apar: dimer-dimer scattering length, sh[r*(1/a-1/2Apar)]-aB*(1/a-1/2Apar)]
#define  TRIAL_TONKS_TRAP11 //(1D,2D,3D) f(r) = |r-aA|, enable HS11 to have f(r)=0 for r<aA
//#define  TRIAL_TONKS_TRAP22 //(1D,2D,3D) f(r) = |r-aB|, enable HS11 to have f(r)=0 for r<aB
//#define  TRIAL_TONKS_TRAP12 //(1D,2D,3D) f(r) = |r-aAB|, enable HS12 to have f(r)=0 for r<aAB
//#define  TRIAL_TONKS_TRAP1122 //(1D,2D,3D) f(r) = |r-b|, enable HS11 to have f(r)=0 for r<b, define TRIAL22_SAME_AS_11 
//#define  TRIAL_DIPOLE_Ko11 /*(2D) Atrial Ko(2/sqrt(r)), r<Rpar11*L/2; Btrial*exp(-Ctrial*(1./r+1./(L-r))) r>Rpar11*Lhalf*/
//#define  TRIAL_DIPOLE_Ko22 /*(2D) Atrial Ko(2/sqrt(r)), r<Rpar22*L/2; Btrial*exp(-Ctrial*(1./r+1./(L-r))) r>Rpar22*Lhalf*/
//#define  TRIAL_DIPOLE_Ko_TRAP1122 /*(2D) Ko(2/sqrt(r))
//#define  TRIAL_DIPOLES_PHONON11 /* (2D):  Atrial*exp(-2./sqrt(r)), r<Rpar*Lhalf, Btrial*exp(-Ctrial*(1./r+1./(L-r))) r>Rpar*Lhalf */
//#define  TRIAL_DIPOLE_GAUSS12 /*(2D) attractive 12: Atrial exp(-Apar r^2), r<Rpar12*L/2; Btrial*exp(-Ctrial*(1./r+1./(Bpar12-r))), r<Bpar12/2; 1, r<L/2
//#define  TRIAL_SCHIFF_VERLET11_TRAP /* (1D,2D,3D) exp(-Apar / r^Bpar) */
//#define  TRIAL_ION_12

//#define  TRIAL_SYM_GAUSS /* exp( -a*r^2 / (1 + b*r) )   */
//#define  TRIAL_SYM_FERMI /* 1 / ( 1 + exp( (r - b) / a ))   */
//#define  TRIAL_SYM_YAROSLAV /* 1 - exp(- ( a / r)^b )   */
//#define  TRIAL_SUM_EXPONENTIALS /* c*exp(-ar)+c2*exp(-2ar)+c3*exp(-3ar)+c4*exp(-4ar)+c5*exp(-5ax)+c6*exp(-6ax)+c7*exp(-a1(x-b1)^2)+c8*exp(-a3(x-b2)^2)   */
//#define  TRIAL_SYM_TWO_BODY /* */
//#define  TRIAL_SYM_MCGUIRE // (1D,3D) 1D: f(r) = exp(-r/aAB), 3D: f(r) = exp(-r/aAB)/r, bound state of a pseudopotential bound state of a pseudopotential
//#define  TRIAL_SYM_MCGUIRE12_A2 // f(x) = exp[-x*(1+x)/(aAB+Ro*x)]

//============================ determinant orbitals ======================================
/* uncomment to restrict summation in the orbitals over pairs only to r_ij < L/2 (normally commented)*/
//#define L2_CHECK
// orbital goes to a constant value (normally uncommented)
//#define L2_ORBITAL_CONST

//Determinant orbitals are interpolated
//#define INTERPOLATE_LINEAR_ORBITAL
//#define INTERPOLATE_SPLINE_ORBITAL

//#define SYMMETRIZE_ORBITAL_WF // enable to use f(r) -> f(r) + f(L-r)

/* use sum of plane waves as orbital (reproduces an ideal gas) of the Det*/
// normal gas
//norm
//#define ORBITAL_ISOTROPIC // add orbitalF()
//#define ORBITAL_ANISOTROPIC_RELATIVE // define to add plane waves or similar terms (old ORBITAL_DESCRETE_BCS) with the weight Opar/ Ntot : normal FG (BCS! IFG!) 
//#define ORBITAL_DESCRETE_COULOMB // 2body solution summed over images

/* and choose s-wave contribution to the orbital of the Det.*/
// superfluid gas
#define ORBITAL_ABSENT
//#define ORBITAL_EXTERNAL_WF // load from file
//#define TRIAL_TWO_BODY_SCATTERING_NUMERICAL_ORBITAL // Solve scattering problem by shooting method
//#define ORBITAL_SQ_WELL_BOUND_STATE // superfluid FG (BEC!): sin(Ar)/r; exp{-kr}/r 
//#define ORBITAL_SQ_WELL_BOUND_STATE_DECAY // sin(Ar)/r; exp{-kr}/r; 1 - a(exp{-br} + exp{b(r-L)})
//s#define ORBITAL_EXPONENT_ALPHA // (1. + OEA_gamma * r) * exp(-Opar * r) + (1 + OEA_gamma * (L - r))*exp(-Opar * (L - r)) - OEA_B
//#define ORBITAL_DECAY //BCS! side A*(exp(-B*r/L)+exp(-B*(L-r)/L))
//#define ORBITAL_IG // ideal gas wf. from the BCS theory 
//#define ORBITAL_SQ_WELL_ZERO_ENERGY /*A sin(kappa r)/r, B(1+a/r)*/
//#define ORBITAL_SQ_WELL_ZERO_CONST  /*A sin(kappa r)/r, B(1+a/r), 1+C exp(-Apar r)*/
//#define ORBITAL_SQ_WELL_FREE_STATE
//#define ORBITAL_SQ_WELL_FREE_STATE_BCS_SIDE

//#define ORBITAL_SQ_WELL_MIXTURE
//#define ORBITAL_PSEUDOPOTENTIAL
//#define ORBITAL_PSEUDOPOTENTIAL_BOUND_STATE
//#define ORBITAL_PSEUDOPOTENTIAL_BOUND_STATE_UNITARY
//#define ORBITAL_PSEUDOPOTENTIAL_MIXTURE
//#define ORBITAL_CH2 // 2/ch^2(r) potential, 2-body scat. solution
//#define ORBITAL_CH2_EXP // 2/ch^2(r) potential, Bpar, Cpar - var. parameters
//#define ORBITAL_CH2_PHONON // 2/ch^2(r) potential

//#define ORBITAL_SQUARE_WELL2D // (2D) SW+SW(E>0) Jo(kappa' r), Ko(k r)
//#define ORBITAL_SQUARE_WELL2D_SIN // (2D) SW+SW(E>0) Jo(kappa' r), Ko(k r), sin, Cpar
//#define ORBITAL_SQUARE_WELL2D_PHONON // (2D) SW+SW(E>0) Jo(kappa' r), Ko(k r), exp(-const/r), symm
//t#define ORBITAL_HYDROGEN_1S // (3D) exp(-r/2ao)

// define (if necessary) the external confinement
//#define LATTICE
// define (if necessary) presence of crystallic one-body terms
//#define CRYSTAL // and specify:
//#define CRYSTAL_SYMM_SUM_PARTICLE //! symmetric w.f. prod_{j,Nc} sum_{i=1,N} exp(- alpha (r_i - r_j^c)^2)
#define CRYSTAL_NONSYMMETRIC // nonsymmetric w.f. for the crystal
//#define CRYSTAL_SYMM_SUM_LATTICE // symmetric w.f. prod_{i=1,N} sum_{j,Nc} exp(- alpha (r_i - r_j^c)^2)
//#define CRYSTAL_WIDTH_ARRAY // define to treat Crystal.Rx[i] Ry[i] Rz[i] w[i] as parameters loaded from crystal coordinates file (x,y,z,Rx,Ry,Rz,w)
//#define LATTICE_3D_CUBIC_TWICE // default 3D lattice NaCl type structure
//#define LATTICE_3D_CUBIC_MULTIPLE_OCCUPATION

/* define statistics */
//#define FERMIONS // evaluate determinants
//#define OBDM_BOSONS // use Bose-Fermi mapping for calculation of Bose and Fermi OBDM and n(k) in 1D

// polarized orbitals:
//#define ORBITAL_POLARIZED_DESCRETE_BCS  // for Nup>Ndn uses ORBITAL_ANISOTROPIC_RELATIVE
//#define ORBITAL_POLARIZED_3BODY  // 2 elements: 1) cos(2pi(X-Y)/L)  2) 0
//#define ORBITAL_POLARIZED_ALPHA  // for Nup>Ndn uses additional orbitals (1, cos(_2PI*z/L *((alpha+1)/2)), sin ...)
//#define ORBITAL_3BODY // with ORBITAL_POLARIZED_ALPHA, redefine orbitals: use cos(2 pi r/L); plane waves otherwise (1, cos, ...)

//#define ORBITAL_ANISOTROPIC_ABSOLUTE // in 3-body case, use absolute (not relative) coordinates
//#define ORBITAL_POLARIZED_3BODY_WITH_ZERO_MOMENTUM // normal w.f. for 3 particles, enable ORBITAL_ANISOTROPIC_ABSOLUTE

//#define CENTER_OF_MASS_IS_NOT_MOVED
//#define CENTER_OF_MASS_SUBTRACTED_IN_RADIAL_DISTRIBUTION // radial distribution is measured with respect to CM

#define TRIAL_1D
//#define TRIAL_2D
//#define TRIAL_3D

//#define RESCALE_ENERGY_3D_HS // unit of energy is h2/2ma2 instead of h2/ma2

/* define ONLY ONE of following boundary conditions */
#define BC_ABSENT
/* no boundary conditions */
//#define BC_1DPBC     /* boundary condition in z direction */
//#define BC_1DPBC_Z   // also define BC_1DPBC
//#define BC_BILAYER /* 2D system */
//#define BC_2DPBC_SQUARE     /* boundary condition in (X,Y) plane */
//#define BC_2DPBC_HEXAGON
//#define BC_3DPBC_CUBE   /* PBC in all directions */
//#define BC_3DPBC_TRUNCATED_OCTAHEDRON /* overlap between cube and |x-L/2|+|y-L/2|+|z-L/2| < 3L/4 */
//#define BC_XY_PBC_Z_FREE   /* PBC in (x,y) and trap in (z) */

//#define TRAP_POTENTIAL // harmonic oscillator in external energy omega_x_up, omega_y_up, omega_z_up, omega_x_dn, omega_y_dn, omega_z_dn
//#define TRAP_DOUBLE_WELL
//#define UP_PARTICLES_PINNED_TO_A_CHAIN // ensures that up particles stay in a linear chain, enable INFINITE_MASS_COMPONENT_UP
//#define TRAP_LINEAR_CHAIN_OF_HARMONIC_OSCILLATORS_UP // external potential as a linear chain
//#define COORDINATES_BORN_OPPENHEIMER_2COMPONENT // generates two particles in down component separated by Dpar*L/2

//#define ONE_BODY_TRIAL_TRAP // Gaussian w.f. alpha_x * x*x + alpha_y * y*y + alpha_z * z*z;
#define ONE_BODY_TRIAL_TRAP_DOUBLE_GAUSSIAN // Gaussian w.f. exp(-alpha_z_up (r-rGauss_up)^2) + exp(-alpha_z_up (r+rGauss_up)^2); rGauss_dn for down species
//#define ONE_BODY_TRIAL_LINEAR_CHAIN_OF_HARMONIC_OSCILLATORS_UP // Gaussians (alpha_x_up,alpha_y_up,alpha_z_up) located at ((i+0.5) L/Nup,0,0) [pbc] and 0; +/- L/Nup ... [free bc] for UP particles
//#define ONE_BODY_TRIAL_LINEAR_CHAIN_OF_HARMONIC_OSCILLATORS_DN // Gaussians (alpha_x_dn,alpha_y_dn,alpha_z_dn) located at ((i+0.5) L/Nup,0,0) [pbc] and 0; +/- L/Ndn ... [free bc] for DN particles
//#define ONE_BODY2_TRIAL_1D_WAVE // down particles: |cos(one_body_momentum_i 2 pi z/L)|, where one_body_momentum_i = 0,1,2,...
//#define ONE_BODY_TRIAL_1D_WAVE_2COMP_INPHASE // 1 + Srecoil cos (one_body_momentum_i 2 pi z/L) for {up/dn}
//#define ONE_BODY_TRIAL_1D_WAVE_2COMP_OUT_OF_PHASE // 1 + Srecoil {cos/sin} (one_body_momentum_i 2 pi z/L) for {up/dn}
//#define ONE_BODY_TRIAL_3D_WAVE_2COMP_OUT_OF_PHASE // 1 + Srecoil {cos/sin} (one_body_momentum_i 2 pi {x,y,z}/L) for {up/dn}
//#define ONE_BODY_TRIAL_1D_WAVE_2COMP_OUT_OF_PHASE_KPAR // 1 + Srecoil {cos/sin}^Kpar (one_body_momentum_i 2 pi z/L) for {up/dn}
//#define ONE_BODY_TRIAL_ABC // antiperiodic boundary condition: sin(pi x /L)^alpha_latt
//#define ONE_BODY_TRIAL_FERMI // (1D) 1/(1 + exp((r-Epar)/T))
//#define ONE_BODY_TRIAL_GP_LHY_1D
#define ONE_BODY_TRIAL_SIN // one-body term: [sin(kL*x)^2 + sin(kL*y)^2 + sin(kL*z)^2]^alpha_R with kL = PI*Nlattice/L

//#define OPTICAL_LATTICE // experimental values, Srecoil*0.5*kL*kL*(cos(kL*x)*cos(kL*x) + cos(kL*y)*cos(kL*y) + cos(kL*z)*cos(kL*z));
#define VEXT_COS2 // (1D,2D,3D) cos^2 external lattice potential, Srecoil*(Cos(kL*x)*Cos(kL*x) + Cos(kL*y)*Cos(kL*y) + Cos(kL*z)*Cos(kL*z))
//#define OPTICAL_LATTICE_WITH_IMPURITY // different recoil energy for the lattice (define also OPTICAL_LATTICE)
//#define OPTICAL_LATTICE_WITH_IMPURITY_HARMONIC_TERM

//#define VORTEX // external potential, (1/2)c^2r^2, with up//dn vortices separated by lambda*L
//#define ONE_BODY_VORTEX // f1(r) = sin(pi*r/L)^Dpar

/* disable checks of the continuity of the trial wf.*/
//#define DONT_CHECK_CONTINUITY_OF_WF
#define DONT_CHECK_CONTINUITY_OF_ELOC

// chose one of the following for SD_MEASURE
//#define SD_MEASURE_GLOBAL_WINDING // superfluid density of both components
#define SD_MEASURE_ONLY_SPIN_DOWN // effective mass of single (down) impurity
//#define SD_MEASURE_ONLY_Z_DIRECTION
//#define SD_MEASURE_W1_MINUS_W2 // measure difference between two winding numbers
//#define SD_MEASURE_W1_TIMES_W2 // measure the product of two two winding numbers
//#define SD_MEASURE_DIFFUSION_COEFFICIENT

//#define PD_MEASURE_ONLY_Z_DIRECTION

/*#ifdef INTERACTION_LENNARD_JONES
#  define energy_unit T_lj
#else
#  define energy_unit 1.
#endif*/

#define energy_unit 1. // 1 means that the energy is given per Nup
//#define energy_unit 0.5 // 0.5 means that the energy is given per pair
//#define energy_unit 16.079147954821714 // He3 units, i.e. [hbar^2 / (mHe3 A^2)] / (kB K) = 16.
//#define energy_unit 12.115916747700853 // He4 units
// 3D
//#define energy_unit 0.8082916323275207  // 0.5/Eid @ n=1e-1
//#define energy_unit 3.7517574149140653  // 0.5/Eid @ n=1e-2
//#define energy_unit 17.414115323489067  // 0.5/Eid @ n=1e-3
//#define energy_unit 80.82916323275207  // 0.5/Eid @ n=1e-4
//#define energy_unit 375.1757414914063  // 0.5/Eid @ n=1e-5
//#define energy_unit 1741.4115323489068 // 0.5/Eid @ n=1e-6
//#define energy_unit 8082.916323275201  // 0.5/Eid @ n=1e-7
//#define energy_unit 37517.57414914063  // 0.5/Eid @ n=1e-8

// 2D
//#define energy_unit 318309.8861837907  // 0.5/Eid @ n=1e-6
//#define energy_unit 3183.098861837907  // 0.5/Eid @ n=1e-4
//#define energy_unit 31.83098861837907  // 0.5/Eid @ n=1e-2
//#define energy_unit 0.3183098861837907 // 0.5/Eid @ n=1

#define MEASURE_SK11 // enable to sum contributions to sk11 from A+A
#define MEASURE_SK22 // enable to sum contributions to sk11 from B+B

#define INPATH  "./"
#define OUTPATH "./"

#ifndef TRIAL_1D
#  ifndef TRIAL_2D
#    define TRIAL_3D
#  endif
#endif

#ifdef TRIAL_TONKS
#  define DONT_CHECK_CONTINUITY_OF_WF
#endif

#ifdef TRIAL_LIEB
#  define DONT_CHECK_CONTINUITY_OF_ELOC
#endif

#ifdef TRIAL_DIPOLE
#  define DONT_CHECK_CONTINUITY_OF_WF
#  define DONT_CHECK_CONTINUITY_OF_ELOC
#endif

#ifdef BC_ABSENT
#  define boundary NO_BOUNDARY_CONDITIONS
#  ifndef CENTER_OF_MASS_IS_NOT_MOVED
//#    define EXTERNAL_POTENTIAL
#  endif
#endif

#ifdef BC_1DPBC_Z
#  define BC_1DPBC
#endif

#ifdef BC_1DPBC
#  define boundary ONE_BOUNDARY_CONDITION
#endif

#ifdef BC_BILAYER
#  define boundary BILAYER_BOUNDARY_CONDITIONS
#endif

#ifdef BC_2DPBC
#  define boundary TWO_BOUNDARY_CONDITIONS
#endif

#ifdef BC_3DPBC  
#  define boundary THREE_BOUNDARY_CONDITIONS
#endif

#ifdef BC_3DPBC
#  define measure_RDz OFF
#else
#  define measure_RDz ON
#endif

#ifdef BC_ABSENT
//#  define ONE_BODY_TRIAL_TERMS
#endif

#ifdef CRYSTAL
#  define ONE_BODY_TRIAL_TERMS
#endif

#ifdef OPTICAL_LATTICE
#  define EXTERNAL_POTENTIAL
#endif

#ifdef VORTEX
#  define EXTERNAL_POTENTIAL
#endif

#ifdef VEXT_COS2
#  define EXTERNAL_POTENTIAL
#endif

#define VARIATIONAL 0
#define DIFFUSION 1
#define CLASSICAL 2
#define MOLECULAR_DYNAMICS 4 // SmartMC= 0 fixed temperature, SmartMC= 1 fixed energy
#define PIMC 5
#define PIGS 6

#define NO_BOUNDARY_CONDITIONS 0
#define THREE_BOUNDARY_CONDITIONS 3
#define TWO_BOUNDARY_CONDITIONS 2
#define ONE_BOUNDARY_CONDITION 1
#define BILAYER_BOUNDARY_CONDITIONS 4

#define DMC_QUADRATIC 0
#define DMC_LINEAR_GAUSSIAN 1
#define DMC_LINEAR_MIDDLE 2
#define DMC_PSEUDOPOTENTIAL 3
#define DMC_LINEAR_METROPOLIS 4
#define DMC_LINEAR_METROPOLIS_MIDDLE 5
#define DMC_PSEUDOPOTENTIAL_REWEIGHTING 6

#define VMC_MOVE_ALL 0
#define VMC_MOVE_ONE 1

#define PIGS_PRIMITIVE 0
#define PIGS_PSEUDOPOTENTIAL 1
#define PIGS_PSEUDOPOTENTIAL_OBDM 2

#ifdef CRYSTAL
#  ifndef CRYSTAL_NONSYMMETRIC
#    ifndef CRYSTAL_SYMM_SUM_LATTICE 
#      ifndef CRYSTAL_SYMM_SUM_PARTICLE
#        error "define type of crystal one body terms!"
#      endif
#    endif
#  endif
#endif

#ifdef JASTROW_ABSENT
#  undef JASTROW_SAME_SPIN
#  undef JASTROW_OPPOSITE_SPIN
#endif

#define PI 3.14159265358979
#include <time.h>

#define RAN2_SEED 1023
int AllocateMemory(void);

#ifdef LATTICE
#  include "crystal.h"
#else
#  undef CRYSTAL_SYMM_SUM_PARTICLE
#endif

#ifdef DATATYPE_FLOAT
typedef float DOUBLE;
#  define LF "f"
#  define LE "e"
#  define LG "g"
#  define Cos cosf
#  define Sin sinf
#  define Tg tanf
#  define Arctg atanf
#  define Log logf
#  define Exp expf
#  define Sqrt sqrtf
#endif

#ifdef DATATYPE_DOUBLE
//typedef double DOUBLE;
#  define DOUBLE double
#  define LF "lf"
#  define LE "le"
#  define LG "lg"
#  define Cos cos
#  define Sin sin
#  define Tg tan
#  define Arctg atan
#  define Log log
#  define Exp exp
#  define Sqrt sqrt
#endif

#ifdef DATATYPE_LONG_DOUBLE
#  define DOUBLE long double
#  define LF "Lf"
#  define LE "Le"
#  define LG "Lg"
#  define Cos cosl
#  define Sin sinl
#  define Tg tanl
#  define Arctg atanl
#  define Log logl
#  define Exp expl
#  define Sqrt sqrtl
#endif

DOUBLE orbitalF(DOUBLE r); // introduce function before defining substitution rule
DOUBLE orbitalFp(DOUBLE r);
DOUBLE orbitalEloc(DOUBLE r);

#ifdef INTERPOLATE_LINEAR_ORBITAL // linear interpolation
  #define orbitalF orbitalGridF
  #define orbitalFp orbitalGridFp
  #define orbitalEloc orbitalGridEloc
#else // no interpolation
#ifdef INTERPOLATE_SPLINE_ORBITAL // spline interpolation
  #define orbitalF(r)     InterpolateSplineF(&Gorbital,r)
  #define orbitalFp(r)    InterpolateSplineFp(&Gorbital,r)
  #define orbitalEloc(r)  InterpolateSplineEloc(&Gorbital,r)
#else // no interpolation
#ifndef SYMMETRIZE_ORBITAL_WF // exact
  #define orbitalF    orbitalExactF
  #define orbitalFp   orbitalExactFp
  #define orbitalEloc orbitalExactEloc
#else // symmetrize
  #define orbitalF(r) ((r>Lhalf)?(1.):(1.- 2.*orbitalExactF(Lhalf) + orbitalExactF(r)+orbitalExactF(L-r)))
  #define orbitalFp(r) ((r>Lhalf)?(0.):(orbitalExactFp(r)-orbitalExactFp(L-r)))
  #define orbitalEloc(r) ((r>Lhalf)?(0.):(orbitalExactEloc(r)+orbitalExactEloc(L-r) + 2./r*orbitalExactEloc(L-r)*L/(L-r)))
#endif
#endif
#endif

// decide what is InterpolateU12

#ifdef JASTROW_RIGHT_BOUNDARY_PHONONS12
#  define INTERPOLATE_DEFINED_EXPLICITLY12
#endif

#ifdef INTERPOLATE_DEFINED_EXPLICITLY12
  DOUBLE InterpolateU12(struct Grid *Grid, DOUBLE r);
  DOUBLE InterpolateFp12(struct Grid *Grid, DOUBLE r);
  DOUBLE InterpolateE12(struct Grid *Grid, DOUBLE r);
#else
#ifdef INTERPOLATE_LINEAR_WF
  #define InterpolateU12 InterpolateGridU
  #define InterpolateFp12 InterpolateGridFp
  #define InterpolateE12 InterpolateGridE
#else // no linear interp.
#ifdef INTERPOLATE_SPLINE_WF12
  #define InterpolateU12(G,r)  InterpolateSplineU(G,r)
  #define InterpolateFp12(G,r) InterpolateSplineFp(G,r)
  #define InterpolateE12(G,r)  InterpolateSplineE(G,r)
#else //no linear, no spline interpolation

#ifndef SYMMETRIZE_TRIAL_WF12
  #define InterpolateU12(G,r) InterpolateExactU12(r)
  #define InterpolateFp12(G,r) InterpolateExactFp12(r)
  #define InterpolateE12(G,r) InterpolateExactE12(r)
#else // SYMMETRIZE_TRIAL_WF12
#define InterpolateU12(Grid,r) ((r>Lhalf)?(0):(InterpolateExactU12(r)+InterpolateExactU12(L-r)))
#define InterpolateFp12(Grid,r) ((r>Lhalf)?(0):(InterpolateExactFp12(r)-InterpolateExactFp12(L-r)))
#ifdef TRIAL_1D
#define InterpolateE12(Grid,r) ((r>Lhalf)?(0):(InterpolateExactE12(r) + InterpolateExactE12(L-r)))
#endif
#ifdef TRIAL_2D
#define InterpolateE12(Grid,r) ((r>Lhalf)?(0):(InterpolateExactE12(r) + InterpolateExactE12(L-r) + 1./r*InterpolateExactFp12(L-r)*L/(L-r)))
#endif
#ifdef TRIAL_3D
#define  InterpolateE12(Grid,r) ((r>Lhalf)?(0):(InterpolateExactE12(r) + InterpolateExactE12(L-r) + 2./r*InterpolateExactFp12(L-r)*L/(L-r)))
#endif
#endif

#endif // end SYMMETRIZE_TRIAL_WF
#endif // decide what is InterpolateU12
#endif // INTERPOLATE_DEFINED_EXPLICITLY12

// decide what is InterpolateU11
#ifdef INTERPOLATE_LINEAR_WF11
  #define InterpolateU11 InterpolateGridU
  #define InterpolateFp11 InterpolateGridFp
  #define InterpolateE11 InterpolateGridE
#else // no linear interp.
#ifdef INTERPOLATE_SPLINE_WF11
  #define InterpolateU11(G,r)  InterpolateSplineU(G,r)
  #define InterpolateFp11(G,r) InterpolateSplineFp(G,r)
  #define InterpolateE11(G,r)  InterpolateSplineE(G,r)
#else //no linear, no spline interpolation
#ifndef SYMMETRIZE_TRIAL_WF11
#define InterpolateU11(G,r) InterpolateExactU11(r)
#define InterpolateFp11(G,r) InterpolateExactFp11(r)
#define InterpolateE11(G,r) InterpolateExactE11(r)
#else // SYMMETRIZE_TRIAL_WF11
#define InterpolateU11(Grid,r) ((r>Lhalf)?(0):(InterpolateExactU11(r)+InterpolateExactU11(L-r)))
#define InterpolateFp11(Grid,r) ((r>Lhalf)?(0):(InterpolateExactFp11(r)-InterpolateExactFp11(L-r)))
#ifdef TRIAL_1D
#define InterpolateE11(Grid,r) ((r>Lhalf)?(0):(InterpolateExactE11(r) + InterpolateExactE11(L-r)))
#endif
#ifdef TRIAL_2D
#define InterpolateE11(Grid,r) ((r>Lhalf)?(0):(InterpolateExactE11(r) + InterpolateExactE11(L-r) + 1./r*InterpolateExactFp11(L-r)*L/(L-r)))
#endif
#ifdef TRIAL_3D
#define  InterpolateE11(Grid,r) ((r>Lhalf)?(0):(InterpolateExactE11(r) + InterpolateExactE11(L-r) + 2./r*InterpolateExactFp11(L-r)*L/(L-r)))
#endif
#endif
#endif// end SYMMETRIZE_TRIAL_WF
#endif // decide what is InterpolateU11

// decide what is InterpolateU22
#ifdef INTERPOLATE_LINEAR_WF22
  #define InterpolateU InterpolateGridU
  #define InterpolateFp InterpolateGridFp
  #define InterpolateE InterpolateGridE
#else // no linear interp.
#ifdef INTERPOLATE_SPLINE_WF22
  #define InterpolateU22(G,r)  InterpolateSplineU(G,r)
  #define InterpolateFp22(G,r) InterpolateSplineFp(G,r)
  #define InterpolateE22(G,r)  InterpolateSplineE(G,r)
#else //no linear, no spline interpolation
#ifndef SYMMETRIZE_TRIAL_WF22
#define InterpolateU22(G,r) InterpolateExactU22(r)
#define InterpolateFp22(G,r) InterpolateExactFp22(r)
#define InterpolateE22(G,r) InterpolateExactE22(r)
#else // SYMMETRIZE_TRIAL_WF22
#define InterpolateU22(Grid,r) ((r>Lhalf)?(0):(InterpolateExactU22(r)+InterpolateExactU22(L-r)))
#define InterpolateFp22(Grid,r) ((r>Lhalf)?(0):(InterpolateExactFp22(r)-InterpolateExactFp22(L-r)))
#ifdef TRIAL_1D
#define InterpolateE22(Grid,r) ((r>Lhalf)?(0):(InterpolateExactE22(r) + InterpolateExactE22(L-r)))
#endif
#ifdef TRIAL_2D
#define InterpolateE22(Grid,r) ((r>Lhalf)?(0):(InterpolateExactE22(r) + InterpolateExactE22(L-r) + 1./r*InterpolateExactFp22(L-r)*L/(L-r)))
#endif
#ifdef TRIAL_3D
#define  InterpolateE22(Grid,r) (r>Lhalf)?(0):(InterpolateExactE22(r) + InterpolateExactE22(L-r) + 2./r*InterpolateExactFp22(L-r)*L/(L-r))
#endif
#endif// end SYMMETRIZE_TRIAL_WF
#endif
#endif // decide what is InterpolateU22

#ifdef INTERPOLATE_SPLINE_WF12_SYM
#  define InterpolateSYMF(G,r) InterpolateSplinef(G,r)
#  define InterpolateSYMFp(G,r) InterpolateSplinefp(G,r)
#  define InterpolateSYME(G,r) InterpolateSplinefpp(G,r)
#else // no spline interpolation
#  define InterpolateSYMF(G,r) InterpolateExactSymF(r)
#  define InterpolateSYMFp(G,r) InterpolateExactSymFp(r)
#  define InterpolateSYME(G,r) InterpolateExactSymE(r)
#endif

#ifdef SCALABLE
#  define LUinv LUinvSparse
#else
#ifdef USE_LAPACK
#  define LUinv LUinvLapack
#else
#  define LUinv LUinvFull
#endif
#endif

#ifdef TRIAL_1D
#  define DIMENSION 1
#endif
#ifdef TRIAL_2D
#  define DIMENSION 2
#endif
#ifdef TRIAL_3D
#  define DIMENSION 3
#endif

#ifdef BC_3DPBC_CUBE
#  define boundary THREE_BOUNDARY_CONDITIONS
#  define BC_3DPBC
#  define BC_PBC_X
#  define BC_PBC_Y
#  define BC_PBC_Z
#endif

#ifdef BC_3DPBC_TRUNCATED_OCTAHEDRON
#  define boundary THREE_BOUNDARY_CONDITIONS
#  define BC_3DPBC
#  define BC_PBC_X
#  define BC_PBC_Y
#  define BC_PBC_Z
#endif

#ifdef BC_2DPBC_HEXAGON
#  define BC_2DPBC
#  define boundary TWO_BOUNDARY_CONDITIONS
#  define BC_PBC_X
#  define BC_PBC_Y
#endif

#ifdef BC_2DPBC_SQUARE
#  define BC_2DPBC
#  define boundary TWO_BOUNDARY_CONDITIONS
#  define BC_PBC_X
#  define BC_PBC_Y
#endif

#ifndef BC_2DPBC_HEXAGON 
#  define BOX_EQUAL_SIDES // set Lx=Ly=Lz
#endif

#ifdef TRAP_POTENTIAL
#  define EXTERNAL_POTENTIAL
#endif

#ifdef TRAP_DOUBLE_WELL
#  define EXTERNAL_POTENTIAL
#endif

#ifdef TRAP_LINEAR_CHAIN_OF_HARMONIC_OSCILLATORS_UP
#  define EXTERNAL_POTENTIAL
#endif

#ifdef TRIAL_3D
#  define MOVE_IN_X
#  define MOVE_IN_Y
#  define MOVE_IN_Z
#endif

#ifdef TRIAL_2D
#  define MOVE_IN_X
#  define MOVE_IN_Y
#endif

#ifdef TRIAL_1D
#ifdef BC_1DPBC_X // in 1D trap move in z
#  define MOVE_IN_X
#else
#  define MOVE_IN_Z
#endif
#endif

#ifdef MOVE_IN_X
# define CaseX(arg) arg
#else
# define CaseX(arg)
#endif

#ifdef MOVE_IN_Y
# define CaseY(arg) arg
#else
# define CaseY(arg)
#endif

#ifdef MOVE_IN_Z
# define CaseZ(arg) arg
#else
# define CaseZ(arg)
#endif

#ifdef MOVE_IN_X
# define CaseNotX(arg)
#else
# define CaseNotX(arg) arg
#endif

#ifdef MOVE_IN_Y
# define CaseNotY(arg) 
#else
# define CaseNotY(arg) arg
#endif

#ifdef MOVE_IN_Z
# define CaseNotZ(arg) 
#else
# define CaseNotZ(arg) arg
#endif


struct Walker {
  // coordinates
  DOUBLE *x;
  DOUBLE *y;
  DOUBLE *z;
  DOUBLE *xdn;
  DOUBLE *ydn;
  DOUBLE *zdn;
  // velocities
  DOUBLE *Vx;
  DOUBLE *Vy;
  DOUBLE *Vz;
  DOUBLE *Vxdn;
  DOUBLE *Vydn;
  DOUBLE *Vzdn;
  DOUBLE **rhoRe1;  // form factor measurement [gridSKT_t][gridSKT_k]
  DOUBLE **rhoIm1;  // form factor measurement [gridSKT_t][gridSKT_k]
  DOUBLE **rhoRe2;  // form factor measurement [gridSKT_t][gridSKT_k]
  DOUBLE **rhoIm2;  // form factor measurement [gridSKT_t][gridSKT_k]
  DOUBLE E; 
  DOUBLE EFF; 
  DOUBLE Epot; 
  DOUBLE Ekin; 
  DOUBLE Eext; 
  DOUBLE Eold; 
  DOUBLE Determinant; 
  DOUBLE U; /* f=exp(U) */
  /* the flag : ALIVE, KILLED, REINCARNATION */
  int status;
  DOUBLE weight, weightp;
#ifdef CRYSTAL
  DOUBLE UcrystalUp;
  DOUBLE UcrystalDn;
#endif

  DOUBLE r2, z2;       // storing information of the current block
  DOUBLE r2old, z2old; // storing information of the previous block

  int **PD;     // Pure pair distribution [grid_pure_block][gridPD]
  int **PDup;   // Pure pair distribution [grid_pure_block][gridPD]
  int **PDdn;   // Pure pair distribution [grid_pure_block][gridPD]
  int ***PD_MATRIX11; // pure pair distribution matrix
  int ***PD_MATRIX12; // pure pair distribution matrix
  int ***PD_MATRIX22; // pure pair distribution matrix
  int ***PD_MATRIX11_pure; // pure pair distribution matrix
  int ***PD_MATRIX12_pure; // pure pair distribution matrix
  int ***PD_MATRIX22_pure; // pure pair distribution matrix
  int PD_position;

  int **RDup; // Pure radial distribution [grid_pure_block][gridRD]
  int **RDdn; // Pure radial distribution [grid_pure_block][gridRD]
  int RD_wait;
  int RD_position;

  int g3_store; // g3(0)
  int g3_accum;

  DOUBLE **Sk; // Pure static structure factor
  DOUBLE **Sk12; // Pure static structure factor
  DOUBLE **Sk_N; // Pure static structure factor

  DOUBLE *Epot_pure; // Pure potential energy [grid_pure_block]
  DOUBLE *x_pure; // Pure coordinates [N]
  DOUBLE *y_pure; // Pure coordinates [N]
  DOUBLE *z_pure; // Pure coordinates [N]
  DOUBLE *xdn_pure; // Pure coordinates [N]
  DOUBLE *ydn_pure; // Pure coordinates [N]
  DOUBLE *zdn_pure; // Pure coordinates [N]

  int Sk_position;
  int Sk_count;

  // calculation of the superfluid density
  DOUBLE **CM; // [(x,y,z)][length] center of mass position
  DOUBLE **rreal; // [N][3]

  DOUBLE *psi_U; // weight of w.f. at moment of time t [gridPhi_t]
  DOUBLE *psi_r; // position of extra particle [gridPhi_t]

  int w; // its number
  int varparindex; // pointer to the array of variational parameters

  // DMC or smart VMC drift variables 
  DOUBLE **F, **Fp, **Fpp; // FF[N][6], total Force
  DOUBLE **R, **Rp, **Rpp, **Rppp; // coordinates in different moments (R, R', R'', R''')
  DOUBLE **dR;
  DOUBLE **Dup, **Ddn, **Fup, **Fdn; // Determinant and Jastrow/One-body contributions to the Force
  DOUBLE **Fo;

#ifdef SYM_OPPOSITE_SPIN
  // DMC + SYM terms
  DOUBLE **P1up, **P1dn, **P2up, **P2dn;
  DOUBLE *Sup;
  DOUBLE *Sdn;
#endif

  //  MEASURE_CORRELATION_FUNCTIONS_IN_THE_MIDDLE_OF_DRIFT
  int status_end_of_step_initialized;
  DOUBLE **dR_drift, **dR_drift_old; // for Metropolis linear move
};

#define ALIVE 0
#define DEAD 1
#define REINCARNATION 2
#define KILLED 3

#ifdef HARD_SPHERE12
#  define HARD_SPHERE
#endif

#ifdef HARD_SPHERE11
#  define HARD_SPHERE
#endif

#ifdef CHECK_HARDCORE_RADIUS
#  define HARD_SPHERE
#  define HARD_SPHERE12
#  define HARD_SPHERE11
#  define HARD_SPHERE22
#endif

#ifdef HARD_SPHERE
#  define CHECK_OVERLAPPING
#endif

#ifdef ONE_BODY2_TRIAL_1D_WAVE
#  define ONE_BODY_TRIAL_TERMS
#endif

#ifdef ONE_BODY_TRIAL_ABC 
#  define ONE_BODY_TRIAL_TERMS
#endif

#ifdef ONE_BODY_TRIAL_SIN
#  define ONE_BODY_TRIAL_TERMS
#endif

#ifdef ONE_BODY_TRIAL_TRAP
#  define ONE_BODY_TRIAL_TERMS
#endif

#ifdef ONE_BODY_TRIAL_TRAP_DOUBLE_GAUSSIAN
#  define ONE_BODY_TRIAL_TERMS
#endif

#ifdef ONE_BODY_TRIAL_LINEAR_CHAIN_OF_HARMONIC_OSCILLATORS_UP
#  define ONE_BODY_TRIAL_TERMS
#endif

#ifdef ONE_BODY_TRIAL_1D_WAVE_2COMP_OUT_OF_PHASE
#  define ONE_BODY_TRIAL_TERMS
#endif

#ifdef ONE_BODY_TRIAL_1D_WAVE_2COMP_OUT_OF_PHASE_KPAR
#  define ONE_BODY_TRIAL_TERMS
#endif

#ifdef ONE_BODY_TRIAL_1D_WAVE_2COMP_INPHASE
#  define ONE_BODY_TRIAL_TERMS
#endif

#ifdef ONE_BODY_TRIAL_3D_WAVE_2COMP_OUT_OF_PHASE
#  define ONE_BODY_TRIAL_TERMS
#endif

#ifdef ONE_BODY_VORTEX
#  define ONE_BODY_TRIAL_TERMS
#endif

#ifdef ONE_BODY_TRIAL_FERMI
#  define ONE_BODY_TRIAL_TERMS
#endif

#ifdef ONE_BODY_TRIAL_GP_LHY_1D
#  define ONE_BODY_TRIAL_TERMS
#endif

#ifdef TRIAL_LIEB_PHONON11
#  define TRIAL_LIEB_PHONON
#endif
#ifdef TRIAL_LIEB_PHONON12
#  define TRIAL_LIEB_PHONON
#endif

#ifdef TRIAL22_SAME_AS_11
#  define InterpolateExactU22 InterpolateExactU11
#  define InterpolateExactFp22 InterpolateExactFp11
#  define InterpolateExactE22 InterpolateExactE11
#endif

#ifdef TRIAL12_SAME_AS_11
#  define InterpolateExactU12 InterpolateExactU11
#  define InterpolateExactFp12 InterpolateExactFp11
#  define InterpolateExactE12 InterpolateExactE11
#endif

#ifdef TRIAL11_SAME_AS_12
#  define InterpolateExactU11 InterpolateExactU12
#  define InterpolateExactFp11 InterpolateExactFp12
#  define InterpolateExactE11 InterpolateExactE12
#endif

#ifdef TRIAL22_SAME_AS_12
#  define InterpolateExactU22 InterpolateExactU12
#  define InterpolateExactFp22 InterpolateExactFp12
#  define InterpolateExactE22 InterpolateExactE12
#endif

#ifdef TRIAL_SCHIFF_VERLET_PHONON
# define TRIAL_SCHIFF_VERLET
#endif

#ifdef TRIAL_SCHIFF_VERLET_PHONON12
# define TRIAL_SCHIFF_VERLET12
#endif

#ifndef INTERACTION_SUM_OVER_IMAGES
#  ifndef INTERACTION_EWALD
#    define INTERACTION_WITHOUT_IMAGES
#  endif
#endif

// VMC Move One movements
#ifdef UP_PARTICLES_PINNED_TO_A_CHAIN
#  define MOVE_ONLY_DOWN_PARTICLES 
#endif

#ifdef INFINITE_MASS_COMPONENT_DN
#  define MOVE_ONLY_UP_PARTICLES
#else 
# define FINITE_MASS_COMPONENT_DN
#endif

#ifdef INFINITE_MASS_COMPONENT_UP
#  define MOVE_ONLY_DOWN_PARTICLES
#else 
#  define FINITE_MASS_COMPONENT_UP
#endif

#ifdef _OPENMP
#  define PARALLEL_CODE
#ifndef MPI
#  define OPENP_ONLY
#else
#  define MPI_WITH_OPENMP
#endif
#endif

#ifdef MPI
#  define PARALLEL_CODE
#ifndef _OPENMP
#  define MPI_ONLY
#endif
#endif

#define TWO_COMPONENT_CODE

#define JASTROW_GOES_TO_ONE_AT_LARGEST_DISTANCE

#ifdef JASTROW_RIGHT_BOUNDARY_FROM_BOUND_STATE_SOLUTION
#  undef JASTROW_GOES_TO_ONE_AT_LARGEST_DISTANCE
#endif

#ifdef JASTROW_RIGHT_BOUNDARY_PHONONS
#  undef JASTROW_GOES_TO_ONE_AT_LARGEST_DISTANCE
#endif

#ifdef JASTROW_RIGHT_BOUNDARY_INVERSE_EXPONENTIAL
#  undef JASTROW_GOES_TO_ONE_AT_LARGEST_DISTANCE
#endif

#ifdef JASTROW_RIGHT_BOUNDARY_POLARON_GPE
#  undef JASTROW_GOES_TO_ONE_AT_LARGEST_DISTANCE
#endif

#ifndef JASTROW_GOES_TO_ONE_AT_LARGEST_DISTANCE
#  define SPLINE_ANALYTIC_RIGHT_HAND_SIDE
#endif

/* define Walker structure BEFORE including "quantities.h" */
#include "quantities.h"

extern int MC, SmartMC;
extern struct Walker *W, *Wp;
extern struct Grid G11,G22,G12,Gorbital,GSYM;

extern struct sOBDM OBDM, OBDMtrap, OBDM22;
extern struct sTBDM TBDM12, TBDM11, TBDM22;
#ifdef OBDM_BOSONS
extern struct sOBDM OBDMbosons, OBDMbosons22;
extern struct sTBDM TBDMbosons12, TBDMbosons11, TBDMbosons22;
#endif
extern struct sOBDM OBDMpath_integral;
extern struct sMATRIX OBDM_MATRIX, TBDM_MATRIX, Phi, SKT, PD_MATRIX11, PD_MATRIX12, PD_MATRIX22, PD_MATRIX11_pure, PD_MATRIX12_pure, PD_MATRIX22_pure, XY2_MATRIX;
extern struct Distribution PDz, PD_pure, PDup_pure, PDdn_pure;
extern struct Distribution PD, PDup, PDdn;
extern struct Distribution RD; // contains .size; .times_measured
extern struct Distribution RDxup, RDyup, RDzup, RDup, RDup_pure;
extern struct Distribution RDxdn, RDydn, RDzdn, RDdn, RDdn_pure;
extern struct Distribution HR;
extern struct sSk Sk, Sk_pure;
extern struct sNk Nk;
extern struct sSD SD;
extern struct sOrderParameter OrderParameter;
extern struct sPureEstimator Epot_pure;

extern DOUBLE L, Lhalf, L2, Lhalf2, Lmax, L_inv, Lcutoff_pot, Lcutoff_pot2, Lwf, Lhalfwf;
extern DOUBLE Lx, Ly, Lz, L_half_x, L_half_y, L_half_z, L_inv_x, L_inv_y, L_inv_z;
extern int grid_trial;
extern int gridOBDM;
extern int gridTBDM;
extern int gridOBDM_MATRIX;
extern int gridTBDM_MATRIX;
extern int gridPD_MATRIX;
extern int gridRD;
extern int gridPD;
extern int gridg3;
extern int grid_pure_block;
extern int gridSk;
extern int gridNk;
extern long int Niter, Nmeasure;
extern DOUBLE PD_MATRIXmax;
extern DOUBLE Rpar, Apar, Bpar, Cpar, Dpar, Epar, Kpar, Mpar, Opar, beta, beta2, two_lambda_beta, lambda_beta, Gamma, beta2, Gamma2, Rpar2, Rpar11, Rpar12, Rpar22, Apar12, Bpar11, Bpar12, Bpar22, Kpar11, Kpar12;
extern DOUBLE c, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, a1, a3, a4, a5, a6, a7, a8, b1, b3, b4, b5, b6, b7, b8; // a2 = a*a
extern DOUBLE alpha_Rx, alpha_Ry, alpha_Rz;
extern DOUBLE alpha_x_up, two_alpha_x_up;
extern DOUBLE alpha_y_up, two_alpha_y_up;
extern DOUBLE alpha_z_up, two_alpha_z_up;
extern DOUBLE alpha_x_dn, two_alpha_x_dn;
extern DOUBLE alpha_y_dn, two_alpha_y_dn;
extern DOUBLE alpha_z_dn, two_alpha_z_dn;
extern DOUBLE omega_x_up, omega_y_up, omega_z_up;
extern DOUBLE omega_x2_up, omega_y2_up, omega_z2_up;
extern DOUBLE omega_x_dn, omega_y_dn, omega_z_dn;
extern DOUBLE omega_x2_dn, omega_y2_dn, omega_z2_dn;

extern DOUBLE n, a, a2, ap, b, aA, aB, lambda, lambda2, lambda4, lambda12;
extern DOUBLE dt, dt_all, dt_one, acceptance_rate, dt1, dt2;
extern int Nwalkers, NwalkersMax, Nimp;
extern int Nup, Ndn, Ntot, Nmax;
extern DOUBLE Ndens, Nmean;

extern DOUBLE Nwalkersw, Eint;
extern int Npop, Npop_max, Npop_min;
extern int None, Nall;
extern int blck, blck_heating;
extern int McMillan_points, McMillanTBDM_points;

#ifndef USE_UTF8 
#  define CHAR char
#else
#  define CHAR wchar_t
#endif

extern char file_particles[], file_energy[], file_wf[];
extern char file_OBDM[], file_OBDM_MATRIX[],  file_PDz[], file_RD[], file_RDz[];
extern char file_R2[], file_z2[], file_R2_pure[], file_z2_pure[];
extern char file_PD[], file_PD_pure[];
extern char file_Sk[], file_Sk_pure[], file_SD[];

extern int measure_energy;
extern int measure_OBDM;
extern int measure_Nk;
extern int measure_TBDM;
extern int measure_OBDM_MATRIX;
extern int measure_TBDM_MATRIX;
extern int measure_PairDistrMATRIX;
extern int measure_SD;
extern int measure_RadDistr;
extern int measure_PairDistr;
extern int measure_g3;
extern int measure_R2;
extern int measure_Sk;
extern int measure_Sk_pure;
extern int measure_OP;
extern int measure_pure_coordinates;
extern int pure_block_size;
extern int reweighting;

extern int video;
extern int branchng_present;

extern int verbosity;
extern int accepted, rejected, overlaped;
extern int accepted_one, rejected_one;
extern int accepted_all, rejected_all;
extern int generate_new_coordinates;
extern int generate_crystal_coordinates;

extern DOUBLE *u_mi, *u_ij; 
extern DOUBLE *mu_k, *mu_p_k, *mu_pp_k;
extern DOUBLE **varpar;

extern DOUBLE E, EFF, Eo, Evar, Eck;
extern DOUBLE xR2, zR2;

extern clock_t time_start;
extern clock_t time_simulating;
extern clock_t time_measuring;
extern clock_t time_communicating;

extern int *dead_walkers_storage;
extern int *walkers_storage;
extern int *killed_walkers_storage;
extern int Ndead_walkers;
extern DOUBLE reduce, amplify;
extern DOUBLE energy_shift;

extern int overlaped;
extern int tried, Nmeasured;

extern DOUBLE Srecoil, Eext, Ekin, Epot; 
extern int Nlattice, Nlattice3;

extern int measure_Lind;
extern DOUBLE LindemannRatioF;
extern int LindemannRatioN;
extern int var_par_array;
extern DOUBLE *MoUp, *MoDn;
extern int iteration_global, block;
extern DOUBLE imag_time_done;

extern DOUBLE mass_ratio;
extern DOUBLE m_up_inv, m_dn_inv, m_up_inv_sqrt, m_dn_inv_sqrt, m_up, m_dn, m_mu;

extern int Niter_store;
extern int optimization;

extern DOUBLE orbital_weight1;
extern DOUBLE orbital_weight2;
extern DOUBLE orbital_weight3;
extern DOUBLE orbital_weight4;
extern DOUBLE orbital_weight5;

extern DOUBLE bilayer_width, bilayer_width2;
extern DOUBLE kF;

extern DOUBLE lambda1,lambda2; // CSM parameters for mixture
extern int file_append; // append to data files 
extern int file_particles_append; // append coordinates to file

extern int measure_spectral_weight;
extern int gridPhi_t, gridPhi_x;
extern DOUBLE Phi_dk;
extern int measure_FormFactor;
extern int gridSKT_k, gridSKT_t;
extern DOUBLE SKT_dk;
extern DOUBLE Ro, RoSW, RoSS, Ro11, Ro12, Ro22;
extern DOUBLE excitonRadius;
extern int one_body_momentum_i;
extern DOUBLE one_body_momentum_k;
extern DOUBLE omega;
extern DOUBLE rGauss_up, rGauss_dn;
extern DOUBLE T;
extern DOUBLE kL, alpha_latt, beta_latt;
extern int sum_over_images_cut_off_num;
extern int measure_effective_up_dn_potential;
extern int check_kinetic_energy;
extern int NCrystal;
extern DOUBLE Epotcutoff;
extern int measure_XY2;

void Measure(int, int);

#include "optimiz.h"

#endif