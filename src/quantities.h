/*quantities.h*/

#ifndef _QUANTITIES_H_
#define _QUANTITIES_H_

#include "main.h"

#ifdef INTERACTION_LENNARD_JONES

// T_lj = h^2/2m A^2
// U_lj = 4 eps_lj / T_lj 
// H2
/*
#  define eps_lj 10.7 //= 42.8/4
#  define sigma_lj 2.97
#  define T_lj 24.0668
#  define U_lj 1.77839 */

//4He
#  define eps_lj 10.22
#  define sigma_lj 2.556
#  define T_lj (2*6.0599278)
#  define U_lj (4*eps_lj/T_lj)

#endif

DOUBLE Energy(DOUBLE *Epot, DOUBLE *Ekin, DOUBLE *Eff, DOUBLE *Eext);
DOUBLE WalkerEnergy(struct Walker *W, DOUBLE *V, DOUBLE *Ekin, DOUBLE *Eff, DOUBLE *Eext);
DOUBLE WalkerEnergy0(struct Walker *W);
DOUBLE VextUp(DOUBLE x, DOUBLE y, DOUBLE z, int i);
DOUBLE VextDn(DOUBLE x, DOUBLE y, DOUBLE z, int i);
DOUBLE InteractionEnergy11(DOUBLE r);
DOUBLE InteractionEnergy22(DOUBLE r);
DOUBLE InteractionEnergy12(DOUBLE r);

int MeasureOBDM(void);
int MeasureOBDM1D(void);
int MeasureTBDM(void);
int MeasureTBDM1D(void);
int MeasureOBDMMatrix(void);
int MeasureOBDMresidue(int w);
void MeasureSuperfluidDensity(void);
void MeasurePairDistribution(int w);
void MeasureRadialDistribution(void);
void MeasureRadialDistributionWalker(struct Walker *W);
void EmptyDistributionArrays(void);
void MeasureStaticStructureFactor(int w);
void MeasureStaticStructureFactorAngularPart(void);
int MeasureMomentumDistribution(void);
void MeasureLindemannRatio(void);
void MeasureSpectralWeight(void);
void MeasureFormFactor(int w, int final_measurement);
void MeasureHyperRadius(void);
DOUBLE WalkerPotentialEnergy(struct Walker *W);
void GaussianJumpWalker(int w, DOUBLE sigma);
void MeasurePurePotentialEnergy(void);
DOUBLE KineticEnergyNumerically(struct Walker *walkern);
void MeasureXY2(int w);

struct sOBDM {
  long int size;
  DOUBLE step;
  DOUBLE min;
  DOUBLE max;
  DOUBLE *f;
  DOUBLE *N;

  int Nksize; // parameters of momentum distribution are stored here
  DOUBLE kmin; 
  DOUBLE kmax; 
  DOUBLE *k;  // grid for nk
  DOUBLE *Nk; // Nk
  int times_measured; // needed for Nk
  int Nktimes_measured; // needed for Nk
};

struct sTBDM {
  long int size;
  DOUBLE step;
  DOUBLE min;
  DOUBLE max;
  DOUBLE *f; // TBDM
  DOUBLE *N; // times measured
  DOUBLE *R2; // r^2 (direct estimator)
  DOUBLE *OBDM; // OBDM
  DOUBLE *fcorrected; // TBDM (corrected)
  DOUBLE *R2corrected; // r^2 (corrected estimator) 
  DOUBLE *fr; // TBDM in the other direction (molecule orbital w.f.)
  DOUBLE *Nr; // times measured fr
};

struct sMATRIX {
  long int size;
  DOUBLE step;
  DOUBLE min;
  DOUBLE max;
  DOUBLE **f;
  DOUBLE **f12; // Sk12 
  int **N;
  DOUBLE Norma;
  int times_measured;
};

struct sSk {
  long int size; // length of input array
  DOUBLE *N;     // times measured, NMean can be non integer
  DOUBLE *k;     // k^2 = kx^2+ky^2+kz^2
  DOUBLE *kx;
  DOUBLE *ky;
  DOUBLE *kz;
  int *index;    // number of non-degenerate values of momenta
  DOUBLE *f;     // same spin
  DOUBLE *f12;   // different spin
  DOUBLE L;      // maximal distance
  DOUBLE *cos;
  int *degeneracy; // 1-no degeneracy, set d>1 for the first degerate momentum and d=0 to other 
  int times_measured;
  DOUBLE *phi;    // phi(k2) - phi(k1)
  DOUBLE *phiN;
};

struct Distribution {
  int times_measured;
  long int size;
  DOUBLE step;
  DOUBLE min;
  DOUBLE max;
  DOUBLE r2;
  DOUBLE r2recent;
  DOUBLE width;
  int *N;
  int g3;
  DOUBLE *f;
};

struct sSD {
  DOUBLE *CM2;
  int *N;
  int size;
  int spacing;
};

struct sOrderParameter {
  DOUBLE cos;
  DOUBLE sin;
  int times_measured;
};

struct sNk {
  long int size;   // length of input array
  DOUBLE *k;
  DOUBLE **f;      // momentum distribution
  int times_measured;
};

struct sPureEstimator {
  long int size;
  DOUBLE step;
  DOUBLE min;
  DOUBLE max;
  DOUBLE *f;
  DOUBLE *x;
  int times_measured;
};

#endif
