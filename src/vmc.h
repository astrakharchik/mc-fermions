/*vmc.h*/
#ifndef _VMC_H
#define _VMC_H

#include <math.h>
#include "main.h"
#include "utils.h"

DOUBLE Psi(struct Walker Walker);
DOUBLE U(struct Walker Walker);
DOUBLE OneBodyUWalker(struct Walker Walker, int sum_up, int sum_down);
DOUBLE UOneBodyCrystalParticle(DOUBLE x, DOUBLE y, DOUBLE z, int up);

DOUBLE OneBodyU(DOUBLE x, DOUBLE y, DOUBLE z, int i, int up);
void OneBodyFp(DOUBLE *Fx, DOUBLE *Fy, DOUBLE *Fz, DOUBLE x, DOUBLE y, DOUBLE z, int i, int up);
DOUBLE OneBodyE(DOUBLE x, DOUBLE y, DOUBLE z, int i, int up);

DOUBLE WalkerEnergy2(struct Walker *W);

void VMCMoveOneByOne(int w);
void VMCMoveAll(int w);
void VMCMoveSmart(int w, DOUBLE dt);

void ClassicalMoveOneByOne(int w);

int CheckInteractionCondition(const DOUBLE z, const DOUBLE r2);
int CheckInteractionConditionOrbital(const DOUBLE z, const DOUBLE r2); // i.e. always check


/********************************** Overlapping *******************************/
#ifndef HARD_SPHERE
#  define Overlapping(R) 0
#  define OverlappingWalker(W) 0
#  define CheckOverlapping() 0
#  define CheckWalkerOverlapping(W) 0
#else
int Overlapping(DOUBLE **R);
int OverlappingWalker(const struct Walker* W);
int CheckOverlapping(void);
int CheckWalkerOverlapping(const struct Walker W);
#endif

#endif
