/*trialf.h*/

#ifndef __TRIALF_H_
#define __TRIALF_H_

#include "main.h"

void LoadOrbital(void);

void ConstructSqWellDepth(void);
void ConstructSqWellDepthPwave(void);
DOUBLE ConstructSoftSphereHeight(DOUBLE Ro, DOUBLE a, DOUBLE mu);
void ConstructGridBCS(void);
void ConstructGridBCSdecay(void);
void ConstructGridBCSPseudopotential(void);
void ConstructGridBCSSquareWell(void);
void ConstructGridBCSSquareWellBCSSide(void);
void ConstructGridBCSSquareWellZeroEnergy(void);
void ConstructGridBCSSquareWellZeroConst(void);
void ConstructGridBCSIdealGas(void);
void ConstructGridFermion(void);
void ConstructGridDecay(void);
void ConstructGridCh2Exp(void);
void ConstructGridCh2PhononOrbital(void);
void ConstructGridSquareWell2DSinus(void);
void ConstructGridSquareWell2DPhonon(void);
void ConstructGridExponentAlpha(void);

DOUBLE orbitalDescreteBCS_F(DOUBLE x, DOUBLE y, DOUBLE z);
void orbitalDescreteBCS_Fp(DOUBLE *Fx, DOUBLE *Fy, DOUBLE *Fz, DOUBLE x, DOUBLE y, DOUBLE z);
DOUBLE orbitalDescreteBCS_Fpp(DOUBLE x, DOUBLE y, DOUBLE z);

DOUBLE orbitalFewBody_F(DOUBLE x, DOUBLE y, DOUBLE z, DOUBLE xdn, DOUBLE ydn, DOUBLE zdn, int i, int j);
void orbitalFewBody_Fp(DOUBLE *Fxup, DOUBLE *Fyup, DOUBLE *Fzup, DOUBLE *Fxdn, DOUBLE *Fydn, DOUBLE *Fzdn, DOUBLE x, DOUBLE y, DOUBLE z, DOUBLE xdn, DOUBLE ydn, DOUBLE zdn, int i, int j);
void orbitalFewBody_Fpp(DOUBLE *Fupup, DOUBLE *Fdndn, DOUBLE x, DOUBLE y, DOUBLE z, DOUBLE xdn, DOUBLE ydn, DOUBLE zdn, int i, int j);

DOUBLE orbitalExactF(DOUBLE r);
DOUBLE orbitalExactFp(DOUBLE r);
DOUBLE orbitalExactEloc(DOUBLE r);

DOUBLE orbitalGridF(DOUBLE r);
DOUBLE orbitalGridFp(DOUBLE r);
DOUBLE orbitalGridEloc(DOUBLE r);

struct GridBCS {
  long int size; // number of elements in the grid
  DOUBLE step;   //  dx = (max-min) / N;
  DOUBLE I_step;
  DOUBLE min;
  DOUBLE max;
  DOUBLE max2;
  DOUBLE *x;  // for(i=1; i<N; i++)    x = min + dx * (DOUBLE) i;
  DOUBLE *f;  // f
  DOUBLE *fp; // f'
  DOUBLE *E;  // f" +2/r f' = u"/r
};

struct GridBCS BCS;
extern int subtract_energy;
extern DOUBLE BCSsqE2;
extern DOUBLE BCS2trialKappa, BCS2Dtrial, BCS2Etrial;

#endif
