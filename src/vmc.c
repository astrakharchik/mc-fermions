/*vmc.c*/

#include <math.h>
#include "vmc.h"
#include "dmc.h"
#include "main.h"
#include "randnorm.h"
#include "utils.h"
#include "trialf.h"
#include "trial.h"
#include "compatab.h"
#include "crystal.h"
#include "fermions.h"
#include "spline.h"
#include "memory.h"
#include "move.h"
#include "md.h"
#include "rw.h"

#define INFTY 1e100

#ifdef HARD_SPHERE
int Overlapping(DOUBLE **R) {
  int i,j;
  DOUBLE dr[3], r2;

#ifdef HARD_SPHERE12
  for(i=0; i<Nup; i++) {
    for(j=0; j<Ndn; j++) {
      dr[0] = R[i][0] - R[j][3];
      dr[1] = R[i][1] - R[j][4];
      dr[2] = R[i][2] - R[j][5];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(r2 <= a2) 
        return 1;
    }
  }
#endif

#ifdef HARD_SPHERE11
  for(i=0; i<Nup; i++) {
    for(j=i+1; j<Nup; j++) {
      dr[0] = R[i][0] - R[j][0];
      dr[1] = R[i][1] - R[j][1];
      dr[2] = R[i][2] - R[j][2];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(r2 <= aA*aA) 
        return 1;
    }
  }
#endif

#ifdef HARD_SPHERE22
  for(i=0; i<Ndn; i++) {
    for(j=i+1; j<Ndn; j++) {
      dr[0] = R[i][3] - R[j][3];
      dr[1] = R[i][4] - R[j][4];
      dr[2] = R[i][5] - R[j][5];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(r2 <= aB*aB) 
        return 1;
    }
  }
#endif

  return 0;
}

/********************************** Overlapping Walker ************************/
int OverlappingWalker(const struct Walker* W) {
  int i,j;
  DOUBLE dr[3], r2;

#ifdef HARD_SPHERE12
  for(i=0; i<Nup; i++) {
    for(j=0; j<Ndn; j++) {
      dr[0] = W->x[i] - W->xdn[j];
      dr[1] = W->y[i] - W->ydn[j];
      dr[2] = W->z[i] - W->zdn[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(r2 <= a2) 
        return 1;
    }
  }
#endif

#ifdef HARD_SPHERE11
  for(i=0; i<Nup; i++) {
    for(j=i+1; j<Nup; j++) {
      dr[0] = W->x[i] - W->x[j];
      dr[1] = W->y[i] - W->y[j];
      dr[2] = W->z[i] - W->z[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(r2 <= aA*aA) 
        return 1;
    }
  }
#endif

#ifdef HARD_SPHERE22
  for(i=0; i<Ndn; i++) {
    for(j=i+1; j<Ndn; j++) {
      dr[0] = W->xdn[i] - W->xdn[j];
      dr[1] = W->ydn[i] - W->ydn[j];
      dr[2] = W->zdn[i] - W->zdn[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(r2 <= aB*aB) return 1;
    }
  }
#endif

  return 0;
}

/************************ Check Overlapping ********************************/
int CheckOverlapping(void) {
  int w,i,j;
  DOUBLE dr[3], r2;

  for(w=0; w<Nwalkers; w++) {
#ifdef HARD_SPHERE12
    for(i=0; i<Nup; i++) {
      for(j=0; j<Ndn; j++) {
        dr[0] = W[w].x[i] - W[w].xdn[j];
        dr[1] = W[w].y[i] - W[w].ydn[j];
        dr[2] = W[w].z[i] - W[w].zdn[j];

        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
        if(r2 <= a2) 
          return w+1;
      }
    }
#endif

#ifdef HARD_SPHERE11
    for(i=0; i<Nup; i++) {
      for(j=i+1; j<Nup; j++) {
        dr[0] = W[w].x[i] - W[w].x[j];
        dr[1] = W[w].y[i] - W[w].y[j];
        dr[2] = W[w].z[i] - W[w].z[j];
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
        if(r2 <= aA*aA)
          return w+1;
      }
    }
#endif

#ifdef HARD_SPHERE22
    for(i=0; i<Ndn; i++) {
      for(j=i+1; j<Ndn; j++) {
        dr[0] = W[w].xdn[i] - W[w].xdn[j];
        dr[1] = W[w].ydn[i] - W[w].ydn[j];
        dr[2] = W[w].zdn[i] - W[w].zdn[j];
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
        if(r2 <= aB*aB)
          return w+1;
      }
    }
#endif
  }
  return 0;
}

/************************ Check Walker Overlapping ***************************/
int CheckWalkerOverlapping(const struct Walker W) {
  int i,j;
  DOUBLE dr[3], r2;

#ifdef HARD_SPHERE12
  for(i=0; i<Nup; i++) {
    for(j=0; j<Ndn; j++) {
      dr[0] = W.x[i] - W.xdn[j];
      dr[1] = W.y[i] - W.ydn[j];
      dr[2] = W.z[i] - W.zdn[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(r2 <= a2) return 1;
    }
  }
#endif

#ifdef HARD_SPHERE11
  for(i=0; i<Nup; i++) {
    for(j=i+1; j<Nup; j++) {
      dr[0] = W.x[i] - W.x[j];
      dr[1] = W.y[i] - W.y[j];
      dr[2] = W.z[i] - W.z[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(r2 <= aA*aA) return 1;
    }
  }
#endif

#ifdef HARD_SPHERE22
  for(i=0; i<Ndn; i++) {
    for(j=i+1; j<Ndn; j++) {
      dr[0] = W.xdn[i] - W.xdn[j];
      dr[1] = W.ydn[i] - W.ydn[j];
      dr[2] = W.zdn[i] - W.zdn[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(r2 <= aB*aB) return 1;
    }
  }
#endif

#ifdef HARD_SPHERE12
  for(i=0; i<Nup; i++) {
    for(j=i+1; j<Ndn; j++) {
      dr[0] = W.x[i] - W.xdn[j];
      dr[1] = W.y[i] - W.ydn[j];
      dr[2] = W.z[i] - W.zdn[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(r2 <= a*a) return 1;
    }
  }
#endif

  return 0;
}
#endif

/***************************** Move All **************************************/
void VMCMoveAll(int w) {
  int i;
  DOUBLE xi;
  DOUBLE dx,dy,dz;
  int accept;
  DOUBLE du;
  DOUBLE sigma;
#ifdef FERMIONS
  int j,alpha;
  DOUBLE x,y,z,r2,dr[3];
#endif

  sigma = sqrt(0.5*dt_all);

  //  Move particle np of the walker w
  for(i=0; i<Nup; i++) {
    RandomNormal3(&dx, &dy, &dz, 0., sigma);
#ifdef TRIAL_1D
    dx = dy = 0.;
#endif
#ifdef TRIAL_2D
    dz = 0;
#endif
    CaseX(Wp[w].x[i] = W[w].x[i] + m_up_inv_sqrt*dx;)
    CaseY(Wp[w].y[i] = W[w].y[i] + m_up_inv_sqrt*dy;)
    CaseZ(Wp[w].z[i] = W[w].z[i] + m_up_inv_sqrt*dz;)
  }

  for(i=0; i<Ndn; i++) {
    RandomNormal3(&dx, &dy, &dz, 0., sigma);
#ifdef TRIAL_1D
    dx = dy = 0.;
#endif
#ifdef TRIAL_2D
    dz = 0;
#endif
    CaseX(Wp[w].xdn[i] = W[w].xdn[i] + m_dn_inv_sqrt*dx;)
    CaseY(Wp[w].ydn[i] = W[w].ydn[i] + m_dn_inv_sqrt*dy;)
    CaseZ(Wp[w].zdn[i] = W[w].zdn[i] + m_dn_inv_sqrt*dz;)
  }

//#ifdef CENTER_OF_MASS_IS_NOT_MOVED // propose such movement that CM position is not moved
//  AdjustCenterOfMassWalker(&Wp[w]);
//#endif

  ReduceWalkerToTheBox(&Wp[w]);

#ifdef HARD_SPHERE
  if(CheckWalkerOverlapping(Wp[w])) {
    overlaped++;
    return;
  }
#endif

  Wp[w].U = U(Wp[w]);

#ifdef FERMIONS
  // Fill Determinant Matrix
  for(i=0; i<Nup; i++) {
    x = Wp[w].x[i];
    y = Wp[w].y[i];
    z = Wp[w].z[i];
    for(j=0; j<Nup; j++) {
      DeterminantMove[i][j] = 0.;
#ifdef ORBITAL_ANISOTROPIC_ABSOLUTE // for all j
      DeterminantMove[i][j] += orbitalFewBody_F(x,y,z,Wp[w].xdn[j],Wp[w].ydn[j],Wp[w].zdn[j],i,j);
#endif
      if(j<Ndn) { // unpolarized
        dr[0] = x - Wp[w].xdn[j];
        dr[1] = y - Wp[w].ydn[j];
        dr[2] = z - Wp[w].zdn[j];
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef ORBITAL_ISOTROPIC
        DeterminantMove[i][j] += orbitalF(sqrt(r2));
#endif
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
        DeterminantMove[i][j] += orbitalDescreteBCS_F(dr[0], dr[1], dr[2]);
#endif
      }
      else { // polarized
#ifdef ORBITAL_POLARIZED_DESCRETE_BCS
        DeterminantMove[i][j] += orbitalDescreteBCS_F(dr[0], dr[1], dr[2]);
#endif
#ifdef ORBITAL_POLARIZED_ALPHA
        alpha = Nup - 1 - j;
        DeterminantMove[i][j] = orbital(Wp[w].x[i], orbital_numbers_unpaired[alpha][0])*orbital(Wp[w].y[i], orbital_numbers_unpaired[alpha][1])*orbital(Wp[w].z[i], orbital_numbers_unpaired[alpha][2]);
#endif
      }
    }
  }

  // Invert the matrix
  if(LUinv(DeterminantMove, DeterminantMoveInv, Nup) == 0) {
    Warning("VMC Move Allinverse: singular matrix, this move will be rejected!\n");
    rejected++;
    return;
  }

  Wp[w].Determinant = determinantLU;

  du = Wp[w].U - W[w].U + Log(fabs(Wp[w].Determinant/W[w].Determinant));
#else // bosons
  du = Wp[w].U - W[w].U;
#endif

  if(du > 0.) {
    accept = ON;
  }
  else {
    xi = Random();
    if(xi<Exp(2.*du)) {
      accept = ON;
    }
    else {
      rejected++;
      return;
    }
  }

  if(accept) {
    accepted++;
    imag_time_done += sqrt(dx*dx+dy*dy+dz*dz);
    CopyWalker(&W[w], Wp[w]);
    W[w].w = w;
    W[w].weight = 1.;
#ifdef FERMIONS
    CopyMatrix(DeterminantInv[w], DeterminantMoveInv, Nup);
#endif
    return;
  }
}

/***************************** Move One By One *******************************/
void VMCMoveOneByOne(int w) {
  int i,j,iloop;
  DOUBLE q = 0;
  DOUBLE xi;
  DOUBLE dx,dy,dz,x,y,z,r2;
  DOUBLE du, dr[3];
  int accept;
  int overlap = OFF;
  DOUBLE r_trial[3];
  int up;
  int Nsame, Ndiff; // Number of particles with the same or different spin orientation
  DOUBLE weight;
  int first_time = 0;
#ifdef FERMIONS
  int alpha, beta;
  DOUBLE phi_new[1000];
  DOUBLE **matrixSwap;
#endif
  DOUBLE sqrt_dt_one = sqrt(dt_one);

#ifdef FERMIONS
#ifndef TRIAL_1D
  if(Nup != Ndn) Error("Move by one is implemented only for unpolarized gas!\n");
#endif
#endif

#ifdef SYM_OPPOSITE_SPIN
  Error("Move one by one is not implemented for SYM_OPPOSITE_SPIN, use MoveAll\n");
#endif

  if((iteration_global-1) % 1000 == 0) first_time = ON; // update on the first iteration and each 1000th

  if(var_par_array) AdjustVariationalParameter(W[w].varparindex);

/*#ifdef CRYSTAL
  for(i=0; i<N; i++) {
    Wp[w].x[i] = W[w].x[i];
    Wp[w].y[i] = W[w].y[i];
    Wp[w].z[i] = W[w].z[i];
    Wp[w].xdn[i] = W[w].xdn[i];
    Wp[w].ydn[i] = W[w].ydn[i];
    Wp[w].zdn[i] = W[w].zdn[i];
  }
  if(first_time) {
    W[w].UcrystalUp = OneBodyUWalker(W[w], ON, OFF);
    W[w].UcrystalDn = OneBodyUWalker(W[w], OFF, ON);
  }
#endif*/
#ifdef CRYSTAL // crystal
#ifdef CRYSTAL_SYMMETRIC
    Error("  can not use VMCMoveOneByOne for symmetrized crystal w.f.\n");
#endif
#endif

#ifdef FERMIONS
  if(first_time == ON) {
    // Fill Determinant Matrix
    for(i=0; i<Nup; i++) {
      x = W[w].x[i];
      y = W[w].y[i];
      z = W[w].z[i];
      for(j=0; j<Ndn; j++) {
        dr[0] = x - W[w].xdn[j];
        dr[1] = y - W[w].ydn[j];
        dr[2] = z - W[w].zdn[j];
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
        Determinant[w][i][j] = orbitalF(sqrt(r2));
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
        Determinant[w][i][j] += orbitalDescreteBCS_F(dr[0], dr[1], dr[2]);
#endif
       }
    }
    // Invert the matrix
    LUinv(Determinant[w], DeterminantInv[w], Nup);
    W[w].Determinant = determinantLU;
  }
#endif

#ifdef MOVE_ONLY_DOWN_PARTICLES
  for(iloop=Nup; iloop<Nup+Ndn; iloop++) {
#else
#ifdef MOVE_ONLY_UP_PARTICLES
  for(iloop=0; iloop<Nup; iloop++) {
#else
  for(iloop=0; iloop<Nup+Ndn; iloop++) { //  move i-th particle to the point rm
#endif
#endif
    du = 0;

    if(iloop<Nup) { // move particle up
      up = ON;
      i = iloop;
      Nsame = Nup;
      Ndiff = Ndn;
    }
    else { // move particle down
      up = OFF;
      i = iloop-Nup;
      Nsame = Ndn;
      Ndiff = Nup;
    }

    RandomNormal3(&dx, &dy, &dz, 0., sqrt_dt_one); //(x,y,z, mu, sigma)

#ifdef TRIAL_2D
    dz = 0.;
#endif

#ifdef TRIAL_1D
    dx = 0.;
    dy = 0.;
#endif

    // r_trial is the trial position
    if(up) {
      x = W[w].x[i];
      y = W[w].y[i];
      z = W[w].z[i];
      r_trial[0] = x + m_up_inv_sqrt*dx;
      r_trial[1] = y + m_up_inv_sqrt*dy;
      r_trial[2] = z + m_up_inv_sqrt*dz;
    }
    else {
      x = W[w].xdn[i];
      y = W[w].ydn[i];
      z = W[w].zdn[i];
      r_trial[0] = x + m_dn_inv_sqrt*dx;
      r_trial[1] = y + m_dn_inv_sqrt*dy;
      r_trial[2] = z + m_dn_inv_sqrt*dz;
    }

    ReduceToTheBox(r_trial);

    // Jastrow and external field contribution
#ifdef ONE_BODY_TRIAL_TERMS // One body terms
    du += OneBodyU(r_trial[0], r_trial[1], r_trial[2], i, up);
    if(up)
      du -= OneBodyU(W[w].x[i],  W[w].y[i],  W[w].z[i], i, up);
    else
      du -= OneBodyU(W[w].xdn[i],  W[w].ydn[i],  W[w].zdn[i], i, up);
#endif

   // Determinant contribution & Jastrow opposite spin
    q = 0.;
    for(j=0; j<Ndiff; j++) {
#ifdef FERMIONS
      phi_new[j] = 0.;
#endif
      if(up) {
        dr[0] = r_trial[0] - W[w].xdn[j];
        dr[1] = r_trial[1] - W[w].ydn[j];
        dr[2] = r_trial[2] - W[w].zdn[j];
      }
      else {
        dr[0] = r_trial[0] - W[w].x[j];
        dr[1] = r_trial[1] - W[w].y[j];
        dr[2] = r_trial[2] - W[w].z[j];
      }
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef CHECK_HARDCORE_RADIUS
      if(r2<HARDCORE_RADIUS_SQUARED_12) overlap = ON;
#endif

#ifdef FERMIONS
#ifdef ORBITAL_ANISOTROPIC_ABSOLUTE
       Error("Move One by One not implemented for ORBITAL_ANISOTROPIC_ABSOLUTE\n");
#endif
#ifdef ORBITAL_ISOTROPIC
      phi_new[j] += orbitalF(sqrt(r2));
#endif
#ifdef ORBITAL_ANISOTROPIC_RELATIVE // determinant
      phi_new[j] += orbitalDescreteBCS_F(dr[0], dr[1], dr[2]);
#endif
#endif

#ifdef JASTROW_OPPOSITE_SPIN // Jastrow
      if(CheckInteractionCondition(dr[2], r2)) 
        du += InterpolateU12(&G12, sqrt(r2)); // new value of the wavefunction

      if(up) { // old value
        CaseX(dr[0] = x - W[w].xdn[j]);
        CaseY(dr[1] = y - W[w].ydn[j]);
        CaseZ(dr[2] = z - W[w].zdn[j]);
      }
      else {
        CaseX(dr[0] = x - W[w].x[j]);
        CaseY(dr[1] = y - W[w].y[j]);
        CaseZ(dr[2] = z - W[w].z[j]);
      }
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(CheckInteractionCondition(dr[2], r2)) 
        du -= InterpolateU12(&G12, sqrt(r2)); // new value of the wavefunction
#endif
    }

#ifdef FERMIONS
    if(up)
      for(j=0; j<Nup; j++) q += DeterminantInv[w][i][j] * phi_new[j];
    else
      for(j=0; j<Nup; j++) q += DeterminantInv[w][j][i] * phi_new[j];
#endif

#ifdef JASTROW_SAME_SPIN
    for(j=0; j<Nsame; j++) {
      if(j != i) {
        if(up) {
          CaseX(dr[0] = r_trial[0] - W[w].x[j]); // calculate new value of the wavefunction
          CaseY(dr[1] = r_trial[1] - W[w].y[j]);
          CaseZ(dr[2] = r_trial[2] - W[w].z[j]);
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef CHECK_HARDCORE_RADIUS
          if(r2<HARDCORE_RADIUS_SQUARED_11) overlap = ON;
#endif
          if(CheckInteractionCondition(dr[2], r2)) 
            du += InterpolateU11(&G11, sqrt(r2));

          CaseX(dr[0] = x - W[w].x[j]); // calculate old value of the wavefunction
          CaseY(dr[1] = y - W[w].y[j]);
          CaseZ(dr[2] = z - W[w].z[j]);
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          if(CheckInteractionCondition(dr[2], r2)) 
            du -= InterpolateU11(&G11, sqrt(r2));
        }
        else {
          CaseX(dr[0] = r_trial[0] - W[w].xdn[j]); // calculate new value of the wavefunction
          CaseY(dr[1] = r_trial[1] - W[w].ydn[j]);
          CaseZ(dr[2] = r_trial[2] - W[w].zdn[j]);
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          if(CheckInteractionCondition(dr[2], r2)) du += InterpolateU22(&G22, sqrt(r2));

          CaseX(dr[0] = x - W[w].xdn[j]); // calculate old value of the wavefunction
          CaseY(dr[1] = y - W[w].ydn[j]);
          CaseZ(dr[2] = z - W[w].zdn[j]);
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef CHECK_HARDCORE_RADIUS
          if(r2<HARDCORE_RADIUS_SQUARED_22) overlap = ON;
#endif
          if(CheckInteractionCondition(dr[2], r2)) du -= InterpolateU22(&G22, sqrt(r2));
        }
      }
    }
#endif

    // The Metropolis code
#ifdef FERMIONS
    weight = q*q*Exp(2.*du);
#else
    weight = Exp(2.*du);
#endif
    if(weight >= 1.) {
      accept = ON;
    }
    else {
      xi = Random();
      if(xi < weight) {
        accept = ON;
        accepted++;
      }
      else {
        accept = OFF;
        rejected++;
      }
    }

#ifdef CHECK_HARDCORE_RADIUS
  if(overlap) accept = OFF;
#endif

    if(accept) {
      imag_time_done += sqrt(dx*dx+dy*dy+dz*dz);
      if(up) {
        CaseX(W[w].x[i] = r_trial[0]);
        CaseY(W[w].y[i] = r_trial[1]);
        CaseZ(W[w].z[i] = r_trial[2]);
      }
      else {
        CaseX(W[w].xdn[i] = r_trial[0]);
        CaseY(W[w].ydn[i] = r_trial[1]);
        CaseZ(W[w].zdn[i] = r_trial[2]);
      }
#ifdef CRYSTAL // Crystal contribution
    //if(up) 
    //  W[w].UcrystalUp = UcrystalNew;
    //else
    //  W[w].UcrystalDn = UcrystalNew;
#endif

#ifdef FERMIONS
      // Update the inverse matrix
      for(j=0; j<Nup; j++) {
        if(up) {
          for(alpha=0; alpha<Nup; alpha++) {
            if(j == i)
              Determinant[w][j][alpha] = DeterminantInv[w][j][alpha]/q;
            else {
              Determinant[w][j][alpha] = 0.;
              for(beta=0; beta<Nup; beta++) {
                Determinant[w][j][alpha] += DeterminantInv[w][j][beta]*phi_new[beta];
              }
              Determinant[w][j][alpha] = DeterminantInv[w][j][alpha] - DeterminantInv[w][i][alpha]*Determinant[w][j][alpha]/q;
            }
          }
        }
        else { // down
          for(alpha=0; alpha<Nup; alpha++) {
            if(j == i)
              Determinant[w][alpha][j] = DeterminantInv[w][alpha][j]/q;
            else {
              Determinant[w][alpha][j] = 0.;
              for(beta=0; beta<Nup; beta++) {
                Determinant[w][alpha][j] += DeterminantInv[w][beta][j]*phi_new[beta];
              }
              Determinant[w][alpha][j] = DeterminantInv[w][alpha][j] - DeterminantInv[w][alpha][i]*Determinant[w][alpha][j]/q;
            }
          }
        }
      }
      // exchange Determinant and DeterminantInv matrices
      matrixSwap = DeterminantInv[w];
      DeterminantInv[w] = Determinant[w];
      Determinant[w] = matrixSwap;
#endif
    }
  }
}

/********************************* U One Body ********************************/
// returns w.f. of given walker in form f = Exp(U)
// up/down are flags for doing the summation
DOUBLE OneBodyUWalker(struct Walker Walker, int sum_up, int sum_down) {
#ifndef ONE_BODY_TRIAL_TERMS // homogeneous liquid phase
  return 0;
#else
  int i;
  DOUBLE u = 0.;

#ifdef CRYSTAL
#ifdef CRYSTAL_SYMMETRIC
  DOUBLE dr[3] = {0,0,0};
  DOUBLE r2;
  DOUBLE ucr1, ucr2;
  int j;
#endif
#endif

  if(var_par_array) AdjustVariationalParameter(Walker.varparindex);

  if(sum_up) for(i=0; i<Nup; i++) u += OneBodyU(Walker.x[i], Walker.y[i], Walker.z[i], i, ON);
  if(sum_down) for(i=0; i<Ndn; i++) u += OneBodyU(Walker.xdn[i], Walker.ydn[i], Walker.zdn[i], i, OFF);

#ifdef CRYSTAL
#ifdef CRYSTAL_SYMM_SUM_PARTICLE
  for(i=0; i<Nup; i++) { // sum over lattice sites
    ucr1 = ucr2 = 0.;
    for(j=0; j<Nup; j++) { // sum over particles
      if(sum_up) {
        dr[0] = Walker.x[j] - Crystal.x[i];
        dr[1] = Walker.y[j] - Crystal.y[i];
        dr[2] = Walker.z[j] - Crystal.z[i];
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
        ucr1 += Exp(-Crystal.R * r2);
        //ucr += Crystal_dot_weight_j*Exp(-Crystal_dot_Rx_i*dr[0]*dr[0]-Crystal_dot_Ry_i*dr[1]*dr[1]-Crystal_dot_Rz_i*dr[2]*dr[2]);
      }
      if(sum_down) {
        dr[0] = Walker.xdn[j] - Crystal.x[i];
        dr[1] = Walker.ydn[j] - Crystal.y[i];
        dr[2] = Walker.zdn[j] - Crystal.z[i];
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
        ucr2 += Exp(-Crystal.R * r2);
      }
    }
    if(sum_up) u += Log(ucr1);
    if(sum_down) u += Log(ucr2);
  }
#endif
#endif

  return u;
#endif
}


/********************************* U One Body Particle ***********************/
DOUBLE UOneBodyCrystalParticle(DOUBLE x, DOUBLE y, DOUBLE z, int up) {

#ifdef CRYSTAL // symmetric solid phase
  int i;
  DOUBLE u = 0.;
  DOUBLE r2;
  DOUBLE dr[3];

#ifdef CRYSTAL_SYMM_SUM_PARTICLE
  Warning("  Symmetric w.f. call for a symmetric phase (2)!\n");
#endif

  for(i=0; i<Crystal.size; i++) {
    if(up) {
      dr[0] = x - Crystal.x[i];
      dr[1] = y - Crystal.y[i];
      dr[2] = z - Crystal.z[i];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(CheckInteractionCondition(dr[2], r2)) {
#ifdef CRYSTAL_WIDTH_ARRAY
        u += Exp(-Crystal.Rx[i] * dr[0]*dr[0] - Crystal.Ry[i] * dr[1]*dr[1] - Crystal.Rz[i] * dr[2]*dr[2]);
#else
        u += Exp(-Crystal.R * r2);
#endif
      }
    }
    else {
      dr[0] = x - Crystal.xdn[i];
      dr[1] = y - Crystal.ydn[i];
      dr[2] = z - Crystal.zdn[i];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
       if(CheckInteractionCondition(dr[2], r2)) {
#ifdef CRYSTAL_WIDTH_ARRAY
        u += Exp(-Crystal.Rxdn[i] * dr[0]*dr[0] - Crystal.Rydn[i] * dr[1]*dr[1] - Crystal.Rzdn[i] * dr[2]*dr[2]);
#else
        u += Exp(-Crystal.R * r2);
#endif
      }
    }
  }
  return Log(u);
#else
  return 0;
#endif
}

/********************************* U *****************************************/
DOUBLE U(struct Walker Walker) {
  int i,j;
  DOUBLE u = 0;
  DOUBLE dr[3]={0,0,0};
  DOUBLE r, r2;
#ifdef SYM_OPPOSITE_SPIN
  DOUBLE p1, p2, s;
#endif

  if(var_par_array) AdjustVariationalParameter(Walker.varparindex);

#ifdef ONE_BODY_TRIAL_TERMS
  u += OneBodyUWalker(Walker, ON, ON);
#endif

#ifdef JASTROW_SAME_SPIN
  for(i=0; i<Nup; i++) {
    for(j=i+1; j<Nup; j++) {// SPIN UP
      CaseX(dr[0] = Walker.x[i] - Walker.x[j]);
      CaseY(dr[1] = Walker.y[i] - Walker.y[j]);
      CaseZ(dr[2] = Walker.z[i] - Walker.z[j]);

      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);

      if(CheckInteractionCondition(dr[2], r2)) {
        u += InterpolateU11(&G11, sqrt(r2));
      }
    }
  }

  for(i=0; i<Ndn; i++) {
    for(j=i+1; j<Ndn; j++) {// SPIN DOWN
      CaseX(dr[0] = Walker.xdn[i] - Walker.xdn[j]);
      CaseY(dr[1] = Walker.ydn[i] - Walker.ydn[j]);
      CaseZ(dr[2] = Walker.zdn[i] - Walker.zdn[j]);

      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(CheckInteractionCondition(dr[2], r2)) {
        u += InterpolateU22(&G22, sqrt(r2));
      }
    }
  }
#endif

#ifdef JASTROW_OPPOSITE_SPIN
  for(i=0; i<Nup; i++) {
    for(j=0; j<Ndn; j++) {// MIXED terms (of SPIN UP with SPIN DOWN)
      // calculate new value of the wave function
      CaseX(dr[0] = Walker.x[i] - Walker.xdn[j]);
      CaseY(dr[1] = Walker.y[i] - Walker.ydn[j]);
      CaseZ(dr[2] = Walker.z[i] - Walker.zdn[j]);
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(CheckInteractionCondition(dr[2], r2)) {
        r = sqrt(r2);
        u += InterpolateU12(&G12, r);
      }
    }
  }
#endif

#ifdef SYM_OPPOSITE_SPIN // MIXED terms (of SPIN UP with SPIN DOWN)
  p1 = 1.;
  for(i = 0; i<Nup; i++) {
    s = 0.;
    for (j = 0; j<Ndn; j++) {
      CaseX(dr[0] = Walker.x[i] - Walker.xdn[j]);
      CaseY(dr[1] = Walker.y[i] - Walker.ydn[j]);
      CaseZ(dr[2] = Walker.z[i] - Walker.zdn[j]);
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if (CheckInteractionCondition(dr[2], r2)) {
        s += InterpolateSYMF(&GSYM, sqrt(r2));
      }
    }
    p1 *= s;
  }

  p2 = 1;
  for(j = 0; j<Ndn; j++) {
    s = 0;
    for(i = 0; i<Nup; i++) {
      CaseX(dr[0] = Walker.x[i] - Walker.xdn[j]);
      CaseY(dr[1] = Walker.y[i] - Walker.ydn[j]);
      CaseZ(dr[2] = Walker.z[i] - Walker.zdn[j]);
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(CheckInteractionCondition(dr[2], r2)) {
        s += InterpolateSYMF(&GSYM, sqrt(r2));
      }
    }
    p2 *= s;
  }
  u += Log(p1 + p2);
#endif

  return u;
}

/******************** Check Interaction Condition ****************************/
int CheckInteractionCondition(const DOUBLE z, const DOUBLE r2) {

#ifdef BC_3DPBC // 3D homogeneous system
  if(r2 < Lhalf2)
    return 1;
  else
    return 0;
#endif

#ifdef BC_2DPBC // 2D homogeneous system
  if(r2 < Lhalf2)
    return 1;
  else
    return 0;
#endif

#ifdef BC_BILAYER 
  if(r2 < Lhalf2)
    return 1;
  else
    return 0;
#endif

#ifdef BC_1DPBC // 1D homogeneous system
  if(fabs(z)<Lhalf)
    return 1;
  else
    return 0;
#endif

#ifdef BC_ABSENT
  return 1;
#endif
}

int CheckInteractionConditionOrbital(const DOUBLE z, const DOUBLE r2) {

#ifndef L2_CHECK
  return 1;
#else

#ifdef BC_3DPBC // 3D homogeneous system
  if(r2 < Lhalf2)
    return 1;
  else
    return 0;
#endif


#ifdef BC_2DPBC // 2D homogeneous system
  if(r2 < Lhalf2)
    return 1;
  else
    return 0;
#endif

#ifdef BC_BILAYER
  if(r2 < Lhalf2)
    return 1;
  else
    return 0;
#endif

#ifdef BC_1DPBC // 1D homogeneous system
  if(fabs(z)<Lhalf)
    return 1;
  else
    return 0;
#endif

#ifdef BC_ABSENT
  return 1;
#endif
#endif
}

#ifdef ONE_BODY_TRIAL_GP_LHY_1D
  DOUBLE mudimabs2, dmudim2;
#endif
void ConstructOneBodyGPHLY(void) {
#ifdef ONE_BODY_TRIAL_GP_LHY_1D
  DOUBLE xo, Eo, psio, No; // dimensionless units of distance, energy and wavefunction, Eqs. (2,3,4) PRA 98 013631
  DOUBLE g12, dg, g, m = 1.;
  DOUBLE mudim, dmudim, Edim;
  DOUBLE Ndim;
  DOUBLE Ysave, Xsave;
  int i;

  g = -2/aA;
  g12 = -2/a;
  dg = g12 + g;
  if(dg<0.) Error("  expecting dg = %lf > 0\n", dg);

  xo = PI*sqrt(dg) / (sqrt(2.)*m*g*sqrt(g));
  Eo = 1./(m*xo*xo);
  psio = sqrt(2.*m) / (PI*dg) * g*sqrt(g);
  No = psio*psio*xo;

  Ndim = (Nup+Ndn) / No;
  Message("  Constructing GP + LHY solution\n");
  Message("  N / No = %lf\n", Ndim);

  if(Ndim > 10.) {
    dmudim = 8./9. * exp(-2. -3./2.*Ndim);
    Edim = -6./27.*Ndim + 16.*exp(-2.)/27. - 16./27.*exp(-2.-1.5*Ndim);

    Message("  E/N = ", Edim*Eo / Ndim);
  }
  else {
    dmudim = -0.382*pow(Ndim, 2./3.);
    for(i=0; i<100; i++) dmudim = ((4. + 2.*sqrt(2.)*sqrt(2. - 9.*dmudim) - 9.*dmudim)*exp(-(sqrt(2.)*sqrt(2. - 9.*dmudim)) - (3.*Ndim)/2.))/9.;
  }
  mudim = -2./9. + dmudim;

  dmudim2 = 3.*sqrt(dmudim*Eo/2.);
  mudimabs2  = sqrt(-2.*mudim*Eo);

  Message("  mu = %lf, dmu = %e\n", mudim*Eo, dmudim*Eo);

  Xsave = pow(Ndim, -1.28); // small N expansion
  if(Xsave<0.65) Xsave = 0.65; // large N expansion
  Xsave *= xo; // adjust units
  Xsave *= 10.;

  Xsave = 4./9.*(DOUBLE)Nup;

  Xsave = 1./1.1;
  do {
    Ysave = exp(OneBodyU(0., 0., Xsave, 0, 1));
    Xsave *= 1.1;
  }
  while(Ysave > 1e-10);

  SaveOneBodyTerm(-Xsave, Xsave, "wf1body.dat", 1000, 1);
#endif
}

/********************************* One Body U ********************************/
DOUBLE OneBodyU(DOUBLE x, DOUBLE y, DOUBLE z, int i, int up) {
#ifndef ONE_BODY_TRIAL_TERMS // i.e. homogeneous liquid system
  return 0;
#else
  DOUBLE u = 0.;

#ifdef CRYSTAL
#ifdef CRYSTAL_NONSYMMETRIC
  DOUBLE dr[3],r2;
#endif
#endif

#ifdef IMPURITY
  int j;
  DOUBLE dr[3],r2;
#endif
#ifdef ONE_BODY_VORTEX
  DOUBLE dr[3],r2;
#endif

#ifdef ONE_BODY_TRIAL_TRAP // i.e. in a trap
#ifdef TRIAL_3D
  if(up)
    u -= alpha_x_up * x*x + alpha_y_up * y*y + alpha_z_up * z*z;
  else
    u -= alpha_x_dn * x*x + alpha_y_dn * y*y + alpha_z_dn * z*z;
#endif
#ifdef TRIAL_2D
  if(up)
    u -= alpha_x_up * x*x + alpha_y_up * y*y;
  else
    u -= alpha_x_dn * x*x + alpha_y_dn * y*y;
#endif
#ifdef TRIAL_1D
  if(up)
    u -= alpha_z_up * z*z;
  else
    u -= alpha_z_dn * z*z ;
#endif
#endif

#ifdef ONE_BODY_TRIAL_LINEAR_CHAIN_OF_HARMONIC_OSCILLATORS_UP // Gaussians (alpha_x_up,alpha_y_up,alpha_z_up) located at ((i+0.5) L/Nup,0,0) for UP particles
  if(up) {
#ifdef BC_ABSENT // centered at 0
    z -= Lz *((DOUBLE)i - 0.5*((DOUBLE)Nup-1.))/(DOUBLE)Nup;
#else // PBC
    z -= Lz / (DOUBLE)Nup*((DOUBLE)i + 0.5);
    if(z > L_half_z) {
      z -= Lz;
    }
    else if(z < -L_half_z) {
      z += Lz;
    }
#endif
    u -= alpha_x_up * x*x + alpha_y_up * y*y + alpha_z_up * z*z;
  }
#endif

#ifdef ONE_BODY_TRIAL_LINEAR_CHAIN_OF_HARMONIC_OSCILLATORS_DN
  if(!up) {
#ifdef BC_ABSENT // centered at 0
    z -= Lz *((DOUBLE)i - 0.5*((DOUBLE)Ndn-1.))/(DOUBLE)Ndn;
#else // PBC
    z -= Lz / (DOUBLE)Nup*((DOUBLE)i + (DOUBLE)((int)(Nup/2)) + 0.5);
    if(z > L_half_z) {
      z -= Lz;
    }
    else if(z < -L_half_z) {
      z += Lz;
    }
#endif
    u -= alpha_x_dn * x*x + alpha_y_dn * y*y + alpha_z_dn * z*z;
  }
#endif

#ifdef ONE_BODY_TRIAL_TRAP_DOUBLE_GAUSSIAN
#ifdef TRIAL_3D
#error "_DOUBLE_GAUSSIAN not defined in 3D"
#endif
#ifdef TRIAL_2D
#error "_DOUBLE_GAUSSIAN not defined in 2D"
#endif
#ifdef TRIAL_1D
  if(up)
    u += Log(Exp(-alpha_z_up *(z-rGauss_up)*(z-rGauss_up)) + Exp(-alpha_z_up *(z+rGauss_up)*(z+rGauss_up)));
  else
    u += Log(Exp(-alpha_z_dn *(z-rGauss_dn)*(z-rGauss_dn)) + Exp(-alpha_z_dn *(z+rGauss_dn)*(z+rGauss_dn)));
#endif
#endif

#ifdef ONE_BODY_VORTEX
  if(up)
    dr[0] = x - L_half_x - 0.5*lambda;
  else 
    dr[0] = x - L_half_x + 0.5*lambda;
  dr[1] = y - L_half_y;
  dr[2] = 0;
  r2 = FindNearestImage(&dr[0], &dr[1], &dr[2]);
  if(r2<Lhalf2) u += Dpar*Log(sin(PI*sqrt(r2)/L));
#endif

#ifdef INTERPOLATE_SPLINE_ONE_BODY_Z_WF
  u += InterpolateSplineU(&G1, z);
#endif

#ifdef SPECKLES
  u += SpecklesU(x, y);
#endif

#ifdef IMPURITY // impurity
  for(j=0; j<Crystal.size; j++) {
    dr[0] = x - Crystal.x[i];
    dr[1] = y - Crystal.y[i];
    dr[2] = z - Crystal.z[i];
    r2 = FindNearestImage(&dr[0], &dr[1], &dr[2]);
    u += ImpurityInterpolateU(Sqrt(r2));
  }
#endif

#ifdef CRYSTAL
#ifndef CRYSTAL_SYMMETRIC
  if(up) {
    dr[0] = x - Crystal.x[i];
    dr[1] = y - Crystal.y[i];
    dr[2] = z - Crystal.z[i];
    r2 = FindNearestImage(&dr[0], &dr[1], &dr[2]);
#ifdef CRYSTAL_WIDTH_ARRAY
    u -= Crystal.Rx[i] * dr[0]*dr[0] + Crystal.Ry[i] * dr[1]*dr[1] + Crystal.Rz[i] * dr[2]*dr[2];
#else
    u -= Crystal.R * r2;
#endif
  }
  else {
    dr[0] = x - Crystal.xdn[i];
    dr[1] = y - Crystal.ydn[i];
    dr[2] = z - Crystal.zdn[i];
    r2 = FindNearestImage(&dr[0], &dr[1], &dr[2]);
#ifdef CRYSTAL_WIDTH_ARRAY
    u -= Crystal.Rxdn[i] * dr[0]*dr[0] + Crystal.Rydn[i] * dr[1]*dr[1] + Crystal.Rzdn[i] * dr[2]*dr[2];
#else
    u -= Crystal.R * r2;
#endif
  }
#endif
#endif

#ifdef ONE_BODY2_TRIAL_1D_WAVE
  if(!up) u += Log(fabs(Cos(one_body_momentum_k*z)));
#endif

#ifdef ONE_BODY_TRIAL_1D_WAVE_2COMP_OUT_OF_PHASE
  if(up)
    u += Log(fabs(1.+Srecoil*cos(one_body_momentum_k*z)));
  else
    u += Log(fabs(1.+Srecoil*sin(one_body_momentum_k*z)));
#endif

#ifdef ONE_BODY_TRIAL_1D_WAVE_2COMP_OUT_OF_PHASE_KPAR
  if(up) {
    if(cos(one_body_momentum_k*z) > 0.) {
      u += Log(fabs(1. + Srecoil*pow(fabs(cos(one_body_momentum_k*z)), Kpar)));
    }
    else {
      u += Log(fabs(1. - Srecoil*pow(fabs(cos(one_body_momentum_k*z)), Kpar)));
    }
  }
  else {
    if(sin(one_body_momentum_k*z) > 0.) {
      u += Log(fabs(1. + Srecoil*pow(fabs(sin(one_body_momentum_k*z)), Kpar)));
    }
    else {
      u += Log(fabs(1. - Srecoil*pow(fabs(sin(one_body_momentum_k*z)), Kpar)));
    }
  }
#endif

#ifdef ONE_BODY_TRIAL_1D_WAVE_2COMP_INPHASE
  u += Log(fabs(1.+Srecoil*cos(one_body_momentum_k*z)));
#endif

#ifdef ONE_BODY_TRIAL_3D_WAVE_2COMP_OUT_OF_PHASE
  if(up)
    u += Log(fabs((1.+Srecoil*cos(one_body_momentum_k*x))*(1.+Srecoil*cos(one_body_momentum_k*y))*(1.+Srecoil*cos(one_body_momentum_k*z))));
  else
    u += Log(fabs((1.+Srecoil*sin(one_body_momentum_k*x))*(1.+Srecoil*sin(one_body_momentum_k*y))*(1.+Srecoil*sin(one_body_momentum_k*z))));
#endif

#ifdef ONE_BODY_TRIAL_ABC
  u += alpha_latt*Log(fabs(Sin(kL*z)));
  //u += Log(fabs(Sin(kL*z)));
#endif

#ifdef ONE_BODY_SOLITON
  u += 0.5*Log(solitonV2 + (1.-solitonV2)*tanh(solitonk*(z-Lhalf))*tanh(solitonk*(z-Lhalf)));
#endif

#ifdef ONE_BODY_TRIAL_FERMI
  u += log(1./(1. + exp((fabs(z)-Epar)/T)));
#endif

#ifdef ONE_BODY_TRIAL_GP_LHY_1D
  u += log(1./(1. + dmudim2*cosh(mudimabs2*fabs(z))));
#endif

#ifdef ONE_BODY_TRIAL_SIN
#ifdef TRIAL_3D
  u += alpha_latt*log(sin(kL*x)*sin(kL*x) + sin(kL*y)*sin(kL*y) + sin(kL*z)*sin(kL*z));
#endif
#ifdef TRIAL_2D
  u += alpha_latt*log(sin(kL*x)*sin(kL*x) + sin(kL*y)*sin(kL*y));
#endif
#ifdef TRIAL_1D
  u += log(beta_latt+pow(fabs(sin(kL*z)),alpha_latt));
#endif
#endif

  return u;
#endif
}

/********************************* One Body Fp *******************************/
// Fx = [dF / dx] / F
// Fy = [dF / dy] / F
// Fz = [dF / dz] / F
void OneBodyFp(DOUBLE *Fx, DOUBLE *Fy, DOUBLE *Fz, DOUBLE x, DOUBLE y, DOUBLE z, int i, int up) {
#ifdef CRYSTAL
#ifdef CRYSTAL_NONSYMMETRIC // nonsymmetric crystal
  DOUBLE dr[3]={0.,0.,0.},r2;
#endif
#endif
#ifdef ONE_BODY_VORTEX
  DOUBLE dr[3]={0.,0.,0.},r2,r;
#endif
#ifdef IMPURITY // add contribution from the w.f. of impurities
  int j;
#endif

#ifdef ONE_BODY_TRIAL_TRAP
  if(up) {
    CaseX(*Fx -= two_alpha_x_up * x);
    CaseY(*Fy -= two_alpha_y_up * y);
    CaseZ(*Fz -= two_alpha_z_up * z);
  }
  else {
    CaseX(*Fx -= two_alpha_x_dn * x);
    CaseY(*Fy -= two_alpha_y_dn * y);
    CaseZ(*Fz -= two_alpha_z_dn * z);
  }
#endif

#ifdef ONE_BODY_TRIAL_TRAP_DOUBLE_GAUSSIAN
  if(up) 
    *Fz -= two_alpha_z_up * (z - rGauss_up * tanh(two_alpha_z_up * rGauss_up * z));
  else 
    *Fz -= two_alpha_z_dn * (z - rGauss_dn * tanh(two_alpha_z_dn * rGauss_dn * z));
#endif

#ifdef ONE_BODY_TRIAL_LINEAR_CHAIN_OF_HARMONIC_OSCILLATORS_UP // Gaussians (alpha_x_up,alpha_y_up,alpha_z_up) located at ((i+0.5) L/Nup,0,0) for UP particles
  if(up) {
#ifdef BC_ABSENT // centered at 0
    z -= Lz *((DOUBLE)i - 0.5*((DOUBLE)Nup-1.))/(DOUBLE)Nup;
#else // PBC
    z -= Lz / (DOUBLE)Nup*((DOUBLE)i + 0.5);
    if(z > L_half_z) {
      z -= Lz;
    }
    else if(z < -L_half_z) {
      z += Lz;
    }
#endif
    CaseX(*Fx -= two_alpha_x_up * x);
    CaseY(*Fy -= two_alpha_y_up * y);
    CaseZ(*Fz -= two_alpha_z_up * z);
  }
#endif

#ifdef ONE_BODY_TRIAL_LINEAR_CHAIN_OF_HARMONIC_OSCILLATORS_DN
  if(!up) {
#ifdef BC_ABSENT // centered at 0
    z -= Lz *((DOUBLE)i - 0.5*((DOUBLE)Ndn-1.))/(DOUBLE)Ndn;
#else // PBC
    z -= Lz / (DOUBLE)Nup*((DOUBLE)i + (DOUBLE)((int)(Nup/2)) + 0.5);
    if(z > L_half_z) {
      z -= Lz;
    }
    else if(z < -L_half_z) {
      z += Lz;
    }
#endif
    CaseX(*Fx -= two_alpha_x_dn * x);
    CaseY(*Fy -= two_alpha_y_dn * y);
    CaseZ(*Fz -= two_alpha_z_dn * z);
  }
#endif

#ifdef ONE_BODY_VORTEX
  if(up)
    dr[0] = x - L_half_x - 0.5*lambda;
  else 
    dr[0] = x - L_half_x + 0.5*lambda;
  dr[1] = y - L_half_y;
  dr[2] = 0;
  r2 = FindNearestImage(&dr[0], &dr[1], &dr[2]);
  if(r2 < Lhalf2) {
    r = sqrt(r2);
    *Fx += Dpar*PI/L/tan(PI*r/L)*dr[0]/r;
    *Fy += Dpar*PI/L/tan(PI*r/L)*dr[1]/r;
  }
#endif

#ifdef SPECKLES
  SpecklesFp(Fx, Fy, x, y);
#endif

#ifdef INTERPOLATE_SPLINE_ONE_BODY_Z_WF
  Fz += InterpolateSplineFp(&G1, z);
#endif

#ifdef ONE_BODY_TRIAL_SIN
#ifdef TRIAL_3D
  *Fx += alpha_latt*kL*sin(2.*kL*x)/(sin(kL*x)*sin(kL*x) + sin(kL*y)*sin(kL*y) + sin(kL*z)*sin(kL*z));
  *Fy += alpha_latt*kL*sin(2.*kL*y)/(sin(kL*x)*sin(kL*x) + sin(kL*y)*sin(kL*y) + sin(kL*z)*sin(kL*z));
  *Fz += alpha_latt*kL*sin(2.*kL*z)/(sin(kL*x)*sin(kL*x) + sin(kL*y)*sin(kL*y) + sin(kL*z)*sin(kL*z));
#endif
#ifdef TRIAL_2D
  *Fx += alpha_latt*kL*sin(2.*kL*x)/(sin(kL*x)*sin(kL*x) + sin(kL*y)*sin(kL*y));
  *Fy += alpha_latt*kL*sin(2.*kL*y)/(sin(kL*x)*sin(kL*x) + sin(kL*y)*sin(kL*y));
#endif
#ifdef TRIAL_1D
  z = z - (int)(z); // attention!, z is reduces to [0, 1]
  *Fz += alpha_latt*kL*cos(kL*z)*pow(fabs(sin(kL*z)),alpha_latt-1)/(beta_latt+pow(fabs(sin(kL*z)),alpha_latt));
#endif
#endif

#ifdef IMPURITY // add contribution from the w.f. of impurities
  for(j=0; j<Crystal.size; j++) {
    dr[0] = x - Crystal.x[j];
    dr[1] = y - Crystal.y[j];
    dr[2] = z - Crystal.z[j];
    r2 = FindNearestImage(&dr[0], &dr[1], &dr[2]);
    if(CheckInteractionConditionWF(dr[0], dr[1], dr[2], r2)) {
      r = Sqrt(r2);
      //*Ekin += ImpurityInterpolateE(r);
      Force = ImpurityInterpolateFp(r)/r;
      *Fx += Force * dr[0];
      *Fy += Force * dr[1];
      *Fz += Force * dr[2];
    }
  }
#endif

#ifdef CRYSTAL
#ifndef CRYSTAL_SYMMETRIC
  if(up) {
    CaseX(dr[0] = x - Crystal.x[i]);
    CaseY(dr[1] = y - Crystal.y[i]);
    CaseZ(dr[2] = z - Crystal.z[i]);
    r2 = FindNearestImage(&dr[0], &dr[1], &dr[2]);
#ifdef CRYSTAL_WIDTH_ARRAY
    CaseX(*Fx -= 2.*Crystal.Rx[i] * dr[0]);
    CaseY(*Fy -= 2.*Crystal.Ry[i] * dr[1]);
    CaseZ(*Fz -= 2.*Crystal.Rz[i] * dr[2]);
#else
    CaseX(*Fx -= 2.*Crystal.R * dr[0]);
    CaseY(*Fy -= 2.*Crystal.R * dr[1]);
    CaseZ(*Fz -= 2.*Crystal.R * dr[2]);
#endif
  }
  else {
    CaseX(dr[0] = x - Crystal.xdn[i]);
    CaseY(dr[1] = y - Crystal.ydn[i]);
    CaseZ(dr[2] = z - Crystal.zdn[i]);
    r2 = FindNearestImage(&dr[0], &dr[1], &dr[2]);
#ifdef CRYSTAL_WIDTH_ARRAY
    CaseX(*Fx -= 2.*Crystal.Rxdn[i] * dr[0]);
    CaseY(*Fy -= 2.*Crystal.Rydn[i] * dr[1]);
    CaseZ(*Fz -= 2.*Crystal.Rzdn[i] * dr[2]);
#else
    CaseX(*Fx -= 2.*Crystal.R * dr[0]);
    CaseY(*Fy -= 2.*Crystal.R * dr[1]);
    CaseZ(*Fz -= 2.*Crystal.R * dr[2]);
#endif
  }

#endif
#endif

#ifdef ONE_BODY2_TRIAL_1D_WAVE
  if(!up) *Fz -= one_body_momentum_k*Tg(one_body_momentum_k*z);
#endif

#ifdef ONE_BODY_TRIAL_1D_WAVE_2COMP_INPHASE
  *Fz -= one_body_momentum_k*Srecoil*sin(one_body_momentum_k*z) / (1.+Srecoil*cos(one_body_momentum_k*z));
#endif

#ifdef ONE_BODY_TRIAL_1D_WAVE_2COMP_OUT_OF_PHASE
  if(up)
    *Fz -= one_body_momentum_k*Srecoil*sin(one_body_momentum_k*z) / (1.+Srecoil*cos(one_body_momentum_k*z));
  else
    *Fz += one_body_momentum_k*Srecoil*cos(one_body_momentum_k*z) / (1.+Srecoil*sin(one_body_momentum_k*z));
#endif

#ifdef ONE_BODY_TRIAL_1D_WAVE_2COMP_OUT_OF_PHASE_KPAR
  if(up) {
    if(cos(one_body_momentum_k*z) > 0.) {
      *Fz -= one_body_momentum_k*Kpar*Srecoil*tan(one_body_momentum_k*z) / (Srecoil+pow(cos(one_body_momentum_k*z),-Kpar));
    }
    else {
      *Fz -= one_body_momentum_k*Kpar*Srecoil*tan(one_body_momentum_k*z) / (Srecoil-pow(-cos(one_body_momentum_k*z),-Kpar));
    }
  }
  else {
    if(sin(one_body_momentum_k*z) > 0.) {
      *Fz += one_body_momentum_k*Kpar*Srecoil/tan(one_body_momentum_k*z) / (Srecoil+pow(sin(one_body_momentum_k*z),-Kpar));
    }
    else {
      *Fz += one_body_momentum_k*Kpar*Srecoil/tan(one_body_momentum_k*z) / (Srecoil-pow(-sin(one_body_momentum_k*z),-Kpar));
    }
  }
#endif

#ifdef ONE_BODY_TRIAL_3D_WAVE_2COMP_OUT_OF_PHASE
  if(up) {
    *Fx -= one_body_momentum_k*Srecoil*sin(one_body_momentum_k*x) / (1.+Srecoil*cos(one_body_momentum_k*x));
    *Fy -= one_body_momentum_k*Srecoil*sin(one_body_momentum_k*y) / (1.+Srecoil*cos(one_body_momentum_k*y));
    *Fz -= one_body_momentum_k*Srecoil*sin(one_body_momentum_k*z) / (1.+Srecoil*cos(one_body_momentum_k*z));
  }
  else {
    *Fx += one_body_momentum_k*Srecoil*cos(one_body_momentum_k*x) / (1.+Srecoil*sin(one_body_momentum_k*x));
    *Fy += one_body_momentum_k*Srecoil*cos(one_body_momentum_k*y) / (1.+Srecoil*sin(one_body_momentum_k*y));
    *Fz += one_body_momentum_k*Srecoil*cos(one_body_momentum_k*z) / (1.+Srecoil*sin(one_body_momentum_k*z));
  }
#endif

#ifdef ONE_BODY_TRIAL_ABC
  *Fz += alpha_latt*kL/Tg(kL*z);
#endif

#ifdef ONE_BODY_TRIAL_FERMI
  *Fz -= exp((fabs(z) - Epar) / T) / ((1. + exp((fabs(z) - Epar) / T))*T);
#endif

#ifdef ONE_BODY_TRIAL_GP_LHY_1D
  *Fz -= dmudim2*mudimabs2*sinh(mudimabs2*fabs(z)) / (1. + dmudim2*cosh(mudimabs2*fabs(z)));
#endif
}

/********************************* One Body Eloc *****************************/
// Eloc = [-f_x"/f+(f_x'/f)^2] + [-f_y"/f+(f_y'/f)^2] + [-f_z"/f+(f_z'/f)^2]
DOUBLE OneBodyE(DOUBLE x, DOUBLE y, DOUBLE z, int i, int up) {
  DOUBLE E = 0.;

#ifdef IMPURITY // add contribution from the w.f. of impurities
  int j;
#endif
#ifdef ONE_BODY_VORTEX
  DOUBLE dr[3]={0.,0.,0.},r2,r;
#endif

#ifdef ONE_BODY_TRIAL_TRAP
  if(up) { //E += m_up_inv*(two_alpha_x_up + two_alpha_y_up + two_alpha_z_up);
    CaseX(E += m_up_inv*two_alpha_x_up);
    CaseY(E += m_up_inv*two_alpha_y_up);
    CaseZ(E += m_up_inv*two_alpha_z_up);
  }
  else { // E += m_dn_inv*(two_alpha_x_dn + two_alpha_y_dn + two_alpha_z_dn);
    CaseX(E += m_dn_inv*two_alpha_x_dn);
    CaseY(E += m_dn_inv*two_alpha_y_dn);
    CaseZ(E += m_dn_inv*two_alpha_z_dn);
  }
#endif

#ifdef ONE_BODY_TRIAL_TRAP_DOUBLE_GAUSSIAN
  if(up)
    E += m_up_inv*two_alpha_z_up*(1. - two_alpha_z_up*rGauss_up*rGauss_up / cosh(two_alpha_z_up*rGauss_up*z)/ cosh(two_alpha_z_up*rGauss_up*z));
  else 
    E += m_dn_inv*two_alpha_z_dn*(1. - two_alpha_z_dn*rGauss_dn*rGauss_dn / cosh(two_alpha_z_dn*rGauss_dn*z)/ cosh(two_alpha_z_dn*rGauss_dn*z));
#endif

#ifdef ONE_BODY_TRIAL_LINEAR_CHAIN_OF_HARMONIC_OSCILLATORS_UP // Gaussians (alpha_x_up,alpha_y_up,alpha_z_up) located at ((i+0.5) L/Nup,0,0) for UP particles
  if(up) {
    CaseX(E += m_up_inv*two_alpha_x_up);
    CaseY(E += m_up_inv*two_alpha_y_up);
    CaseZ(E += m_up_inv*two_alpha_z_up);
  }
#endif

#ifdef ONE_BODY_TRIAL_LINEAR_CHAIN_OF_HARMONIC_OSCILLATORS_DN
  if(!up) {
    CaseX(E += m_dn_inv*two_alpha_x_dn);
    CaseY(E += m_dn_inv*two_alpha_y_dn);
    CaseZ(E += m_dn_inv*two_alpha_z_dn);
  }
#endif

#ifdef ONE_BODY_VORTEX
  if(up)
    dr[0] = x - L_half_x - 0.5*lambda;
  else 
    dr[0] = x - L_half_x + 0.5*lambda;
  dr[1] = y - L_half_y;
  dr[2] = 0;
  r2 = FindNearestImage(&dr[0], &dr[1], &dr[2]);
  r = sqrt(r2);
  if(r2 < Lhalf2) {
    E += Dpar*PI*(-L/tan(PI*r/L) + pi*r/(sin(PI*r/L)*sin(PI*r/L))) / (L2*r);
  }
#endif

#ifdef SPECKLES
  E += SpecklesFpp(x,y);
#endif

#ifdef INTERPOLATE_SPLINE_ONE_BODY_Z_WF
  E += InterpolateSplineE(&G1, z);
#endif

#ifdef ONE_BODY_TRIAL_SIN
#ifdef TRIAL_3D
  E += (4.*kL*kL*alpha_latt*(3. - 3.*cos(2.*kL*x) + cos(2.*kL*(x - y)) - 3.*cos(2.*kL*y) + cos(2.*kL*(x + y)) 
        + cos(2.*kL*(x - z)) + cos(2.*kL*(y - z)) - 3.*cos(2.*kL*z) + cos(2.*kL*(x + z)) + cos(2.*kL*(y + z))))
       /(cos(2.*kL*x) + cos(2.*kL*y) + cos(2.*kL*z) - 3.)
       /(cos(2.*kL*x) + cos(2.*kL*y) + cos(2.*kL*z) - 3.);
#endif
#ifdef TRIAL_2D
  E += 4.*kL*kL*alpha_latt*(-2.*cos(2.*kL*x) + cos(2.*kL*(x - y)) + cos(2.*kL*(x + y)) + 4.*sin(kL*y)*sin(kL*y))
       /(cos(2.*kL*x) + cos(2.*kL*y)-2.)
       /(cos(2.*kL*x) + cos(2.*kL*y)-2.);
#endif
#ifdef TRIAL_1D
  z = z - (floor)(z); // attention!, z is reduces to [0, 1], also negative z
  E +=  -0.5*(alpha_latt*kL*kL*pow(sin(kL*z),alpha_latt-2.)*(beta_latt*(-2. + alpha_latt + alpha_latt*cos(2.*kL*z)) 
           - 2.*pow(sin(kL*z),alpha_latt)))
           / (beta_latt + pow(sin(kL*z),alpha_latt))
           / (beta_latt + pow(sin(kL*z),alpha_latt));
#endif
#endif

#ifdef IMPURITY // add contribution from the w.f. of impurities
  for(j=0; j<Crystal.size; j++) {
    dr[0] = x - Crystal.x[j];
    dr[1] = y - Crystal.y[j];
    dr[2] = z - Crystal.z[j];
    r2 = FindNearestImage(&dr[0], &dr[1], &dr[2]);
    if(CheckInteractionConditionWF(dr[0], dr[1], dr[2], r2)) {
      r = Sqrt(r2);
      E += ImpurityInterpolateE(r);
    }
  }
#endif

#ifdef CRYSTAL
#ifndef CRYSTAL_SYMMETRIC
#ifdef TRIAL_1D
#ifdef CRYSTAL_WIDTH_ARRAY
  if(up)
    E += 2.*Crystal.Rz[i];
  else
    E += 2.*Crystal.Rzdn[i];
#else
  E += 2.*Crystal.R;
#endif
#endif
#ifdef TRIAL_2D
#ifdef CRYSTAL_WIDTH_ARRAY
  if(up)
    E += 2.*(Crystal.Rx[i] + Crystal.Ry[i]);
  else
    E += 2.*(Crystal.Rxdn[i] + Crystal.Rydn[i]);
#else
  E += 4.*Crystal.R;
#endif
#endif
#ifdef TRIAL_3D
#ifdef CRYSTAL_WIDTH_ARRAY
  if(up)
    E += 2.*(Crystal.Rx[i] + Crystal.Ry[i] + Crystal.Rz[i]);
  else
    E += 2.*(Crystal.Rxdn[i] + Crystal.Rydn[i] + Crystal.Rzdn[i]);
#else
  E += 6.*Crystal.R;
#endif
#endif
#endif
#endif

#ifdef ONE_BODY2_TRIAL_1D_WAVE
  if(!up) E += one_body_momentum_k*one_body_momentum_k*(1.+Tg(one_body_momentum_k*z)*Tg(one_body_momentum_k*z))*m_dn_inv;
#endif

#ifdef ONE_BODY_TRIAL_1D_WAVE_2COMP_INPHASE
  E += one_body_momentum_k*one_body_momentum_k*Srecoil*(Srecoil+cos(one_body_momentum_k*z)) / ((1.+Srecoil*cos(one_body_momentum_k*z))*(1.+Srecoil*cos(one_body_momentum_k*z)));
#endif

#ifdef ONE_BODY_TRIAL_1D_WAVE_2COMP_OUT_OF_PHASE
  if(up)
    E += one_body_momentum_k*one_body_momentum_k*Srecoil*(Srecoil+cos(one_body_momentum_k*z)) / ((1.+Srecoil*cos(one_body_momentum_k*z))*(1.+Srecoil*cos(one_body_momentum_k*z)));
  else
    E += one_body_momentum_k*one_body_momentum_k*Srecoil*(Srecoil+sin(one_body_momentum_k*z)) / ((1.+Srecoil*sin(one_body_momentum_k*z))*(1.+Srecoil*sin(one_body_momentum_k*z)));
#endif

#ifdef ONE_BODY_TRIAL_1D_WAVE_2COMP_OUT_OF_PHASE_KPAR
  if(up) {
    if(cos(one_body_momentum_k*z) > 0.) {
      E += (one_body_momentum_k*one_body_momentum_k*Kpar*Srecoil*pow(cos(one_body_momentum_k*z),Kpar-2.)*(2. - Kpar + 2*Srecoil*pow(cos(one_body_momentum_k*z),Kpar) + Kpar*cos(2.*one_body_momentum_k*z)))/(2.*pow(1 + Srecoil*pow(cos(one_body_momentum_k*z),Kpar),2.));
    }
    else {
      E += (-one_body_momentum_k*one_body_momentum_k*Kpar*Srecoil*pow(-cos(one_body_momentum_k*z),Kpar-2.)*(2. - Kpar - 2*Srecoil*pow(-cos(one_body_momentum_k*z),Kpar) + Kpar*cos(2.*one_body_momentum_k*z)))/(2.*pow(1 - Srecoil*pow(-cos(one_body_momentum_k*z),Kpar),2.));
    }
  }
  else {
    if(sin(one_body_momentum_k*z) > 0.) {
      E += -(one_body_momentum_k*one_body_momentum_k*Kpar*Srecoil*pow(sin(one_body_momentum_k*z),-2. + Kpar)*(-2. + Kpar + Kpar*cos(2.*one_body_momentum_k*z) - 2.*Srecoil*pow(sin(one_body_momentum_k*z),Kpar)))/(2.*pow(1. + Srecoil*pow(sin(one_body_momentum_k*z),Kpar),2.));
    }
    else {
      E += (one_body_momentum_k*one_body_momentum_k*Kpar*Srecoil*pow(-sin(one_body_momentum_k*z),-2. + Kpar)*(-2. + Kpar + Kpar*cos(2.*one_body_momentum_k*z) + 2.*Srecoil*pow(-sin(one_body_momentum_k*z),Kpar)))/(2.*pow(1. - Srecoil*pow(-sin(one_body_momentum_k*z),Kpar),2.));
    }
  }
#endif

#ifdef ONE_BODY_TRIAL_3D_WAVE_2COMP_OUT_OF_PHASE
  if(up) {
    E += one_body_momentum_k*one_body_momentum_k*Srecoil*(Srecoil+cos(one_body_momentum_k*x)) / ((1.+Srecoil*cos(one_body_momentum_k*x))*(1.+Srecoil*cos(one_body_momentum_k*x)));
    E += one_body_momentum_k*one_body_momentum_k*Srecoil*(Srecoil+cos(one_body_momentum_k*y)) / ((1.+Srecoil*cos(one_body_momentum_k*y))*(1.+Srecoil*cos(one_body_momentum_k*y)));
    E += one_body_momentum_k*one_body_momentum_k*Srecoil*(Srecoil+cos(one_body_momentum_k*z)) / ((1.+Srecoil*cos(one_body_momentum_k*z))*(1.+Srecoil*cos(one_body_momentum_k*z)));
  }
  else {
    E += one_body_momentum_k*one_body_momentum_k*Srecoil*(Srecoil+sin(one_body_momentum_k*x)) / ((1.+Srecoil*sin(one_body_momentum_k*x))*(1.+Srecoil*sin(one_body_momentum_k*x)));
    E += one_body_momentum_k*one_body_momentum_k*Srecoil*(Srecoil+sin(one_body_momentum_k*y)) / ((1.+Srecoil*sin(one_body_momentum_k*y))*(1.+Srecoil*sin(one_body_momentum_k*y)));
    E += one_body_momentum_k*one_body_momentum_k*Srecoil*(Srecoil+sin(one_body_momentum_k*z)) / ((1.+Srecoil*sin(one_body_momentum_k*z))*(1.+Srecoil*sin(one_body_momentum_k*z)));
  }
#endif

#ifdef ONE_BODY_TRIAL_ABC
  E += kL*kL*alpha_latt/(Sin(kL*z)*Sin(kL*z));
#endif

#ifdef ONE_BODY_TRIAL_FERMI 
  {
  double g;

  g = 2.*T*cosh(0.5*(Epar-fabs(z)) / T);
  E += 1./(g*g);
  }
#endif

#ifdef ONE_BODY_TRIAL_GP_LHY_1D
  E += dmudim2*mudimabs2*mudimabs2*(dmudim2+cosh(mudimabs2*fabs(z))) / (1. + dmudim2*cosh(mudimabs2*fabs(z))) / (1. + dmudim2*cosh(mudimabs2*fabs(z)));
#endif

  return E;
}

/***************************** ClassicalMoveOneByOne *******************************/
void ClassicalMoveOneByOne(int w) {
  int i,j;
  DOUBLE xi;
  DOUBLE Wp[3] = {0,0,0};
  DOUBLE dr[3] = {0,0,0};
  DOUBLE r2;
  DOUBLE x,y,z;
  DOUBLE dx,dy,dz;
  DOUBLE dE;
  int overlap = OFF;
  int move_accepted;

  // Move particle np of the walker w
  for(i=0; i<Nup; i++) {
    dE = 0.;

    x = W[w].x[i]; // old position
    y = W[w].y[i];
    z = W[w].z[i];

    // Wp is the trial position
    RandomNormal3(&dx, &dy, &dz, 0., Sqrt(dt1));
    Wp[0] = x + dx; // new position
    Wp[1] = y + dy;
    Wp[2] = z + dz;

    ReduceToTheBox(Wp);

    // A-A interaction
    for(j=0; j<Nup; j++) {
      if(j != i) {
        // calculate new value of energy
        dr[0] = Wp[0] - W[w].x[j];
        dr[1] = Wp[1] - W[w].y[j];
        dr[2] = Wp[2] - W[w].z[j];
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef CHECK_HARDCORE_RADIUS
        if(r2<HARDCORE_RADIUS_SQUARED_11) overlap = ON;
#endif
        if(CheckInteractionCondition(dr[2], r2)) dE += InteractionEnergy11(Sqrt(r2));

        // calculate old value of the energy
        dr[0] = x - W[w].x[j];
        dr[1] = y - W[w].y[j];
        dr[2] = z - W[w].z[j];
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
        if(CheckInteractionCondition(dr[2], r2)) dE -= InteractionEnergy11(Sqrt(r2));
      }
    }

    // A-B interaction
    for(j=0; j<Ndn; j++) {
      // calculate new value of energy
      dr[0] = Wp[0] - W[w].xdn[j];
      dr[1] = Wp[1] - W[w].ydn[j];
      dr[2] = Wp[2] - W[w].zdn[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef CHECK_HARDCORE_RADIUS
      if(r2<HARDCORE_RADIUS_SQUARED_12) overlap = ON;
#endif
      if(CheckInteractionCondition(dr[2], r2)) dE += InteractionEnergy12(Sqrt(r2));

      // calculate old value of the energy
      dr[0] = x - W[w].xdn[j];
      dr[1] = y - W[w].ydn[j];
      dr[2] = z - W[w].zdn[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(CheckInteractionCondition(dr[2], r2)) dE -= InteractionEnergy12(Sqrt(r2));
    }

    // The Metropolis code
    if(dE <= 0.) {
      move_accepted = ON;
    }
    else {
      xi = Random();
      if(xi<Exp(-dE/T)) {
        move_accepted = ON;
      }
      else {
        move_accepted = OFF;
      }
    }

#ifdef CHECK_HARDCORE_RADIUS
    if(overlap == ON) {
      move_accepted = OFF;
      overlaped++;
    }
#endif

    if(move_accepted) {
      accepted++;
      W[w].x[i] = Wp[0];
      W[w].y[i] = Wp[1];
      W[w].z[i] = Wp[2];
      W[w].E += dE;

      if(measure_SD) { // update SD with dR = chi + F(R)dt
        CaseX(W[w].rreal[i][0] += dx);
        CaseY(W[w].rreal[i][1] += dy);
        CaseZ(W[w].rreal[i][2] += dz);
      }
    }
    else {
      rejected++;
    }
  }

  // Move particle np of the walker w
  overlap = OFF;
  for(i=0; i<Ndn; i++) {
    dE = 0.;

    x = W[w].xdn[i];
    y = W[w].ydn[i];
    z = W[w].zdn[i];

    // Wp is the trial position
    RandomNormal3(&dx, &dy, &dz, 0., Sqrt(dt2));

    Wp[0] = x + dx;
    Wp[1] = y + dy;
    Wp[2] = z + dz;

    ReduceToTheBox(Wp);

    // species B-B
    for(j=0; j<Ndn; j++) {
      if(j != i) {
        // calculate new value of energy
        dr[0] = Wp[0] - W[w].xdn[j];
        dr[1] = Wp[1] - W[w].ydn[j];
        dr[2] = Wp[2] - W[w].zdn[j];
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef CHECK_HARDCORE_RADIUS
        if(r2<HARDCORE_RADIUS_SQUARED_22) overlap = ON;
#endif
        if(CheckInteractionCondition(dr[2], r2)) dE += InteractionEnergy22(Sqrt(r2));

        // calculate old value of the energy
        dr[0] = x - W[w].xdn[j];
        dr[1] = y - W[w].ydn[j];
        dr[2] = z - W[w].zdn[j];
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
        if(CheckInteractionCondition(dr[2], r2)) dE -= InteractionEnergy22(Sqrt(r2));
      }
    }

    // species AB
    for(j=0; j<Nup; j++) {
      // calculate new value of energy
      dr[0] = Wp[0] - W[w].x[j];
      dr[1] = Wp[1] - W[w].y[j];
      dr[2] = Wp[2] - W[w].z[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef CHECK_HARDCORE_RADIUS
      if(r2 < HARDCORE_RADIUS_SQUARED_12) overlap = ON;
#endif
      if(CheckInteractionCondition(dr[2], r2)) dE += InteractionEnergy12(Sqrt(r2));

      // calculate old value of the energy
      dr[0] = x - W[w].x[j];
      dr[1] = y - W[w].y[j];
      dr[2] = z - W[w].z[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(CheckInteractionCondition(dr[2], r2)) dE -= InteractionEnergy12(Sqrt(r2));
    }
    // The Metropolis code
    if(dE <= 0.) {
      move_accepted = ON;
    }
    else {
      xi = Random();
      if(xi<Exp(-dE/T)) {
        move_accepted = ON;
      }
      else {
        move_accepted = OFF;
      }
    }

#ifdef CHECK_HARDCORE_RADIUS
    if(overlap == ON) {
      move_accepted = OFF;
      overlaped++;
    }
#endif

    if(move_accepted) {
      accepted++;
      W[w].xdn[i] = Wp[0];
      W[w].ydn[i] = Wp[1];
      W[w].zdn[i] = Wp[2];
      W[w].E += dE;
      /*if(measure_SD) { // update SD with dR = chi + F(R)dt
        CaseX(W[w].rreal[i][0] += dx);
        CaseY(W[w].rreal[i][1] += dy);
        CaseZ(W[w].rreal[i][2] += dz);
      }*/
    }
    else {
      rejected++;
    }
  }
}
