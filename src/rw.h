/*rw.h*/

#ifndef _RW_H_
#define _RW_H_

#include "main.h"

int LoadParameters(void);
int LoadCoordinates(void);
int LoadVelocities(void);
int LoadParametersSk(void);
int LoadCrystalCoordinates(void);

int SaveCoordinates(void);
int SaveVelocities(void);
int SaveEnergyVMC(DOUBLE E, DOUBLE EFF);
int SaveEnergyDMC(DOUBLE E);
int SaveEnergyPIGS(void);
int SaveOBDM(void);
int SaveOBDM_PathIntegral(void);
int SaveOBDMMatrix(void);
int SaveTBDMMatrix(void);
int SavePairDistribution(void);
int SavePairDistributionPure(void);
int SaveRadialDistribution(void);
int SaveR2(DOUBLE f);
int SaveZ2(DOUBLE f);
int SavePureR2(DOUBLE f);
int SavePureZ2(DOUBLE f);
int SaveSuperfluidDensity(void);
int SaveWaveFunction11(struct Grid *G, DOUBLE min, DOUBLE max, char *file_out);
int SaveWaveFunction22(struct Grid *G, DOUBLE min, DOUBLE max, char *file_out);
int SaveWaveFunction12(struct Grid *G, DOUBLE min, DOUBLE max, char *file_out);
int SaveBCSWaveFunction(DOUBLE min, DOUBLE max, char *file_out);
int SaveWaveFunction12SYM(struct Grid *G, DOUBLE min, DOUBLE max, char *file_out);
int SaveOneBodyTerm(DOUBLE min, DOUBLE max, char *file_out, int N, int up);
int SaveOrderParameter(void);
int StaticStructureFactor(void);
int SaveStaticStructureFactorAngularPart(void);
int SaveMomentumDistribution(void);
int SaveLindemannRatio(void);
int SaveVarParWeights(void);
int SaveOrbitalCuts(void);
int SaveEnergyCut(void);
int SaveFormFactor(void);
int SaveHyperRadius(void);
int SaveEnergyCLS(void);
int SaveEnergyMD(void);
int SavePairDistribtuionMatrix(void);
int SaveVext(void);
void CopyPureCoordinates(void);
int SavePureCoordinates(void);
int SaveEpotPure(void);
int SaveXY2Matrix(void);

// functions that call other functions are of void type
void SaveBlockData(int);
void SaveMeanR2(void);
void SaveMeanR2DMC(void);

DOUBLE IntegrateMomentum(DOUBLE p);

#define Fopen(out, fname, mode, text) \
  out = fopen(fname, mode);\
  if(out == NULL) {        \
    Message("Can't load/save %s from/to file %s\n", text, fname); \
    perror("\nError:");    \
    return 1;\
  }

#ifndef USE_UTF8
#  define CHECK(text, type, address)  {if(strcmp(ch,text)==0) { fscanf(in, type, address); Message("\n  %s" type, text, *address);    }  }
#  define CHECKs(text, type, address) {if(strcmp(ch,text)==0) { fscanf(in, type, address); Message("\n  %s" type, text, address);    }  }
#else // UTF in windows 
#ifdef _WIN32
#  define CHECK(text, type, address)  {if(wcscmp(ch,L##text)==0) { fwscanf(in, L##type, address); Message("\n  %s" type, L##text, *address);    }  }
#  define CHECKs(text, type, address) {if(wcscmp(ch,L##text)==0) { fwscanf(in, type, address); Message("\n  %s %S" , L##text, address);    }  }
#else
// example CHECK("MC=","%i", &MC); ch - wide char
#  define CHECK(searching_for, type, address) if(strcmp(ch,searching_for)==0) { fwscanf(in, L##type, address); Message("\n  %s" type, searching_for, *address); }
//#define CHECK(searching_for, type, address) {wcscpy(string_wide,L"MC=\n"); Message("\nwide '%s' searching for '%s'", ch, searching_for); if(wcscmp(ch,"MC=\n")==0) {Message("match!!!\n");} else {Message("no match\n");} }
//#define CHECK(searching_for, type, address) {wcstombs(string_not_wide, ch, maximal_string_length); Message("\nwide '%s' searching for '%s' ", ch, searching_for); if(strcmp(string_not_wide,"MC=")==0) {Message("match!!!\n");} else {Message("no match\n");} }
//#define CHECK(searching_for, type, address) { if(wcscmp(ch,L"MC=")==0) {Message("match!");}  }
//#define CHECK(searching_for, type, address) { if(strcmp(ch,searching_for)==0) {fwscanf(in, L##type, address); Message("\nmatch for %s", searching_for);    }  }
//#define CHECK(text, type, address)  {wcstombs(string_not_wide, ch, 20); Message("wide '%s' -> char '%s' being compare to %s\n", ch, string_not_wide, text); if(strcmp(ch,text)==0) { fwscanf(in, L##type, address); Message("match");    }  }
//#define CHECK(text, type, address)  {if(wcscmp(ch,L##text)==0) { fwscanf(in, L##type, address); Message("\n  %s" type, L##text, *address);    }  }
//#define CHECK(text, type, address)  {if(wcscmp(ch,L##text)==0) { fwscanf(in, L##type, address); Message("\n  %s" type, text, *address);    }  }

#  define CHECKs(text, type, address) {if(strcmp(ch,text)==0) { fwscanf(in, L##type, address); Message("\n  %s" type, text, address);    }  }
//#define CHECKs(text, type, address)
//#define CHECKs(text, type, address) {if(wcscmp(ch,L##text)==0) { fwscanf(in, L##type, string_wide); wcstombs(string_not_wide, string_wide, 20); strcpy(address, string_not_wide); Message("\n  %s" type, text, address);    }  }
//#define CHECKs(text, type, address) {if(strcmp(ch,text)==0) { fscanf(in, type, address); Message("\n  %s" type, text, address);    }  }
// Example  CHECKs("file_particles=", "%s", file_particles);
//#define CHECKs(text, type, address) {if(strcmp(ch,text)==0) { fwscanf(in, L##type, address); wcstombs(string_wide, string_not_wide, 20); strcpy(address, string_not_wide); Message("\n  %s" type, text, address);    }  }
//#define CHECKs(text, type, address) {if(wstrcmp(ch,L##text)==0) { fwscanf(string_wide, L##type, address); wcstombs(string_wide, string_not_wide, 20); strcpy(address, string_not_wide); Message("\n  %s" type, text, address);    }  }
//#define CHECKs(text, type, address) {if(strcmp(ch,text)==0) { fscanf(in, type, address); Message("\n  %s" type, text, address);    }  }
//#define CHECKs(text, type, address) {if(wcscmp(ch,L##text)==0) { fwscanf(in, type, address); Message("\n  %s %s" , L##text, address);    }  }
//#define CHECKs(text, type, address) {if(wcscmp(ch,L##text)==0) { fwscanf(in, L##type, address); Message("\n  %s %s" , L##text, address);    }  }
#endif
#endif

#endif // _RW_H_
