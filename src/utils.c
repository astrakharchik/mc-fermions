/*utils.c*/

#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <math.h>

#include "main.h"
#ifdef USE_UTF8
#  include <wchar.h>
#  include <fcntl.h>
#ifdef _WIN32
#  include <io.h>
#endif
#endif

#include "utils.h"
#include "compatab.h"
#include "memory.h"

#ifdef MPI
#  include "mpi.h"
#  include "parallel.h"
#endif

/************************************ Error ********************************/
void Error(const char * format, ...) {
  va_list arg;
  FILE *out;
  char text[1000];

#ifndef MS
  out = fopen(OUTPATH "MC.log","a");
#else
  char num[2]=" ";
  num[0] = '0' + MPI_myid;
  strcpy(text, OUTPATH "MC");
  strcat(text, num);
  strcat(text, ".log");
  out = fopen(text, "a");
#endif

  va_start(arg, format);
  vsprintf(text, format, arg);
  va_end(arg);

  fprintf(stderr,"\nFATAL ERROR:\n");
  fprintf(stderr, "%s\n", text);

  fprintf(out, "\nFATAL ERROR:\n %s", text);
  fclose(out);

#ifdef MPI
// MPI_Finalize();
#endif

  exit(0);
}

/************************************** Warning ****************************/
#ifndef USE_UTF8
void Message(const char * format, ...) {
  char text[1000];
#else // UTF

void WMessage(const wchar_t * format, ...) {
  wchar_t text[1000];
#endif
  va_list arg;
  static int initialized = OFF;
  static int CR = ON, x=20;
  FILE *out;

#ifndef MPI
#ifndef USE_UTF8
  out = fopen(OUTPATH "MC.log", initialized++ ? "a" : "w");
#else // UTF
#ifdef _WIN32 // windows
  _setmode(_fileno(stdout), _O_U16TEXT);
#endif
  out = fopen("MC.log", initialized++ ? "a,ccs=UTF-8" : "w,ccs=UTF-8");
#endif
#else
  char num[2]=" ";
  num[0] = '0' + MPI_myid;
  strcpy(text, OUTPATH "MC");
  strcat(text, num);
  strcat(text, ".log");
  out = fopen(text, initialized++?"a":"w");
#endif

  va_start(arg, format);

#ifndef USE_UTF8
  vsprintf(text, format, arg);
#else // UTF
  vswprintf(text, 1000, format, arg);
#endif

  if(video == OFF) {
#ifndef USE_UTF8
    printf("%s", text);
#else // UTF
    fputws(text, stdout);
    //wprintf(L"%s", text);
    //va_end ( arg );
#endif
  }
  else{
    if(CR) {
      setcolor(BLACK);
      bar(0,0, 1200, 20);
    }
    setcolor(WHITE);
    outtextxy(x,1,text);
#ifndef USE_UTF8
    if(text[strlen(text)-1] != '\n') {
      CR = OFF;
      x += strlen(text)*9;
    }
    else {
      CR = ON;
      x = 20;
    }
#endif
  }

  if(out) {
#ifndef USE_UTF8

#ifdef MPI
    fprintf(out, "%i %s", MPI_myid, text);
#else
    fprintf(out, "%s", text);
#endif

#else // UTF
    //fwprintf(out, L"%s", text);
    fputws(text, out);
#endif

    fclose(out);
  }
}

/********************************** Check Mantissa **************************/
/*

mantissa
        linux     windows/debug  windows/release
        gcc icc   icc visual     icc visual
float   6   18    6   6          14  14
double  14  18    14  14         14  14
long    18  18    14  14         14  14 */
void CheckMantissa(void) {
  int i;
  float a1 = 1.1f;
  double a2 = 1.1;
  long double a3 = 1.1;
  DOUBLE a4 = 1.1;
  int l1 = 0;
  int l2 = 0;
  int l3 = 0;
  int l4 = 0;
  double d1,d2,d3,d4;
  for(i=0; i<20; i++) {
    d1 = (a1-1.)*pow(10.,(double)(i+1));
    d2 = (a2-1.)*pow(10.,(double)(i+1));
    d3 = (a3-1.)*pow(10.,(double)(i+1));
    d4 = (a4-1.)*pow(10.,(double)(i+1));
    a1 = 0.1f*(a1-1.f) + 1.f;
    a2 = 0.1*(a2-1.) + 1.;
    a3 = 0.1*(a3-1.) + 1.;
    a4 = 0.1*(a4-1.) + 1.;
    if(d1>0) l1 = i+1;
    if(d2>0) l2 = i+1;
    if(d3>0) l3 = i+1;
    if(d4>0) l4 = i+1;
  }
  Message("  Checking datatypes, number of digits in mantissa ...\n");
  Message("    float       %i \n", l1);
  Message("    double      %i \n", l2);
  Message("    long double %i \n", l3);
  Message("    DOUBLE      %i \n\n", l4);
}

/******************* Check Contigious Array *********************************/
void MessageTimeElapsed(int sec) {
  int day, min, hour;

  if(sec<60) {
    Message("%2dsec ", sec);
  }
  else if(sec<3600) {
    min = sec/60;
    sec = sec%60;
    Message("%2d:%2d ", min, sec);
  }
  else if(sec<86400) {
    min = sec/60;
    sec = sec%60;
    hour = min/60;
    min = min%60;
    Message("%2d:%2d:%2d ", hour, min, sec);
  }
  else {
    min = sec/60;
    sec = sec%60;
    hour = min/60;
    day = hour / 24;
    min = min%60;
    hour = hour%24;
    Message("%2dd %2dh %2dm %2ds ", hour, min, sec);
  }
}

/******************* Check Memory Copy **************************************/
int CheckMemcpy(void) {
  int i;
  int t, repeat_times=1000000;
  clock_t time_memcpy;
  DOUBLE time1, time2;
  int check_passed = ON;
  int index_copy, memory_size;

  if(Nwalkers == NwalkersMax) return -1; // comparison can not be done

  Message("  Checking memory copy speed ... \n");
  // Copy first walker to the last walker in the memory using different methods
  index_copy = NwalkersMax-1;

  CopyWalker(&W[index_copy], W[0]);

  time_memcpy = clock();
  for(t=0; t<repeat_times; t++) {
    for(i=0; i<Nup; i++) {
      W[index_copy].x[i] = W[0].x[i];
      W[index_copy].y[i] = W[0].y[i];
      W[index_copy].z[i] = W[0].z[i];
    }
  }
  time1 = (DOUBLE)(clock()-time_memcpy)/(DOUBLE)CLOCKS_PER_SEC;
  Message("    loop copy: %e sec\n", time1);
  if(WalkerCompare(W[index_copy], W[0])) check_passed = OFF;

  for(i=0; i<Nup; i++) {
    W[index_copy].x[i] = W[0].x[i]+1;
    W[index_copy].y[i] = W[0].y[i]+1;
    W[index_copy].z[i] = W[0].z[i]+1;
  }

  //memory_size = sizeof(DOUBLE)*N;
  memory_size = sizeof(*W[0].x)*Nup;
  time_memcpy = clock();
  for(t=0; t<repeat_times; t++) {
    //memcpy(W[index_copy].x, W[0].x, sizeof(DOUBLE)*3*N);
    memcpy(W[index_copy].x, W[0].x, memory_size);
    memcpy(W[index_copy].y, W[0].y, memory_size);
    memcpy(W[index_copy].z, W[0].z, memory_size);
  }
  time2 = (DOUBLE)(clock()-time_memcpy)/(DOUBLE)CLOCKS_PER_SEC;
  if(WalkerCompare(W[index_copy], W[0])) check_passed = OFF;
  if(check_passed == OFF) Error("  walkers compare: check not passed.");
  Message("    memcopy: %e sec\n", time2);
  Message("    speed gain: %g times\n", time1/time2);

  /*time_memcpy = clock();
  for(t=0; t<repeat_times; t++) {
    W[index_copy] = W[0];
  }
  time2 = (DOUBLE)(clock()-time_memcpy)/(DOUBLE)CLOCKS_PER_SEC;
  Message("    struct copy: %e sec\n", time2);
  Message("    speed gain: %g times\n", time1/time2);*/

  Message("  done\n");

  return check_passed;
}

/******************* Check Contigious Array *********************************/
int CheckContigiousArray(int **a, int dim1, int dim2) {
// checks order in memory of elements
  int i,j;

  for(i=0; i<dim1; i++) {
    for(j=0; j<dim2; j++) {
    //Message("%i %i a[i][j]=%i &a[i][j]=%i &a[i][j]-&a[i]=%i &a[i][j]-&a=%i\n", i, j, a[i][j], &a[i][j], &a[i][j]-a[i], &a[i][j]-&a[0][0]);
      if(&a[i][j] - &a[0][0] != i*dim2+j) {
        Warning("  memory allocation of this array is not contiguous!\n");
        return 1;
      }
    }
  }
  return 0;
}

/****************** Check Energy And Derivatives ****************************/
int CheckEnergyAndDerivatives(void) {
  int w;
  DOUBLE Ekin;
  DOUBLE epsilon = 1e-8;
  int Nmax = 10;

  if(Nmax > Nwalkers) Nmax = Nwalkers;

  Message("\n  Checking calculation of the kinetic energy by discrete differences (per Nmean = %" LG ")\n", Nmean);

#pragma omp parallel for
for(w=0; w<Nmax; w++) {
    //W[w].Eold = W[w].E = WalkerEnergy0(&W[w]);
    //W[w].U = U(W[w]);
    Ekin = KineticEnergyNumerically(&W[w]);
    Message("    %i Analytic %.3"LE"  discrete %.3"LE" Err: rel %.2"LF" %%  abs %.2"LE"\n", 
      w, W[w].Ekin/Nmean, Ekin/Nmean, fabs((W[w].Ekin-Ekin)/(Ekin+epsilon))*100., fabs(W[w].Ekin-Ekin)/Nmean);
  }
  Message("  done\n");

  return 0;
}

/****************** Check Coordinates Dimensionality ************************************/
int CheckCoordinatesDimensionality(void) {
  int w, i;
  int check_passed = ON;

#pragma omp parallel for
  for(w=0; w<Nmax; w++) {
    for(i=0; i<Nup; i++) {
      CaseNotX(if(fabs(W[w].x[i])>1e-3) check_passed = OFF;)
      CaseNotY(if(fabs(W[w].y[i])>1e-3) check_passed = OFF;)
      CaseNotZ(if(fabs(W[w].z[i])>1e-3) check_passed = OFF;)
      if(check_passed == OFF) {
        Message("  walker %i particle %i (%lf, %lf, %lf)\n", w+1, i+1, W[w].x[i], W[w].y[i], W[w].z[i]);
      }
    }
    for(i=0; i<Ndn; i++) {
       CaseNotX(if(fabs(W[w].xdn[i])>1e-3) check_passed = OFF;)
       CaseNotY(if(fabs(W[w].ydn[i])>1e-3) check_passed = OFF;)
       CaseNotZ(if(fabs(W[w].zdn[i])>1e-3) check_passed = OFF;)
       if(check_passed == OFF) {
          Message("  walker %i particle %i (%lf, %lf, %lf) down\n", w+1, i+1, W[w].xdn[i], W[w].ydn[i], W[w].zdn[i]);
       }
    }
  }

  return 1-check_passed;
}

/****************** Check Coordinates In The Box ****************************************/
int CheckCoordinatesInTheBox(void) {
  int w, i;
  int check_passed = ON;

  for(w=0; w<Nwalkers; w++) {
    for(i=0; i<Nup; i++) {
      CaseX(if(W[w].x[i]>L || W[w].x[i]<0.) check_passed = OFF;)
      CaseY(if(W[w].y[i]>L || W[w].y[i]<0.) check_passed = OFF;)
      CaseZ(if(W[w].z[i]>L || W[w].z[i]<0.) check_passed = OFF;)
      if(check_passed == OFF) {
        Message("  walker %i particle %i (%lf, %lf, %lf)\n", w+1, i+1, W[w].x[i], W[w].y[i], W[w].z[i]);
        return 1;
      }
    }
    for(i=0; i<Ndn; i++) {
      CaseX(if(W[w].xdn[i]>L || W[w].xdn[i]<0.) check_passed = OFF;)
      CaseY(if(W[w].ydn[i]>L || W[w].ydn[i]<0.) check_passed = OFF;)
      CaseZ(if(W[w].zdn[i]>L || W[w].zdn[i]<0.) check_passed = OFF;)
       if(check_passed == OFF) {
          Message("  walker %i particle %i (%lf, %lf, %lf) down\n", w+1, i+1, W[w].xdn[i], W[w].ydn[i], W[w].zdn[i]);
          return 1;
       }
    }
  }

  return 0;
}

