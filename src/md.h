/*md.h*/
#ifndef _MD_H
#define _MD_H

#include <math.h>
#include "main.h"
#include "utils.h"
#include "move.h"
#include "vmc.h"

void MolecularDynamicsMove(int w);

DOUBLE Force11(DOUBLE r);
DOUBLE Force12(DOUBLE r);
DOUBLE Force22(DOUBLE r);

DOUBLE WalkerKineticPotentialEnergy(struct Walker *W, DOUBLE *Ekin, DOUBLE *Eint);

void AdjustVelocitiesToTemperature(void);
void GenerateVelocities(void);

void AdjustCenterOfMass(void);
void AdjustCenterOfMassWalker(struct Walker *Walker);
void AdjustUpParticlesPinnedToAChain(void);
void AdjustUpParticlesPinnedToAChainWalker(struct Walker *Walker);

#endif
