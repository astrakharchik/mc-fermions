/*rw.c*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <wchar.h>
#include "parallel.h"
#include "rw.h"
#include "main.h"
#include "memory.h"
#include "utils.h"
#include "move.h"
#include "quantities.h"
#include "compatab.h"
#include "trial.h"
#include "fermions.h"
#include "trialf.h"
#include "spline.h"
#include "md.h"
#include "pigs.h"

#define maximal_string_length 1000

/*//void CHECK(searching_for, type, address) {wcscpy(string_wide,L"MC=\n"); Message("\nwide '%s' searching for '%s'", ch, searching_for); if(wcscmp(ch,"MC=\n")==0) {Message("match!!!\n");} else {Message("no match\n");} }
void CHECKi(wchar_t* ch, char* searching_for, char* type, int *address) {
  wchar_t string_wide[maximal_string_length];

  wcscpy(string_wide,L"MC="); 
  Message("\nwide '%s' searching for '%s'", ch, searching_for); 
  //if(wcscmp(ch,L"MC=")==0) 
  if(wmemcmp(ch, string_wide, 3) == 0)
    Message("match!!!\n"); 
  else 
    Message("no match\n"); 
}*/


/******************************** Load Config ******************************/
int LoadParameters(void) {
  FILE *in;
  DOUBLE alpha_x = 0.;
  DOUBLE alpha_y = 0.;
  DOUBLE alpha_z = 0.;
  DOUBLE precision = 1e-8;
#ifndef USE_UTF8
  char ch[100];
#else // UTF
  wchar_t ch[maximal_string_length];
  wchar_t string_wide[maximal_string_length];
  char string_not_wide[maximal_string_length];
#endif
  int Ntmp = -1;
  Nup = Ndn = -1;

  alpha_x_dn = -1; // check if its value will be initialized
  Ndens = -1;
  RoSW = RoSS = -1;
  SKT_dk = -1;

  Message("Loading parameters from MC.cfg ... \n");

  Fopen(in, INPATH "MC.cfg", "r", "MC.cfg");

  RD.width = 1000;
  RD.max = 5;
  RDxup.max = RDyup.max = RDzup.max = RDxdn.max = RDydn.max = RDzdn.max = RDup.max = RDdn.max = RD.max;

#ifdef TRIAL_1D
  OBDM.Nksize = 100;
#endif

  /*while(fwscanf(in, L"%s\n", ch) != EOF) {
    CHECKi(ch, "MC=","%i", &MC);
    CHECKs("file_particles=", "%s", file_particles);
  }

  exit(1);*/

#ifndef USE_UTF8
  while(fscanf(in, "%s\n", ch) != EOF) {
#else // UTF
  while(fwscanf(in, L"%s\n", ch) != EOF) {
#endif
    CHECK("MC=","%i", &MC);
    CHECK("SmartMC=","%i", &SmartMC);
    CHECK("Nimp=","%i", &Nimp);
    CHECK("n=", "%"LF, &n);
    CHECK("a=", "%"LF, &a);
    CHECK("b=", "%"LF, &b);
    CHECK("Niter=", "%li", &Niter);
    CHECK("blck_heating=", "%i", &blck_heating);
    CHECK("blck=", "%i", &blck);
    CHECK("dt=", "%"LF, &dt);
    CHECK("dt_all=", "%"LF, &dt_all);
    CHECK("dt_one=", "%"LF, &dt_one);
    CHECK("Rpar=", "%"LF, &Rpar);
    CHECK("Rpar2=", "%"LF, &Rpar2);
    CHECK("Rpar11=", "%"LF, &Rpar11);
    CHECK("Rpar12=", "%"LF, &Rpar12);
    CHECK("Rpar22=", "%"LF, &Rpar22);
    CHECK("Apar=", "%"LF, &Apar);
    CHECK("Bpar=", "%"LF, &Bpar);
    CHECK("Apar12=", "%"LF, &Apar12);
    CHECK("Bpar11=", "%"LF, &Bpar11);
    CHECK("Bpar12=", "%"LF, &Bpar12);
    CHECK("Bpar22=", "%"LF, &Bpar22);
    CHECK("Cpar=", "%"LF, &Cpar);
    CHECK("Dpar=", "%"LF, &Dpar);
    CHECK("Mpar=", "%"LF, &Mpar);
    CHECK("Opar=", "%"LF, &Opar);
    CHECK("Kpar=", "%"LF, &Kpar);
    CHECK("Kpar11=", "%"LF, &Kpar11);
    CHECK("Kpar12=", "%"LF, &Kpar12);
    CHECKs("file_particles=", "%s", file_particles);
    CHECKs("file_wf=", "%s", file_wf);
    CHECKs("file_energy=", "%s", file_energy);
    CHECKs("file_OBDM=", "%s", file_OBDM);
    CHECKs("file_OBDM_MATRIX=", "%s", file_OBDM_MATRIX);
    CHECKs("file_PD=", "%s", file_PD);
    CHECKs("file_PD_pure=", "%s", file_PD_pure);
    CHECKs("file_RD=", "%s", file_RD);
    CHECKs("file_PDz=", "%s", file_PDz);
    CHECKs("file_RDz=", "%s", file_RDz);
    CHECKs("file_R2=", "%s", file_R2);
    CHECKs("file_z2=", "%s", file_z2);
    CHECKs("file_R2_pure=", "%s", file_R2_pure);
    CHECKs("file_z2_pure=", "%s", file_z2_pure);
    CHECKs("file_Sk=", "%s", file_Sk);
    CHECKs("file_Sk_pure=", "%s", file_Sk_pure);
    CHECKs("file_SD=", "%s", file_SD);
    CHECK("verbosity=", "%i", &verbosity);
    CHECK("Nmeasure=", "%li", &Nmeasure);
    CHECK("grid_trial=", "%i", &grid_trial);
    CHECK("gridOBDM=", "%i", &gridOBDM);
    CHECK("gridTBDM=", "%i", &gridTBDM);
    CHECK("gridOBDM_MATRIX=", "%i", &gridOBDM_MATRIX);
    CHECK("gridTBDM_MATRIX=", "%i", &gridTBDM_MATRIX);
    CHECK("gridRD=", "%i", &gridRD);
    CHECK("gridPD=", "%i", &gridPD);
    CHECK("gridg3=", "%i", &gridg3);
    CHECK("Nall=", "%i", &Nall);
    CHECK("None=", "%i", &None);
    CHECK("McMillan_points=", "%i", &McMillan_points);
    CHECK("McMillanTBDM_points=", "%i", &McMillanTBDM_points);
    CHECK("NwalkersMax=", "%i", &NwalkersMax);
    CHECK("Npop=", "%i", &Npop);
    CHECK("generate_new_coordinates=", "%i", &generate_new_coordinates);
    CHECK("measure_energy=", "%i", &measure_energy);
    CHECK("measure_OBDM=", "%i", &measure_OBDM);
    CHECK("measure_Nk=", "%i", &measure_Nk);
    CHECK("measure_TBDM=", "%i", &measure_TBDM);
    CHECK("measure_OBDM_MATRIX=", "%i", &measure_OBDM_MATRIX);
    CHECK("measure_TBDM_MATRIX=", "%i", &measure_TBDM_MATRIX);
    CHECK("measure_RadDistr=", "%i", &measure_RadDistr);
    CHECK("measure_PairDistr=", "%i", &measure_PairDistr);
    CHECK("measure_g3=", "%i", &measure_g3);
    CHECK("acceptance_rate=", "%"LF, &acceptance_rate);
    CHECK("video=", "%i", &video);
    CHECK("lambda=", "%"LF, &lambda);
    CHECK("alpha_x=", "%"LF, &alpha_x);
    CHECK("alpha_y=", "%"LF, &alpha_y);
    CHECK("alpha_z=", "%"LF, &alpha_z);
    CHECK("alpha_x_up=", "%"LF, &alpha_x_up);
    CHECK("alpha_y_up=", "%"LF, &alpha_y_up);
    CHECK("alpha_z_up=", "%"LF, &alpha_z_up);
    CHECK("alpha_x_dn=", "%"LF, &alpha_x_dn);
    CHECK("alpha_y_dn=", "%"LF, &alpha_y_dn);
    CHECK("alpha_z_dn=", "%"LF, &alpha_z_dn);
    CHECK("alpha_R=", "%"LF, &alpha_Rx);
    CHECK("omega_x_up=", "%"LF, &omega_x_up);
    CHECK("omega_y_up=", "%"LF, &omega_y_up);
    CHECK("omega_z_up=", "%"LF, &omega_z_up);
    CHECK("omega_x_dn=", "%"LF, &omega_x_dn);
    CHECK("omega_y_dn=", "%"LF, &omega_y_dn);
    CHECK("omega_z_dn=", "%"LF, &omega_z_dn);
    CHECK("beta=", "%"LF, &beta);
    CHECK("measure_R2=", "%i", &measure_R2);
    CHECK("RDwidth=", "%"LF, &RD.width);
    CHECK("RDmax=", "%"LF, &RD.max);
    CHECK("RDupmax=", "%"LF, &RDup.max);
    CHECK("RDdnmax=", "%"LF, &RDdn.max);
    CHECK("RDxmax=", "%"LF, &RDxup.max);
    CHECK("RDymax=", "%"LF, &RDyup.max);
    CHECK("RDzmax=", "%"LF, &RDzup.max);
    CHECK("PDmax=", "%"LF, &PD.max);
    CHECK("Skmax=", "%"LF, &Sk.L);
    CHECK("OBDMmax=", "%"LF, &OBDM.max);
    CHECK("TBDMmax=", "%"LF, &TBDM12.max);
    CHECK("OBDM_MATRIXmax=", "%"LF, &OBDM_MATRIX.max);
    CHECK("TBDM_MATRIXmax=", "%"LF, &TBDM_MATRIX.max);
    CHECK("gridPD_pure_block=", "%i", &grid_pure_block);
    CHECK("grid_pure_block=", "%i", &grid_pure_block);
    CHECK("measure_Sk=", "%i", &measure_Sk);
    CHECK("measure_Sk_pure=", "%i", &measure_Sk_pure);
    CHECK("gridSk=", "%i", &gridSk);
    CHECK("gridSD=", "%i", &SD.size);
    CHECK("SDspacing=", "%i", &SD.spacing);
    CHECK("measure_SD=", "%i", &measure_SD);
    CHECK("measure_OP=", "%i", &measure_OP);
    CHECK("reweighting=", "%i", &reweighting);
    CHECK("S=", "%"LF, &Srecoil);
    CHECK("measure_Lind=", "%i", &measure_Lind);
    CHECK("gamma=", "%"LF, &Gamma);
    CHECK("var_par_array=", "%i", &var_par_array);
    CHECK("mass_ratio=", "%"LF, &mass_ratio);
    CHECK("N=","%i", &Ntmp);
    CHECK("Nup=","%i", &Nup);
    CHECK("Ndn=","%i", &Ndn);
    CHECK("NCrystal=","%i", &NCrystal);
    CHECK("Ndens=", "%"LF, &Ndens);
    CHECK("alpha_polarized=","%i", &alpha_polarized);
    CHECK("gamma2=", "%"LF, &Gamma2);
    CHECK("beta2=", "%"LF, &beta2);
    CHECK("Niter_store=","%i", &Niter_store);
    CHECK("optimization=","%i", &optimization);
    CHECK("orbital_weight1=", "%"LF, &orbital_weight1);
    CHECK("orbital_weight2=", "%"LF, &orbital_weight2);
    CHECK("orbital_weight3=", "%"LF, &orbital_weight3);
    CHECK("orbital_weight4=", "%"LF, &orbital_weight4);
    CHECK("orbital_weight5=", "%"LF, &orbital_weight5);
    CHECK("bilayer_width=", "%"LF, &bilayer_width);
    CHECK("lambda1=", "%"LF, &lambda1);
    CHECK("lambda2=", "%"LF, &lambda2);
    CHECK("lambda12=", "%"LF, &lambda12);
#ifdef TRIAL_1D
    CHECK("gridNk=", "%i", &OBDM.Nksize);
#endif
    CHECK("file_append=", "%i", &file_append);
    CHECK("file_particles_append=", "%i", &file_particles_append);
    CHECK("measure_spectral_weight=", "%i", &measure_spectral_weight);
    CHECK("gridPhi_x=", "%i", &gridPhi_x);
    CHECK("gridPhi_t=", "%i", &gridPhi_t);
    CHECK("gridSKT_k=", "%i", &gridSKT_k);
    CHECK("gridSKT_t=", "%i", &gridSKT_t);
    CHECK("SKT_dk=", "%"LF, &SKT_dk);
    CHECK("measure_FormFactor=", "%i", &measure_FormFactor);
    CHECK("Ro=", "%"LF, &Ro);
    CHECK("Ro12=", "%"LF, &Ro12);
    CHECK("Ro11=", "%"LF, &Ro11);
    CHECK("Ro22=", "%"LF, &Ro22);
    CHECK("RoSS=", "%"LF, &RoSS);
    CHECK("RoSW=", "%"LF, &RoSW);
    CHECK("excitonRadius=", "%"LF, &excitonRadius);
    CHECK("ap=", "%"LF, &ap);
    CHECK("one_body_momentum_i=", "%i", &one_body_momentum_i);
    CHECK("aA=", "%"LF, &aA);
    CHECK("aB=", "%"LF, &aB);
    CHECK("aAB=", "%"LF, &a);
    CHECK("measure_PairDistrMATRIX=", "%i", &measure_PairDistrMATRIX);
    CHECK("PD_MATRIXmax=", "%"LF, &PD_MATRIXmax);
    CHECK("gridPD_MATRIX=", "%i", &gridPD_MATRIX);
    CHECK("rGauss_up=", "%"LF, &rGauss_up);
    CHECK("rGauss_dn=", "%"LF, &rGauss_dn);
    CHECK("T=", "%"LF, &T);
    CHECK("dt1=", "%"LF, &dt1);
    CHECK("dt2=", "%"LF, &dt2);
    //CHECK("sum_over_images_cut_off_num=", "%%"LF, &sum_over_images_cut_off_num);
    CHECK("sum_over_images_cut_off_num=", "%i", &sum_over_images_cut_off_num);
    CHECK("alpha_latt=", "%"LF, &alpha_latt);
    CHECK("beta_latt=", "%"LF, &beta_latt);
    CHECK("measure_pure_coordinates=", "%i", &measure_pure_coordinates);
    CHECK("measure_effective_up_dn_potential=", "%i", &measure_effective_up_dn_potential);
    CHECK("Epar=", "%"LF, &Epar);
    CHECK("c=", "%"LF, &c);
    CHECK("c2=", "%"LF, &c2);
    CHECK("c3=", "%"LF, &c3);
    CHECK("c4=", "%"LF, &c4);
    CHECK("c5=", "%"LF, &c5);
    CHECK("c6=", "%"LF, &c6);
    CHECK("c7=", "%"LF, &c7);
    CHECK("c8=", "%"LF, &c8);
    CHECK("c9=", "%"LF, &c9);
    CHECK("c10=", "%"LF, &c10);
    CHECK("c11=", "%"LF, &c11);
    CHECK("c12=", "%"LF, &c12);
    CHECK("c13=", "%"LF, &c13);
    CHECK("a1=", "%"LF, &a1);
    //CHECK("a2=", "%"LF, &a2); a2 = a*a
    CHECK("a3=", "%"LF, &a3);
    CHECK("a4=", "%"LF, &a4);
    CHECK("a5=", "%"LF, &a5);
    CHECK("a6=", "%"LF, &a6);
    CHECK("a7=", "%"LF, &a7);
    CHECK("a8=", "%"LF, &a8);
    CHECK("b1=", "%"LF, &b1);
    CHECK("b3=", "%"LF, &b3);
    CHECK("b4=", "%"LF, &b4);
    CHECK("b5=", "%"LF, &b5);
    CHECK("b6=", "%"LF, &b6);
    CHECK("b7=", "%"LF, &b7);
    CHECK("b8=", "%"LF, &b8);
    CHECK("L=", "%"LF, &L);
    CHECK("check_kinetic_energy=", "%i", &check_kinetic_energy);
    CHECK("Nlattice=", "%i", &Nlattice);
    CHECK("Epotcutoff=", "%"LF, &Epotcutoff);
    CHECK("generate_crystal_coordinates=", "%i", &generate_crystal_coordinates);
    CHECK("measure_XY2=", "%i", &measure_XY2);
  }

  if(optimization == 1) {
    Message("Variational parameters will be optimized automatically\n");
    if(MC != VARIATIONAL) {
      Warning("  Switching to variational configuration in order to do optimization\n");
    }
  }

  if(NwalkersMax<Npop) {
    Message("  Adjusting NwalkersMax to %i", Npop);
    NwalkersMax = Npop;
  }

  if(mass_ratio>1e9) {
    Message("  particle down will be pinned!\n");
    m_up_inv = 2;
    m_up_inv_sqrt = sqrt(2.);
    m_dn_inv = 0;
    m_dn_inv_sqrt = 0;
  }
  else {
    // 0.5*(1.+M/m)*(2*mu) = 0.5*(1.+M/m)*2*M*m/(M+m) = M, i.e. spin down is "heavy"
    m_dn = 0.5*(1.+mass_ratio);
    m_up = 0.5*(1.+1./mass_ratio);
#ifdef UNIT_MASS_COMPONENT_UP
    Message("\n  Setting mass of the first component to 1\n");
    m_dn /= m_up;
    m_up = 1.;
#endif
#ifdef UNIT_MASS_COMPONENT_DN
    Message("\n  Setting mass of the second component to 1\n");
    m_up = mass_ratio;
    m_dn = 1.;
#endif
    m_up_inv = 1./m_up;
    m_dn_inv = 1./m_dn;
    m_up_inv_sqrt = sqrt(m_up_inv);
    m_dn_inv_sqrt = sqrt(m_dn_inv);
    if(mass_ratio != 1.) {
      Message("  Mass ratio is different from 1 and equals %"LF"\n", mass_ratio);
      Message("  Species has following masses: spin up %"LF", spin down %"LF"\n", m_up, m_dn);
    }
  }

#ifdef INFINITE_MASS_COMPONENT_DN
  Message("  Infinite mass particles in the second component.\n");
  m_dn_inv = m_dn_inv_sqrt = 0.; 
#endif
#ifdef INFINITE_MASS_COMPONENT_UP
  Message("  Infinite mass particles in the first component.\n");
  m_up_inv = m_up_inv_sqrt = 0.; 
#endif

  // reduced mass
  if(fabs(m_dn_inv) < precision) {
    Message("  mB = inf");
    m_mu = m_up;
  }
  else if(fabs(m_up_inv) < precision) {
    Message("  mA = inf");
    m_mu = m_dn;
  }
  else if(fabs(m_up/m_dn-1) < precision) {
    Message("  mA = mB");
    m_mu = m_up*m_dn / (m_up + m_dn);
  }
  else {
    m_mu = m_up*m_dn / (m_up + m_dn);
  }
  Message(", mu = %"LE"\n", m_mu);

#ifdef TRIAL_1D
  if(SKT_dk<0) {
    SKT_dk = 2./(DOUBLE)(Nup+Ndn);
    Message("  Adjusting SKT_dt to %lf\n", SKT_dk);
  }
#endif

  if(alpha_x_dn<-0.1) { // i.e. alpha_x_dn were no initialized
    Message("  Adjusting alpha_x=%"LF", alpha_y=%"LF", alpha_z=%"LF" for different components according to their masses (m_up = %"LF", m_dn = %"LF")\n", alpha_x, alpha_y, alpha_z, m_up, m_dn);
    alpha_x_up = alpha_x*m_up;
    alpha_y_up = alpha_y*m_up;
    alpha_z_up = alpha_z*m_up;
    alpha_x_dn = alpha_x*m_dn;
    alpha_y_dn = alpha_y*m_dn;
    alpha_z_dn = alpha_z*m_dn;
  }
  /*else {
    alpha_x_up = alpha_x;
    alpha_y_up = alpha_y;
    alpha_z_up = alpha_z;
  }*/
  Message("  alpha_x_up = %"LF", alpha_y_up = %"LF", alpha_z_up = %"LF"\n", alpha_x_up, alpha_y_up, alpha_z_up);
  Message("  alpha_x_dn = %"LF", alpha_y_dn = %"LF", alpha_z_dn = %"LF"\n", alpha_x_dn, alpha_y_dn, alpha_z_dn);
  two_alpha_x_up = 2. * alpha_x_up;
  two_alpha_y_up = 2. * alpha_y_up;
  two_alpha_z_up = 2. * alpha_z_up;
  two_alpha_x_dn = 2. * alpha_x_dn;
  two_alpha_y_dn = 2. * alpha_y_dn;
  two_alpha_z_dn = 2. * alpha_z_dn;

  omega_x2_up = omega_x_up*omega_x_up;
  omega_y2_up = omega_y_up*omega_y_up;
  omega_z2_up = omega_z_up*omega_z_up;
  omega_x2_dn = omega_x_dn*omega_x_dn;
  omega_y2_dn = omega_y_dn*omega_y_dn;
  omega_z2_dn = omega_z_dn*omega_z_dn;

  RDxdn.max = RDxup.max;
  RDydn.max = RDyup.max;
  RDzdn.max = RDzup.max;
 
#ifdef TRAP_POTENTIAL
  Message("  Frequencies of the harmonic trap:\n");
  CaseX(Message("    omega_x_up = %"LF"\n", omega_x_up);)
  CaseY(Message("    omega_y_up = %"LF"\n", omega_y_up);)
  CaseZ(Message("    omega_z_up = %"LF"\n", omega_z_up);)
  CaseX(Message("    omega_x_dn = %"LF"\n", omega_x_dn);)
  CaseY(Message("    omega_y_dn = %"LF"\n", omega_y_dn);)
  CaseZ(Message("    omega_z_dn = %"LF"\n\n", omega_z_dn);)
#endif  

#ifdef TRAP_DOUBLE_WELL
  Message("  Double well potential:\n");
  Message("    alpha_latt = %"LF"\n", alpha_latt);
#endif  

  if(Ro>0) {
    if(RoSS<0) RoSS = Ro;
    if(RoSW<0) RoSW = Ro;
  }
          
  a2 = a*a;
  //lambda2 = lambda*lambda;
  //lambda4 = lambda2*lambda2;

  Message("\n\n  Type of the algorithm: ");
  if(MC == DIFFUSION) {
    if(SmartMC == DMC_QUADRATIC) Message("QUADRATIC\n");
    if(SmartMC == DMC_LINEAR_GAUSSIAN) Message("LINEAR, energy is calculated at the drift\n");
    if(SmartMC == DMC_LINEAR_MIDDLE) Message("LINEAR, energy is calculated in the MIDDLE of drift force move\n");
    if(SmartMC == DMC_LINEAR_METROPOLIS) Message("Metropolis + branching linear algorithm, energy is calculated at the drift\n");
    if(SmartMC == DMC_LINEAR_METROPOLIS_MIDDLE) Message("Metropolis + branching linear algorithm, energy is calculated in the MIDDLE of the drift\n");
  }
  else if(MC == VARIATIONAL) { // VMC
    if(SmartMC == VMC_MOVE_ALL) Message("moving ALL particles\n");
    if(SmartMC == VMC_MOVE_ONE) Message("moving particles one by one\n");
  }
  else if(MC == CLASSICAL) {
    Message("classical\n");
  }
  else if(MC == MOLECULAR_DYNAMICS) {
    Message("molecular dynamics\n");
  }

  if(Ntmp != -1 && Nup == -1 && Nup == -1) { // Nup and Ndn not defined but N is defined
    Message(" Number of particles: Nup = Ndn = %i (unpolarized)\n", Ntmp);
    Nup = Ndn = Ntmp;
  }
  else {
    Message(" Number of particles: Nup = %i, Ndn = %i (can be polarized)\n", Nup, Ndn);
  }
  Ntot = Nup + Ndn;
  Nmean = 0.5*Ntot;
  Message(" Nmean = %"LF", energy out put: total energy x %"LF"\n", Nmean, energy_unit/Nmean); 

  if(fabs(Ndens+1)<1e-3) Ndens = Nmean;
  Nmax = Nup; // set Nmax = max(Nup,Ndn)
  if(Nmax<Ndn) Nmax = Ndn; 

  if(Nup<Ndn && DIMENSION != 1 && (MC == VARIATIONAL || MC == DIFFUSION)) Error("Nup must be larger or equal to Ndn!\n");

  if(Ndens == -1) {
    Ndens = Nup;
    Warning(" Ndens is undefined, assuming it is equal to %"LF"\n", Ndens);
  }
  else {
    Message(" Ndens is equal to %"LF"\n", Ndens);
  }

  if(Npop>NwalkersMax) {
    Warning("  Npop = %i > NwalkersMax = %i, increasing NwalkersMax\n", Npop, NwalkersMax);
    NwalkersMax = 2*Npop;
  }

  //Print type of the trial pair wave function
  Message("\n  Jastrow wavefunction: \n");
#ifdef JASTROW_OPPOSITE_SPIN
  Message("  - opposite spin.\n");
#endif
#ifdef JASTROW_SAME_SPIN
  Message("  - same spin.\n");
#endif

  Message("\n  Trial pair wavefunctions: ");
#ifdef TRIAL_EXTERNAL_WF11
  Message("Jastrow 11 loaded from file\n");
#endif
#ifdef TRIAL_EXTERNAL_WF12
  Message("Jastrow 12 loaded from file\n");
#endif
#ifdef TRIAL_EXTERNAL_WF22
  Message("Jastrow 22 loaded from file\n");
#endif
#ifdef TRIAL_HS
  Message("(3D HS+HS solution) + (exponential decay) -> hard spheres\n");
#endif
#ifdef TRIAL_SS
  Message("(3D SS+SS solution) + (exponential decay) -> hard spheres\n");
#endif
#ifdef TRIAL_HS1D
  Message("(1D HS+HS solution) + (exponential decay) -> hard rodes\n");
#endif
#ifdef TRIAL_TONKS
  Message("Tonks wavefunction (1D) f = |sin(sqrt{par}*(x-a))|\n");
#endif
#ifdef TRIAL_LIEB
  Message("Lieb wavefunction (1D) f = cos(k(|z|-Rpar))\n");
#endif
#ifdef TRIAL_LIM
  Message("f = 1 + a/r\n");
#endif
#ifdef TRIAL_POWER
  Message("f = 1 + a/r^Rpar\n");
#endif
#ifdef TRIAL_LIEB_PHONON11
  Message("f11: Lieb + phonon wavefunction (1D) A cos(k(x-B)) (x<L*Rpar), sin^a(pi*x/L), i.e. Lieb + phonons matching \n");
#endif
#ifdef TRIAL_PHONON_LUTTINGER11
  Message("f11: phonon Luttinger wavefunction (1D) A Cos(k(x-B)) (x<L*Rpar11), sin^a(pi*x/L), i.e. Lieb + phonons matching, parameters: Kpar11, Rpar11\n");
#endif
#ifdef TRIAL_PHONON_LUTTINGER12
  Message("f12: phonon Luttinger wavefunction (1D) A Cos(k(x-B)) (x<L*Rpar11), sin^a(pi*x/L), i.e. Lieb + phonons matching, parameters: Kpar12, Rpar12\n");
#endif
#ifdef TRIAL_PHONON_LUTTINGER_AUTO11
  Message("f11: phonon Luttinger wavefunction (1D) A Cos(k(x-B)) (x<L*Rpar11), sin^a(pi*x/L), i.e. Lieb + phonons matching, parameters: Kpar11, automatic Rpar11\n");
#endif
#ifdef TRIAL_PHONON_LUTTINGER_AUTO12
  Message("f12: phonon Luttinger wavefunction (1D) A Cos(k(x-B)) (x<L*Rpar11), sin^a(pi*x/L), i.e. Lieb + phonons matching, parameters: Kpar12, automatic Rpar12\n");
#endif
#ifdef TRIAL_LIEB_PHONON12
  Message("f12: Lieb + phonon wavefunction (1D) A cos(k(x-B)) (x<L*Rpar), sin^a(pi*x/L), i.e. Lieb + phonons matching \n");
#endif
#ifdef TRIAL_MCGUIRE11
  Message("f11: (1D) f(r) = exp(-r/aA), bound state of a pseudopotential \n");
#endif
#ifdef TRIAL_MCGUIRE12
  Message("f12: (1D) f(r) = exp(-r/aAB), bound state of a pseudopotential \n");
#endif
#ifdef TRIAL_MCGUIRE_PBC22_EPAR
  Message("f22: (1D) f(r) = exp(-Epar*(r-L/2)) + exp(-Epar*(L/2-r))\n");
#endif
#ifdef TRIAL_HS_SIMPLE
  Message("(3D) HS + HS solution, no variational parameters \n");
#endif
#ifdef TRIAL_PSEUDOPOT_SIMPLE
  Message("(3D) pseudopotential, no variational parameters \n");
#endif
#ifdef TRIAL_PSEUDOPOT_FS
  Message("(3D) A/r |sin(kr + B)|, no variational parameters \n");
#endif
#ifdef TRIAL_PSEUDOPOT_BS
  Message("(3D) A Exp(-r/a)/r \n");
#endif
#ifdef TRIAL_SQ_WELL_ZERO_CONST
  Message("(3D) A sin(kappa r)/r, B(1+a/r), 1+C Exp(-Apar r) (Square well zero constant)\n");
#endif
#ifdef TRIAL_SQ_WELL_TRAP
  Message("(3D) A sin(kappa r)/r, B(1+a/r)\n");
#endif
#ifdef TRIAL_SQ_WELL_BS_TRAP
  Message("(3D) A sin(kappa r)/r, exp(-r/a)/r\n");
#endif
#ifdef TRIAL_TONKS_TRAP12
  Message("12 Tonks-Girardeau wavefunction in trap f(r) = |r - a|, enable HS12 to have f(r)=0, r<a\n");
#endif
#ifdef TRIAL_TONKS_TRAP1122
  Message("11:22 Tonks-Girardeau wavefunction in trap f(r) = |r - b|, enable HS11 to have f(r)=0, r<b\n");
#endif

#ifdef VEXT_COS2
#ifdef BC_3DPBC_CUBE
  Nlattice = (int)(pow(NCrystal, 1./3.)+1e-12);
  Nlattice3 = Nlattice*Nlattice*Nlattice;
  if(Nlattice3 != NCrystal) Error("Cannot construct cubic lattice with %i lattice sites!", NCrystal);
  Message("  Size of the cubic lattice is %ix%ix%i\n", Nlattice, Nlattice, Nlattice);
#endif
#ifdef BC_2DPBC_SQUARE
  Nlattice = (pow(NCrystal, 1./2.)+1e-12);
  Nlattice3 = Nlattice*Nlattice;
  if(Nlattice3 != NCrystal) Error("Cannot construct square lattice with %i lattice sites!", NCrystal);
  Message("  Size of the square lattice is %ix%i\n", Nlattice, Nlattice);
#endif
#ifdef BC_1DPBC_Z
  if(NCrystal<0)
    Nlattice = Nup;
  else
    Nlattice = NCrystal;
  Message("  Size of the lattice is %i\n", Nlattice);
#endif
#endif

#ifdef HARD_SPHERE11
  Warning("Hard-sphere check11 is enabled, size aA = %"LF"\n", aA);
#endif
#ifdef HARD_SPHERE12
  Warning("Hard-sphere check12 is enabled, size a = %"LF"\n", a);
#endif
#ifdef HARD_SPHERE22
  Warning("Hard-sphere check22 is enabled, size aB = %"LF"\n", aB);
#endif

  // Print number of dimensions
  Message("  Number of dimensions: ");
#ifdef TRIAL_1D
  Message("1D\n");
#endif
#ifdef TRIAL_2D
  Message("2D\n");
#endif
#ifdef TRIAL_3D
  Message("3D\n");
#endif

  Message("  Boundary conditions: ");
#ifdef  BC_ABSENT
  Message("absent (external confinement)\n");
#endif
#ifdef BC_1DPBC
  Message("1D Periodic boundary conditions\n");
#endif
#ifdef BC_BILAYER
  Message("2D Periodic boundary conditions, bilayer\n");
#endif
#ifdef BC_2DPBC
  Message("2D Periodic boundary conditions\n");
#endif
#ifdef BC_2DPBC_HEXAGON
  Message("box geometry: HEXAGON\n");
#endif
#ifdef BC_3DPBC
  Message("3D Periodic boundary conditions\n");
#endif
#ifdef BC_3DPBC_TRUNCATED_OCTAHEDRON
  Message("box geometry: TRUNCATED OCTAHEDRON\n");
#endif

  Message("  Interaction potential:\n    ");
  // up + down potential
#ifdef INTERACTION_ABSENT //IFG
  Message("12) none (ideal gas)\n  ");
#endif
#ifdef INTERACTION12_COULOMB_ATTRACTION
  Message("12) Coulomb -1/(r^2+d^2) modified by bilayer_width\n");
#endif
#ifdef INTERACTION12_COULOMB_REPULSION
  Message("12) Coulomb 1/r\n");
#endif
#ifdef INTERACTION12_SQUARE_WELL
  Message("12) Square well\n");
#endif
#ifdef INTERACTION12_SOFT_SPHERE
  Message("12) Soft sphere\n");
#endif
#ifdef INTERACTION_CH2_12
  Message("12) 1/ch^2\n");
#endif
#ifdef INTERACTION12_SUTHERLAND
  Message("12) Calgero-Sutherland with parameter lambda12 = %lf\n", lambda12);
#endif
#ifdef INTERACTION12_RYDBERG
  Message("12) Rydberg D/r^6 with parameter D\n");
#endif
#ifdef INTERACTION12_ION
  Message("12) Ion -1/r^4 + regularization, V(r) = -a6(r^2 - c6^2) / [(r^2 + c6^2)*(r^2 + b6^2)^2], a= %lf, b= %lf, c= %lf\n", a6, b6, c6);
#endif

  Message("\n    ");

  // up + up, down + down  potential
#ifdef INTERACTION11_ABSENT //IFG
  Message("11,22) none (ideal gas)\n");
#endif
#ifdef INTERACTION11_COULOMB // 1/r
  Message("11,22) Coulomb\n");
#endif
#ifdef INTERACTION11_SUTHERLAND
  Message("11) Calogero-Sutherland model with parameter lambda1 = %lf\n", lambda1);
#endif
#ifdef INTERACTION22_SUTHERLAND
  Message("22) Calogero-Sutherland model with parameter lambda2 = %lf\n", lambda2);
#endif
#ifdef INTERACTION11_SQUARE_WELL
  Message("11) Square well\n  ");
#endif
#ifdef INTERACTION11_SOFT_SPHERE
  Message("11) Soft sphere\n");
#endif
#ifdef INTERACTION_CH2_11
  Message("11) 1/ch^2\n");
#endif
#ifdef INTERACTION_CH2_11
  Message("11) 1/ch^2\n");
#endif

#ifdef INTERACTION22_SQUARE_WELL
  Message("22) Square well\n  ");
#endif
#ifdef INTERACTION22_SOFT_SPHERE
  Message("22) Soft sphere\n");
#endif

  Message("\n  done\n");

  acceptance_rate /= 100.;

  if(beta < 0) { // beta = 1 / 4 lambda <z2>
#ifdef TRIAL_TONKS
    beta = 1 / 4*Nmean;
    Warning("Automatically setting TONKS value for parameter    beta = %"LF"\n", beta);
#else
    beta = 5./4. * lambda*pow(3*(Nmean-1)*lambda*a, -2./3.);
    if(beta>0.5) beta = 0.5;
    Warning("Automatically setting MEAN-FIELD value for parameter\n  beta = %"LF"\n", beta);
#endif
  }

  if(Npop == 1) {
    branchng_present = OFF;
    Warning(" Branching is absent.\n");
  }

  if(boundary == NO_BOUNDARY_CONDITIONS) {
#ifdef TRIAL_HS
    Warning("Automatically converting Rpar=%"LF"[a] => Rpar=%"LF"[aosc]\n", Rpar, Rpar*a);
    Rpar *= a;
#endif
#ifdef TRIAL_SS
    if(Apar > a) Warning("WARNING: Apar is larger than a\n");
    Warning("Automatically converting Apar=%"LF"[a] => Apar=%"LF"[aosc]\n", Apar, Apar*a);
    Apar *= a;
    Warning("Automatically converting Rpar=%"LF"[a] => Rpar=%"LF"[aosc]\n", Rpar, Rpar*a);
    Rpar *= a;
#endif
  } 

#ifdef BC_ABSENT
  if(boundary != NO_BOUNDARY_CONDITIONS) Error("Can not simulate free system: define BC_ABSENT and recompile the program\n");
/*  Warning("Automatically converting dt=%"LE"[a] => dt=%"LE"[aosc]\n", dt, dt*a*a);
  dt *= a*a;*/
#endif

#ifdef BC_3DPBC
  if(boundary != THREE_BOUNDARY_CONDITIONS) Error("Can not simulate system with 3D PBC: define BC_1DPBC and recompile the program\n");
  if(a != 1.) {
    Warning("a differs from 1.\n");
    //Warning("Forcing a = 1\n"); a = 1.;
  }
#endif

#ifdef CRYSTAL
#ifdef CRYSTAL_NONSYMMETRIC
    Message("  Simulation of the nonsymmetric crystal phase\n");
#endif
#ifdef CRYSTAL_SYMM_SUM_LATTICE
    Message("  Simulation of the symmetric crystal phase, type 1: sum over lattice sites and product over particles\n");
#endif
#ifdef CRYSTAL_SYMM_SUM_PARTICLE
    Message("  Simulation of the symmetric crystal phase, type 2: sum over particles and product over lattice sites\n");
#endif
#endif

  Message("  Unit of energy (in output) is %"LF"\n", energy_unit);

#ifdef ORBITAL_SQ_WELL_BOUND_STATE
#ifdef L2_ORBITAL_CONST
  Warning("Orbital is truncated to a constant value at L/2\n");
#else
Warning("Orbital is NOT truncated to a constant value at L/2\n");
#endif
#endif

#ifdef IMAGES_FLOOR
  Message("  Reduce to the box uses FLOOR function\n");
#endif
#ifdef IMAGES_IF_IF
  Message("  Reduce to the box uses IF IF function\n");
#endif
#ifdef IMAGES_ABS_IF
  Message("  Reduce to the box uses ABS IF function\n");
#endif

  if(MC == 0 && var_par_array == ON) var_par_array = OFF;

  bilayer_width2 = bilayer_width*bilayer_width;

#ifndef __WATCOMC__
#ifndef LINUX_GRAPHICS
#ifndef __MVC__
  video = OFF;
#endif
#endif
#endif

#ifdef SD_MEASURE_GLOBAL_WINDING
  Message("  SD: global winding number (up+dn) will be found\n");
#endif
#ifdef SD_MEASURE_ONLY_SPIN_DOWN
  Message("  SD: only winding number of 1 particle from second component will be measured\n");
#endif
#ifdef SD_MEASURE_W1_MINUS_W2
  Message("  SD: difference (W1-W2)^2 between winding numbers of different components W1 and W2 will be measured\n");
#endif
#ifdef SD_MEASURE_W1_TIMES_W2
  Message("  SD: product (W1*W2) of winding numbers of different components W1 and W2 will be measured\n");
#endif
#ifdef SD_MEASURE_ONLY_Z_DIRECTION
  Message("  SD: only diffusion in Z direction will be measured\n");
#endif

  G12.Apar = Apar12;
  G11.Apar = G22.Apar = Apar;
  G12.Bpar = Bpar12;
  G11.Bpar = G22.Bpar = Bpar;

  fclose(in);

  return 0;
}

/************************* Load Particle Coordinates *************************/
int LoadCoordinates(void) {
  FILE *in;
  int w, i;
  DOUBLE Ew;

  if(verbosity) Warning("Loading walker coordinates ...\n  ");
  Fopen(in, "in3Dprev.in", "r", "coordinates");

  fscanf(in, "%i\n", &Nwalkers); // Define number of walkers
  if(Nwalkers != Npop) Warning("  number of walkers in MC.cfg (Npop=%i) is different from actual number (%i)\n", Npop, Nwalkers);

  if(MC == PIGS) NwalkersMax = Nwalkers = Npop;

  AllocateWalkers();

  E = 0.;
  for(w=0; w<Nwalkers; w++) {
    fscanf(in, "%"LF"\n", &Ew);
    W[w].E = Ew;
    E += Ew;
    for(i=0; i<Nup; i++) {
      fscanf(in, "%"LF" %"LF" %"LF"\n", &W[w].x[i], &W[w].y[i], &W[w].z[i]);

#ifdef TRIAL_1D
      if(W[w].x[i] != 0 || W[w].y[i] != 0) {
        Warning("1D simulation: forcing coordinates %i %i %"LF" %"LF" be equal to zero\n", w+1, i+1, W[w].x[i], W[w].y[i]);
        W[w].x[i] = W[w].y[i] = 0.;
      }
#endif

#ifdef TRIAL_2D
      if(W[w].z[i] != 0) {
        Warning("2D simulation: forcing coordinates %i %i %"LF" be equal to zero\n", w+1, i+1, W[w].z[i]);
        W[w].z[i] = W[w].z[i] = 0.;
      }
#endif
      // inside of the box check
      if((boundary == ONE_BOUNDARY_CONDITION && (W[w].z[i]>L || W[w].z[i]<0))
      || (boundary == THREE_BOUNDARY_CONDITIONS && (W[w].x[i]>L || W[w].x[i]<0 
          || W[w].y[i]>L || W[w].y[i]<0 || W[w].z[i]>L || W[w].z[i]<0))) {
        Warning("Walker %i, particle %i\n", w+1, i+1);
        Warning("coords %"LF" %"LF" %"LF", L = %"LF"\n", W[w].x[i], W[w].y[i], W[w].z[i], L);
        Error("bad particle coordinate\n");
      }
    }
    for(i=0; i<Ndn; i++) {
      fscanf(in, "%"LF" %"LF" %"LF"\n", &W[w].xdn[i], &W[w].ydn[i], &W[w].zdn[i]);

#ifdef TRIAL_1D
      if(W[w].xdn[i] != 0 || W[w].ydn[i] != 0) {
        Warning("1D simulation: forcing coordinates %i %i %"LF" %"LF" be equal to zero\n", w+1, i+1, W[w].xdn[i], W[w].ydn[i]);
        W[w].xdn[i] = W[w].ydn[i] = 0.;
      }
#endif

#ifdef TRIAL_2D
      if(W[w].zdn[i] != 0) {
        Warning("2D simulation: forcing coordinates %i %i %"LF" be equal to zero\n", w+1, i+1, W[w].zdn[i]);
        W[w].zdn[i] = W[w].zdn[i] = 0.;
      }
#endif
      // inside of the box check
      if((boundary == ONE_BOUNDARY_CONDITION && (W[w].zdn[i]>L || W[w].zdn[i]<0))
      || (boundary == THREE_BOUNDARY_CONDITIONS && (W[w].xdn[i]>L || W[w].xdn[i]<0 
          || W[w].ydn[i]>L || W[w].ydn[i]<0 || W[w].zdn[i]>L || W[w].zdn[i]<0))) {
        Warning("Walker %i, particle %i\n", w+1, i+1);
        Warning("coords %"LF" %"LF" %"LF", L = %"LF"\n", W[w].xdn[i], W[w].ydn[i], W[w].zdn[i], L);
        Error("bad particle coordinate\n");
      }
    }
    if(CheckWalkerOverlapping(W[w])) 
      Error("Bad initial configuration, particles overlap, check %s file, walker %i", file_particles, w);
    W[w].U = U(W[w]);
  }

  E /= (DOUBLE) Nwalkers;
  Nwalkersw = (DOUBLE) Nwalkers;
  fclose(in);

  if(verbosity) Warning("done\n");
  return 0;
}

/************************* Load Particle Velocities *************************/
int LoadVelocities(void) {
  FILE *in;
  int w, i;
  DOUBLE Ew;

  Message("  Loading walker velocities ... ");
  Fopen(in, "in3Dvel.in", "r", "velocities");
  fscanf(in, "%i\n", &Nwalkers); // Define number of walkers 

  for(w=0; w<Nwalkers; w++) {
    fscanf(in, "%"LF"\n", &Ew);
    for(i=0; i<Nup; i++) {
      fscanf(in, "%"LF" %"LF" %"LF" %"LF" %"LF" %"LF"\n", &W[w].x[i], &W[w].y[i], &W[w].z[i], &W[w].Vx[i], &W[w].Vy[i], &W[w].Vz[i]);
    }
    for(i=0; i<Ndn; i++) {
      fscanf(in, "%"LF" %"LF" %"LF" %"LF" %"LF" %"LF"\n", &W[w].xdn[i], &W[w].ydn[i], &W[w].zdn[i], &W[w].Vxdn[i], &W[w].Vydn[i], &W[w].Vzdn[i]);
    }
  }
  fclose(in);

  Message("done\n");
  return 0;
}

/****************************** Save Coordinates ************************/
int SaveCoordinates(void) {
  FILE *out;
  int w, i;

#ifdef CHECK_OVERLAPPING
  if(CheckOverlapping()) Warning("Save Coordinates : particles overlap");
#endif

  Fopen(out, "in3Dprev.in", file_particles_append?"a":"w", "coordinates");
  fprintf(out, "%i\n", Nwalkers);
  for(w=0; w<Nwalkers; w++) {
    W[w].w = w;
    fprintf(out, "%.15"LE "\n", WalkerEnergy0(&W[w]));
    for(i=0; i<Nup; i++) fprintf(out, "%.15"LE " %.15"LE " %.15"LE "\n", W[w].x[i], W[w].y[i], W[w].z[i]);
    for(i=0; i<Ndn; i++) fprintf(out, "%.15"LE " %.15"LE " %.15"LE "\n", W[w].xdn[i], W[w].ydn[i], W[w].zdn[i]);
  }
  fclose(out);

  return 0;
}

/****************************** Save Coordinates ************************/
int SaveVelocities(void) {
  FILE *out;
  int w, i;

  Fopen(out, "in3Dvel.in", "w", "velocities");
  fprintf(out, "%i\n", Nwalkers);
  for(w=0; w<Nwalkers; w++) {
    W[w].w = w;
    fprintf(out, "%.15"LE "\n", WalkerEnergy0(&W[w]));
    for(i=0; i<Nup; i++) fprintf(out, "%.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE "\n", W[w].x[i], W[w].y[i], W[w].z[i], W[w].Vx[i], W[w].Vy[i], W[w].Vz[i]);
    for(i=0; i<Ndn; i++) fprintf(out, "%.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE "\n", W[w].xdn[i], W[w].ydn[i], W[w].zdn[i], W[w].Vxdn[i], W[w].Vydn[i], W[w].Vzdn[i]);
  }
  fclose(out);

  return 0;
}

/************************* Load Parameters Sk ********************************/
int LoadParametersSk(void) {
  FILE *in;
  int i,j;
  int number;
  //int nx,ny,nz,n2;
  DOUBLE nx,ny,nz,n2;
  int index;
  DOUBLE k,kold;
  FILE *out;

  if(verbosity) Warning("Loading Sk parameters...\n  ");

  Fopen(in, "in3Dkas.in", "r", "in3Dkas");

  index = 0; // index pointing to the first degenerate momenta
  kold = -1;
  Sk.size = gridSk;
 
#ifdef BC_1DPBC_X // HO in y direction
  Warning("  momentum in x direction is in inverse interparticle units, in y direction in osc. units\n");
  Message("  N\tkx\tky\n");
#else
  Message("No k (index, degeneracy) nx ny nz\n");
#endif

#ifndef TRIAL_1D
  Fopen(out, "outskindex.dat", "w", "sk index");
#endif

  for(i=0; i<Sk.size; i++) {
    fscanf(in, "%i", &number);
    fscanf(in, "%"LF, &nx);
    fscanf(in, "%"LF, &ny);
    fscanf(in, "%"LF, &nz);
    fscanf(in, "%"LF"\n", &n2);
#ifdef BC_1DPBC_X // HO in y direction
    Sk.kx[i] = 2.*PI/(Lx/(DOUBLE) N)*nx;
    Sk.ky[i] = ny;
    Sk.index[i] = i;
    Sk.k[i] = Sqrt(Sk.kx[i]*Sk.kx[i]+Sk.ky[i]*Sk.ky[i]);
    Message("  %i\t%"LG" \t%"LG"\n", i+1, Sk.kx[i], Sk.ky[i]);
#else
    if(fabs(nx)>1e-8) Sk.kx[i] = PI/L_half_x*nx;
    if(fabs(ny)>1e-8) Sk.ky[i] = PI/L_half_y*ny;
    if(fabs(nz)>1e-8) Sk.kz[i] = PI/L_half_z*nz;
    k = Sqrt(Sk.kx[i]*Sk.kx[i]+Sk.ky[i]*Sk.ky[i]+Sk.kz[i]*Sk.kz[i]);
    Sk.k[i] = k;

    if(k>kold)   // comment this line to skip degenerated values of momenta
      index = i;

    Sk.index[i] = index;
    Sk.degeneracy[index]++;
    kold = k;

    Message("%i %"LF"  (%i : %i) %"LF" %"LF" %"LF"  \n", i+1, k, Sk.index[i]+1, Sk.degeneracy[index], nx, ny, nz);
#endif
#ifndef TRIAL_1D
  if(i) fprintf(out, "%.15"LE "\n", Sk.k[i]);
#endif
  }

  if(verbosity) Warning("done\n");
#ifndef TRIAL_1D
  fclose(out);
#endif
  fclose(in);

  return 0;
}

/******************************** Save Energy VMC ************************/
int SaveEnergyVMC(DOUBLE E, DOUBLE EFF) {
  FILE *out;
  static int first_time = ON;

#ifdef MPI
  ParallelSaveEnergyVMC();
  return 0;
#endif
  Fopen(out, file_energy, (file_append || !first_time)?"a":"w", "energy");

  if(first_time) {
    fprintf(out, "#E EFF Epot Eext Ekin\n"); // NB for delta-pseudopotential interaction Ekin = EFF-Epot, Edelta = E-EFF
    first_time = OFF;
  }
  fprintf(out, "%.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE "\n", (E+energy_shift)*energy_unit, (EFF+energy_shift)*energy_unit, Epot*energy_unit, Eext*energy_unit, Ekin*energy_unit);

  fclose(out);

  return 0;
}

/******************************** Save Energy DMC ************************/
int SaveEnergyDMC(DOUBLE E) {
  FILE *out;
  static long int i = 0; // variable i tells how many times the energy was saved

  Fopen(out, file_energy, (file_append || i++)?"a":"w", "energy");
  if(SmartMC != DMC_PSEUDOPOTENTIAL && SmartMC != DMC_PSEUDOPOTENTIAL_REWEIGHTING) {
    if(branchng_present)
      fprintf(out, "%.15"LE " %i %"LE" %"LE"\n", energy_unit*(E+energy_shift), Nwalkers, energy_unit*Epot/(DOUBLE)Nwalkers, energy_unit*Ekin/(DOUBLE)Nwalkers);
    else
      fprintf(out, "%.15"LE " %.15"LE " %.2"LF" %.15"LE "\n", energy_unit*(E+energy_shift), energy_unit*(EFF+energy_shift), Nwalkersw, energy_unit*(Eint+energy_shift));
  }
  else { // DMC_PSEUDOPOTENTIAL
    fprintf(out, "%.15"LE " %i\n", energy_unit*(E+energy_shift), Nwalkers);
  }

  fclose(out);

  return 0;
}

/******************************** Save Energy CLS ************************/
int SaveEnergyCLS(void) {
  FILE *out;
  int w;
  static long int i = 0; // variable i tells how many times the energy was saved

  Fopen(out, file_energy, (file_append || i++)?"a":"w", "energy");
  E = 0.;
  for(w=0; w<Nwalkers; w++) {
    W[w].Epot = WalkerPotentialEnergy(&W[w]);
    E += W[w].Epot;
  }
  E /= (Nup+Ndn)* (DOUBLE) Nwalkers;

  fprintf(out, "%.15"LE "\n", E);
  fclose(out);

  return 0;
}

/******************************** Save Energy MD *************************/
int SaveEnergyMD(void) {
  FILE *out;
  int w;
  static long int i = 0; // variable i tells how many times the energy was saved

  Fopen(out, file_energy, (file_append || i++)?"a":"w", "energy");

  E = Ekin = Epot = 0.;
  for(w=0; w<Nwalkers; w++) {
    WalkerKineticPotentialEnergy(&W[w], &W[w].Ekin, &W[w].Epot);
    Epot += W[w].Epot;
    Ekin += W[w].Ekin;
  }
  E = Epot + Ekin;
  E /= (Nup+Ndn)* (DOUBLE) Nwalkers;
  Ekin /= (Nup+Ndn)* (DOUBLE) Nwalkers;
  Epot /= (Nup+Ndn)* (DOUBLE) Nwalkers;

  fprintf(out, "%.15"LE " %.15"LE " %.15"LE "\n", E, Epot, Ekin);
  fclose(out);

  return 0;
}

/******************************** Save Energy MD *************************/
int SaveEnergyPIGS(void) {
// accumulates the energy and saves the average value
  int average_over_number_iterations = 1; // 1000
  FILE *out;
  int w;
  static int first_time = ON;
  //int i;
  //DOUBLE Epot0; // potential energy at border
  //DOUBLE Ekin0, EFF0, Epot0b;
  DOUBLE EkinM, EpotM; // calculated in the middle point

  static int iteration = 0;
  static DOUBLE Eav = 0, E2av = 0, Epotav = 0.;

  w = Nwalkers / 2; // center of the chain will be used to measure the energy
  EkinM = PIGSKineticEnergy(w) / Nmean; // give index at which bead the kinetic energy will be calculated
  EpotM = WalkerPotentialEnergy(&W[w]) / Nmean;
  //Epot0 = WalkerPotentialEnergy(&W[0]) / Nmean;

  //E = WalkerEnergy0(&W[0]) / Nmean; // also updates Ekin, EFF, Eext, Epot attributes of the walker
  E = 0.5*(WalkerEnergy0(&W[0]) + WalkerEnergy0(&W[Nwalkers-1])) / Nmean; // also updates Ekin, EFF, Eext, Epot attributes of the walker
  //Ekin0 = W[0].Ekin / Nmean;
  //EFF0 = W[0].EFF; // already dividied by Nmean
  //Epot0b = (W[0].Eext + W[0].Epot) / Nmean;
  //fprintf(out, "%.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE "\n", E, EkinM, EpotM, Epot0, Epot0b, Ekin0, EFF0);

  Eav += E;
  E2av += EkinM + EpotM;
  Epotav += EpotM;

  if(++iteration == average_over_number_iterations) {
    Eav /= (DOUBLE)average_over_number_iterations;
    E2av /= (DOUBLE)average_over_number_iterations;
    Epotav /= (DOUBLE)average_over_number_iterations;
    Fopen(out, file_energy, (file_append || !first_time)?"a":"w", "energy");
    //fprintf(out, "%.15"LE " %.15"LE " %.15"LE "\n", E, EkinM+EpotM, EpotM);
    //if(first_time) fprintf(out, "#1) Eloc at edge\n#2) Ekin + Epot at center\n#3) Epot at center\n");
    fprintf(out, "%.15"LE " %.15"LE " %.15"LE "\n", energy_unit*Eav, energy_unit*E2av, energy_unit*EpotM);
    fclose(out);
    iteration = 0;
    Eav = E2av = Epotav = 0.;
    if(first_time == ON) first_time = OFF;
  }

  return 0;
}
/*********************** Save Pair Distribution ******************************/
int SavePairDistribution(void) {
  // homogeneous bose gas
  int i;
  DOUBLE r, V;
  FILE *out12, *out11, *out22, *outveff22;
  DOUBLE normalization11, normalization12, normalization22;
  DOUBLE combinations11, combinations12, combinations22;
  static int initialized = OFF;

  if(measure_PairDistrMATRIX) SavePairDistribtuionMatrix();

  if(PD.times_measured == 0) {
    Warning("Attempt to save pair distribution function without any measurement\n");
    return 1;
  }

  Fopen(out12, "outpd12.dat", (initialized || file_append)?"a":"w", "pair distribution function");
  Fopen(out11, "outpd11.dat", (initialized || file_append)?"a":"w", "pair distribution function");
  Fopen(out22, "outpd22.dat", (initialized || file_append)?"a":"w", "pair distribution function");
  if(measure_effective_up_dn_potential) {
    Fopen(outveff22, "outveff22.dat", (initialized || file_append) ? "a" : "w", "effective interaction potential");
  }

#ifdef BC_ABSENT
  normalization11 = 1. / ((DOUBLE)(PD.times_measured)*Nmean*PD.step);
  normalization12 = 1. / ((DOUBLE)(PD.times_measured)*Nmean*PD.step);
  normalization22 = 1. / ((DOUBLE)(PD.times_measured)*Nmean*PD.step);
#else // PBC
  V = 2. * Ndens / n;
  combinations11 = (DOUBLE) (((Nup-1)*Nup) / 2);
  combinations12 = (DOUBLE) (Nup*Ndn);
  combinations22 = (Ndn<2)?1:(DOUBLE) (((Ndn-1)*Ndn) / 2);
#ifdef TRIAL_3D
  normalization11 = 1./((DOUBLE)(PD.times_measured)*combinations11/V*4.*PI*PD.step);
  normalization12 = 1./((DOUBLE)(PD.times_measured)*combinations12/V*4.*PI*PD.step);
  normalization22 = 1./((DOUBLE)(PD.times_measured)*combinations22/V*4.*PI*PD.step);
#endif
#ifdef TRIAL_2D
  normalization11 = 1./((DOUBLE)(PD.times_measured)*combinations11/V*2.*PI*PD.step);
  normalization12 = 1./((DOUBLE)(PD.times_measured)*combinations12/V*2.*PI*PD.step);
  normalization22 = 1./((DOUBLE)(PD.times_measured)*combinations22/V*2.*PI*PD.step);
#endif
#ifdef TRIAL_1D
  normalization11 = 1./((DOUBLE)(PD.times_measured)*combinations11/V*2.*PD.step);
  normalization12 = 1./((DOUBLE)(PD.times_measured)*combinations12/V*2.*PD.step);
  normalization22 = 1./((DOUBLE)(PD.times_measured)*combinations22/V*2.*PD.step);
#endif
#endif

  for(i=0; i<PD.size; i++) {
    r = PD.min + (DOUBLE) (i+0.5) * PD.step;

#ifdef TRIAL_3D
    fprintf(out12, "%"LF" %.15"LE "\n", r, normalization12*(DOUBLE)PD.N[i]/(r*r));
    fprintf(out11, "%"LF" %.15"LE "\n", r, normalization11*(DOUBLE)PDup.N[i]/(r*r));
    fprintf(out22, "%"LF" %.15"LE "\n", r, normalization22*(DOUBLE)PDdn.N[i]/(r*r));
#endif

#ifdef TRIAL_2D
    fprintf(out12, "%"LF" %.15"LE "\n", r, normalization12*(DOUBLE)PD.N[i]/r);
    fprintf(out11, "%"LF" %.15"LE "\n", r, normalization11*(DOUBLE)PDup.N[i]/r);
    fprintf(out22, "%"LF" %.15"LE "\n", r, normalization22*(DOUBLE)PDdn.N[i]/r);
#endif

#ifdef TRIAL_1D
    r *= n; // rescale by the total density
    //r *= L/Nmean; // rescale by single component density
    fprintf(out12, "%"LF" %.15"LE "\n", r, normalization12*(DOUBLE)PD.N[i]);
    fprintf(out11, "%"LF" %.15"LE "\n", r, normalization11*(DOUBLE)PDup.N[i]);
    fprintf(out22, "%"LF" %.15"LE "\n", r, normalization22*(DOUBLE)PDdn.N[i]);
#endif

    if(measure_effective_up_dn_potential) fprintf(outveff22, "%"LF" %.15"LE "\n", r, (PDdn.N[i]>0)?((DOUBLE)PDdn.f[i]/(DOUBLE)PDdn.N[i]):(0));

#ifdef HARD_SPHERE12
    if(r*r<a2 && PD.N[i]>0) Warning("  unreal pair distribution function, particle at distance %"LF" < %"LF"\n", r, a);
#endif

    PD.N[i] = 0;
    PDup.N[i] = 0;
    PDdn.N[i] = 0;
    if(measure_effective_up_dn_potential) PDdn.f[i] = 0.;
  }
  PD.times_measured = 0;
  fclose(out12);
  fclose(out11);
  fclose(out22);
  if(measure_effective_up_dn_potential) fclose(outveff22);

  PD.times_measured = 0;
  initialized = ON;

  return 0;
}

int SavePairDistributionPure(void) {
  // homogeneous bose gas
  int i;
  DOUBLE r;
  FILE *out12, *out11, *out22;
  DOUBLE normalization11, normalization12, normalization22;
  static int initialized = OFF;
  int check;
  DOUBLE V;
  DOUBLE combinations11, combinations12, combinations22;

  check = OFF;
  for(i=0; i<gridPD; i++) if(W[0].PD[grid_pure_block-1][i]>0) check = ON;
  if(PD_pure.times_measured == 0 || check == OFF) {
    Warning("Attempt to save pure pair distribution function without any measurement\n");
    return 1;
  }

  Fopen(out12, "outpdp12.dat", (initialized || file_append)?"a":"w", "pair distribution function");
  Fopen(out11, "outpdp11.dat", (initialized || file_append)?"a":"w", "pair distribution function");
  Fopen(out22, "outpdp22.dat", (initialized || file_append)?"a":"w", "pair distribution function");

#ifdef BC_ABSENT
  normalization11 = 1. / ((DOUBLE)(PD_pure.times_measured)*Nmean*PD.step);
  normalization12 = 1. / ((DOUBLE)(PD_pure.times_measured)*Nmean*PD.step);
  normalization22 = 1. / ((DOUBLE)(PD_pure.times_measured)*Nmean*PD.step);
#else
  V = 2. * Ndens / n;
  combinations11 = (DOUBLE) (((Nup-1)*Nup) / 2);
  combinations12 = (DOUBLE) (Nup*Ndn);
  combinations22 = (Ndn<2)?1:(DOUBLE) (((Ndn-1)*Ndn) / 2);
#ifdef TRIAL_3D
  normalization11 = 1./((DOUBLE)(PD_pure.times_measured)*combinations11/V*4.*PI*PD.step);
  normalization12 = 1./((DOUBLE)(PD_pure.times_measured)*combinations12/V*4.*PI*PD.step);
  normalization22 = 1./((DOUBLE)(PD_pure.times_measured)*combinations22/V*4.*PI*PD.step);
#endif
#ifdef TRIAL_2D
  normalization11 = 1./((DOUBLE)(PD_pure.times_measured)*combinations11/V*2.*PI*PD.step);
  normalization12 = 1./((DOUBLE)(PD_pure.times_measured)*combinations12/V*2.*PI*PD.step);
  normalization22 = 1./((DOUBLE)(PD_pure.times_measured)*combinations22/V*2.*PI*PD.step);
#endif
#ifdef TRIAL_1D
  normalization11 = 1./((DOUBLE)(PD_pure.times_measured)*combinations11/V*PD.step);
  normalization12 = 1./((DOUBLE)(PD_pure.times_measured)*combinations12/V*PD.step);
  normalization22 = 1./((DOUBLE)(PD_pure.times_measured)*combinations22/V*PD.step);
#endif
#endif

  for(i=0; i<PD_pure.size; i++) {
    r = PD.min + (DOUBLE) (i+0.5) * PD.step;
#ifdef TRIAL_3D
    fprintf(out12, "%"LF" %.15"LE "\n", r, normalization12*(DOUBLE)PD_pure.N[i]/(r*r));
    fprintf(out11, "%"LF" %.15"LE "\n", r, normalization11*(DOUBLE)PDup_pure.N[i]/(r*r));
    fprintf(out22, "%"LF" %.15"LE "\n", r, normalization22*(DOUBLE)PDdn_pure.N[i]/(r*r));
#endif
#ifdef TRIAL_2D
    fprintf(out12, "%"LF" %.15"LE "\n", r, normalization12*(DOUBLE)PD_pure.N[i]/r);
    fprintf(out11, "%"LF" %.15"LE "\n", r, normalization11*(DOUBLE)PDup_pure.N[i]/r);
    fprintf(out22, "%"LF" %.15"LE "\n", r, normalization22*(DOUBLE)PDdn_pure.N[i]/r);
#endif
#ifdef TRIAL_1D
    r *= n; // rescale by the total density
    //r *= L/Nmean; // rescale by single component density
    fprintf(out12, "%"LF" %.15"LE "\n", r, normalization12*(DOUBLE)PD_pure.N[i]);
    fprintf(out11, "%"LF" %.15"LE "\n", r, normalization11*(DOUBLE)PDup_pure.N[i]);
    fprintf(out22, "%"LF" %.15"LE "\n", r, normalization22*(DOUBLE)PDdn_pure.N[i]);
#endif
#ifdef HARD_SPHERE12
    if(r*r<a2 && PD_pure.N[i]>0)  Warning("unreal pure pair distribution function\n");
#endif
    PD_pure.N[i] = 0;
    PDup_pure.N[i] = 0;
    PDdn_pure.N[i] = 0;
  }
  PD_pure.times_measured = 0;
  fclose(out12);
  fclose(out11);
  fclose(out22);
  initialized = ON;

  return 0;
}

DOUBLE IntegrateMomentum(DOUBLE p) {
  return p*Sin(p*PD.width);
}

/*********************** Save Radial Distribution ****************************/
int SaveRadialDistribution(void) {
  int i;
  DOUBLE r, normalization, normalization_pure_up, normalization_pure_dn;
  FILE *out,*out2;
  static int initialized = OFF;
  int save_pure = ON;

  if(RD.times_measured == 0) {
    Warning("Attempt to save radial distribution function without any measurement\n");
    return 1;
  }

#ifdef BC_ABSENT
#ifdef TRIAL_3D
    normalization = 1./(DOUBLE)(4.*PI*RD.step*RD.times_measured*Nup);
#endif
#ifdef TRIAL_2D
    normalization = 1./(DOUBLE)(2.*PI*RD.step*RD.times_measured*Nup);
#endif
#ifdef TRIAL_1D
    normalization = 1./((DOUBLE) (2*RD.times_measured*Nup)*RDup.step);
#endif
  Fopen(out, "outrdup.dat", (initialized || file_append)?"a":"w", "Radial Distribution");
  for(i=0; i<RD.size; i++) {
    r = (DOUBLE) (i+0.5) * RDup.step;
#ifdef TRIAL_3D
    fprintf(out, "%" LF " %.15" LE "\n", r, normalization*(DOUBLE)RDup.N[i]/(r*r));
#endif
#ifdef TRIAL_2D
    fprintf(out, "%" LF " %.15" LE "\n", r, normalization*(DOUBLE)RDup.N[i]/(r));
#endif
#ifdef TRIAL_1D
    fprintf(out, "%"LF" %.15"LE "\n", r, normalization*(DOUBLE)RDup.N[i]);
#endif
  }
  fclose(out);
#endif

#ifdef BC_PBC_X
  CaseX(
    normalization = 1./((DOUBLE) (RD.times_measured)*Nup*RDxup.step/RDxup.max);
    Fopen(out, "outrdxup.dat", (initialized || file_append)?"a":"w", "Radial Distribution");
    for(i=0; i<RD.size; i++) {
      r = (DOUBLE) (i+0.5) * RDxup.step;
      fprintf(out, "%"LF" %.15"LE "\n", r, normalization*(DOUBLE)RDxup.N[i]);
    }
    fclose(out);
  );
#else
  CaseX(
    normalization = 1./((DOUBLE) (2*RD.times_measured*Nup)*RDxup.step);
    Fopen(out, "outrdxup.dat", (initialized || file_append)?"a":"w", "Radial Distribution");
    for(i=0; i<RD.size; i++) {
      r = (DOUBLE) (i+0.5) * RDxup.step;
      fprintf(out, "%"LF" %.15"LE "\n", r, normalization*(DOUBLE)RDxup.N[i]);
    }
    fclose(out);
  );
#endif

#ifdef BC_PBC_Y
  CaseY(
    normalization = 1./((DOUBLE) (RD.times_measured)*Nup*RDyup.step/RDyup.max);
    Fopen(out, "outrdyup.dat", (initialized || file_append)?"a":"w", "Radial Distribution");
    for(i=0; i<RD.size; i++) {
      r = (DOUBLE) (i+0.5) * RDyup.step;
      fprintf(out, "%"LF" %.15"LE "\n", r, normalization*(DOUBLE)RDyup.N[i]);
    }
    fclose(out);
  );
#else
  CaseY(
    normalization = 1./((DOUBLE) (2*RD.times_measured*Nup)*RDyup.step);
    Fopen(out, "outrdyup.dat", (initialized || file_append)?"a":"w", "Radial Distribution");
    for(i=0; i<RD.size; i++) {
      r = (DOUBLE) (i+0.5) * RDyup.step;
      fprintf(out, "%"LF" %.15"LE "\n", r, normalization*(DOUBLE)RDyup.N[i]);
    }
    fclose(out);
  );
#endif

#ifdef BC_PBC_Z
  CaseZ(
    Fopen(out, "outrdzup.dat", (initialized || file_append)?"a":"w", "Radial Distribution");
    normalization = 1./((DOUBLE) (RD.times_measured)*Nup*RDzup.step/RDzup.max);
    for(i=0; i<RD.size; i++) {
      r = (DOUBLE) (i+0.5) * RDzup.step;
      fprintf(out, "%"LF" %.15"LE "\n", r, normalization*(DOUBLE)RDzup.N[i]);
    }
    fclose(out);
  );
#else
  CaseZ(
    Fopen(out, "outrdzup.dat", (initialized || file_append)?"a":"w", "Radial Distribution");
    normalization = 1./((DOUBLE) (2*RD.times_measured*Nup)*RDzup.step);
    for(i=0; i<RD.size; i++) {
      r = (DOUBLE) (i+0.5) * RDzup.step;
      fprintf(out, "%"LF" %.15"LE "\n", r, normalization*(DOUBLE)RDzup.N[i]);
    }
    fclose(out);
  );
#endif

  // down distributions
#ifdef BC_ABSENT
#ifdef TRIAL_3D
    normalization = 1./(DOUBLE)(4.*PI*RD.step*RD.times_measured*Ndn);
#endif
#ifdef TRIAL_2D
    normalization = 1./(DOUBLE)(2.*PI*RD.step*RD.times_measured*Ndn);
#endif
#ifdef TRIAL_1D
    normalization = 1./((DOUBLE) (2*RD.times_measured*Ndn)*RDdn.step);
#endif
  Fopen(out, "outrddn.dat", (initialized || file_append)?"a":"w", "Radial Distribution");
  for(i=0; i<RD.size; i++) {
    r = (DOUBLE) (i+0.5) * RDdn.step;
#ifdef TRIAL_3D
    fprintf(out, "%" LF " %.15" LE "\n", r, normalization*(DOUBLE)RDdn.N[i]/(r*r));
#endif
#ifdef TRIAL_2D
    fprintf(out, "%" LF " %.15" LE "\n", r, normalization*(DOUBLE)RDdn.N[i]/(r));
#endif
#ifdef TRIAL_1D
    fprintf(out, "%"LF" %.15"LE "\n", r, normalization*(DOUBLE)RDdn.N[i]);
#endif
  }
  fclose(out);
#endif

#ifdef BC_PBC_X
  CaseX(
    normalization = 1./((DOUBLE) (RD.times_measured)*Ndn*RDxdn.step/RDxdn.max);
    Fopen(out, "outrdxdn.dat", (initialized || file_append)?"a":"w", "Radial Distribution");
    for(i=0; i<RD.size; i++) {
      r = (DOUBLE) (i+0.5) * RDxdn.step;
      fprintf(out, "%"LF" %.15"LE "\n", r, normalization*(DOUBLE)RDxdn.N[i]);
    }
    fclose(out);
  );
#else
  CaseX(
    normalization = 1./((DOUBLE) (2*RD.times_measured*Ndn)*RDxup.step);
    Fopen(out, "outrdxdn.dat", (initialized || file_append)?"a":"w", "Radial Distribution");
    for(i=0; i<RD.size; i++) {
      r = (DOUBLE) (i+0.5) * RDxdn.step;
      fprintf(out, "%"LF" %.15"LE "\n", r, normalization*(DOUBLE)RDxdn.N[i]);
    }
    fclose(out);
  );
#endif

#ifdef BC_PBC_Y
  CaseY(
    normalization = 1./((DOUBLE) (RD.times_measured)*Ndn*RDydn.step/RDydn.max);
    Fopen(out, "outrdydn.dat", (initialized || file_append)?"a":"w", "Radial Distribution");
    for(i=0; i<RD.size; i++) {
      r = (DOUBLE) (i+0.5) * RDydn.step;
      fprintf(out, "%"LF" %.15"LE "\n", r, normalization*(DOUBLE)RDydn.N[i]);
    }
    fclose(out);
  );
#else
  CaseY(
    normalization = 1./((DOUBLE) (2*RD.times_measured*Ndn)*RDyup.step);
    Fopen(out, "outrdydn.dat", (initialized || file_append)?"a":"w", "Radial Distribution");
    for(i=0; i<RD.size; i++) {
      r = (DOUBLE) (i+0.5) * RDydn.step;
      fprintf(out, "%"LF" %.15"LE "\n", r, normalization*(DOUBLE)RDydn.N[i]);
    }
    fclose(out);
  );
#endif

#ifdef BC_PBC_Z
  CaseZ(
    normalization = 1./((DOUBLE) (RD.times_measured)*Ndn*RDzdn.step/RDzdn.max);
    Fopen(out, "outrdzdn.dat", (initialized || file_append)?"a":"w", "Radial Distribution");
    for(i=0; i<RD.size; i++) {
      r = (DOUBLE) (i+0.5) * RDzdn.step;
      fprintf(out, "%"LF" %.15"LE "\n", r, normalization*(DOUBLE)RDzdn.N[i]);
    }
    fclose(out);
  );
#else
  CaseZ(
    normalization = 1./((DOUBLE) (2*RD.times_measured*Ndn)*RDzup.step);
    Fopen(out, "outrdzdn.dat", (initialized || file_append)?"a":"w", "Radial Distribution");
    for(i=0; i<RD.size; i++) {
      r = (DOUBLE) (i+0.5) * RDzdn.step;
      fprintf(out, "%"LF" %.15"LE "\n", r, normalization*(DOUBLE)RDzdn.N[i]);
    }
    fclose(out);
  );
#endif

  if(MC != DIFFUSION) save_pure = OFF;

  if(save_pure) {
    if(RDup_pure.times_measured == 0) {
      Warning("Attempt to save pure radial distribution function without any measurement\n");
      save_pure = OFF;
    }
    else {
#ifdef BC_ABSENT
      //normalization_pure = 1./((DOUBLE) (RDup_pure.times_measured)*Nmean*RD.step);
      //normalization_pure = 1./((DOUBLE) (2*RDup_pure.times_measured)*RD.step);
      normalization_pure_up = 1./((DOUBLE) (2*RDup_pure.times_measured)*RDzup.step) / (DOUBLE) Nup;
      normalization_pure_dn = 1./((DOUBLE) (2*RDup_pure.times_measured)*RDzup.step) / (DOUBLE) Ndn;
#else
      normalization_pure_up = 1./((DOUBLE) (RDup_pure.times_measured)*Nup*RD.step/RD.max);
      normalization_pure_dn = 1./((DOUBLE) (RDup_pure.times_measured)*Ndn*RD.step/RD.max);
#endif

      Fopen(out,  "outrdzuppure.dat", (initialized || file_append)?"a":"w", "Pure radial distribution");
      Fopen(out2, "outrdzdnpure.dat", (initialized || file_append)?"a":"w", "Pure radial distribution");
      for(i=0; i<RD.size; i++) {
        //r = (DOUBLE) (i+0.5) * RD.step;
        r = (DOUBLE) (i+0.5) * RDzup.step;
        fprintf(out,  "%"LF" %.15"LE "\n", r, normalization_pure_up*(DOUBLE)RDup_pure.N[i]);
        fprintf(out2, "%"LF" %.15"LE "\n", r, normalization_pure_dn*(DOUBLE)RDdn_pure.N[i]);
        RDup_pure.N[i] = 0;
        RDdn_pure.N[i] = 0;
      }
      RDup_pure.times_measured = 0;
      RDdn_pure.times_measured = 0;
      fclose(out);
      fclose(out2);
    }
  }

  initialized = ON;
  return 0;
}

/****************************** Save TBDM ************************************/
int SaveTBDM(void) {
  int i;
  DOUBLE r;
  FILE *out;
  static int initialized = OFF;

#ifndef TRIAL_1D // 2D or 3D
  Fopen(out, "outtr.dat", (initialized || file_append)?"a":"w", "TBDM");
#else // save TBDM, same spin in 1D
  Fopen(out, "outtr12.dat", (initialized || file_append)?"a":"w", "TBDM12");
#endif

  // save TBDM
  if(TBDM12.f[0]/TBDM12.N[0]<0.1) {
    TBDM12.f[0] = 0.;
    Warning("TBDM goes to zero as r -> 0 !\n");
  }

  for(i=0; i<TBDM12.size; i++) {
    r = (DOUBLE) (i+0.5) * TBDM12.step;
    if(TBDM12.N[i] > 0)
      fprintf(out, "%"LF" %.15"LE "\n", r, TBDM12.f[i]/TBDM12.N[i]);
    else
      fprintf(out, "%"LF" 0.0\n", r);
  }
  fclose(out);

#ifdef TRIAL_1D // save TBDM, same spin in 1D
  out = Fopen(out, "outtr11.dat", (initialized || file_append)?"a":"w", "TBDM11");
  for(i=0; i<TBDM11.size; i++) {
    r = (DOUBLE) (i+0.) * TBDM11.step;
    if(TBDM11.N[i] > 0)
      fprintf(out, "%"LF" %.15"LE "\n", r, TBDM11.f[i]/TBDM11.N[i]);
    else
      fprintf(out, "%"LF" 0.0\n", r);
  }
  fclose(out);

  out = Fopen(out, "outtr22.dat", (initialized || file_append)?"a":"w", "TBDM22");
  for(i=0; i<TBDM22.size; i++) {
    r = (DOUBLE) (i+0.) * TBDM22.step;
    if(TBDM22.N[i] > 0)
      fprintf(out, "%"LF" %.15"LE "\n", r, TBDM22.f[i]/TBDM22.N[i]);
    else
      fprintf(out, "%"LF" 0.0\n", r);
  }
  fclose(out);

  // save OBDM
  Fopen(out, "outdrt11.dat", (initialized || file_append)?"a":"w", "OBDM2");
  for(i=0; i<TBDM11.size; i++) {
    r = (DOUBLE) (i+0.) * TBDM11.step;
    if(TBDM11.N[i] > 0)
      fprintf(out, "%"LF" %.15"LE "\n", r, TBDM11.OBDM[i]/TBDM11.N[i]);
    else
      fprintf(out, "%"LF" 0.0\n", r);
  }
  fclose(out);

  Fopen(out, "outdrt22.dat", (initialized || file_append)?"a":"w", "OBDM2");
  for(i=0; i<TBDM22.size; i++) {
    r = (DOUBLE) (i+0.) * TBDM22.step;
    if(TBDM22.Nr[i] > 0)
      fprintf(out, "%"LF" %.15"LE "\n", r, TBDM22.OBDM[i]/TBDM22.N[i]);
    else
      fprintf(out, "%"LF" 0.0\n", r);
  }
  fclose(out);

  out = Fopen(out, "outtr12r.dat", (initialized || file_append)?"a":"w", "TBDM11");
  for(i=0; i<TBDM11.size; i++) {
    r = (DOUBLE) (i+0.5) * TBDM11.step;
    if(TBDM12.Nr[i] > 0)
      fprintf(out, "%"LF" %.15"LE "\n", r, TBDM12.fr[i]/TBDM12.Nr[i]);
    else
      fprintf(out, "%"LF" 0.0\n", r);
  }
  fclose(out);

  out = Fopen(out, "outtr11r.dat", (initialized || file_append)?"a":"w", "TBDM11");
  for(i=0; i<TBDM11.size; i++) {
    r = (DOUBLE) (i+0.5) * TBDM11.step;
    if(TBDM11.Nr[i] > 0)
      fprintf(out, "%"LF" %.15"LE "\n", r, TBDM11.fr[i]/TBDM11.Nr[i]);
    else
      fprintf(out, "%"LF" 0.0\n", r);
  }
  fclose(out);

  out = Fopen(out, "outtr22r.dat", (initialized || file_append)?"a":"w", "TBDM22");
  for(i=0; i<TBDM22.size; i++) {
    r = (DOUBLE) (i+0.5) * TBDM22.step;
    if(TBDM22.Nr[i] > 0)
      fprintf(out, "%"LF" %.15"LE "\n", r, TBDM22.fr[i]/TBDM22.Nr[i]);
    else
      fprintf(out, "%"LF" 0.0\n", r);
  }
  fclose(out);
#endif

#ifdef OBDM_BOSONS
  Fopen(out, "outtr12b.dat", (initialized || file_append)?"a":"w", "TBDM12");
  for(i=0; i<TBDMbosons12.size; i++) {
    r = (DOUBLE) (i+0.5) * TBDMbosons12.step;
    if(TBDMbosons12.N[i] > 0)
      fprintf(out, "%"LF" %.15"LE "\n", r, TBDMbosons12.f[i]/TBDMbosons12.N[i]);
    else
      fprintf(out, "%"LF" 0.0\n", r);
  }
  fclose(out);

  out = Fopen(out, "outtr11b.dat", (initialized || file_append)?"a":"w", "TBDM11");
  for(i=0; i<TBDMbosons11.size; i++) {
    r = (DOUBLE) (i+0.) * TBDMbosons11.step;
    if(TBDMbosons11.N[i] > 0)
      fprintf(out, "%"LF" %.15"LE "\n", r, TBDMbosons11.f[i]/TBDMbosons11.N[i]);
    else
      fprintf(out, "%"LF" 0.0\n", r);
  }
  fclose(out);

  out = Fopen(out, "outtr22b.dat", (initialized || file_append)?"a":"w", "TBDM22");
  for(i=0; i<TBDMbosons22.size; i++) {
    r = (DOUBLE) (i+0.) * TBDMbosons22.step;
    if(TBDMbosons22.N[i] > 0)
      fprintf(out, "%"LF" %.15"LE "\n", r, TBDMbosons22.f[i]/TBDMbosons22.N[i]);
    else
      fprintf(out, "%"LF" 0.0\n", r);
  }
  fclose(out);

  // save OBDM
  Fopen(out, "outdrt11b.dat", (initialized || file_append)?"a":"w", "OBDM2");
  for(i=0; i<TBDMbosons11.size; i++) {
    r = (DOUBLE) (i+0.) * TBDMbosons11.step;
    if(TBDMbosons11.N[i] > 0)
      fprintf(out, "%"LF" %.15"LE "\n", r, TBDMbosons11.OBDM[i]/TBDMbosons11.N[i]);
    else
      fprintf(out, "%"LF" 0.0\n", r);
  }
  fclose(out);

  Fopen(out, "outdrt22b.dat", (initialized || file_append)?"a":"w", "OBDM2");
  for(i=0; i<TBDMbosons22.size; i++) {
    r = (DOUBLE) (i+0.) * TBDMbosons22.step;
    if(TBDMbosons22.Nr[i] > 0)
      fprintf(out, "%"LF" %.15"LE "\n", r, TBDMbosons22.OBDM[i]/TBDMbosons22.N[i]);
    else
      fprintf(out, "%"LF" 0.0\n", r);
  }
  fclose(out);

  out = Fopen(out, "outtr12rb.dat", (initialized || file_append)?"a":"w", "TBDM11");
  for(i=0; i<TBDMbosons12.size; i++) {
    r = (DOUBLE) (i+0.5) * TBDMbosons12.step;
    if(TBDMbosons12.Nr[i] > 0)
      fprintf(out, "%"LF" %.15"LE "\n", r, TBDMbosons12.fr[i]/TBDMbosons12.Nr[i]);
    else
      fprintf(out, "%"LF" 0.0\n", r);
  }
  fclose(out);

  out = Fopen(out, "outtr11rb.dat", (initialized || file_append)?"a":"w", "TBDM11");
  for(i=0; i<TBDMbosons11.size; i++) {
    r = (DOUBLE) (i+0.5) * TBDMbosons11.step;
    if(TBDMbosons11.Nr[i] > 0)
      fprintf(out, "%"LF" %.15"LE "\n", r, TBDMbosons11.fr[i]/TBDMbosons11.Nr[i]);
    else
      fprintf(out, "%"LF" 0.0\n", r);
  }
  fclose(out);

  out = Fopen(out, "outtr22rb.dat", (initialized || file_append)?"a":"w", "TBDM22");
  for(i=0; i<TBDMbosons22.size; i++) {
    r = (DOUBLE) (i+0.5) * TBDMbosons22.step;
    if(TBDMbosons22.Nr[i] > 0)
      fprintf(out, "%"LF" %.15"LE "\n", r, TBDMbosons22.fr[i]/TBDMbosons22.Nr[i]);
    else
      fprintf(out, "%"LF" 0.0\n", r);
  }
  fclose(out);
#endif

#ifndef TRIAL_1D
  // save OBDM
  Fopen(out, "outdr2.dat", (initialized || file_append)?"a":"w", "OBDM2");
  for(i=0; i<TBDM12.size; i++) {
    r = (DOUBLE) (i+0.5) * TBDM12.step;
    if(TBDM12.N[i] > 0)
      fprintf(out, "%"LF" %.15"LE "\n", r, TBDM12.OBDM[i]/TBDM12.N[i]);
    else
      fprintf(out, "%"LF" 0.0\n", r);
  }
  fclose(out);

  // save r2 (direct estimator)
  Fopen(out, "outtr2.dat", (initialized || file_append)?"a":"w", "TBDM");
  for(i=0; i<TBDM12.size; i++) {
    r = (DOUBLE) (i+0.5) * TBDM12.step;
    if(TBDM12.R2[i]<0) Warning("  TBDM.R2: negative value of TBDM.R2\n");
    if(TBDM12.f[i]<0) Warning("  TBDM.R2: negative value of TBDM.f\n");
    if(TBDM12.f[i] > 0)
      //fprintf(out, "%"LF" %.15"LE "\n", r, sqrt(fabs(TBDM.R2[i]/TBDM.f[i]))*kF);
      fprintf(out, "%"LF" %.15"LE "\n", r, TBDM12.R2[i]/TBDM12.f[i]*kF*kF);
    else
      fprintf(out, "%"LF" 0.0\n", r);
  }
  fclose(out);

  // save r2 (corrected estimator)
  Fopen(out, "outdr2c.dat", (initialized || file_append)?"a":"w", "TBDM");
  for(i=0; i<TBDM12.size; i++) {
    r = (DOUBLE) (i+0.5) * TBDM12.step;
    if(TBDM12.f[i]>0)
     // fprintf(out, "%"LF" %.15"LE "\n", r, (TBDM.R2corrected[i])/TBDM.fcorrected[i]);
     //fprintf(out, "%"LF" %.15"LE "\n", r, sqrt(fabs(TBDM.R2corrected[i]/TBDM.fcorrected[i]))*kF);
     fprintf(out, "%"LF" %.15"LE "\n", r, TBDM12.R2corrected[i]/TBDM12.fcorrected[i]*kF*kF);
    else
      fprintf(out, "%"LF" 0.0\n", r);
  }
  fclose(out);

  // save phi(r)
  Fopen(out, "outphi.dat", (initialized || file_append)?"a":"w", "phi(r)");
  for(i=0; i<TBDM_MATRIX.size; i++) {
    r = ((DOUBLE) i + 0.5) * TBDM_MATRIX.step;
    if(TBDM_MATRIX.N[0][0] > 0) // F(r) r^2 
      fprintf(out, "%"LF" %.15"LE "\n", r, TBDM_MATRIX.f[0][i]/(4.*pi*TBDM_MATRIX.step*TBDM_MATRIX.N[0][0]));
    else
      fprintf(out, "%"LF" 0.0\n", r);

    TBDM_MATRIX.f[0][i] = 0.;
  }
  TBDM_MATRIX.N[0][0] = 0;
  fclose(out);

  // save phi(r) r^2
  Fopen(out, "outphir2.dat", (initialized || file_append)?"a":"w", "phi(r)");
  for(i=0; i<TBDM_MATRIX.size; i++) {
    r = ((DOUBLE) i + 0.5) * TBDM_MATRIX.step;
    if(TBDM_MATRIX.N[1][0] > 0)
      fprintf(out, "%"LF" %.15"LE "\n", r, TBDM_MATRIX.f[1][i]/TBDM_MATRIX.N[1][0]);
    else
      fprintf(out, "%"LF" 0.0\n", r);

    TBDM_MATRIX.f[1][i] = 0.;
  }
  TBDM_MATRIX.N[1][0] = 0;
  fclose(out);

  // save phi(r) r^4
  Fopen(out, "outphir4.dat", (initialized || file_append)?"a":"w", "phi(r)");
  for(i=0; i<TBDM_MATRIX.size; i++) {
    r = ((DOUBLE) i + 0.5) * TBDM_MATRIX.step;
    if(TBDM_MATRIX.N[2][0] > 0)
     fprintf(out, "%"LF" %.15"LE "\n", r, TBDM_MATRIX.f[2][i]/TBDM_MATRIX.N[2][0]);
    else
      fprintf(out, "%"LF" 0.0\n", r);

    TBDM_MATRIX.f[2][i] = 0.;
  }
  TBDM_MATRIX.N[2][0] = 0;
  fclose(out);
#endif

  initialized = ON;

  return 0;
}

/****************************** Save OBDM ************************************/
int SaveOBDM(void) {
  int i;
  DOUBLE r;
  FILE *out;
#ifdef BC_ABSENT // i.e. in the trap
  FILE *out2;
#endif
#ifdef TRIAL_1D
  FILE *out3;
#endif
  static int initialized = OFF;

#ifndef TRIAL_1D // 2D and 3D
  Fopen(out, file_OBDM, (initialized || file_append)?"a":"w", "OBDM");
#else // 1D
  Fopen(out,  "outdr11.dat", (initialized || file_append)?"a":"w", "OBDM");
  Fopen(out3, "outdr22.dat", (initialized || file_append)?"a":"w", "OBDM");
#endif
#ifdef BC_ABSENT // i.e. in the trap
  Fopen(out2, "outdrn.dat", (initialized || file_append)?"a":"w", "OBDM");
#endif

  if(OBDM.f[0]/OBDM.N[0]<0.1) {
     OBDM.f[0] = 0.;
     Warning("OBDM goes to zero as r -> 0!\n");
  }

  for(i=0; i<OBDM.size; i++) {
    r = (DOUBLE) (i+0.5) * OBDM.step;

#ifdef BC_1DPBC
    //r *= n; // rescale by total density
    r *= L/Nmean; // rescale by single component density
#endif

    if(OBDM.N[i] > 0) 
      fprintf(out, "%"LF" %.15"LE "\n", r, OBDM.f[i]/OBDM.N[i]);
    else
      fprintf(out, "%"LF" 0.0\n", r);

#ifdef BC_ABSENT // i.e. in the trap
    if(OBDMtrap.N[i] > 0) 
      fprintf(out2, "%"LF" %.15"LE "\n", r, OBDMtrap.f[i]/OBDMtrap.N[i]);
    else
      fprintf(out2, "%"LF" 0.0\n", r);
#endif

#ifdef TRIAL_1D
    if(OBDM22.N[i] > 0) 
      fprintf(out3, "%"LF" %.15"LE "\n", r, OBDM22.f[i]/OBDM22.N[i]);
    else
      fprintf(out3, "%"LF" 0.0\n", r);
#endif
  }

  fclose(out);
#ifdef BC_ABSENT // i.e. in the trap
  fclose(out2);
#endif
#ifdef TRIAL_1D
  fclose(out3);

#endif

#ifdef OBDM_BOSONS
  Fopen(out,  "outdr11b.dat", (initialized || file_append)?"a":"w", "OBDM");
  Fopen(out3, "outdr22b.dat", (initialized || file_append)?"a":"w", "OBDM");

  for(i=0; i<OBDM.size; i++) {
    r = (DOUBLE) (i+0.5) * OBDM.step;
#ifdef BC_1DPBC
    //r *= n; // rescale by density
    r *= L/Nmean; // rescale by single component density

#endif
    if(OBDMbosons.N[i] > 0) 
      fprintf(out, "%"LF" %.15"LE "\n", r, OBDMbosons.f[i]/OBDMbosons.N[i]);
    else
      fprintf(out, "%"LF" 0.0\n", r);

    if(OBDMbosons22.N[i] > 0) 
      fprintf(out3, "%"LF" %.15"LE "\n", r, OBDMbosons22.f[i]/OBDMbosons22.N[i]);
    else
      fprintf(out3, "%"LF" 0.0\n", r);
  }

  fclose(out);
  fclose(out3);
#endif

  initialized = ON;
  return 0;
}

/****************************** Save OBDM ************************************/
int SaveOBDM_PathIntegral(void) {
  int i;
  DOUBLE r, normalization, V;
  FILE *out;
  static int initialized = OFF;

  Fopen(out, file_OBDM, (initialized || file_append)?"a":"w", "OBDM");

  V = 2. * Ndens / n;
  normalization = 1./((DOUBLE)(OBDMpath_integral.times_measured)/V*2.*OBDMpath_integral.step);

  for(i=0; i<OBDMpath_integral.size; i++) {
    r = (DOUBLE) (i+0.5) * OBDMpath_integral.step;

    //r *= n; // rescale by total density
    r *= L/Nmean; // rescale by single component density

    if(OBDMpath_integral.N[i] > 0) 
      fprintf(out, "%"LF" %.15"LE "\n", r, OBDMpath_integral.N[i]*normalization);
    else
      fprintf(out, "%"LF" 0.0\n", r);
  }
  fclose(out);

  for(i=0; i<OBDMpath_integral.size; i++) OBDMpath_integral.N[i] = 0;
  OBDMpath_integral.times_measured = 0;

  initialized = ON;
  return 0;
}

/****************************** Save OBDM Matrix *****************************/
int SaveOBDMMatrix(void) {
  int i, j;
  DOUBLE x, y;
  FILE *out, *out1;

  Fopen(out, file_OBDM_MATRIX, "a", "OBDMmatrix");
  Fopen(out1, "outobdmh.dat", "w", "OBDMmatrix");
  for(i=0; i<OBDM_MATRIX.size; i++) {
    x = (DOUBLE) i * OBDM_MATRIX.step;
    for(j=0; j<OBDM_MATRIX.size; j++) {
      y = (DOUBLE) j * OBDM_MATRIX.step;

      if(OBDM_MATRIX.N[i][j] > 0)
        fprintf(out, "%.15"LE " ", OBDM_MATRIX.f[i][j] / OBDM_MATRIX.N[i][j]);
      else
        fprintf(out, "0 ");
      OBDM_MATRIX.f[i][j] = 0;
      OBDM_MATRIX.N[i][j] = 0;
    }
    fprintf(out, "\n");
  }
  fclose(out);
  fprintf(out1, "%"LG, OBDM_MATRIX.step);
  fclose(out1);

  return 0;
}

/****************************** Save TBDM Matrix *****************************/
int SaveTBDMMatrix(void) {
  int i, j;
  DOUBLE x, y;
  FILE *out;
  static int initialized = OFF;

  Fopen(out, "outtbdm.dat", (initialized || file_append)?"a":"w", "TBDMmatrix");
  for(i=0; i<TBDM12.size; i++) {
    x = (DOUBLE) i * TBDM12.step;
    for(j=0; j<TBDM_MATRIX.size; j++) {
      y = (DOUBLE) j * TBDM_MATRIX.step;

      if(TBDM_MATRIX.N[i][j]>0)
        fprintf(out, "%.15"LE " ", TBDM_MATRIX.f[i][j] / TBDM_MATRIX.N[i][j]);
      else
        fprintf(out, "0 ");
      TBDM_MATRIX.f[i][j] = 0.;
      TBDM_MATRIX.N[i][j] = 0;
    }
    fprintf(out, "\n");
  }
  fclose(out);

  return 0;
}

/****************************** Save Momentum Distribution *******************/
int SaveMomentumDistribution(void) {
  int i, j;
  FILE *out;
  static int initialized = OFF;
  DOUBLE normalization;
  DOUBLE dk;

  Fopen(out, "outnk1k2.dat", (initialized || file_append)?"a":"w", "momentum distribution");
  normalization = 1./Nk.times_measured;
  for(i=0; i<Nk.size; i++) {
    for(j=0; j<Nk.size; j++) {
      fprintf(out, "%.15"LE " ", normalization*Nk.f[i][j]);
      //Nk.f[i][j] = 0.;
    }
    fprintf(out, "\n");
  }
  fclose(out);

  Fopen(out, "outnk.dat", (initialized || file_append)?"a":"w", "momentum distribution");
  dk = 2.*PI/L;
  for(i=0; i<Nk.size; i++) {
    for(j=0; j<Nk.size; j++) {
      fprintf(out, "%.15"LE " %.15"LE "\n", sqrt(Nk.k[i]*Nk.k[i]+Nk.k[j]*Nk.k[j])/kF, normalization*Nk.f[i][j]/(2.*PI*dk));
    }
  }
  Nk.times_measured = 0;
  fclose(out);

  for(i=0; i<Nk.size; i++) {
    for(j=0; j<Nk.size; j++) {
      Nk.f[i][j] = 0.;
    }
  }
  Nk.times_measured = 0;

  initialized = ON;
  return 0;
}

int SaveMomentumDistribution1D(void) {
// Save momentum distribution 1D
// n(k) = 2 \int_0^\infty Cos(k x) g_1(x) dx
  FILE *out11, *out22;
  static int initialized = OFF;
  int i;
  DOUBLE k, normalization, dk;

  Fopen(out11, "outnk11.dat", (initialized || file_append)?"a":"w", "momentum distribution 11");
  Fopen(out22, "outnk22.dat", (initialized || file_append)?"a":"w", "momentum distribution 22");
#ifdef BC_ABSENT
  dk = OBDM.k[1]-OBDM.k[0];
  //normalization = OBDM.max/dk/(double)Nmean/(2.*PI)/(DOUBLE)OBDM.Nktimes_measured;

  normalization = dk*OBDM.Nk[0];
  for(i=1; i<OBDM.Nksize; i++) normalization += 2.*dk*OBDM.Nk[i];
  normalization /= (2.*PI);
  normalization = (DOUBLE)Nup/normalization;
  for(i=0; i<OBDM.Nksize; i++) {
    fprintf(out11, "%.15"LF" %.15"LE "\n", OBDM.k[i], OBDM.Nktimes_measured?(normalization*OBDM.Nk[i]):0);
    OBDM.Nk[i] = 0.;
  }

  normalization = dk*OBDM.Nk[0];
  for(i=0; i<OBDM.Nksize; i++) normalization += 2.*dk*OBDM22.Nk[i];
  normalization /= (2.*PI);
  normalization = (DOUBLE)Ndn/normalization;
  for(i=0; i<OBDM.Nksize; i++) {
    fprintf(out22, "%.15"LF" %.15"LE "\n", OBDM.k[i], OBDM22.Nktimes_measured?(normalization*OBDM22.Nk[i]):0);
    OBDM22.Nk[i] = 0.;
  }
#else
  dk = 2.*PI/L;

  //normalization = n/dk; //  normalize by the total density
  normalization = L/Nmean/dk; //  normalize by the sinlge component density
  normalization *= 2.*PI;
  for(i=0; i<OBDM.Nksize; i++) {
    //k = OBDM.k[i]/n; // normalize by the total density
    //k = OBDM.k[i]/(L/Nmean); // normalize by the sinlge component density
    k = OBDM.k[i]/(PI*L/Nmean); // normalize by the Fermi energyof a sinlge component
    fprintf(out11, "%.15"LF" %.15"LE "\n", k, OBDM.Nktimes_measured?(normalization*OBDM.Nk[i]/(DOUBLE)OBDM.Nktimes_measured):0);
    fprintf(out22, "%.15"LF" %.15"LE "\n", k, OBDM22.Nktimes_measured?(normalization*OBDM22.Nk[i]/(DOUBLE)OBDM22.Nktimes_measured):0);
    OBDM.Nk[i] = OBDM22.Nk[i] = 0.;
  }
#endif

  fclose(out11);
  fclose(out22);

  OBDM.Nktimes_measured = 0;
  OBDM22.Nktimes_measured = 0;

#ifdef OBDM_BOSONS
  Fopen(out11, "outnk11b.dat", (initialized || file_append)?"a":"w", "momentum distribution 11");
  Fopen(out22, "outnk22b.dat", (initialized || file_append)?"a":"w", "momentum distribution 22");
  for(i=0; i<OBDM.Nksize; i++) {
#ifdef BC_ABSENT
    k = OBDM.k[i];
#else
    //k = OBDM.k[i]/n; // normalize by the total density
    //k = OBDM.k[i]/(L/Nmean); // normalize by the sinlge component density
    k = OBDM.k[i]/(PI*L/Nmean); // normalize by the Fermi energyof a sinlge component
#endif
    fprintf(out11, "%.15"LF" %.15"LE "\n", k, OBDMbosons.Nktimes_measured?(normalization*OBDMbosons.Nk[i]/(DOUBLE)OBDMbosons.Nktimes_measured):0);
    fprintf(out22, "%.15"LF" %.15"LE "\n", k, OBDMbosons22.Nktimes_measured?(normalization*OBDMbosons22.Nk[i]/(DOUBLE)OBDMbosons22.Nktimes_measured):0);
    OBDMbosons.Nk[i] = OBDMbosons22.Nk[i] = 0.;
  }
  fclose(out11);
  fclose(out22);

  OBDMbosons.Nktimes_measured = 0;
  OBDMbosons22.Nktimes_measured = 0;
#endif

  initialized = ON;

  return 0;
}

/****************************** Save R2 **************************************/
int SaveR2(DOUBLE f) {
  FILE *out;
  static int initialized = OFF;

  Fopen(out, file_R2, (initialized || file_append)?"a":"w", "R2");
  fprintf(out, "%"LG"\n", f);

  fclose(out);
  initialized = ON;

  return 0;
}

/****************************** Save Z2 **************************************/
int SaveZ2(DOUBLE f) {
  FILE *out;
  static int initialized = OFF;

  Fopen(out, file_z2, (initialized || file_append)?"a":"w", "Z2");
  fprintf(out, "%"LG"\n", f);

  fclose(out);
  initialized = ON;

  return 0;
}

/****************************** Save Pure R2 *********************************/
int SavePureR2(DOUBLE f) {

  FILE *out;
  static int initialized = OFF;

  Fopen(out, file_R2_pure, (initialized || file_append)?"a":"w", "r2 pure");
  if(initialized) fprintf(out, "%"LG"\n", f);

  fclose(out);
  initialized = ON;

  return 0;
}

/****************************** Save Pure Z2 *********************************/
int SavePureZ2(DOUBLE f) {

  FILE *out;
  static int initialized = OFF;

  Fopen(out, file_z2_pure, (initialized || file_append)?"a":"w", "z2 pure");
  if(initialized) fprintf(out, "%"LG"\n", f);

  fclose(out);
  initialized = ON;

  return 0;
}

/**************************** Save Mean R2 ************************************/
void SaveMeanR2(void) {
  xR2 = sqrt(RD.r2 / (DOUBLE) (RD.times_measured * Nmean));
  SaveR2(RD.r2recent);
}

/**************************** Save Mean R2 DMC ********************************/
void SaveMeanR2DMC(void) {
  DOUBLE R2=0, Z2=0;
  DOUBLE R2pure=0, Z2pure=0;
  DOUBLE weight, norm = 0.;
  int w;

  for(w=0; w<Nwalkers; w++) {
    weight = W[w].weight;
    norm += weight;
    R2 += W[w].r2 * weight;
    Z2 += W[w].z2 * weight;
    if(boundary == NO_BOUNDARY_CONDITIONS) {
      R2pure += W[w].r2old * weight;
      Z2pure += W[w].z2old * weight;
    }
  }

  norm = 1./ (DOUBLE) Nwalkers;

  R2 *= norm;
  xR2 += R2;
  R2 = sqrt(R2);
  SaveR2(R2);
  RD.r2recent = R2;

  R2pure = sqrt(norm*R2pure);
  SavePureR2(R2pure);

  if(boundary == NO_BOUNDARY_CONDITIONS) {
    Z2 *= norm;
    zR2 += Z2;
    Z2 = sqrt(Z2);
    SaveZ2(Z2);

    Z2pure = sqrt(norm*Z2pure);
    SavePureZ2(Z2pure);
  }
}

/****************************** SaveWaveFunction *****************************/
int SaveWaveFunction11(struct Grid *G, DOUBLE min, DOUBLE max, char *file_out) {
  int i;
  FILE *out;
  DOUBLE dx, x;
  DOUBLE testNaN;
  int N = 1000;
  static int first_time = ON;

  if(max < 1e-8) { // numerical zero
    Warning("R11.nmax = 0, increasing to %"LF"\n", Lhalf);
    G->max = Lhalf;
    max = Lhalf;
  }
  if(max<Lhalf) max = Lhalf * 1.2;

  Fopen(out, file_out, "w", "Saving wavefunction");
  if(grid_trial>3) N = grid_trial;

  if(first_time) Message("Saving wavefunction (up - up).\n");

  if(max<min) {
    Warning("Save Wavefunction: 'min' argument is larger than the 'max' one\n");
    max = 10.*min;
  }

#ifdef HARD_SPHERE11
  if(min<aA) Warning("Save wavefunction: 'min' is smaller than the size of the HS, aA = %"LF"\n", aA);
  min = aA*1.01;
#endif

  dx = (max-min) / (N-1);

  fprintf(out, "#1)x 2)f 3)lnf 4)fp 5)-f''/f - 2/r f'/f 6)-f''/f - 2/r f'/f + (f'/f)^2 7)[-f''/f - 2/r f'/f]/(m_up/2) + V 8) V\n"); // Eloc = -f''/f - 2/r f'/f + (f'/f)^2

  for(i=0; i<N; i++) {
    x = min + dx * (DOUBLE) i;

    //testNaN = Exp(InterpolateU11(G, x)); // check if f(x) is finite
    testNaN = InterpolateU11(G, x); // check if ln(f(x)) is finite
    if(testNaN == testNaN && testNaN != 2*testNaN) { // check for NaN by comparison to itself and INF
      fprintf(out, "%.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE "\n", x,//1
        Exp(InterpolateU11(G, x)), //2
        InterpolateU11(G, x), //3
        InterpolateFp11(G, x), //4
        InterpolateE11(G, x)-InterpolateFp11(G, x)*InterpolateFp11(G, x), //5
        InterpolateE11(G, x), //6
        (InterpolateE11(G, x)-InterpolateFp11(G, x)*InterpolateFp11(G, x))/m_up + InteractionEnergy11(x), //7
        energy_unit*InteractionEnergy11(x)); //8
    }
  }

  fclose(out);
  first_time = OFF;

  return 0;
}

/****************************** SaveWaveFunction 22 *****************************/
int SaveWaveFunction22(struct Grid *G, DOUBLE min, DOUBLE max, char *file_out) {
  int i;
  FILE *out;
  DOUBLE dx, x;
  DOUBLE testNaN;
  int N = 1000;
  static int first_time = ON;

  if(max < 1e-8) { // numerical zero
    Warning("R22.nmax = 0, increasing to %"LF"\n", Lhalf);
    G->max = Lhalf;
    max = Lhalf;
  }
  if(max<Lhalf) max = Lhalf * 1.2;

  Fopen(out, file_out, "w", "Saving wavefunction22");
  if(grid_trial>3) N = grid_trial;

  if(first_time) Message("Saving wavefunction (up - up).\n");

  if(max<min) {
    Warning("Save Wavefunction: 'min' argument is larger than the 'max' one\n");
    max = 10.*min;
  }

#ifdef HARD_SPHERE22
  if(min<aB) Warning("Save wavefunction: 'min' is smaller than the size of the HS, aB = %"LF"\n");
  min = aB*1.01;
#endif

  dx = (max-min) / (N-1);

  fprintf(out, "#1)x 2)f 3)lnf 4)fp 5)-f''/f - 2/r f'/f 6)-f''/f - 2/r f'/f + (f'/f)^2 7)[-f''/f - 2/r f'/f]/(m_dn/2) + V 8) V\n"); // Eloc = -f''/f - 2/r f'/f + (f'/f)^2

  for(i=0; i<N; i++) {
    x = min + dx * (DOUBLE) i;

    testNaN = Exp(InterpolateU22(G, x));
    if(testNaN == testNaN && testNaN != 2*testNaN) { // check for NaN by comparison to itself and INF
      fprintf(out, "%.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE "\n", 
        x, 
        Exp(InterpolateU22(G, x)), 
        InterpolateU22(G, x),
        InterpolateFp22(G, x), 
        InterpolateE22(G, x)-InterpolateFp22(G, x)*InterpolateFp22(G, x), 
        InterpolateE22(G, x), 
        (InterpolateE22(G, x)-InterpolateFp22(G, x)*InterpolateFp22(G, x))/m_dn + InteractionEnergy22(x),
        energy_unit*InteractionEnergy22(x));
    }
  }

  fclose(out);
  first_time = OFF;

  return 0;
}

/****************************** SaveWaveFunction 12 *****************************/
int SaveWaveFunction12(struct Grid *G, DOUBLE min, DOUBLE max, char *file_out) {
  int i;
  FILE *out;
  DOUBLE dx, x;
  DOUBLE testNaN;
  int N = 1000;
  static int first_time = ON;

  if(grid_trial>3) N = grid_trial;
  if(max < 1e-8) { // numerical zero
    Warning("R12.nmax = 0, increasing to %"LF"\n", Lhalf);
    G->max = Lhalf;
    max = Lhalf;
  }
  if(max<Lhalf) max = Lhalf * 1.2;

  Fopen(out, file_out, "w", "Saving wavefunction12");
  if(first_time) Message("Saving wavefunction (up - down).\n");

  if(max<min) {
    Warning("Save Wavefunction: 'min' argument is larger than the 'max' one\n");
    max = 10.*min;
  }
  if(max<1e-5) {
    Warning("Save Wavefunction: 'max' argument is small, %"LF"\n", max);
  }

#ifdef HARD_SPHERE12
  if(min<a) Warning("Save wavefunction: 'min' is smaller than the size of the HS, a = %"LF"\n", a);
  min = a*1.01;
#endif

  dx = (max-min) / (N-1);

  fprintf(out, "#1)x 2)f 3)lnf 4)fp 5)-f''/f - 2/r f'/f 6)-f''/f - 2/r f'/f + (f'/f)^2 7)[-f''/f - 2/r f'/f]/(2mu) + V 8) V\n"); // Eloc = -f''/f - 2/r f'/f + (f'/f)^2

  for(i=0; i<N; i++) {
    x = min + dx * (DOUBLE) i;

    //testNaN = Exp(InterpolateU12(G, x)); // check if f(x) is finite
    testNaN = InterpolateU12(G, x); // check if ln(f(x)) is finite
    if((testNaN == testNaN && testNaN != 2*testNaN) || testNaN == 0.) { // check for NaN by comparison to itself and INF
      fprintf(out, "%.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE "\n", 
        x, //1
        Exp(InterpolateU12(G, x)), //2
        InterpolateU12(G, x),//3
        InterpolateFp12(G, x), //4
        InterpolateE12(G, x)-InterpolateFp12(G, x)*InterpolateFp12(G, x), //5
        InterpolateE12(G, x), //6
        (InterpolateE12(G, x)-InterpolateFp12(G, x)*InterpolateFp12(G, x))/(2.*m_mu) + InteractionEnergy12(x), //7
        energy_unit*InteractionEnergy12(x)); //8
    }
  }

  fclose(out);
  first_time = OFF;

  return 0;
}

/****************************** SaveWaveFunction *****************************/
int SaveBCSWaveFunction(DOUBLE min, DOUBLE max, char *file_out) {
  long int i;
  FILE *out;
  DOUBLE dx, x;
  int N = 1000;
  static int first_time = ON;

  Fopen(out, file_out, "w", "Saving wavefunction BCS");
  if(first_time) Message("Saving BCS wavefunction.\n");

  if(max<min) {
    Warning("Save Wavefunction: 'min' argument is larger than the 'max' one\n");
    max = 10.*min;
  }

  dx = (max-min) / (DOUBLE) N;

  //fprintf(out, "#N= %li\nmin= %.15"LE "\nmax= %.15"LE "\n", N, min+dx, max);
  ////fprintf(out, "V= %.15"LE "\n", trial_Vo);
  fprintf(out, "#x f fp fpp EV\n");

  for(i=1; i<N; i++) {
    x = min + dx * (DOUBLE) i;
    fprintf(out, "%.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE "\n", x, orbitalF(x), orbitalFp(x), orbitalEloc(x), -orbitalEloc(x)/orbitalF(x)+InteractionEnergy12(x));
  }

  fclose(out);
  first_time = OFF;

  return 0;
}

/****************************** SaveWaveFunction 12 SYM *****************************/
int SaveWaveFunction12SYM(struct Grid *G, DOUBLE min, DOUBLE max, char *file_out) {
#ifdef SYM_OPPOSITE_SPIN
  int i;
  FILE *out;
  DOUBLE dx, x;
  DOUBLE testNaN;
  int N = 1000;
  static int first_time = ON;

  if(grid_trial>3) N = grid_trial;
  if(max < 1e-8) {
    Warning("  saveing w.f. up to max = 10\n");
    max = 10.;
  }

  Fopen(out, file_out, "w", "Saving wavefunction12 SYM");
  if(first_time) Message("Saving wavefunction (up - down).\n");

  if(max<min) {
    Warning("Save Wavefunction: 'min' argument is larger than the 'max' one\n");
    max = 10.*min;
  }
  if(max<1e-5) {
    Warning("Save Wavefunction: 'max' argument is small, %"LF"\n", max);
  }

  dx = (max - min) / (N - 1);

  fprintf(out, "#1)x 2)f 3)lnf 4)fp 5)-f''/f-2/rf'/f 6)-f''/f-2/r f'/f+(f'/f)^2 7)-f''/f-2/rf'/f+V 8)V 9)f' 10)-f''-f'/r\n"); 

  for(i = 0; i<N; i++) {
    x = min + dx * (DOUBLE)i;

    testNaN = InterpolateSYMF(G, x);
    if(testNaN == testNaN && testNaN != 2 * testNaN) { // check for NaN by comparison to itself and INF
      fprintf(out, "%.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE "\n", x, // 1
        InterpolateSYMF(G, x), // 2
        Log(InterpolateSYMF(G, x)), // 3
        InterpolateSYMFp(G, x) / InterpolateSYMF(G, x), // 4
        InterpolateSYME(G, x) / InterpolateSYMF(G, x), // 5
        InterpolateSYME(G, x) / InterpolateSYMF(G, x) + InterpolateSYMFp(G, x)*InterpolateSYMFp(G, x), // 6
        InterpolateSYME(G, x) / InterpolateSYMF(G, x) + InteractionEnergy12(x), // 7
        energy_unit*InteractionEnergy12(x), // 8
        InterpolateSYMFp(G, x), // 9
        InterpolateSYME(G, x)); // 10
    }
  }

  fclose(out);
  first_time = OFF;
#endif

  return 0;
}

/****************************** SaveOrbitalCuts *****************************/
int SaveOrbitalCuts(void) {
  long int i;
  FILE *out;
  DOUBLE dr,r,x,y,z,f1,f2,f2x,f2y,f2z,fE,norm;
  int N = 1000;
  static int first_time = ON;

  if(first_time) Message("Saving orbitals.\n");

  //norm = orbitalF(1e-10);
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
  //norm += orbitalDescreteBCS_F(0,0,0);
#endif
  norm = 1.;

  // [001] direction
  Fopen(out, "wf001.dat", "w", "SaveOrbitalCuts");
  dr = 1./ (DOUBLE) (N-1) * Lhalf;
  for(i=0; i<N; i++) {
    x = (DOUBLE) (i) *dr;
    y = 0;
    z = 0;
    r = sqrt(x*x+y*y+z*z);

    f1 = orbitalF(r);
    f2 = f2x = f2y = f2z = fE = 0.;
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
    f2 = orbitalDescreteBCS_F(x,y,z);
    orbitalDescreteBCS_Fp(&f2x, &f2y, &f2z, x, y, z);
    fE = orbitalDescreteBCS_Fpp(x,y,z);
#endif
    fprintf(out, "%.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE "\n", r, (f1+f2)/norm, f1/norm, f2/norm, f2x, f2y, f2z, fE);
  }
  fclose(out);

  // [011] direction
  Fopen(out, "wf011.dat", "w", "SaveOrbitalCuts");
  dr = 1./ (DOUBLE) (N-1) * Lhalf;
  for(i=0; i<N; i++) {
    x = (DOUBLE) (i) *dr;
    y = (DOUBLE) (i) *dr;
    z = 0;
    r = sqrt(x*x+y*y+z*z);

    f1 = orbitalF(r);
    f2 = f2x = f2y = f2z = fE = 0.;
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
    f2 = orbitalDescreteBCS_F(x,y,z);
    orbitalDescreteBCS_Fp(&f2x, &f2y, &f2z, x, y, z);
    fE = orbitalDescreteBCS_Fpp(x,y,z);
#endif
    fprintf(out, "%.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE "\n", r, (f1+f2)/norm, f1/norm, f2/norm, f2x, f2y, f2z, fE);
  }
  fclose(out);

  // [111] direction
  Fopen(out, "wf111.dat", "w", "SaveOrbitalCuts");
  dr = 1./ (DOUBLE) (N-1) * Lhalf;
  for(i=0; i<N; i++) {
    x = (DOUBLE) (i) *dr;
    y = (DOUBLE) (i) *dr;
    z = (DOUBLE) (i) *dr;
    r = sqrt(x*x+y*y+z*z);

    f1 = orbitalF(r);
    f2 = f2x = f2y = f2z = fE = 0.;
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
    f2 = orbitalDescreteBCS_F(x,y,z);
    orbitalDescreteBCS_Fp(&f2x, &f2y, &f2z, x, y, z);
    fE = orbitalDescreteBCS_Fpp(x,y,z);
#endif
    fprintf(out, "%.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE " %.15"LE "\n", r, (f1+f2)/norm, f1/norm, f2/norm, f2x, f2y, f2z, fE);
  }
  fclose(out);

  first_time = OFF;

  return 0;
}

/****************************** SaveEnergyCut *****************************/
int SaveEnergyCut(void) {
  int w, i;
  FILE *out, *out2;
  DOUBLE direction[3], distance, dr, r, r2, x, y, Eref,E;
  int Ngrid = 1000;

  Message("Saving energy cut ...\n");

  Fopen(out, "prevUp.dat", "w", "SaveEnergyCut");
  for(i=0; i<Nup; i++) fprintf(out, "%.3"LE" %.3"LE" %.3"LE"\n", W[0].x[i], W[0].y[i], W[0].z[i]);
  fclose(out);
  Fopen(out, "prevDn.dat", "w", "SaveEnergyCut");
  for(i=0; i<Ndn; i++) fprintf(out, "%.3"LE" %.3"LE" %.3"LE"\n", W[0].xdn[i], W[0].ydn[i], W[0].zdn[i]);
  fclose(out);

  distance = Lhalf;
  w = 0;
  Eref = WalkerEnergy0(&W[w])*energy_unit;
  Fopen(out, "outEcut11xy.dat", "w", "SaveEnergyCut");
  direction[0] = W[w].x[0]; // store coord. here
  direction[1] = W[w].y[0];
  direction[2] = W[w].z[0];

  dr = distance / (DOUBLE) (50);

  Fopen(out2, "prevBad.dat", "w", "SaveEnergyCut");
  for(x=direction[0]-distance; x<direction[0]+distance; x += dr) {
    for(y=direction[1]-distance; y<direction[1]+distance; y += dr) {
      W[w].x[0] = x;
      W[w].y[0] = y;
      ReduceWalkerToTheBox(&W[w]);
      E = WalkerEnergy0(&W[w])*energy_unit;
      fprintf(out, "%.3"LG" %.3"LG" %.3"LE"\n", x, y, E/Eref);
      if(fabs(E/Eref)>2) fprintf(out2, "%.3"LE" %.3"LE" %.3"LE"\n", W[0].x[0], W[0].y[0], W[0].z[0]);
    }
    fprintf(out, "\n");
  }
  fclose(out);
  fclose(out2);
  W[w].x[0] = direction[0];
  W[w].y[0] = direction[1];
  W[w].z[0] = direction[2];

  distance = pow(n, -1./3.);
  Fopen(out, "outEcut11.dat", "w", "SaveEnergyCut");
  for(w=0; w<Nwalkers; w++) {
    direction[0] = W[w].x[0]-W[w].x[1];
    direction[1] = W[w].y[0]-W[w].y[1];
    direction[2] = W[w].z[0]-W[w].z[1];
    FindNearestImage3D(r2, direction[0], direction[1], direction[2]);
    r = sqrt(r2);
    direction[0] /= r;
    direction[1] /= r;
    direction[2] /= r;

    dr = distance/ (DOUBLE) (Ngrid);
    for(i=1; i<=Ngrid; i++) {
      W[w].x[0] = W[w].x[1] + (DOUBLE)(i) * dr*direction[0];
      W[w].y[0] = W[w].y[1] + (DOUBLE)(i) * dr*direction[1];
      W[w].z[0] = W[w].z[1] + (DOUBLE)(i) * dr*direction[2];
      ReduceWalkerToTheBox(&W[w]);

      fprintf(out, "%.15"LE " %.15"LE "\n", (DOUBLE)(i)*dr, WalkerEnergy0(&W[w])*energy_unit);
    }
    W[w].x[0] = W[w].x[1] + r*direction[0];
    W[w].y[0] = W[w].y[1] + r*direction[1];
    W[w].z[0] = W[w].z[1] + r*direction[2];
    ReduceWalkerToTheBox(&W[w]);
  }
  fclose(out);

  Fopen(out, "outEcut12.dat", "w", "SaveEnergyCut");
  for(w=0; w<Nwalkers; w++) {
    direction[0] = W[w].x[0]-W[w].xdn[1];
    direction[1] = W[w].y[0]-W[w].ydn[1];
    direction[2] = W[w].z[0]-W[w].zdn[1];
    FindNearestImage3D(r2, direction[0], direction[1], direction[2]);
    r = sqrt(r2);
    direction[0] /= r;
    direction[1] /= r;
    direction[2] /= r;

    dr = distance/ (DOUBLE) (Ngrid);
    for(i=1; i<=Ngrid; i++) {
      W[w].x[0] = W[w].xdn[1] + (DOUBLE)(i) * dr*direction[0];
      W[w].y[0] = W[w].ydn[1] + (DOUBLE)(i) * dr*direction[1];
      W[w].z[0] = W[w].zdn[1] + (DOUBLE)(i) * dr*direction[2];
      ReduceWalkerToTheBox(&W[w]);

      fprintf(out, "%.15"LE " %.15"LE "\n", (DOUBLE)(i)*dr, WalkerEnergy0(&W[w])*energy_unit);
    }
    W[w].x[0] = W[w].xdn[1] + r*direction[0];
    W[w].y[0] = W[w].ydn[1] + r*direction[1];
    W[w].z[0] = W[w].zdn[1] + r*direction[2];
    ReduceWalkerToTheBox(&W[w]);
  }
  fclose(out);

  return 0;
}

/****************************** SaveSk ***************************************/
int SaveStaticStructureFactor(void) {
  int i;
  FILE *out1, *out2, *out3, *out4;
  DOUBLE normalization, normalization2;
  DOUBLE Sk11, Sk12;
#ifdef BC_1DPBC_Z // 1D
  DOUBLE kF;
#else // 3D, 2D
  DOUBLE k;
#endif
  static int initialized = OFF;

  // Save mixed/variational estimator
  Fopen(out1, "outsk11.dat", (initialized || file_append)?"a":"w", "SaveStaticStructureFactor");
  Fopen(out2, "outsk12.dat", (initialized || file_append)?"a":"w", "SaveStaticStructureFactor");
  Fopen(out3, "outsk.dat",   (initialized || file_append)?"a":"w", "SaveStaticStructureFactor"); // total Sk
  Fopen(out4, "outskm.dat",  (initialized || file_append)?"a":"w", "SaveStaticStructureFactor"); // magnetic Sk

  // check if the function was measured
  if(Sk.times_measured == 0) {
    Warning("Attempt to save static structure factor without any measurement\n");
    return 1;
  }
  // SAA(k) = <rho_A(k) rho_A(-k)> / NA
  // SBB(k) = <rho_B(k) rho_B(-k)> / NB
  // SAB(k) = <rho_A(k) rho_B(-k)> / (NA NB)^{1/2}
  if(Sk.times_measured) { // protect from division by 0
    normalization = 1. / ((DOUBLE)(Nup+Ndn)*(DOUBLE)Sk.times_measured); // sum both for Nup and Ndn
  }
  else {
    normalization = 1.;
  }
  if(Sk.times_measured && Nup*Ndn) { // protect from division by 0
    normalization2 = 1. / ((DOUBLE)(sqrt(Nup*Ndn))*(DOUBLE)Sk.times_measured);
  }
  else {
    normalization2 = 1.;
  }

  for(i=0; i<Sk.size; i++) {
    Sk11 = Sk.N[i]?(normalization*Sk.f[i]):0;
    Sk12 = Sk.N[i]?(normalization2*Sk.f12[i]):0;
#ifdef BC_1DPBC_Z // rescale by density 1D PBC
    kF = PI*n;
    fprintf(out1, "%.5e %.5e\n", Sk.k[i]/kF, Sk11);
    fprintf(out2, "%.5e %.5e\n", Sk.k[i]/kF, Sk12);
    fprintf(out3, "%.5e %.5e\n", Sk.k[i]/kF, Sk11+Sk12);
    fprintf(out4, "%.5e %.5e\n", Sk.k[i]/kF, Sk11-Sk12);
#else
    k = Sk.k[i];
    if(Sk.degeneracy[i] && k>0) {
      fprintf(out1, "%"LF" %.15"LE "\n", k, Sk11/Sk.degeneracy[i]);
      fprintf(out2, "%"LF" %.15"LE "\n", k, Sk12/Sk.degeneracy[i]);
      fprintf(out3, "%"LF" %.15"LE "\n", k, (Sk11+Sk12)/Sk.degeneracy[i]);
      fprintf(out4, "%"LF" %.15"LE "\n", k, (Sk11-Sk12)/Sk.degeneracy[i]);
    }
#endif
    Sk.f[i] = 0.;
    Sk.f12[i] = 0.;
    Sk.cos[i] = 0.;
    Sk.N[i] = 0;
  }
  Sk.times_measured = 0;

  fclose(out1);
  fclose(out2);
  fclose(out3);
  fclose(out4);

  // Save pure estimator
  if(measure_Sk && MC == DIFFUSION) {
    // open file
    Fopen(out1, "outsk11p.dat", (initialized || file_append)?"a":"w", "SaveStaticStructureFactor");
    Fopen(out2, "outsk12p.dat", (initialized || file_append)?"a":"w", "SaveStaticStructureFactor");
    Fopen(out3, "outskp.dat",   (initialized || file_append)?"a":"w", "SaveStaticStructureFactor"); // total Sk
    Fopen(out4, "outskmp.dat",  (initialized || file_append)?"a":"w", "SaveStaticStructureFactor"); // magnetic Sk
    // check if the function was measured
    if(Sk_pure.times_measured == 0) {
      Warning("Attempt to save pure static structure factor without any measurement\n");
      initialized = ON;
      return 1;
    }
    if(Sk_pure.times_measured) {
      normalization = 1. / ((DOUBLE)(Nup+Ndn)*(DOUBLE)Sk_pure.times_measured); // sum both for Nup and Ndn
    }
    else {
      normalization = 1.;
    }
    if(Sk_pure.times_measured && Nup*Ndn) {
      normalization2 = 1. / ((DOUBLE)(sqrt(Nup*Ndn))*(DOUBLE)Sk_pure.times_measured);
    }
    else {
      normalization2 = 1.;
    }

    for(i=0; i<Sk.size; i++) {
      Sk11 = Sk_pure.N[i]?(normalization*Sk_pure.f[i]):0;
      Sk12 = Sk_pure.N[i]?(normalization2*Sk_pure.f12[i]):0;
#ifdef BC_1DPBC_Z // rescale by density 1D PBC
      kF = PI*n;
      fprintf(out1, "%.5e %.5e\n", Sk.k[i]/kF, Sk11);
      fprintf(out2, "%.5e %.5e\n", Sk.k[i]/kF, Sk12);
      fprintf(out3, "%.5e %.5e\n", Sk.k[i]/kF, Sk11+Sk12);
      fprintf(out4, "%.5e %.5e\n", Sk.k[i]/kF, Sk11-Sk12);
#else
      k = Sk.k[i];
      if(Sk.degeneracy[i] && k>0) {
        fprintf(out1, "%"LF" %.15"LE "\n", k, Sk11/Sk.degeneracy[i]);
        fprintf(out2, "%"LF" %.15"LE "\n", k, Sk12/Sk.degeneracy[i]);
        fprintf(out3, "%"LF" %.15"LE "\n", k, (Sk11+Sk12)/Sk.degeneracy[i]);
        fprintf(out4, "%"LF" %.15"LE "\n", k, (Sk11-Sk12)/Sk.degeneracy[i]);
      }
#endif
      Sk_pure.f[i] = 0.;
      Sk_pure.f12[i] = 0.;
      Sk_pure.N[i] = 0;
    }
    fclose(out1);
    fclose(out2);
    fclose(out3);
    fclose(out4);
    Sk_pure.times_measured = 0;
  }

  initialized = ON;

  return 0;
}

int SaveStaticStructureFactorAngularPart(void) {
  int i;
  FILE *out;
  DOUBLE dphi, phi_min=0.1;
  static int initialized = OFF;

  // Save mixed/variational estimator
  Fopen(out, "outska11.dat", (initialized || file_append)?"a":"w", "SaveStaticStructureFactorAngularPart");
  dphi = phi_min/Sk.size;
  for(i=0; i<Sk.size; i++) {
    fprintf(out, "%"LF" %.15"LE "\n", pi-phi_min+dphi*i, Sk.phiN[i]?(Sk.phi[i]/Sk.phiN[i]):0);
    Sk.phi[i] = 0.;
    Sk.phiN[i] = 0;
  }
  fclose(out);

  initialized = ON;

  return 0;
}

/***************************** Save Superfluid Density *********************/
int SaveSuperfluidDensity(void) {
  FILE *out;
  int i;
  DOUBLE Dreal;
  static int initialized = OFF;
  int Nparticles;

  if(SD.N[SD.size-2] == 0) {
    Warning("superfluid density array is not yet full, cannot save data\n");
    return 0;
  }

#ifdef SD_MEASURE_GLOBAL_WINDING
  Nparticles = Ntot;
#endif
#ifdef SD_MEASURE_ONLY_SPIN_DOWN
  Nparticles = Ndn;
#endif
#ifdef SD_MEASURE_W1_MINUS_W2
  Nparticles = Ntot;
#endif
#ifdef SD_MEASURE_DIFFUSION_COEFFICIENT
  Nparticles = 1;
#endif

  Fopen(out, file_SD, (initialized || file_append)?"a":"w", "SaveSuperfluidDensity");
  for(i=0; i<SD.size-1; i++) {
    if(SD.N[i] == 0)
      Dreal = 0.;
    else
#ifdef TRIAL_1D
      Dreal = (DOUBLE) Nparticles * SD.CM2[i] / ((i+1)*2. * dt * SD.spacing*SD.N[i]);
#endif
#ifdef TRIAL_2D
      Dreal = (DOUBLE) Nparticles * SD.CM2[i] / ((i+1)*4. * dt * SD.spacing*SD.N[i]);
#endif
#ifdef TRIAL_3D
      Dreal = (DOUBLE) Nparticles * SD.CM2[i] / ((i+1)*6. * dt * SD.spacing*SD.N[i]);
#endif

    if(MC == CLASSICAL) {
      Dreal = (DOUBLE) Nparticles * SD.CM2[i] / ((i+1)*6. * dt1 * SD.spacing*SD.N[i]);
      fprintf(out, "%"LG" %.15"LE "\n", (i+1)*dt1*SD.spacing, Dreal); // 3/2 for single component at hght T
    }
    else if(MC == MOLECULAR_DYNAMICS) { // <r2>
      //Dreal = (DOUBLE) Nparticles * SD.CM2[i] / ((i+1)*6. * dt * SD.spacing*SD.N[i]);
      Dreal = (DOUBLE) Nparticles * SD.CM2[i] / (SD.N[i]);
      fprintf(out, "%"LG" %.15"LE "\n", (i+1)*dt*SD.spacing, Dreal); // 3/2 for single component at hght T
    }
    else { // DMC
      fprintf(out, "%"LG" %.15"LE "\n", (i+1)*dt*SD.spacing, Dreal);
    }
  }

  fclose(out);
  initialized = ON;
  return 0;
}

/***************************** Save Order Parameter ************************/
int SaveOrderParameter(void) {
  FILE *out;
  static int initialized = OFF;
  DOUBLE norm;

  Fopen(out, "outop.dat", (initialized || file_append)?"a":"w", "SaveOrderParameter");
  norm = Nmean*OrderParameter.times_measured;

  fprintf(out, "%.15"LE "\n", sqrt(OrderParameter.cos*OrderParameter.cos+OrderParameter.sin*OrderParameter.sin)/norm);

  OrderParameter.cos = 0.;
  OrderParameter.sin = 0.;
  OrderParameter.times_measured = 0;

  fclose(out);
  return 0;
}

/***************************** Save Lindeman Ratio *************************/
int SaveLindemannRatio(void) {
  FILE *out;
  static int initialized = OFF;

  Fopen(out, "outlind.dat", (initialized || file_append)?"a":"w", "SaveLindemannRatio");
  fprintf(out, "%.15"LE "\n", LindemannRatioF/LindemannRatioN/(L/Nlattice)/(L/Nlattice));

  LindemannRatioF = 0.;
  LindemannRatioN = 0;

  fclose(out);
  return 0;
}

/***************************** Save Variational parameter weights ***********/
int SaveVarParWeights(void) {
  FILE *out;
  static int initialized = OFF;
  DOUBLE mean = 0.;
  DOUBLE data[10];
  int number[10];
  int i,w;
  int search;

  Fopen(out, "outvarpr.dat", (initialized || file_append)?"a":"w", "SaveVarParWeights");
  for(w=0; w<Nwalkers; w++) mean += varpar[W[w].varparindex][VAR_PAR_NUMBER];
  mean /= Nwalkers;
  fprintf(out, "%.15"LE "\n", mean*VAR_PAR_UNIT);
  fclose(out);

  for(i=0; i<10; i++) { // do some statistics
    data[i] = 0.;
    number[i] = 0;
  }

  for(w=0; w<Nwalkers; w++) {
    search = ON;
    for(i=0; i<10 && search; i++) {
      if(varpar[W[w].varparindex][VAR_PAR_NUMBER] == data[i]) {
        data[i] = varpar[W[w].varparindex][VAR_PAR_NUMBER];
        number[i]++;
        search = OFF;
      }
      else if(data[i]==0) {
        data[i] = varpar[W[w].varparindex][VAR_PAR_NUMBER];
        number[i]++;
        search = OFF;
      }
    }
  }

  Message("  Branching chooses:");
  for(i=0; i<10; i++) {
    if(number[i]) Message("  %.2"LF" - %.0lf% %%", data[i]*VAR_PAR_UNIT, 100.*(DOUBLE)(number[i])/(DOUBLE)Nwalkers);
  }
  Message("\n");

  if(number[0] > 0.9 * Nwalkers|| number[1] > 0.9 * Nwalkers || number[2] > 0.9 * Nwalkers) {
    Message("  Changing variational parameters\n");
    varpar[0][VAR_PAR_NUMBER] = mean;
    for(i=0; i<Nwalkers/3; i++) W[i].varparindex = 0;
    varpar[1][VAR_PAR_NUMBER] = mean*1.2;
    for(i=Nwalkers/3; i<2*Nwalkers/3; i++) W[i].varparindex = 1;
    varpar[2][VAR_PAR_NUMBER] = mean*0.8;
    for(i=2*Nwalkers/3; i<Nwalkers; i++) W[i].varparindex = 2;
  }

  return 0;
}

/******************************** Save ********************************/
int SaveSpectralWeight(void) {
  FILE *out, *out2;
  static int first_time = ON;  // variable i tells how many times the energy was saved 
  int i,k;
  DOUBLE Phi_integrated;
  int Phi_measured;

  if(Phi.times_measured == 0) return 1;

  Fopen(out, "outG1tau.dat", (first_time == ON && file_append == OFF)?"w":"a", "spectral weight");
  Fopen(out2, "outG1.dat", (first_time == ON && file_append == OFF)?"w":"a", "spectral weight");

  for(i=1; i<gridPhi_t; i++) {
    Phi_integrated = 0.;
    Phi_measured = 0;
    for(k=0; k<gridPhi_x; k++) {
      fprintf(out, "%.15"LE " ", Phi.f[k][i] / (DOUBLE) (Phi.times_measured));
      Phi_integrated += Phi.f[k][i];
      Phi_measured++;
    }
    fprintf(out2, "%.15"LE " %.15"LE "\n", i*dt*(DOUBLE)Nmeasure, Phi_integrated/(DOUBLE)(Phi_measured));
    fprintf(out, "\n");
    
    for(k=0; k<gridPhi_x; k++) Phi.f[k][i] = 0.;
  }
  Phi.times_measured = 0;
  fclose(out);
  fclose(out2);

  if(first_time == ON) {
    Fopen(out, "outG1h.dat", "w", "spectral weight");
    fprintf(out, "%i %i %.15"LE " %.15"LE "\n", gridPhi_t-1, gridPhi_x, dt*(DOUBLE)Nmeasure, Phi.step);
    fclose(out);
    first_time = OFF;
  }

  first_time = OFF;

  return 0;
}

/*********************************** Save Data *******************************/
void SaveBlockData(int block) {
// save distributions
#ifdef MPI
  extern DOUBLE time_done_in_parallel;

  //time_done_in_parallel += MPI_Wtime();
  ParallelSaveBlockDataStart();
  if(MPI_myid == MASTER) {
#endif

  SaveCoordinates();

  if(MC == MOLECULAR_DYNAMICS) SaveVelocities();

  if(measure_OBDM || measure_Nk) SaveOBDM();
  if(measure_TBDM) SaveTBDM();
  if(measure_TBDM_MATRIX) SaveTBDMMatrix();
  if(measure_OBDM_MATRIX) SaveOBDMMatrix();
  if(measure_PairDistr) SavePairDistribution();
  if(measure_g3) SaveHyperRadius();

  if(MC == DIFFUSION &&  measure_PairDistr) SavePairDistributionPure();
  if(measure_RadDistr) SaveRadialDistribution();
  if(measure_Sk) {
    SaveStaticStructureFactor();
    //SaveStaticStructureFactorAngularPart();
  }
  if(measure_OP) SaveOrderParameter();
#ifdef TRIAL_1D
  if(measure_Nk) SaveMomentumDistribution1D();
#else // 2D, 3D
  if(measure_Nk) SaveMomentumDistribution();

#endif
  if(measure_Lind) SaveLindemannRatio();
  if(var_par_array) SaveVarParWeights();
  if(MC == DIFFUSION && measure_spectral_weight) SaveSpectralWeight();
  if(MC == DIFFUSION && measure_FormFactor) SaveFormFactor();
  if(MC == DIFFUSION && measure_SD) SaveSuperfluidDensity();
  if(MC == DIFFUSION) SaveEpotPure();
  if(MC == PIGS && SmartMC == PIGS_PSEUDOPOTENTIAL_OBDM) SaveOBDM_PathIntegral();
  if(measure_XY2) SaveXY2Matrix();

#ifdef MPI
  }
  ParallelSaveBlockDataEnd();
  EmptyDistributionArrays();
  //time_done_in_parallel -= MPI_Wtime();
#endif
}

/******************************** Save SKT ****************************/
int SaveFormFactor(void) {
  FILE *out1, *out2;
  DOUBLE norm;
  static int first_time = ON;  // variable i tells how many times the energy was saved 
  int i,k;

  Fopen(out1, "outsktau_k.dat", "w", "form factor");
  for(k=1; k<gridSKT_k; k++) { // damp all momenta (with degeneracy)
    fprintf(out1, "%.15"LE "\n", Sk.k[k]);
  }
  fclose(out1);

  Fopen(out1, "outsktau.dat", (first_time == ON && file_append == OFF)?"w":"a", "form factor");
  Fopen(out2, "outskmtau.dat", (first_time == ON && file_append == OFF)?"w":"a", "form factor");

//#ifdef TRIAL_1D
//  for(k=0; k<gridSKT_k; k++) fprintf(out2, "%"LF" %.15"LE "\n", (DOUBLE)(k+1)*SKT_dk*PI*n, SKT.f[0][k] / (DOUBLE) (SKT.times_measured*Nmean));
//#endif

  if(SKT.f[0][0]) // normalization Sk[0] = N
    norm = Ndens/SKT.f[0][0];
  else
    norm = 0;

  for(i=0; i<gridSKT_t; i++) {
    fprintf(out1, "%.15"LE " ", i*dt*(DOUBLE)Nmeasure);
    fprintf(out2, "%.15"LE " ", i*dt*(DOUBLE)Nmeasure);
#ifdef TRIAL_1D
    for(k=0; k<gridSKT_k; k++) {
      fprintf(out1, "%.15"LE " ", norm*(SKT.f[i][k] + 2.*SKT.f12[i][k]));
      fprintf(out2, "%.15"LE " ", norm*(SKT.f[i][k] - 2.*SKT.f12[i][k]));
    }
#else
    //fprintf(out1, "%"LF" ", (DOUBLE)(i+1)*SKT_dk*PI*n);
    //fprintf(out2, "%"LF" ", (DOUBLE)(i+1)*SKT_dk*PI*n);
    for(k=1; k<gridSKT_k; k++) { // skip 0 point
      //fprintf(out1, "%.15"LE " ", SKT.f[i][k]/(DOUBLE)(SKT.times_measured*Nmean));
      //fprintf(out2, "%.15"LE " ", SKT.f12[i][k]/(DOUBLE)(SKT.times_measured*Nmean));
      //fprintf(out3, "%"LF" %.15"LE "\n", k, Sk.N[i]?(normalization*Sk.f[i]/(2.*Sk.N[i])+normalization2*Sk.f12[i]/(Sk.N[i])):0);
      //fprintf(out4, "%"LF" %.15"LE "\n", k, Sk.N[i]?(normalization*Sk.f[i]/(2.*Sk.N[i])-normalization2*Sk.f12[i]/(Sk.N[i])):0);
      //norm = (SKT.N[i][k])?(1./(DOUBLE)SKT.N[i][k]):(0);

      //if(Sk.degeneracy[i] && k>0) {
        fprintf(out1, "%.15"LE " ", norm*(SKT.f[i][k] + 2.*SKT.f12[i][k]));
        fprintf(out2, "%.15"LE " ", norm*(SKT.f[i][k] - 2.*SKT.f12[i][k]));
      //}
    }
#endif
    //fprintf(out2, "%.15"LE " %.15"LE "\n", i*dt*(DOUBLE)Nmeasure, PhiTau.f[i][0]/(DOUBLE)(SKT.times_measured));
    fprintf(out1, "\n");
    fprintf(out2, "\n");

    for(k=0; k<gridSKT_k; k++) SKT.f[i][k] = 0.;
    for(k=0; k<gridSKT_k; k++) SKT.f12[i][k] = 0.;
    for(k=0; k<gridSKT_k; k++) SKT.N[i][k] = 0;
  }
  SKT.times_measured = 0;
  fclose(out1);
  fclose(out2);

  if(first_time == ON) {
    Fopen(out1, "outsktauh.dat", "w", "form factor");
#ifdef TRIAL_1D
    fprintf(out1, "%i %i %.15"LE " %.15"LE "\n", gridSKT_t, gridSKT_k, dt*(DOUBLE)Nmeasure, SKT_dk);
#else
    fprintf(out1, "%i %i %.15"LE " %.15"LE "\n", gridSKT_t, gridSKT_k-1, dt*(DOUBLE)Nmeasure, SKT_dk);
#endif
    fclose(out1);
    first_time = OFF;
  }

  first_time = OFF;

  return 0;
}

/******************************** Save Hyper Radius *******************/
int SaveHyperRadius(void) {
  int i;
  DOUBLE r;
  FILE *out;
  DOUBLE normalization;
  static int initialized = OFF;

  if(HR.times_measured == 0) {
    Warning("Attempt to save hyper radius without any measurement\n");
    return 1;
  }

  Fopen(out, "outhr.dat", (initialized || file_append)?"a":"w", "hyper radius");

  normalization = 1./(n*n/(2.*PI)*(DOUBLE)(HR.times_measured)*HR.step);

  for(i=0; i<HR.size; i++) {
    r = HR.min + (DOUBLE) (i+0.5) * HR.step;

    fprintf(out, "%"LF" %.15"LE "\n", r, normalization*HR.f[i]);
    HR.N[i] = 0;
    HR.f[i] = 0.;
  }
  HR.times_measured = 0;
  fclose(out);

  HR.times_measured = 0;
  initialized = ON;

  return 0;
}

/****************************** Save Pair Distribution Matrix *****************************/
int SavePairDistribtuionMatrix(void) {
  int i, j;
  DOUBLE x, y;
  FILE *out;
  static int initialized = OFF;
  DOUBLE normalization;

  Fopen(out, "outpdh.dat", "w", "Pair distribution matrix");
  fprintf(out, "%i\n", gridPD_MATRIX);
  fprintf(out, "%i\n", gridPD_MATRIX);
  fprintf(out, "%"LG"\n", PD_MATRIX11.step);
  fprintf(out, "%"LG"\n", PD_MATRIX11.step);
  fclose(out);

  Fopen(out, "outpdm11.dat", (initialized || file_append)?"a":"w", "Pair distribution matrix");
  normalization = 1./ ((DOUBLE)(PD_MATRIX11.times_measured)*PD_MATRIX11.step*PD_MATRIX11.step);
  for(i=0; i<gridPD_MATRIX; i++) {
    x = (DOUBLE) (i+0.5) * PD_MATRIX11.step;
    for(j=0; j<gridPD_MATRIX; j++) {
      y = (DOUBLE) (j+0.5) * PD_MATRIX11.step;
      if(PD_MATRIX11.N[i][j] > 0)
        fprintf(out, "%.2"LE " ", normalization*PD_MATRIX11.N[i][j]);
      else
        fprintf(out, "0 ");
      PD_MATRIX11.N[i][j] = 0;
    }
    fprintf(out, "\n");
  }
  PD_MATRIX11.times_measured = 0;
  fclose(out);

  Fopen(out, "outpdm12.dat", (initialized || file_append)?"a":"w", "Pair distribution matrix");
  normalization = 1./ ((DOUBLE)(PD_MATRIX12.times_measured)*PD_MATRIX12.step*PD_MATRIX12.step);
  for(i=0; i<gridPD_MATRIX; i++) {
    x = (DOUBLE) (i+0.5) * PD_MATRIX12.step;
    for(j=0; j<gridPD_MATRIX; j++) {
      y = (DOUBLE) (j+0.5) * PD_MATRIX12.step;
      if(PD_MATRIX12.N[i][j] > 0)
        fprintf(out, "%.2"LE " ", normalization*PD_MATRIX12.N[i][j]);
      else
        fprintf(out, "0 ");
      PD_MATRIX12.N[i][j] = 0;
    }
    fprintf(out, "\n");
  }
  PD_MATRIX12.times_measured = 0;
  fclose(out);

  Fopen(out, "outpdm22.dat", (initialized || file_append)?"a":"w", "Pair distribution matrix");
  normalization = 1./ ((DOUBLE)(PD_MATRIX22.times_measured)*PD_MATRIX22.step*PD_MATRIX22.step);
  for(i=0; i<gridPD_MATRIX; i++) {
    x = (DOUBLE) (i+0.5) * PD_MATRIX22.step;
    for(j=0; j<gridPD_MATRIX; j++) {
      y = (DOUBLE) (j+0.5) * PD_MATRIX22.step;
      if(PD_MATRIX22.N[i][j] > 0)
        fprintf(out, "%.2"LE " ", normalization*PD_MATRIX22.N[i][j]);
      else
        fprintf(out, "0 ");
      PD_MATRIX22.N[i][j] = 0;
    }
    fprintf(out, "\n");
  }
  PD_MATRIX22.times_measured = 0;
  fclose(out);

  if(MC) { // save pure estimator
    Fopen(out, "outpdm11p.dat", (initialized || file_append)?"a":"w", "Pair distribution matrix");
    normalization = 1./ ((DOUBLE)(PD_MATRIX11_pure.times_measured)*PD_MATRIX11.step*PD_MATRIX11.step);
    for(i=0; i<gridPD_MATRIX; i++) {
      x = (DOUBLE) (i+0.5) * PD_MATRIX11.step;
      for(j=0; j<gridPD_MATRIX; j++) {
        y = (DOUBLE) (j+0.5) * PD_MATRIX11.step;
        if(PD_MATRIX11.N[i][j] > 0)
          fprintf(out, "%.2"LE " ", normalization*PD_MATRIX11_pure.N[i][j]);
        else
          fprintf(out, "0 ");
        PD_MATRIX11_pure.N[i][j] = 0;
      }
      fprintf(out, "\n");
    }
    PD_MATRIX11_pure.times_measured = 0;
    fclose(out);

    Fopen(out, "outpdm12p.dat", (initialized || file_append)?"a":"w", "Pair distribution matrix");
    normalization = 1./ ((DOUBLE)(PD_MATRIX12_pure.times_measured)*PD_MATRIX12.step*PD_MATRIX12.step);
    for(i=0; i<gridPD_MATRIX; i++) {
      x = (DOUBLE) (i+0.5) * PD_MATRIX12.step;
      for(j=0; j<gridPD_MATRIX; j++) {
        y = (DOUBLE) (j+0.5) * PD_MATRIX12.step;
        if(PD_MATRIX11.N[i][j] > 0)
          fprintf(out, "%.2"LE " ", normalization*PD_MATRIX12_pure.N[i][j]);
        else
          fprintf(out, "0 ");
        PD_MATRIX12_pure.N[i][j] = 0;
      }
      fprintf(out, "\n");
    }
    PD_MATRIX12_pure.times_measured = 0;
    fclose(out);

    Fopen(out, "outpdm22p.dat", (initialized || file_append)?"a":"w", "Pair distribution matrix");
    normalization = 1./ ((DOUBLE)(PD_MATRIX22_pure.times_measured)*PD_MATRIX22.step*PD_MATRIX22.step);
    for(i=0; i<gridPD_MATRIX; i++) {
      x = (DOUBLE) (i+0.5) * PD_MATRIX22.step;
      for(j=0; j<gridPD_MATRIX; j++) {
        y = (DOUBLE) (j+0.5) * PD_MATRIX22.step;
        if(PD_MATRIX22.N[i][j] > 0)
          fprintf(out, "%.2"LE " ", normalization*PD_MATRIX22_pure.N[i][j]);
        else
          fprintf(out, "0 ");
        PD_MATRIX22_pure.N[i][j] = 0;
      }
      fprintf(out, "\n");
    }
    PD_MATRIX11_pure.times_measured = 0;
    fclose(out);
  }

  initialized = ON;

  return 0;
}

/*********************************** Save Vext *******************************/
int SaveVext(void) {
  FILE *out;
  int i, Nmax;
  int particle_index = 0;
  DOUBLE x,y,z;

  x=y=z=0;
  Lmax = L;

  Message("\n  Saving external potential ... ");
  Fopen(out, "Vext.dat", "w", "external potential");
  fprintf(out, "#z VextUp VextDn\n");
  Nmax = 100;
  for(i=0; i<Nmax; i++) {
    z = ((DOUBLE) i+1.)/(DOUBLE) (Nmax) * Lmax;
    //particle_index = i-(i/Nup)*Nup;
    fprintf(out,"%.8"LE" %.8"LE" %.8"LE"\n", z, VextUp(x,y,z,particle_index), VextDn(x,y,z,particle_index));
  }
  fclose(out);
  Message("done\n");

  return 0;
}

/****************************** Save Pure Coordinates ************************/
void CopyPureCoordinates(void) {
  int w, i;

  for(w=0; w<Nwalkers; w++) {
    for(i=0; i<Nup; i++) {
      CaseX(W[w].x_pure[i] = W[w].x[i]);
      CaseY(W[w].y_pure[i] = W[w].y[i]);
      CaseZ(W[w].z_pure[i] = W[w].z[i]);
    }
    for(i=0; i<Ndn; i++) {
      CaseX(W[w].xdn_pure[i] = W[w].xdn[i]);
      CaseY(W[w].ydn_pure[i] = W[w].ydn[i]);
      CaseZ(W[w].zdn_pure[i] = W[w].zdn[i]);
    }
  }
}

int SavePureCoordinates(void) {
  FILE *out;
  int w, i;
  static int first_time = ON;

  Fopen(out, "in3Dpure.dat", first_time?"w":"a", "coordinates");

  if(MC == DIFFUSION) { // save 1st walker
    w = 0;
    fprintf(out, "%"LG"\n", W[w].E);
    for(i=0; i<Nup; i++) fprintf(out, "%.15"LE " %.15"LE " %.15"LE "\n", W[w].x_pure[i], W[w].y_pure[i], W[w].z_pure[i]);
    for(i=0; i<Ndn; i++) fprintf(out, "%.15"LE " %.15"LE " %.15"LE "\n", W[w].xdn_pure[i], W[w].ydn_pure[i], W[w].zdn_pure[i]);
  }
  else  { // VARIATIONAL, save all walkers
    for(w=0; w<Nwalkers; w++) { // Save all walkers
      fprintf(out, "%"LG"\n", W[w].E);
      for(i=0; i<Nup; i++) fprintf(out, "%.15"LE " %.15"LE " %.15"LE "\n", W[w].x_pure[i], W[w].y_pure[i], W[w].z_pure[i]);
      for(i=0; i<Ndn; i++) fprintf(out, "%.15"LE " %.15"LE " %.15"LE "\n", W[w].xdn_pure[i], W[w].ydn_pure[i], W[w].zdn_pure[i]);
    }
  }
  fclose(out);

  CopyPureCoordinates();

  first_time = OFF;

  return 0;
}

/****************************** Save Epot Pure *******************************/
int SaveEpotPure(void) {
  int i;
  FILE *out;
  static int first_time = ON;
  DOUBLE normalization;

  Fopen(out, "outepot.dat", first_time?"w":"a", "pure Epot");
  normalization = energy_unit / ((DOUBLE)Epot_pure.times_measured * Nmean);

  for(i=0; i<Epot_pure.size; i++) fprintf(out, "%.15"LE " %.15"LE "\n", Epot_pure.x[i], Epot_pure.f[i] * normalization);

  fclose(out);

  for(i=0; i<Epot_pure.size; i++) Epot_pure.f[i] = 0.;
  Epot_pure.times_measured = 0;

  first_time = OFF;

  return 0;
}

/****************************** Save One Body Term ***************************/
int SaveOneBodyTerm(DOUBLE min, DOUBLE max, char *file_out, int N, int up) {
  int i;
  FILE *out;
  DOUBLE dx, x, y, z;
  DOUBLE Fx, Fy, Fz;
  DOUBLE testNaN;

  if(max < 1e-8) { // numerical zero
    max = Lhalf;
  }

  Fopen(out, file_out, "w", "Saving wavefunction");

  Message("Saving one-body term\n");

  if(max<min) {
    Warning("Save Wavefunction: 'min' argument is larger than the 'max' one\n");
    max = 10.*min;
  }

  dx = (max-min) / (N-1);
  fprintf(out, "#1)x 2)f 3)lnf 4)fp 5)-f''/f - 2/r f'/f 6)-f''/f - 2/r f'/f + (f'/f)^2 7)[-f''/f - 2/r f'/f]/(m_up/2) + V 8) V\n"); // Eloc = -f''/f - 2/r f'/f + (f'/f)^2

  x = y = 0.;
  for(i=0; i<N; i++) {
    z = min + dx * (DOUBLE) i;

   OneBodyFp(&Fx, &Fy, &Fz, x, y, z, 0, up);

    testNaN = Exp(OneBodyU(x, y, z, 0, up));
    if(testNaN == testNaN && testNaN != 2*testNaN) { // check for NaN by comparison to itself and INF
      fprintf(out, "%.15"LE " %.15"LE " %.15"LE " %.15"LE "\n", z, //1
        Exp(OneBodyU(x, y, z, 0, up)), //2
        Fz, //3
        OneBodyE(x, y, z, 0, up)); //4
    }
  }

  fclose(out);

  return 0;
}

/************************* Load Crystal Coordinates *************************/
int LoadCrystalCoordinates(void) {
#ifdef CRYSTAL
  FILE *in;
  int i;

  Message("  Loading crystal coordinates ...\n");

  in = fopen(INPATH "in3Dcryst.in", "r");
  if(in == NULL) {
    perror("\nError:");
    Error("\ncan't read %s", "in3Dcryst.in");
  }

/*#ifndef BC_ABSENT
  fscanf(in, "    Lx=%" LF " Ly=%" LF " Lz=%" LF "\n", &Lx, &Ly, &Lz);
  Warning("Box size is set from file to Lx=%lf, Ly=%lf, Lz=%lf, density n=%lf\n", Lx, Ly, Lz, (DOUBLE)N/(Lx*Ly*Lz));
  if(fabs((DOUBLE)N/(Lx*Ly*Lz)/n-1)>1e-3) Error("  wrong density!\n");
#endif*/

  //Crystal.size = NCrystal;
  Crystal.size = Nup;
  for(i=0; i<Nup; i++) {
#ifdef CRYSTAL_WIDTH_ARRAY
    fscanf(in, "%" LF " %" LF " %" LF " %" LF " %" LF " %" LF "\n", &Crystal.x[i], &Crystal.y[i], &Crystal.z[i], &Crystal.Rx[i], &Crystal.Ry[i], &Crystal.Rz[i]);
#else
    fscanf(in, "%" LF " %" LF " %" LF "\n", &Crystal.x[i], &Crystal.y[i], &Crystal.z[i]);
#endif
  }
  for(i=0; i<Ndn; i++) {
#ifdef CRYSTAL_WIDTH_ARRAY
    fscanf(in, "%" LF " %" LF " %" LF " %" LF " %" LF " %" LF "\n", &Crystal.xdn[i], &Crystal.ydn[i], &Crystal.zdn[i], &Crystal.Rxdn[i], &Crystal.Rydn[i], &Crystal.Rzdn[i]);
#else
    fscanf(in, "%" LF " %" LF " %" LF "\n", &Crystal.xdn[i], &Crystal.ydn[i], &Crystal.zdn[i]);
#endif
  }
  fclose(in);

#ifdef CRYSTAL_WIDTH_ARRAY
  for(i=0; i<Nup; i++) {
    Message("\n  Crystal [%i]:", i);
    Message(" x = %" LE , Crystal.x[i]);
    Message(" y = %" LE , Crystal.y[i]);
    Message(" z = %" LE , Crystal.z[i]);
    Message(" Rx = %" LE , Crystal.Rx[i]);
    Message(" Ry = %" LE , Crystal.Ry[i]);
    Message(" Rz = %" LE , Crystal.Rz[i]);
  }
  for(i=0; i<Ndn; i++) {
    Message("\n  Crystal [%i]:", i);
    Message(" x = %" LE , Crystal.xdn[i]);
    Message(" y = %" LE , Crystal.ydn[i]);
    Message(" z = %" LE , Crystal.zdn[i]);
    Message(" Rx = %" LE , Crystal.Rxdn[i]);
    Message(" Ry = %" LE , Crystal.Rydn[i]);
    Message(" Rz = %" LE , Crystal.Rzdn[i]);
  }
#endif

  Message("  done\n");
#endif

  return 0;
}


/****************************** Save XY Matrix *****************************/
int SaveXY2Matrix(void) {
  int i, j;
  DOUBLE x, y;
  FILE *out;
  static int first_time = ON;

  Fopen(out, "outxy2.dat", first_time?"w":"a", "OBDMmatrix");
  for(i=0; i<Nup; i++) {
    for(j=0; j<Nup; j++) {
      if(XY2_MATRIX.times_measured > 0)
        fprintf(out, "%.15"LE " ", XY2_MATRIX.f[i][j] / (DOUBLE) XY2_MATRIX.times_measured);
      else
        fprintf(out, "0 ");
      XY2_MATRIX.f[i][j] = 0.;
    }
    fprintf(out, "\n");
  }
  fclose(out);

  XY2_MATRIX.times_measured = 0;
  first_time = OFF;

  return 0;
}
