/*sse.c*/

#include <stdio.h>
#include <string.h>
#include <math.h>
#include "main.h"
#include "move.h"
#include "quantities.h"
#include "compatab.h"
#include "trial.h"
#include "fermions.h"
#include "trialf.h"
#include "sse.h"

#ifdef SSE_OPT

inline void PBC(float *x, float *y, float *z, float *r, int remain) {
  int i;
  for(i=0; i<remain; i++) {
    if(x[i] > Lhalf) x[i] -= L;
    else if(x[i]<-Lhalf) x[i] += L;

    if(y[i] > Lhalf) y[i] -= L;
    else if(y[i]<-Lhalf) y[i] += L;

    if(z[i] > Lhalf) z[i] -= L;
    else if(z[i]<-Lhalf) z[i] += L;

    r[i] = sqrt(x[i]*x[i]+y[i]*y[i]+z[i]*z[i]);

    x[i] /= r[i];
    y[i] /= r[i];
    z[i] /= r[i];
  }
}

inline void PBC_SSE(float *x, float *y, float *z, float *r, int N) {
  register int i;
  div_t q;

  q = div(N,4);

  __m128 vx, vy, vz;
  __m128 vL, vL2;
  __m128 vr, vr_inv;

  // get size of the box L
  vL = _mm_set1_ps(L);
  vL2  = _mm_set1_ps(Lhalf);

  for(i=0;i<q.quot*4;i+=4) {
    // load x[4], y[4], z[4]
    vx = _mm_load_ps(&x[i]);
    vy = _mm_load_ps(&y[i]);
    vz = _mm_load_ps(&z[i]);

    // adjust borders
    vx = _mm_add_ps(vx,_mm_and_ps(_mm_cmplt_ps(vx,-vL2),vL));
    vx = _mm_sub_ps(vx,_mm_and_ps(_mm_cmpgt_ps(vx,vL2),vL));
    vy = _mm_add_ps(vy,_mm_and_ps(_mm_cmplt_ps(vy,-vL2),vL));
    vy = _mm_sub_ps(vy,_mm_and_ps(_mm_cmpgt_ps(vy,vL2),vL));
    vz = _mm_add_ps(vz,_mm_and_ps(_mm_cmplt_ps(vz,-vL2),vL));
    vz = _mm_sub_ps(vz,_mm_and_ps(_mm_cmpgt_ps(vz,vL2),vL));

    // calculate r2
    vr = _mm_mul_ps(vx,vx);
    vr = _mm_add_ps(vr,_mm_mul_ps(vy,vy));
    vr = _mm_add_ps(vr,_mm_mul_ps(vz,vz));

    // calculate r^-1
    //vr_inv = _mm_rsqrt_ps(vr);
    vr_inv = _mm_div_ps(_mm_set1_ps(1.),_mm_sqrt_ps(vr));

    // r = r^2 * r^-1
    vr = _mm_mul_ps(vr,vr_inv);
    vx = _mm_mul_ps(vx,vr_inv);
    vy = _mm_mul_ps(vy,vr_inv);
    vz = _mm_mul_ps(vz,vr_inv);

    _mm_store_ps(&x[i],vx);
    _mm_store_ps(&y[i],vy);
    _mm_store_ps(&z[i],vz);
    _mm_store_ps(&r[i],vr);
  }

  if(q.rem) PBC(&x[4*q.quot],&y[4*q.quot],&z[4*q.quot],&r[4*q.quot],q.rem);
}

float WalkerEnergySSE(struct Walker *Walker, float *V, float *Ekin, float *EFF, float *Eext) {
  int i,j,k;
  float dr[3], r, r2;
  float Force, dF;
  float x,y,z;
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
  float dFx, dFy, dFz;
#endif
  float M0, M1[3], M2, M2p, e;

  if(var_par_array) AdjustVariationalParameter(Walker->varparindex);

  // Fill the Determinant Matrix
  for(i=0; i<N; i++) {
    x = Walker->x[i];
    y = Walker->y[i];
    z = Walker->z[i];
    for(j=0; j<N; j++) {
      dr[0] = x - Walker->xdn[j];
      dr[1] = y - Walker->ydn[j];
      dr[2] = z - Walker->zdn[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      Determinant[Walker->w][i][j] = orbitalF(sqrt(r2));
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
      Determinant[Walker->w][i][j] += orbitalDescreteBCS_F(dr[0], dr[1], dr[2]);
#endif
    }
  }
  // Invert the matrix
  // sum k A[i][k] Ainv[j][k] = delta(i,j)
  // sum k A[k][i] Ainv[k][j] = delta(i,j)
  LUinv(Determinant[Walker->w], DeterminantInv[Walker->w], N);
  Walker->Determinant = determinantLU;

  *V = *Ekin = *EFF = *Eext = 0.;
  // set Force array elements to zero 
  for(i=0; i<N; i++) for(k=0; k<3; k++) Dup[i][k] = Ddn[i][k] = 0.;
  for(i=0; i<N; i++) for(k=0; k<3; k++) Fup[i][k] = Fdn[i][k] = 0.;

#ifdef CRYSTAL_SYMM_SUM_PARTICLE
  for(j=0; j<Crystal.size; j++) {// fill Mo array
    MoUp[j] = MoDn[j] = 0.;
    for(i=0; i<N; i++) {
      dr[0] = Walker->x[i] - Crystal.x[j]; 
      dr[1] = Walker->y[i] - Crystal.y[j];
      dr[2] = Walker->z[i] - Crystal.z[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      //if(CheckInteractionCondition(dr[2], r2)) {
        MoUp[j] += Exp(-Crystal.R*r2);
      //}
      dr[0] = Walker->xdn[i] - Crystal.x[j];
      dr[1] = Walker->ydn[i] - Crystal.y[j];
      dr[2] = Walker->zdn[i] - Crystal.z[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      //if(CheckInteractionCondition(dr[2], r2)) {
        MoDn[j] += Exp(-Crystal.R*r2);
      //}
    }
  }
  for(j=0; j<Crystal.size; j++) { // calculate contribution of the crystal w.f. (second type)
    M2 = M2p = 0.; // spin up
    for(i=0; i<N; i++) { 
      dr[0] = Walker->x[i] - Crystal.x[j];
      dr[1] = Walker->y[i] - Crystal.y[j];
      dr[2] = Walker->z[i] - Crystal.z[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      //if(CheckInteractionCondition(dr[2], r2)) {
        e = Exp(-Crystal.R*r2);
        for(k=0; k<3; k++) Fup[i][k] += -2*Crystal.R*dr[k]*e/MoUp[j];
        M2 += r2 * e;
        M2p += r2 * e*e;
      //}
    }
    *Ekin -= 4*Crystal.R*Crystal.R *(M2 - M2p/MoUp[j])/MoUp[j];
    M2 = M2p = 0.; // spin down
    for(i=0; i<N; i++) { 
      dr[0] = Walker->xdn[i] - Crystal.x[j];
      dr[1] = Walker->ydn[i] - Crystal.y[j];
      dr[2] = Walker->zdn[i] - Crystal.z[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      //if(CheckInteractionCondition(dr[2], r2)) {
        e = Exp(-Crystal.R*r2);
        for(k=0; k<3; k++) Fdn[i][k] += -2*Crystal.R*dr[k]*e/MoDn[j];
        M2 += r2 * e;
        M2p += r2 * e*e;
       //}
    }
    *Ekin -= 4*Crystal.R*Crystal.R *(M2 - M2p/MoDn[j])/MoDn[j];
  }
#endif

  // Lattice
  for(i=0; i<N; i++) {
    x = Walker->x[i];
    y = Walker->y[i];
    z = Walker->z[i];
#ifdef EXTERNAL_POTENTIAL // add the external potential energy
    *Eext += Vext(Walker->x[i], Walker->y[i], Walker->z[i]);
    *Eext += Vext(Walker->xdn[i], Walker->ydn[i], Walker->zdn[i]);
#endif
#ifdef CRYSTAL // add contribution from the w.f. of crystal
    *Ekin += 12*Crystal.R; // 2[unit] 3[dimension] 2[spin], i.e. in units of h^2/2mR^2

#ifdef CRYSTAL_NONSYMMETRIC // nonsymmetric crystal
    dr[0] = Walker->x[i] - Crystal.x[i]; // spin up
    dr[1] = Walker->y[i] - Crystal.y[i];
    dr[2] = Walker->z[i] - Crystal.z[i];
    FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
    Fup[i][0] -= 2*Crystal.R * dr[0];
    Fup[i][1] -= 2*Crystal.R * dr[1];
    Fup[i][2] -= 2*Crystal.R * dr[2];

    dr[0] = Walker->xdn[i] - Crystal.x[i]; // spin up
    dr[1] = Walker->ydn[i] - Crystal.y[i];
    dr[2] = Walker->zdn[i] - Crystal.z[i];
    FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
    Fdn[i][0] -= 2*Crystal.R * dr[0];
    Fdn[i][1] -= 2*Crystal.R * dr[1];
    Fdn[i][2] -= 2*Crystal.R * dr[2];
#endif
#ifdef CRYSTAL_SYMM_SUM_LATTICE
    M0 = M1[0] = M1[1] = M1[2] = M2 = 0.;
    for(j=0; j<Crystal.size; j++) {
      dr[0] = Walker->x[i] - Crystal.x[j]; // spin up
      dr[1] = Walker->y[i] - Crystal.y[j];
      dr[2] = Walker->z[i] - Crystal.z[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(CheckInteractionCondition(dr[2], r2)) {
        e = Exp(-Crystal.R*r2);
        M0 += e;
        for(k=0; k<3; k++) M1[k] += dr[k]*e;
        M2 += r2*e;
      }
    }
    for(k=0; k<3; k++) M1[k] *= -2*Crystal.R / M0;
    for(k=0; k<3; k++) Fup[i][k] += M1[k];
    *Ekin -= 4*Crystal.R*Crystal.R *M2/M0 - M1[0]*M1[0]-M1[1]*M1[1]-M1[2]*M1[2];

    M0 = M1[0] = M1[1] = M1[2] = M2 = 0.;
    for(j=0; j<Crystal.size; j++) {
      dr[0] = Walker->xdn[i] - Crystal.x[j]; // spin up
      dr[1] = Walker->ydn[i] - Crystal.y[j];
      dr[2] = Walker->zdn[i] - Crystal.z[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(CheckInteractionCondition(dr[2], r2)) {
        e = Exp(-Crystal.R*r2);
        M0 += e;
        for(k=0; k<3; k++) M1[k] += dr[k]*e;
        M2 += r2*e;
      }
    }
    for(k=0; k<3; k++) M1[k] *= -2*Crystal.R/ M0;
    for(k=0; k<3; k++) Fdn[i][k] += M1[k];
    *Ekin -= 4*Crystal.R*Crystal.R *M2/M0 - M1[0]*M1[0]-M1[1]*M1[1]-M1[2]*M1[2];
#endif
#endif// end crystal
  }

  // Determinant contribution + Jastrow Mixed terms
  k = 0; // fill (diff12) arrays
  for(i=0; i<N; i++) {
    x = Walker->x[i];
    y = Walker->y[i];
    z = Walker->z[i];
    for(j=0; j<N; j++) { // particle-particle force
      xdiff12[w][k] = x - Walker->xdn[j];
      ydiff12[w][k] = y - Walker->ydn[j];
      zdiff12[w][k] = z - Walker->zdn[j];
      k++;
    }
  }

  // do optimized calculation
  PBC_SSE(xdiff, ydiff, zdiff, rdiff, k);

  // calculate Force
  k = 0;
  for(i=0; i<N; i++) {
    for(j=0; j<N; j++) {
      Force = 1;
      Fup[i][0] += Force*xdiff[k];
      Fup[i][1] += Force*ydiff[k];
      Fup[i][2] += Force*zdiff[k];
      Fdn[j][0] -= Force*xdiff[k];
      Fdn[j][1] -= Force*ydiff[k];
      Fdn[j][2] -= Force*zdiff[k];
      //printf("%i %i dr %"LE" %"LE" %"LE"\n", i, j, xdiff[k], ydiff[k], zdiff[k]);
      k++; //k = (i*(i-1))/2+j;
    }
  }

  k = 0;
  for(i=0; i<N; i++) {
    for(j=0; j<N; j++) { // particle-particle force
      if(rdiff12[k]<Lhalf) {
        r = rdiff12[k];
        *V += InteractionEnergy12(r);
        Force = DeterminantInv[Walker->w][i][j]*orbitalFp(r); // orbitalFp = f'
        Dup[i][0] += Force * xdiff12[k];
        Dup[i][1] += Force * ydiff12[k];
        Dup[i][2] += Force * zdiff12[k];
        Ddn[j][0] -= Force * xdiff12[k];
        Ddn[j][1] -= Force * ydiff12[k];
        Ddn[j][2] -= Force * zdiff12[k];
        *Ekin -= 2.*DeterminantInv[Walker->w][i][j] * orbitalEloc(r); // - 2 [+(f" +2/r f')]
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
        orbitalDescreteBCS_Fp(&dFx, &dFy, &dFz, dr[0], dr[1], dr[2]);
        Dup[i][0] += DeterminantInv[Walker->w][i][j] * dFx;
        Dup[i][1] += DeterminantInv[Walker->w][i][j] * dFy;
        Dup[i][2] += DeterminantInv[Walker->w][i][j] * dFz;
        Ddn[j][0] -= DeterminantInv[Walker->w][i][j] * dFx;
        Ddn[j][1] -= DeterminantInv[Walker->w][i][j] * dFy;
        Ddn[j][2] -= DeterminantInv[Walker->w][i][j] * dFz;
        *Ekin -= 2.*DeterminantInv[Walker->w][i][j] * orbitalDescreteBCS_Fpp(dr[0], dr[1], dr[2]);
#endif
#ifdef JASTROW_OPPOSITE_SPIN // Jastrow MIXED terms
        Force = InterpolateFp(&G, r)/r;
        Fup[i][0] += Force * xdiff12[k];
        Fup[i][1] += Force * ydiff12[k];
        Fup[i][2] += Force * zdiff12[k];
        Fdn[j][0] -= Force * xdiff12[k];
        Fdn[j][1] -= Force * ydiff12[k];
        Fdn[j][2] -= Force * zdiff12[k];
        *Ekin += 2.*InterpolateE(&G, r);
#endif
      }
      k++;
    }
  }

#ifdef JASTROW_SAME_SPIN // Jastrow contribution (same spin particle-particle force)
  for(i=0; i<Nup; i++) {
    for(j=i+1; j<Nup; j++) { 
      // spin up
      dr[0] = Walker->x[i] - Walker->x[j];
      dr[1] = Walker->y[i] - Walker->y[j];
      dr[2] = Walker->z[i] - Walker->z[j];

      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);

      if(CheckInteractionCondition(dr[2], r2)) {
        r = sqrt(r2);
       *V += InteractionEnergy11(r);
       *Ekin += 2*InterpolateE11(&G11, r); // [-(f"+2f'/r)/f] + (f'/f)^2
        Force = InterpolateFp11(&G11, r) / r; // Fp = f' / f
        for(k=0; k<3; k++) {
          dF = Force * dr[k];
          Fup[i][k] += dF;
          Fup[j][k] -= dF;
        }
      }

      // spin down
      dr[0] = Walker->xdn[i] - Walker->xdn[j];
      dr[1] = Walker->ydn[i] - Walker->ydn[j];
      dr[2] = Walker->zdn[i] - Walker->zdn[j];

      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);

      if(CheckInteractionCondition(dr[2], r2)) {
        r = sqrt(r2);
       *V += InteractionEnergy22(r);
       *Ekin += 2*InterpolateE22(&G11, r);
        Force = InterpolateFp22(&G11, r) / r;
        for(k=0; k<3; k++) {
          dF = Force * dr[k];
          Fdn[i][k] += dF;
          Fdn[j][k] -= dF;
        }
      }
    }
  }
#endif

  // calculate modulus squared [F + D]^2
  for(i=0; i<N; i++) {
    *Ekin +=  Dup[i][0]*Dup[i][0] + Ddn[i][0]*Ddn[i][0]
             +Dup[i][1]*Dup[i][1] + Ddn[i][1]*Ddn[i][1]
             +Dup[i][2]*Dup[i][2] + Ddn[i][2]*Ddn[i][2];
    *EFF +=   (Dup[i][0]+Fup[i][0])*(Dup[i][0]+Fup[i][0]) + (Ddn[i][0]+Fdn[i][0])*(Ddn[i][0]+Fdn[i][0])
            + (Dup[i][1]+Fup[i][1])*(Dup[i][1]+Fup[i][1]) + (Ddn[i][1]+Fdn[i][1])*(Ddn[i][1]+Fdn[i][1])
            + (Dup[i][2]+Fup[i][2])*(Dup[i][2]+Fup[i][2]) + (Ddn[i][2]+Fdn[i][2])*(Ddn[i][2]+Fdn[i][2]);
    if(MC == DIFFUSION && SmartMC != DMC_QUADRATIC) { // Linear DMC code optimization, remember forces
        W[Walker->w].F[i][0] = Fup[i][0] + Dup[i][0];
        W[Walker->w].F[i][1] = Fup[i][1] + Dup[i][1];
        W[Walker->w].F[i][2] = Fup[i][2] + Dup[i][2];
        W[Walker->w].F[i][3] = Fdn[i][0] + Ddn[i][0];
        W[Walker->w].F[i][4] = Fdn[i][1] + Ddn[i][1];
        W[Walker->w].F[i][5] = Fdn[i][2] + Ddn[i][2];
    }
  }

// in a homogeneous 3D system energy is measured in units of [h^2/2ma^2]
#ifdef RESCALE_ENERGY_3D_HS
  *V *= 2;
  *Eext *= 2;
#else
// otherwise in units of [h^2/mr^2] or [hw], add 1/2 to the kinetic term
  *Ekin *= 0.5;
  *EFF  *= 0.5;
#endif
  *Ekin -= *EFF;
  *EFF += *V + *Eext;

  return *V + *Ekin + *Eext;
}

#endif