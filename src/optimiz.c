/*optimiz.c*/

#include <math.h>
#include <stdio.h>
#include "quantities.h"
#include "main.h"
#include "utils.h"
#include "trialf.h"
#include "trial.h"
#include "move.h"
#include "randnorm.h"
#include "rw.h"
#include "compatab.h"
#include "crystal.h"
#include "fermions.h"
#include "spline.h"
#include "memory.h"
#include "optimiz.h"
#include "display.h"

DOUBLE opt_par[10][1]; // array with pointers to parameters to be optimized
DOUBLE opt_gradient[10];
int opt_N; // number of parameters
DOUBLE opt_U;

DOUBLE opt_Etarget = 0.3;
DOUBLE opt_sigma;

/******************************* Optimization *******************************/
void Optimization(void) {
  int i=0;

  Message("\n\nStarting automatic variational optimization of parameters\n");

  LoadCoordinatesOptimization();

  E = Energy(&Epot, &Ekin, &EFF, &Eext);
  E = (E+energy_shift)*energy_unit;
  Message("  Starting energy is %"LE" [Ef]\n", E);
  for(i=0; i<Niter*blck; i++) {
    Message("\n");
    Message("E= %"LG" parameters: %"LG" %"LG" %"LG" %"LG" %"LG, E, orbital_weight1, orbital_weight2, orbital_weight3, orbital_weight4, orbital_weight5);
    //opt_gradient[0] = OptimizationGradient(&orbital_weight1);
    //opt_gradient[1] = OptimizationGradient(&orbital_weight2);
    //opt_gradient[2] = OptimizationGradient(&orbital_weight3);
    OptimizationAnnealing(&orbital_weight1);
    OptimizationAnnealing(&orbital_weight2);
    OptimizationAnnealing(&orbital_weight3);
    if(video) Picture();
  }
}

/************************ Optimization Gradient *****************************/
DOUBLE OptimizationGradient(DOUBLE *par) {
  DOUBLE Ep;
  DOUBLE par_store;
  DOUBLE gradient;
  DOUBLE dx = 1e-2;

  par_store = *par;

  *par *= 1+dx;

  ConstructGridFermion();

  //Ep = Energy(&Epot, &Ekin, &EFF, &Eext);
  OptimizationEnergy(opt_Etarget, &Ep, &opt_sigma);
  Ep = (Ep+energy_shift)*energy_unit;

  *par = par_store;

  gradient = (Ep/E-1.)/(dx*(*par));

  //Message("E= %"LE" gradient %.3"LE", par %"LE" -> ", E, gradient, *par);
//   *par = *par - gradient*1e-3;
  if(gradient>0)
    *par /= 1.1;
  else
    *par *= 1.1;
  //Message("%"LE" ", *par);

  E = Ep;

  return gradient;
}

/************************ Optimization Gradient *****************************/
void OptimizationAnnealing(DOUBLE *par) {
  DOUBLE Ep, dE;
  DOUBLE sigmap, dsigma;
  DOUBLE par_store;
  DOUBLE xi;
  DOUBLE dx,dy,dz;
  static DOUBLE T = 0.1; // effective temperature
  static DOUBLE dt = 1;

  par_store = *par;

  RandomNormal3(&dx, &dy, &dz, 0., sqrt(dt));
  *par *= fabs(1+dx);

  ConstructGridFermion();

  OptimizationEnergy(opt_Etarget, &Ep, &sigmap);

  Ep = (Ep+energy_shift)*energy_unit;
  sigmap *= energy_unit;

  dE = Ep-E;
  dsigma = sigmap - opt_sigma;

  //if(dE <= 0.) {
  if(dsigma <= 0.) {
    accepted++;
    E = Ep;
    opt_sigma = sigmap;
  }
  else {
    xi = Random();
    //if(xi<Exp(-dE/T)) {
    if(xi<Exp(-dsigma/T)) {
      accepted++;
      E = Ep;
      opt_sigma = sigmap;
    }
    else {
      rejected++;
      *par = par_store;
    }
  }

  T *= 0.99;
}

/************************ Measure energy reweighting ************************/
void MeasureEnergyReweightingNumber(int reweighting) {
  DOUBLE var_par_old;
  DOUBLE E, Ep, Weight, weight, Etot;
  DOUBLE dE, dEpot, dEkin, dEff, dEext;
  DOUBLE DetOld;
  int w;
  FILE *out;
  DOUBLE factor = 1.001, dpar;
  int automatically_adjust = ON;
  DOUBLE strength;
  char text[10];
  //DOUBLE check;
  int update = OFF;

  static DOUBLE dE_dpar[10] = {0,0,0,0,0,0,0,0,0,0}, dE_dpar2[10] = {0,0,0,0,0,0,0,0,0,0}, sigma[10] = {0,0,0,0,0,0,0,0,0,0};
  static DOUBLE Eo[10] = {0,0,0,0,0,0,0,0,0,0}, Eo2[10] = {0,0,0,0,0,0,0,0,0,0}, sigmaEo[10] = {0,0,0,0,0,0,0,0,0,0};
  static int times_measured[10] = {0,0,0,0,0,0,0,0,0,0};
  static long int i[10] = {0,0,0,0,0,0,0,0,0,0};
  static int time_par[10] = {0,0,0,0,0,0,0,0,0,0};
  static int skipped[10] = {0,0,0,0,0,0,0,0,0,0};

  /*for(w=0; w<Nwalkers; w++) {
    check = U(W[w]);
    if(W[w].U != check) Warning(" Weight not updated!\n");
    W[w].U = check;
  }*/

  if(reweighting == 1) { // Rpar
    if(i[reweighting]==0) Message("  Reweighting parameter is Rpar = %.3f -> Rpar/%"LF" = %.3f\n", Rpar/Lhalf, factor,Rpar*factor/Lhalf);
    var_par_old = Rpar;
    Rpar *= factor; // modify the var. parameter
    dpar = Rpar - var_par_old;
    ConstructGridBCS();
  }
  else if(reweighting == 2) {
    if(i[reweighting]==0) Message("  Reweighting parameter is Gamma = %.3f -> %"LF" Gamma = %.3f\n", Gamma, factor, Gamma*factor);
    var_par_old = Gamma;
    Gamma *= factor; // modify the var. parameter
    dpar = Gamma - var_par_old;
  }
  else if(reweighting == 3) {
    if(i[reweighting]==0) Message("  Reweighting parameter is beta = %.3f -> %"LF" beta = %.3f\n", beta, factor, beta*factor);
    var_par_old = beta;
    beta *= factor; // modify the var. parameter
    dpar = beta - var_par_old;
  }
  else if(reweighting == 4) {
    Apar /= Cpar; // do it each time before Construct Grid
    if(i[reweighting]==0) Message("  Reweighting parameter is Apar = %.3f -> Apar/%"LF" = %.3f\n", Apar, factor, Apar*factor);
    var_par_old = Apar;
    Apar *= factor; // modify the var. parameter
    dpar = Apar - var_par_old;
    ConstructGridSoftSphereSquareWell(&G11);
  }
  else if(reweighting == 5) {
    if(i[reweighting]==0) Message("  Reweighting parameter is Cpar = %.3f -> Cpar/%"LF" = %.3f\n", Cpar, factor, Cpar*factor);
    var_par_old = Cpar;
    Cpar *= factor; // modify the var. parameter
    dpar = Cpar - var_par_old;
    Apar /= Cpar; // do it each time before Construct Grid
    ConstructGridSoftSphereSquareWell(&G11);
  }
  else if(reweighting == 6) {
    if(i[reweighting]==0) Message("  Reweighting parameter is Gamma2 = %.3f -> %"LF" Gamma2 = %.3f\n", Gamma2, factor, Gamma2*factor);
    var_par_old = Gamma2;
    Gamma2 *= factor; // modify the var. parameter
    dpar = Gamma2 - var_par_old;
  }
  else if(reweighting == 7) {
    if(i[reweighting]==0) Message("  Reweighting parameter is beta2 = %.3f -> %"LF" beta2 = %.3f\n", beta2, factor, beta2*factor);
    var_par_old = beta2;
    beta2 *= factor; // modify the var. parameter
    dpar = beta2 - var_par_old;
  }

  if(i[reweighting]==0) {
    SaveWaveFunction11(&G11, 1e-6, 1.2*Lhalf, "wf11rew.dat");
    SaveWaveFunction12(&G12, 1e-6, 1.2*Lhalf, "wf12rew.dat");
    SaveBCSWaveFunction(1e-6, 1.2*Lhalf, "wfBCSrew.dat");
  }

  dE = Weight = 0.;
  Etot = 0;
  for(w=0; w<Nwalkers; w++) {
    // Accumulate energy difference
    E = W[w].Epot + W[w].Ekin + W[w].Eext;

    DetOld = W[w].Determinant;

    Ep = WalkerEnergy(&W[w], &dEpot, &dEkin, &dEff, &dEext); // new energy

    weight = (W[w].Determinant/DetOld)*(W[w].Determinant/DetOld) * Exp(2*(U(W[w])-W[w].U));

    W[w].Determinant = DetOld;

    dE += Ep * weight;
    Etot += E;
    Weight += weight;
  }

  dE /= (DOUBLE) (Nmean * Weight);
  Etot /= (DOUBLE) (Nmean * Nwalkers);
  dE -= Etot; // difference

  i[reweighting]++;
  sprintf(text, "outerw%i.dat",reweighting);
  out = fopen(text, i[reweighting]?"a":"w");
  if(out == NULL) {
    perror("\nError:");
    Warning("can't write to energy file %s\n", "outerw.dat");
    return;
  }
  fprintf(out, "%.15"LE"\n", dE*energy_unit/dpar);
  fclose(out);

  dE /= dpar;
  //dE /= Etot;
  if(fabs(dE)*energy_unit < 1000) {
    dE_dpar[reweighting] +=  dE;
    dE_dpar2[reweighting] += dE*dE;
    Eo[reweighting] += Etot;
    Eo2[reweighting] += Etot*Etot;
    times_measured[reweighting]++;
  }
  else {
    skipped[reweighting]++;
    //times_measured[reweighting]++;
  }

  if(automatically_adjust== ON && times_measured[reweighting] == Niter/Nmeasure) { // end of the block, adjust var. parameter
    dE_dpar[reweighting] *= energy_unit/(DOUBLE) times_measured[reweighting];
    dE_dpar2[reweighting] *= energy_unit*energy_unit/(DOUBLE) times_measured[reweighting];
    sigma[reweighting] = sqrt(dE_dpar2[reweighting]-dE_dpar[reweighting]*dE_dpar[reweighting]);
    sigma[reweighting] /= sqrt(times_measured[reweighting]);
    Eo[reweighting] *= energy_unit/(DOUBLE) times_measured[reweighting];
    Eo2[reweighting] *= energy_unit*energy_unit/(DOUBLE) times_measured[reweighting];
    sigmaEo[reweighting] = sqrt(Eo2[reweighting]-Eo[reweighting]*Eo[reweighting]);
    sigmaEo[reweighting] /= sqrt(times_measured[reweighting]);

    Message("  Par.%i, <E>=%.3"LG", dE/dx=%"LG"+/-%"LG", %"LG, reweighting, Eo[reweighting], dE_dpar[reweighting], sigma[reweighting], var_par_old);
    //if(sigma/dE_dpar < 0.5)  var_par_old -= dE_dpar / 5.;

    strength = fabs(dE_dpar[reweighting]);
    /*if(strength<0.5 && strength>0.001) 
      strength = 1+fabs(dE_dpar);
    else*/
      strength = 1.1;

    if(dE_dpar[reweighting]<0) var_par_old *= strength;
    if(dE_dpar[reweighting]>0) var_par_old /= strength;
    Message("->%"LG, var_par_old);
    if(skipped[reweighting]) Message(" skip %.f%%", (DOUBLE) skipped[reweighting] /(DOUBLE) (Niter/Nmeasure)*100);
    Message("\n");

    update = ON;

    sprintf(text, "outpar%i.dat", reweighting);
    out = fopen(text, time_par[reweighting]++?"a":"w");
    fprintf(out, "%i %"LG" %"LG" %"LG" %"LG" %"LG"\n", time_par[reweighting], var_par_old, dE_dpar[reweighting], sigma[reweighting], Eo[reweighting], sigmaEo[reweighting]);
    fclose(out);

    dE_dpar[reweighting] = dE_dpar2[reweighting] = Eo[reweighting] = sigma[reweighting] = sigmaEo[reweighting] = 0.;
    times_measured[reweighting] = skipped[reweighting] = 0;
  }

  // restore the wave function
  if(reweighting == 1) { // Rpar
    Rpar = var_par_old; // modify the var. parameter
    ConstructGridBCS();
  }
  else if(reweighting == 2) {
    Gamma = var_par_old;
  }
  else if(reweighting == 3) {
    beta = var_par_old;
  }
  else if(reweighting == 4) {
    Apar = var_par_old;
    ConstructGridSoftSphereSquareWell(&G11);
  }
  else if(reweighting == 5) {
    Cpar = var_par_old;
    Apar /= Cpar; // do it each time before Construct Grid
    ConstructGridSoftSphereSquareWell(&G11);
  }
  else if(reweighting == 6) {
    Gamma2 = var_par_old;
  }
  else if(reweighting == 7) {
    beta2 = var_par_old;
  }

  if(update) {
    for(w=0; w<Nwalkers; w++) W[w].U = U(W[w]);
    SaveWaveFunction11(&G11, 1e-6, 1.2*Lhalf, "wf11rew.dat");
    SaveWaveFunction12(&G12, 1e-6, 1.2*Lhalf, "wf12rew.dat");
    SaveBCSWaveFunction(1e-6, 1.2*Lhalf, "wfBCSrew.dat");
  }
}

void MeasureEnergyReweighting(void) {
  MeasureEnergyReweightingNumber(reweighting);
  // 1:Rpar,2:gamma,3:beta,4:Apar,5:Cpar,6:gamma2,7:beta2
  //MeasureEnergyReweightingNumber(1);
  //MeasureEnergyReweightingNumber(2);
  //MeasureEnergyReweightingNumber(4);
  //MeasureEnergyReweightingNumber(5);
  //MeasureEnergyReweightingNumber(6);
  //MeasureEnergyReweightingNumber(7);
}

/****************************** Save Coordinates ************************/
int SaveCoordinatesOptimization(void) {
  FILE *out;
  int w, i;
  static int walkers_saved = 0;

  // save coordinates without PBC
  Fopen(out, "3Dreal.dat", walkers_saved?"a":"w", "Save coordinates real position (no pbc)");
  for(w=0; w<Nwalkers; w++) {
    for(i=0; i<Nup; i++) fprintf(out, "%.7"LF" %.7"LF" %.7"LF"\n", W[w].rreal[i][0], W[w].rreal[i][1], W[w].rreal[i][2]);
    for(i=0; i<Ndn; i++) fprintf(out, "%.7"LF" %.7"LF" %.7"LF"\n", W[w].rreal[i][3], W[w].rreal[i][4], W[w].rreal[i][5]);
  }
  fclose(out);

  // save coordinates in the box
  Fopen(out, "3Dprev.dat", walkers_saved?"a":"w", "Save coordinates optimization");
  //fprintf(out, "%i\n", Nwalkers);
  walkers_saved += Nwalkers;
  for(w=0; w<Nwalkers; w++) {
    fprintf(out, "%.15"LE"\n", WalkerEnergy0(&W[w]));
    for(i=0; i<Nup; i++) fprintf(out, "%.7"LF" %.7"LF" %.7"LF"\n", W[w].x[i], W[w].y[i], W[w].z[i]);
    for(i=0; i<Ndn; i++) fprintf(out, "%.7"LF" %.7"LF" %.7"LF"\n", W[w].xdn[i], W[w].ydn[i], W[w].zdn[i]);
  }
  fclose(out);

  Fopen(out, "3DprevNw.dat", "w", "Save coordinates optimization");
  fprintf(out, "%i\n", walkers_saved);
  fclose(out);

  return 0;
}

/****************************** Load Coordinates ************************/
int LoadCoordinatesOptimization(void) {
  FILE *in;
  int w, i;

  Message("  Loading stored coordinates ...\n");

  Fopen(in, "3DprevNw.dat", "r", "Load coordinates optimization");
  fscanf(in, "%i\n", &Nwalkers);
  fclose(in);

  NwalkersMax = Nwalkers;

  Message("  Allocating memory for %i walkers ... ", Nwalkers);
  AllocateWalkers();
  Message("done\n  Loading walkers ...");

  Fopen(in, "3Dprev.dat", "r", "Load coordinates optimization");
  for(w=0; w<Nwalkers; w++) {
    fscanf(in,"%"LF, &W[w].E);
    for(i=0; i<Nup; i++) fscanf(in,"%"LF" %"LF" %"LF"\n", &W[w].x[i], &W[w].y[i], &W[w].z[i]);
    for(i=0; i<Ndn; i++) fscanf(in,"%"LF" %"LF" %"LF"\n", &W[w].xdn[i], &W[w].ydn[i], &W[w].zdn[i]);
    W[w].w = 0;
  }
  fclose(in);

  Message("  done\n");

  return 0;
}

/********************************* Energy ************************************/
// returns energy of the system averaged over all walkers
// and variance, relative to Etarget
void OptimizationEnergy(DOUBLE Etarget, DOUBLE *Emean, DOUBLE *sigma) {
  DOUBLE dEpot, dEkin, dEff, dEext;
  DOUBLE weight, Weight = 0;
  DOUBLE Etot = 0.;
  DOUBLE DetOld;
  DOUBLE E, Ep;
  int w;

  *Emean = 0.; // energy
  *sigma = 0.; // variance

  for(w=0; w<Nwalkers; w++) {
    E = W[w].Epot + W[w].Ekin + W[w].Eext; // old energy
    DetOld = W[w].Determinant;

    Ep = WalkerEnergy(&W[w], &dEpot, &dEkin, &dEff, &dEext); // new energy

    //weight = 1.;
    weight = (W[w].Determinant/DetOld)*(W[w].Determinant/DetOld) * Exp(2*(U(W[w])-W[w].U));

    W[w].Determinant = DetOld;

    Etot += E; // mean "old" energy
    *Emean += Ep * weight; // mean "new" energy
    *sigma += (Ep-Etarget)*(Ep-Etarget);
    Weight += weight;
  }

  Etot /= (DOUBLE) (Nmean * Nwalkers);
  *Emean /= (DOUBLE) (Nmean * Weight);
  // -= Etot; // difference
  *sigma /= (DOUBLE) (Nmean * Nwalkers);
  *sigma = sqrt(*sigma);
}