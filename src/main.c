/*main.c*/
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "main.h"
#include "randnorm.h"
#include "memory.h"
#include "utils.h"
#include "rw.h"
#include "move.h"
#include "dmc.h"
#include "quantities.h"
#include "gencoord.h"
#include "display.h"
#include "crystal.h"
#include "fermions.h"
#include "trialf.h"
#include "spline.h"
#include "trial.h"
#include "fewbody.h"
#include "compatab.h"
#include "md.h"
#include "pigs.h"
#ifdef MPI
#  include "mpi.h"
#  include "parallel.h"
#endif
#ifdef _OPENMP
#  include <omp.h>
#  include "parallel.h"
DOUBLE time_start_omp;
#endif
#ifdef USE_UTF8
#  include <wchar.h>
#  include <locale.h>
#endif

int grid_trial = 1000;
int gridOBDM = 601;
int gridTBDM = 601;
int gridOBDM_MATRIX = 60;
int gridTBDM_MATRIX = 60;
int gridPD_MATRIX = 10;
int gridRD = 601;
int gridPD = 601;
int gridg3 = 100;
int grid_pure_block = 100;
int gridSk = 10;
int gridNk = 10;
int gridSKT_k = 10;
int gridSKT_t = 10;

long int Niter=100, Nmeasure=10;
int Niter_store = 100;
DOUBLE Rpar=0.5, Apar=1, Bpar=20, Cpar=1, Dpar=1, Epar = 1, Kpar=1, Mpar=0.5, Opar=1, beta=0.5, beta2, Gamma = 1, Gamma2, Rpar2=0.7, Rpar11=0.5, Rpar12=0.5, Rpar22=0.5, Apar12, Bpar11=0.1, Bpar12=0.1, Bpar22=0.1, Kpar11=1, Kpar12=1;
DOUBLE c=1, c2=1, c3=1, c4=1, c5=1, c6=1, c7=1, c8=1, c9=1, c10=1, c11=1, c12=1, c13=1, a1=1, a3=1, a4=1, a5=1, a6=1, a7=1, a8=1, b1=1, b3=1, b4=1, b5=1, b6=1, b7=1, b8=1;
DOUBLE alpha_Rx, alpha_Ry, alpha_Rz;
DOUBLE alpha_x_up, two_alpha_x_up;
DOUBLE alpha_y_up, two_alpha_y_up;
DOUBLE alpha_z_up, two_alpha_z_up;
DOUBLE alpha_x_dn, two_alpha_x_dn;
DOUBLE alpha_y_dn, two_alpha_y_dn;
DOUBLE alpha_z_dn, two_alpha_z_dn;
DOUBLE omega_x_up, omega_y_up, omega_z_up;
DOUBLE omega_x2_up, omega_y2_up, omega_z2_up;
DOUBLE omega_x_dn, omega_y_dn, omega_z_dn;
DOUBLE omega_x2_dn, omega_y2_dn, omega_z2_dn;

DOUBLE n = 0.01, dt = 1e-3, dt_all = 1e-3, dt_one = 1e-2, dt1 = 1, dt2 = 1;
DOUBLE acceptance_rate = 0.;
int Nwalkers=100, NwalkersMax=100, Nimp=0;
int Nup = 7, Ndn = 7, Ntot = 14, Nmax;
DOUBLE Ndens = -1, Nmean = 7;
DOUBLE Nwalkersw=100., Eint;
int None=1, Nall=1;
int blck=10, blck_heating=0;
int verbosity=0;
DOUBLE E, EFF, Eo, Epot, Ekin, Eext, Eck;
DOUBLE L=-1, Lhalf, L2, Lhalf2, Lmax, Lm1, L_inv, Lcutoff_pot, Lcutoff_pot2, Lwf, Lhalfwf;
DOUBLE Lx, Ly, Lz, L_half_x, L_half_y, L_half_z, L_inv_x, L_inv_y, L_inv_z;
DOUBLE a=1, a2, b=1, ap=1, aA, aB;
DOUBLE lambda=1, lambda2, lambda4, lambda12;
DOUBLE two_alpha, two_lambda_beta, lambda_beta;
DOUBLE Srecoil = 0; // lattice depth in recoil energies 
int Nlattice, Nlattice3, NCrystal = -1; // size of the lattice
DOUBLE mass_ratio=1.;
DOUBLE m_up_inv, m_dn_inv, m_up_inv_sqrt, m_dn_inv_sqrt, m_up=1, m_dn=1, m_mu=0.5;
DOUBLE bilayer_width= 0, bilayer_width2= 0;
DOUBLE lambda1=1,lambda2=1; // CSM parameters for mixture
DOUBLE excitonRadius=1; // Bohr radius for Coulomb interaction
DOUBLE PD_MATRIXmax = 1.;

char file_particles[25] = "in3Dprev.in";
char file_energy[25] = "oute.dat";
char file_OBDM[25] = "outdr.dat";
char file_OBDM_MATRIX[25] = "outobdm.dat";
char file_PD[25] = "outpd.dat";
char file_PD_pure[25] = "outpdp.dat";
char file_PDz[25] = "outpdz.dat";
char file_RD[25] = "outrd.dat";
char file_RDz[25] = "outrdz.dat";
char file_R2[25] = "outr2.dat";
char file_z2[25] = "outz2.dat";
char file_R2_pure[25] = "outr2pur.dat";
char file_z2_pure[25] = "outz2pur.dat";
char file_wf[25] = "inwf.in";
char file_Sk[25] = "outsk.dat";
char file_Sk_pure[25] = "outskpur.dat";
char file_SD[25] = "outsd.dat";

int measure_energy = ON;
int measure_OBDM = OFF;
int measure_Nk = OFF;
int measure_TBDM = OFF;
int measure_OBDM_MATRIX = OFF;
int measure_TBDM_MATRIX = OFF;
int measure_PairDistrMATRIX = OFF;
int measure_SD = OFF;
int measure_RadDistr = ON;
int measure_PairDistr = ON;
int measure_g3 = OFF;
int measure_Sk = OFF;
int measure_Sk_pure = OFF;
int measure_R2 = ON;
int measure_OP = OFF;
int measure_FormFactor = OFF;
int reweighting = OFF;
int measure_Lind = OFF;
int measure_pure_coordinates = OFF;
int measure_effective_up_dn_potential = OFF;
int optimization = OFF;
int check_kinetic_energy = ON;
int measure_XY2 = OFF;

int video = OFF;
int branchng_present = ON;

struct Walker *W, *Wp;
struct Grid G11, G12, G22, Gorbital, GSYM;

struct sOBDM OBDM, OBDMtrap, OBDM22;
struct sTBDM TBDM12, TBDM11, TBDM22;
#ifdef OBDM_BOSONS
struct sOBDM OBDMbosons, OBDMbosons22;
struct sTBDM TBDMbosons12, TBDMbosons11, TBDMbosons22;
#endif
struct sOBDM OBDMpath_integral;
struct sSk Sk, Sk_pure;
struct sMATRIX OBDM_MATRIX, TBDM_MATRIX, PD_MATRIX11, PD_MATRIX12, PD_MATRIX22, Phi, SKT, PD_MATRIX11_pure, PD_MATRIX12_pure, PD_MATRIX22_pure, XY2_MATRIX;
struct sNk Nk;

struct Distribution PDz, PD_pure, PDup_pure, PDdn_pure;
struct Distribution PD, PDup, PDdn;
struct Distribution RD; // contains .size; .times_measured
struct Distribution RDxup, RDyup, RDzup, RDup, RDup_pure;
struct Distribution RDxdn, RDydn, RDzdn, RDdn, RDdn_pure;
struct Distribution HR;

struct sPureEstimator Epot_pure;
struct sSD SD;
struct sOrderParameter OrderParameter;

DOUBLE *u_mi, *u_ij;
DOUBLE *mu_k, *mu_p_k, *mu_pp_k;
DOUBLE *MoUp, *MoDn;
DOUBLE **varpar; // set of variational parameters
DOUBLE LindemannRatioF;

int LindemannRatioN;
int var_par_array = OFF; // array of var. parameters is used

DOUBLE Etail;
DOUBLE energy_shift = 0.;

int accepted = 0;
int rejected = 0;
int accepted_one = 0;
int rejected_one = 0;
int accepted_all = 0;
int rejected_all = 0;
int overlaped = 0;
int tried = 0;
int Nmeasured = 0;
int McMillan_points = 1;
int McMillanTBDM_points = 100;
int Npop = 50, Npop_max, Npop_min;
int generate_new_coordinates = OFF;
int generate_crystal_coordinates = ON;
DOUBLE Evar = 0,  Evar2 = 0.;
DOUBLE EFFvar=0., EFFvar2 = 0.;
DOUBLE Epotvar=0., Epotvar2 = 0.;
DOUBLE Ekinvar=0., Ekinvar2 = 0.;
DOUBLE kF=1.;
DOUBLE xR2 = 0., zR2 = 0.;
DOUBLE reduce = 0.9, amplify = 1.1;
DOUBLE Ro = 1., RoSS, RoSW;
DOUBLE Ro11 = 1., Ro12 = 1., Ro22 = 1.;

clock_t time_start = 0;
clock_t time_simulating = 0;
clock_t time_measuring = 0;
clock_t time_communicating = 0;
time_t time_era_begin, time_era_end;

int MC = ON;
int SmartMC = 0;
int *walkers_storage;        /* contains indices to the alive walkers */
int *dead_walkers_storage;   /* contains indices to empty walkers */
int Ndead_walkers = 0;
int iteration_global, block; // number of iterations done
DOUBLE imag_time_done = 0.; // imaginary time done 
DOUBLE SKT_dk;

DOUBLE orbital_weight1 = 1; // weights of Slater orbitals
DOUBLE orbital_weight2 = 1;
DOUBLE orbital_weight3 = 1;
DOUBLE orbital_weight4 = 0;
DOUBLE orbital_weight5 = 0;

int file_append = OFF; // append to data files 
int file_particles_append = OFF; // append coordinates to file

int measure_spectral_weight = OFF;
int gridPhi_t = 10; // Spectral weight time resolution
int gridPhi_x = 10; // Spectral weight space resolution
int one_body_momentum_i = 0;
DOUBLE one_body_momentum_k;
DOUBLE omega = 1;
DOUBLE rGauss_up = 0.; // var. par for double Gaussian one-body w.f.
DOUBLE rGauss_dn = 0.; // var. par for double Gaussian one-body w.f.
DOUBLE T=1;
DOUBLE alpha_latt=1., beta_latt=1., kL=-1.;
DOUBLE Epotcutoff = 1e3;

int sum_over_images_cut_off_num = 2; // needed when INTERACTION_SUM_OVER_IMAGES is defined

int main(int argn, char **argv) {
  DOUBLE sigma, error;
  DOUBLE Eblck = 0;
  DOUBLE Emessage;
  int i, w;
  long int measured_blck = 0;
  clock_t time_begin;
#ifdef FERMIONS
  FILE *out;
#endif

#ifdef USE_UTF8
  setlocale(LC_ALL, "en_US.UTF-8");
#endif

  time_start = clock();
  time(&time_era_begin);

  Message("%s", SRC_CODE_DATE);
#ifdef MPI
  ParallelInitializeMPI(argn, argv);
#endif
#ifdef _OPENMP
  time_start_omp = omp_get_wtime();
  ParallelInitializeOpenMP(argn, argv);
#endif
#ifdef PARALLEL_CODE // OpenMP and/or MPI
  ParallelRandInit();
#else
  RandInit();
#endif

#ifdef SECURE
  Warning("Secure/Debug option enabled\n");
#endif
#ifdef _DEBUG
  Warning("  Code was compiled with DEBUG option and might be not efficient.\n\n"); 
#ifdef __INTEL_COMPILER
  Message("  Intel compiler is used.\n");
#endif
#ifdef _MSC_VER
  Message("  Microsoft visual studio compiler is used.\n");
#endif
#endif
#ifdef DATATYPE_FLOAT
  Message("  Goal precision: float (8 digits after comma)\n");
#endif
#ifdef DATATYPE_DOUBLE
  Message("  Goal precision: double (16 digits after comma)\n");
#endif
#ifdef DATATYPE_LONG_DOUBLE
  Message("  Goal precision: long double (32 digits after comma)\n");
#endif

#ifdef MEMORY_CONTIGUOUS
  Message("  \nMemory allocation: contiguous arrays (i.e. optimized)\n");
#else
  Message("  \nMemory allocation: scattered arrays (i.e. non optimized)\n");
#endif
  CheckMemcpy();
#ifdef USE_UTF8
  Message("  \nUTF characters are allowed: α β γ\n");
#endif

  CheckMantissa();

  Message("  particles move in ");
  CaseX(Message("X ");)
  CaseY(Message("Y ");)
  CaseZ(Message("Z ");)
  Message("directions\n");

#ifdef CALCULATE_DERIVATIVES_NUMERICALLY
  Message("  Derivatives will be calculated numerically (kinetic energy, drift force)\n");
#endif
#ifdef CALCULATE_DERIVATIVES_ANALYTICALLY
  Message("  Derivatives will be calculated analytically (kinetic energy, drift force)\n");
#endif

  LoadParameters();

#ifdef FERMIONS
  out = fopen("k.dat", "w");
#ifdef TRIAL_3D
  kF = pow(3.*PI*PI*n, 1./3.);
#else
  kF = sqrt(2.*PI*n);
#endif
  fprintf(out, "%.15"LE"\n", -1./(kF*a));
  fclose(out);
#endif

  if(energy_unit != 1.) Warning(" unit of energy is different from 1 and equals %"LG"\n", energy_unit);

  if(boundary == NO_BOUNDARY_CONDITIONS) {
    if(L<0) L = 10;
  }
  else if(boundary == ONE_BOUNDARY_CONDITION) { // n is total density (spin up + spin down)
    L = (DOUBLE) (2.*Ndens) / n;
    Message(" box size is fixed by Ndens\n");
    Message("  L/2 = %"LE"\n", 0.5*L);
  }
  else if(boundary == THREE_BOUNDARY_CONDITIONS) { // n is total density (spin up + spin down)
#ifdef BC_3DPBC_TRUNCATED_OCTAHEDRON
    L = pow((DOUBLE) (4*Ndens) / n, (1./3.));
#else // cube
    L = pow((DOUBLE) (2*Ndens) / n, (1./3.));
#endif
  }
  else if(boundary == TWO_BOUNDARY_CONDITIONS) { // n is total density (spin up + spin down)
#ifdef BC_2DPBC_HEXAGON
    Ly = 2.*sqrt((DOUBLE) (2*Ndens) / n *2./(3.*sqrt(3.)));
    Lx = sqrt(3.)/2.*Ly;
    Message("  Box size: Lx= %lf, Ly= %lf\n", Lx, Ly);
    L = Lx;
#else // square
    L = sqrt((DOUBLE) (2.*Ndens) / n);
#endif
  }
  else if(boundary == BILAYER_BOUNDARY_CONDITIONS) {
    L = pow((DOUBLE) (Ndens)/n, (1./2.));
  }

#ifdef TRAP_LINEAR_CHAIN_OF_HARMONIC_OSCILLATORS_UP
  Message("  TRAP_LINEAR_CHAIN_OF_HARMONIC_OSCILLATORS_UP\n");
  Lz = (DOUBLE) Nup / n;
  Message(" box size is fixed by Nup, Lz = %"LF"\n", Lz);
  L = Lz;
  Lx = L;
#endif

  a2 = a*a;
  Eck = 1e10;

#ifdef CENTER_OF_MASS_IS_NOT_MOVED
  Message("  Center of mass is not moved\n");
#endif

#ifdef INTERACTION_ABSENT
  Message("  Interaction potential (not included in Eloc part): ABSENT\n");
#endif
#ifdef INTERACTION11_COULOMB
  Message("  Interaction potential 11, 22 (not included in Eloc part): COULOMB (set bilayer_width for up + down interaction)\n");
#endif
#ifdef INTERACTION12_COULOMB_REPULSION
  Message("  Interaction potential 12 (not included in Eloc part): COULOMB repulsion\n");
#endif
#ifdef INTERACTION12_COULOMB_ATTRACTION
  Message("  Interaction potential 12 (not included in Eloc part): COULOMB attraction\n");
#endif
#ifdef INTERACTION_DIPOLE
  Message("  Interaction potential (not included in Eloc part): DIPOLE\n");
#endif
#ifdef INTERACTION_LENNARD_JONES
  Message("  Interaction potential (not included in Eloc part): LENNARD JONES\n");
#endif
#ifdef INTERACTION11_HAMILTONIAN_MEAN_FIELD
  Message("  Interaction potential: HMF Dpar*cos(PI*r/L)\n");
#endif

  Message("  Summation of interaction potential over all images: ");
#ifdef INTERACTION_SUM_OVER_IMAGES
  Message("direct summation\n");
  Message("  Cut-off distance %lf (%i boxes)\n", L*(0.5+(int)sum_over_images_cut_off_num), sum_over_images_cut_off_num);
#endif
#ifdef INTERACTION_EWALD
  Message("Ewald summation\n");
#endif
#ifdef INTERACTION_WITHOUT_IMAGES
  Message("disabled\n");
#endif

#ifdef RESCALE_ENERGY_3D_HS
  Message("  Unit of energy: h^2/2ma^2\n");
#else
  Message("  Unit of energy: h^2/ma^2\n");
#endif

  Lhalf = 0.5*L;
  Lhalf2 = Lhalf*Lhalf;
  Lwf = L;
  Lhalfwf = 0.5*Lwf;
  L2 = L*L;
  Lm1 = 1. / L;
  L_inv = 1. / L;
#ifdef BOX_EQUAL_SIDES
  Lx = Ly = Lz = L;
#endif
  Message("\n  L=%.15"LE"\n", L);
  Message("  L/2=%.15"LE"\n", Lhalf);

#ifdef L2_CHECK
  Message("L/2 check is enabled\n");
#else
  Message("L/2 check is disabled\n");
#endif

  L_half_x = 0.5*Lx;
  L_half_y = 0.5*Ly;
  L_half_z = 0.5*Lz;
  L_inv_x = 1./Lx;
  L_inv_y = 1./Ly;
  L_inv_z = 1./Lz;

#ifdef BC_3DPBC_TRUNCATED_OCTAHEDRON
  Lcutoff_pot = L*sqrt(3.)/4.;
#else
#ifdef BC_2DPBC_HEXAGON
  Lcutoff_pot = Lx/2.;
#else // standard case
  Lcutoff_pot = L/2.;
#endif
#endif
  Message("  Potential energy, corr functions, etc. are calculated up to the cut-off distance Rc= %lf = %lf L/2\n", Lcutoff_pot, Lcutoff_pot/Lhalf);
  Lcutoff_pot2 = Lcutoff_pot*Lcutoff_pot;

  // allocate memory for distribution arrays
  AllocateGrids();

#ifdef BC_2DPBC
  if(measure_Sk) LoadParametersSk();
#endif
#ifdef BC_3DPBC
  if(measure_Sk) LoadParametersSk();
#endif

#ifdef INTERACTION12_SQUARE_WELL
  ConstructSqWellDepth();
#endif

#ifdef INTERACTION11_SQUARE_WELL
  ConstructSqWellDepthPwave();
#endif

#ifdef INTERACTION12_SOFT_SPHERE
  trial_Vo = ConstructSoftSphereHeight(Ro12, a, m_mu);
#endif

#ifdef INTERACTION11_SOFT_SPHERE
  trial11_Vo = ConstructSoftSphereHeight(Ro11, aA, 0.5*m_up);
#endif

#ifdef INTERACTION22_SOFT_SPHERE
  trial22_Vo = ConstructSoftSphereHeight(Ro22, aB, 0.5*m_dn);
#endif

  Message("\nConstructing trial wave function ...\n  ");
  Message("\nDeterminant orbital term ...\n  ");

#ifdef INTERPOLATE_LINEAR_ORBITAL
  LoadOrbital();
#endif
#ifdef TRIAL_TWO_BODY_SCATTERING_NUMERICAL_ORBITAL
  ConstructGridSpline2Body_ij(&Gorbital, &InteractionEnergy12, 0, 1);
#endif
#ifdef ORBITAL_EXTERNAL_WF
  LoadTrialWaveFunction(&Gorbital, "inorbital.in");
#endif
#ifdef INTERPOLATE_SPLINE_ORBITAL
  Message("  Spline interpolation will be used for Orbital terms\n");
  SplineConstruct(&Gorbital, 2e30, 0e30);
  SaveWaveFunctionSpline(&Gorbital, "wforbspline.dat");
#endif
#ifdef ORBITAL_SQ_WELL_BOUND_STATE
  ConstructGridBCS();
#endif
#ifdef ORBITAL_DESCRETE_COULOMB
  ConstructGridBCS();
#endif
#ifdef ORBITAL_SQ_WELL_BOUND_STATE_DECAY
  ConstructGridBCSdecay();
#endif
#ifdef ORBITAL_EXPONENT_ALPHA
  ConstructGridExponentAlpha();
#endif
#ifdef ORBITAL_SQ_WELL_FREE_STATE
  ConstructGridBCSSquareWell();
#endif
#ifdef ORBITAL_SQ_WELL_FREE_STATE_BCS_SIDE
  ConstructGridBCSSquareWellBCSSide();
#endif
#ifdef ORBITAL_SQ_WELL_ZERO_ENERGY
  ConstructGridBCSSquareWellZeroEnergy();
#endif
#ifdef ORBITAL_SQ_WELL_ZERO_CONST
  ConstructGridSquareWellZeroConst();
#endif
#ifdef ORBITAL_SQ_WELL_MIXTURE
  ConstructGridBCS();
  ConstructGridBCSSquareWell();
#endif
#ifdef ORBITAL_PSEUDOPOTENTIAL
  ConstructGridBCSPseudopotential();
#endif
#ifdef ORBITAL_PSEUDOPOTENTIAL_MIXTURE
  ConstructGridBCSPseudopotential();
#endif
#ifdef ORBITAL_IG
  ConstructGridBCSIdealGas();
#endif
#ifdef ORBITAL_DECAY
  ConstructGridDecay();
#endif
#ifdef ORBITAL_CH2_EXP
  ConstructGridCh2Exp();
#endif
#ifdef ORBITAL_CH2_PHONON
  ConstructGridCh2PhononOrbital();
#endif
#ifdef ORBITAL_SQUARE_WELL2D
  ConstructGridSquareWell2D();
#endif
#ifdef ORBITAL_SQUARE_WELL2D_PHONON
  ConstructGridSquareWell2DPhonon();
#endif
#ifdef ORBITAL_SQUARE_WELL2D_SIN
  ConstructGridSquareWell2DSinus();
#endif

// Jastrow function
  Message("\nJastrow term ...\n");
#ifdef JASTROW_DIMENSIONALITY_FROM_DPAR
    Message("  Dpar = %lf will be used as an effective dimensionality for constructing Jastrow term\n", Dpar);
#endif
#ifdef TRIAL_TWO_BODY_SCATTERING_NUMERICAL11
    ConstructGridSpline2Body_ij(&G11, &InteractionEnergy11, 0, 0);
#endif
#ifdef TRIAL_TWO_BODY_SCATTERING_NUMERICAL12
    ConstructGridSpline2Body_ij(&G12, &InteractionEnergy12, 0, 1);
#endif
#ifdef TRIAL_TWO_BODY_SCATTERING_NUMERICAL22
    ConstructGridSpline2Body_ij(&G22, &InteractionEnergy22, 1, 1);
#endif
#ifdef TRIAL_TWO_BODY_SCATTERING_NUMERICAL12_SYM
    ConstructGridSpline2Body_ij(&GSYM, &InteractionEnergy12, 0, 1);
#endif
#ifdef TRIAL_HS12
  ConstructGridHS12(&G12);
#endif
#ifdef TRIAL_HS11
  ConstructGridHS11(&G11);
#endif
#ifdef TRIAL_HS_SIMPLE
  ConstructGridHSSimple(&G12);
#endif
#ifdef TRIAL_HS_SIMPLE11
  ConstructGridHSSimple11(&G11);
#endif
#ifdef TRIAL_PSEUDOPOT_SIMPLE
  ConstructGridHSSimple(&G11); // coef are the same as for HS
#endif
#ifdef TRIAL_ZERO_RANGE_UNITARY
  ConstructGridZeroRangeUnitaryPhonons(&G12);
#endif
#ifdef TRIAL_SS
  ConstructGridSS(&G11, a, Apar, Rpar, Bpar, grid_trial, 0, L);
#endif
#ifdef TRIAL_SS_EXP12
  ConstructGridSSExp12(&G12);
#endif
#ifdef TRIAL_SS_EXP11
  ConstructGridSSExp11(&G11);
#endif
#ifdef TRIAL_SS_EXP22
  ConstructGridSSExp22(&G22);
#endif
#ifdef TRIAL_SS_PHONONS11
  ConstructGridSoftSpherePhonons11(&G11, Ro11);
#endif
#ifdef TRIAL_SS_PHONONS12
  ConstructGridSoftSpherePhonons12(&G12, Ro12);
#endif
#ifdef TRIAL_SS_PHONONS22
  ConstructGridSoftSpherePhonons22(&G22, Ro22);
#endif
#ifdef TRIAL_LIM
  ConstructGridLim(&G11, 0, L, grid_trial);
#endif
#ifdef TRIAL_POWER
  ConstructGridPower(&G11, 0, L, grid_trial);
#endif
#ifdef TRIAL_HS1D
  ConstructGridHS1D(&G11, Rpar, Bpar, a,  grid_trial, 0, L);
#endif
#ifdef TRIAL_TONKS11
  ConstructGridTonks11(&G11, grid_trial, 0, L);
#endif
#ifdef TRIAL_TONKS12
  ConstructGridTonks12(&G12, grid_trial, 0, L);
#endif
#ifdef TRIAL_TONKS22
  ConstructGridTonks22(&G22, grid_trial, 0, L);
#endif
#ifdef TRIAL_LIEB
  ConstructGridLieb(&G11, Rpar, 0, grid_trial, 0, L);
#endif
#ifdef TRIAL_LIEB_PHONON11
  Warning(" Rpar11 [L/2] -> Rpar11 = %lf\n", Rpar11*=Lhalf);
  ConstructGridLiebPhonons11(&G11, aA, Rpar11);
#endif
#ifdef TRIAL_PHONON_LUTTINGER11
  ConstructGridPhononLuttinger11(&G11);
#endif
#ifdef TRIAL_PHONON_LUTTINGER12
  ConstructGridPhononLuttinger12(&G12);
#endif
#ifdef TRIAL_PHONON_LUTTINGER_AUTO11
  ConstructGridPhononLuttingerAuto11(&G11);
#endif
#ifdef TRIAL_PHONON_LUTTINGER_AUTO12
  ConstructGridPhononLuttingerAuto12(&G12);
#endif
#ifdef TRIAL_LIEB_PHONON12
  Warning(" Rpar12 [L/2] -> Rpar12 = %lf\n", Rpar12*=Lhalf);
  ConstructGridLiebPhonons12(&G11, a, Rpar12);
#endif
#ifdef TRIAL_SUTHERLAND12
  ConstructGridSutherland(&G12);
#endif
#ifdef TRIAL_SUTHERLAND11
  ConstructGridSutherland(&G11);
#endif
#ifdef TRIAL_SUTHERLAND22
  ConstructGridSutherland(&G22);
#endif
#ifdef TRIAL_SUTHERLAND_CHANGING_LAMBDA1122
  Message("  Input parameter Apar Bpar define lambda = 1 + Apar r + Bar r^2 from the CSM\n");
  ConstructGridSutherland(&G11);
#endif
#ifdef TRIAL_DIPOLE
  ConstructGridDipole(&G11, Apar, Bpar, n);
#endif
#ifdef TRIAL_SCHIFF_VERLET // with or without PHONONS
    ConstructGridSchiffVerlet(&G11, Apar, Bpar, Rpar, n);
    ConstructGridSchiffVerlet(&G22, Apar, Bpar, Rpar, n);
#endif
#ifdef TRIAL_SCHIFF_VERLET12 // with or without PHONONS
    ConstructGridSchiffVerlet(&G12, Apar12, Bpar12, Rpar12, n);
#endif
#ifdef TRIAL_DIPOLE_Ko11
  ConstructGridDipoleKo11(&G11);
#endif
#ifdef TRIAL_DIPOLE_Ko22
  ConstructGridDipoleKo22(&G22);
#endif
#ifdef TRIAL_DIPOLES_PHONON11
  ConstructGridDipolePhonon(&G11);
#endif
#ifdef TRIAL_DIPOLE_GAUSS12
  ConstructGridDipoleGauss(&G12);
#endif
#ifdef TRIAL_COULOMB_1D_PHONON
  ConstructGridCoulomb1DPhonon(&G11);
#endif
#ifdef TRIAL_COULOMB_3D_PHONON11
  ConstructGridCoulombPhonon11(&G11);
#endif
#ifdef TRIAL_COULOMB_3D_PHONON12
  ConstructGridCoulombPhonon12(&G12);
#endif
#ifdef TRIAL_COULOMB12exPHONON
  ConstructGridCoulomb1DPhononEx(&G11);
#endif
#ifdef TRIAL_ABSENT
ConstructAbsent(&G11);
#endif

#ifdef TRIAL_SQUARE_WELL_FS
  ConstructGridSquareWellFreeState(&G12);
#endif

#ifdef TRIAL11_SQUARE_WELL_FS_GAUSS
  ConstructGridSquareWellFreeStateGauss11(&G11);
#endif

#ifdef TRIAL11_SQUARE_WELL_FS_P_WAVE
  ConstructGridSquareWellFreeStatePWave11(&G11);
#endif

#ifdef TRIAL_SQUARE_WELL_FS_PHONONS12
  ConstructGridSquareWellFreeStatePhonons12(&G12);
#endif

#ifdef TRIAL_SQUARE_WELL_FS_GPE_POLARON12
  ConstructGridSquareWellFreeStateGPEPolaron12(&G12);
#endif

#ifdef TRIAL_SQUARE_WELL_FS_PHONONS11
  ConstructGridSquareWellFreeStatePhonons11(&G11);
#endif

#ifdef TRIAL_SQUARE_WELL_BS
  ConstructGridSquareWellBoundState(&G11);
#endif

#ifdef TRIAL_SQUARE_WELL_BS_PHONONS
  //FindSWBindingEnergy();
  ConstructGridSquareWellBoundStatePhonons(&G12);
#endif

#ifdef TRIAL11_SQUARE_WELL_BS_PHONONS
  ConstructGridSquareWellBoundStatePhonons11(&G11);
#endif

#ifdef TRIAL_SQUARE_WELL_SMOOTH
  ConstructGridSquareWellSmooth(&G12);
#endif

#ifdef TRIAL_SH
  ConstructGridSh(&G11);
#endif

#ifdef TRIAL_PSEUDOPOT_FS
  Rpar = Lhalf;
  ConstructGridPseudopotentialFreeState(&G11, a, Rpar);
#endif

#ifdef TRIAL_PSEUDOPOT_BS
  ConstructGridPseudopotentialBoundState(&G11);
#endif

#ifdef TRIAL_SS_SW
  ConstructGridSoftSphereSquareWell(&G11);
#endif
#ifdef TRIAL_SQ_WELL_ZERO_CONST
  ConstructGridSquareWellZeroConst();
#endif
#ifdef TRIAL11_SQ_WELL_ZERO_CONST
  ConstructGridSquareWellZeroConst11();
#endif
#ifdef TRIAL_SQ_WELL_UNITARITY
  ConstructGridSquareWellUnitarity();
#endif
#ifdef TRIAL_SQ_WELL_TRAP
  ConstructGridSquareWellTrap();
#endif
#ifdef TRIAL_SQ_WELL_BS_TRAP
  ConstructGridSquareWellBSTrap();
#endif
#ifdef TRIAL_SQ_WELL_BS_LATTICE_POLARON
 ConstructGridSquareWellBSLatticePolaron();
#endif
#ifdef TRIAL_SQUARE_WELL2D
  ConstructGridSquareWell2D();
#endif
#ifdef TRIAL_SQUARE_WELL2D_ZERO_ENERGY_PHONONS12
  ConstructGridSquareWell2DZeroEnergyPhonons12(&G12);
#endif
#ifdef TRIAL_ZERO_RANGE_LOG_PHONONS12
  ConstructGridZeroRangeLogPhonons12(&G12);
#endif
#ifdef TRIAL_SOFT_DISKS_2D_ZERO_ENERGY_PHONONS11
  ConstructGridSoftDisks2DZeroEnergyPhonons11(&G11);
#endif
#ifdef TRIAL_SOFT_DISKS_2D_ZERO_ENERGY_PHONONS12
  ConstructGridSoftDisks2DZeroEnergyPhonons12(&G12);
#endif
#ifdef TRIAL_SOFT_DISKS_2D_ZERO_ENERGY_PHONONS22
  ConstructGridSoftDisks2DZeroEnergyPhonons22(&G22);
#endif
#ifdef TRIAL_SQUARE_WELL2D_K0_PHONONS12
  ConstructGridSquareWell2D_K0_Phonons12(&G12);
#endif
#ifdef TRIAL_SQUARE_WELL2D_J0_PHONONS12
  ConstructGridSquareWell2D_J0_Phonons12(&G12);
#endif
#ifdef TRIAL_ZERO_RANGE_K0_PHONONS12
  ConstructGridZeroRange_K0_Phonons12(&G12);
#endif
#ifdef TRIAL_ZERO_RANGE_K0_EXP12
  ConstructGridZeroRange_K0_Exp12(&G12);
#endif
#ifdef TRIAL_ZERO_RANGE_K0_GPE_POLARON
  ConstructGridZeroRange_K0_GPE_Polaron(&G12);
#endif
#ifdef TRIAL_ZERO_RANGE_LOG_GPE_POLARON
  ConstructGridZeroRange_Log_GPE_Polaron(&G12);
#endif
#ifdef TRIAL_ZERO_RANGE_UNITARY_GPE_POLARON
  ConstructGridZeroRange_Unitary_GPE_Polaron(&G12);
#endif
#ifdef TRIAL_CH2_PHONON11
  ConstructGridCh2Phonon11();
#endif
#ifdef TRIAL_CH2_PHONON12
  ConstructGridCh2Phonon12();
#endif
#ifdef TRIAL_MCGUIRE_PBC12
  ConstructGridMcGuirePBC();
#endif
#ifdef TRIAL_MCGUIRE_PBC12_RPAR
  ConstructGridMcGuirePBCRpar();
#endif
#ifdef TRIAL_MCGUIRE_PHONON12
  ConstructGridMcGuirePhonon12();
#endif
#ifdef TRIAL_MCGUIRE_PHONON11
  ConstructGridMcGuirePhonon11();
#endif
#ifdef TRIAL_MCGUIRE12_GAUSSIAN1D
  ConstructGridMcGuireGaussian1D();
#endif
#ifdef TRIAL_MCGUIRE12_GAUSSIAN3D
  ConstructGridMcGuireGaussian3D();
#endif
#ifdef TRIAL_MCGUIRE12_FERMI
  ConstructGridMcGuireFermi();
#endif
#ifdef TRIAL_HS2D_FINITE_ENERGY11
  ConstructGridHS2DFiniteEnergy11(&G11);
#endif
#ifdef TRIAL_HS2D_ZERO_ENERGY_PHONONS11
  ConstructGridHS2DZeroEnergyPhonons11(&G11);
#endif
#ifdef TRIAL_DIPOLE_Ko_TRAP1122
  ConstructGridDipoleKoTrap(&G11);
  ConstructGridDipoleKoTrap(&G22);
#endif
#ifdef TRIAL_SCHIFF_VERLET11_TRAP
  ConstructGridSchiffVerlett11_Trap(&G11);
#endif
#ifdef TRIAL_DARK_SOLITON12
  ConstructGridDarkSoltion12(&G12);
#endif

#ifdef TRIAL_SYM_GAUSS
  ConstructGridSYMGAUSS(&GSYM);
#endif
#ifdef TRIAL_SYM_FERMI
  ConstructGridSYMFERMI(&GSYM);
#endif
#ifdef TRIAL_SYM_YAROSLAV
  ConstructGridSYMYAROSLAV(&GSYM);
#endif
#ifdef TRIAL_SUM_EXPONENTIALS
  ConstructGridSUMEXP(&GSYM);
#endif
// normal gas
#ifdef FERMIONS
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
  Message("  Using anisotropic relative orbitals (plane waves or similar)\n");
  ConstructGridFermion();
#else
  if(Ndn<Nup) ConstructGridFermion();
#endif
#endif

#ifdef CRYSTAL
#ifdef CRYSTAL_NONSYMMETRIC
  Message("  Solid phase simulation: non symmetrized trial wave function.\n");
#else
  Message("  Solid phase simulation: symmetrized trial wave function.\n");
#endif
  ConstructGridCrystal(&Crystal, Nup);

  if(generate_crystal_coordinates == OFF) LoadCrystalCoordinates();
#endif

#ifdef ONE_BODY2_TRIAL_1D_WAVE
  Message("  One-body term for second component: |cos(one_body_momentum_i pi z/L)|, where one_body_momentum_i = 0,1,2,...\n");
  one_body_momentum_k = PI/L*(DOUBLE) one_body_momentum_i;
  Message("  Momentum %"LE" corresponding to excitation of level %i\n", one_body_momentum_k, one_body_momentum_i);
#endif

#ifdef ONE_BODY_TRIAL_1D_WAVE_2COMP_OUT_OF_PHASE // 1 + Srecoil cos(one_body_momentum_i 2 pi z/L + 0.5*pi * up)  where up = {0;1}
  Message("  One-body term for (up) |1 + Srecoil cos(one_body_momentum_i pi z/L)|, where one_body_momentum_i = 0,1,2,...\n");
  Message("  One-body term for (dn) |1 + Srecoil sin(one_body_momentum_i pi z/L)|, where one_body_momentum_i = 0,1,2,...\n");
  one_body_momentum_k = 2.*PI/L*(DOUBLE) one_body_momentum_i;
  Message("  Momentum %"LE" corresponding to excitation of level %i\n", one_body_momentum_k, one_body_momentum_i);
#endif

#ifdef ONE_BODY_TRIAL_1D_WAVE_2COMP_OUT_OF_PHASE_KPAR // 1 + Srecoil cos^Kpar(one_body_momentum_i 2 pi z/L + 0.5*pi * up)  where up = {0;1}
  Message("  One-body term for (up) |1 + Srecoil^Kpar cos(one_body_momentum_i pi z/L)|, where one_body_momentum_i = 0,1,2,...\n");
  Message("  One-body term for (dn) |1 + Srecoil^Kpar sin(one_body_momentum_i pi z/L)|, where one_body_momentum_i = 0,1,2,...\n");
  one_body_momentum_k = 2.*PI/L*(DOUBLE) one_body_momentum_i;
  Message("  Momentum %"LE" corresponding to excitation of level %i\n", one_body_momentum_k, one_body_momentum_i);
#endif

#ifdef ONE_BODY_TRIAL_1D_WAVE_2COMP_INPHASE // 1 + Srecoil cos(one_body_momentum_i 2 pi z/L + 0.5*pi * up)  where up = {0;1}
  Message("  One-body term for (up,dn) |1 + Srecoil cos(one_body_momentum_i pi z/L)|, where one_body_momentum_i = 0,1,2,...\n");
  one_body_momentum_k = 2.*PI/L*(DOUBLE) one_body_momentum_i;
  Message("  Momentum %"LE" corresponding to excitation of level %i\n", one_body_momentum_k, one_body_momentum_i);
#endif

#ifdef ONE_BODY_TRIAL_3D_WAVE_2COMP_OUT_OF_PHASE // 1 + Srecoil cos(one_body_momentum_i 2 pi z/L + 0.5*pi * up)  where up = {0;1}
  Message("  One-body term for (up) |1 + Srecoil cos(one_body_momentum_i pi z/L)|, where one_body_momentum_i = 0,1,2,...\n");
  Message("  One-body term for (dn) |1 + Srecoil sin(one_body_momentum_i pi z/L)|, where one_body_momentum_i = 0,1,2,...\n");
  one_body_momentum_k = 2.*PI/L*(DOUBLE) one_body_momentum_i;
  Message("  Momentum %"LE" corresponding to excitation of level %i\n", one_body_momentum_k, one_body_momentum_i);
#endif

#ifdef ONE_BODY_TRIAL_ABC
  Message("  one-body term ABC: sin(kL*z)^alpha_latt with kL = PI/L\n");
  kL = PI/L;
#endif

#ifdef ONE_BODY_TRIAL_FERMI
  Message("  one-body term: 1/(1 + exp((r-Epar)/T))\n");
#endif

#ifdef ONE_BODY_TRIAL_GP_LHY_1D
  ConstructOneBodyGPHLY();
  Message("  one-body term: GP + LHY\n");
#endif

#ifdef VORTEX 
  lambda *= L;
  Message("  external potential, (1/2)c^2r^2, c = %lf, with up/dn vortices separated by lambda*L = %lf\n", c, lambda);
#endif

#ifdef ONE_BODY_VORTEX
  Message("  sin(pi*r/L)^Dpar, where Dpar = (2 C2 \mu) is vortex quantization for C2/r^2 external potential and mu = 1\n");
#endif

#ifdef ONE_BODY_TRIAL_SIN
#ifdef TRIAL_3D
  Message("  one-body term: [sin(kL*x)^2 + sin(kL*y)^2 + sin(kL*z)^2]^alpha_R with kL = PI*Nlattice/L.\n");
#endif
#ifdef TRIAL_2D
  Message("  one-body term: [sin(kL*x)^2 + sin(kL*y)^2]^alpha_R with kL = PI*Nlattice/L.\n");
#endif
#ifdef TRIAL_1D
  Message("  one-body term: beta_latt + sin(kL*z)^alpha_latt with kL = PI*Nlattice/L.\n");
#endif
  if(kL<0) { // kL is used in w.f.
    kL = PI*Nlattice/L;
    Message("  Defining kL from Nlattice, kL = %lf\n", kL);
  }
#endif

#ifdef VEXT_COS2 // kL is used in Vext
  if(kL<0) {
    kL = PI*Nlattice/L;
    Message("  Defining kL from Nlattice, kL = %lf\n", kL);
  }
  Message("  External lattice with height %" LF " Erec and kL = %e is present\n", Srecoil, kL);
#ifndef BC_2DPBC_SQUARE // i.e. NOT in Q2D case
#ifndef UNITS_SET_2M_TO_1
  Warning("  Changing lattice height to recoil energies %lf [Er] -> %lf [Eo], Er = %lf\n", Srecoil, Srecoil*(kL*kL/2.),kL*kL/2.);
  Srecoil *= 0.5*kL*kL;
  Message("  set UNITS_SET_2M_TO_1 to get output in recoil energies\n");
#endif
#endif
#endif

#ifdef TRIAL_GAUSS1122
  Message("  Gaussian Jastrow 11,22 terms will be used: f(r) = const - gamma [exp(-beta r^2) + exp(-beta (L-r)^2)]\n");
  ConstructGridGauss1122();
#endif

#ifdef TRIAL_GAUSS12
  Message("  Gaussian Jastrow 11,22 terms will be used\n");
#endif

#ifdef TRIAL_GAUSSIAN12
  Message("  Gaussian Jastrow 12 term will be used: f(r) = exp(-beta r^2)\n");
  G12.max = Lhalf;
#endif

#ifdef TRIAL_EXTERNAL_WF11
  LoadTrialWaveFunction(&G11, "inwf11.in", ON);
#endif
#ifdef TRIAL_EXTERNAL_WF12
  LoadTrialWaveFunction(&G12, "inwf12.in", ON);
#endif
#ifdef TRIAL_EXTERNAL_WF22
  LoadTrialWaveFunction(&G22, "inwf22.in", ON);
#endif

#ifdef INTERPOLATE_LINEAR_WF12
  Message("  Saving Jastrow12 term to grids for a better performance...\n");
  SampleTrialWaveFunction12(&G22, 1., Lhalf, grid_trial, &InterpolateExactU12, &InterpolateExactFp12, &InterpolateExactE12);
#else
#ifdef INTERPOLATE_SPLINE_WF12
  Message("  Spline interpolation will be used for Jastrow 12 term\n");
  SplineConstruct(&G12, 0., 0.);
  SaveWaveFunctionSpline(&G12, "wf12spline.dat");
#else
  Message("  Jastrow 12 term is calculated in an exact (analytic) form\n");
#endif
#endif

#ifdef INTERPOLATE_LINEAR_WF11
  Message("  Saving Jastrow11 term to grids for a better performance...\n");
  SampleTrialWaveFunction11(&G11, 1., Lhalf, grid_trial, &InterpolateExactU11, &InterpolateExactFp11, &InterpolateExactE11);
#else
#ifdef INTERPOLATE_SPLINE_WF11
  Message("  Spline interpolation will be used for Jastrow 11 term\n");
  SplineConstruct(&G11, 0., 0.); // number for first derivative, or 2e30 for zero second derivative (natural spline)
  SaveWaveFunctionSpline(&G11, "wf11spline.dat");
#else
  Message("  Jastrow 11 term is calculated in an exact (analytic) form\n");
#endif
#endif

#ifdef INTERPOLATE_LINEAR_WF22
  Message("  Saving Jastrow22 term to grids for a better performance...\n");
  SampleTrialWaveFunction22(&G22, 1., Lhalf, grid_trial, &InterpolateExactU12, &InterpolateExactFp12, &InterpolateExactE12);
#else
#ifdef INTERPOLATE_SPLINE_WF22
  Message("  Spline interpolation will be used for Jastrow 22 term\n");
  SplineConstruct(&G22, 0., 0.); // number for first derivative, or 2e30 for zero second derivative (natural spline)
  SaveWaveFunctionSpline(&G22, "wf22spline.dat");
#else
  Message("  Jastrow 22 term is calculated in an exact (analytic) form\n");
#endif
#endif

#ifdef SYM_OPPOSITE_SPIN
#ifdef INTERPOLATE_SPLINE_WF12_SYM
  Message("  Spline interpolation will be used for SYM 12 term\n");
  SplineConstruct(&GSYM, 0., 0.);
  SaveWaveFunctionSpline(&GSYM, "wf12SYMspline.dat");
#else
  Message("  SYM 12 term is calculated in an exact (analytic) form\n");
#endif
#endif

#ifdef INTERPOLATE_LINEAR_ORBITAL
  Message("  Saving Orbital term to grids for a better performance...\n");
  SampleTrialWaveFunctionOrbital(&Gorbital, 1., Lhalf, grid_trial, &orbitalExactF, &orbitalExactFp, &orbitalExactEloc);
#endif

#ifdef SYMMETRIZE_TRIAL_WF12
  Message("  An explicit symmetrization of w.f. will be used according to the rule u12(r) -> u12(r)+u12(L-r)\n");
#endif
#ifdef SYMMETRIZE_TRIAL_WF11
  Message("  An explicit symmetrization of w.f. will be used according to the rule u11(r) -> u11(r)+u11(L-r)\n");
#endif
#ifdef SYMMETRIZE_TRIAL_WF22
  Message("  An explicit symmetrization of w.f. will be used according to the rule u22(r) -> u22(r)+u22(L-r)\n");
#endif
  Message("done\n");

#ifdef SYM_OPPOSITE_SPIN // loop in symmetrized terms
#ifdef INTERPOLATE_SPLINE_WF12_SYM
  Message("  Spline interpolation will be used for SYM 12 term\n");
  SplineConstruct(&GSYM, 0., 0.);
  SaveWaveFunctionSpline(&GSYM, "wf12SYMspline.dat");
#else
  Message("  SYM 12 term is calculated in an exact (analytic) form\n");
#endif
#endif

#ifdef JASTROW_OPPOSITE_SPIN
  //SaveWaveFunction12(&G12, 0e-8, G12.max, "wf12.dat");
  SaveWaveFunction12(&G12, 0e-8, 1.1*G12.max, "wf12.dat");
#else
  Warning("  Jastrow opposite spin is not calculated.\n");
#endif

#ifdef JASTROW_SAME_SPIN
  SaveWaveFunction11(&G11, 0e-8, 1.1*G11.max, "wf11.dat");
  SaveWaveFunction22(&G22, 0e-8, 1.1*G22.max, "wf22.dat");
#else
  Warning("  Jastrow same spin is not calculated.\n");
#endif

#ifdef SYM_OPPOSITE_SPIN
  SaveWaveFunction12SYM(&GSYM, 0e-8, 1.1*GSYM.max, "wf12SYM.dat");
#else
  Warning("  SYM opposite spin is not calculated.\n");
#endif

#ifdef FERMIONS
  SaveBCSWaveFunction(1e-6, Lhalf, "wfBCS.dat");
  SaveOrbitalCuts();
#endif

#ifdef IMPURITY
  Message("  Impurities are present in the system.\n");
  ConstructGridImpurity(&Crystal, Rpar, Nimp);
#endif

  CheckTrialWaveFunction();

  if(var_par_array) {
    Warning("  NOT ENABLED! Branchng will decide which variatonal parameters are optimal.\n");
    //InitializeVariationalParameters();
  }

  // be sure that the interaction energy vanishes at r=L/2
#ifdef INTERACTION_ABSENT
  Message("  Interaction potential (not included in Eloc part): ABSENT\n");
#endif
#ifdef INTERACTION_DIPOLE
  Message("  Interaction potential (not included in Eloc part): DIPOLE\n");
#endif
#ifdef INTERACTION_LENNARD_JONES
  Message("  Interaction potential (not included in Eloc part): LENNARD JONES\n");
#endif

#ifdef TRIAL_1D
  Message("  1D Tail energy (potential energy) %"LE"\n", IntegrateSimpson(Lhalf, Lhalf*10, &InteractionEnergy12, 100000L, DIMENSION)*n);
  Message("  Tonks energy %"LE"\n", PI*PI*n*n/6.);
#endif

#ifdef EXTERNAL_POTENTIAL
  SaveVext();
#endif

  //Message("  Tail energy (kinetic part, up-up) %"LE"\n", IntegrateSimpson(Lhalf, Lhalf*10, &InterpolateExactE11, 100000L, DIMENSION) * n);
  //Message("  Tail energy (kinetic part, up-dn) %"LE"\n", IntegrateSimpson(Lhalf, Lhalf*10, &InterpolateExactE12, 100000L, DIMENSION) * n); // does not work for splines
  //Etail = 0.5*0.5*n*IntegrateSimpson(L*(0.5+(int)sum_over_images_cut_off_num), L*10000, &InteractionEnergy11, 10000000L, DIMENSION);
  //Message("  Tail energy (potential part, up-up) %"LE"\n", energy_unit*Etail);
  //energy_shift += Etail;

  //Etail = 0.5*n*IntegrateSimpson(L*(0.5+(int)sum_over_images_cut_off_num), L*10000, &InteractionEnergy12, 10000000L, DIMENSION);
  //Message("  Tail energy (potential part, up-dn) %"LE"\n", energy_unit*Etail);
  //energy_shift += Etail;

  //Etail = 0.5*0.5*n*IntegrateSimpson(L*(0.5+(int)sum_over_images_cut_off_num), L*10000, &InteractionEnergy22, 10000000L, DIMENSION);
  //Message("  Tail energy (potential part, dn-dn) %"LE"\n", energy_unit*Etail);
  //energy_shift += Etail;

  //Warning("  Energy will be shifted by %"LE"\n", energy_unit*energy_shift);

  if(optimization == 2) {
    Optimization();
    return 0;
  }

#ifdef FERMIONS
  AllocateFermionMatrices();
#endif

  if(generate_new_coordinates) {
    AllocateWalkers();
    Nwalkers = Npop;
    GenerateCoordinates();
  }
  else {
    LoadCoordinates();
  }

  if(MC == DIFFUSION && measure_pure_coordinates) CopyPureCoordinates();

  if(MC == MOLECULAR_DYNAMICS) {
    if(generate_new_coordinates) {
      GenerateVelocities();
    }
    else{
      if(LoadVelocities()) GenerateVelocities();
    }
  }

#ifdef USE_LAPACK // need to have walkers already to be allocated
  Message("\n  Lapack package will be used to do matrix inversion\n\n");
  CheckLapack();
#endif

    if(MC) { // DIFFUSION, CLASSICAL, MOLECULAR_DYNAMICS
    if(measure_SD) {
      for(w=0; w<Nwalkers; w++) {
        for(i=0; i<Nup; i++) {
          W[w].rreal[i][0] = W[w].x[i];
          W[w].rreal[i][1] = W[w].y[i];
          W[w].rreal[i][2] = W[w].z[i];
        }
        for(i=0; i<Ndn; i++) {
          W[w].rreal[i][3] = W[w].xdn[i];
          W[w].rreal[i][4] = W[w].ydn[i];
          W[w].rreal[i][5] = W[w].zdn[i];
        }
      }
    }
  }

  if(MC == DIFFUSION) {
    Npop_max = Npop + Npop / 10;
    Npop_min = Npop - Npop / 10;
    reduce  = 0.5*(DOUBLE) (Npop_min + Npop_max) / (DOUBLE) (Npop_max);
    amplify = 0.5*(DOUBLE) (Npop_min + Npop_max) / (DOUBLE) (Npop_min);
    Ndead_walkers = 0;
    for(i=0; i<Nwalkers; i++) {
      W[i].status = ALIVE;  
      walkers_storage[i] = i;
    }
    for(i=Nwalkers; i<NwalkersMax; i++) {
      W[i].status = DEAD;
      W[i].weight = 0.;
      dead_walkers_storage[i-Nwalkers] = i;
      Ndead_walkers++;
    }
  }

  Eo = 0;
  for(w=0; w<Nwalkers; w++) {
    W[w].Eold = W[w].E = WalkerEnergy0(&W[w]);
    W[w].U = U(W[w]);
    Eo += W[w].E;
  }
  Eo /= (DOUBLE) Nwalkers;

  E = Energy(&Epot, &Ekin, &EFF, &Eext);
  Message("\nEnergy %"LG" = %"LG" * %"LG", EFF = %"LG"\n", E*Nmean, Nmean, E, EFF);
  if(isnan(E) && MC == DIFFUSION) Error("  infinite energy!\n");
  //SaveEnergyCut();

  if((MC == DIFFUSION || MC == VARIATIONAL) && check_kinetic_energy) CheckEnergyAndDerivatives();

#ifdef MPI
  ParallelInitialize2();
#endif

  if(MC == DIFFUSION && SmartMC == DMC_PSEUDOPOTENTIAL) InitializePermutationMatrix();

  Eo = 0;
  if(blck_heating) Message("Starting heating...\n");
  time_communicating = clock() - time_start;
  for(block=-blck_heating; block<blck; block++) {
    time_begin = clock();
    accepted = accepted_one = accepted_all = 0; // control rate initialization
    rejected = rejected_one = rejected_all = 0;
    overlaped = 0;
    tried = 0;
    Eblck = 0.;
    measured_blck = 0;

    if(block == 0) Message("Starting measurements...\n");
    EmptyDistributionArrays();

    if(verbosity>1) Message("Block No %i\n", block+1);
    for(iteration_global=1; iteration_global<=Niter; iteration_global++) {
#ifdef MPI
      //if(MC == DIFFUSION || (iteration_global == 1 && block==-blck_heating)) {
      //time_done_in_parallel += MPI_Wtime();  // update counter
      DistributeAllWalkers();
      //time_done_in_parallel -= MPI_Wtime();  // initialize counter of the parallel job
#endif

#pragma omp parallel for
      for(w=0; w<Nwalkers; w++) {
        if(MC == VARIATIONAL) {
          if(SmartMC == VMC_MOVE_ALL) {
            VMCMoveAll(w);
          }
          else if(SmartMC == VMC_MOVE_ONE) {
            VMCMoveOneByOne(w);
          }
        }
        else if(MC == DIFFUSION) {
          if(SmartMC == DMC_QUADRATIC) {
            DMCQuadraticMove(w);
          }
          else if(SmartMC == DMC_LINEAR_GAUSSIAN) {
            DMCLinearMoveGaussian(w);
          }
          else if(SmartMC == DMC_LINEAR_MIDDLE) {
            DMCLinearMoveMiddle(w);
          }
          else if(SmartMC == DMC_PSEUDOPOTENTIAL) {
            if(w==0) DMCPseudopotentialMove(w);
          }
          else if(SmartMC == DMC_LINEAR_METROPOLIS) {
            DMCLinearMetropolisMove(w);
          }
          else if(SmartMC == DMC_LINEAR_METROPOLIS_MIDDLE) {
            DMCLinearMetropolisMoveMiddle(w);
          }
          else if(SmartMC == DMC_PSEUDOPOTENTIAL_REWEIGHTING) {
            DMCPseudopotentialReweightingMove(w);
          }
        }
        else if(MC == CLASSICAL) { // classical Monte Carlo
          ClassicalMoveOneByOne(w);
        }
        else if(MC == MOLECULAR_DYNAMICS) { // Molecular Dynamics
          MolecularDynamicsMove(w);
        }
        //else if(MC == PIMC) {
        //PIMCMoveOneByOne(w);
        //  PIMCPseudopotentialMoveOneByOne(w);
        //}
        else if(MC == PIGS) {
          if(SmartMC == PIGS_PRIMITIVE)
            PIGSMoveOneByOne(w);
          else if(SmartMC == PIGS_PSEUDOPOTENTIAL)
            PIGSPseudopotentialMoveOneByOne(w);
          else if(SmartMC == PIGS_PSEUDOPOTENTIAL_OBDM)
            PIGSPseudopotentialMoveOneByOneOBDM(w);
        }
      }
      //#pragma omp single

      if(MC == DIFFUSION) { // Branching, serial code
#ifdef MPI
        time_done_in_parallel += MPI_Wtime();  // update counter
        CollectAllWalkers();
        if(MPI_myid == MASTER) Branching();
        DistributeWalkersScatter();
        time_done_in_parallel -= MPI_Wtime();  // update counter
#else
        Branching();
#endif
      }
      else if(MC == PIGS && iteration_global % Nall == 0) {
         PIGSMoveAll();
       }

#ifdef CENTER_OF_MASS_IS_NOT_MOVED
      AdjustCenterOfMass();
#endif
#ifdef UP_PARTICLES_PINNED_TO_A_CHAIN
      AdjustUpParticlesPinnedToAChain();
#endif

      if(block>= 0 && iteration_global % Nmeasure == 0) {
        time_simulating += clock() - time_begin;
        Measure(block, iteration_global);
        time_begin = clock();
        if(video) Picture();
      }
      if(optimization == 1 && block>=0 && iteration_global % Niter_store == 0) SaveCoordinatesOptimization();
      if(MC && block>= 0 && measure_SD && iteration_global % SD.spacing == 0) MeasureSuperfluidDensity();
      //if(MC && block>= 0 && measure_SD && (iteration_global+block*Niter) % (SD.spacing * SD.size) == 0) SaveSuperfluidDensity();
    }

    time_simulating += clock() - time_begin;

    // print acceptance rates
    // adjust the timestep
    Message("%i ", block+1);

    time(&time_era_end);
    MessageTimeElapsed((int)(time_era_end-time_era_begin));

    if(acceptance_rate != 0. && (MC == VARIATIONAL || MC == PIGS || MC == PIMC)) {
      Message("dt= %.1g ", dt);
      if(accepted) // adjust the timestep
        dt *= (DOUBLE) accepted/((DOUBLE) (accepted+rejected)*acceptance_rate);
      else
        dt *= 1e-2;
    }
    if(rejected) Message("(%.2lf%%) ", (DOUBLE) accepted/(DOUBLE)(accepted+rejected)*100);
    if(overlaped) Message("[%.2lf%%] ", (DOUBLE) overlaped/(DOUBLE)(tried)*100.);

    // calculate advance in imaginary time
    if(MC == DIFFUSION) {
      imag_time_done += Niter*dt;
      Message("T=%"LG" ", imag_time_done);
    }
    else if(SmartMC == VMC_MOVE_ONE) {
      Message("T=%"LG" ", imag_time_done / (DOUBLE) (Nmean*Nwalkers));
    }
    else {
      //Message("T=%"LG" ", imag_time_done / (DOUBLE) Nwalkers);
    }

    if(MC == CLASSICAL && Ndn>0) {
      //dt2 = (DOUBLE) (accepted_one+1)/((DOUBLE) (accepted_one+rejected_one+1)); // acceptance 1
      //dt2 /= (DOUBLE) (accepted_all+1)/((DOUBLE) (accepted_all+rejected_all+1)); // a1/a2
      //dt2 *= 1.75*dt1; // 1.75*a1/a2*dt1
      //Message("dt2=%"LG, dt2);
    }
    if(accepted+rejected) {
      Message(" (%.2"LF"%%) ", (DOUBLE) accepted/(DOUBLE)(accepted+rejected)*100.);
      if(overlaped) Message("[%.2"LF"%%] ", (DOUBLE) overlaped/(DOUBLE)(accepted+rejected)*100.);
    }
    if(rejected_one) Message("(one %.2" LF "%%) ", (DOUBLE) accepted_one/(DOUBLE)(accepted_one+rejected_one)*100.);
    if(rejected_all) Message("(all %.2" LF "%%) ", (DOUBLE) accepted_all/(DOUBLE)(accepted_all+rejected_all)*100.);

    if(overlaped) Message("[%.2"LF"%%] ", (DOUBLE) overlaped/(DOUBLE)tried*100.);

    if(MC == DIFFUSION || MC == VARIATIONAL) {
      Emessage = energy_unit*(Evar/(DOUBLE) Nmeasured+energy_shift);
      Message("<E>=%3g ", Emessage);
      Emessage = energy_unit*(E+energy_shift);
      Message("E=%3g ", Emessage);
      Emessage = energy_unit*(EFF+energy_shift);
      Message("EFF=%3g ", Emessage);
      //Message("Ekin=%3g Epot=%"LG" ", energy_unit*Ekin, energy_unit*Epot);
      if(MC == DIFFUSION) Message("Nw=%i ", Nwalkers);
    }

    if(MC == MOLECULAR_DYNAMICS && SmartMC == 0) { // fixed T
      Message("E=%3g Epot=%3g Ekin=%3g ", E, Epot, Ekin);
      AdjustVelocitiesToTemperature();
    }

#ifdef BC_ABSENT
#ifdef TRIAL_1D
    Message(" <%.2"LF",%.2"LF">", RDzup.r2recent, RDzup.r2recent);
    Message(" <%.2"LF",%.2"LF">", RDzdn.r2recent, RDzdn.r2recent);
#endif
#endif

#ifdef _OPENMP
    time(&time_era_end);
    Message("OpenMP %.2lf %.2lf", 
      (DOUBLE)(clock() - time_start) / (DOUBLE)CLOCKS_PER_SEC / (DOUBLE)(time_era_end - time_era_begin),
      (DOUBLE)(clock() - time_start) / (DOUBLE)CLOCKS_PER_SEC / (omp_get_wtime() - time_start_omp));
#endif

    Message("\n");
  }

  if(video) CloseGraph();

  if(Nmeasured == 0) 
    Warning("No measurements done\n");
  else {
    Evar /= (DOUBLE) Nmeasured;
    Evar2 /= (DOUBLE) Nmeasured;
    sigma = sqrt(Evar2-Evar*Evar);
    error = sigma/(sqrt((DOUBLE)(Nmeasured-1)));
    Message("Evar = %.15"LF" +/- %"LF"\n", energy_unit*(Evar+energy_shift), energy_unit*error);

    EFFvar /= (DOUBLE) Nmeasured;
    EFFvar2 /= (DOUBLE) Nmeasured;
    sigma = sqrt(EFFvar2-EFFvar*EFFvar);
    error = sigma/(sqrt((DOUBLE)(Nmeasured-1)));
    Message("EFF  = %"LF" +/- %"LF"\n", energy_unit*(EFFvar+energy_shift), energy_unit*error);

    Epotvar /= (DOUBLE) Nmeasured;
    Epotvar2 /= (DOUBLE) Nmeasured;
    sigma = sqrt(Epotvar2-Epotvar*Epotvar);
    error = sigma/(sqrt((DOUBLE)(Nmeasured-1)));
    Message("Epot = %"LF" +/- %"LF"\n", energy_unit*(Epotvar+energy_shift), energy_unit*error);

    Ekinvar /= (DOUBLE) Nmeasured;
    Ekinvar2 /= (DOUBLE) Nmeasured;
    sigma = sqrt(Ekinvar2-Ekinvar*Ekinvar);
    error = sigma/(sqrt((DOUBLE)(Nmeasured-1)));
    Message("Ekin = %"LF" +/- %"LF"\n", energy_unit*(Ekinvar+energy_shift), energy_unit*error);

    if(boundary == NO_BOUNDARY_CONDITIONS) Message("Eint = %"LF"\n", 2.*(Epotvar-Ekinvar));

    Etail = IntegrateSimpson(Lhalf, Lhalf*10, &InterpolateEG11, 100000L, 1) * n;
    Message("  Tail energy %"LE" (up-up)\n", energy_unit*Etail);
    Etail += IntegrateSimpson(Lhalf, Lhalf*10, &InterpolateEG12, 100000L, 1) * n;
    Message("  Tail energy %"LE"  and (up-down)\n", energy_unit*Etail);
    Message("  Total energy %.15"LE"\n", energy_unit*(Evar + Etail));

    if(boundary == ONE_BOUNDARY_CONDITION) {
      /* i.e. in the case of a tube */ 
      xR2 /= (DOUBLE) blck;
      Message("<r2>^0.5 = %"LF"\n", sqrt(xR2));
      Message("<r2>^0.5/r_osc = %"LF"\n", 2.*lambda*xR2);
      Message("<x2>^0.5 = %"LF"\n", sqrt(0.5*xR2));
    }
    if(boundary == NO_BOUNDARY_CONDITIONS) {
      /* i.e. the trap */
      xR2 /= (DOUBLE) blck;
      zR2 /= (DOUBLE) blck;
      Message("<r2>^0.5 = %"LF"\n", sqrt(xR2));
      Message("<x2>^0.5 = %"LF"\n", sqrt(0.5*xR2));
      Message("<z2>^0.5 = %"LF"\n", sqrt(zR2));
      Message("<z2>^0.5/z_osc = %"LF"\n", 2.*lambda*zR2);
    }
 }

  time(&time_era_end);
  Message("\ntimetotal= %.2lf sec = %.2lf hours = %.2lf days\n", (DOUBLE)(time_era_end-time_era_begin), (DOUBLE)(time_era_end-time_era_begin)/3600., (DOUBLE)(time_era_end-time_era_begin)/3600/24.);
  Message("timeprecise= %.2lf sec = %.2lf hours = %.2lf days\n", (DOUBLE)(clock()-time_start)/(DOUBLE)CLOCKS_PER_SEC, (DOUBLE)(clock()-time_start)/(DOUBLE)CLOCKS_PER_SEC/3600., (DOUBLE)(clock()-time_start)/(DOUBLE)CLOCKS_PER_SEC/3600./24.);
  Message("time used for doing simulation    %.2"LF" hours\n", (DOUBLE) time_simulating/(DOUBLE)CLOCKS_PER_SEC/3600.);
  Message("time used for doing measurements  %.2"LF" hours\n", (DOUBLE) time_measuring/(DOUBLE)CLOCKS_PER_SEC/3600.);
  Message("time used for doing communication %.2"LF" hours\n", (DOUBLE) time_communicating/(DOUBLE)CLOCKS_PER_SEC/3600.);

#ifdef _OPENMP
  Message("Parallel simulation, acceleration= %"LE"\n", (DOUBLE)(clock() - time_start) / (DOUBLE)CLOCKS_PER_SEC / (DOUBLE)(time_era_end - time_era_begin));
#endif

#ifdef MPI
  ParallelStop();
#endif

#ifdef MPI // only master saves coordinates
  if(MPI_myid == MASTER) 
#endif
  SaveCoordinates();

  if(MC == MOLECULAR_DYNAMICS) SaveVelocities();

  Message("done\n");
  return 0;
}

/*********************************** Measure *********************************/
void Measure(int block, int iter) {
  clock_t time_begin;
  static int first_time = ON;
  int w;

  time_begin = clock();
  Nmeasured++;

  if(measure_OBDM_MATRIX) MeasureOBDMMatrix();
  if(measure_g3) MeasureHyperRadius();

  //if(measure_RadDistr) MeasureRadialDistributionWalker(&W[0]);
  if(measure_FormFactor && MC == DIFFUSION) MeasureFormFactor(-1, 1); // update counter

  if(MC != PIGS) {
#pragma omp parallel for
    for(w=0; w<Nwalkers; w++) {
      if(measure_PairDistr) MeasurePairDistribution(w);
      if(measure_RadDistr) MeasureRadialDistributionWalker(&W[w]);
      if(measure_FormFactor && MC == DIFFUSION) MeasureFormFactor(w, 1);
      if(measure_Sk) MeasureStaticStructureFactor(w);
      //if(measure_Sk) MeasureStaticStructureFactorAngularPart();
      if(Ndn == 1 && measure_OBDM) MeasureOBDMresidue(w);
      if(measure_XY2) MeasureXY2(w);
    }
  }
  else { // PIGS
    //w = 0; // at the beggining of the chain (mixed estimator)
    w = Nwalkers/2; // in the middle of the chain (pure estimator)
    if(measure_PairDistr) MeasurePairDistribution(w);
    if(measure_RadDistr) MeasureRadialDistributionWalker(&W[w]);
    if(measure_Sk) MeasureStaticStructureFactor(w);
  }

#ifndef TRIAL_1D // in 1D is measured in OBDM1D
  if(measure_Nk) MeasureMomentumDistribution();
#endif
  if(measure_Lind) MeasureLindemannRatio();

  if(measure_RadDistr) {
    RD.r2recent = 0.;
    if(MC != PIGS) {
      if(first_time == OFF && MC == DIFFUSION) SaveMeanR2DMC();
    }
    RD.r2recent = sqrt(RD.r2recent / (DOUBLE)(Nwalkers * Nmean));
  }

  if(MC == VARIATIONAL) {
#ifdef TRIAL_1D
    if(measure_TBDM) MeasureTBDM1D();
    if(measure_OBDM) MeasureOBDM1D();
#else
    if(measure_TBDM) MeasureTBDM();
    if(measure_OBDM) MeasureOBDM();
#endif

    if(measure_energy) {
      E = Energy(&Epot, &Ekin, &EFF, &Eext);
      //Ekin -= EFF; // now Ekin is the kinetic energy
      E = Epot + Ekin; // total energy
      //EFF += Epot; // force^2 estimator
      if(reweighting) MeasureEnergyReweighting();
      SaveEnergyVMC(E, EFF);
    }

    if(verbosity > 1) {
      if(boundary == THREE_BOUNDARY_CONDITIONS) {
        Message("%i %li E=%3e EFF=%3e Ekin=%3e Nw=%i\n", block+1, iter, E, EFF, Ekin, Nwalkers);
      }
      else if(boundary == ONE_BOUNDARY_CONDITION) {
        Message("%i %li E=%3%"LF" EFF=%3%"LF" Ep=%3%"LF" Ekin=%"LF" <%.2"LF"> Nw=%i\n", 
          block+1, iter, E, EFF, Epot, Ekin, RD.r2 / (DOUBLE) (RD.times_measured * Nmean), Nwalkers);
      }
      else {
        Message("%i %li E=%3%"LF" EFF=%3%"LF" Ep=%3%"LF" Ekin=%"LF" <%.2"LF",%.2"LF"> Nw=%i\n", 
          block+1, iter, E, EFF, Epot, Ekin, RD.r2 / (DOUBLE) (RD.times_measured * Nmean),
          RDzup.r2 / (DOUBLE) (RD.times_measured * Nmean), Nwalkers);
      }
    }
    Evar += E;
    Evar2 += E*E;
    EFFvar += EFF;
    EFFvar2 += EFF*EFF;
    Epotvar += Epot;
    Epotvar2 += Epot*Epot;
    Ekinvar += Ekin;
    Ekinvar2 += Ekin*Ekin;

    if(measure_R2) SaveMeanR2();
  }
  else if(MC == DIFFUSION) { // Diffusion Monte Carlo
    if(SmartMC == DMC_PSEUDOPOTENTIAL) { // save total energy
      E = Eo;
    }
    else { // save energy
      E = Eo / Nmean;
    }

    //E = EnergyO();
    if(measure_energy) SaveEnergyDMC(E);
    if(measure_spectral_weight) MeasureSpectralWeight();
    Evar += E;
    Evar2 += E*E;
    if(verbosity) Message("%i %li E = %"LE" walkers: %i\n", block+1, iter, E, Nwalkers);

    MeasurePurePotentialEnergy();

    if(measure_pure_coordinates && iter % (grid_pure_block*Nmeasure) == 0) SavePureCoordinates();
  }
  else if(MC == CLASSICAL) { // Classical Monte Carlo
    if(measure_RadDistr) MeasureRadialDistribution();
    if(measure_energy) SaveEnergyCLS();
  }
  else if(MC == MOLECULAR_DYNAMICS) { // Molecular Dynamics
    if(measure_RadDistr) MeasureRadialDistribution();
    if(measure_energy) SaveEnergyMD();
  }
  else if(MC == PIGS) {
    if(measure_energy) {
      SaveEnergyPIGS();
    }
  }

#ifdef MPI // only master saves data
  if(MPI_myid == MASTER) 
#endif
  if(iter % Niter == 0) SaveBlockData(block);

  time_measuring += clock() - time_begin; 
}
