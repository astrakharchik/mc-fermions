/*pimc.c*/
#include <stdio.h>
#include "main.h"
#include "randnorm.h"
#include "rw.h"
#include "compatab.h"
#include "memory.h"
#include "vmc.h"
#include <limits.h>
#include <memory.h>

void PIMCMoveOneByOne(int w) {
  int i,k;
  int w1,w2; // index of the walker with [w-1] and [w+1]
  DOUBLE du;
  DOUBLE xi;
  DOUBLE dx,dy,dz;
  DOUBLE x,y,z;
  DOUBLE xp,yp,zp;
  DOUBLE dr[3];
  DOUBLE xm,ym,zm; // position of the middle point
  DOUBLE r2;
  DOUBLE tau;
  //DOUBLE sigma;

  w1 = w-1;
  w2 = w+1;
  if(w1<0) w1 = Nwalkers-1;
  if(w2==Nwalkers) w2 = 0;

  tau = 1./(T*(DOUBLE)Nwalkers);
  //sigma = 0.5*tau;

  // Move particle i of the bid
  for(i=0; i<N; i++) {
    du = 0.;

    x = W[w].x[i];
    y = W[w].y[i];
    z = W[w].z[i];
    xm = 0.5*(W[w1].x[i]+W[w2].x[i]); // middle point
    ym = 0.5*(W[w1].y[i]+W[w2].y[i]);
    zm = 0.5*(W[w1].z[i]+W[w2].z[i]);

    // Wp is the trial position
    RandomNormal3(&dx, &dy, &dz, 0., Sqrt(dt_vmc));

#ifdef TRIAL_3D
    xp = x + dx;
    yp = y + dy;
    zp = z + dz;
#endif

#ifdef TRIAL_2D
    xp = x + dx;
    yp = y + dy;
    zp = 0.;
#endif

#ifdef TRIAL_1D
    xp = 0.;
    yp = 0.;
    zp = z + dz;
#endif

    ReduceToTheBoxXYZ(&xp, &yp, &zp);

    // external potential
    du -= tau*Vext(xp,yp,zp);
    du += tau*Vext(x,y,z);

    // new kinetic energy
    dr[0] = xp - xm;
    dr[1] = yp - ym;
    dr[2] = zp - zm;
    r2 = FindNearestImage(&dr[0], &dr[1], &dr[2]);
    //du -= r2/(2.*sigma);
    du -= r2/(2.*tau);

    // old kinetic energy
    dr[0] = x - xm;
    dr[1] = y - ym;
    dr[2] = z - zm;
    r2 = FindNearestImage(&dr[0], &dr[1], &dr[2]);
    //du += r2/(2.*sigma);
    du += r2/(2.*tau);

    for(k=0; k<N; k++) {
      if(k != i) {
        // calculate new potential energy
        /*dr[0] = xp - W[w1].x[k];
        dr[1] = yp - W[w1].y[k];
        dr[2] = zp - W[w1].z[k];
        r2 = FindNearestImage(&dr[0], &dr[1], &dr[2]);
        du -= tau*InteractionEnergy(sqrt(r2));
        dr[0] = xp - W[w2].x[k];
        dr[1] = yp - W[w2].y[k];
        dr[2] = zp - W[w2].z[k];
        r2 = FindNearestImage(&dr[0], &dr[1], &dr[2]);
        du -= tau*InteractionEnergy(sqrt(r2));*/

        /*// calculate old potential energy
        dr[0] = x - W[w1].x[k];
        dr[1] = y - W[w1].y[k];
        dr[2] = z - W[w1].z[k];
        r2 = FindNearestImage(&dr[0], &dr[1], &dr[2]);
        du += tau*InteractionEnergy(sqrt(r2));
        dr[0] = x - W[w2].x[k];
        dr[1] = y - W[w2].y[k];
        dr[2] = z - W[w2].z[k];
        r2 = FindNearestImage(&dr[0], &dr[1], &dr[2]);
        du += tau*InteractionEnergy(sqrt(r2));*/
      }
    }

    // The Metropolis code
    // f = Exp(u), p = Exp(up-u)
    if(du > 0) {
      W[w].x[i] = xp;
      W[w].y[i] = yp;
      W[w].z[i] = zp;
      W[w].U -= du;
      accepted++;
    }
    else {
      xi = Random();
      if(xi<Exp(2.*du)) {
        W[w].x[i] = xp;
        W[w].y[i] = yp;
        W[w].z[i] = zp;
        W[w].U -= du;
        accepted++;
      }
      else {
        rejected++;
      }
    }
  }
}