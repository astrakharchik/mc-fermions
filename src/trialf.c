/*trialf.c*/
#include <math.h>
#include "trialf.h"
#include "main.h"
#include "utils.h"
#include "memory.h"
#include <stdio.h>
#include "fermions.h"
#include "compatab.h"
#include "mymath.h"
#include "trial.h"

DOUBLE BCSsqE, BCSsqE2, BCSCh2gamma, BCSBtrial, BCStrial_Kappa, BCStrial_Kappa2, BCSdelta, BCStrial_a1, BCStrial_a2;
DOUBLE BCS2sqE, BCS2sqE2, BCS2Ch2gamma, BCS2Btrial, BCS2Ctrial;
DOUBLE BCS2Dtrial, BCS2Etrial;
DOUBLE BCS2trialKappa, BCS2trialKappa2, BCS2delta, BCS2trial_Vo, BCS2trial_delta;
DOUBLE BCSf, BCSfp;
DOUBLE BCSasymptValue;
DOUBLE Ch2gamma, Chnorm;
DOUBLE trial_PI_L, trial_PI_L2; 
int subtract_energy = OFF;

#define power(a,b) pow(a,b)

/************************** Load Trial Wave Function **********************/
void LoadOrbital(void) {
  FILE *in;
  int i;
  long int size;

  Message("Loading orbital term ... ");

  in = fopen("wfBCS.in", "r");

  if(in == NULL) {
    perror("\nError: can't load trial wave function from file,\nexiting ...");
    Error("\nError: can't load " INPATH "%s file,\nexiting ...", "wfBCS.in");
  }

  fscanf(in, "N= %li\n", &size);
  fscanf(in, "min= %"LF"\n", &BCS.min);
  fscanf(in, "max= %"LF"\n", &BCS.max);
  //fscanf(in, "V= %"LF"\n", &trial_Vo);

  Message("\n  Memory allocation : allocating grid of size %li...\n", size);
  BCS.x   = (DOUBLE*) Calloc("BCS->x   ", size, sizeof(DOUBLE));
  BCS.f  =  (DOUBLE*) Calloc("BCS->f  ", size, sizeof(DOUBLE));
  BCS.fp =  (DOUBLE*) Calloc("BCS->fp",  size, sizeof(DOUBLE));
  BCS.E   = (DOUBLE*) Calloc("BCS->E",    size, sizeof(DOUBLE));
  Message("  done\n");

  BCS.size = size;

  fscanf(in, "x f fp Eloc\n");
  for(i=0; i<size; i++) {
    fscanf(in,"%"LF" %"LF" %"LF" %"LF"\n", &BCS.x[i], &BCS.f[i], &BCS.fp[i], &BCS.E[i]);
  };

  fclose(in);

  BCS.step = (BCS.max - BCS.min) / (DOUBLE) (size-1);
  BCS.I_step = (DOUBLE) size / (BCS.max - BCS.min);
  Message("  Last element of the grid: error %"LE" %%\n", (BCS.max-BCS.min-BCS.step*(size-1))/BCS.max*100.);
  BCS.max2 = BCS.max*BCS.max;

  Message("done\n");
}

/************************** Construct SqWell depth ************************/
// uses m_up and m_dn to define the reduced mass
void ConstructSqWellDepth(void) {
#ifdef TRIAL_3D
  DOUBLE V,y,ymin,ymax,x,xmin,xmax;
  DOUBLE precision = 1e-8;
  int search = ON;
  DOUBLE R;
  DOUBLE kappa;

//#ifndef EXTERNAL_POTENTIAL
//  R = 1;
//  Warning("Using square well size as a unit of length, R=1\n");
//#else
  Message("  Constructing square well of diameter RoSW=%lf and s-wave scattering length a=%lf\n", RoSW, a);
  R = RoSW;
//#endif

  if(a<RoSW) Warning("  a<RoSW: SW solution might contain nodes!\n");

  // calculate strength of the potential
  // Solve  tg x / x = 1 - a/R, i.e. zero energy scattering solution
  // Solve  tg x / x = exp(-R/a)
  //       x = kappa R
  //       kappa = sqrt(2 mu Vo ) / hbar
  if(fabs(a)<1e9) {
    V = 1. - a/R;
    if(a>0) { // b.s. is present
      xmin = (pi+precision)/2.;
      xmax = (pi-precision)*3./2.;
    }
    else { // no b.s.
      xmin = precision;
      xmax = (pi-precision)/2.;
    }
    ymin = tg(xmin)/xmin - V;
    ymax = tg(xmax)/xmax - V;
    if(ymin*ymax > 0) 
      Error("R = %"LF" Can't construct square well", R);
    while(search) {
      x = (xmin+xmax)/2.;
      y = tg(x)/x - V;
      if(y*ymin < 0) {
        xmax = x;
        ymax = y;
      } else {
        xmin = x;
        ymin = y;
      }
      if(fabs(y)<precision) {
        x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
        y = tg(x)/x - V;
        search = OFF;
      }
    }
    Message("  Solution is kappa R  = %"LF" = pi/2 + %"LE", error %"LE"\n", x, x-pi/2, y);
    kappa = x / R;
    Message("  Potential depth %"LE"\n", kappa*kappa);
  }
  else {
    Warning("  Unitarity limit\n");
    kappa = pi/(2.*R) + precision;
  }

  Message("  Check a = %"LE", goal value %"LE"\n", R*(1.-tg(kappa*R)/(kappa*R)), a);

  // kappa = sqrt(2 mu Vo ) / hbar
  // Vo = \hbar^2 kappa^2 / (2 mu)
  trial_Vo = kappa*kappa/(2.*m_mu);
  Message("  Potential depth Vo= %"LE"\n", trial_Vo);

  if(a<0) {
    Warning("  Negative scattering length\n");
    a = -a;
  }
#else // 2D system
  DOUBLE V,y,ymin,ymax,x,xmin,xmax;
  DOUBLE precision = 1e-8;
  int search = ON;
  DOUBLE R;
  DOUBLE kappa;
  int iter = 0;

  Message("Constructing square well of radius RoSW = %lf\n", RoSW);
  R = RoSW;
  //calculate strength of the potential
  // define kappa
  // x = kappa R

  if(fabs(a)<1e99) { // Solve Jo(x) / (x J1(x)) = ln(a/R)
    V = Log(a/R);
    xmin = 3.8317059702075107; // a = 0, resonance position
    //xmax = 0.01*sqrt(2./(a/R)); // Jo(x) / (x J1(x)) = 2/x^2, x -> 0
    xmax = 1e-16;
    ymin = BesselJ0(xmin)/(xmin*BesselJ1(xmin)) - V;
    ymax = BesselJ0(xmax)/(xmax*BesselJ1(xmax)) - V;
    if(ymin*ymax > 0) Error("R = %"LF" Can't construct the grid : No solution found", R);
    while(search) {
      x = (xmin+xmax)/2.;
      y = BesselJ0(x)/(x*BesselJ1(x)) - V;
      if(y*ymin < 0) {
        xmax = x;
        ymax = y;
      } else {
        xmin = x;
        ymin = y;
      }
      if(fabs(y)<precision) {
        x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
        y = BesselJ0(x)/(x*BesselJ1(x)) - V;
        search = OFF;
      }
      if(iter++ > 1000) {
        Warning("  maximal number of iterations reached, exiting!\n");
        search = OFF;
      }
      //Message("x = %"LG" y = %"LG"\n", x, y);
    }
    Message("  Solution is kappa R  = %"LF", error %"LE"\n", x, y);
    kappa = x / R;
    Message("  Potential depth Vo= %"LE"\n", kappa*kappa);
  }
  else { 
    Message("  Unitarity limit\n");
    //kappa = pi/2. + precision;
    kappa = 3.8317059702075107 + precision;
  }

  Message("  Check a = %"LE"\n", R*Exp(BesselJ0(kappa*R)/(kappa*R*BesselJ1(kappa*R)))); // Solve Jo(x) / (x J1(x)) = ln(a/R)

  trial_Vo = kappa*kappa/(2.*m_mu);
#endif
}

/************************** Construct SqWell depth ************************/
void ConstructSqWellDepthPwave(void) {
  DOUBLE V,y,ymin,ymax,x,xmin,xmax;
  DOUBLE precision = 1e-8;
  int search = ON;
  DOUBLE Rp;
  DOUBLE kappa;

  Message("  Constructing p-wave square well potential\n");

#ifndef TRAP_POTENTIAL
  Rp = 1.;
  Message("  Using square well size as a unit of length, Rp=1\n");
#else
  Rp = Ro;
#endif

  //calculate strength of the potential
  // define kappa
  // Solve  (s) tg x / x = 1 - a/R, E>V_0
  //        (p) 1/(x tg x) - 1/x^2 = ap/R^3 - 1/3
  //       x = kappa R

  if(fabs(ap)<1e9) {
    //(s) V = 1. - a/R;
    V = ap/Rp - 1./3.;
    if(ap>0) {
      xmin = pi+precision;
      xmax = 2.*pi-precision;
    }
    else {
      xmin = precision;
      xmax = pi-precision;
    }
    x = xmin;
    y = 1./(x*tg(x)) - 1./(x*x) - V;
    ymin = y;
    x = xmax;
    y = 1./(x*tg(x)) - 1./(x*x) - V;
    ymax = y;
    if(ymin*ymax > 0) Error("R = %"LF" Can't construct the grid : No solution found", Rp);
    while(search) {
      x = (xmin+xmax)/2.;
      y = 1./(x*tg(x)) - 1./(x*x) - V;
      if(y*ymin < 0) {
        xmax = x;
        ymax = y;
      } else {
        xmin = x;
        ymin = y;
      }
      if(fabs(y)<precision) {
        x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
        y = 1./(x*tg(x)) - 1./(x*x) - V;
        search = OFF;
      }
    }
    Message("  Solution is kappa R  = %"LF" = pi + %"LE", error %"LE"\n", x, x-pi, y);
    kappa = x / Rp;
    Message("  Potential depth %"LE", momentum kappa = %"LE"\n", kappa*kappa, kappa);
  }
  else {
    Warning("  Unitarity limit, K Rp = pi\n");
    kappa = pi/Rp + precision;
  }

  Message("  Check ap/Rp = %"LE"\n", 1./3.-1./(kappa*Rp*kappa*Rp) + 1./(kappa*Rp*tg(kappa*Rp)));
  Message("  kf ap = %"LE"\n", ap*pow(3.*PI*PI*n, 1./3.));
  trial11_Vo = kappa*kappa;
  trial22_Vo = kappa*kappa;
  Message("  Potential depth Vo = %"LE"\n", trial11_Vo);
  if(ap<0) {
    Warning("  Negative p-wave scattering length\n");
    //ap = -ap;
  }
}

/************************** Construct Soft Sphere Height ************************/
DOUBLE ConstructSoftSphereHeight(DOUBLE Ro, DOUBLE a, DOUBLE mu) {
// mu is the reduced mass (for equal masses of mass 1 is 0.5)
  DOUBLE trial_Vo;

#ifdef TRIAL_3D
  DOUBLE x, xmin, xmax;
  DOUBLE y, ymin, ymax;
  DOUBLE V;
  DOUBLE precision = 1e-12;
  DOUBLE kappa;
  int search = ON;
  int repeat = ON;

  Message("Jastrow term: solution of two-body scattering problem\n");
  Message("  parameters: sphere size Ro = %"LE "\n", Ro);
  Message("  parameters: a = %lf scattering length\n", a);
  if(Ro<a) Error("Size of the soft sphere %"LE " is smaller than the scattering length %"LE " \n", Ro, a);

  // define kappa
  // Solve th x / x = 1 - a/R, E<V_0
  //       tg x / x = 1 - a/R, E>V_0
  //       x = kappa R
  V = 1. - a/Ro;
  xmin = 1e-5;
  xmax = 100;
  ymin = tanh(xmin)/xmin - V;
  ymax = tanh(xmax)/xmax - V;
  if(ymin*ymax > 0) Error("Ro = %%"LF" Can't construct the grid : No solution found", Ro);
  while(search) {
    x = (xmin+xmax)/2.;
    y = tanh(x)/x - V;
    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }
    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y = tanh(x)/x - V;
      search = OFF;
    }
  }

  kappa = x / Ro;
  trial_Vo = kappa*kappa/(2.*mu);
  Message("  Soft-sphere height Vo = %lf\n", trial_Vo);
#else // 2D system
  DOUBLE V,y,ymin,ymax,x,xmin,xmax;
  DOUBLE precision = 1e-8;
  int search = ON;
  DOUBLE kappa;

  //calculate strength of the potential
  // define kappa
  // x = kappa R
  Message("\n  Constructing soft disk potential with Ro = %"LF" and aA = %"LE"\n", Ro, a);

  // soft disk:   Solve Jo(x) / (x J1(x)) = ln(a/R)
  // square well: Solve Io(x) / (x I1(x)) = ln(R/a)
  V = Log(Ro/a);

  xmin = 100./V; // Io(x) / (x I1(x)) = 1/x, x -> inf
  xmax = 0.01*sqrt(2./V); //  Io(x) / (x I1(x)) = 2/x^2, x -> 0
  ymin = BesselI0(xmin)/(xmin*BesselI1(xmin)) - V;
  ymax = BesselI0(xmax)/(xmax*BesselI1(xmax)) - V;
  if(ymin*ymax > 0) Error("Ro = %"LF" Can't construct the grid : No solution found", Ro);
  while(search) {
    x = (xmin+xmax)/2.;
    y = BesselI0(x)/(x*BesselI1(x)) - V;
    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }
    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y = BesselI0(x)/(x*BesselI1(x)) - V;
      search = OFF;
    }
    //Message("x = %"LG" y = %"LG"\n", x, y);
  }
  Message("  Solution is kappa R = %"LF", error %"LE"\n", x, y);
  kappa = x / Ro;
  trial_Vo = kappa*kappa/(2.*mu);
  Message("  Soft-disk height Vo = %lf\n", trial_Vo);

  Message("  Check a = %"LE"\n", Ro*Exp(-BesselI0(kappa*Ro)/(kappa*Ro*BesselI1(kappa*Ro))));

  trial_Vo = kappa*kappa/(2.*mu);
#endif

  return trial_Vo;
}

/************************** Construct Grid Fermion ************************/
void ConstructGridBCS(void) {
  DOUBLE kappa, k;
  DOUBLE V,y,ymin,ymax,x,xmin,xmax;
  DOUBLE precision = PRECISION;
  int search = ON;
  DOUBLE detuning = PRECISION;
  FILE *out;
  static int first_time = ON;

  V = -1;
  kappa = sqrt(2.*m_mu*trial_Vo);

  // x^2 = Eb, - binding energy
  //xmin = detuning*pi/2.*0.1;
  //xmax = detuning*pi/2.*10.;
  xmin = 1e-10;
  xmax = 1e0;

  ymin = sqrt(-xmin*xmin+kappa*kappa)/tg(Ro*sqrt(-xmin*xmin+kappa*kappa))/xmin - V;
  ymax = sqrt(-xmax*xmax+kappa*kappa)/tg(Ro*sqrt(-xmax*xmax+kappa*kappa))/xmax - V;

  if(ymin*ymax > 0) Error("Can't construct the grid : No solution found");

  search = ON;
  while(search) {
    x = (xmin+xmax)/2.;
    y = sqrt(-x*x+kappa*kappa)/tg(Ro*sqrt(-x*x+kappa*kappa))/x - V;

    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }

    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y = sqrt(-x*x+kappa*kappa)/tg(Ro*sqrt(-x*x+kappa*kappa))/x - V;
      search = OFF;
    }
  }
  k = x;
  if(first_time) Message("  Solution k = %"LE", error %"LE" (k_approx = %"LE")\n", k, y, detuning*pi/2.);

  BCSsqE = k;
  BCSsqE2 = k*k;
  BCStrial_Kappa2 = kappa*kappa - k*k;
  BCStrial_Kappa = sqrt(BCStrial_Kappa2); // \tilde K

  // matching to 1
  if(first_time) Warning("  Rpar %"LG" [Lhalf] -> %"LG"\n", Rpar, Rpar*Lhalf);
  Rpar *= Lhalf;
  //Rpar = 10*a;

  //BCSBtrial = Rpar*Exp(k*Rpar);
  //Warning(" Check L/2 is effectively done inside the w.f.\n");
  BCSBtrial = 1.;
#ifdef L2_CHECK
  if(BCSBtrial == 1.) Error("  comment L/2 check and recompile");
#endif
  BCSCh2gamma = BCSBtrial*Exp(-BCSsqE*Ro)/Sin(BCStrial_Kappa*Ro);
  if(first_time) Message("  Binding energy %"LE", approx %"LE"\n", BCSsqE2, (detuning*pi/2.)*(detuning*pi/2.));
  BCSfp = BCSBtrial*Exp(-BCSsqE*Lhalf)/Lhalf*(- BCSsqE - 1./Lhalf);

#ifdef ZERO_ASYMPTOTIC
  if(first_time) Message("  Orbital has zero asymptotic f(r)=0, r>Rpar Lhalf\n");
  BCSf = 0.;
#else
  if(first_time) Message("  Orbital has non zero asymptotic f(r)>0, r>Rpar Lhalf\n");
  BCSf = BCSBtrial*Exp(-BCSsqE*Lhalf)/Lhalf;
#endif
  if(first_time) {
    out = fopen("Eb.dat", "w");
    fprintf(out, "%.15"LE, energy_unit*BCSsqE2);
    fclose(out);
  }

  energy_shift = BCSsqE2;
  if(first_time) Warning("  Binding energy %"LE" will be subtracted from the total energy !\n", energy_shift);
  first_time = OFF;
}

/************************** Construct Grid Fermion ************************/
void ConstructGridBCSdecay(void) {
  DOUBLE kappa, k;
  DOUBLE V,y,ymin,ymax,x,xmin,xmax;
  DOUBLE precision = PRECISION;
  int search = ON;
  DOUBLE detuning = PRECISION;
  FILE *out;
  static int first_time = ON;

  V = -1;
  kappa = sqrt(2.*m_mu*trial_Vo);

  Message("  Orbital: Grid BCS decay\n");

  xmin = 1e-10;
  xmax = 1e0;

  ymin = sqrt(-xmin*xmin+kappa*kappa)/tg(sqrt(-xmin*xmin+kappa*kappa))/xmin - V;
  ymax = sqrt(-xmax*xmax+kappa*kappa)/tg(sqrt(-xmax*xmax+kappa*kappa))/xmax - V;

  if(ymin*ymax > 0) Error("Can't construct the grid : No solution found");

  search = ON;
  while(search) {
    x = (xmin+xmax)/2.;
    y = sqrt(-x*x+kappa*kappa)/tg(sqrt(-x*x+kappa*kappa))/x - V;

    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }

    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y = sqrt(-x*x+kappa*kappa)/tg(sqrt(-x*x+kappa*kappa))/x - V;
      search = OFF;
    }
  }
  k = x;
  if(first_time) Message("  Solution k = %"LE", error %"LE" (k_approx = %"LE")\n", k, y, detuning*pi/2.);

  BCSsqE = k;
  BCSsqE2 = k*k;
  BCStrial_Kappa2 = kappa*kappa - k*k;
  BCStrial_Kappa = sqrt(BCStrial_Kappa2);

  // matching to 1
  //if(first_time) Warning("  Rpar %"LG" [Lhalf] -> %"LG"\n", Rpar, Rpar*Lhalf);
  //Rpar *= Lhalf;
  //Rpar = 10*a;
  if(first_time) Warning("  Rpar %"LG" [L/2] -> %"LG"\n", Rpar, Rpar*Lhalf);
  Rpar *= Lhalf;

  //BCSBtrial = Rpar*Exp(k*Rpar);
  //Warning(" Check L/2 is effectively done inside the w.f.\n");
  BCSBtrial = 1.;
#ifdef L2_CHECK
  if(BCSBtrial == 1.) Error("  comment L/2 check and recompile");
#endif
  BCSCh2gamma = BCSBtrial*Exp(-BCSsqE)/Sin(BCStrial_Kappa);
  if(first_time) Message("  Binding energy %"LE", approx %"LE"\n", BCSsqE2, (detuning*pi/2.)*(detuning*pi/2.));

#ifdef ZERO_ASYMPTOTIC
  if(first_time) Message("  Orbital has zero asymptotic f(r)=0, r>Rpar Lhalf\n");
  BCSf = 0.;
#else
  if(first_time) Message("  Orbital has non zero asymptotic f(r)>0, r>Rpar Lhalf\n");
#endif
  if(first_time) {
    out = fopen("Eb.dat", "w");
    fprintf(out, "%.15"LE, energy_unit*BCSsqE2);
    fclose(out);
  }

  // solve B(k+1/Rpar)/(Rpar(e^{kRpar}-B)) = b th(b(Rpar-L/2))
  BCStrial_a2 = -BCSBtrial*Exp(-k*Rpar)*(1.+k*Rpar)/(Rpar*Rpar*Bpar*(-Exp(-Bpar*Rpar)+Exp(Bpar*(Rpar-L))));
  BCStrial_a1 =  BCSBtrial*Exp(-k*Rpar)/Rpar - BCStrial_a2*(Exp(-Bpar*Rpar)+Exp(Bpar*(Rpar-L)));

  //BCSCh2gamma = (1.-BCSBtrial*Exp(-k*Rpar)/Rpar)/(Exp(-Bpar*Rpar)+Exp(Bpar*(Rpar-L)));
  //BCSCh2gamma = -(-BCSBtrial*Exp(-k*Rpar)/Rpar)/(Exp(-Bpar*Rpar)+Exp(Bpar*(Rpar-L)));
  BCSf = BCStrial_a1 + 2.*BCStrial_a2*Exp(-Bpar*Lhalf);
  //BCSfp = 0.;

  Message("\nTesting continuity of the orbital\n    f(R) ... ");
  if(fabs(orbitalExactF(Rpar-1e-10)-orbitalExactF(Rpar+1e-10))>1e-2)
    Error("f(R-0) = %"LF", f(R+0) = %"LF"\n", orbitalExactF(Rpar-1e-10),orbitalExactF(Rpar+1e-10));
  else
    Message("done\n    f'(R) ... ");

  if(fabs(orbitalExactFp(Rpar-1e-10)-orbitalExactFp(Rpar+1e-10))>1e-2)
    Error("f'(R-0) = %"LF", f(R+0) = %"LF"\n", orbitalExactFp(Rpar-1e-10),orbitalExactFp(Rpar+1e-10));
  else
    Message("done\n");

  energy_shift = BCSsqE2;
  if(first_time) Warning("  Binding energy %"LE" will be subtracted from the total energy !\n", energy_shift);
  first_time = OFF;
}

#ifdef ORBITAL_SQ_WELL_BOUND_STATE_DECAY
DOUBLE orbitalExactF(DOUBLE r) {
  if(r==0.) return BCSCh2gamma*BCStrial_Kappa;

  if(r<Ro)
    return BCSCh2gamma*Sin(BCStrial_Kappa*r)/r;
  else if(r<Rpar)
    return BCSBtrial*Exp(-BCSsqE*r)/r;
  else if(r<Lhalf)
    //return 1. - BCSCh2gamma*(Exp(-Bpar*r)+Exp(Bpar*(r-L)));
    return BCStrial_a1 + BCStrial_a2*(Exp(-Bpar*r)+Exp(Bpar*(r-L)));
  else
    return BCSf;
}

// Fp = f'
DOUBLE orbitalExactFp(DOUBLE r) {
  if(r<Ro)
    return BCSCh2gamma*Sin(BCStrial_Kappa*r)/r*(BCStrial_Kappa/tg(BCStrial_Kappa*r) - 1./r);
  else if(r<Rpar)
    return BCSBtrial*Exp(-BCSsqE*r)/r*(- BCSsqE - 1./r);
  else if(r<Lhalf)
    return BCStrial_a2*Bpar*(-Exp(-Bpar*r)+Exp(Bpar*(r-L)));
  else
    return 0.;
}

// Eloc = f" +2/r f' = u"/r
DOUBLE orbitalExactEloc(DOUBLE r) {
  DOUBLE e1,e2,fp,fpp;
  if(r<Ro)
    return -BCStrial_Kappa2*BCSCh2gamma*Sin(BCStrial_Kappa*r)/r;
  else if(r<Rpar)
    return BCSsqE2*BCSBtrial*Exp(-BCSsqE*r)/r;
  else if(r<Lhalf) {
    e1 = Exp(-Bpar*r);
    e2 = Exp(Bpar*(r-L));
    //f = BCStrial_a1 + BCStrial_a2*(e1+e2);
    fp = BCStrial_a2*Bpar*(-e1+e2); // / f;
    fpp = BCStrial_a2*Bpar*Bpar*(e1+e2);//  / f;

    return fpp + 2./r*fp;
  }
  else
    return 0.;
}
#endif

/************************** Construct Grid Fermion ************************/
#ifdef ORBITAL_EXPONENT_ALPHA
DOUBLE OEA_gamma, OEA_B;
#endif

void ConstructGridExponentAlpha(void) {
  static int first_time = ON;
#ifdef ORBITAL_EXPONENT_ALPHA
  DOUBLE r;

  OEA_gamma = Opar - (L*Opar*Opar)/(-1. + Exp(L*Opar) + L*Opar);

  r = L / 2.;
  OEA_B = (1. + OEA_gamma * r) * Exp(-Opar * r) + (1 + OEA_gamma * (L - r))*Exp(-Opar * (L - r));
#endif

  if(first_time) Warning("  orbital: ORBITAL_EXPONENT_ALPHA\n");
  first_time = OFF;
}

#ifdef ORBITAL_EXPONENT_ALPHA
DOUBLE orbitalExactF(DOUBLE r) {
  if(r<Lhalf)
    return (1. + OEA_gamma * r) * Exp(-Opar * r) + (1 + OEA_gamma * (L - r))*Exp(-Opar * (L - r)) - OEA_B;
  else
    return 0.;
}

// Fp = f'
DOUBLE orbitalExactFp(DOUBLE r) {
  if(r<Lhalf)
    return (Exp(2.*Opar*r)*(-OEA_gamma + Opar + OEA_gamma*Opar*(L - r)) - Exp(L*Opar)*(-OEA_gamma + Opar + OEA_gamma*Opar*r))/Exp(Opar*(L + r));
  else
    return 0.;
}

// Eloc = f" +2/r f' = u"/r
DOUBLE orbitalExactEloc(DOUBLE r) {
  if(r<Lhalf)
    return (Exp(2.*Opar*r)*(-2.*OEA_gamma + Opar*(2. + 2.*OEA_gamma*L + (-4.*OEA_gamma + Opar + OEA_gamma*Opar*(L - r))*r)) + Exp(L*Opar)*(2.*OEA_gamma + Opar*(-2. + Opar*r + OEA_gamma*r*(-4. + Opar*r))))/(Exp(Opar*(L + r))*r);
  else
    return 0.;
}
#endif

/************************** Construct Grid Fermion ************************/
void ConstructGridBCSSquareWellBCSSide(void) {
  DOUBLE kappa, k;
  DOUBLE V,y,ymin,ymax,x,xmin,xmax;
  DOUBLE precision = 1e-10;
  int search = ON;

  Warning("  Rpar %"LE" -> %"LE, Rpar, Rpar*Lhalf);
  Rpar *= Lhalf;

  // matching conditions
  V = 1;
  kappa = sqrt(2.*m_mu*trial_Vo);
  xmin = 1e-10;
  xmax = pi/(2*(Rpar-1))-precision;
  ymin = sqrt(xmin*xmin+kappa*kappa)/(xmin*tg(sqrt(xmin*xmin+kappa*kappa))*tg((Rpar-1)*xmin)) - V;
  ymax = sqrt(xmax*xmax+kappa*kappa)/(xmax*tg(sqrt(xmin*xmin+kappa*kappa))*tg((Rpar-1)*xmax)) - V;

  if(ymin*ymax > 0) Error("Can't construct the grid : No solution found");

  while(search) {
    x = (xmin+xmax)/2.;
    y = sqrt(x*x+kappa*kappa)/(x*tg(sqrt(xmin*xmin+kappa*kappa))*tg((Rpar-1)*x)) - V;

    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }

    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y = sqrt(x*x+kappa*kappa)/(x*tg(sqrt(xmin*xmin+kappa*kappa))*tg((Rpar-1)*x)) - V;
      search = OFF;
    }
  }
  k = x;

  Message("  Solution k = %"LE", error %"LE"\n", k, y);

  BCS2sqE = k;
  BCS2sqE2 = k*k;
  BCS2trial_Vo = kappa*kappa/(2.*m_mu);
  BCS2trialKappa2 = k*k + kappa*kappa;
  BCS2trialKappa = sqrt(BCS2trialKappa2);

  BCS2trial_delta = 0.5*pi-k*Rpar;
  BCS2Btrial = Lhalf/Sin(k*Rpar+BCS2trial_delta);
  BCS2Ch2gamma = BCS2Btrial*Sin(k+BCS2trial_delta)/Sin(BCS2trialKappa);

  Message(" Matching to one -> %"LF"", BCS2Btrial*Sin(k*Lhalf+BCS2trial_delta)/Lhalf);
}

/************************** Construct Grid Fermion ************************/
void ConstructGridBCSSquareWellZeroConst(void) {
  DOUBLE Ccheck;
  Message("  BCS square well zero energy + const\n");

  Warning("  Scattering length must be negative in order to use this w.f.\n");

  BCS2trialKappa = sqrt(2.*m_mu*trial_Vo);
  BCS2trialKappa2 = trial_Vo;

  Warning("  Changing Rpar %"LE" -> %"LE"\n", Rpar, Rpar*Lhalf);
  Rpar *= Lhalf;

  BCS2Btrial = 1./(1+a/Rpar*(1-1./(Rpar*Bpar)));
  BCS2Ch2gamma = BCS2Btrial*(1.+a)/Sin(BCS2trialKappa);
  BCS2Ctrial = (BCS2Btrial*a/(Rpar*Rpar*Bpar))*Exp(Bpar*Rpar);
  Ccheck = (BCS2Btrial*(1+a/Rpar)-1)*Exp(Bpar*Rpar);

  if(fabs((Ccheck-BCS2Ctrial)/Ccheck)>1e-6) Error(" Continuity check failed\n");
}

/************************** Construct Grid Fermion ************************/
void ConstructGridBCSSquareWell(void) {
  DOUBLE kappa, k;
  DOUBLE V,y,ymin,ymax,x,xmin,xmax;
  DOUBLE precision = 1e-10;
  DOUBLE BCS2trial_delta_check;
  int search = ON;

  // matching conditions
  V = Lhalf - 1;
  kappa = sqrt(2.*m_mu*trial_Vo);

  xmin = 1e-4;
  xmax = 4;
  ymin = (atan(xmin*Lhalf) - atan(xmin/sqrt(xmin*xmin+kappa*kappa)*tan(sqrt(xmin*xmin+kappa*kappa))))/xmin - V;
  ymax = (atan(xmax*Lhalf) - atan(xmax/sqrt(xmax*xmax+kappa*kappa)*tan(sqrt(xmax*xmax+kappa*kappa))))/xmax - V;

  if(ymin*ymax > 0) Error("Can't construct the grid : No solution found");

  while(search) {
    x = (xmin+xmax)/2.;
    y = (atan(x*Lhalf) - atan(x/sqrt(x*x+kappa*kappa)*tan(sqrt(x*x+kappa*kappa))))/x - V;

    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }

    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y = (atan(x*Lhalf) - atan(x/sqrt(x*x+kappa*kappa)*tan(sqrt(x*x+kappa*kappa))))/x - V;
      search = OFF;
    }
  }
  k = x;

  Message("  Solution k = %"LE", error %"LE"\n", k, y);

  BCS2sqE = k;
  BCS2sqE2 = k*k;
  BCS2trial_Vo = kappa*kappa/(2.*m_mu);
  BCS2trialKappa2 = k*k + kappa*kappa;
  BCS2trialKappa = sqrt(BCS2trialKappa2);

  BCS2trial_delta = atan(k*Lhalf) - k*Lhalf;
  BCS2trial_delta_check = atan(k/BCS2trialKappa*tan(BCS2trialKappa)) - k;
  if(fabs((BCS2trial_delta_check-BCS2trial_delta)/BCS2trial_delta)>1e-8) Error("  Invalid solution\n");

  BCS2Btrial = Lhalf/Sin(k*Lhalf+BCS2trial_delta);
  BCS2Ch2gamma = BCS2Btrial*Sin(k+BCS2trial_delta)/Sin(BCS2trialKappa);

  Message(" Matching to one -> %"LF"", BCS2Btrial*Sin(k*Lhalf+BCS2trial_delta)/Lhalf);
}

/************************** Construct Grid BCS pseudopot ******************/
void ConstructGridBCSPseudopotential(void) {
  DOUBLE V,a_1;
  DOUBLE precision = 1e-12;
  int search = ON;
  DOUBLE x, xmin, xmax, y, ymin, ymax;
  Message("  Pseudopotential free-state wavefunction\n");

  //a_1 = Lhalf/a;
  a_1 = 0.;
  Warning("  Unitary regime\n");

  // (arctg 1/ka + arctg 1/kL) / kL = -1
  V = -1.;

  xmin = pi/2.+precision;
  xmax = pi;

  ymin = (arctg(a_1/xmin)+arctg(1./xmin)-pi)/xmin - V;
  ymax = (arctg(a_1/xmax)+arctg(1./xmax)-pi)/xmax - V;
  if(ymin*ymax > 0) Error("Can't construct the grid : No solution found");
  while(search) {
    x = (xmin+xmax)/2.;
    y = (arctg(a_1/x)+arctg(1/x)-pi)/x - V;
    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }
    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y = (arctg(a_1/x)+arctg(1/x)-pi)/x - V;
      search = OFF;
    }
  }

  Message("  Solution is kappa k Lhalf %"LE", error %"LE"\n", x, y);
  BCSsqE = x / Lhalf;
  BCSsqE2 = BCSsqE*BCSsqE;
  BCSdelta = arctg(a_1/Lhalf/BCSsqE);
  BCSCh2gamma = Lhalf / Cos(BCSsqE*Lhalf+BCSdelta);
}

/************************** Orbital ***************************************/
#ifdef FERMIONS

#ifdef INTERPOLATE_SPLINE_ORBITAL
DOUBLE orbitalExactF(DOUBLE r) {
  int i;
  DOUBLE dx;

  if(r <= BCS.min) return BCS.f[0];

  i = (int) ((r-BCS.min)/BCS.step + 0.5);
  if(i > BCS.size-3) return 1.;

  dx = r - BCS.x[i];

  // linear interpolation 
  return BCS.f[i] + (BCS.f[i+1]-BCS.f[i]) * BCS.I_step * dx;
}

DOUBLE orbitalExactFp(DOUBLE r) {
  int i;
  DOUBLE dx;

  if(r <= BCS.min) return BCS.fp[0];

  i = (int) ((r-BCS.min)/BCS.step + 0.5);
  if(i > BCS.size-3) return 0.;

  dx = r - BCS.x[i];

  // linear interpolation 
  return BCS.fp[i] + (BCS.fp[i+1]-BCS.fp[i]) * BCS.I_step * dx;
}

DOUBLE orbitalExactEloc(DOUBLE r) {
  int i;
  DOUBLE dx;

  if(r <= BCS.min) return BCS.E[0];

  i = (int) ((r-BCS.min)/BCS.step + 0.5);
  if(i > BCS.size-3) return 0.;

  dx = r - BCS.x[i];

  // linear interpolation 
  return BCS.E[i] + (BCS.E[i+1]-BCS.E[i]) * BCS.I_step * dx;
}
#endif

#ifdef ORBITAL_SQ_WELL_BOUND_STATE
DOUBLE orbitalExactF(DOUBLE r) {
  if(r==0.) return BCSCh2gamma*BCStrial_Kappa;

#ifdef BC_ABSENT
  if(r<Ro)
    return BCSCh2gamma*Sin(BCStrial_Kappa*r)/r;
  else
    return BCSBtrial*Exp(-BCSsqE*r)/r;
#else
  if(r<Ro)
    return BCSCh2gamma*Sin(BCStrial_Kappa*r)/r;
#ifdef L2_ORBITAL_CONST
  else if(r<Lhalf)
    return BCSBtrial*Exp(-BCSsqE*r)/r;
  else
    return BCSf;
#else
  else
    return BCSBtrial*Exp(-BCSsqE*r)/r;
#endif
#endif
}

// Fp = f'
DOUBLE orbitalExactFp(DOUBLE r) {
#ifdef BC_ABSENT
  if(r<Ro)
    return BCSCh2gamma*Sin(BCStrial_Kappa*r)/r*(BCStrial_Kappa/tg(BCStrial_Kappa*r) - 1./r);
  else
    return BCSBtrial*Exp(-BCSsqE*r)/r*(- BCSsqE - 1./r);
#else
  if(r<Ro)
    return BCSCh2gamma*Sin(BCStrial_Kappa*r)/r*(BCStrial_Kappa/tg(BCStrial_Kappa*r) - 1./r);
#ifdef L2_ORBITAL_CONST
  else if(r<Lhalf)
    return BCSBtrial*Exp(-BCSsqE*r)/r*(- BCSsqE - 1./r);
  else
    return BCSfp;
#else
  else
    return BCSBtrial*Exp(-BCSsqE*r)/r*(- BCSsqE - 1./r);
#endif
#endif
}

// Eloc = f" + 2/r f' = u"/r
DOUBLE orbitalExactEloc(DOUBLE r) {
#ifdef BC_ABSENT
  if(r<Ro)
    return -BCStrial_Kappa2*BCSCh2gamma*Sin(BCStrial_Kappa*r)/r;
  else
    return BCSsqE2*BCSBtrial*Exp(-BCSsqE*r)/r;
#else
  if(r<Ro)
    return -BCStrial_Kappa2*BCSCh2gamma*Sin(BCStrial_Kappa*r)/r;
#ifdef L2_ORBITAL_CONST
  else if(r<Lhalf)
    return BCSsqE2*BCSBtrial*Exp(-BCSsqE*r)/r;
  else
    return 0.;
#else
  else
    return BCSsqE2*BCSBtrial*Exp(-BCSsqE*r)/r;
#endif
#endif
}
#endif

#ifdef ORBITAL_SQ_WELL_FREE_STATE
DOUBLE orbitalExactF(DOUBLE r) {
  if(r<Ro)
    return BCS2Ch2gamma*Sin(BCS2trialKappa*r)/r;
  else
    return BCS2Btrial*Sin(BCS2sqE*r+BCS2trial_delta)/r;
}

// Fp = f'
DOUBLE orbitalExactFp(DOUBLE r) {
  if(r<Ro)
    return (BCS2trialKappa/tg(BCS2trialKappa*r) - 1./r)*BCS2Ch2gamma*Sin(BCS2trialKappa*r)/r;
  else
    return (BCS2sqE/tg(BCS2sqE*r+BCS2trial_delta) - 1./r)*BCS2Btrial*Sin(BCS2sqE*r+BCS2trial_delta)/r;
}

// Eloc = f" +2/r f' = u"/r
DOUBLE orbitalExactEloc(DOUBLE r) {
  if(r<Ro)
    return -BCS2trialKappa2*BCS2Ch2gamma*Sin(BCS2trialKappa*r)/r;
  else
    return -BCS2sqE2*BCS2Btrial*Sin(BCS2sqE*r+BCS2trial_delta)/r;
}
#endif

#ifdef ORBITAL_SQ_WELL_MIXTURE
DOUBLE orbitalExactF(DOUBLE r) {
  DOUBLE FBS, Ffree;

  if(r<Ro)
    FBS = fabs(BCSCh2gamma*Sin(BCStrial_Kappa*r)/r);
  else
    FBS = BCSBtrial*Exp(-BCSsqE*r)/r;

  if(r<Ro)
    Ffree = BCS2Ch2gamma*Sin(BCS2trialKappa*r)/r;
  else
    Ffree = BCS2Btrial*Sin(BCS2sqE*r+BCS2trial_delta)/r;

  return Bpar*FBS + (1-Bpar)*Ffree;
}

// Fp = f'
DOUBLE orbitalExactFp(DOUBLE r) {
  DOUBLE FpBS, FpFree;

  if(r<Ro)
    FpBS = fabs(BCSCh2gamma*Sin(BCStrial_Kappa*r)/r)*(BCStrial_Kappa/tg(BCStrial_Kappa*r) - 1./r);
  else
    FpBS = BCSBtrial*Exp(-BCSsqE*r)/r*(- BCSsqE - 1./r);

  if(r<Ro)
    FpFree = (BCS2trialKappa/tg(BCS2trialKappa*r) - 1./r)*BCS2Ch2gamma*Sin(BCS2trialKappa*r)/r;
  else
    FpFree = (BCS2sqE/tg(BCS2sqE*r+BCS2trial_delta) - 1./r)*BCS2Btrial*Sin(BCS2sqE*r+BCS2trial_delta)/r;

  return Bpar*FpBS + (1-Bpar)*FpFree;
}

// Eloc = f" +2/r f' = u"/r
DOUBLE orbitalExactEloc(DOUBLE r) {
  DOUBLE EBS, EFree;

  if(r<Ro)
    EBS = -BCStrial_Kappa2*BCSCh2gamma*Sin(BCStrial_Kappa*r)/r;
  else
    EBS = BCSsqE2*BCSBtrial*Exp(-BCSsqE*r)/r;

  if(r<Ro)
    EFree = BCS2trialKappa2*BCS2Ch2gamma*Sin(BCS2trialKappa*r)/r;
  else
    EFree = BCS2sqE2*BCS2Btrial*Sin(BCS2sqE*r+BCS2trial_delta)/r;

  return Bpar*EBS + (1-Bpar)*EFree;
}
#endif

#ifdef ORBITAL_PSEUDOPOTENTIAL_BOUND_STATE// Pseudopotential Bound State
DOUBLE orbitalExactF(DOUBLE r) {
  return Lhalf/r*Exp((Lhalf-r)/a);
}

// Fp = f'
DOUBLE orbitalExactFp(DOUBLE r) {
  return -orbitalExactF(r)*(1./a+1./r);
}

// Eloc = f" +2/r f' = u"/r
DOUBLE orbitalExactEloc(DOUBLE r) {
  return orbitalExactF(r)/(a*a);
}
#endif

#ifdef ORBITAL_PSEUDOPOTENTIAL_BOUND_STATE_UNITARY// Pseudopotential Bound State
DOUBLE orbitalExactF(DOUBLE r) {
  return Lhalf/r;
}

// Fp = f'
DOUBLE orbitalExactFp(DOUBLE r) {
  return -orbitalExactF(r)/r;
}

// Eloc = f" +2/r f' = u"/r
DOUBLE orbitalExactEloc(DOUBLE r) {
  return 0;
}
#endif

#ifdef ORBITAL_PSEUDOPOTENTIAL// Pseudopotential Free State
DOUBLE orbitalExactF(DOUBLE r) {
  return BCSCh2gamma*Cos(BCSsqE*r+BCSdelta)/r;
}

// Fp = f'
DOUBLE orbitalExactFp(DOUBLE r) {
  return -orbitalExactF(r)*(BCSsqE*tg(BCSsqE*r+BCSdelta)+1./r);
}

// Eloc = f" +2/r f' = u"/r
DOUBLE orbitalExactEloc(DOUBLE r) {
  return -BCSsqE2*orbitalExactF(r);
}
#endif

#ifdef ORBITAL_PSEUDOPOTENTIAL_MIXTURE // Pseudopotential Mixture State: Bpar |BS> + (1-Bpar) |FS>
DOUBLE orbitalExactF(DOUBLE r) {
  return Bpar*Lhalf/r*Exp((Lhalf-r)/a) + (1-Bpar)* BCSCh2gamma*Cos(BCSsqE*r+BCSdelta)/r;
}

// Fp = f'
DOUBLE orbitalExactFp(DOUBLE r) {
  return Bpar*(-Lhalf/r*Exp((Lhalf-r)/a)*(1./a+1./r)) + (1-Bpar)* (-BCSCh2gamma)*Cos(BCSsqE*r+BCSdelta)/r*(BCSsqE*tg(BCSsqE*r+BCSdelta)+1./r);
}

// Eloc = f" +2/r f' = u"/r
DOUBLE orbitalExactEloc(DOUBLE r) {
  return Bpar*Lhalf/r*Exp((Lhalf-r)/a)/(a*a) + (1-Bpar)* (-BCSsqE2)*BCSCh2gamma*Cos(BCSsqE*r+BCSdelta)/r;
}
#endif

#else // BOSONS
/*
DOUBLE orbitalExactF(DOUBLE r) {
  return 1;
}

// Fp = f'
DOUBLE orbitalExactFp(DOUBLE r) {
  return 0;
}

// Eloc = f" +2/r f' = u"/r
DOUBLE orbitalExactEloc(DOUBLE r) {
  return 0;
}*/
#endif

#ifdef ORBITAL_SQ_WELL_FREE_STATE_BCS_SIDE
DOUBLE orbitalExactF(DOUBLE r) {
  if(r<Ro)
    return BCS2Ch2gamma*Sin(BCS2trialKappa*r)/r;
  else if(r<Rpar)
    return BCS2Btrial*Sin(BCS2sqE*r+BCS2trial_delta)/r;
  else
    return Lhalf/(r);
}

// Fp = f'
DOUBLE orbitalExactFp(DOUBLE r) {
  if(r<Ro)
    return (BCS2trialKappa/tg(BCS2trialKappa*r) - 1./r)*BCS2Ch2gamma*Sin(BCS2trialKappa*r)/r;
  else if(r<Rpar)
    return (BCS2sqE/tg(BCS2sqE*r+BCS2trial_delta) - 1./r)*BCS2Btrial*Sin(BCS2sqE*r+BCS2trial_delta)/r;
  else
    return -Lhalf/(r*r);
}

// Eloc = f" +2/r f' = u"/r
DOUBLE orbitalExactEloc(DOUBLE r) {
  if(r<Ro)
    return -BCS2trialKappa2*BCS2Ch2gamma*Sin(BCS2trialKappa*r)/r;
  else if(r<Rpar)
    return -BCS2sqE2*BCS2Btrial*Sin(BCS2sqE*r+BCS2trial_delta)/r;
  else
    return 0;
}
#endif

#ifdef ORBITAL_SQ_WELL_ZERO_CONST
/************************** Construct Grid Zero Energy ********************/
void ConstructGridBCSSquareWellZeroEnergy(void) {
  Message("  BCS square well zero energy\n");

  Warning("  Scattering length must be negative in order to use this w.f.\n");

  BCS2trialKappa = sqrt(2.*m_mu*trial_Vo);
  BCS2trialKappa2 = 2.*m_mu*trial_Vo;

  BCS2Btrial = 1./(1. + a/Lhalf);
  BCS2Ch2gamma = BCS2Btrial*(1.+a)/Sin(BCS2trialKappa);
}

DOUBLE orbitalExactF(DOUBLE r) {
  if(r<Ro)
    return BCS2Ch2gamma*Sin(BCS2trialKappa*r)/r;
  else if(r<Rpar)
    return BCS2Btrial*(1.+a/r);
  else
    return 1. + BCS2Ctrial*Exp(-Bpar*r);
}

// Fp = f'
DOUBLE orbitalExactFp(DOUBLE r) {
  if(r<Ro)
    return (BCS2trialKappa/tg(BCS2trialKappa*r) - 1./r)*BCS2Ch2gamma*Sin(BCS2trialKappa*r)/r;
  else if(r<Rpar)
    return -BCS2Btrial*a/(r*r);
  else
    return -BCS2Ctrial*Bpar*Exp(-Bpar*r);
}

// Eloc = f" +2/r f' = u"/r
DOUBLE orbitalExactEloc(DOUBLE r) {
  if(r<Ro)
    return -BCS2trialKappa2*BCS2Ch2gamma*Sin(BCS2trialKappa*r)/r;
  else if(r<Rpar)
    return 0;
  else
    return Bpar*(Bpar-2./r)*BCS2Ctrial*Exp(-Bpar*r);
}
#endif

/************************** Construct Grid Zero Energy ********************/
void ConstructGridBCSIdealGas(void) {
  static int first_time = ON;
  DOUBLE precision = 1e-12;
  int search = ON;
  DOUBLE x, xmin, xmax, y, ymin, ymax;

  if(first_time) {
    BCS2trialKappa = Apar*pow(3*PI*PI*n, 1./3.);
    //BCS2trialKappa = 0.028523221424532872;
    BCS2Etrial = Bpar;
    first_time = OFF;
    Message("  BCS square well zero energy (ideal gas) * Exp(-D r^E)\n");

    Warning("  Scattering length must be negative in order to use this w.f.\n");
  }
  BCS2trialKappa2 = BCS2trialKappa*BCS2trialKappa;

  xmin = -1e2;
  xmax = 1e2;
  x = xmin;
  ymin = (3*(-(BCS2trialKappa*Lhalf*(-3 + (-3 + BCS2Etrial)*power(Lhalf,BCS2Etrial)*x)*Cos(BCS2trialKappa*Lhalf)) + (-3 + (-3 + BCS2Etrial)*power(Lhalf,BCS2Etrial)*x + power(BCS2trialKappa,2)*power(Lhalf,2)*(1 + power(Lhalf,BCS2Etrial)*x))*Sin(BCS2trialKappa*Lhalf)))/(BCS2trialKappa*BCS2trialKappa2*power(Lhalf,4));
  x = xmax;
  ymax = (3*(-(BCS2trialKappa*Lhalf*(-3 + (-3 + BCS2Etrial)*power(Lhalf,BCS2Etrial)*x)*Cos(BCS2trialKappa*Lhalf)) + (-3 + (-3 + BCS2Etrial)*power(Lhalf,BCS2Etrial)*x + power(BCS2trialKappa,2)*power(Lhalf,2)*(1 + power(Lhalf,BCS2Etrial)*x))*Sin(BCS2trialKappa*Lhalf)))/(BCS2trialKappa*BCS2trialKappa2*power(Lhalf,4));
  if(ymin*ymax >= 0) Error("Can't construct the grid : No solution found");
  while(search) {
    x = (xmin+xmax)/2.;
    y = (3*(-(BCS2trialKappa*Lhalf*(-3 + (-3 + BCS2Etrial)*power(Lhalf,BCS2Etrial)*x)*Cos(BCS2trialKappa*Lhalf)) + (-3 + (-3 + BCS2Etrial)*power(Lhalf,BCS2Etrial)*x + power(BCS2trialKappa,2)*power(Lhalf,2)*(1 + power(Lhalf,BCS2Etrial)*x))*Sin(BCS2trialKappa*Lhalf)))/(BCS2trialKappa*BCS2trialKappa2*power(Lhalf,4));
    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }
    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y = (3*(-(BCS2trialKappa*Lhalf*(-3 + (-3 + BCS2Etrial)*power(Lhalf,BCS2Etrial)*x)*Cos(BCS2trialKappa*Lhalf)) + (-3 + (-3 + BCS2Etrial)*power(Lhalf,BCS2Etrial)*x + power(BCS2trialKappa,2)*power(Lhalf,2)*(1 + power(Lhalf,BCS2Etrial)*x))*Sin(BCS2trialKappa*Lhalf)))/(BCS2trialKappa*BCS2trialKappa2*power(Lhalf,4));
      search = OFF;
    }
  }
  BCS2Dtrial = x;

  Message("  Solution is D = %"LE", error %"LE"\n", x, y);
}

#ifdef ORBITAL_IG
DOUBLE orbitalExactF(DOUBLE r) {
  if(r>Lhalf) r = Lhalf;
  return (3*(1 + BCS2Dtrial*power(r,BCS2Etrial))*(-Cos(BCS2trialKappa*r) + Sin(BCS2trialKappa*r)/(BCS2trialKappa*r)))/(BCS2trialKappa2*power(r,2));
}

// Fp = f'
DOUBLE orbitalExactFp(DOUBLE r) {
  if(r>Lhalf) return 0;
  return (3*(-(BCS2trialKappa*r*(-3 + BCS2Dtrial*(-3 + BCS2Etrial)*power(r,BCS2Etrial))*Cos(BCS2trialKappa*r)) + (-3 + BCS2Dtrial*(-3 + BCS2Etrial)*power(r,BCS2Etrial) + power(BCS2trialKappa,2)*power(r,2)*(1 + BCS2Dtrial*power(r,BCS2Etrial)))*Sin(BCS2trialKappa*r)))/(BCS2trialKappa*BCS2trialKappa2*power(r,4));
}

// Eloc = f" +2/r f' = u"/r
DOUBLE orbitalExactEloc(DOUBLE r) {
  if(r>Lhalf) return 0;
  return (3*(BCS2trialKappa*r*(-6 - BCS2Dtrial*(6 - 5*BCS2Etrial + power(BCS2Etrial,2))*power(r,BCS2Etrial) + power(BCS2trialKappa,2)*power(r,2)*(1 + BCS2Dtrial*power(r,BCS2Etrial)))*Cos(BCS2trialKappa*r) + (6 + BCS2Dtrial*(6 - 5*BCS2Etrial + power(BCS2Etrial,2))*power(r,BCS2Etrial) + power(BCS2trialKappa,2)*power(r,2)*(-3 + BCS2Dtrial*(-3 + 2*BCS2Etrial)*power(r,BCS2Etrial)))*Sin(BCS2trialKappa*r)))/(BCS2trialKappa*BCS2trialKappa2*power(r,5));
}
#endif

/************************** Construct Grid Zero Energy ********************/
void ConstructGridDecay(void) {
  Message("  S-wave factor corresponds to a smooth decay\n");
  //Warning("  Automatically adjust Cpar %"LF" -> %"LF"\n", Cpar, Cpar/L);
  //Cpar = Cpar/L;
  BCSasymptValue = Apar*(Exp(-Cpar *Lhalf)+Exp(-Cpar *(L-Lhalf)));
}

#ifdef ORBITAL_DECAY
DOUBLE orbitalExactF(DOUBLE r) {
  if(r<Lhalf)
    return Dpar+Apar*(Exp(-Cpar*r)+Exp(Cpar*(r-L)));
  else
    return Dpar+BCSasymptValue;
}

// Fp = f'
DOUBLE orbitalExactFp(DOUBLE r) {
//f'/f = -Cpar*tanh(Cpar*(Lhalf-r));
  if(r<Lhalf)
    return Apar*Cpar*(-Exp(-Cpar*r)+Exp(Cpar*(r-L)));
  else
    return 0;
}

// Eloc = f" +2/r f' = u"/r
DOUBLE orbitalExactEloc(DOUBLE r) {
// Eloc/f =  Cpar*(-Cpar + 2/r*tanh(Cpar*(Lhalf-r)));
  if(r<Lhalf)
    return Apar*Cpar*(Exp(-Cpar*r)*(Cpar-2/r)+Exp(Cpar*(r-L))*(Cpar+2/r));
  else
    return 0;
}
#endif

/*************************** Fermions Absent *******************************/
#ifdef ORBITAL_ABSENT
DOUBLE orbitalExactF(DOUBLE r) {
#ifdef ORBITAL_3BODY
  return 1;
#else
  return 0;
#endif
}

DOUBLE orbitalExactFp(DOUBLE r) {
  return 0;
}
DOUBLE orbitalExactEloc(DOUBLE r) {
  return 0;
}
#endif

/************************** orbital F, Fp, Eloc ***************************/
DOUBLE orbitalGridF(DOUBLE r) {
  int i;

  if(r<=Gorbital.min) return orbitalExactF(r);

  i = (int) ((r-Gorbital.min)/Gorbital.step + 0.5);
  if(i >= Gorbital.size-1) return orbitalExactF(r);

  return Gorbital.lnf[i] + (Gorbital.lnf[i+1] - Gorbital.lnf[i]) * Gorbital.I_step * (r - Gorbital.x[i]);
}

DOUBLE orbitalGridFp(DOUBLE r) {
  int i;

  if(r<=Gorbital.min) return orbitalExactFp(r);

  i = (int) ((r-Gorbital.min)/Gorbital.step + 0.5);
  if(i >= Gorbital.size-1) return orbitalExactFp(r);

  return Gorbital.fp[i] + (Gorbital.fp[i+1] - Gorbital.fp[i]) * Gorbital.I_step * (r - Gorbital.x[i]);
}

DOUBLE orbitalGridEloc(DOUBLE r) {
  int i;

  if(r<=Gorbital.min) return orbitalExactEloc(r);

  i = (int) ((r-Gorbital.min)/Gorbital.step + 0.5);
  if(i >= Gorbital.size-1) return orbitalExactEloc(r);

  return Gorbital.E[i] + (Gorbital.E[i+1] - Gorbital.E[i]) * Gorbital.I_step * (r - Gorbital.x[i]);
}

/************************** Construct Grid Fermion ************************/
void ConstructGridFermion(void) {
  int nE, nx, ny, nz,nzm,nym,nxm;
  int go = ON;
  int alpha = 0;
  static int first_time = ON;

  // initialize
  for(alpha=0; alpha<ORBITALS_MAX && first_time; alpha++) {
    orbital_numbers_IFG[alpha][0] = 0;
    orbital_numbers_IFG[alpha][1] = 0;
    orbital_numbers_IFG[alpha][2] = 0;
    orbital_weight[alpha] = 0;

    orbital_numbers_unpaired[alpha][0] = 0;
    orbital_numbers_unpaired[alpha][1] = 0;
    orbital_numbers_unpaired[alpha][2] = 0;
  }
  if(first_time) alpha = 0;

#ifdef BC_ABSENT
  orbital_weight[0] = 1; // needed for trapped case
#endif

  // fill orbital numbers
  if(boundary == THREE_BOUNDARY_CONDITIONS) {
    for(nE=0; nE<=38 && go && first_time; nE++) {
      for(nzm=0; nzm<=nE && go; nzm++) {
        if(nzm%2==0)
          nz = -nzm/2;
        else
          nz = (nzm+1)/2;
        for(nym=0; nym<=nE && go; nym++) {
          if(nym%2==0)
            ny = -nym/2;
          else
            ny = (nym+1)/2;
          for(nx=0; nx<=nE && go; nx++) {
            if(nx*nx + ny*ny + nz*nz == nE && (nx>0 || nx==0 && (ny>0 || ny==0 && nz>=0))) {
              orbital_numbers_IFG[alpha][0] = nx;
              orbital_numbers_IFG[alpha][1] = ny;
              orbital_numbers_IFG[alpha][2] = nz;
              alpha++;
            }
          }
        }
      }
    }
  }

  if(boundary == TWO_BOUNDARY_CONDITIONS) {
    for(nE=0; nE<=38 && go && first_time; nE++) {
      for(nym=0; nym<=2*nE && go; nym++) {
        if(nym%2==0)
          ny = -nym/2;
        else
          ny = (nym+1)/2;
        for(nxm=0; nxm<=2*nE && go; nxm++) {
          if(nxm%2==0) // to print all shells
            nx = -nxm/2;
          else
            nx = (nxm+1)/2;
          //if(nx*nx + ny*ny == nE) {
          if(nx*nx + ny*ny == nE && (nx>0 || nx==0 && (ny>0 || ny==0))) {
            orbital_numbers_IFG[alpha][0] = nx;
            orbital_numbers_IFG[alpha][1] = ny;
            orbital_numbers_IFG[alpha][2] = 0;
            alpha++;
            //Message("%2i) nE = %2i (%2i,%2i)\n", alpha, nE, nx, ny);
          }
        }
      }
    }
  }

  if(boundary == BILAYER_BOUNDARY_CONDITIONS) {
    for(nE=0; nE<=38 && go && first_time; nE++) {
      for(nym=0; nym<=nE && go; nym++) {
        if(nym%2==0)
          ny = -nym/2;
        else
          ny = (nym+1)/2;
        for(nx=0; nx<=nE && go; nx++) {
          if(nx*nx + ny*ny == nE && (nx>0 || nx==0 && (ny>0 || ny==0 && nz>=0))) {
            orbital_numbers_IFG[alpha][0] = nx;
            orbital_numbers_IFG[alpha][1] = ny;
            orbital_numbers_IFG[alpha][2] = 0;
            alpha++;
          }
        }
      }
    }
  }

  //orbital_N = (Ndens+1)/2;
  //Message("  Number of filled orbitals of IFG with %i particles is:  orbital_N =  %i\n", Ndens, orbital_N);

  // Carlson, N=38
  //Warning("  Setting custom orbitals (Carlson, N=38)\n");
  /*orbital_numbers_IFG[0][0] = 0; // const
  orbital_numbers_IFG[0][1] = 0;
  orbital_numbers_IFG[0][2] = 0;

  orbital_numbers_IFG[1][0] = 1; // cos x
  orbital_numbers_IFG[1][1] = 0;
  orbital_numbers_IFG[1][2] = 0;
  orbital_numbers_IFG[2][0] = 0; // cos y
  orbital_numbers_IFG[2][1] = 1;
  orbital_numbers_IFG[2][2] = 0;
  orbital_numbers_IFG[3][0] = 0; // cos z
  orbital_numbers_IFG[3][1] = 0;
  orbital_numbers_IFG[3][2] = 1;

  //  phi += 2*orbital_weight[alpha]*Cos(k0*(orbital_numbers_IFG[alpha][0]*x+orbital_numbers_IFG[alpha][1]*y+orbital_numbers_IFG[alpha][2]*z));
  //+ 2*alpha2*(Cos(2*Pi*(x - y)) + Cos(2*Pi*(x + y)) + Cos(2*Pi*(x - z)) + Cos(2*Pi*(y - z)) + Cos(2*Pi*(x + z)) + Cos(2*Pi*(y + z)))
  orbital_numbers_IFG[4][0] = 1; // cos (x+y)
  orbital_numbers_IFG[4][1] = 1;
  orbital_numbers_IFG[4][2] = 0;
  orbital_numbers_IFG[5][0] = 1; // cos (x-y)
  orbital_numbers_IFG[5][1] = -1;
  orbital_numbers_IFG[5][2] = 0;
  orbital_numbers_IFG[6][0] = 1; // cos (x+z)
  orbital_numbers_IFG[6][1] = 0;
  orbital_numbers_IFG[6][2] = 1;
  orbital_numbers_IFG[7][0] = 1; // cos (x-z)
  orbital_numbers_IFG[7][1] = 0;
  orbital_numbers_IFG[7][2] = -1;
  orbital_numbers_IFG[8][0] = 0; // cos (y+z)
  orbital_numbers_IFG[8][1] = 1;
  orbital_numbers_IFG[8][2] = 1;
  orbital_numbers_IFG[9][0] = 0; // cos (y-z)
  orbital_numbers_IFG[9][1] = 1;
  orbital_numbers_IFG[9][2] = -1;

  orbital_numbers_IFG[10][0] = 1; // cos (x+y+z)
  orbital_numbers_IFG[10][1] = 1;
  orbital_numbers_IFG[10][2] = 1;
  orbital_numbers_IFG[11][0] = 1; // cos (x+y-z)
  orbital_numbers_IFG[11][1] = 1;
  orbital_numbers_IFG[11][2] = -1;
  orbital_numbers_IFG[12][0] = 1; // cos (x-y+z)
  orbital_numbers_IFG[12][1] = -1;
  orbital_numbers_IFG[12][2] = 1;
  orbital_numbers_IFG[13][0] = 1; // cos (x-y-z)
  orbital_numbers_IFG[13][1] = -1;
  orbital_numbers_IFG[13][2] = -1;

  orbital_numbers_IFG[14][0] = 2; // cos 2x
  orbital_numbers_IFG[14][1] = 0;
  orbital_numbers_IFG[14][2] = 0;
  orbital_numbers_IFG[15][0] = 0; // cos 2y
  orbital_numbers_IFG[15][1] = 2;
  orbital_numbers_IFG[15][2] = 0;
  orbital_numbers_IFG[16][0] = 0; // cos 2z
  orbital_numbers_IFG[16][1] = 0;
  orbital_numbers_IFG[16][2] = 2;*/

//   alpha0 - 0
// + 2*alpha1*(Cos(2*Pi*x) + Cos(2*Pi*y) + Cos(2*Pi*z)) - 1,2,3
//   4*alpha2*(Cos(2*Pi*x)*Cos(2*Pi*y)+Cos(2*Pi*x)*Cos(2*Pi*z)+Cos(2*Pi*y)*Cos(2*Pi*z)) - 4,5,6,7,8,9
//   2*alpha3*(Cos(2*pi*(x+y+z)) + Cos(2*pi*(x+y-z)) + Cos(2*pi*(x-y+z)) + Cos(2*pi*(x-y-z))) 10,11,12,13
// + 2*alpha4*(Cos(4*Pi*x) + Cos(4*Pi*y) + Cos(4*Pi*z)) - 14,15,16
  // Slater
//   orbital_weight[0] = 1;
//   orbital_weight[1] = orbital_weight[2] = orbital_weight[3] =  1;
//   orbital_weight[4] = orbital_weight[5] = orbital_weight[6] = orbital_weight[7] = orbital_weight[8] = orbital_weight[9] = 1;
//   orbital_weight[10] = orbital_weight[11] = orbital_weight[12] = orbital_weight[13] = 0;
//   orbital_weight[14] = orbital_weight[15] = orbital_weight[16] = 0;

//// no two body term
//   orbital_weight[0] = 0.24;
//   orbital_weight[1] = orbital_weight[2] = orbital_weight[3] =  1;
//   orbital_weight[4] = orbital_weight[5] = orbital_weight[6] = orbital_weight[7] = orbital_weight[8] = orbital_weight[9] = 0.2;
//   orbital_weight[10] = orbital_weight[11] = orbital_weight[12] = orbital_weight[13] = 0.057;
//   orbital_weight[14] = orbital_weight[15] = orbital_weight[16] = 0.035;
  if(first_time) orbital_N = 1;

#ifdef BC_3DPBC
  for(alpha = 0; alpha<ORBITALS_MAX; alpha++) {
    nE = orbital_numbers_IFG[alpha][0]*orbital_numbers_IFG[alpha][0]+orbital_numbers_IFG[alpha][1]*orbital_numbers_IFG[alpha][1]+orbital_numbers_IFG[alpha][2]*orbital_numbers_IFG[alpha][2];
    if(nE == 0)
      orbital_weight[alpha] = orbital_weight1;
    else if(nE == 1)
      orbital_weight[alpha] = orbital_weight2;
    else if(nE == 2)
      orbital_weight[alpha] = orbital_weight3;
    else if(nE == 3)
      orbital_weight[alpha] = orbital_weight4;
    else if(nE == 4)
      orbital_weight[alpha] = orbital_weight5;
  }
#endif
// with two body term
//     if(nE == 0)
//       orbital_weight[alpha] = 0.016;
//     else if(nE == 1)
//       orbital_weight[alpha] = 0.466;
//     else if(nE == 2)
//       orbital_weight[alpha] = 0.0068;
//     else if(nE == 3)
//       orbital_weight[alpha] = 0.00091;
//     else if(nE == 4)
//       orbital_weight[alpha] = 0.0007;

//     if(Opar>0)
//       orbital_weight[alpha] /= Opar;
//     else
//       orbital_weight[alpha] *= 1e3;

  //for(alpha=0; alpha<orbital_N; alpha++) orbital_weight[alpha] = Opar/(DOUBLE)(orbital_N);

  if(Nup>Ndn) {
    // numbers:
    // 0 - const
    // 1 - cos kx
    // 2 - sin kx
    // 3 - cos 2kx
    // 4 - sin 2kx
    // 5 - cos 3kx
    if(Nup == 33 && Ndn == 32) { // x= 0.96969697
      Warning("  Filling orbital (0) : const\n");
      orbital_numbers_unpaired[0][0] = 0;
      orbital_numbers_unpaired[0][1] = 0;
      orbital_numbers_unpaired[0][2] = 0;
      Message("Orbital energy in units of Fermi energy is %lf\n", 0. * 4.*pi*pi/pow(Ndens*3.*pi*pi,2./3.));
    }
    else if(Nup == 34 && Ndn == 32) {  // x = 0.9412
      Warning("  Filling orbital (0)\n");
      orbital_numbers_unpaired[0][0] = 1;
      orbital_numbers_unpaired[0][1] = 1;
      orbital_numbers_unpaired[0][2] = 1;
    }
    else if(Nup == 37 && Ndn == 29) { // x= 0.7838
      Warning("  Filling orbital (3)\n");
//   3  11  3  2Cos(x+y+z)
//   3  12  3  2Cos(x-y+z)
//   3  13  3  2Cos(x+y-z)
//   3  14  3  2Cos(x-y-z)
      orbital_numbers_unpaired[0][0] = 2;
      orbital_numbers_unpaired[0][1] = 2;
      orbital_numbers_unpaired[0][2] = 2;

      orbital_numbers_unpaired[1][0] = 2;
      orbital_numbers_unpaired[1][1] = 2;
      orbital_numbers_unpaired[1][2] = 3;

      orbital_numbers_unpaired[2][0] = 2;
      orbital_numbers_unpaired[2][1] = 3;
      orbital_numbers_unpaired[2][2] = 2;

      orbital_numbers_unpaired[3][0] = 2;
      orbital_numbers_unpaired[3][1] = 3;
      orbital_numbers_unpaired[3][2] = 3;

      orbital_numbers_unpaired[4][0] = 3;
      orbital_numbers_unpaired[4][1] = 2;
      orbital_numbers_unpaired[4][2] = 2;

      orbital_numbers_unpaired[5][0] = 3;
      orbital_numbers_unpaired[5][1] = 2;
      orbital_numbers_unpaired[5][2] = 3;

      orbital_numbers_unpaired[6][0] = 3;
      orbital_numbers_unpaired[6][1] = 3;
      orbital_numbers_unpaired[6][2] = 2;

      orbital_numbers_unpaired[7][0] = 3;
      orbital_numbers_unpaired[7][1] = 3;
      orbital_numbers_unpaired[7][2] = 3;
    }
    else if(Nup == 40 && Ndn == 26) { // x= 0.65
      Warning("  Filling orbitals (3,4)\n");
// 3  11  3  2Cos(x+y+z)
// 3  12  3  2Cos(x-y+z)
// 3  13  3  2Cos(x+y-z)
// 3  14  3  2Cos(x-y-z)
// 4  15  4  2Cos(2x)
// 4  16  4  2Cos(2y)
// 4  17  4  2Cos(2z)
      orbital_numbers_unpaired[0][0] = 2;
      orbital_numbers_unpaired[0][1] = 2;
      orbital_numbers_unpaired[0][2] = 2;

      orbital_numbers_unpaired[1][0] = 2;
      orbital_numbers_unpaired[1][1] = 2;
      orbital_numbers_unpaired[1][2] = 3;

      orbital_numbers_unpaired[2][0] = 2;
      orbital_numbers_unpaired[2][1] = 3;
      orbital_numbers_unpaired[2][2] = 2;

      orbital_numbers_unpaired[3][0] = 2;
      orbital_numbers_unpaired[3][1] = 3;
      orbital_numbers_unpaired[3][2] = 3;

      orbital_numbers_unpaired[4][0] = 3;
      orbital_numbers_unpaired[4][1] = 2;
      orbital_numbers_unpaired[4][2] = 2;

      orbital_numbers_unpaired[5][0] = 3;
      orbital_numbers_unpaired[5][1] = 2;
      orbital_numbers_unpaired[5][2] = 3;

      orbital_numbers_unpaired[6][0] = 3;
      orbital_numbers_unpaired[6][1] = 3;
      orbital_numbers_unpaired[6][2] = 2;

      orbital_numbers_unpaired[7][0] = 3;
      orbital_numbers_unpaired[7][1] = 3;
      orbital_numbers_unpaired[7][2] = 3;

      orbital_numbers_unpaired[8][0] = 4;
      orbital_numbers_unpaired[8][1] = 0;
      orbital_numbers_unpaired[8][2] = 0;

      orbital_numbers_unpaired[9][0] = 5;
      orbital_numbers_unpaired[9][1] = 0;
      orbital_numbers_unpaired[9][2] = 0;

      orbital_numbers_unpaired[10][0] = 0;
      orbital_numbers_unpaired[10][1] = 4;
      orbital_numbers_unpaired[10][2] = 0;

      orbital_numbers_unpaired[11][0] = 0;
      orbital_numbers_unpaired[11][1] = 5;
      orbital_numbers_unpaired[11][2] = 0;

      orbital_numbers_unpaired[12][0] = 0;
      orbital_numbers_unpaired[12][1] = 0;
      orbital_numbers_unpaired[12][2] = 4;

      orbital_numbers_unpaired[13][0] = 0;
      orbital_numbers_unpaired[13][1] = 0;
      orbital_numbers_unpaired[13][2] = 5;
    }
    else if(Nup == 33 && Ndn == 26) {
      Warning("  Filling orbitals (1,2)\n");
      orbital_numbers_unpaired[0][0] = 1;
      orbital_numbers_unpaired[0][1] = 1;
      orbital_numbers_unpaired[0][2] = 1;

      orbital_numbers_unpaired[1][0] = 2;
      orbital_numbers_unpaired[1][1] = 0;
      orbital_numbers_unpaired[1][2] = 0;

      orbital_numbers_unpaired[2][0] = 3;
      orbital_numbers_unpaired[2][1] = 0;
      orbital_numbers_unpaired[2][2] = 0;

      orbital_numbers_unpaired[3][0] = 0;
      orbital_numbers_unpaired[3][1] = 2;
      orbital_numbers_unpaired[3][2] = 0;

      orbital_numbers_unpaired[4][0] = 0;
      orbital_numbers_unpaired[4][1] = 3;
      orbital_numbers_unpaired[4][2] = 0;

      orbital_numbers_unpaired[5][0] = 0;
      orbital_numbers_unpaired[5][1] = 0;
      orbital_numbers_unpaired[5][2] = 2;

      orbital_numbers_unpaired[6][0] = 0;
      orbital_numbers_unpaired[6][1] = 0;
      orbital_numbers_unpaired[6][2] = 3;
    }
    else if(Nup == 34 && Ndn == 33) {
      alpha = 0;
      Warning("\nSetting index of first polarized orbital to alpha = %i\n", alpha_polarized);

      // numbers:
      // 0 - const
      // 1 - cos kx
      // 2 - sin kx
      // 3 - cos 2kx
      // 4 - sin 2kx
      // 5 - cos 3kx
      // ...
      if(alpha_polarized == 1) {
        orbital_numbers_unpaired[alpha][0] = 1;
        orbital_numbers_unpaired[alpha][1] = 0;
        orbital_numbers_unpaired[alpha][2] = 0;
        Message("Corresponding orbital has k^2 = 1 kmin^2 and is given by phi(r) = Cos(kx)\n");
        Message("Orbital energy in units of Fermi energy is %lf\n", 1. * 4.*pi*pi/pow(Ndens*3.*pi*pi,2./3.));
      }
      else if(alpha_polarized == 2) {
        orbital_numbers_unpaired[alpha][0] = 1;
        orbital_numbers_unpaired[alpha][1] = 1;
        orbital_numbers_unpaired[alpha][2] = 0;
        Message("Corresponding orbital has k^2 = 2 kmin^2 and is given by phi(r) = Cos(kx) Cos(ky)\n");
        Message("Orbital energy in units of Fermi energy is %lf\n", 2. * 4.*pi*pi/pow(Ndens*3.*pi*pi,2./3.));
      }
      else if(alpha_polarized == 3) {
        orbital_numbers_unpaired[alpha][0] = 1;
        orbital_numbers_unpaired[alpha][1] = 1;
        orbital_numbers_unpaired[alpha][2] = 1;
        Message("Corresponding orbital has k^2 = 3 kmin^2 and is given by phi(r) = Cos(kx) Cos(ky) Cos(kz)\n");
        Message("Orbital energy in units of Fermi energy is %lf\n", 3. * 4.*pi*pi/pow(Ndens*3.*pi*pi,2./3.));
      }
      else if(alpha_polarized == 4) {
        orbital_numbers_unpaired[alpha][0] = 3;//2
        orbital_numbers_unpaired[alpha][1] = 0;
        orbital_numbers_unpaired[alpha][2] = 0;
        Message("Corresponding orbital has k^2 = 4 kmin^2 and is given by phi(r) = Cos(2kx)\n");
        Message("Orbital energy in units of Fermi energy is %lf\n", 4. * 4.*pi*pi/pow(Ndens*3.*pi*pi,2./3.));
      }
      else if(alpha_polarized == 5) {
        orbital_numbers_unpaired[alpha][0] = 3;//2
        orbital_numbers_unpaired[alpha][1] = 1;
        orbital_numbers_unpaired[alpha][2] = 0;
        Message("Corresponding orbital has k^2 = 5 kmin^2 and is given by phi(r) = Cos(2kx) Cos(ky)\n");
        Message("Orbital energy in units of Fermi energy is %lf\n", 5. * 4.*pi*pi/pow(Ndens*3.*pi*pi,2./3.));
      }
      else if(alpha_polarized == 6) {
        orbital_numbers_unpaired[alpha][0] = 3;//2
        orbital_numbers_unpaired[alpha][1] = 1;
        orbital_numbers_unpaired[alpha][2] = 1;
        Message("Corresponding orbital has k^2 = 6 kmin^2 and is given by phi(r) = Cos(2kx) Cos(ky) Cos(kz)\n");
        Message("Orbital energy in units of Fermi energy is %lf\n", 6. * 4.*pi*pi/pow(Ndens*3.*pi*pi,2./3.));
      }
      else if(alpha_polarized == 8) {
        orbital_numbers_unpaired[alpha][0] = 3;//2
        orbital_numbers_unpaired[alpha][1] = 3;//2
        orbital_numbers_unpaired[alpha][2] = 0;
        Message("Corresponding orbital has k^2 = 8 kmin^2 and is given by phi(r) = Cos(2kx) Cos(2ky)\n");
        Message("Orbital energy in units of Fermi energy is %lf\n", 8. * 4.*pi*pi/pow(Ndens*3.*pi*pi,2./3.));
      }
      else if(alpha_polarized == 9) {
        orbital_numbers_unpaired[alpha][0] = 3;//2
        orbital_numbers_unpaired[alpha][1] = 3;//2
        orbital_numbers_unpaired[alpha][2] = 1;
        Message("Corresponding orbital has k^2 = 9 kmin^2 and is given by phi(r) = Cos(2kx) Cos(2ky) Cos(kz)\n");
      }
      else if(alpha_polarized == 10) {
        orbital_numbers_unpaired[alpha][0] = 5;//3
        orbital_numbers_unpaired[alpha][1] = 1;
        orbital_numbers_unpaired[alpha][2] = 0;
        Message("Corresponding orbital has k^2 = 10 kmin^2 and is given by phi(r) = Cos(3kx) Cos(ky)\n");
      }
      else if(alpha_polarized == 11) {
        orbital_numbers_unpaired[alpha][0] = 5;//3
        orbital_numbers_unpaired[alpha][1] = 1;
        orbital_numbers_unpaired[alpha][2] = 1;
        Message("Corresponding orbital has k^2 = 11 kmin^2 and is given by phi(r) = Cos(3kx) Cos(ky) Cos(kz)\n");
      }
      else if(alpha_polarized == 12) {
        orbital_numbers_unpaired[alpha][0] = 3;//2
        orbital_numbers_unpaired[alpha][1] = 3;//2
        orbital_numbers_unpaired[alpha][2] = 3;//2
        Message("Corresponding orbital has k^2 = 12 kmin^2 and is given by phi(r) = Cos(2kx) Cos(2ky) Cos(2kz)\n");
      }
      else { // alpha_polarized == 0
        orbital_numbers_unpaired[alpha][0] = 0;
        orbital_numbers_unpaired[alpha][1] = 0;
        orbital_numbers_unpaired[alpha][2] = 0;
        Message("Corresponding orbital has k^2 = 0 kmin^2 and is given by phi(r) = const\n");
        Message("Orbital energy in units of Fermi energy is %lf\n", 0. * 4.*pi*pi/pow(Ndens*3.*pi*pi,2./3.));
      }

      /*if(Nup-Ndn == 2) {
        alpha = 1;
        orbital_numbers_polarized[alpha] = 1;
        orbital_numbers_unpaired[alpha][0] = 1;
        orbital_numbers_unpaired[alpha][1] = 0;
        orbital_numbers_unpaired[alpha][2] = 0;
        Message("Second orbital has k^2 = 1 kmin^2 and is given by phi(r) = Cos(kx)\n");
      }*/
    }
  }

  if(first_time) Message("  Following weights are used: \n");
  alpha=0;

  if(boundary == THREE_BOUNDARY_CONDITIONS) {
    for(nE=0; nE<=38 && go; nE++) {
      for(nzm=0; nzm<=nE && go; nzm++) {
        if(nzm%2==0)
          nz = -nzm/2;
        else
          nz = (nzm+1)/2;
        for(nym=0; nym<=nE && go; nym++) {
          if(nym%2==0)
            ny = -nym/2;
          else
            ny = (nym+1)/2;
          for(nx=0; nx<=nE && go; nx++) {
            if(nx*nx + ny*ny + nz*nz == nE && (nx>0 || nx==0 && (ny>0 || ny==0 && nz>=0))) {
              orbital_numbers_IFG[alpha][0] = nx;
              orbital_numbers_IFG[alpha][1] = ny;
              orbital_numbers_IFG[alpha][2] = nz;
              if(orbital_weight[alpha] && alpha>=orbital_N) {
                Warning("  increasing orbital_N to %i\n", alpha+1);
                orbital_N = alpha+1;
              }
              alpha++;
              if(orbital_weight[alpha-1] && first_time) {
                Message(" %3i ", alpha);
                Message(" k2 = %i ", nE);
                Message(" %lf ", orbital_weight[alpha-1]);
                alpha==1?Message(" 1"):Message(" 2 Cos(");
                nx<0?Message("-"):Message(""); // +/-
                //Message(abs(nx)>1?"%ix":(nx==0?"":"x"),abs(nx));
                //Message(ny==0?"":(nx==0?(ny<0?"-":""):(ny<0?"-":"+"))); // +/-
                //Message(abs(ny)>1?"%iy":(ny==0?"":"y"),abs(ny));
                //Message(nz==0?"":(nx==0&&ny==0?(nz<0?"-":""):(nz<0?"-":"+"))); // +/-
                //Message(abs(nz)>1?"%iz":(nz==0?"":"z"),abs(nz));
                alpha>1 ?Message(")"):Message("");
                Message("\n");
              }
            }
          }
        }
      }
    }
  }

  if(boundary == TWO_BOUNDARY_CONDITIONS) {
    for(nE=0; nE<=38 && go; nE++) {
      for(nym=0; nym<=nE && go; nym++) {
        if(nym%2==0)
          ny = -nym/2;
        else
          ny = (nym+1)/2;
        for(nx=0; nx<=nE && go; nx++) {
          if(nx*nx + ny*ny== nE && (nx>0 || nx==0 && (ny>0 || ny==0))) {
            orbital_numbers_IFG[alpha][0] = nx;
            orbital_numbers_IFG[alpha][1] = ny;
            orbital_numbers_IFG[alpha][2] = 0;
            if(2*(alpha-1)+1<Nup) {
              //Warning("  increasing orbital_N to %i\n", alpha+1);
              orbital_N = alpha+1;
              orbital_weight[alpha] = 1;
            }
            alpha++;
            if(orbital_weight[alpha-1] && first_time) {
              Message(" %3i ", alpha);
              Message(" k2 = %i ", nE);
              Message(" %lf ", orbital_weight[alpha-1]);
              alpha == 1 ? Message(" 1") : Message(" 2 Cos(");
              nx<0 ? Message("-") : Message(""); // +/-
              //Message(abs(nx)>1?"%ix":(nx==0?"":"x"),abs(nx));
              //Message(ny==0?"":(nx==0?(ny<0?"-":""):(ny<0?"-":"+"))); // +/-
              //Message(abs(ny)>1?"%iy":(ny==0?"":"y"),abs(ny));
              alpha>1 ? Message(")") : Message("");
              Message("\n");
            }
          }
        }
      }
    }
  }

  if(boundary == BILAYER_BOUNDARY_CONDITIONS) {
    for(nE=0; nE<=38 && go; nE++) {
      for(nym=0; nym<=nE && go; nym++) {
        if(nym%2==0)
          ny = -nym/2;
        else
          ny = (nym+1)/2;
        for(nx=0; nx<=nE && go; nx++) {
          if(nx*nx + ny*ny== nE && (nx>0 || nx==0 && (ny>0 || ny==0))) {
            orbital_numbers_IFG[alpha][0] = nx;
            orbital_numbers_IFG[alpha][1] = ny;
            orbital_numbers_IFG[alpha][2] = 0;
            if(2*(alpha-1)+1<Nup) {
              //Warning("  increasing orbital_N to %i\n", alpha+1);
              orbital_N = alpha+1;
              orbital_weight[alpha] = 1;
            }
            alpha++;
            if(orbital_weight[alpha-1] && first_time) {
              Message(" %3i ", alpha);
              Message(" k2 = %i ", nE);
              Message(" %lf ", orbital_weight[alpha-1]);
              //Message(alpha==1?" 1":" 2 Cos(");
              //Message(nx<0?"-":""); // +/-
              //Message(abs(nx)>1?"%ix":(nx==0?"":"x"),abs(nx));
              //Message(ny==0?"":(nx==0?(ny<0?"-":""):(ny<0?"-":"+"))); // +/-
              //Message(abs(ny)>1?"%iy":(ny==0?"":"y"),abs(ny));
              //Message(alpha>1?")":"");
              Message("\n");
            }
          }
        }
      }
    }
  }

#ifdef ORBITAL_3BODY
  /*Message(" orbitals: 1 1 1\n");
  orbital_numbers_unpaired[0][0] = 0;
  orbital_numbers_unpaired[0][1] = 0;
  orbital_numbers_unpaired[0][2] = 0;

  Warning("setting orbital_N to 2, i.e. const + Cos(2 pi x /L)\n");
  orbital_N = 2;*/

  Message(" orbitals: Cos(2 pi x/L) 1 1\n");
  orbital_numbers_unpaired[0][0] = 1;
  orbital_numbers_unpaired[0][1] = 0;
  orbital_numbers_unpaired[0][2] = 0;

  /*Message(" orbitals: Cos(2 pi x/L) Cos(2 pi y/L) Cos(2 pi z/L)\n");
  orbital_numbers_unpaired[0][0] = 1;
  orbital_numbers_unpaired[0][1] = 1;
  orbital_numbers_unpaired[0][2] = 1;*/

  orbital_numbers_unpaired[1][0] = 0;
  orbital_numbers_unpaired[1][1] = 0;
  orbital_numbers_unpaired[1][2] = 0;
  orbital_numbers_unpaired[2][0] = 0;
  orbital_numbers_unpaired[2][1] = 0;
  orbital_numbers_unpaired[2][2] = 0;
#endif

#ifdef ORBITAL_POLARIZED_3BODY_WITH_ZERO_MOMENTUM
  Message(" orbitals: Cos(2 pi x/L) 1 1\n");

  orbital_numbers_unpaired[0][0] = 1;
  orbital_numbers_unpaired[0][1] = 0;
  orbital_numbers_unpaired[0][2] = 0;

  orbital_numbers_unpaired[1][0] = 0;
  orbital_numbers_unpaired[1][1] = 0;
  orbital_numbers_unpaired[1][2] = 0;
  orbital_numbers_unpaired[2][0] = 0;
  orbital_numbers_unpaired[2][1] = 0;
  orbital_numbers_unpaired[2][2] = 0;

  orbital_N = 2; // 0 + 1*Cos(k x)
#endif

  first_time = OFF;
  if(first_time) Message("\n");
}

/************************** orbitalFewBody_F ************************/
// returns phi(x,y,z)
DOUBLE orbitalFewBody_F(DOUBLE x, DOUBLE y, DOUBLE z, DOUBLE xdn, DOUBLE ydn, DOUBLE zdn, int i, int j) {
  DOUBLE k0 = _2PI/L;
// i - index of x
// j - index of xdn
#ifdef ORBITAL_POLARIZED_3BODY_WITH_ZERO_MOMENTUM
   if(j==0) {
     return Cos(k0*x);
   }
    else {
     return Sin(k0*x);
   }
#else
  return 0;
#endif
}

/************************** orbitalFewBody_Fp ************************/
// returns [Fx,Fy,Fz] = phi'(x,y,z)
void orbitalFewBody_Fp(DOUBLE *Fxup, DOUBLE *Fyup, DOUBLE *Fzup, DOUBLE *Fxdn, DOUBLE *Fydn, DOUBLE *Fzdn, DOUBLE x, DOUBLE y, DOUBLE z, DOUBLE xdn, DOUBLE ydn, DOUBLE zdn, int i, int j) {
  *Fxup = *Fyup = *Fzup = *Fxdn = *Fydn = *Fzdn = 0.;

#ifdef ORBITAL_POLARIZED_3BODY_WITH_ZERO_MOMENTUM
  DOUBLE k0 = _2PI/L;
   if(j==0) {
      *Fxup += -k0*Sin(k0*x);
   }
    else {
      *Fxup += k0*Cos(k0*x);
   }
#endif
}

/************************** orbitalFewBody_Fpp ************************/
// returns phi"(x,y,z)
void orbitalFewBody_Fpp(DOUBLE *Fupup, DOUBLE *Fdndn, DOUBLE x, DOUBLE y, DOUBLE z, DOUBLE xdn, DOUBLE ydn, DOUBLE zdn, int i, int j) {
  int alpha;

  *Fupup = *Fdndn = 0.;

#ifdef ORBITAL_POLARIZED_3BODY_WITH_ZERO_MOMENTUM
  DOUBLE k0 = _2PI/L;
if(j==0) {
      *Fupup += -k0*k0*Cos(k0*x);
   }
    else {
      *Fupup += -k0*k0*Sin(k0*x);
   }
#endif
}

/************************** orbitalDescreteBCS_F ************************/
// returns phi(x,y,z)
#ifndef ORBITAL_DESCRETE_COULOMB // i.e. in the case of IFG
DOUBLE orbitalDescreteBCS_F(DOUBLE x, DOUBLE y, DOUBLE z) {
  int alpha;
  DOUBLE phi;
  DOUBLE k0 = _2PI/L;

  phi = orbital_weight[0]; // alpha = 0 term
  for(alpha=1; alpha<orbital_N; alpha++) {
    phi += 2.*orbital_weight[alpha]*Cos(k0*(orbital_numbers_IFG[alpha][0]*x+orbital_numbers_IFG[alpha][1]*y+orbital_numbers_IFG[alpha][2]*z));
  }

  return phi;
}

/************************** orbitalDescreteBCS_Fp ************************/
// returns [Fx,Fy,Fz] = phi'(x,y,z)
void orbitalDescreteBCS_Fp(DOUBLE *Fx, DOUBLE *Fy, DOUBLE *Fz, DOUBLE x, DOUBLE y, DOUBLE z) {
  int alpha;
  DOUBLE Orb;
  DOUBLE kx,ky,kz;
  DOUBLE k0 = _2PI/L;

  *Fx = *Fy = *Fz = 0;
  for(alpha=1; alpha<orbital_N; alpha++) {
    kx = k0*orbital_numbers_IFG[alpha][0];
    ky = k0*orbital_numbers_IFG[alpha][1];
    kz = k0*orbital_numbers_IFG[alpha][2];
    Orb = -2*orbital_weight[alpha]*Sin(kx*x + ky*y + kz*z);
    *Fx += kx*Orb;
    *Fy += ky*Orb;
    *Fz += kz*Orb;
  }
}

/************************** orbitalDescreteBCS_Fpp ************************/
// returns phi"(x,y,z)
DOUBLE orbitalDescreteBCS_Fpp(DOUBLE x, DOUBLE y, DOUBLE z) {
  int alpha;
  DOUBLE kx,ky,kz;
  DOUBLE Ekin = 0;
  DOUBLE k0 = _2PI/L;

  for(alpha=1; alpha<orbital_N; alpha++) {
    kx = k0*orbital_numbers_IFG[alpha][0];
    ky = k0*orbital_numbers_IFG[alpha][1];
    kz = k0*orbital_numbers_IFG[alpha][2];
    Ekin -= 2*orbital_weight[alpha]*(kx*kx+ky*ky+kz*kz)*Cos(kx*x + ky*y + kz*z);
  }
  return Ekin;
}
#else // PBC sum of the orbitals
int sum_over_images_cut_off_int_orbital = 0;
DOUBLE orbitalDescreteBCS_F(DOUBLE x, DOUBLE y, DOUBLE z) {
  DOUBLE phi = 0.;
  DOUBLE r,xp,yp,zp;
  int nx, ny, nz, n2;

  for(nx=-sum_over_images_cut_off_int_orbital; nx<=sum_over_images_cut_off_int_orbital; nx++) {
    xp = x + nx*L;
    for(ny=-sum_over_images_cut_off_int_orbital; ny<=sum_over_images_cut_off_int_orbital; ny++) {
      yp = y + ny*L;
      for(nz=-sum_over_images_cut_off_int_orbital; nz<=sum_over_images_cut_off_int_orbital; nz++) {
        zp = z + nz*L;
        n2 = nx*nx + ny*ny + nz*nz;
        r = sqrt(xp*xp+yp*yp+zp*zp);
        if(n2>0 || (n2 == 0 && r>1.)) {
          phi += BCSBtrial*Exp(-BCSsqE*r)/r;
        }
        else if (r>0.) {
          phi += BCSCh2gamma*Sin(BCStrial_Kappa*r)/r;
        }
        else {
          phi += BCSCh2gamma*BCStrial_Kappa;
        }
      }
    }
  }

  return phi;
}

/************************** orbitalDescreteBCS_Fp ************************/
// returns [Fx,Fy,Fz] = phi'(x,y,z)
void orbitalDescreteBCS_Fp(DOUBLE *Fx, DOUBLE *Fy, DOUBLE *Fz, DOUBLE x, DOUBLE y, DOUBLE z) {
  DOUBLE r,xp,yp,zp,Fr;
  int nx, ny, nz, n2;

  *Fx = *Fy = *Fz = 0;

  for(nx=-sum_over_images_cut_off_int_orbital; nx<=sum_over_images_cut_off_int_orbital; nx++) {
    xp = x + nx*L;
    for(ny=-sum_over_images_cut_off_int_orbital; ny<=sum_over_images_cut_off_int_orbital; ny++) {
      yp = y + ny*L;
      for(nz=-sum_over_images_cut_off_int_orbital; nz<=sum_over_images_cut_off_int_orbital; nz++) {
        zp = z + nz*L;
        n2 = nx*nx + ny*ny + nz*nz;
        r = sqrt(xp*xp+yp*yp+zp*zp);
        if(n2>0 || (n2==0 && r>1.)) {
          Fr = BCSBtrial*Exp(-BCSsqE*r)/r*(-BCSsqE - 1./r);
        }
        else {
          Fr = BCSCh2gamma*Sin(BCStrial_Kappa*r)/r*(BCStrial_Kappa/tg(BCStrial_Kappa*r) - 1./r);
        }
        *Fx += xp*Fr/r;
        *Fy += yp*Fr/r;
        *Fz += zp*Fr/r;
      }
    }
  }
}

/************************** orbitalDescreteBCS_Fpp ************************/
// returns phi"(x,y,z)
DOUBLE orbitalDescreteBCS_Fpp(DOUBLE x, DOUBLE y, DOUBLE z) {
  int nx, ny, nz, n2;
  DOUBLE Ekin = 0.;
  DOUBLE r,xp,yp,zp;

  for(nx=-sum_over_images_cut_off_int_orbital; nx<=sum_over_images_cut_off_int_orbital; nx++) {
    xp = x + nx*L;
    for(ny=-sum_over_images_cut_off_int_orbital; ny<=sum_over_images_cut_off_int_orbital; ny++) {
      yp = y + ny*L;
      for(nz=-sum_over_images_cut_off_int_orbital; nz<=sum_over_images_cut_off_int_orbital; nz++) {
        zp = z + nz*L;
        n2 = nx*nx + ny*ny + nz*nz;
        r = sqrt(xp*xp+yp*yp+zp*zp);
        if(n2>0 || (n2 == 0 && r>1.)) {
          Ekin += BCSsqE*BCSsqE*BCSBtrial*Exp(-BCSsqE*r)/r;
        }
        else {
          Ekin += -BCStrial_Kappa*BCStrial_Kappa*BCSCh2gamma*Sin(BCStrial_Kappa*r)/r;
        }
      }
    }
  }

  return Ekin;
}

// Eloc = f" +2/r f' = u"/r
DOUBLE orbitalExactElocsum(DOUBLE r) {
  DOUBLE e1,e2,fp,fpp;
}
#endif
////

#ifdef ORBITAL_CH2
DOUBLE orbitalExactF(DOUBLE r) {
  if(r==0.)
    return 1.;
  else
    return tanh(r)/r;
}

// Fp = f'
DOUBLE orbitalExactFp(DOUBLE r) {
  DOUBLE e;

  e = 1./cosh(r);

  //return -1/r+1./(cosh(r)*sinh(r));
  return e*e/r-tanh(r)/(r*r);
}

// Eloc = f" +2/r f' = u"/r
DOUBLE orbitalExactEloc(DOUBLE r) {
  DOUBLE e;

  e = 1./cosh(r);

  //return 2.*e*e;
  return -2.*e*e*tanh(r)/r;
}
#endif

DOUBLE Atrial, Btrial, Ctrial;
void ConstructGridCh2PhononOrbital(void) {

  Message("  th(r)/r; Btrial + Ctrial*(Exp(-Bpar*r)+Exp(-Bpar*(L-r))), Rpar, Bpar - var. par");
  Warning("Rpar -> Rpar L/2 (%lf -> %lf)\n", Rpar, Rpar*Lhalf);
  Rpar *= Lhalf;

  Ctrial = (Exp(Bpar*(L + Rpar))*(-2*Rpar + sinh(2*Rpar)))/
   (2*Exp((Bpar*L)/2. + Bpar*Rpar)*(-2*Rpar + sinh(2*Rpar)) + 
     Exp(Bpar*L)*(2*Rpar + (-1 + Bpar*Rpar)*sinh(2*Rpar)) + 
     Exp(2*Bpar*Rpar)*(2*Rpar - (1 + Bpar*Rpar)*sinh(2*Rpar)));


  Btrial = 1 - 2.*Ctrial*Exp(-Bpar*Lhalf);
  Atrial = (Btrial + Ctrial*(Exp(-Bpar*Rpar)+Exp(-Bpar*(L-Rpar))))*Rpar/tanh(Rpar);
}

#ifdef ORBITAL_CH2_PHONON
DOUBLE orbitalExactF(DOUBLE r) {
  if(r==0.)
    return 1.;
  else if(r<Rpar)
    return Atrial*tanh(r)/r;
  else //if(r<Lhalf)
    return Btrial + Ctrial*(Exp(-Bpar*r)+Exp(-Bpar*(L-r)));
  //else
  //  return 0.;
}

// Fp = f'
DOUBLE orbitalExactFp(DOUBLE r) {
  DOUBLE e;

  if(r<Rpar) {
    e = 1./cosh(r);
    return Atrial*(e*e/r-tanh(r)/(r*r));
  }
  else //if(r<Lhalf)
    return Bpar*Ctrial*(-Exp(-Bpar*r)+Exp(-Bpar*(L-r)));
  //else
  //  return 0.;
}

// Eloc = f" +2/r f' = u"/r
DOUBLE orbitalExactEloc(DOUBLE r) {
  DOUBLE e;
  DOUBLE Lmr2,Lm2r2;

  if(r<Rpar) {
    e = 1./cosh(r);
    return -2.*Atrial*e*e*tanh(r)/r;
  }
  else //if(r<Lhalf) {
    return -((Bpar*Ctrial*(Exp(Bpar*(L-r))*(-2+Bpar*r) + Exp(Bpar*r)*(2+Bpar*r)))/(Exp(Bpar*L)*r));
  //}
  //else
  //  return 0.;
}
#endif

void ConstructGridCh2Exp(void) {
  Message("  Ch2 exp w.f.: (1 + Ch2gamma Bpar r) (1 - Exp(-Cpar Bpar r)) Exp(-Bpar r) /(Cpar Bpar r)");
  Ch2gamma = 1 + Cpar*0.5;
  Chnorm = 1.;
  Chnorm = 1./orbitalExactF(Lhalf);

}

#ifdef ORBITAL_CH2_EXP
DOUBLE orbitalExactF(DOUBLE r) {
  if(r<=Lhalf)
    return Chnorm*(((-1+Exp(Bpar*Cpar*(L-r)))*(1+Ch2gamma*Bpar*(L-r)))/(Exp(Bpar*(1+Cpar)*(L-r))*(L-r))+((-1+Exp(Bpar*Cpar*r))*(1+Ch2gamma*Bpar*r))/(Exp(Bpar*(1+Cpar)*r)*r))/(Bpar*Cpar);
  else
    return 1;
}

// Fp = f'
DOUBLE orbitalExactFp(DOUBLE r) {
  if(r<Lhalf)
    return Chnorm*((Ch2gamma*(Bpar*Bpar)*(Exp(Bpar*Cpar*(L+r))*(-Exp(Bpar*L)+Exp(2*Bpar*r))+(1+Cpar)*(Exp(Bpar*(1+Cpar)*L)-Exp(2*Bpar*(1+Cpar)*r))))/Exp(Bpar*(1+Cpar)*(L+r))+(Exp(Bpar*(-L+r))*(1+Bpar*L-Bpar*r))/((L-r)*(L-r))-(1+Bpar*r)/(Exp(Bpar*r)*(r*r))+(1+Bpar*(1+Cpar)*r)/(Exp(Bpar*(1+Cpar)*r)*(r*r))+(-1+Bpar*(1+Cpar)*(-L+r))/(Exp(Bpar*(1+Cpar)*(L-r))*((L-r)*(L-r))))/(Bpar*Cpar);
  else
    return 0.;
}

// Eloc = f" +2/r f' = u"/r
DOUBLE orbitalExactEloc(DOUBLE r) {
  //return (-((Bpar*Bpar)*Exp(Bpar*(L+Cpar*L-r))*((L-r)*(L-r)*(L-r))*(1+Ch2gamma*(-2+Bpar*r)))+(Bpar*Bpar)*(1+Cpar)*Exp(Bpar*(1+Cpar)*(L-r))*((L-r)*(L-r)*(L-r))*(1+Cpar+Ch2gamma*(-2+Bpar*(1+Cpar)*r))+Exp(Bpar*(Cpar*L+r))*(-(Ch2gamma*(Bpar*Bpar)*(L*L*L)*(2+Bpar*r))+(Bpar*Bpar)*(r*r*r)*(-1+2*Ch2gamma+Ch2gamma*Bpar*r)+Bpar*(L*L)*(2+Bpar*r)*(-1+3*Ch2gamma*Bpar*r)+L*(-2+Bpar*r*(2+Bpar*r*(2-3*Ch2gamma*(2+Bpar*r)))))+Exp(Bpar*(1+Cpar)*r)*(Ch2gamma*(Bpar*Bpar)*(1+Cpar)*(L*L*L)*(2+Bpar*(1+Cpar)*r)-Bpar*(1+Cpar)*(L*L)*(-1+3*Ch2gamma*Bpar*r)*(2+Bpar*(1+Cpar)*r)+(Bpar*Bpar)*(1+Cpar)*(r*r*r)*(1+Cpar-Ch2gamma*(2+Bpar*(1+Cpar)*r))+L*(2+Bpar*(1+Cpar)*r*(-2+Bpar*r*(-2*(1+Cpar)+3*Ch2gamma*(2+Bpar*(1+Cpar)*r))))))/(Bpar*Cpar*Exp(Bpar*(1+Cpar)*L)*r*(r-L)*(r-L)*(r-L));

  if(r<Lhalf)
    return 
  Chnorm*(-(power(Bpar,2)*Exp(Bpar*(L + Cpar*L - r))*power(L - r,3)*
        (1 + Ch2gamma*(-2 + Bpar*r))) + 
     power(Bpar,2)*(1 + Cpar)*Exp(Bpar*(1 + Cpar)*(L - r))*power(L - r,3)*
      (1 + Cpar + Ch2gamma*(-2 + Bpar*(1 + Cpar)*r)) + 
     Exp(Bpar*(Cpar*L + r))*(-(Ch2gamma*power(Bpar,2)*power(L,3)*(2 + Bpar*r)) + 
        power(Bpar,2)*power(r,3)*(-1 + 2*Ch2gamma + Ch2gamma*Bpar*r) + 
        Bpar*power(L,2)*(2 + Bpar*r)*(-1 + 3*Ch2gamma*Bpar*r) + 
        L*(-2 + Bpar*r*(2 + Bpar*r*(2 - 3*Ch2gamma*(2 + Bpar*r))))) + 
     Exp(Bpar*(1 + Cpar)*r)*(Ch2gamma*power(Bpar,2)*(1 + Cpar)*power(L,3)*
         (2 + Bpar*(1 + Cpar)*r) - 
        Bpar*(1 + Cpar)*power(L,2)*(-1 + 3*Ch2gamma*Bpar*r)*
         (2 + Bpar*(1 + Cpar)*r) + 
        power(Bpar,2)*(1 + Cpar)*power(r,3)*
         (1 + Cpar - Ch2gamma*(2 + Bpar*(1 + Cpar)*r)) + 
        L*(2 + Bpar*(1 + Cpar)*r*(-2 + 
              Bpar*r*(-2*(1 + Cpar) + 3*Ch2gamma*(2 + Bpar*(1 + Cpar)*r))))))/
   (Bpar*Cpar*Exp(Bpar*(1 + Cpar)*L)*r*power(-L + r,3));
  else
    return 0.;
}
#endif

#ifdef ORBITAL_SQUARE_WELL2D
DOUBLE orbitalExactF(DOUBLE r) {
  if(r<Ro)
    return A1SW*BesselJ0(trial_Kappa*r);
  else //if(r<Lhalf)
    return A2SW*BesselK0(trial_k*r);
  else
    return 1.;
}

// Fp = f'
DOUBLE orbitalExactFp(DOUBLE r) {
  if(r<Ro)
    return -A1SW*trial_Kappa*BesselJ1(trial_Kappa*r);
  else if(r<Lhalf)
    return -A2SW*trial_k*BesselK1(trial_k*r);
  else
    return 0;
}

// Eloc = f" +2/r f' = u"/r
DOUBLE orbitalExactEloc(DOUBLE r) {
  if(r<Ro)
    return -trial_Kappa2*A1SW*BesselJ0(trial_Kappa*r);
  else if(r<Lhalf)
    return trial_k2*A2SW*BesselK0(trial_k*r);
  else
    return 0.;
}
#endif

/************************** Construct Grid Fermion ************************/
void ConstructGridSquareWell2DSinus(void) {
  static int first_time = ON;
  DOUBLE f1,f2;
  DOUBLE xmin, xmax, ymin, ymax, x, y;
  int search = ON;
  DOUBLE precision = 1e-8;

  trial_Kappa = sqrt(2.*m_mu*trial_Vo);
  trial_Kappa2 = 2.*m_mu*trial_Vo;

  if(first_time) {
    Message("  2D square well (sinus)\n");
    if(Rpar<1) Error("  Rpar must be larger than 1\n");
    if(Rpar>Lhalf) Error("  Rpar must be smaller than L/2\n");
    first_time = OFF;
  }

  // define trial_k
  // Solve: k K1(k)/K0(k) -  ... = 0
  xmin = 1e-5;
  xmax = sqrt(2.*m_mu*trial_Vo)-1e-5;
  x = xmin;
  ymin = x*BesselK1(x)/BesselK0(x) - sqrt(2.*m_mu*trial_Vo-x*x)*BesselJ1(sqrt(2.*m_mu*trial_Vo-x*x))/BesselJ0(sqrt(2.*m_mu*trial_Vo-x*x));
  x = xmax;
  ymax = x*BesselK1(x)/BesselK0(x) - sqrt(2.*m_mu*trial_Vo-x*x)*BesselJ1(sqrt(2.*m_mu*trial_Vo-x*x))/BesselJ0(sqrt(2.*m_mu*trial_Vo-x*x));
  Message("  Solving equation for k ... \n");
  if(ymin*ymax > 0) Error("Can't construct the grid : No solution found");
  while(search) {
    x = (xmin+xmax)/2.;
    y = x*BesselK1(x)/BesselK0(x) - sqrt(2.*m_mu*trial_Vo-x*x)*BesselJ1(sqrt(2.*m_mu*trial_Vo-x*x))/BesselJ0(sqrt(2.*m_mu*trial_Vo-x*x));
    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }
    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y = x*BesselK1(x)/BesselK0(x) - sqrt(2.*m_mu*trial_Vo-x*x)*BesselJ1(sqrt(2.*m_mu*trial_Vo-x*x))/BesselJ0(sqrt(2.*m_mu*trial_Vo-x*x));
      search = OFF;
    }
  }
  Message("  Solution is scattering momentum k = %"LF", error %"LE"\n", x, y);
  trial_k = x;
  trial_k2 = trial_k*trial_k;
  trial_Kappa = sqrt(2.*m_mu*trial_Vo-trial_k2);
  trial_Kappa2 = 2.*m_mu*trial_Vo-trial_k2;

  energy_shift = trial_k2;
  Warning("  Binding energy %"LE" will be subtracted from the total energy !\n", energy_shift);

  trial_PI_L = PI/L;
  A2SW = Cpar / (BesselK0(trial_k*Rpar)+trial_k/trial_PI_L*BesselK1(trial_k*Rpar)*(Sin(trial_PI_L*Rpar)-1)/Cos(trial_PI_L*Rpar));
  A4SW = A2SW*trial_k/trial_PI_L*BesselK1(trial_k*Rpar)/Cos(trial_PI_L*Rpar);
  A3SW = A4SW + Cpar;
  A1SW = A2SW*BesselK0(trial_k) / BesselJ0(trial_Kappa);

  Message("  Testing continuity ... ");
  f1 = orbitalF(1.-1e-8);
  f2 = orbitalF(1.+1e-8); 
  if(fabs(f1/f2-1.)>1e-3) Error("  discontinuity at f(1): %"LG" and %"LG"\n", f1, f2);

  f1 = orbitalFp(1.-1e-8);
  f2 = orbitalFp(1.+1e-8); 
  if(fabs(f1/f2-1.)>1e-3) Error("  discontinuity at f'(1): %"LG" and %"LG"\n", f1, f2);

  f1 = orbitalF(Lhalf-1e-8);
  f2 = orbitalF(Lhalf+1e-8);
  if(fabs(f1/f2-1.)>1e-3 && f2 != 0.) Error("  discontinuity at f(1): %"LG" and %"LG"\n", f1, f2);

  f1 = orbitalFp(Lhalf-1e-8);
  f2 = orbitalFp(Lhalf+1e-8);
  if(fabs(f1/f2-1.)>precision) Warning("  discontinuity at f'(L/2): %"LG" and %"LG"\n", f1, f2);

  f1 = orbitalFp(Rpar-1e-8);
  f2 = orbitalFp(Rpar+1e-8);
  if(fabs(f1/f2-1.)>precision) Warning("  discontinuity at f'(Rpar): %"LG" and %"LG"\n", f1, f2);

  Message("done\n");
}

#ifdef ORBITAL_SQUARE_WELL2D_SIN

DOUBLE orbitalExactF(DOUBLE r) {
  if(r<Ro)
    return A1SW*BesselJ0(trial_Kappa*r);
  else if(r<Rpar)
    return A2SW*BesselK0(trial_k*r);
  else if(r<Lhalf)
    return A3SW-A4SW*Sin(trial_PI_L*r);
  else
    return Cpar;
}

// Fp = f'
DOUBLE orbitalExactFp(DOUBLE r) {
  if(r<Ro)
    return -A1SW*trial_Kappa*BesselJ1(trial_Kappa*r);
  else if(r<Rpar)
    return -A2SW*trial_k*BesselK1(trial_k*r);
  else if(r<Lhalf)
    return -A4SW*trial_PI_L*Cos(trial_PI_L*r);
  else
    return 0;
}

// Eloc = f" +1/r f' = u"/r
extern DOUBLE A1SW, A2SW, A3SW, A4SW, A5SW;
DOUBLE orbitalExactEloc(DOUBLE r) {

  if(r<Ro)
    return -A1SW*trial_Kappa2*BesselJ0(trial_Kappa*r);
  else if(r<Rpar)
    return A2SW*trial_k2*BesselK0(trial_k*r);
  else if(r<Lhalf) 
    return A4SW*trial_PI_L*Cos(trial_PI_L*r)*(trial_PI_L*tan(trial_PI_L*r)-1./r);
  else
    return 0.;
}

#endif

/************************** Construct Grid Fermion ************************/
void ConstructGridSquareWell2DPhonon(void) {
  static int first_time = ON;
  DOUBLE f1,f2;
  DOUBLE xmin, xmax, ymin, ymax, x, y, V;
  int search = ON;
  DOUBLE precision = 1e-8;

  trial_Kappa = sqrt(2.*m_mu*trial_Vo);
  trial_Kappa2 = 2.*m_mu*trial_Vo;

  if(first_time) {
    Message("  2D square well (phonons)\n");
    //Warning("  Changing units of Rpar to [L/2], %"LG" -> %"LG"\n", Rpar, Rpar*Lhalf);
    //Rpar *= Lhalf;
    if(Rpar<1) Error("  Rpar must be larger than 1\n");
    if(Rpar>Lhalf) Error("  Rpar must be smaller than L/2\n");
    first_time = OFF;
  }

  // define trial_k
  // Solve: k K1(k)/K0(k) -  ... = 0
  xmin = 1e-5;
  xmax = sqrt(2.*m_mu*trial_Vo)-1e-5;
  x = xmin;
  ymin = x*BesselK1(x)/BesselK0(x) - sqrt(2.*m_mu*trial_Vo-x*x)*BesselJ1(sqrt(2.*m_mu*trial_Vo-x*x))/BesselJ0(sqrt(2.*m_mu*trial_Vo-x*x));
  x = xmax;
  ymax = x*BesselK1(x)/BesselK0(x) - sqrt(2.*m_mu*trial_Vo-x*x)*BesselJ1(sqrt(2.*m_mu*trial_Vo-x*x))/BesselJ0(sqrt(2.*m_mu*trial_Vo-x*x));
  Message("  Solving equation for k ... \n");
  if(ymin*ymax > 0) Error("Can't construct the grid : No solution found");
  while(search) {
    x = (xmin+xmax)/2.;
    y = x*BesselK1(x)/BesselK0(x) - sqrt(2.*m_mu*trial_Vo-x*x)*BesselJ1(sqrt(2.*m_mu*trial_Vo-x*x))/BesselJ0(sqrt(2.*m_mu*trial_Vo-x*x));
    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }
    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y =  x*BesselK1(x)/BesselK0(x) - sqrt(2.*m_mu*trial_Vo-x*x)*BesselJ1(sqrt(2.*m_mu*trial_Vo-x*x))/BesselJ0(sqrt(2.*m_mu*trial_Vo-x*x));
      search = OFF;
    }
  }
  Message("  Solution is scattering momentum k = %"LF", error %"LE"\n", x, y);
  trial_k = x;
  trial_k2 = trial_k*trial_k;
  trial_Kappa = sqrt(2.*m_mu*trial_Vo-trial_k2);
  trial_Kappa2 = 2.*m_mu*trial_Vo-trial_k2;

  energy_shift = trial_k2;
  Warning("  Binding energy k^2=%"LE" (%"LE" EF) will be subtracted from the total energy !\n", energy_shift, energy_shift*energy_unit);

  // define A4
  // Solve: k K1(k Rpar)/K0(k Rpar) -  ... = 0
  search = ON;
  xmin = 1e-5;
  xmax = Rpar*100.;
  x = xmin;
  V = -trial_k*BesselK1(trial_k*Rpar)/BesselK0(trial_k*Rpar);
  V = -trial_k*BesselK1(trial_k*Rpar)/BesselK0(trial_k*Rpar);
  ymin = x * (-Exp(x/Rpar)/(Rpar*Rpar) + Exp(x/(L-Rpar))/((L-Rpar)*(L-Rpar))) / (Exp(x/Rpar)+Exp(x/(L-Rpar))) - V;
  x = xmax;
  ymax = x * (-Exp(x/Rpar)/(Rpar*Rpar) + Exp(x/(L-Rpar))/((L-Rpar)*(L-Rpar))) / (Exp(x/Rpar)+Exp(x/(L-Rpar))) - V;
  Message("  Solving equation for A4 ... \n");

  if(ymin*ymax > 0) Error("Can't construct the grid : No solution found");
  while(search) {
    x = (xmin+xmax)/2.;
    y = x * (-Exp(x/Rpar)/(Rpar*Rpar) + Exp(x/(L-Rpar))/((L-Rpar)*(L-Rpar))) / (Exp(x/Rpar)+Exp(x/(L-Rpar))) - V;
    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }
    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y = x * (-Exp(x/Rpar)/(Rpar*Rpar) + Exp(x/(L-Rpar))/((L-Rpar)*(L-Rpar))) / (Exp(x/Rpar)+Exp(x/(L-Rpar))) - V;
      search = OFF;
    }
  }
  Message("  Solution for decay constant is A4 = %"LF", error %"LE"\n", x, y);
  A4SW = x;

  A3SW = 1./(2.*Exp(A4SW/Lhalf));
  A2SW = A3SW*(Exp(A4SW/Rpar)+Exp(A4SW/(L-Rpar)))/BesselK0(trial_k*Rpar);
  A1SW = A2SW*BesselK0(trial_k) / BesselJ0(trial_Kappa);

  A5SW = 1.;
  //A5SW = 1./orbitalExactF(0.);
  A5SW = sqrt(orbitalExactF(Lhalf)/orbitalExactF(0.));
  A1SW *= A5SW;
  A2SW *= A5SW;
  A3SW *= A5SW;

  Message("  Testing continuity ... ");
  f1 = orbitalF(1.-1e-8);
  f2 = orbitalF(1.+1e-8); 
  if(fabs(f1/f2-1.)>1e-3) Error("  discontinuity at f(1): %"LG" and %"LG"\n", f1, f2);

  f1 = orbitalFp(1.-1e-8);
  f2 = orbitalFp(1.+1e-8); 
  if(fabs(f1/f2-1.)>1e-3) Error("  discontinuity at f'(1): %"LG" and %"LG"\n", f1, f2);

  f1 = orbitalF(Lhalf-1e-8);
  f2 = orbitalF(Lhalf+1e-8);
  if(fabs(f1/f2-1.)>1e-3 && f2 != 0.) Error("  discontinuity at f(1): %"LG" and %"LG"\n", f1, f2);

  f1 = orbitalFp(Lhalf-1e-8);
  f2 = orbitalFp(Lhalf+1e-8);
  if(fabs(f1/f2-1.)>precision) Warning("  discontinuity at f'(L/2): %"LG" and %"LG"\n", f1, f2);

  f1 = orbitalFp(Rpar-1e-8);
  f2 = orbitalFp(Rpar+1e-8);
  if(fabs(f1/f2-1.)>precision) Warning("  discontinuity at f'(Rpar): %"LG" and %"LG"\n", f1, f2);

  Message("done\n");
}

#ifdef ORBITAL_SQUARE_WELL2D_PHONON
DOUBLE orbitalExactF(DOUBLE r) {
  if(r<Ro)
    return A1SW*BesselJ0(trial_Kappa*r);
  else if(r<Rpar)
    return A2SW*BesselK0(trial_k*r);
  else if(r<Lhalf)
    return A3SW*(Exp(A4SW/r)+Exp(A4SW/(L-r)));
  else
    return A5SW;
}

// Fp = f'
DOUBLE orbitalExactFp(DOUBLE r) {
  if(r<Ro)
    return -A1SW*trial_Kappa*BesselJ1(trial_Kappa*r);
  else if(r<Rpar)
    return -A2SW*trial_k*BesselK1(trial_k*r);
  else if(r<Lhalf)
    return A3SW*A4SW*(-Exp(A4SW/r)/(r*r)+Exp(A4SW/(L-r))/((L-r)*(L-r)));
  else
    return 0;
}

// Eloc = f" +1/r f' = u"/r
extern DOUBLE A1SW, A2SW, A3SW, A4SW, A5SW;
DOUBLE orbitalExactEloc(DOUBLE r) {
  DOUBLE Lmr;

  if(r<Ro)
    return -A1SW*trial_Kappa2*BesselJ0(trial_Kappa*r);
  else if(r<Rpar)
    return A2SW*trial_k2*BesselK0(trial_k*r);
  else if(r<Lhalf) {
    Lmr = L - r;
    return A3SW*A4SW*((A4SW+r)*Exp(A4SW/r)/(r*r*r*r) + (A4SW-r+L*L/r)*Exp(A4SW/Lmr)/(Lmr*Lmr*Lmr*Lmr));
  }
  else
    return 0.;
}
#endif

#ifdef ORBITAL_HYDROGEN_1S
DOUBLE orbitalExactF(DOUBLE r) {
  return Exp(-r/(2.*excitonRadius));
}

// Fp = f'
DOUBLE orbitalExactFp(DOUBLE r) {
  return -0.5/excitonRadius*Exp(-r/(2.*excitonRadius));
}

// Eloc = f" +2/r f'
extern DOUBLE A1SW, A2SW, A3SW, A4SW, A5SW;
DOUBLE orbitalExactEloc(DOUBLE r) {
  return -(4.*excitonRadius - r)/(4.*Exp(r/(2.*excitonRadius))*excitonRadius*excitonRadius*r);
}
#endif

