/*spline.c*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "spline.h"
#include "main.h"
#include "utils.h"
#include "trial.h"
#include "memory.h"
#include "compatab.h"
#include "mymath.h"

#define SPLINE_EPSILON 1e-10

/****************************** SaveWaveFunctionSpline *****************************/
void SaveWaveFunctionSpline(struct Grid *G, char *file_out) {
  int i;
  int Nspline_export = 1000;
  FILE *out;
  DOUBLE x, yInter, ypInter, yppInter;
  static int first_time = ON;

  out = fopen(file_out, "w");

  if(first_time) Message("Saving wavefunction (spline).\n");

  fprintf(out, "#x f fp fpp\n");
  fprintf(out, "#fine grid\n"); // fine grid
  if(Nspline_export>G->size) Nspline_export = G->size;
  for(i=0; i<100; i++) { 
    x = G->min + 0.1*G->step*(DOUBLE) i;
    InterpolateSpline(G, x, &yInter, &ypInter, &yppInter);
#ifdef INTERPOLATE_LOG
    yInter = Exp(InterpolateSplineU(G, x));
    ypInter = InterpolateSplineFp(G, x);
    yppInter = InterpolateSplineE(G, x);
#endif
    fprintf(out, "%.15"LE" %.15"LE" %.15"LE" %.15"LE"\n", x, yInter, ypInter, yppInter);
  }
  fprintf(out, "\n\n");

  fprintf(out, "#spline tail\n"); // spline tail
  for(i = 0; i<100; i++) {
    x = G->max - 10 * G->step + 0.1*G->step*(DOUBLE)(i - 50);
    //x = G->min + G->step*(DOUBLE)i;
    InterpolateSpline(G, x, &yInter, &ypInter, &yppInter);
#ifdef INTERPOLATE_LOG
    yInter = Exp(InterpolateSplineU(G, x));
    ypInter = InterpolateSplineFp(G, x);
    yppInter = InterpolateSplineE(G, x);
#endif
    fprintf(out, "%.15"LE" %.15"LE" %.15"LE" %.15"LE"\n", x, yInter, ypInter, yppInter);
  }
  fprintf(out, "\n\n");

  fprintf(out, "#spline elements\n");
  for(i=0; i<Nspline_export; i++) {
    x = G->min + G->step*(DOUBLE) i;
    InterpolateSpline(G, x, &yInter, &ypInter, &yppInter);
#ifdef INTERPOLATE_LOG
    yInter = Exp(InterpolateSplineU(G, x));
    ypInter = InterpolateSplineFp(G, x);
    yppInter = InterpolateSplineE(G, x);
#endif
    fprintf(out, "%.15"LE" %.15"LE" %.15"LE" %.15"LE"\n", x, yInter, ypInter, yppInter);
  }
  fclose(out);

  first_time = OFF;
}

/************************************ spline construction *******************************/
void SplineConstruct(struct Grid *G, DOUBLE yp1, DOUBLE ypn) {
  int i,k;
  DOUBLE p,qn,sig,un;
  int n;

  n = G->size;

#ifndef INTERPOLATE_LOG
  for(i=0;i<n;i++) { // check if Jastrow is non negative
    if(G->f[i]>0) {
      // do nothing
    }
    else {
      Warning("  Spline Construction : x = %lf, f = %lf is not positive setting to zero\n", G->x[i], G->f[i]);
      G->f[i] = 0.; // set to zero
    }
  }
#endif

  //G->fpp[0] = G->fp[0] = 0.; // natural spline
  if(yp1>1e30) // zero second derivative
    G->fpp[0] = G->fp[0]=0.;
  else { // first derivative equal to yp1
    G->fpp[0] = -0.5;
    G->fp[0] = (3./(G->x[1]-G->x[0]))*((G->f[1]-G->f[0])/(G->x[1]-G->x[0])-yp1);
  }

  for(i=1;i<n-1;i++) {
    sig=(G->x[i]-G->x[i-1])/(G->x[i+1]-G->x[i-1]);
    p=sig*G->fpp[i-1]+2.0;
    G->fpp[i]=(sig-1.0)/p;
    G->fp[i]=(G->f[i+1]-G->f[i])/(G->x[i+1]-G->x[i]) - (G->f[i]-G->f[i-1])/(G->x[i]-G->x[i-1]);
    G->fp[i]=(6.0*G->fp[i]/(G->x[i+1]-G->x[i-1])-sig*G->fp[i-1])/p;
  }

  if(ypn > 1e30)
    qn = un = 0.;
  else {
    qn=0.5;
    un=(3.0/(G->x[n-1]-G->x[n-2]))*(ypn-(G->f[n-1]-G->f[n-2])/(G->x[n-1]-G->x[n-2]));
  }
  G->fpp[n-1]=(un-qn*G->fp[n-2])/(qn*G->fpp[n-2]+1.);
  for(k=n-2; k>=0; k--) G->fpp[k]=G->fpp[k]*G->fpp[k+1]+G->fp[k];
}

/************************************ interpolate spline  *******************************/
void InterpolateSpline(struct Grid *G, DOUBLE r, DOUBLE *y, DOUBLE *yp, DOUBLE *ypp) {
  DOUBLE a,b;
  int i;

  /*int klo,khi,k; // uncomment for non uniform grid
  DOUBLE h;
  
  klo=0;
  khi=G->size-1;
  while(khi-klo > 1) {
    //k=(khi+klo) >> 1;
    k = (int)(0.5*((DOUBLE)(khi+klo)));
    if(G->x[k]>r)
      khi=k;
    else 
      klo=k;
  }
  h=G->x[khi]-G->x[klo];
  i = klo;
  if(h == 0.) Warning("Bad input to InterpolateSpline");*/

  // uniform grid
  if(r<=G->min) r = G->min; // adjust lower bound
  //i = (int) ((r-G->min)*G->I_step + 0.5);
  //i = (int) ((r-G->min)*G->I_step);
  i = (int) ((r-G->min)*G->I_step+SPLINE_EPSILON); // shift by a small amount for the check at points G->x[i]

//#ifdef SECURE
  if(i<G->size-1 && (r+SPLINE_EPSILON<G->x[i] || r-SPLINE_EPSILON>G->x[i+1])) 
    Warning("Interp. spline check failed i=%i (%"LE " <%"LE " <%"LE " )\n", i, G->x[i], r, G->x[i+1]);
//#endif
  if(i >= G->size-1) i = G->size-2; // adjust upper bound

  a = (G->x[i+1]-r)*G->I_step;
  b = (r-G->x[i])*G->I_step;

  // (3.3.3)
  *y   = a*G->f[i] + b*G->f[i+1] + ((a*a*a-a)*G->fpp[i] + (b*b*b-b)*G->fpp[i+1])*(G->step*G->step)/6.;
  *yp  = (G->f[i+1]-G->f[i])*G->I_step + ((1.-3.*a*a)*G->fpp[i]+(3.*b*b-1.)*G->fpp[i+1])*G->step/6.;
  *ypp = a*G->fpp[i] + b*G->fpp[i+1];
}

/************************ Interpolate U ************************************/
// U = ln(f)
DOUBLE InterpolateSplineU(struct Grid *G, DOUBLE r) {
  DOUBLE a,b,f;
  int i;

#ifdef SPLINE_ANALYTIC_RIGHT_HAND_SIDE
   if(r>G->max-10*G->step) {
#ifdef JASTROW_RIGHT_BOUNDARY_FROM_BOUND_STATE_SOLUTION
#ifdef TRIAL_3D
    f = exp(-G->k*r)/r;
#endif
#ifdef TRIAL_2D
    f = K0(G->k*r);
#endif
#ifdef TRIAL_1D
    f = exp(-G->k*r);
#endif
#endif

#ifdef JASTROW_RIGHT_BOUNDARY_PHONONS
#ifdef TRIAL_3D
    f = -G->Apar/(r*r) - G->Apar/((L-r)*(L-r)) + 2.*G->Apar/Lhalf2;
#endif
#ifdef TRIAL_2D
    f = - G->Apar/r - G->Apar/(L-r) + 2.*G->Apar/Lhalf;
#endif
#ifdef TRIAL_1D
    f = pow(fabs(Sin(PI*r/L)), 1./G->Apar);
    f = Log(f);
#endif
#endif

#ifdef JASTROW_RIGHT_BOUNDARY_INVERSE_EXPONENTIAL
    f = G->Apar * (Lhalf - r) * (Lhalf - r) / (2.*Lhalf*r*r);
#endif

#ifdef JASTROW_RIGHT_BOUNDARY_POLARON_GPE
    f = G->Atrial*(1. + G->Bpar*exp(-G->Apar*r) + G->Bpar*exp(G->Apar*(r - L)));
    f = Log(f);
#endif

    if(isfinite(f))
      return f;
    else
      return 10*Log(DBL_EPSILON);
  }
#endif // SPLINE_ANALYTIC_RIGHT_HAND_SIDE

  if(r<=G->min) r = G->min; // adjust lower bound
  i = (int) ((r-G->min)*G->I_step+SPLINE_EPSILON); // shift by a small amount for the check at points G->x[i]
#ifdef SECURE
  if(i<G->size-1 && (r+SPLINE_EPSILON<G->x[i] || r-SPLINE_EPSILON>G->x[i+1])) Warning("  Interpolate spline U check failed, i=%i (%"LE " < %"LE "  <%"LE " )\n", i, G->x[i], r, G->x[i+1]);
#endif
  //if(i >= G->size-1) i = G->size-2; // adjust upper bound
  if(i >= G->size-1) 
#ifdef INTERPOLATE_LOG // u(r)
    //return G->f[G->size-1];
    return 0.;
#else
    //return Log(G->f[G->size-1]);
    return 0.;
#endif

  a = (G->x[i+1]-r)*G->I_step;
  b = (r-G->x[i])*G->I_step;

  f = a*G->f[i] + b*G->f[i+1] + ((a*a*a-a)*G->fpp[i] + (b*b*b-b)*G->fpp[i+1])*(G->step*G->step)/6.;

#ifdef INTERPOLATE_LOG // u(r)
  return f;
#else // f(r)
#ifdef SECURE
  if(f<0) Warning("Interpolate spline, ln() of negative argument\n");
#endif
  if(f<0) return -1e8;

  return Log(f);
#endif
}

/************************ InterpolateFp ************************************/
// fp = f'/f
DOUBLE InterpolateSplineFp(struct Grid *G, DOUBLE r) {
  DOUBLE a,b;
  int i;
#ifndef INTERPOLATE_LOG
  DOUBLE fp, f;
#endif

#ifdef SPLINE_ANALYTIC_RIGHT_HAND_SIDE
  if(r>G->max-10*G->step) {
#ifdef JASTROW_RIGHT_BOUNDARY_FROM_BOUND_STATE_SOLUTION
#ifdef TRIAL_1D
    return -G->k;
#endif
#ifdef TRIAL_2D
    return -G->k * K1(G->k*r) / K0(G->k*r);
#endif
#ifdef TRIAL_3D
    return -G->k - 1./r;
#endif
#endif

#ifdef JASTROW_RIGHT_BOUNDARY_PHONONS
#ifdef TRIAL_1D
    return PI/(L*Tg(PI*r/L))/G->Apar;
#endif
#ifdef TRIAL_2D
    return G->Apar*(1./(r*r)-1./((L-r)*(L-r)));
#endif
#ifdef TRIAL_3D
    return 2.*G->Apar*(1./(r*r*r)-1./((L-r)*(L-r)*(L-r)));
#endif
#endif

#ifdef JASTROW_RIGHT_BOUNDARY_INVERSE_EXPONENTIAL
    return G->Apar*(r - Lhalf)/(r*r*r);
#endif

#ifdef JASTROW_RIGHT_BOUNDARY_POLARON_GPE
    return (G->Apar*G->Bpar*(-exp(G->Apar*L) + exp(2.*G->Apar*r)))/(exp(G->Apar*(L + r)) + G->Bpar*(exp(G->Apar*L) + exp(2.*G->Apar*r)));
#endif
  }
#endif

  if(r<=G->min) r = G->min; // adjust lower bound
  i = (int) ((r-G->min)*G->I_step+SPLINE_EPSILON); // shift by a small amount for the check at points G->x[i]
#ifdef SECURE
  if(i<G->size-1 && (r+SPLINE_EPSILON<G->x[i] || r-SPLINE_EPSILON>G->x[i+1])) Warning("  Interpolate spline Fp check failed, i=%i (%"LE " < %"LE "  <%"LE " )\n", i, G->x[i], r, G->x[i+1]);
#endif
  //if(i >= G->size-1) i = G->size-2; // adjust upper bound
  if(i >= G->size-1) return 0.;

  a = (G->x[i+1]-r)*G->I_step;
  b = (r-G->x[i])*G->I_step;

#ifdef INTERPOLATE_LOG // f'/f = u'
  return (G->f[i+1]-G->f[i])*G->I_step + ((1.-3.*a*a)*G->fpp[i]+(3.*b*b-1.)*G->fpp[i+1])*G->step/6.;
#else // f'/f
  f = a*G->f[i] + b*G->f[i+1] + ((a*a*a-a)*G->fpp[i] + (b*b*b-b)*G->fpp[i+1])*(G->step*G->step)/6.;
  fp  = (G->f[i+1]-G->f[i])*G->I_step + ((1.-3.*a*a)*G->fpp[i]+(3.*b*b-1.)*G->fpp[i+1])*G->step/6.;
#ifdef SECURE
  if(f<0) Warning("Interpolate spline Fp of negative argument\n");
#endif

  if(f<0) return 0;

  return fp/f;
#endif
}

/************************ InterpolateFp ************************************/
// Eloc = -(f" +2/r f')/f+(f'/f)^2
DOUBLE InterpolateSplineE(struct Grid *G, DOUBLE r) {
  DOUBLE a,b;
  int i;
#ifdef INTERPOLATE_LOG
  DOUBLE up,upp;
#else
  DOUBLE f,fp,fpp;
#endif

#ifdef SPLINE_ANALYTIC_RIGHT_HAND_SIDE
  if(r>G->max-10*G->step) {
#ifdef JASTROW_RIGHT_BOUNDARY_FROM_BOUND_STATE_SOLUTION
#ifdef TRIAL_3D
    return (1.+2.*G->k*r)/(r*r);
#endif
#ifdef TRIAL_2D
    DOUBLE Fp;
    Fp = -G->k*K1(G->k*r) / K0(G->k*r);
    return -G->k*G->k + Fp*Fp;
#endif
#ifdef TRIAL_1D
    return 0.;
#endif
#endif

#ifdef JASTROW_RIGHT_BOUNDARY_PHONONS
#ifdef TRIAL_1D
    DOUBLE c;
    c = 1. / Tg(PI*r/L);
    return (PI*PI/(L*L))*(1.+c*c)/G->Apar;
#endif
#ifdef TRIAL_2D
    return (G->Apar*L*(L*L - 3.*L*r + 4.*r*r))/((L - r)*(L - r)*(L - r)*r*r*r);
#endif
#ifdef TRIAL_3D
    return 2.*G->Apar*(L*L*L*L - 4.*L*L*L*r + 6.*L*L*r*r - 2.*L*r*r*r + 2.*r*r*r*r)/(r*r*r*r*(L-r)*(L-r)*(L-r)*(L-r));
#endif
#endif

#ifdef JASTROW_RIGHT_BOUNDARY_INVERSE_EXPONENTIAL
#ifdef TRIAL_1D
    return G->Apar*(2.*r - 3.*Lhalf)/(r*r*r*r);
#endif
#ifdef TRIAL_2D
    return G->Apar*(r - 2.*Lhalf)/(r*r*r*r);
#endif
#ifdef TRIAL_3D
    return G->Apar*(-Lhalf)/(r*r*r*r);
#endif
#endif

#ifdef JASTROW_RIGHT_BOUNDARY_POLARON_GPE
#ifdef TRIAL_1D
    return -(G->Apar*G->Apar*G->Bpar*exp(G->Apar*(L + r))*(exp(G->Apar*L) + 4.*G->Bpar*exp(G->Apar*r) + exp(2.*G->Apar*r)))/((exp(G->Apar*(L + r)) + G->Bpar*(exp(G->Apar*L) + exp(2.*G->Apar*r)))*(exp(G->Apar*(L + r)) + G->Bpar*(exp(G->Apar*L) + exp(2.*G->Apar*r))));
#endif
#ifdef TRIAL_2D
    return -((G->Apar*G->Bpar*(G->Bpar*(-exp(2.*G->Apar*L) + exp(4.*G->Apar*r) + 4.*G->Apar*exp(G->Apar*(L + 2.*r))*r) + exp(G->Apar*(L + r))*(exp(G->Apar*L)*(-1. + G->Apar*r) + exp(2.*G->Apar*r)*(1. + G->Apar*r))))/((exp(G->Apar*(L + r)) + G->Bpar*(exp(G->Apar*L) + exp(2.*G->Apar*r)))*(exp(G->Apar*(L + r)) + G->Bpar*(exp(G->Apar*L) + exp(2.*G->Apar*r)))*r));
#endif
#ifdef TRIAL_3D
    return -((G->Apar*G->Bpar*(G->Bpar*(-2.*exp(2.*G->Apar*L) + 2.*exp(4.*G->Apar*r) + 4.*G->Apar*exp(G->Apar*(L + 2.*r))*r) + exp(G->Apar*(L + r))*(exp(G->Apar*L)*(-2. + G->Apar*r) + exp(2.*G->Apar*r)*(2. + G->Apar*r))))/((exp(G->Apar*(L + r)) + G->Bpar*(exp(G->Apar*L) + exp(2.*G->Apar*r)))*(exp(G->Apar*(L + r)) + G->Bpar*(exp(G->Apar*L) + exp(2.*G->Apar*r)))*r));
#endif
#endif

  }
#endif

  if(r == 0) return 0.;

  if(r<=G->min) r = G->min; // adjust lower bound
  i = (int) ((r-G->min)*G->I_step+SPLINE_EPSILON); // shift by a small amount for the check at points G->x[i]
#ifdef SECURE
  if(i<G->size-1 && (r+SPLINE_EPSILON<G->x[i] || r-SPLINE_EPSILON>G->x[i+1])) Warning("  Interpolate spline E check failed, i=%i (%"LE " < %"LE "  <%"LE " )\n", i, G->x[i], r, G->x[i+1]);
#endif
  //if(i >= G->size-1) i = G->size-2; // adjust upper bound
  if(i >= G->size-1) return 0.;

  a = (G->x[i+1]-r)*G->I_step;
  b = (r-G->x[i])*G->I_step;

#ifdef INTERPOLATE_LOG // (-f" + 2/r f')/f + (f'/f)^2 = - u" - u' 2/r
  up  = (G->f[i+1]-G->f[i])*G->I_step + ((1.-3.*a*a)*G->fpp[i]+(3.*b*b-1.)*G->fpp[i+1])*G->step/6.;
  upp = a*G->fpp[i] + b*G->fpp[i+1];

#ifdef TRIAL_1D
  return - upp;
#endif
#ifdef TRIAL_2D
  return - upp - up/r;
#endif
#ifdef TRIAL_3D
  return - upp - 2.*up/r;
#endif
#else // (-f" + 2/r f')/f + (f'/f)^2
  f = a*G->f[i] + b*G->f[i+1] + ((a*a*a-a)*G->fpp[i] + (b*b*b-b)*G->fpp[i+1])*(G->step*G->step)/6.;
  fp  = (G->f[i+1]-G->f[i])*G->I_step + ((1.-3.*a*a)*G->fpp[i]+(3.*b*b-1.)*G->fpp[i+1])*G->step/6.;
  fpp = a*G->fpp[i] + b*G->fpp[i+1];

  fp /= f;
  fpp  /= f;

  if(r==0.) return 0.;
#ifdef SECURE
  if(f<0) Warning("Interpolate spline E of negative argument\n");
#endif

  if(f<0) return 0;

#ifdef TRIAL_1D
  return -fpp + fp*fp;
#endif
#ifdef TRIAL_2D
  return -fpp - fp/r + fp*fp;
#endif
#ifdef TRIAL_3D
  return -fpp - 2.*fp/r + fp*fp;
#endif

#endif
}

/************************ Interpolate f ************************************/
DOUBLE InterpolateSplinef(struct Grid *G, DOUBLE r) {
  DOUBLE a, b, f;
  int i;

#ifdef SPLINE_ANALYTIC_RIGHT_HAND_SIDE
  if(r>G->max-10*G->step) { // 2D bound state
#ifdef JASTROW_RIGHT_BOUNDARY_FROM_BOUND_STATE_SOLUTION
#ifdef TRIAL_1D
    f = exp(-G->k*r);
#endif
#ifdef TRIAL_2D
    f = K0(G->k*r);
#endif
#ifdef TRIAL_3D
    f = exp(-G->k*r)/r;
#endif
#endif

#ifdef JASTROW_RIGHT_BOUNDARY_PHONONS
#ifdef TRIAL_1D
    f = pow(fabs(Sin(PI*r/L)), 1./G->Apar);
#endif
#ifdef TRIAL_2D
    f = exp(-G->Apar/(r*r) - G->Apar/((L-r)*(L-r)) + 2.*G->Apar/Lhalf2);
#endif
#ifdef TRIAL_3D
    f = exp(-G->Apar/(r*r) - G->Apar/((L-r)*(L-r)) + 2.*G->Apar/Lhalf2);
#endif
#endif

#ifdef JASTROW_RIGHT_BOUNDARY_INVERSE_EXPONENTIAL
    f = exp(G->Apar * (Lhalf - r) * (Lhalf - r) / (2.*Lhalf*r*r));
#endif

#ifdef JASTROW_RIGHT_BOUNDARY_POLARON_GPE
    f = G->Atrial*(1. + G->Bpar*exp(-G->Apar*r) + G->Bpar*exp(G->Apar*(r - L)));
#endif

    return (f>1e-100)?(f) : (1e-100);
  }
#endif

  if(r <= G->min) r = G->min; // adjust lower bound
  i = (int)((r - G->min)*G->I_step + SPLINE_EPSILON); // shift by a small amount for the check at points G->x[i]
#ifdef SECURE
  if (i<G->size - 1 && (r + SPLINE_EPSILON<G->x[i] || r - SPLINE_EPSILON>G->x[i + 1])) Warning("  Interpolate spline U check failed, i=%i (%"LE " < %"LE "  <%"LE " )\n", i, G->x[i], r, G->x[i + 1]);
#endif
  if(i >= G->size - 1) return 1.;

  a = (G->x[i + 1] - r)*G->I_step;
  b = (r - G->x[i])*G->I_step;

  f = a*G->f[i] + b*G->f[i + 1] + ((a*a*a - a)*G->fpp[i] + (b*b*b - b)*G->fpp[i + 1])*(G->step*G->step) / 6.;
  return f;
}

/************************ Interpolatefp ************************************/
// fp = f'
DOUBLE InterpolateSplinefp(struct Grid *G, DOUBLE r) {
  DOUBLE a, b;
  int i;

#ifdef SPLINE_ANALYTIC_RIGHT_HAND_SIDE
  if(r>G->max-10*G->step) {
#ifdef JASTROW_RIGHT_BOUNDARY_FROM_BOUND_STATE_SOLUTION
#ifdef TRIAL_1D
    return -G->k * exp(-G->k*r);
#endif
#ifdef TRIAL_2D
    return -G->k * K1(G->k*r);
#endif
#ifdef TRIAL_3D
    return  -exp(-G->k*r) * (1.+G->k*r) / (r*r);
#endif
#endif

#ifdef JASTROW_RIGHT_BOUNDARY_PHONONS
#ifdef TRIAL_1D
    return pow(fabs(Sin(PI*r/L)), 1./G->Apar-1.) * Cos(PI*r/L) * PI/(L*G->Apar);
#endif
#ifdef TRIAL_2D
    return exp(-G->Apar/(r*r) - G->Apar/((L-r)*(L-r)) + 2.*G->Apar/Lhalf2) * G->Apar*(1./(r*r)-1./((L-r)*(L-r)));
#endif
#ifdef TRIAL_3D
    return 2.*G->Apar*exp(-G->Apar/(r*r) - G->Apar/((L-r)*(L-r)) + 2.*G->Apar/Lhalf2) * (1./(r*r*r)- 1./((L-r)*(L-r)*(L-r)));
#endif
#endif

#ifdef JASTROW_RIGHT_BOUNDARY_INVERSE_EXPONENTIAL
    return exp(G->Apar * (Lhalf - r) * (Lhalf - r) / (2.*Lhalf*r*r)) * G->Apar*(r - Lhalf)/(r*r*r);
#endif

#ifdef JASTROW_RIGHT_BOUNDARY_POLARON_GPE
  return G->Apar*G->Atrial*G->Bpar*(-exp(-(G->Apar*r)) + exp(G->Apar*(-L + r)));
#endif

  }
#endif

  if(r <= G->min) r = G->min; // adjust lower bound
  i = (int)((r - G->min)*G->I_step + SPLINE_EPSILON); // shift by a small amount for the check at points G->x[i]
#ifdef SECURE
  if(i<G->size - 1 && (r + SPLINE_EPSILON<G->x[i] || r - SPLINE_EPSILON>G->x[i + 1])) Warning("  Interpolate spline Fp check failed, i=%i (%"LE " < %"LE "  <%"LE " )\n", i, G->x[i], r, G->x[i + 1]);
#endif
  if(i >= G->size - 1) return 0.;

  a = (G->x[i + 1] - r)*G->I_step;
  b = (r - G->x[i])*G->I_step;

  return (G->f[i + 1] - G->f[i])*G->I_step + ((1. - 3.*a*a)*G->fpp[i] + (3.*b*b - 1.)*G->fpp[i + 1])*G->step / 6.;
}

/************************ Interpolatefpp ************************************/
// -f"-(D-1)f�/r
DOUBLE InterpolateSplinefpp(struct Grid *G, DOUBLE r) {
  DOUBLE a, b;
  int i;
  DOUBLE f, fp, fpp;

#ifdef SPLINE_ANALYTIC_RIGHT_HAND_SIDE
  if(r>G->max-10*G->step) {
#ifdef JASTROW_RIGHT_BOUNDARY_FROM_BOUND_STATE_SOLUTION
#ifdef TRIAL_1D
    return -G->k*G->k * exp(-G->k*r);
#endif
#ifdef TRIAL_2D
    return -G->k*G->k * K0(G->k * r);
#endif
#ifdef TRIAL_3D
    return -G->k*G->k * exp(-G->k*r) / r;
#endif
#endif

#ifdef JASTROW_RIGHT_BOUNDARY_PHONONS
#ifdef TRIAL_1D
    return -pow(fabs(Sin(PI*r/L)), 1./G->Apar-2.) *(1. - 2.*G->Apar + Cos(2.*PI*r/L)) * PI*PI/(2.*L*L*G->Apar*G->Apar);
#endif
#ifdef TRIAL_2D
    return -((G->Apar*exp(G->Apar*(4./L+L/(r*(-L+r))))*L*(G->Apar*L*(L-2.*r)*(L-2.*r)-(L-r)*r*(L*L-3.*L*r+4.*r*r)))/((L-r)*(L-r)*(L-r)*(L-r)*r*r*r*r));
#endif
#ifdef TRIAL_3D
    return (-2.*G->Apar*exp(G->Apar*(8./(L*L)-1./((L-r)*(L-r))-1./(r*r)))*(2.*G->Apar*(L-2.*r)*(L-2.*r)*((L*L)-L*r+r*r)*((L*L)-L*r+r*r)-(L-r)*(L-r)*r*r*(L*L*L*L-4.*L*L*L*r+6.*L*L*r*r-2.*L*r*r*r+2.*r*r*r*r)))/((L-r)*(L-r)*(L-r)*(L-r)*(L-r)*(L-r)*r*r*r*r*r*r);
#endif
#endif

#ifdef JASTROW_RIGHT_BOUNDARY_INVERSE_EXPONENTIAL
#ifdef TRIAL_1D
    return G->Apar*(2.*r - 3.*Lhalf)/(r*r*r*r) * exp(G->Apar * (Lhalf - r) * (Lhalf - r) / (2.*Lhalf*r*r));
#endif
#ifdef TRIAL_2D
    return G->Apar*(r - 2.*Lhalf)/(r*r*r*r) * exp(G->Apar * (Lhalf - r) * (Lhalf - r) / (2.*Lhalf*r*r));
#endif
#ifdef TRIAL_3D
    return G->Apar*(-Lhalf)/(r*r*r*r) * exp(G->Apar * (Lhalf - r) * (Lhalf - r) / (2.*Lhalf*r*r));
#endif
#endif

#ifdef JASTROW_RIGHT_BOUNDARY_POLARON_GPE
#ifdef TRIAL_1D
  return -((G->Apar*G->Apar*G->Atrial*G->Bpar*(exp(G->Apar*(L - r)) + exp(G->Apar*r)))/exp(G->Apar*L));
#endif
#ifdef TRIAL_2D
  return -((G->Apar*G->Atrial*G->Bpar*(exp(G->Apar*(L - r))*(-1. + G->Apar*r) + exp(G->Apar*r)*(1. + G->Apar*r)))/(exp(G->Apar*L)*r));
#endif
#ifdef TRIAL_3D
  return -((G->Apar*G->Atrial*G->Bpar*(exp(G->Apar*(L - r))*(-2. + G->Apar*r) + exp(G->Apar*r)*(2. + G->Apar*r)))/(exp(G->Apar*L)*r));
#endif
#endif

  }
#endif

  if(r == 0) return 0.;

  if(r <= G->min) r = G->min; // adjust lower bound
  i = (int)((r - G->min)*G->I_step + SPLINE_EPSILON); // shift by a small amount for the check at points G->x[i]
#ifdef SECURE
  if (i<G->size - 1 && (r + SPLINE_EPSILON<G->x[i] || r - SPLINE_EPSILON>G->x[i + 1])) Warning("  Interpolate spline E check failed, i=%i (%"LE " < %"LE "  <%"LE " )\n", i, G->x[i], r, G->x[i + 1]);
#endif
  //if(i >= G->size-1) i = G->size-2; // adjust upper bound
  if (i >= G->size - 1) return 0.;

  a = (G->x[i + 1] - r)*G->I_step;
  b = (r - G->x[i])*G->I_step;

  f = a*G->f[i] + b*G->f[i + 1] + ((a*a*a - a)*G->fpp[i] + (b*b*b - b)*G->fpp[i + 1])*(G->step*G->step) / 6.;
  fp = (G->f[i + 1] - G->f[i])*G->I_step + ((1. - 3.*a*a)*G->fpp[i] + (3.*b*b - 1.)*G->fpp[i + 1])*G->step / 6.;
  fpp = a*G->fpp[i] + b*G->fpp[i + 1];

  if (r == 0.) return 0.;
#ifdef SECURE
  if (f<0) Warning("Interpolate spline E of negative argument\n");
#endif

#ifdef TRIAL_1D
  return -fpp;
#endif
#ifdef TRIAL_2D
  return -fpp - fp / r;
#endif
#ifdef TRIAL_3D
  return -fpp - 2.*fp / r;
#endif
}

/************************** Construct Grid Spline 2 body solution  **************************/
// construct 2body solution for use with spline or linear interpolation
void ConstructGridSpline2Body_ij(struct Grid *G, DOUBLE(*InteractionEnergy)(DOUBLE x), int spin1, int spin2) {
  // choose boundary condition of the scattering problem
  int size = 1000;
  DOUBLE E = 0.; // scattering energy
  DOUBLE xmin, xmax, ymin, ymax, x, y;
  int search = ON;
  DOUBLE precision = 1e-14; //1e-8
  int Nitermax = 1000000; // maximal allowed number of iterations
  DOUBLE energy_step = 1.1; // 1.01 to find ymin*ymax<0
  //DOUBLE Epotcutoff = 1e3; // 1e3 maximal allowed potential energy for V(xmin)
  DOUBLE Eorigin; // V(0)
  int iter = 0;
  int i;
  int function_was_negative = OFF;

//#ifdef INTERPOLATE_LOG // direct solution for u(r) = ln f(r) is not yet implemented
//  int flag_lnf_instead_of_f = ON;
//#else
  int flag_lnf_instead_of_f = OFF;
//#endif

  Message("  Jastrow term will be constructed as 2-body scattering solution with for interaction potential\n");
  Message("  Spline size grid_trial=%i\n", grid_trial);
  size = grid_trial;
  AllocateWFGrid(G, size);

  G->size = size;
  G->max = Lhalf; // BC present

#ifdef BC_ABSENT
  if(spin1 == spin2) {
    Message("  maximal distance for the spline Rpar = %lf\n", Rpar);
    G->max = Rpar;
    if(Rpar<0) Error("  Incorrect Rpar = %lf < 0\n", Rpar);
    Lhalf = Rpar;
  }
  else {
    Message("  maximal distance for the spline Rpar12 = %lf\n", Rpar12);
    G->max = Rpar12;
    Warning("  changing L/2 = Rpar12 = %lf\n", Rpar12);
    Lhalf = Rpar12;
  }
  L = 2.*Lhalf;
  Lhalf2 = Lhalf*Lhalf;
  Lwf = L;
  Lhalfwf = L / 2;
  //Lhalfwf2 = Lhalfwf*Lhalfwf;

#else // BC present
  Message("  maximal distance for the spline Rpar12 x L/2 = %lf\n", Rpar12*Lhalf);

#ifdef TRIAL_TWO_BODY_SCATTERING_NUMERICAL12
  if(spin1 != spin2) {
    G->max = Lhalf*Rpar12;
    if(Rpar12<0) Error("  Incorrect Rpar12 = %lf < 0\n", Rpar12);
  }
#endif

#ifdef TRIAL_TWO_BODY_SCATTERING_NUMERICAL12_SYM
  if(spin1 != spin2) {
    G->max = Lhalf*Rpar12;
    if(Rpar12<0) Error("  Incorrect Rpar12 = %lf < 0\n", Rpar12);
  }
#endif

#ifdef TRIAL_TWO_BODY_SCATTERING_NUMERICAL11
  if(spin1 == spin2) { 
    G->max = Lhalf*Rpar11;
    if(Rpar12<0) Error("  Incorrect Rpar12 = %lf < 0\n", Rpar11);
  }
#endif
#endif

  if(G->max<1e-8) Error("  Cannot solve with G->max = 0\n");

  if(DIMENSION > 1)
    G->min = G->max / (DOUBLE)size;
  else
    G->min = 0;

#ifdef HARD_SPHERE
//  G->min = 1.;
#endif

#ifdef INTERACTION_Aziz
  Warning("Minimal distance for the spline is x=1 (limited by the hard-core of the potential)\n");
  G->min = 1.;
#endif

#ifdef INTERACTION_HYDROGEN
  Warning("Minimal distance for the spline is x=1 (limited by the hard-core of the potential)\n");
  G->min = 2;
#endif

  while(isnan(Eorigin = InteractionEnergy(G->min))) {
    Warning("  diverging energy at rmin = %lf, increasing rmin\n", G->min);
    G->min += Lhalf / 1e6;
  }

  while(InteractionEnergy(G->min)>Epotcutoff) { // remove divergency at zero, if present
    if(G->min<1e-6) G->min = 1e-6;
    Warning("  V(%"LG") = %"LG", increasing G->min to %lf\n", G->min, InteractionEnergy(G->min), G->min*1.1);
    G->min *= 1.1;
  }
Epotcutoff = InteractionEnergy(G->min);//!!!
  G->max2 = G->max*G->max;
  G->step = (G->max-G->min) / (DOUBLE)(G->size-1);
  for(i=0; i<size; i++) G->x[i] = G->min + (DOUBLE)(i)*G->step;

  Message("  Determining the scattering energy by shooting iterations\n");
  //xmin = 1e-5*E;
  if(flag_lnf_instead_of_f == OFF) { // solve for f(r)
    xmin = 1e-10;
    //xmin = 1e-6;
  } // solve for u(r)
  else {
    if (DIMENSION == 1)
      E = PI*PI / L / L; // characteristic energy
    else if (DIMENSION == 2)
      E = n; // characteristic energy
    else if (DIMENSION == 3)
      E = 1e-30; // characteristic energy

    xmin = 0.1*E;
  }
  ymin = SolveScatteringProblem_ij(G, InteractionEnergy, xmin, &function_was_negative, flag_lnf_instead_of_f, spin1, spin2);

  if(isnan(ymin) || isinf(ymin)) { // xmin is too small
    search = ON;
    Message("  Initial guess for xmin gives NaN, trying to adjust xmin...\n");
    while (search) {
      xmin /= 10;
      ymin = SolveScatteringProblem_ij(G, InteractionEnergy, xmin, &function_was_negative, flag_lnf_instead_of_f, spin1, spin2);
      if(!isnan(ymin)) search = OFF;
      if(iter++>1000)
        Error("Cannot find solution, adjust parameters of the shooting\n");
    }
  }

  xmax = xmin;
  search = ON;
  while(search) {
    xmax *= energy_step;
    ymax = SolveScatteringProblem_ij(G, InteractionEnergy, xmax, &function_was_negative, flag_lnf_instead_of_f, spin1, spin2);
    if(ymin*ymax<0) {
      xmin = xmax / energy_step;
      ymin = SolveScatteringProblem_ij(G, InteractionEnergy, xmin, &function_was_negative, flag_lnf_instead_of_f, spin1, spin2);
      search = OFF;
    }
    if(iter++>10000)
      Error("Cannot find solution, adjust parameters of the shooting\n");
  }

  if(G->f[2]<-0.1) {
    Warning("  function is still negative, trying negative energy solution...\n");
    function_was_negative = ON;
  }

  //Message(" xmin = %lf xmax = %lf ymin = %lf ymax = %lf\n", xmin, xmax, ymin, ymax);
  //ymin = SolveScatteringProblem(G, InteractionEnergy, xmin, &function_was_negative, flag_lnf_instead_of_f);
  //SaveGrid("ymin.dat", G);
  //ymax = SolveScatteringProblem(G, InteractionEnergy, xmax, &function_was_negative, flag_lnf_instead_of_f);
  //SaveGrid("ymax.dat", G);

#ifdef SPINFULL_TUNNELING
  Warning("  Looking only for negative energy solution!\n");
  function_was_negative = ON;
#endif
  if(function_was_negative) {
    Warning("  Positive energy solution has nodes, trying to find negative energy solution\n");
    // negative energy solution
    //xmin = -1000; // -100 @ n = 1e-2
    //energy_step = 0.99;
    xmin = -1e-50; // -100 @ n = 1e-2
    energy_step = 1.1;

    // loop again
    iter = 1;
    ymin = SolveScatteringProblem_ij(G, InteractionEnergy, xmin, &function_was_negative, flag_lnf_instead_of_f, spin1, spin2);

    if(isnan(ymin)) { // xmin is too small
      search = ON;
      Message("  Initial guess for xmin gives NaN, trying to adjust xmin...\n");
      while(search) {
        xmin /= 10;
        ymin = SolveScatteringProblem_ij(G, InteractionEnergy, xmin, &function_was_negative, flag_lnf_instead_of_f, spin1, spin2);
        if(!isnan(ymin) && function_was_negative == 0) search = OFF; // search for a function without nodes
      }
      if(iter++>1000)
        Error("Cannot find solution, adjust parameters of the shooting\n");
    }
    // end of loop

    if(function_was_negative)
      Message("  negative energy solution will be constructed by shooting iterations...\n");
    else
      Warning("  negative energy solution still has nodes\n");
  }

  xmax = xmin;
  search = ON;
  while(search) {
    xmax *= energy_step;
    ymax = SolveScatteringProblem_ij(G, InteractionEnergy, xmax, &function_was_negative, flag_lnf_instead_of_f, spin1, spin2);
    if(ymin*ymax<0) {
      xmin = xmax / energy_step;
      ymin = SolveScatteringProblem_ij(G, InteractionEnergy, xmin, &function_was_negative, flag_lnf_instead_of_f, spin1, spin2);
      search = OFF;
    }
    if(iter++>10000)
      Error("Cannot find solution, adjust parameters of the shooting\n");
  }

  search = ON;
  iter = 0;
  while(search) {
    if(ymin*ymax>0) // sanity check
      Message("  problems with shooting: xmin=%lf ymin=%lf, xmax=%lf ymax=%lf\n", xmin, ymin, xmax, ymax);

    x = (xmin + xmax) / 2.;
    y = SolveScatteringProblem_ij(G, InteractionEnergy, x, &function_was_negative, flag_lnf_instead_of_f, spin1, spin2);
    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    }
    else {
      xmin = x;
      ymin = y;
    }
    if(fabs(y)<precision) {
      x = (xmax*ymin - xmin*ymax) / (ymin - ymax);
      y = SolveScatteringProblem_ij(G, InteractionEnergy, x, &function_was_negative, flag_lnf_instead_of_f, spin1, spin2);
      search = OFF;
    }
    if(iter++>Nitermax) {
      Warning("  maximal allowed number of iterations (%i) is exceeded. Have to stop.\n", Nitermax);
      search = OFF;
    }
    if(xmax == (xmin + xmax) / 2 || xmin == (xmin + xmax) / 2) {
      Warning("  xmax = xmin within numerical accuracy. Have to stop.\n");
      Message("  xmin = %.15"LE", ymin = %.15"LE"\n", xmin, ymin);
      Message("  xmax = %.15"LE", ymax = %.15"LE"\n", xmax, ymax);
      search = OFF;
    }
  }
  E = x;

  Message("  Scattering energy Escat= %.15"LE "\n", E);
  if(E>0)
    Message("  or k = sqrt(2E) = %.15"LE " = %.15"LE "(pi/L)\n", sqrt(2.*E), sqrt(2.*E) / (PI / L));
  else
    Message("  or k = sqrt(-2E) = %.15"LE " = %.15"LE "(pi/L)\n", sqrt(-2.*E), sqrt(-2.*E) / (PI / L));

#ifndef JASTROW_ENERGY_FROM_EPAR
  G->k = sqrt(fabs(E));
#endif

  Message("  Achieved precision is %"LE " \n", y);

#ifdef JASTROW_DIMENSIONALITY_FROM_DPAR // restore true dimensionality
  Dpar = DIMENSION;
#endif

  Message("  Prepairing a spline... \n");
  G->size = grid_trial;
  G->min = G->x[0];
  G->max = G->x[grid_trial - 1];
  G->step = (G->max - G->min) / (DOUBLE)(grid_trial - 1);
  G->I_step = 1. / G->step;
  G->max2 = G->max*G->max;

#ifdef INTERPOLATE_LOG
  Message("  Spline will fit: u(r), i.e. logarithm of f(r)\n");
  for(i=0; i<size; i++) {
    if(G->f[i]<0) {
      Warning(" spline element %i is equal to %lf and is negative, inverting\n", i, G->f[i]);
      G->f[i] = fabs(G->f[i]);
    }
    G->f[i] = Log(G->f[i]);
  }

#ifdef JASTROW_GOES_TO_ONE_AT_LARGEST_DISTANCE
  if(G->f[size-1] != 0.) { // make sure that f(L/2) = 1
    Warning("  last element is different from 1. Adjusting the w.f. ...\n");
    for(i=0; i<size; i++) G->f[i] -= G->f[size-1];
  }
#endif
#else // INTERPOLATE_LOG
  Message("  Spline will fit: f(r) and not ln f(r) \n");

#ifdef JASTROW_GOES_TO_ONE_AT_LARGEST_DISTANCE
  if(G->f[size-1] != 1.) { // make sure that f(L/2) = 1
    Warning("  last element is different from 1. Adjusting the w.f. ...\n");
    for(i=0; i<size; i++) G->f[i] /= G->f[size-1];
  }
#else
  Warning("  Jastow term is allowed to be different from one at the largest distance\n");
#endif
#endif

  // Check spline for NaN
  for(i=0; i<size; i++) {
    if(isnan(G->f[1])) {
      Warning("Spline error, NaN found at position %i\n", i);
    }
  }

  Message("done\n");
}

/************************** Spline Right Hand Side ******************************************/
DOUBLE SplineRightHandSide(struct Grid *Grid, DOUBLE r, int spin1, int spin2) {
  static int first_time = ON;
  DOUBLE f = 1.;
// flag_boundary_condition_right:
// 0 - set f(R) = 1; f'(R) = 0
// 1 - f[size-2] and f[size-1] are already initialized
// 2 - bound-state in free space
//     1D f(x) = Exp(-sqrt(-E) r)
//     2D f(x) = K_1(sqrt(-E) r); f'/f = -k K_1(sqrt(-E) r) / K_0(sqrt(-E) r)
//     3D f(x) = Exp(-sqrt(-E) r) / r;   f'/f = -k - 1/r
  // 2) self-consistent with the energy
#ifdef JASTROW_RIGHT_BOUNDARY_FROM_BOUND_STATE_SOLUTION
  if(first_time) Message("  right boundary condition spin(%i%i): from scattering solution, k = %lf\n", spin1, spin2, Grid->k);
#ifdef TRIAL_3D
  f = exp(-Grid->k*r)/r;
#endif
#ifdef TRIAL_2D
  f = K0(Grid->k*r);
#endif
#ifdef TRIAL_1D
  f = exp(-Grid->k*r);
#endif
#endif

#ifdef JASTROW_RIGHT_BOUNDARY_PHONONS // initialize according to the phononic wave function
  if(first_time) Message("  right boundary condition spin(%i%i): from symmetrized phonons, Apar = %lf\n", spin1, spin2, Grid->Apar);
#ifdef TRIAL_3D
   f = exp(-Grid->Apar/(r*r) - Grid->Apar/((L-r)*(L-r)) + 2.*Grid->Apar/Lhalf2);
#endif
#ifdef TRIAL_2D
   f = exp(-Grid->Apar/r - Grid->Apar/(L-r) + 2.*Grid->Apar/Lhalf);
#endif
#ifdef TRIAL_1D
   f = pow(fabs(sin(PI*r/L)), 1./Grid->Apar);
#endif
#endif

#ifdef JASTROW_RIGHT_BOUNDARY_INVERSE_EXPONENTIAL
  if(first_time) Message("  right boundary condition spin(%i%i): from GPE polaron, Apar = %lf\n", spin1, spin2, Grid->Apar);
  f = exp(Grid->Apar * (Lhalf - r) * (Lhalf - r) / (2.*Lhalf*r*r));
#endif

#ifdef JASTROW_RIGHT_BOUNDARY_POLARON_GPE
  if(first_time) {
    Message("  right boundary condition spin(%i%i): from GPE polaron, Apar = %lf, Bpar = %lf\n", spin1, spin2, Grid->Apar, Grid->Bpar);
  }
  Grid->Atrial = 1./ (1. + 2.*Grid->Bpar*exp(-Grid->Apar*Lhalf)); // outside of (first_time) in case of 11,12,22 calls

  f = Grid->Atrial*(1. + Grid->Bpar*exp(-Grid->Apar*r) + Grid->Bpar*exp(Grid->Apar*(r - L)));
#endif

  first_time = OFF;
  return f;
}

/************************** Construct Grid Spline 2 body solution  **************************/
// see also SolveScatteringProblem
// flag_boundary_condition_left:
// 0 - zero boundary condition, f(0) = 0
// 1 - zero derivatice, f'(0) = 0
DOUBLE SolveScatteringProblem_ij(struct Grid *G, DOUBLE(*InteractionEnergy)(DOUBLE x), DOUBLE E, int *function_was_negative, int flag_lnf_instead_of_f, int spin1, int spin2) {
  DOUBLE r, dr, dr2;
  int i;
  //int flag_lnf_instead_of_f = ON; // 1D : OFF - f, ON - u = ln f
  static int first_time = ON;
  DOUBLE det;
  DOUBLE invert_last_point_factor = 1.; // 1 or -1
  DOUBLE Vint; // save read
  DOUBLE mu = 0.5; // reduced mass
  DOUBLE D = DIMENSION; // effective dimensionality
  DOUBLE sqrt_arg; //check sqrt in non-linear equation
#ifdef JASTROW_DIMENSIONALITY_FROM_DPAR
  D = Dpar;
  Message("  effective dimensionality taken from Dpar = %lf\n", Dpar);
#endif
#ifdef JASTROW_MASS_FROM_MPAR
  mu = Mpar;
  Message("  effective mass taken from Mpar = %lf\n", Mpar);
#endif

  *function_was_negative = OFF;

#ifdef INFINITE_MASS_COMPONENT_DN
  Warning("  reduced mass equals to m (infinite mass of 2nd particle)\n");
  mu = 1.;
#endif

#ifdef TRIAL_1D
  if(first_time == ON)
    if(flag_lnf_instead_of_f == ON)
      Message("  Scattering problem is solved for log of two-body w.f. u(r),\n  i.e. -u'' - (D-1)/r u' + 2mu [V(r)-E] u  - (u')^2 = 0\n");
    else
      Message("  Scattering problem is solved for the two-body w.f. f(r),\n  i.e. -f'' - (D-1)/r f' + 2mu [V(r)-E] f = 0\n");
  first_time = OFF;
#endif

  // initialize right boundary
  i = G->size-1;
  r = G->x[i];
  dr = G->step;
  dr2 = dr*dr;

#ifdef JASTROW_RIGHT_BOUNDARY_FROM_BOUND_STATE_SOLUTION // convert energy to momentum
#ifdef JASTROW_ENERGY_FROM_EPAR
  G->k = sqrt(fabs(Epar));
#else
  G->k = sqrt(fabs(E));
#endif
  if(first_time) Message("  Starting with scattering momentum %lf\n", G->k);
#endif 

  // Not logarithmic (i.e. linear scale): standard Schroedinger equation for f(r)
  // -f'' - (D-1)/r f' + 2mu [V(r)-E] f = 0
  // - (f[i+1]+f[i-1]-2 f[i]) / dr^2 - (f[i+2]-f[i])/(2dr) + 2mu (V[i]-E) f[i] = 0
  // f'(L/2) = 0
  // f[N] = f[N-2]
  if(flag_lnf_instead_of_f == OFF) {
    // initialize assuming special r.h.s. case, and correct later to zero derivative if needed
    G->f[i]   = SplineRightHandSide(G, G->x[i],   spin1, spin2);
    G->f[i-1] = SplineRightHandSide(G, G->x[i-1], spin1, spin2);
    // in case of zero derivative, f'(r) = 0
#ifdef JASTROW_RIGHT_BOUNDARY_ZERO_DERIVATIVE
    if(first_time) Message("right boundary condition spin(%i%i): zero derivative (0)\n", spin1, spin2);
    G->f[i] = 1.;
    Vint = InteractionEnergy(r);
    G->f[i-1] = G->f[i]*(1. + 0.5*2.*mu*(Vint-E)*dr2);
#endif
    for(i=G->size-2; i>=1; i--) {
      r = G->x[i];
      Vint = InteractionEnergy(r);
      if(isnan(Vint) || isinf(Vint))
        Warning("  NaN encountered at point %i at distance %lf, Vint(r) = %lf\n", i, r, Vint);

      G->f[i-1] = (G->f[i] * (2.+2.*mu*(Vint-E)*dr2)*r + G->f[i+1] * (-r-(D-1.)*dr*0.5)) / (r-(D-1.)*dr*0.5);
      if(isnan(G->f[i-1]) || isinf(G->f[i-1])) {
        Warning("  NaN encountered at point %i at distance %lf, Vint(r) = %lf\n", i, r, Vint);
      }
      if(G->f[i-1]<0.)
        *function_was_negative = ON;
    }
  }
  else {
    // equation for u(r) = ln f(r)
    // -u'' - (D-1)/r u' + 2.*mu*[V(r)-E] u  - (u')^2 = 0
    // u'(L/2) = 0
    // u[N] = u[N-2]
    // f[i-1] = f[i+1] + ((D-1.)*dr)/r-2. +/- sqrt(dr*dr*(D-1.)*(D-1.) + 4.*dr*r - 4.*D*dr*r + 4.*r*r + 8*r*r*f[i] - 8*r*r*f[i+1] + 4*dr*dr*r*r*(V[i]-E))/r

    // initialize assuming special case, and correct later to zero derivative if needed
    G->f[i]   = Log(SplineRightHandSide(G, G->x[i],   spin1, spin2));
    G->f[i-1] = Log(SplineRightHandSide(G, G->x[i-1], spin1, spin2));
    // 1) zero derivative, f'(r) = 0
#ifdef JASTROW_RIGHT_BOUNDARY_ZERO_DERIVATIVE
    if(first_time) Message("right boundary condition spin(%i%i): zero derivative (0)\n", spin1, spin2);
    // zero derivative for f' = Exp(u)' = f u' = 0 -> u' = 0
    G->f[i] = 0; // 0 gives a trivial and non-normalizable solution u = 0
    G->f[i-1] = (4.+4*dr*dr*2.*mu*(InteractionEnergy(r)-E)+((D-1.)*(D-1.)*dr*dr)/(r*r)-(4.*(D-1.)*dr)/r-(dr-D*dr+2.*r)*(dr-D*dr+2.*r)/(r*r) + 8.*G->f[i])/8.;
#endif

    for(i=G->size-2; i>=1; i--) {
      r = G->x[i];
      Vint = InteractionEnergy(r);
      //G->f[i-1] = G->f[i+1] + ((D-1.)/r*G->f[i] + G->f[i]*G->f[i] - 2.*mu*(Vint-E))*2.*dr;
      sqrt_arg = dr*dr*(D-1.)*(D-1.) + 4.*dr*r - 4.*D*dr*r + 4.*r*r + 8*r*r*G->f[i] - 8*r*r*G->f[i+1] + 4*dr*dr*r*r*2.*mu*(Vint-E);
       if(sqrt_arg<0.) {
         Warning("  imaginary solution sqrt(%lf) encountered at distance r[%i] = %lf, E = %e\n", sqrt_arg, i, r, E);
       }
      G->f[i-1] = G->f[i+1] + ((D-1.)*dr)/r-2. + sqrt(sqrt_arg)/r; // note that +/- sqrt solutions are possible
    }
    //for(i=0;i<G->size;i++) G->f[i] -= G->f[G->size-1]; // constant shift to ensure that f(L/2) = 1

    for(i=0;i<G->size;i++) G->f[i] = Exp(G->f[i]); // convert back to standard notation
  }

  first_time = OFF;

  // return residue for the left boundary condition
#ifdef JASTROW_LEFT_BOUNDARY_ZERO_FUNCTION
  // zero boundary condition, f(0) = 0;
  return G->f[0] * invert_last_point_factor;
#else
  // zero derivative f'(0) = 0;
  return (G->f[1] - G->f[0])*invert_last_point_factor;
#endif
}
