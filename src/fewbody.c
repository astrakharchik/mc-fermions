/*fewbody.c*/

#include "fewbody.h"
#include <math.h>
#include <stdio.h>
#include "main.h"
#include "dmc.h"
#include "randnorm.h"
#include "memory.h"
#include "mymath.h" // Erfc
//#include "amp_math.h" // erfc in visual c

// global variables are defined here
DOUBLE *Mass;
DOUBLE TotalMass = 0.;
int NumPosPerm = 0, NumNegPerm = 0; //numbers of positive and negative permutations can be different
int PosPermInd[12][5]; //matrix of positive permutations
int NegPermInd[12][5]; //matrix of negative permutations 
// 12=6*2= maximum of our Nup!*Ndn!, 5=3+2= maximum of our Nup+Ndn
DOUBLE ch_omegatau, th_omegatau, sh_omegatau;
DOUBLE mu;

/**************************** Initialize **********************************************/
void InitializePermutationMatrix(void) {
  int p,i,j,w;
  DOUBLE tau = dt;
  DOUBLE R_CM[3];
  //int Nup,Ndn;

//testing InverseFunction
/*  DOUBLE y, y0, rweight;
  DOUBLE MaxHist = 8.;
  int NHist = 100;
  DOUBLE Hist[NHist][4];//histogram
  int r;
  DOUBLE dr[3],dR[3]; 
  m_up = 0.5*(1.+mass_ratio);
  m_dn = 0.5*(1.+1./mass_ratio);
  mu = m_up*m_dn/(m_up+m_dn);
  FILE *file;

  DOUBLE x;
  DOUBLE x_test;

  sh_omegatau = sinh(omega*tau);
  ch_omegatau = cosh(omega*tau);
  th_omegatau = tanh(omega*tau);

y0=1.0;

Message("Testing NormalDistr Exp[-mu*(y+y0)^2/2./tau]\n");
Message("y0 = %lf; a = %lf; mu = %lf; tau = %lf\n",y0,a,mu,tau); 
for(i=0;i<NHist;i++) {
  Hist[i][0]=((DOUBLE)i+0.5)/((DOUBLE)NHist)*MaxHist; 
  Hist[i][1] = 0.; 
  y = Hist[i][0]; 
  Hist[i][2] = sqrt(mu/2./PI/(th_omegatau/omega))*(mu/2./PI/(th_omegatau/omega))*Exp(-mu*omega*(y)*(y)/2./th_omegatau)*4.*PI*y*y;
}
for(i=0;i<50000;i++) {
 RandomNormal3(&dR[0], &dR[1], &dR[2], 0., Sqrt(th_omegatau/omega/m_up));
 RandomNormal3(&dr[0], &dr[1], &dr[2], 0., Sqrt(th_omegatau/omega/m_dn));
 y=sqrt((dR[0]-dr[0])*(dR[0]-dr[0])+(dR[1]-dr[1])*(dR[1]-dr[1])+(dR[2]-dr[2])*(dR[2]-dr[2]));
r = (int)((y)/(MaxHist/((DOUBLE)NHist)));
 if((r<NHist) && (r>=0)) Hist[r][1] += 1./(50000.*(MaxHist/((DOUBLE)NHist)));
}
i=3;
Message("Hist[%i] = {%lf %lf %lf}\n",i,Hist[i][0],Hist[i][1],Hist[i][2]);

 file = fopen("/home/petrov/KLiKLiK/FewBodyC/BinNormal.txt","w");
 for(i=0;i<NHist;i++) {
  fprintf(file,"%lf %lf %lf\n",Hist[i][0],Hist[i][1],Hist[i][2]);
 }
 fclose(file);

/*Message("Testing InverseFunction\n");
Message("y0 = %lf; a = %lf; mu = %lf; tau = %lf\n",y0,a,mu,tau); 
Message("I will now try to sample the CFD given by Antider\n");
for(i=0;i<NHist;i++) {
  Hist[i][0]=((DOUBLE)i+0.5)/((DOUBLE)NHist)*MaxHist; 
  Hist[i][1] = 0.; 
  y = Hist[i][0]; 
  Hist[i][2] = 1./y0*y*(sqrt(2.*mu/PI/tau)*Exp(-mu*(y+y0)*(y+y0)/2./tau)+Exp(tau/2./mu/a/a-(y+y0)/a)/a*erfc(sqrt(mu/2./tau)*(y0+y)-sqrt(tau/2./mu)/a))/RelWeight(y0,a,mu,tau);
}
for(i=0;i<50000;i++) {
 r = (int)(InverseFunction(Random(),y0,a,mu,tau)/(MaxHist/((DOUBLE)NHist)));
 if(r<NHist) Hist[r][1] += 1./(50000.*(MaxHist/((DOUBLE)NHist)));
}
i=3;
Message("Hist[%i] = {%lf %lf %lf}\n",i,Hist[i][0],Hist[i][1],Hist[i][2]);

 file = fopen("/home/petrov/KLiKLiK/FewBodyC/Bin.txt","w");
 for(i=0;i<NHist;i++) {
  fprintf(file,"%lf %lf %lf\n",Hist[i][0],Hist[i][1],Hist[i][2]);
 }
 fclose(file); */

//checking the mother-child sampling
/*Message("Checking the mother-child sampling\n");
for(i=0;i<NHist;i++) {
  Hist[i][0]=((DOUBLE)i+0.5)/((DOUBLE)NHist)*MaxHist; 
  Hist[i][1] = 0.; 
  y = Hist[i][0]; 
  Hist[i][2] = 1./y0*y*sqrt(mu/2./PI/(th_omegatau/omega))*(Exp(-mu*omega*(y-y0)*(y-y0)/2./th_omegatau)-Exp(-mu*omega*(y+y0)*(y+y0)/2./th_omegatau))+1./y0*y*(sqrt(2.*mu*omega/PI/th_omegatau)*Exp(-mu*omega*(y+y0)*(y+y0)/2./th_omegatau)+Exp(tau/2./mu/a/a-(y+y0)/a)/a*erfc(sqrt(mu/2./tau)*(y0+y)-sqrt(tau/2./mu)/a));
  Hist[i][3] = 1./y0*y*sqrt(mu/2./PI/(th_omegatau/omega))*(Exp(-mu*omega*(y-y0)*(y-y0)/2./th_omegatau)-Exp(-mu*omega*(y+y0)*(y+y0)/2./th_omegatau));
}
for(i=0;i<50000;i++) {
 rweight = RelWeight(y0,a,mu,th_omegatau/omega);
 if(Random()*(1.+rweight)>1.) {//child
   r = (int)(InverseFunction(Random(),y0,a,mu,th_omegatau/omega)/(MaxHist/((DOUBLE)NHist)));
 }
 else {//mother
   RandomNormal3(&dR[0], &dR[1], &dR[2], 0., Sqrt(th_omegatau/omega/m_up));
   RandomNormal3(&dr[0], &dr[1], &dr[2], 0., Sqrt(th_omegatau/omega/m_dn));
   y=sqrt((dR[0]-dr[0]-y0)*(dR[0]-dr[0]-y0)+(dR[1]-dr[1])*(dR[1]-dr[1])+(dR[2]-dr[2])*(dR[2]-dr[2]));
   r = (int)((y)/(MaxHist/((DOUBLE)NHist)));
 }
if(r<NHist) Hist[r][1] += (1.+rweight)/(50000.*(MaxHist/((DOUBLE)NHist)));
}

 file = fopen("/home/petrov/KLiKLiK/FewBodyC/BinMotherChild.txt","w");
 for(i=0;i<NHist;i++) {
  fprintf(file,"%lf %lf %lf %lf\n",Hist[i][0],Hist[i][1],Hist[i][2],Hist[i][3]);
 }
 fclose(file);


/*  Message(" testing Antider()\n");
  Message(" x  y f^{-1}(y)\n");
  for(x=0.1; x<5; x += 0.1) { // Antider(y, DOUBLE y0, DOUBLE a, DOUBLE mu, DOUBLE tau)
    y = Antider(x, y0, a, mu, tau);
    x_test = InverseFunction(y, y0, a, mu, tau);
    Message("%lf %lf %lf\n", x, y, x_test);
  }

Message("Testing erfc\n");
 file = fopen("/home/petrov/KLiKLiK/FewBodyC/Erfc.txt","w");
 for(y=-2.;y<2.;y+=0.1) {
  fprintf(file,"%lf %lf\n",y,erfc(y));
 }
 fclose(file);
*/
  // allocate memory for an array of DOUBLEs with [Nup+Ndn] size
  Mass = (DOUBLE*) Calloc("F", Nup+Ndn, sizeof(DOUBLE));
  m_up = 0.5*(1.+mass_ratio);
  m_dn = 0.5*(1.+1./mass_ratio);
#ifdef UNIT_MASS_COMPONENT_UP
  m_dn /= m_up;
  m_up = 1.;
#endif
#ifdef UNIT_MASS_COMPONENT_DN
  m_dn = 1.;
  m_up = mass_ratio;
#endif
  mu = m_up*m_dn/(m_up+m_dn);

  sh_omegatau = sinh(omega*tau);
  ch_omegatau = cosh(omega*tau);
  th_omegatau = tanh(omega*tau);

   for(w=0; w<Nwalkers; w++) { // move all walkers
     for(i=0; i<Nup; i++) { // R is the original (Ro) vector
      W[w].R[i][0] = W[w].x[i];
      W[w].R[i][1] = W[w].y[i];
      W[w].R[i][2] = W[w].z[i];
    }
    for(i=0; i<Ndn; i++) {
      W[w].R[Nup+i][0] = W[w].xdn[i];
      W[w].R[Nup+i][1] = W[w].ydn[i];
      W[w].R[Nup+i][2] = W[w].zdn[i];
    }
    R_CM[0] = R_CM[1] = R_CM[2] = 0.;
    for(i=0; i<Nup+Ndn; i++) for(j=0; j<3; j++) R_CM[j] += W[w].Rp[i][j]*Mass[i]/TotalMass;
    for(i=0; i<Nup+Ndn; i++) for(j=0; j<3; j++) W[w].Rp[i][j] -= R_CM[j]; // put CM back to (0,0,0)
  }

  for(i=0; i<Nup; i++) {
    Mass[i] = m_up;
    TotalMass += m_up;
  }

  for(i=Nup; i<Nup+Ndn; i++) {
    Mass[i] = m_dn;
    TotalMass += m_dn;
  }

  for(p=0; p<Factorial(Nup)*Factorial(Ndn); p++) {
    if(SignatureHeavyLight(p)>0) {
      for(i=0; i<Nup+Ndn; i++) {
        PosPermInd[NumPosPerm][i] = PermHeavyLight(p,i);
      }
      NumPosPerm++;
    }
    else {
      for(i=0; i<Nup+Ndn; i++) {
        NegPermInd[NumNegPerm][i] = PermHeavyLight(p,i);
      }
      NumNegPerm++;
    }
  }

  Message("\nMasses are\n");
  for(i=0;i<Nup+Ndn;i++) Message("%lf ", Mass[i]);
  Message("\nTotal mass is %lf\n", TotalMass);
  Message("\nNumPosPerm = %i; NumNegPerm = %i\n", NumPosPerm, NumNegPerm);

  Message("\nPositive permutations from the matrix\n");
  for(p=0; p<NumPosPerm; p++) {
    for(i=0; i<Nup+Ndn; i++) {
      Message("%i ", PosPermInd[p][i]);
    }
    Message("\n");
  }
  Message("\nNegative permutations from the matrix\n");
  for(p=0; p<NumNegPerm; p++) {
    for(i=0; i<Nup+Ndn; i++) {
      Message("%i ", NegPermInd[p][i]);
    }
    Message("\n");
  }

  Message("\nExpected non-interacting Eo = %lf\n\n",(((DOUBLE)DIMENSION/2.*(DOUBLE)(Nup + Ndn - 1) + (DOUBLE)(Nup - 1) + (DOUBLE)((Ndn > 0)?(Ndn - 1):(0)))*omega));
}

/**************************** DMC Pseudoptential Move *********************************
// Normalized Green's function, Eq. (9)
// arguments are arrays Nx3
DOUBLE GreensFunctionNormalized(DOUBLE **Ro, DOUBLE **R, DOUBLE tau) {
  int i,j;
  DOUBLE dR2 = 0.;
  DOUBLE ch_tau, th_tau;

  ch_tau = cosh(tau);
  th_tau = tanh(tau);

  for(i=0; i<N; i++) for(j=0; j<3; j++) dR2 += (R[i][j] - Ro[i][j]/ch_tau) * (R[i][j] - Ro[i][j]/ch_tau);
  return pow(2.*PI*th_tau, -3./2.*(DOUBLE)N)*Exp(-0.5*dR2/th_tau);
}*/

// Eq. (13) (only the last exponent, we will multiply it by weight when calculating (Gpos-Gneg)/Gpos)
DOUBLE GreensFunction(DOUBLE **Ro, DOUBLE **R, DOUBLE tau, DOUBLE m_up, DOUBLE m_dn, DOUBLE omega) {
  int i,j;
  DOUBLE mdR2 = 0.;

  for(i=0; i<Nup+Ndn; i++) for(j=0; j<3; j++) mdR2 += Mass[i]*(R[i][j] - Ro[i][j]/ch_omegatau) * (R[i][j] - Ro[i][j]/ch_omegatau);

//  return pow(2.*PI*sh_omegatau/omega, -(DOUBLE)DIMENSION/2.*(DOUBLE)(Nup+Ndn))*pow(m_up,(DOUBLE)DIMENSION/2.*(DOUBLE)Nup)*pow(m_dn,(DOUBLE)DIMENSION/2.*(DOUBLE)Nup)*Exp(-0.5*omega*(m_up*Ro2up+m_dn*Ro2dn)*th_omegatau - 0.5*omega*(m_up*dR2up+m_dn*dR2dn)/th_omegatau);

return Exp(-0.5*omega*mdR2/th_omegatau);
}

/**************************** Factorial ************************************************/
int Factorial(int i){
  if(i<=1)
    return 1;
  else
    return i*Factorial(i-1);
}

/**************************** Permutations *********************************************/
/* Min<=Max; p is the index of permutation in [0,(Max-Min+1)!-1]; i in [Min,Max]*/
int Permutations(int Min, int Max, int p, int i) {
  int factorial,IP;

  factorial = Factorial(Max-Min);
  IP = (int)((DOUBLE)p/(DOUBLE)factorial);

  if(Min==Max)
    return Min;
  else {
    if(i==IP+Min)
      return Min;
    else {
      if(i<IP+Min) {
        return Permutations(Min+1,Max,p-factorial*IP,i+1);
      }
      else {
        return Permutations(Min+1,Max,p-factorial*IP,i);
      }
    }
  }
}

/**************************** Signature ************************************************/
int Signature(int Min, int Max, int p) {
  int factorial,IP;
  factorial = Factorial(Max-Min);

  IP = (int)((DOUBLE)p/(DOUBLE)factorial);
  if(Min>=Max)
    return 1;
  else
    return Signature(Min+1,Max,p-factorial*IP)*(int)pow(-1,IP);
}

/**************************** PermHeavyLight ********************************************/
 /* p in [0,Nup!*Ndn!-1]; i in [0,Nup+Ndn-1] */
int PermHeavyLight(int p,int i) {
  int pup=(int)((DOUBLE)p/(DOUBLE)Factorial(Ndn));
  int pdn=p-pup*Factorial(Ndn);

  if(i<Nup)
    return Permutations(0,Nup-1,pup,i);
  else
    return Permutations(Nup,Nup+Ndn-1,pdn,i);
}

/**************************** SignatureHeavyLight **************************************/
/* p in [0,Nup!*Ndn!-1] */
int SignatureHeavyLight(int p) {
  int pup=(int)((DOUBLE)p/(DOUBLE)Factorial(Ndn));
  int pdn=p-pup*Factorial(Ndn);

  return Signature(0,Nup-1,pup)*Signature(Nup,Nup+Ndn-1,pdn);
}

/*************************** Calculate weight of walker w ******************************/
// Calculate adjusted weight according to Eq. (11)
DOUBLE Weight(int w, DOUBLE weight_average) {
  int p,i,j,wp;
  DOUBLE weight_positive, weight_negative, weight_positive_binary_int ; // correction to the branching weight
  DOUBLE mdR2;
  DOUBLE Modyo,Mody,ymyo2,Prod,yo,y,temp,Sum;
  int iup,idn; // These guys are local to the interaction part
  DOUBLE tau=dt;

  weight_positive = weight_negative = weight_positive_binary_int = 0.;

  for(p=0; p<NumPosPerm; p++) {
    for(wp=0; wp<Nwalkers; wp++) {
      //begin interaction stuff. So far with a= infty, i.e., without erfc 
      Prod = Sum = 1.;
      for(iup=0; iup<Nup; iup++) {
       for(idn=Nup; idn<Nup+Ndn; idn++) {
        Modyo = Mody = ymyo2 = 0.;  
        for(j=0;j<3;j++) {
          yo = (W[wp].R[PosPermInd[p][iup]][j]-W[wp].R[PosPermInd[p][idn]][j])/ch_omegatau;
          y = W[w].Rpp[iup][j]-W[w].Rpp[idn][j];
          Modyo += yo*yo;
          Mody  += y*y;
          ymyo2 += (y-yo)*(y-yo);
        }
        Modyo=sqrt(Modyo);
        Mody=sqrt(Mody);
        temp = 1./(4.*PI*Modyo*Mody*pow(mu*omega/(2.*PI*th_omegatau),3./2.)*Exp(-mu*omega/2./th_omegatau*ymyo2))*(sqrt(2.*mu/PI/tau)*Exp(-mu*omega/2./th_omegatau*(Mody+Modyo)*(Mody+Modyo))+Exp(tau/(2.*mu*a*a)-(Mody+Modyo)/a)/a*erfc(sqrt(mu/2./tau)*(Mody+Modyo)-sqrt(tau/2./mu)/a));
        Prod *= 1. + temp;
        Sum += temp;
       }
      }
      //end interaction stuff. Here we arrive with Prod = Product_iup Product_idn I_iup,idn
//Message("Prod = %lf; Sum = %lf\n",Prod,Sum); 
      mdR2=0.;
      for(i=0; i<Nup+Ndn; i++) for(j=0; j<3; j++) mdR2 += Mass[i]*(W[w].Rpp[i][j] - W[wp].R[PosPermInd[p][i]][j]/ch_omegatau) * (W[w].Rpp[i][j] - W[wp].R[PosPermInd[p][i]][j]/ch_omegatau);
      weight_positive        += W[wp].weight*Prod*Exp(-0.5*omega*mdR2/th_omegatau);
      weight_positive_binary_int += W[wp].weight*Sum*Exp(-0.5*omega*mdR2/th_omegatau);
    }
  }

  for(p=0; p<NumNegPerm; p++) {
    for(wp=0; wp<Nwalkers; wp++) {
      //begin interaction stuff. So far with a= infty, i.e., without erfc
      Prod = 1.;
      for(iup=0;iup<Nup;iup++) {
        for(idn=Nup;idn<Nup+Ndn;idn++) {
          Modyo = Mody = ymyo2 = 0.;  
          for(j=0;j<3;j++) {
            yo = (W[wp].R[NegPermInd[p][iup]][j]-W[wp].R[NegPermInd[p][idn]][j])/ch_omegatau;
            y = W[w].Rpp[iup][j]-W[w].Rpp[idn][j];
            Modyo += yo*yo;
            Mody += y*y;
            ymyo2 += (y-yo)*(y-yo);
          }
          Modyo=sqrt(Modyo);
          Mody=sqrt(Mody);
          temp = 1./(4.*PI*Modyo*Mody*pow(mu*omega/(2.*PI*th_omegatau),3./2.)*Exp(-mu*omega/2./th_omegatau*ymyo2))*(sqrt(2.*mu/PI/tau)*Exp(-mu*omega/2./th_omegatau*(Mody+Modyo)*(Mody+Modyo))+Exp(tau/(2.*mu*a*a)-(Mody+Modyo)/a)/a*erfc(sqrt(mu/2./tau)*(Mody+Modyo)-sqrt(tau/2./mu)/a));
        Prod *= 1. + temp;
        }
      }
      //end interaction stuff. Here we arrive with Prod = P_k P_l I_k,l

      mdR2=0.;
      for(i=0; i<Nup+Ndn; i++) for(j=0; j<3; j++) mdR2 += Mass[i]*(W[w].Rpp[i][j] - W[wp].R[NegPermInd[p][i]][j]/ch_omegatau) * (W[w].Rpp[i][j] - W[wp].R[NegPermInd[p][i]][j]/ch_omegatau);
      weight_negative += W[wp].weight*Prod*Exp(-0.5*omega*mdR2/th_omegatau);
    }
  }
//Message("(weight_positive-weight_negative) / weight_positive_binary_intProd = %lf\n",(weight_positive-weight_negative) / weight_positive_binary_int);
  return weight_average*(weight_positive-weight_negative) / weight_positive_binary_int;
}

/**************************** Child's relative weight **********************************/
DOUBLE RelWeight(DOUBLE y0, DOUBLE a, DOUBLE mu, DOUBLE tau) {
  DOUBLE temp;

  temp = sqrt(mu/2./tau);
  return a/y0*(Exp(tau/(2.*a*a*mu)-y0/a)*erfc(temp*y0-1./(2.*a*temp))-erfc(temp*y0));
}

/**************************** Antiderivative *******************************************/
DOUBLE Antider(DOUBLE y, DOUBLE y0, DOUBLE a, DOUBLE mu, DOUBLE tau) {
  DOUBLE temp, temp1;
  DOUBLE norm;

  temp = sqrt(mu/2./tau);
  temp1 = Exp(tau/(2.*a*a*mu)-y0/a);
  norm = temp1*erfc(temp*y0-1./(2.*a*temp))-erfc(temp*y0);
  return 1.+(erfc(temp*(y+y0))-temp1*Exp(-y/a)*(1.+y/a)*erfc(temp*(y0+y)-1./(2.*a*temp)))/norm;
}

/**************************** Inverse function *****************************************/
DOUBLE InverseFunction(DOUBLE xi, DOUBLE y0, DOUBLE a, DOUBLE mu, DOUBLE tau) {
  // matching conditions
  DOUBLE y,ymin,ymax,x,xmin,xmax;
  DOUBLE precision = 1e-10;
  //int search = ON;

  xmin = 0.;
  xmax = 10.;
  ymin = Antider(xmin, y0, a, mu, tau) - xi;
  ymax = Antider(xmax, y0, a, mu, tau) - xi;

  if(ymin*ymax > 0.) Error("Can't construct the grid : No solution found");
  y=1.;  
  while(fabs(y)>precision) {
    x = (xmin+xmax)/2.;
    y = Antider(x, y0, a, mu, tau) - xi;

    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }

   // if(fabs(y)<precision) {
   //   x = (xmax*ymin-xmin*ymax) / (ymin-ymax); // do linear interpolation
      //y = Antider(x, y0, a, mu, tau) - xi;
   //   search = OFF;
   // }
  }
//Message("InverseFunction returns %lf\n",x);
  return x; 
}

/**************************** DMC Pseudopotential Move *********************************/
// I understand it as the main loop of Kalos' scheme
// Nup is supposed to be >1, otherwise not interesting at all; any Ndn
// Dimension is supposed to be 3. It's really a shame
void DMCPseudopotentialMove(int dummy) {
  int i,j,w,wp;
  DOUBLE dr[3];
  //DOUBLE dx,dy,dz;
  DOUBLE weight_sum, weight_average;
  DOUBLE xi;
  DOUBLE R_CM[3];
  DOUBLE tau = dt;
  DOUBLE Modyo,Mody,yo; int iup,idn; // These guys are local to the interaction part
  DOUBLE rweight[20],rU[21],y_new,Y_rel[3];
  int IND,INDp; // this is what we use for correcting walker's coordinates

  for(w=0; w<Nwalkers; w++) { 
    // calculate initial weight, first term in Eq. (13)
    W[w].r2 = 0.;//actually, it will be mr^2
    for(i=0; i<Nup+Ndn; i++) for(j=0; j<3; j++) W[w].r2 += W[w].R[i][j] * W[w].R[i][j] * Mass[i];

    // weight according to Eq. (13) I removed the center of mass right from here
    W[w].weight *= pow(ch_omegatau, -((DOUBLE)DIMENSION)/2.*((DOUBLE)(Nup+Ndn-1)))*Exp(-0.5*omega*W[w].r2*th_omegatau);
    // Rp is Ro/ch_omegatau plus Gaussian move, Eq. (9)
    for(i=0; i<Nup+Ndn; i++) {
      RandomNormal3(&dr[0], &dr[1], &dr[2], 0., Sqrt(th_omegatau/omega/Mass[i])); // (x,y,z, mu, sigma)
      for(j=0; j<3; j++) W[w].Rp[i][j] = W[w].R[i][j] / ch_omegatau + dr[j];
     }

    // begin correcting walker's coordinates according to the two-body interactions
    // first assign relative weights to different children (pairwise moves)
    for(iup=0;iup<Nup;iup++) {
      for(idn=Nup;idn<Nup+Ndn;idn++) {
         IND = iup + Nup * (idn-Nup);
         rweight[IND] = RelWeight(sqrt((W[w].R[iup][0]-W[w].R[idn][0])*(W[w].R[iup][0]-W[w].R[idn][0])+(W[w].R[iup][1]-W[w].R[idn][1])*(W[w].R[iup][1]-W[w].R[idn][1])+(W[w].R[iup][2]-W[w].R[idn][2])*(W[w].R[iup][2]-W[w].R[idn][2]))/ch_omegatau,a,mu,th_omegatau/omega);
      }
    }
    // prepare to choose in between one of the children or the mother
    rU[0] = rweight[0];
    for(IND=1; IND<Nup*Ndn; IND++) rU[IND] = rU[IND-1]+rweight[IND];
    rU[Nup*Ndn] = rU[Nup*Ndn-1] + 1.;
    W[w].weight *= rU[Nup*Ndn]; // now we know the new weight of the walker
    // So, the mother or one of the kids?
    xi = Random()*rU[Nup*Ndn];
    for(INDp=0; rU[INDp]<xi; INDp++); //INDp gives you the pair to move
    if(INDp<Nup*Ndn) { // if true we turn to child number INDp, else do nothing (stick to the mom)
      //Message("We choose the child. INDp = %i\n",INDp);
      iup = INDp % Nup;     // please check that this is really the remainder 
      idn = Nup + INDp/Nup; // please check that this is really the integer part of INDp/Nup
      for(j=0; j<3; j++) R_CM[j] = (W[w].Rp[iup][j]*m_up+W[w].Rp[idn][j]*m_dn)/(m_up+m_dn);
      Modyo = Mody = 0.;
      RandomNormal3(&Y_rel[0], &Y_rel[1], &Y_rel[2], 0., 1.);
      for(j=0; j<3; j++) {
        yo = (W[w].R[iup][j]-W[w].R[idn][j])/ch_omegatau;
        //Y_rel[j] = W[w].Rp[iup][j]-W[w].Rp[idn][j];
        Modyo += yo*yo;
        Mody += Y_rel[j]*Y_rel[j];
      }
      Modyo=sqrt(Modyo);
      Mody=sqrt(Mody);
      y_new = InverseFunction(Random(),Modyo,a,mu,th_omegatau/omega); //InverseFunction(DOUBLE xi, DOUBLE y0, DOUBLE a, DOUBLE mu, DOUBLE tau)
      //Message("I shift walker %i by factor %lf\n",w,y_new/Mody);
      for(j=0; j<3; j++) {
        W[w].Rp[iup][j] = R_CM[j] + m_dn/(m_up+m_dn)*Y_rel[j]*y_new/Mody;
        W[w].Rp[idn][j] = R_CM[j] - m_up/(m_up+m_dn)*Y_rel[j]*y_new/Mody;
      }
//Message("Child of walker %i is moved to {%lf %lf %lf %lf %lf %lf}\n",w,W[w].Rp[0][0],W[w].Rp[0][1],W[w].Rp[0][2],W[w].Rp[1][0],W[w].Rp[1][1],W[w].Rp[1][2]);
    }
//end correcting walker's coordinates

    //begin removing center of mass
    R_CM[0] = R_CM[1] = R_CM[2] = 0.;
    for(i=0; i<Nup+Ndn; i++) for(j=0; j<3; j++) R_CM[j] += W[w].Rp[i][j]*Mass[i]/TotalMass;
    for(i=0; i<Nup+Ndn; i++) for(j=0; j<3; j++) W[w].Rp[i][j] -= R_CM[j]; // put CM back to (0,0,0)
    //end removing center of mass
  }

  // Rpp: create pool {Rpp} in a random way
  // first calculate sum of the weights
  W[0].U = W[0].weight;

  for(w=1; w<Nwalkers; w++) W[w].U = W[w-1].U + W[w].weight;
  weight_sum = W[Nwalkers-1].U;
  weight_average = weight_sum / (DOUBLE) Nwalkers;

  // generate random point on a line [0, weight_sum]
  for(w=0; w<Nwalkers; w++) { // move all walkers
    xi = Random()*weight_sum;
    //simple  wp = w; // direct copy, no change
    for(wp=0; W[wp].U<xi; wp++);// find index of the corresponding walker
    // copy walker's coordinates
    for(i=0; i<Nup+Ndn; i++) for(j=0; j<3; j++) W[w].Rpp[i][j] = W[wp].Rp[i][j]; // copy moved coordinates
  }

  // Calculate adjusted weight according to Eq. (11)
#pragma omp parallel for
  for(w=0; w<Nwalkers; w++) {
    // Calculate the new weight of walker w according to Eq. (11) we cannot use W[w].weight because we still use the old weight in the cycle above. So, I added weightp to the structure W
    W[w].weightp = Weight(w, weight_average);
    //W[w].weightp = weight_average*(weight_positive-weight_negative) / weight_positive; 

    if(W[w].weightp<0.) {
      //Warning("  negative weight! Killing walker\n");
      W[w].weightp = 0.;
    }
  }

  for(w=0; w<Nwalkers; w++) W[w].weight = W[w].weightp;
  for(w=0; w<Nwalkers; w++) for(i=0; i<Nup+Ndn; i++) for(j=0; j<3; j++) W[w].R[i][j] = W[w].Rpp[i][j];
}

/********************************** Branching ********************************/
void BranchingPseudopotential(void) {
  int i, w;
  DOUBLE wmax = 0.;
  DOUBLE total_weight = 0.;
  DOUBLE tau = dt;

  // Update walkers' coordinates
  for(w=0; w<Nwalkers; w++) {
    for(i=0; i<Nup; i++) {
      W[w].x[i] = W[w].Rpp[i][0];
      W[w].y[i] = W[w].Rpp[i][1];
      W[w].z[i] = W[w].Rpp[i][2];
    }
    for(i=0; i<Ndn; i++) {
      W[w].xdn[i] = W[w].Rpp[Nup+i][0];
      W[w].ydn[i] = W[w].Rpp[Nup+i][1];
      W[w].zdn[i] = W[w].Rpp[Nup+i][2];
    }
  }

  // calculate total weight
  for(w=0; w<Nwalkers; w++) {
    total_weight += W[w].weight;
  }
  if(total_weight<1e-14) {
    Message("  small total weight = %"LE"\n", total_weight);
  }

  // correct Eo in such a way that the total normalization is Npop
  // Weight' = Exp(Eo*dt) Weight = Npop
  E = Eo = Log((DOUBLE)Nwalkers/total_weight)/dt;
  //Message("Eo = %lf\n",Eo);

  for(w=0; w<Nwalkers; w++) W[w].weight *= (DOUBLE)Nwalkers/total_weight;
  //weight_adjust_factor = Exp(Eo*dt);
  //weight_adjust_factor = (DOUBLE)Npop/total_weight;

  if(Npop!=Nwalkers) Message("\n Jesus Christ! Npop is not equal to Nwalkers \n"); 

return;
}

void DMCPseudopotentialReweightingMove(int w) {
  // move according to Gaussian Eq. (3) and sample erfc as a weight
  int i,j;
  double r,r0;
  double psi = 1.;

#ifndef TRIAL_1D
  Error("  DMCPseudopotentialReweightingMove implemented only in 1D\n");
#endif

  // R = R(i-1) + chi, f(chi) = Exp(-R^2/2dt)
  // Exp(-(x-mu)^2/(2 sigma^2))
  GaussianJump(w, Sqrt(dt)); // R = W[w] + chi, f(chi) = exp(-R^2/4dt)
  CopyWalkerToVector(W[w].Rp, W[w]); // R' = W[w]

  // reweight according to delta-function propagator
  for(i=0; i<Nup; i++) { // Same spin, up, s-wave scattering length aA
    for(j=i+1; j<Nup; j++) { 
      r0 = fabs(W[w].Rp[i][2] - W[w].Rp[j][2]); // before move
      r = fabs(W[w].R[i][2] - W[w].R[j][2]); // after move
      psi += sqrt(pi*dt)/aA * exp(dt/(aA*aA) + (r-r0)*(r-r0)/(4.*dt) - (fabs(r)+fabs(r0))/aA) * erfc(sqrt(0.25/dt)*(fabs(r)+fabs(r0)) - sqrt(dt)/aA);
    }
  }

  for(i=0; i<Ndn; i++) { // Same spin, down, s-wave scattering length aA
    for(j=i+1; j<Ndn; j++) { 
      r0 = W[w].Rp[i][5] - W[w].Rp[j][5];
      r = W[w].R[i][5] - W[w].R[j][5];
      psi += sqrt(pi*dt)/aA * exp(dt/(aA*aA) + (r-r0)*(r-r0)/(4.*dt) - (fabs(r)+fabs(r0))/aA) * erfc(sqrt(0.25/dt)*(fabs(r)+fabs(r0)) - sqrt(dt)/aA);
    }
  }

  for(i=0; i<Nup; i++) { // spin up + spin down, s-wave scattering length a
    for(j=0; j<Ndn; j++) { 
      r0 = W[w].Rp[i][2] - W[w].Rp[j][5]; // before move, can be positive or negative
      r = W[w].R[i][2] - W[w].R[j][5]; // after move, can be positive or negative
      psi += sqrt(pi*dt)/a * exp(-dt/(a*a) + (r-r0)*(r-r0)/(4.*dt) - (fabs(r)+fabs(r0))/a) * erfc(sqrt(0.25/dt)*(fabs(r)+fabs(r0)) - sqrt(dt)/a);
    }
  }

  CopyVectorToWalker(&W[w], W[w].R);
  W[w].weight = psi;

  //ReduceWalkerToTheBox(&W[w]);
}

/********************************** Branching ********************************/
void BranchingPseudopotentialReweighting(void) {
  int w;
  double total_weight = 0.;

  for(w=0; w<Nwalkers; w++) total_weight += W[w].weight;
  total_weight /= (double)Nwalkers;

  Eo = -log(total_weight) / dt;

  if(branchng_present)
    for(w=0; w<Nwalkers; w++)
      if(W[w].status == ALIVE)
        BranchingWalkerPseudopotentialReweighting(w);

  //Eo = EnergyO();

  Nwalkers = 0;
  Nwalkersw = 0;
  for(w=0; w<NwalkersMax; w++) {
    if(W[w].status == REINCARNATION)
      W[w].status = ALIVE;
    else if(W[w].status == KILLED) {
      W[w].status = DEAD;
      W[w].weight = 0.;
    }
  }

  for(w=0; w<NwalkersMax; w++) {
    if(W[w].status == ALIVE) {
      if(w != Nwalkers) {
        CopyWalker(&W[Nwalkers], W[w]);
        W[w].status = DEAD;
        W[w].weight = 0;
      }
      Nwalkers++;
      Nwalkersw += W[w].weight;
    }
  }

  if(Nwalkers == 0) {
    Error("All walkers have died");
  }

  Ndead_walkers = 0;
  for(w=NwalkersMax-1; w>=Nwalkers; w--) {
    dead_walkers_storage[Ndead_walkers++] = w;
  }
}

/******************************* Branching Walker ****************************/
int BranchingWalkerPseudopotentialReweighting(int w) {
  int multiplicity;
  int i, j, wp;
  DOUBLE dE, weight;

  //dE = Eo - 0.5*(W[w].E + W[w].Eold);

  //weight = W[w].weight * Exp(dt*Eo);
  if(W[w].weight < 0) Warning("  negative weight!\n");

  weight = fabs(W[w].weight) * Exp(dt*Eo);

   multiplicity = (int) (weight + Random());

  if(Nwalkers > Npop_max)
    multiplicity = (int) ((DOUBLE) multiplicity*reduce + Random());
  else if(Nwalkers < Npop_min)
    multiplicity = (int) ((DOUBLE) multiplicity*amplify + Random());

  if(multiplicity == 0) {
    W[w].status = KILLED;
    W[w].weight = weight;
  }
  else {
    W[w].weight = weight;// / (DOUBLE) multiplicity;
    for(i=1; i<multiplicity; i++) { // make replicae
      if(Ndead_walkers < 1) {
        Warning("walker %i was killed because it had too many sons (%i)\n", w, multiplicity);
        Message("weight %lf Nwalkers %i, Eo=%lf, E=%lf, Eold=%lf\n", weight, Nwalkers, Eo, W[w].E, W[w].Eold);
        Message("killed walker coordinates:\n");
        for(j=0; j<Nup; j++) Message("%lf %lf %lf\n", W[w].x[j], W[w].y[j], W[w].z[j]);
        for(j=0; j<Ndn; j++) Message("%lf %lf %lf\n", W[w].xdn[j], W[w].ydn[j], W[w].zdn[j]);
        multiplicity = 0;
        W[w].status = DEAD;
        W[w].weight = 0;
      }
      else {
        wp = dead_walkers_storage[Ndead_walkers-1];
        CopyWalker(&W[wp], W[w]);
        W[wp].status = REINCARNATION;
        W[wp].weight = 0.;
        Ndead_walkers--;
      }
    }
  }
  return multiplicity;
}

