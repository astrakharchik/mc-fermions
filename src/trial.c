/*trial.c*/
/* best trial wavefunction Psi_T = Prod_{i<j} f(r_ij)
   U = ln f
   Fp = f' / f
3D Eloc = [-(f" + 2f'/r)/f] + (f'/f)^2
2D Eloc = [-(f" + f'/r)/f] + (f'/f)^2
1D Eloc = [-f"/f] + (f'/f)^2

   in terms of u(r)
   Fp = u'
   Eloc = -u'' - 2u'/r
*/

#include <math.h>
#include <stdio.h>
#include <ctype.h>
#include "trial.h"
#include "main.h"
#include "utils.h"
#include "memory.h"
#include "fermions.h"
#include "spline.h"
#include "mymath.h"
#include "trialf.h"
#include "compatab.h"

DOUBLE sqE, sqE2, expBpar, alpha_1, Xi, A1SW, A3SW, A4SW, alpha2E;
//SS
DOUBLE A1SWSS, A3SWSS, A4SWSS;
DOUBLE A1SW, A2SW, A3SW, A4SW, A5SW;
DOUBLE A1SD11, A2SD11, A3SD11, A4SD11, A5SD11;
DOUBLE A1SD12, A2SD12, A3SD12, A4SD12, A5SD12;
DOUBLE A1SD22, A2SD22, A3SD22, A4SD22, A5SD22;
DOUBLE sqEkappa, sqEkappa2, trial_delta, trial_Kappa, trial_Kappa2, trial_k, trial_k2;
DOUBLE trial_Vo, trial11_Vo, trial22_Vo;
//p-wave
DOUBLE sqE11, sqE211, trial11_Kappa, trial11_Kappa2, A1SW11, A2SW11, A3SW11, A4SW11, A5SW11, trial11_delta;
DOUBLE sqE22, sqE222, trial22_Kappa, trial22_Kappa2, A1SW22, A2SW22, A3SW22, A4SW22, A5SW22, trial22_delta;
DOUBLE sqE12, sqE212, trial12_Kappa, trial12_Kappa2, A1SW12, A2SW12, A3SW12, A4SW12, A5SW12, trial12_delta;
DOUBLE alpha11_1, alpha12_1;
DOUBLE Atrial, Btrial, Ctrial;
DOUBLE Atrial12, Btrial12, Ctrial12;
DOUBLE Atrial11, Btrial11, Ctrial11;
DOUBLE Atrial22, Btrial22, Ctrial22;

int high_energy;

/************************** W F Symmetrization *****************************
#ifdef SYMMETRIZE_TRIAL_WF
DOUBLE InterpolateU11(struct Grid *Grid, DOUBLE r) {
   u(r) + u(L-r)
  return ((r>Lhalf)?(0):(InterpolateExactU11(r)+InterpolateExactU11(L-r)));
}
DOUBLE InterpolateFp11(struct Grid *Grid, DOUBLE r) {
  // fp(r) - fp(L-r)
  return ((r>Lhalf)?(0):(InterpolateExactFp11(r)-InterpolateExactFp11(L-r)));
}
DOUBLE InterpolateE11(struct Grid *Grid, DOUBLE r) {
#ifdef TRIAL_1D
  return (r>Lhalf)?(0):(InterpolateExactE11(r) + InterpolateExactE11(L-r));
#endif
#ifdef TRIAL_2D
  return (r>Lhalf)?(0):(InterpolateExactE11(r) + InterpolateExactE11(L-r) + 1./r*InterpolateExactFp11(L-r)*L/(L-r));
#endif
#ifdef TRIAL_3D
  return (r>Lhalf)?(0):(InterpolateExactE11(r) + InterpolateExactE11(L-r) + 2./r*InterpolateExactFp11(L-r)*L/(L-r));
#endif
}

DOUBLE InterpolateU12(struct Grid *Grid, DOUBLE r) {
  // u(r) + u(L-r)
  return ((r>Lhalf)?(0):(InterpolateExactU12(r)+InterpolateExactU12(L-r)));
}
DOUBLE InterpolateFp12(struct Grid *Grid, DOUBLE r) {
  // fp(r) - fp(L-r)
  return ((r>Lhalf)?(0):(InterpolateExactFp12(r)-InterpolateExactFp12(L-r)));
}
DOUBLE InterpolateE12(struct Grid *Grid, DOUBLE r) {
#ifdef TRIAL_1D
  return (r>Lhalf)?(0):(InterpolateExactE12(r) + InterpolateExactE12(L-r));
#endif
#ifdef TRIAL_2D
  return (r>Lhalf)?(0):(InterpolateExactE12(r) + InterpolateExactE12(L-r) + 1./r*InterpolateExactFp12(L-r)*L/(L-r));
#endif
#ifdef TRIAL_3D
  return (r>Lhalf)?(0):(InterpolateExactE12(r) + InterpolateExactE12(L-r) + 2./r*InterpolateExactFp12(L-r)*L/(L-r));
#endif
}
#endif
*/

/************************** Load Trial Wave Function **********************/
void LoadTrialWaveFunction(struct Grid *G, char *file_wf, int normalize_to_one) {
  FILE *in;
  int size;
  DOUBLE a_check, dummy;
  int i;

  Message("Loading trial wavefunction from file %s... \n", file_wf);

  in = fopen(file_wf, "r");

  if(in == NULL) {
    perror("\nError: can't load trial wave function from file,\nexiting ...");
    Error("\nError: can't load " INPATH "%s file,\nexiting ...", file_wf);
  }

#ifdef TRIAL_LARGE_DISTANCE_FREE_SCATTERING_ASYMPTOTIC // take the energy from scat. solution and use analytic wf. for no potential
  fscanf(in, "Escat= %%"LF"\n", &G->k);
  G->k = sqrt(fabs(G->k));
  gaussian_alpha = gaussian_beta = 0;
  Warning("  Assuming interaction potential is added to local energy, setting gaussian_alpha = gaussian_beta = 0;\n");
#endif

  fscanf(in, "N= %i a= %"LF"\n", &size, &a_check);

#ifdef INTERACTION12_COULOMB // bilayer
  if(a_check != bilayer_width) Error("Can't continue: bilayer_width = %"LG" although in 'inwf.in' a = %"LG"\n", bilayer_width, a_check);
#else
#ifdef INTERACTION12_DIPOLE_BILAYER
  if (a_check != bilayer_width) Error("Can't continue: a = %"LG" although in 'inwf.in' a = %"LG"\n", bilayer_width, a_check);
#else
  if(a_check != a) Error("Can't continue: a = %"LG" although in 'inwf.in' a = %"LG"\n", a, a_check);
#endif
#endif

  AllocateWFGrid(G, size);
  G->size = size;

  fscanf(in, "x f lnf fp Eloc E EV\n");
  for(i=0; i<size; i++) {
    fscanf(in,"%"LF" %"LF" %"LF" %"LF" %"LF" %"LF" %"LF"\n", &G->x[i], &G->f[i], &G->lnf[i], &G->fp[i], &G->E[i], &dummy, &dummy);
  }
  fclose(in);

  G->min = G->x[0];
  G->max = G->x[size-1];
  if(G->max < G->min) Error(" not valid input file, G->max = %e must be larger than G->min = %lf\n", G->max, G->min);
  G->step = (G->max - G->min) / (DOUBLE) (size-1);
  G->I_step = 1./G->step;
  G->max2 = G->max*G->max;

  if(normalize_to_one && G->f[size - 1] != 1.) { // make sure that f(L/2) = 1
    Warning("  last element is different from 1 and equals to %lf. Adjusting the w.f. ...\n, G->f[size - 1]");
    for(i=0; i<size; i++) G->f[i] /= G->f[size-1];
    for(i=0; i<size; i++) G->lnf[i] -= G->lnf[size-1];
  }

  if(fabs(G->lnf[0])<1e-6 && fabs(G->lnf[size-1])<1e-6) {
    Warning("  lnf[] = 0, setting from f(r)\n");
    for(i=0; i<size; i++) G->lnf[i] = log(G->f[i]);
  }

  Message("done\n");
}

/************************** Sample Trial Wave Function **********************/
void SampleTrialWaveFunction11(struct Grid *G, DOUBLE min, DOUBLE max, long int size, DOUBLE (*U)(DOUBLE x), DOUBLE (*Fp)(DOUBLE x), DOUBLE (*E)(DOUBLE x)) {
  int i;
  DOUBLE x;
  DOUBLE error1 ,error2, error3;
  DOUBLE error_max1, error_max2, error_max3;
  DOUBLE x1, x2, x3;

  Message("Sampling trial wavefunction (up - up)... ");

  AllocateWFGrid(G, size);

  G->size = size;
  G->step = (max - min) / (DOUBLE) size;
  G->I_step = (DOUBLE) size / (max - min);
  G->max = max;
  G->min = min + G->step;
  G->max2 = G->max*G->max;

  for(i=0; i<size; i++) {
    x = G->min+(DOUBLE)(i)*G->step;
    G->x[i] = x;
    G->lnf[i] = U(x);
    G->fp[i] = Fp(x);
    G->E[i] = E(x);
  };

  error_max1 = error_max2 = error_max3 = 0.;
  x1 = x2 = x3 = 0;
  for(i=0; i<size; i++) {
    x = (DOUBLE)(i+0.5)*G->max/(DOUBLE) size;
    error1 = (InterpolateU11(G, x))?(fabs((InterpolateU11(G, x)-U(x))/U(x))):0;
    if(error1>error_max1) {
      error_max1 = error1;
      x1 = x;
    }
    error2 = (InterpolateFp11(G, x))?(fabs((InterpolateFp11(G, x)-Fp(x))/Fp(x))):0;
    if(error2>error_max2) {
      error_max2 = error2;
      x2 = x;
    }
    error3 = (InterpolateE11(G, x))?(fabs((InterpolateE11(G, x)-E(x))/E(x))):0;
    if(error3>error_max3) {
      error_max3 = error3;
      x3 = x;
    }
  }

  Message("\n  Maximal error of w.f. interpolation:");
  Message("\n   F(r) - %"LG"%% at r=%"LG" (%"LG" instead of %"LG")", error_max1*100, x1, InterpolateU11(G, x1), U(x1));
  Message("\n   Fp(r)- %"LG"%% at r=%"LG" (%"LG" instead of %"LG")", error_max2*100, x2, InterpolateFp11(G, x2), Fp(x2));
  Message("\n   E(r) - %"LG"%% at r=%"LG" (%"LG" instead of %"LG")", error_max3*100, x3, InterpolateE11(G, x3), E(x3));
  Message("\ndone\n");
}

/************************** Sample Trial Wave Function **********************/
void SampleTrialWaveFunction12(struct Grid *G, DOUBLE min, DOUBLE max, long int size, DOUBLE (*U)(DOUBLE x), DOUBLE (*Fp)(DOUBLE x), DOUBLE (*E)(DOUBLE x)) {
  int i;
  DOUBLE x;
  DOUBLE error1 ,error2, error3;
  DOUBLE error_max1, error_max2, error_max3;
  DOUBLE x1, x2, x3;

  Message("Sampling trial wavefunction (orbital)... ");

  AllocateWFGrid(G, size);

  G->size = size;
  G->step = (max - min) / (DOUBLE) size;
  G->I_step = (DOUBLE) size / (max - min);
  G->max = max;
  G->min = min + G->step;
  G->max2 = G->max*G->max;

  for(i=0; i<size; i++) {
    x = G->min+(DOUBLE)(i)*G->step;
    G->x[i] = x;
    G->lnf[i] = U(x);
    G->fp[i] = Fp(x);
    G->E[i] = E(x);
  };

  error_max1 = error_max2 = error_max3 = 0.;
  x1 = x2 = x3 = 0;
  for(i=0; i<size; i++) {
    x = (DOUBLE)(i+0.5)*G->max/(DOUBLE) size;
    error1 = (InterpolateU12(G, x))?(fabs((InterpolateU12(G, x)-U(x))/U(x))):0;
    if(error1>error_max1) {
      error_max1 = error1;
      x1 = x;
    }
    error2 = (InterpolateFp12(G, x))?(fabs((InterpolateFp12(G, x)-Fp(x))/Fp(x))):0;
    if(error2>error_max2) {
      error_max2 = error2;
      x2 = x;
    }
    error3 = (InterpolateE12(G, x))?(fabs((InterpolateE12(G, x)-E(x))/E(x))):0;
    if(error3>error_max3) {
      error_max3 = error3;
      x3 = x;
    }
  }

  Message("\n  Maximal error of w.f. interpolation:");
  Message("\n   F(r) - %"LG"%% at r=%"LG" (%"LG" instead of %"LG")", error_max1*100, x1, InterpolateU12(G, x1), U(x1));
  Message("\n   Fp(r)- %"LG"%% at r=%"LG" (%"LG" instead of %"LG")", error_max2*100, x2, InterpolateFp12(G, x2), Fp(x2));
  Message("\n   E(r) - %"LG"%% at r=%"LG" (%"LG" instead of %"LG")", error_max3*100, x3, InterpolateE12(G, x3), E(x3));
  Message("\ndone\n");
}

/************************** Sample Trial Wave Function **********************/
void SampleTrialWaveFunctionOrbital(struct Grid *G, DOUBLE min, DOUBLE max, long int size, DOUBLE (*U)(DOUBLE x), DOUBLE (*Fp)(DOUBLE x), DOUBLE (*E)(DOUBLE x)) {
  int i;
  DOUBLE x;

  Message("Sampling trial wavefunction (up - down)... ");

  AllocateWFGrid(G, size);

  G->size = size;
  G->step = (max - min) / (DOUBLE) size;
  G->I_step = (DOUBLE) size / (max - min);
  G->max = max;
  G->min = min + G->step;
  G->max2 = G->max*G->max;

  for(i=0; i<size; i++) {
    x = G->min+(DOUBLE)(i)*G->step;
    G->x[i] = x;
    G->lnf[i] = U(x);
    G->fp[i] = Fp(x);
    G->E[i] = E(x);
  };
  Message("\ndone\n");
}

/************************** Construct Grid Fermion ************************/
void ConstructGridSquareWellFreeState(struct Grid *G) {
  DOUBLE kappa, k;
  DOUBLE V,y,ymin,ymax,x,xmin,xmax;
  DOUBLE precision = 1e-10;
  DOUBLE trial_delta_check;
  int search = ON;
  //DOUBLE Lhalf = 15;

  // matching conditions
  V = Lhalf - 1;
  //kappa = sqrt(trial_Vo);
  kappa = sqrt(2.*m_mu*trial_Vo);
  xmin = 1e-8; //1e-4
  xmax = 100; // 4
  ymin = (arctg(xmin*Lhalf) - arctg(xmin/sqrt(xmin*xmin+kappa*kappa)*tg(sqrt(xmin*xmin+kappa*kappa))))/xmin - V;
  ymax = (arctg(xmax*Lhalf) - arctg(xmax/sqrt(xmax*xmax+kappa*kappa)*tg(sqrt(xmax*xmax+kappa*kappa))))/xmax - V;

  if(ymin*ymax > 0) Error("ConstructGridSquareWellFreeState: cannot construct the grid : No solution found");
  while(search) {
    x = (xmin+xmax)/2.;
    y = (arctg(x*Lhalf) - arctg(x/sqrt(x*x+kappa*kappa)*tg(sqrt(x*x+kappa*kappa))))/x - V;

    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }

    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y = (arctg(x*Lhalf) - arctg(x/sqrt(x*x+kappa*kappa)*tg(sqrt(x*x+kappa*kappa))))/x - V;
      search = OFF;
    }
  }
  k = x;

  Message("  Solution k = %"LE", error %"LE"\n", k, y);

  sqE = k;
  sqE2 = k*k;
  trial_Vo = kappa*kappa/(2.*m_mu);
  trial_Kappa2 = k*k + kappa*kappa;
  trial_Kappa = sqrt(trial_Kappa2);

  trial_delta = arctg(k*Lhalf) - k*Lhalf;
  trial_delta_check = arctg(k/trial_Kappa*tg(trial_Kappa)) - k;
  if(fabs((trial_delta_check-trial_delta)/trial_delta)>1e-8) Error("  Invalid solution\n");

  A3SW = Lhalf/Sin(k*Lhalf+trial_delta);
  A1SW = A3SW*Sin(k+trial_delta)/Sin(trial_Kappa);

  //Xi = A1SW*Sin(trial_Kappa);  Xi = A3SW*Sin(k+trial_delta);
  Message(" Matching to one -> %"LF"", A3SW*Sin(k*Lhalf+trial_delta)/Lhalf);

  G->min = 0;
  G->max = Lhalf;
  G->max2 = G->max*G->max;
  Rpar = 1.;
}

#ifdef TRIAL_SQUARE_WELL_FS
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r<Ro)
    return Log(fabs(A1SW*Sin(trial_Kappa*r)/r));
  else if(r<Lhalf)
    return Log(fabs(A3SW*Sin(sqE*r+trial_delta)/r));
  else
    return 0;
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  if(r<Ro)
    return trial_Kappa/tg(trial_Kappa*r) - 1./r;
  else if(r<Lhalf)
    return sqE/tg(sqE*r+trial_delta) - 1./r;
  else
    return 0;
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  DOUBLE fp=0;

  if(r<Ro) {
    fp = trial_Kappa/tg(trial_Kappa*r) - 1./r;
    return trial_Kappa2 + fp*fp;
  }
  else if(r<Lhalf) {
    fp = sqE/tg(sqE*r+trial_delta) - 1./r;
    return sqE2 + fp*fp;
  }
  else
    return 0;
}
#endif

/************************** Construct Grid Fermion ************************/
void ConstructGridSquareWellFreeStatePWave11(struct Grid *G) {
  DOUBLE kappa, k, K;
  DOUBLE y,ymin,ymax,x,xmin,xmax,r;
  DOUBLE precision = 1e-14;
  int search = ON;

  Message("wf 11: (3D) p-wave SW (E>0) A[Sin(Kr)/Kr-Cos(Kr)]/r, B[Sin(kr+delta)/kr-Cos(kr+delta)]/r, no free parameters \n");

  // matching conditions
  // kappa = sqrt(trial11_Vo);
  // kappa = sqrt(2.*m_mu*trial11_Vo);
  kappa = sqrt(m_up*trial11_Vo);
  xmin = 1e-4;
  xmax = 0.02;

  x = xmin;
  K = sqrt(kappa*kappa+x*x);
  ymin = -((-2.*Lhalf*x+(2.-Lhalf2*x*x)*tg(Lhalf*x))/(-2.+Lhalf2*x*x - 2*Lhalf*x*tg(Lhalf*x)))+(K*K*Ro*x-(K*K-x*x+K*Ro*x*x/tg(K*Ro))*tg(Ro*x))/(K*K - x*x + K*Ro*x*x/tg(K*Ro) + K*K*Ro*x*tg(Ro*x));
  x = xmax;
  K = sqrt(kappa*kappa+x*x);
  ymax = -((-2.*Lhalf*x+(2.-Lhalf2*x*x)*tg(Lhalf*x))/(-2.+Lhalf2*x*x - 2*Lhalf*x*tg(Lhalf*x)))+(K*K*Ro*x-(K*K-x*x+K*Ro*x*x/tg(K*Ro))*tg(Ro*x))/(K*K - x*x + K*Ro*x*x/tg(K*Ro) + K*K*Ro*x*tg(Ro*x));

  if(ymin*ymax > 0) Error("Can't construct the grid : No solution found");
  while(search) {
    x = (xmin+xmax)/2.;
    K = sqrt(kappa*kappa+x*x);
    y = -((-2.*Lhalf*x+(2.-Lhalf2*x*x)*tg(Lhalf*x))/(-2.+Lhalf2*x*x - 2*Lhalf*x*tg(Lhalf*x)))+(K*K*Ro*x-(K*K-x*x+K*Ro*x*x/tg(K*Ro))*tg(Ro*x))/(K*K - x*x + K*Ro*x*x/tg(K*Ro) + K*K*Ro*x*tg(Ro*x));

    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }

    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      K = sqrt(kappa*kappa+x*x);
      y = -((-2.*Lhalf*x+(2.-Lhalf2*x*x)*tg(Lhalf*x))/(-2.+Lhalf2*x*x - 2*Lhalf*x*tg(Lhalf*x)))+(K*K*Ro*x-(K*K-x*x+K*Ro*x*x/tg(K*Ro))*tg(Ro*x))/(K*K - x*x + K*Ro*x*x/tg(K*Ro) + K*K*Ro*x*tg(Ro*x));
      search = OFF;
    }
  }
  k = x;

  Message("  Solution k = %"LE", error %"LE"\n", k, y);

  sqE11 = k;
  sqE211 = k*k;
  //trial11_Vo = kappa*kappa;
  trial11_Kappa2 = k*k + kappa*kappa;
  trial11_Kappa = sqrt(trial11_Kappa2);

  trial11_delta = (-2.*x*Lhalf+(2.-x*x*Lhalf2)*tg(x*Lhalf))/(-2.+x*x*Lhalf2 - 2.*x*Lhalf*tg(x*Lhalf));//!!!
  trial11_delta = atan((-2.*x*Lhalf+(2.-x*x*Lhalf2)*tg(x*Lhalf))/(-2.+x*x*Lhalf2 - 2.*x*Lhalf*tg(x*Lhalf)));

  r = Lhalf;
  A2SW11 = -Log(fabs((Sin(sqE11*r+trial11_delta)/(sqE11*r)-Cos(sqE11*r+trial11_delta))/r));

  r = 1.;
  A1SW11 = Log(fabs((Sin(sqE11*r+trial11_delta)/(sqE11*r)-Cos(sqE11*r+trial11_delta))/r))+A2SW11-Log(fabs((Sin(trial11_Kappa*r)/(trial11_Kappa*r)-Cos(trial11_Kappa*r))/r));

  r = Lhalf;
  r = - 2./r + sqE11*r/(1.-sqE211*r/tg(sqE11*r+trial11_delta));

  G->min = 0;
  G->max = Lhalf;
  G->max2 = G->max*G->max;
  Rpar = 1.;
}
#ifdef TRIAL11_SQUARE_WELL_FS_P_WAVE
DOUBLE InterpolateExactU11(DOUBLE r) {
  if(r<Ro)
    return Log(fabs((Sin(trial11_Kappa*r)/(trial11_Kappa*r)-Cos(trial11_Kappa*r))/r))+A1SW11;
  else if(r<Lhalf)
    return Log(fabs((Sin(sqE11*r+trial11_delta)/(sqE11*r)-Cos(sqE11*r+trial11_delta))/r))+A2SW11;
  else
    return 0;
}

DOUBLE InterpolateExactFp11(DOUBLE r) {
  if(r<Ro)
    return - 2./r + trial11_Kappa2*r/(1.-trial11_Kappa*r/tg(trial11_Kappa*r));
  else if(r<Lhalf)
    return - 2./r + sqE211*r/(1.-sqE11*r/tg(sqE11*r+trial11_delta));
  else
    return 0;
}

DOUBLE InterpolateExactE11(DOUBLE r) {
  DOUBLE fp=0;

  fp = InterpolateExactFp11(r);
  if(r<Ro) {
    return trial11_Kappa2 + fp*fp - 2./(r*r); // p-wave solution
  }
  else if(r<Lhalf) {
    return sqE211 + fp*fp - 2./(r*r);
  }
  else
    return 0;
}

DOUBLE InterpolateExactU22(DOUBLE r) {
  if(r<Ro)
    return Log(fabs((Sin(trial11_Kappa*r)/(trial11_Kappa*r)-Cos(trial11_Kappa*r))/r))+A1SW11;
  else if(r<Lhalf)
    return Log(fabs((Sin(sqE11*r+trial11_delta)/(sqE11*r)-Cos(sqE11*r+trial11_delta))/r))+A2SW11;
  else
    return 0;
}

DOUBLE InterpolateExactFp22(DOUBLE r) {
  if(r<Ro)
    return - 2./r + trial11_Kappa2*r/(1.-trial11_Kappa*r/tg(trial11_Kappa*r));
  else if(r<Lhalf)
    return - 2./r + sqE211*r/(1.-sqE11*r/tg(sqE11*r+trial11_delta));
  else
    return 0;
}

DOUBLE InterpolateExactE22(DOUBLE r) {
  DOUBLE fp=0;

  fp = InterpolateExactFp22(r);
  if(r<Ro) {
    return trial11_Kappa2 + fp*fp - 2./(r*r); // p-wave solution
  }
  else if(r<Lhalf) {
    return sqE211 + fp*fp - 2./(r*r);
  }
  else
    return 0;
}
#endif

/************************** Find SW Binding Energy ************************/
void FindSWBindingEnergy(void) {
  DOUBLE kappa, k;
  DOUBLE V,y,ymin,ymax,x,xmin,xmax;
  DOUBLE precision = PRECISION;
  int search = ON;
  DOUBLE detuning = PRECISION;
  FILE *out;
  static int first_time = ON;

  Message("  Finding SW binding energy ...\n");

  V = -1;
  //kappa = sqrt(trial_Vo);
  kappa = sqrt(2.*m_mu*trial_Vo);

  // x^2 = Eb, - binding energy
  //xmin = detuning*pi/2.*0.1;
  //xmax = detuning*pi/2.*10.;
  xmin = 1e-10;
  xmax = 1e0;
  if(xmax > kappa) xmax = kappa*0.99;
  ymin = sqrt(-xmin*xmin+kappa*kappa)/tg(Ro*sqrt(-xmin*xmin+kappa*kappa))/xmin - V;
  ymax = sqrt(-xmax*xmax+kappa*kappa)/tg(Ro*sqrt(-xmax*xmax+kappa*kappa))/xmax - V;

  if(ymin*ymax > 0) Error("Can't construct the grid : No solution found");

  search = ON;
  while(search) {
    x = (xmin+xmax)/2.;
    y = sqrt(-x*x+kappa*kappa)/tg(Ro*sqrt(-x*x+kappa*kappa))/x - V;

    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }

    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y = sqrt(-x*x+kappa*kappa)/tg(Ro*sqrt(-x*x+kappa*kappa))/x - V;
      search = OFF;
    }
  }
  k = x;
  if(first_time) Message("  Solution k = %"LE", error %"LE" (k_approx = %"LE")\n", k, y, detuning*pi/2.);

  if(first_time) {
    out = fopen("Eb.dat", "w");
    fprintf(out, "%.15"LE, energy_unit*k*k);
    fclose(out);
  }

  energy_shift += k*k;
  if(first_time) Warning("  Binding energy %"LE" will be subtracted from the total energy !\n", k*k);
  first_time = OFF;
}

/************************** Construct Grid Fermion ************************/
void ConstructGridSquareWellBoundState(struct Grid *G) {
  DOUBLE kappa, k;
  DOUBLE V,y,ymin,ymax,x,xmin,xmax;
  DOUBLE precision = 1e-10;
  int search = ON;
  DOUBLE detuning = 1e-10;

  //kappa = sqrt(trial_Vo);
  kappa = sqrt(2.*m_mu*trial_Vo);

  V = -1;

  //xmin = detuning*pi/2.*0.1;
  //xmax = detuning*pi/2.*10.;
  xmin = 1e-10;
  xmax = 1e0;

  ymin = sqrt(-xmin*xmin+kappa*kappa)/tg(sqrt(-xmin*xmin+kappa*kappa))/xmin - V;
  ymax = sqrt(-xmax*xmax+kappa*kappa)/tg(sqrt(-xmax*xmax+kappa*kappa))/xmax - V;

  if(ymin*ymax > 0) Error("Can't construct the grid : No solution found");

  search = ON;
  while(search) {
    x = (xmin+xmax)/2.;
    y = sqrt(-x*x+kappa*kappa)/tg(sqrt(-x*x+kappa*kappa))/x - V;

    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }

    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y = sqrt(-x*x+kappa*kappa)/tg(sqrt(-x*x+kappa*kappa))/x - V;
      search = OFF;
    }
  }
  k = x;
  Message("  Solution k = %"LE", error %"LE" (k_approx = %"LE")\n", k, y, detuning*pi/2.);

  sqE = k;
  sqE2 = k*k;
  trial_Kappa2 = kappa*kappa - k*k;
  trial_Kappa = sqrt(trial_Kappa2);

  // matching to 1
  Rpar = Lhalf;
  //Rpar = 10*a;

  A3SW = Rpar*Exp(k*Rpar);
  A1SW = A3SW*Exp(-sqE)/Sin(trial_Kappa);
  Message("  Scattering energy %"LE", approx %"LE"\n", sqE2, (detuning*pi/2.)*(detuning*pi/2.));

  G->min = 0;
  G->max = Lhalf;
  G->max2 = G->max*G->max;
}

/************************** Construct Grid Fermion ************************/
void ConstructGridSquareWellBoundStatePhonons(struct Grid *G) {
  DOUBLE kappa, k;
  DOUBLE V,y,ymin,ymax,x,xmin,xmax;
  DOUBLE precision = 1e-10;
  int search = ON;
  DOUBLE detuning = 1e-10;
  DOUBLE r;

  //kappa = sqrt(trial_Vo);
  kappa = sqrt(2.*m_mu*trial_Vo);
  // matching at Rpar12
  Warning("  Matching distance Rpar12 = %lf [L/2] -> %lf [R]\n", Rpar12, Rpar12*Lhalf);
  if(Rpar12>1 || Rpar12 <0) Error(" Rpar12 out of (0,1) range!\n");
  Rpar12 *= Lhalf;
  if(Rpar12<RoSW) Error("  Rpar12<RoSW, should be other way around");

  Message(" wf 12: SquareWellBoundStatePhonons\n");
  //xmin = detuning*pi/2.*0.1;
  //xmax = detuning*pi/2.*10.;
  xmin = 1e-10;
  xmax = 1e0;

  //xmin = sqrt(kappa*kappa -PI*PI*(1.-1e-6));
  //if(xmax > kappa) xmax = kappa*0.99;
  xmax = sqrt(kappa*kappa - 0.99*(PI/(2.*RoSW))*(PI/(2.*RoSW)));

  V = -1;

  //ymin = sqrt(-xmin*xmin+kappa*kappa)/tg(Ro*sqrt(-xmin*xmin+kappa*kappa))/xmin - V;
  ymin = sqrt(-xmin*xmin+kappa*kappa)/tg(RoSW*sqrt(-xmin*xmin+kappa*kappa))/xmin - V;
  ymax = sqrt(-xmax*xmax+kappa*kappa)/tg(RoSW*sqrt(-xmax*xmax+kappa*kappa))/xmax - V;

  if(ymin*ymax > 0) Error("Can't construct the grid : No solution found");

  search = ON;
  while(search) {
    x = (xmin+xmax)/2.;
    y = sqrt(-x*x+kappa*kappa)/tg(RoSW*sqrt(-x*x+kappa*kappa))/x - V;

    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }

    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y = sqrt(-x*x+kappa*kappa)/tg(RoSW*sqrt(-x*x+kappa*kappa))/x - V;
      search = OFF;
    }
  }
  k = x;
  Message("  Solution k = %"LE", error %"LE" (k_approx = %"LE")\n", k, y, detuning*pi/2.);

  sqE = k;
  sqE2 = k*k;
  Message("  Scattering energy %"LE", approx %"LE"\n", sqE2, (detuning*pi/2.)*(detuning*pi/2.));
  trial_Kappa2 = kappa*kappa - k*k;
  trial_Kappa = sqrt(trial_Kappa2);

  //f'(Rpar12)
  r = Rpar12;
  A5SW = (- sqE - 1./r) / (2.*(1./(r*r*r)-1./((L-r)*(L-r)*(L-r))));
  // A5SW = -(1. + k*Rpar12)/(2.*Rpar12*(1./(Rpar12*Rpar12*Rpar12) - 1./((L-Rpar12)*(L-Rpar12)*(L-Rpar12))));

  // A5SW -> Ctrial
  // f(L/2)
  A2SW = Exp(2.*A5SW/(Lhalf*Lhalf));
  // f(Rpar)
  A3SW = Rpar12*Exp(k*Rpar12)*A2SW*Exp(-A5SW/(Rpar12*Rpar12)-A5SW/((L-Rpar12)*(L-Rpar12)));
  //A1SW = A3SW*Exp(-sqE)/Sin(trial_Kappa);
  // take log of coef for U(r)
  A3SW = Log(A3SW);
  A2SW = Log(A2SW);
  A1SW = A3SW-sqE*RoSW-Log(fabs(Sin(trial_Kappa*RoSW)));

  G->min = 0;
  G->max = Lhalf;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_SQUARE_WELL_BS_PHONONS
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r<RoSW)
    return A1SW + Log(fabs(Sin(trial_Kappa*r)/r));
  else if(r<Rpar12)
    return A3SW-sqE*r-Log(r);
  else if(r<Lhalf)
    return A2SW - A5SW*(1./(r*r)+1./((L-r)*(L-r)));
  else 
    return 0;
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  if(r<RoSW)
    return trial_Kappa/tg(trial_Kappa*r) - 1./r;
  else if(r<Rpar12)
    return - sqE - 1./r;
  else if(r<Lhalf)
    return 2.*A5SW*(1./(r*r*r)-1./((L-r)*(L-r)*(L-r)));
  else
    return 0.;
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  DOUBLE fp;

  if(r<RoSW) {
    fp = trial_Kappa/tg(trial_Kappa*r) - 1./r;
    return trial_Kappa2 + fp*fp;
  }
  else if(r<Rpar12){
    fp = -sqE - 1./r;
    return -sqE2 + fp*fp;
  }
  else if(r<Lhalf)
    return 2.*A5SW*(L*L*L*L - 4.*L*L*L*r + 6.*L*L*r*r - 2.*L*r*r*r + 2.*r*r*r*r)/(r*r*r*r*(L-r)*(L-r)*(L-r)*(L-r));
  else
    return 0.;
}
#endif

/************************** Construct Grid Fermion ************************/
void ConstructGridSquareWellBoundStatePhonons11(struct Grid *G) {
  DOUBLE kappa, k;
  DOUBLE V,y,ymin,ymax,x,xmin,xmax;
  DOUBLE precision = 1e-10;
  int search = ON;
  int bs_in_p_wave = OFF; // ON - in p-wave, OFF - in s-wave
  DOUBLE detuning = 1e-10;

  //kappa = sqrt(trial11_Vo);
  //kappa = sqrt(2.*m_mu*trial11_Vo);
  kappa = sqrt(m_up*trial11_Vo);

  Message(" wf 11: SquareWellBoundStatePhonons\n");

  //xmin = detuning*pi/2.*0.1;
  //xmax = detuning*pi/2.*10.;
  if(bs_in_p_wave) { // for b.s. in p-wave
    xmin = sqrt(kappa*kappa-PI*PI) + precision;
    if(kappa<PI) Error(" kappa<PI, cannot construct w.f., the interaction potential should support a bound state!");
    xmin = sqrt(kappa*kappa-PI*PI)+precision;
    xmax = xmin*2.;
  }
  else { // b.s. in s-wave
    xmin = precision;
    xmax = 1e10;
  }

  if(-xmin*xmin+kappa*kappa<0) xmin = kappa-precision;
  if(-xmax*xmax+kappa*kappa<0) xmax = kappa-precision;

  // b.s. energy E = -hbar^2 k^2/2m
  // log derivative condition -k = \sqrt(kappa^2-k^2) / tg[\sqrt(kappa^2 - k^2)R]
  V = -1;
  ymin = sqrt(-xmin*xmin+kappa*kappa)/tg(sqrt(-xmin*xmin+kappa*kappa))/xmin - V;
  ymax = sqrt(-xmax*xmax+kappa*kappa)/tg(sqrt(-xmax*xmax+kappa*kappa))/xmax - V;

  if(ymin*ymax > 0) Error("Can't construct the grid : No solution found");

  search = ON;
  while(search) {
    x = (xmin+xmax)/2.;
    y = sqrt(-x*x+kappa*kappa)/tg(sqrt(-x*x+kappa*kappa))/x - V;

    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }

    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y = sqrt(-x*x+kappa*kappa)/tg(sqrt(-x*x+kappa*kappa))/x - V;
      search = OFF;
    }
  }
  k = x;
  Message("  Solution k = %"LE", error %"LE" (k_approx = %"LE")\n", k, y, detuning*pi/2.);

  sqE11 = k;
  sqE211 = k*k;
  Message("  Scattering energy %"LE", approx %"LE"\n", sqE211, (detuning*pi/2.)*(detuning*pi/2.));
  trial11_Kappa2 = kappa*kappa - k*k;
  trial11_Kappa = sqrt(trial11_Kappa2);

  // matching at Rpar
  Warning("  Matching distance Rpar = %lf [L/2] -> %lf [R]\n", Rpar, Rpar*Lhalf);
  if(Rpar>1 || Rpar <0) Error(" Rpar out of (0,1) range!\n");
  Rpar *= Lhalf;
  if(Rpar<Ro) Error("Rpar must be larger than range of the potential\n");
  A5SW11 = -(1 + k*Rpar)/(2.*Rpar*(1/(Rpar*Rpar*Rpar) - 1/((L-Rpar)*(L-Rpar)*(L-Rpar))));

  // A5SW -> Ctrial
  //A2SW11 = Exp(2.*A5SW11/(Lhalf*Lhalf));
  A2SW11 = 2.*A5SW11/(Lhalf*Lhalf);
  //A3SW11 = Rpar*Exp(k*Rpar)*A2SW11*Exp(-A5SW/(Rpar*Rpar)-A5SW11/((L-Rpar)*(L-Rpar)));
  A3SW11 = sqE11*Rpar+Log(Rpar) + A2SW11 - A5SW11*(1./(Rpar*Rpar)+1./((L-Rpar)*(L-Rpar)));
  //A1SW11 = A3SW11*Exp(-sqE11)/Sin(trial11_Kappa);
  A1SW11 = A3SW11-sqE11 - Log(fabs(Sin(trial11_Kappa)));

  // take log of coef for U(r)
  //A3SW11 = Log(A3SW11);
  //A2SW11 = Log(A2SW11);

  G->min = 0;
  G->max = Lhalf;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL11_SQUARE_WELL_BS_PHONONS
DOUBLE InterpolateExactU11(DOUBLE r) {
  if(r<1)
    return A1SW11+Log(fabs(Sin(trial11_Kappa*r)/r));
  else if(r<Rpar)
    return A3SW11-sqE11*r-Log(r);
  else
    return A2SW11 - A5SW11*(1./(r*r)+1./((L-r)*(L-r)));
}

DOUBLE InterpolateExactFp11(DOUBLE r) {
  if(r<1)
    return trial11_Kappa/tg(trial11_Kappa*r) - 1./r;
  else if(r<Rpar)
    return - sqE11 - 1./r;
  else
    return 2.*A5SW11*(1./(r*r*r)-1./((L-r)*(L-r)*(L-r)));
}

DOUBLE InterpolateExactE11(DOUBLE r) {
  DOUBLE fp;

  if(r<1) {
    fp = trial11_Kappa/tg(trial11_Kappa*r) - 1./r;
    return trial11_Kappa2 + fp*fp;
  }
  else if(r<Rpar){
    fp = -sqE11 - 1./r;
    return -sqE211 + fp*fp;
  }
  else
    return 2.*A5SW11*(L*L*L*L - 4.*L*L*L*r + 6.*L*L*r*r - 2.*L*r*r*r + 2.*r*r*r*r)/(r*r*r*r*(L-r)*(L-r)*(L-r)*(L-r));
}

DOUBLE InterpolateExactU22(DOUBLE r) {
  if(r<1)
    return A1SW11+Log(fabs(Sin(trial11_Kappa*r)/r));
  else if(r<Rpar)
    return A3SW11-sqE11*r-Log(r);
  else
    return A2SW11 - A5SW11*(1./(r*r)+1./((L-r)*(L-r)));
}

DOUBLE InterpolateExactFp22(DOUBLE r) {
  if(r<1)
    return trial11_Kappa/tg(trial11_Kappa*r) - 1./r;
  else if(r<Rpar)
    return - sqE11 - 1./r;
  else
    return 2.*A5SW11*(1./(r*r*r)-1./((L-r)*(L-r)*(L-r)));
}

DOUBLE InterpolateExactE22(DOUBLE r) {
  DOUBLE fp;

  if(r<1) {
    fp = trial11_Kappa/tg(trial11_Kappa*r) - 1./r;
    return trial11_Kappa2 + fp*fp;
  }
  else if(r<Rpar){
    fp = -sqE11 - 1./r;
    return -sqE211 + fp*fp;
  }
  else
    return 2.*A5SW11*(L*L*L*L - 4.*L*L*L*r + 6.*L*L*r*r - 2.*L*r*r*r + 2.*r*r*r*r)/(r*r*r*r*(L-r)*(L-r)*(L-r)*(L-r));
}
#endif

void ConstructGridCh2Phonon12(void) {
  Message("  wf12: th(r)/r; Exp(-C/r^2-C/(L-r)^2)), Rpar12 - var. par");
  Warning("  Rpar12 -> Rpar12 L/2 (%lf -> %lf)\n", Rpar12, Rpar12*Lhalf);
  Rpar12 *= Lhalf;

  // A1SW -> A, A2SW -> B, A5SW -> C
  A5SW = (-Rpar12*Rpar12*(L-Rpar12)*(L-Rpar12)*(L-Rpar12)/sinh(2.*Rpar12)*(sinh(2.*Rpar12)-2*Rpar12))/(2.*(L*L*L - 3.*L*L*Rpar12 + 3.*L*Rpar12*Rpar12 - 2.*Rpar12*Rpar12*Rpar12));

  A2SW = Exp(2.*A5SW/(Lhalf*Lhalf));
  A1SW = A2SW*Exp(-A5SW*(1./(Rpar12*Rpar12)+1./((L-Rpar12)*(L-Rpar12))))/tanh(Rpar12)*Rpar12;

  // take log of coef for U(r)
  A1SW = Log(A1SW);
  A2SW = Log(A2SW);

  G12.min = 0;
  G12.max = Lhalf;
  G12.max2 = G12.max*G12.max;
}

#ifdef TRIAL_CH2_PHONON12
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r<Rpar12)
    return A1SW + Log(tanh(r)/r);
  else
    return A2SW - A5SW*(1./(r*r)+1./((L-r)*(L-r)));
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  if(r<Rpar12) {
    return 2./sinh(2.*r)-1./r;
  }
    return 2.*A5SW*(1./(r*r*r)-1./((L-r)*(L-r)*(L-r)));
}

// Eloc = f" +2/r f' = u"/r
DOUBLE InterpolateExactE12(DOUBLE r) {
  if(r<Rpar12)
    return (1.+4.*r*(r/tanh(2.*r)-1)/sinh(2.*r))/(r*r);
  else
    return 2.*A5SW*(L*L*L*L - 4.*L*L*L*r + 6.*L*L*r*r - 2.*L*r*r*r + 2.*r*r*r*r)/(r*r*r*r*(L-r)*(L-r)*(L-r)*(L-r));
}
#endif


#ifdef TRIAL_CH2_TRAP12
DOUBLE InterpolateExactU12(DOUBLE r) {
  return Log(tanh(r)/r);
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  return 2./sinh(2.*r)-1./r;
}

// Eloc = f" +2/r f' = u"/r
DOUBLE InterpolateExactE12(DOUBLE r) {
  return (1.+4.*r*(r/tanh(2.*r)-1)/sinh(2.*r))/(r*r);
}
#endif

void ConstructGridCh2Phonon11(void) {
  Message("  wf11: th(r)/r; Exp(-C/r^2-C/(L-r)^2)), Rpar11 - var. par");
  Warning("  Rpar11 -> Rpar11 L/2 (%lf -> %lf)\n", Rpar11, Rpar11*Lhalf);
  Rpar11 *= Lhalf;

  // A1SW11 -> A, A2SW11 -> B, A5SW11 -> C
  A5SW11 = (-Rpar11*Rpar11*(L-Rpar11)*(L-Rpar11)*(L-Rpar11)/sinh(2.*Rpar11)*(sinh(2.*Rpar11)-2*Rpar11))/(2.*(L*L*L - 3.*L*L*Rpar11 + 3.*L*Rpar11*Rpar11 - 2.*Rpar11*Rpar11*Rpar11));

  A2SW11 = Exp(2.*A5SW11/(Lhalf*Lhalf));
  A1SW11 = A2SW11*Exp(-A5SW11*(1./(Rpar11*Rpar11)+1./((L-Rpar11)*(L-Rpar11))))/tanh(Rpar11)*Rpar11;

  // take log of coef for U(r)
  A1SW11 = Log(A1SW11);
  A2SW11 = Log(A2SW11);

  G11.min = 0;
  G11.max = Lhalf;
  G11.max2 = G11.max*G11.max;
}

#ifdef TRIAL_CH2_PHONON11
DOUBLE InterpolateExactU11(DOUBLE r) {
  if(r<Rpar11)
    return A1SW11 + Log(tanh(r)/r);
  else
    return A2SW11 - A5SW11*(1./(r*r)+1./((L-r)*(L-r)));
}

DOUBLE InterpolateExactFp11(DOUBLE r) {
  if(r<Rpar11) {
    return 2./sinh(2.*r)-1./r;
  }
    return 2.*A5SW11*(1./(r*r*r)-1./((L-r)*(L-r)*(L-r)));
}

// Eloc = f" +2/r f' = u"/r
DOUBLE InterpolateExactE11(DOUBLE r) {
  if(r<Rpar11)
    return (1.+4.*r*(r/tanh(2.*r)-1)/sinh(2.*r))/(r*r);
  else
    return 2.*A5SW11*(L*L*L*L - 4.*L*L*L*r + 6.*L*L*r*r - 2.*L*r*r*r + 2.*r*r*r*r)/(r*r*r*r*(L-r)*(L-r)*(L-r)*(L-r));
}
#endif

/************************** Construct Grid Fermion ************************/
void ConstructGridSquareWellSmooth(struct Grid *G) {
  DOUBLE kappa, k;
  DOUBLE V,y,ymin,ymax,x,xmin,xmax;
  DOUBLE precision = 1e-10;
  int search = ON;
  DOUBLE detuning = 1e-10;

  Warning("  Resonance scattering (bound state)\n");
  kappa = pi/2. + detuning;

  V = -1;

  xmin = detuning*pi/2.*0.1;
  xmax = detuning*pi/2.*10.;

  ymin = sqrt(-xmin*xmin+kappa*kappa)/tg(sqrt(-xmin*xmin+kappa*kappa))/xmin - V;
  ymax = sqrt(-xmax*xmax+kappa*kappa)/tg(sqrt(-xmax*xmax+kappa*kappa))/xmax - V;

  if(ymin*ymax > 0) Error("Can't construct the grid : No solution found");

  while(search) {
    x = (xmin+xmax)/2.;
    y = sqrt(-x*x+kappa*kappa)/tg(sqrt(-x*x+kappa*kappa))/x - V;

    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }

    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y = sqrt(-x*x+kappa*kappa)/tg(sqrt(-x*x+kappa*kappa))/x - V;
      search = OFF;
    }
  }
  k = x;
  Message("  Solution k = %"LE", error %"LE" (k_approx = %"LE")\n", k, y, detuning*pi/2.);

  sqE = k;
  sqE2 = k*k;
  trial_Vo = kappa*kappa/(2.*m_mu);
  trial_Kappa2 = kappa*kappa - k*k;
  trial_Kappa = sqrt(trial_Kappa2);

  A3SW = Lhalf*Exp(k*Lhalf);
  A1SW = A3SW*Exp(-sqE)/Sin(trial_Kappa);
  alpha_1 = (k+1./Lhalf)/((Lhalf-1)*(Lhalf-1)*((k+1./Lhalf)*(Lhalf-1)-3.));

  Message("  Scattering energy %"LE", approx %"LE"\n", sqE2, (detuning*pi/2.)*(detuning*pi/2.));

  G->min = 0;
  G->max = Lhalf;
  G->max2 = G->max*G->max;
  Rpar = 1.;
}

#ifdef TRIAL_SQUARE_WELL_SMOOTH
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r<Ro)
    return Log(fabs(A1SW*Sin(trial_Kappa*r)/r));
  else
    return Log(fabs(A3SW*Exp(-sqE*r)/r))*(1.-alpha_1*(r-1.)*(r-1.)*(r-1.));
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  DOUBLE r_1;
  if(r<Ro)
    return trial_Kappa/tg(trial_Kappa*r) - 1./r;
  else {
    r_1 = r -1.;
    return - (sqE + 1./r + 3.*alpha_1*r_1*r_1/(1.-alpha_1*r_1*r_1*r_1));
  }
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  DOUBLE fp;
  DOUBLE r_1, r_1cube, dummy, dummy2;

  if(r<Ro) {
    fp = trial_Kappa/tg(trial_Kappa*r) - 1./r;
    return trial_Kappa2 + fp*fp;
  }
  else {
    r_1 = r - 1.;
    r_1cube = r_1*r_1*r_1;
    dummy = (-1 + r_1cube*alpha_1);
    dummy2 = dummy*dummy;
    return (1. - alpha_1*r_1*(alpha_1*r_1*r_1*r_1*(-1. + 2.*(-2. + r)*r) + 2.*(1 + r - 5.*r*r)) +  2.*dummy2*dummy2*r*sqE)/(dummy2*r*r);
  }
}
#endif


/************************** Construct Grid Fermion ************************/
void ConstructGridSh(struct Grid *G) {

  A1SW = Lhalf/tanh(Lhalf);

  G->min = 0;
  G->max = Lhalf;
  G->max2 = G->max*G->max;
  Rpar = 1.;
}

/************************** Construct Grid Lim ****************************/
void ConstructGridLim(struct Grid *G, const DOUBLE xmin, 
  const DOUBLE xmax, const long int size) {
  DOUBLE dx;

  AllocateWFGrid(G, size);

  G->min = xmin;
  G->max = xmax;
  dx = (G->max - G->min) / (DOUBLE) size;
  G->step = dx;
  G->I_step = (DOUBLE) size / (G->max - G->min);
  G->max2 = G->max*G->max;

  // f(x) = 1. - a/x 
  // f'/f = a / (x*(x-a)); 
  // E = (a / (x(x-a)))^2      [3D]
  // E = a / (x (x-a)^2)       [2D]
  // E = a (2x-a) / (x(x-a))^2 [1D]
}

/************************** Construct Grid Lim ****************************/
void ConstructGridPower(struct Grid *G, const DOUBLE xmin, 
  const DOUBLE xmax, const long int size) {
  DOUBLE dx;

  AllocateWFGrid(G, size);

  G->min = xmin;
  G->max = xmax;
  dx = (G->max - G->min) / (DOUBLE) size;
  G->step = dx;
  G->I_step = (DOUBLE) size / (G->max - G->min);
  G->max2 = G->max*G->max;

  // f(x) = 1. - a/x^n 
  // f'/f = na / (x*(x^n-a)); 
  // E = 1/x^2  na/(x^n-a) [n+2-D + na/(x^n-a)], D=1,2,3
}


/************************** Construct Grid HS12 *****************************/
DOUBLE A1HS12, A2HS12, A3HS12, kHS12, EHS12;
// k in units of 2pi/L
void ConstructGridHS12(struct Grid *G) {
  int size = 3;
  DOUBLE dx;
  DOUBLE x;
  DOUBLE aAB;

  aAB = a;

  G->min = aAB;
  G->max = Lhalf;
  dx = (G->max - G->min) / (DOUBLE) (size-1);
  G->step = dx;
  G->I_step = (DOUBLE) size / (G->max - G->min);
  G->max2 = G->max*G->max;

  Message("  Constructing Hard sphere Jastrow term (TRIAL_HS12), r>aAB\n");
  Message("  (2-body solution + symmetrized phonons) \n");
  Message("  parameters: 0<Rpar12<1 \n");
  kHS12 = PI/(Lhalf-aAB); // maximal value of k in units of r_0
  kHS12 /= 2.*PI/L;
  Message("              0<Bpar12<1/(1-2a/L) = %lf\n", kHS12);
  if(Rpar12<0 || Rpar12>1) Error("  Wrong value of Rpar12 = %lf\n", Rpar12);
  if(Bpar12<0 || Bpar12>kHS12) Error("  Wrong value of Bpar12 = %lf\n", Bpar12);
  Warning("  Automatically converting matching distance: Rpar12 = %"LF" [L/2] => Rpar12 = %"LF"[r_0]\n", Rpar12, Rpar12*Lhalf);
  Rpar12 *= Lhalf;
  Warning("  Automatically converting 2-body scattering momentum: Bpar12 = %lf [2pi/L] => Bpar12 = %lf [r_0]\n", Bpar12, Bpar12);
  Bpar12 *= 2.*PI/L;

  kHS12 = Bpar12;
  EHS12 = kHS12*kHS12;

  // f'(Rpar12)
  x = Rpar12;
  A3HS12 = (kHS12 / Tg(kHS12*(x-aAB)) - 1./x)/(2.*(1./(x*x*x)-1./((L-x)*(L-x)*(L-x))));

  // f(L/2)
  x = Lhalf;
  A2HS12 = A3HS12/(x*x) + A3HS12/((L-x)*(L-x));

  // f(Rpar12)
  x = Rpar12;
  A1HS12 = - Log(Sin(kHS12*(x-aAB))/x) + A2HS12 - A3HS12/(x*x) - A3HS12/((L-x)*(L-x));
}

/* executed only if TRIAL_HS12 is defined */
#ifdef TRIAL_HS12
DOUBLE InterpolateExactU12(DOUBLE x) {
  DOUBLE f;

  if(x<a) {
#ifdef SECURE
    Warning(" call of InterpolateExactU with argument %lf < %lf!\n", x, a);
#endif
    return -100;
  }
  else if(x<Rpar12) {
    return A1HS12 + Log(Sin(kHS12*(x-a))/x);
  }
  else {
    return A2HS12 - A3HS12/(x*x) - A3HS12/((L-x)*(L-x));
  }
}

DOUBLE InterpolateExactFp12(DOUBLE x) {

#ifdef SECURE
  if(x<a)
    Warning("InterpolateExactFp : (r<a) call\n");
#endif

  if(x<Rpar12) {
    return kHS12 / Tg(kHS12*(x-a)) - 1./x;
  }
  else {
    return 2.*A3HS12*(1./(x*x*x)-1./((L-x)*(L-x)*(L-x)));
  }
}

DOUBLE InterpolateExactE12(DOUBLE x) {
  DOUBLE g;

#ifdef SECURE
  if(x<a) Warning("Interpolate E : (r<aAB) call\n");
#endif

  if(x<Rpar12) {
    g = kHS12/ Tg(kHS12*(x-a))-1./x;
    return EHS12 + g*g;
  }
  else {
   return (2.*A3HS12*(L*L*L*L - 4.*L*L*L*x + 6.*L*L*x*x - 2.*L*x*x*x + 2.*x*x*x*x))/((L-x)*(L-x)*(L-x)*(L-x)*x*x*x*x);
  }
}
#endif

/************************** Construct Grid HS11 *****************************/
DOUBLE A1HS11, A2HS11, A3HS11, kHS11, EHS11;
// k in units of 2pi/L
void ConstructGridHS11(struct Grid *G) {
  int size = 3;
  DOUBLE dx;
  DOUBLE x;

  G->min = aA;
  G->max = Lhalf;
  dx = (G->max - G->min) / (DOUBLE) (size-1);
  G->step = dx;
  G->I_step = (DOUBLE) size / (G->max - G->min);
  G->max2 = G->max*G->max;

  Message("  Constructing Hard sphere Jastrow term (TRIAL_HS11), r>aA\n");
  Message("  (2-body solution + symmetrized phonons) \n");
  Message("  parameters: 0<Rpar11<1 \n");
  kHS11 = PI/(Lhalf-aA); // maximal value of k in units of r_0
  kHS11 /= 2.*PI/L;
  Message("              0<Bpar11<1/(1-2aA/L) = %lf\n", kHS11);
  if(Rpar11<0 || Rpar11>1) Error("  Wrong value of Rpar11 = %lf\n", Rpar11);
  if(Bpar11<0 || Bpar11>kHS11) Error("  Wrong value of Bpar11 = %lf\n", Bpar11);
  Warning("  Automatically converting matching distance: Rpar11 = %"LF" [L/2] => Rpar11 = %"LF"[r_0]\n", Rpar11, Rpar11*Lhalf);
  Rpar11 *= Lhalf;
  Warning("  Automatically converting 2-body scattering momentum: Bpar11 = %lf [2pi/L] => Bpar11 = %lf [r_0]\n", Bpar11, Bpar11);
  Bpar11 *= 2.*PI/L;

  kHS11 = Bpar11;
  EHS11 = kHS11*kHS11;

  // f'(Rpar11)
  x = Rpar11;
  A3HS11 = (kHS11 / Tg(kHS11*(x-aA)) - 1./x)/(2.*(1./(x*x*x)-1./((L-x)*(L-x)*(L-x))));

  // f(L/2)
  x = Lhalf;
  A2HS11 = A3HS11/(x*x) + A3HS11/((L-x)*(L-x));

  // f(Rpar11)
  x = Rpar11;
  A1HS11 = - Log(Sin(kHS11*(x-aA))/x) + A2HS11 - A3HS11/(x*x) - A3HS11/((L-x)*(L-x));
}

/* executed only if TRIAL_HS11 is defined */
#ifdef TRIAL_HS11
DOUBLE InterpolateExactU11(DOUBLE x) {
  DOUBLE f;

  if(x<aA) {
#ifdef SECURE
    Warning(" call of InterpolateExactU with argument %lf < %lf!\n", x, aA);
#endif
    return -100;
  }
  else if(x<Rpar11) {
    return A1HS11 + Log(Sin(kHS11*(x-aA))/x);
  }
  else {
    return A2HS11 - A3HS11/(x*x) - A3HS11/((L-x)*(L-x));
  }
}

DOUBLE InterpolateExactFp11(DOUBLE x) {

#ifdef SECURE
  if(x<aA)
    Warning("InterpolateExactFp : (r<aA) call\n");
#endif

  if(x<Rpar11) {
    return kHS11 / Tg(kHS11*(x-aA)) - 1./x;
  }
  else {
    return 2.*A3HS11*(1./(x*x*x)-1./((L-x)*(L-x)*(L-x)));
  }
}

DOUBLE InterpolateExactE11(DOUBLE x) {
  DOUBLE g;

#ifdef SECURE
  if(x<aA) Warning("Interpolate E : (r<aA) call\n");
#endif

  if(x<Rpar11) {
    g = kHS11/ Tg(kHS11*(x-aA))-1./x;
    return EHS11 + g*g;
  }
  else {
   return (2.*A3HS11*(L*L*L*L - 4.*L*L*L*x + 6.*L*L*x*x - 2.*L*x*x*x + 2.*x*x*x*x))/((L-x)*(L-x)*(L-x)*(L-x)*x*x*x*x);
  }
}
#endif

/************************** Construct Grid HS Simple **********************/
void ConstructGridHSSimple(struct Grid *G) {
  DOUBLE x, xmin, xmax;
  DOUBLE y, ymin, ymax;
  DOUBLE V;
  DOUBLE precision = 1e-12;
  int search = ON;

  AllocateWFGrid(G, 3);
  // tg k(r-a) / rk = 1
  V = 1;

  xmin = 1e-5;
  xmax = 3.14/2./(Lhalf-a);

  ymin = tg(xmin*(Lhalf-a))/ (xmin*Lhalf) - V;
  ymax = tg(xmax*(Lhalf-a))/ (xmax*Lhalf) - V;

 if(ymin*ymax > 0) Error("Can't construct the grid : No solution found");

  while(search) {
    x = (xmin+xmax)/2.;
    y = tg(x*(Lhalf-a))/ (x*Lhalf) - V;

    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }

    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y = tg(x*(Lhalf-a))/ (x*Lhalf) - V;
      search = OFF;
    }
  }
  sqE = x;

  Message("Solution is k = %"LF", error %"LE"\n", sqE, y);

  sqE2 = sqE*sqE;
  A1SW = Lhalf/Sin(sqE*(Lhalf-a));
}

#ifdef TRIAL_HS_SIMPLE
DOUBLE InterpolateExactU12(DOUBLE x) {
  if(x<a) {
    return -100;
  }
  else {
    return Log(A1SW/x * Sin(sqE*(x-a)));
  }
}

DOUBLE InterpolateExactFp12(DOUBLE x) {
#ifdef SECURE
  if(x<a)
    Error("InterpolateExactFp : (r<a) call\n");
#endif
  if(x<a)
    return 0.;
  else
    return sqE / tg(sqE*(x-a)) - 1./x;
}

DOUBLE InterpolateExactE12(DOUBLE x) {
  DOUBLE g;

  if(x<a) {
#ifdef SECURE
    Warning("InterpolateExactE12 : (r<a) call\n");
#endif
    return 0.;
  }
  else {
    g = sqE/ tg(sqE*(x-a))-1./x;
    return sqE2 + g*g;
  }
}
#endif

/************************** Construct Grid HS Simple11 **********************/
void ConstructGridHSSimple11(struct Grid *G) {
  DOUBLE x, xmin, xmax;
  DOUBLE y, ymin, ymax;
  DOUBLE V;
  DOUBLE precision = 1e-12;
  int search = ON;

  AllocateWFGrid(G, 3);
  // tg k(r-a) / rk = 1
  V = 1;

  xmin = 1e-15;
  xmax = 3.14/2./(Lhalf-b);

  ymin = tg(xmin*(Lhalf-b))/ (xmin*Lhalf) - V;
  ymax = tg(xmax*(Lhalf-b))/ (xmax*Lhalf) - V;

 if(ymin*ymax > 0) Error("Can't construct the grid : No solution found");

  while(search) {
    x = (xmin+xmax)/2.;
    y = tg(x*(Lhalf-b))/ (x*Lhalf) - V;

    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }

    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y = tg(x*(Lhalf-b))/ (x*Lhalf) - V;
      search = OFF;
    }
  }
  sqE11 = x;

  Message("Solution is k = %"LF", error %"LE"\n", sqE11, y);

  sqE211 = sqE11*sqE11;
  A1SW11 = Lhalf/Sin(sqE11*(Lhalf-b));
}

#ifdef TRIAL_HS_SIMPLE11
DOUBLE InterpolateExactU11(DOUBLE x) {
  if(x<b) {
    return -100;
  }
  else {
    return Log(A1SW11/x * Sin(sqE11*(x-b)));
  }
}

DOUBLE InterpolateExactFp11(DOUBLE x) {
  if(x<b)
    return 0.;
  else
    return sqE11 / tg(sqE11*(x-b)) - 1./x;
}

DOUBLE InterpolateExactE11(DOUBLE x) {
  DOUBLE g;

  if(x<b) {
#ifdef SECURE
    Warning("InterpolateExactE11 : (r<a) call\n");
#endif
    return 0.;
  }
  else {
    g = sqE11/ tg(sqE11*(x-b))-1./x;
    return sqE211 + g*g;
  }
}

DOUBLE InterpolateExactU22(DOUBLE x) {
  if(x<b) {
    return -100;
  }
  else {
    return Log(A1SW11/x * Sin(sqE11*(x-b)));
  }
}

DOUBLE InterpolateExactFp22(DOUBLE x) {
  if(x<b)
    return 0.;
  else
    return sqE11 / tg(sqE11*(x-b)) - 1./x;
}

DOUBLE InterpolateExactE22(DOUBLE x) {
  DOUBLE g;

  if(x<b) {
#ifdef SECURE
    Warning("InterpolateExactE22 : (r<a) call\n");
#endif
    return 0.;
  }
  else {
    g = sqE11/ tg(sqE11*(x-b))-1./x;
    return sqE211 + g*g;
  }
}
#endif

/************************** Construct Grid SS ****************************
R is the size of a soft sphere
Rpar is the matching distance */
void ConstructGridSS(struct Grid *G, const DOUBLE a, const DOUBLE R,
   const DOUBLE Rpar, const DOUBLE Bpar, const int size, const DOUBLE Rmin, const DOUBLE Rmax) {

  DOUBLE x, xmin, xmax;
  DOUBLE y, ymin, ymax;
  DOUBLE V;
  DOUBLE precision = 1e-12;
  DOUBLE k, kappa, K, K2;
  DOUBLE delta, delta_bar;
  int search = ON;
  int repeat = ON;

  if(R>Rpar) Error("Size of the soft sphere %"LE" is larger than the matching distance %"LE"\n", R, Rpar);
  if(R<a) Error("Size of the soft sphere %"LE" is smaller than the scattering length %"LE"\n", R, a);
  if(Rpar<a) Error("Matching distance %"LE" is smaller than the scattering length %"LE"\n", Rpar, a);

  AllocateWFGrid(G, size);

  // define kappa
  // Solve th x / x = 1 - a/R, E<V_0
  //       tg x / x = 1 - a/R, E>V_0
  //       x = kappa R
  V = 1. - a/R;
  xmin = 1e-5;
  xmax = 100;
  ymin = tanh(xmin)/xmin - V;
  ymax = tanh(xmax)/xmax - V;
  if(ymin*ymax > 0) Error("R = %"LF" Can't construct the grid : No solution found", R);
  while(search) {
    x = (xmin+xmax)/2.;
    y = tanh(x)/x - V;
    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }
    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y = tanh(x)/x - V;
      search = OFF;
    }
  }

  kappa = x / R;
  Message("  Solution is kappa = %"LF", error %"LE"\n", kappa, y);

  search = ON;
  // initialize delta_bar 
  delta_bar = -a/Rpar;

  // loop to determine delta
  while(repeat) {
    // Solve 1 + delta_bar = 1/x * arctg(x(y-2)/(x^2+y-2))
    V = 1. + delta_bar;
    xmin = 1e-5;
    xmax = 100;
    ymin = arctg(xmin*(Bpar-2.)/(xmin*xmin+Bpar-2.))/xmin - V;
    ymax = arctg(xmax*(Bpar-2.)/(xmax*xmax+Bpar-2.))/xmax - V;
    if(ymin*ymax > 0) Error("Can't construct the grid : No solution found (2)");
    while(search) {
      x = (xmin+xmax)/2.;
      y = arctg(x*(Bpar-2.)/(x*x+Bpar-2.))/x - V;
      if(y*ymin < 0) {
        xmax = x;
        ymax = y;
      } else {
        xmin = x;
        ymin = y;
      }
      if(fabs(y)<precision) {
        x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
        y = arctg(x*(Bpar-2.)/(x*x+Bpar-2.))/x - V;
        search = OFF;
      }
    }
    // determine momenta
    k = x / Rpar;
    K2 = k*k - kappa*kappa;
    K = sqrt(fabs(K2));

    if(K2>0) high_energy = ON;
    else high_energy = OFF;

    // extract value of delta from momenta (scattering problem)
    if(high_energy) {
      // delta = arctg(k/K tg(KR))-kR
      delta = arctg(k/K*tg(K*R))-k*R;
      //Message("  low energy case, delta=%"LE"\n", delta);
    }
    else {
      // delta = arctg(k/K th(KR))-kR
      delta = arctg(k/K*tanh(K*R))-k*R;
      //Message("  large energy case, delta=%"LE"\n", delta);
    }

    // check if delta has converged 
    if(fabs((delta_bar-delta/x)/delta_bar)<precision) {
      repeat = OFF;
    }
    else {
      delta_bar = 0.5*(delta_bar + delta/x);
    }
  }
  Message("  delta=%"LF" kR=%"LF"\n", delta, k*R);

  A3SW = Rpar / Sin(x+delta) * Bpar*(Bpar-2.) / (x*x+Bpar*Bpar-2.*Bpar);
  A4SW = Exp(Bpar) * x*x / (x*x+Bpar*Bpar-2*Bpar);
  alpha_1 = Bpar / Rpar;
  if(high_energy) {
    A1SW = A3SW * Sin(k*R+delta) / Sin(k*R);
    Message("  High energy\n");
  }
  else {
    A1SW = A3SW * Sin(k*R+delta) / sinh(K*R);
    Message("  Low energy\n");
  }

  G->min = Rmin;
  G->max = Rmax;
  G->step = (G->max - G->min) / (DOUBLE) (size-1);
  G->I_step = (DOUBLE) size / (G->max - G->min);
  G->max2 = G->max*G->max;

  sqE = k;
  sqE2 = sqE*sqE;
  sqEkappa = K;
  sqEkappa2 = K2;
  trial_delta = delta;

  Warning("  A = %"LF" B = %"LF" C = %"LF"\n", A1SW, A3SW, A4SW);
}

/************************** Construct Grid 1D ******************************/
void ConstructGridHS1D(struct Grid *G, const DOUBLE R, const DOUBLE Bpar,
  const DOUBLE a, const int size, const DOUBLE Rmin, const DOUBLE Rmax) {
  DOUBLE x;
  DOUBLE xmin, xmax;
  DOUBLE y, ymin, ymax;
  DOUBLE precision = 1e-10;
  int search = ON;
  int i;
  DOUBLE dx;
  DOUBLE alpha;

  if(a >= R) Error("The size of the hard sphere is larger than the matching distance\n");

  AllocateWFGrid(G, size);
  i = (int) G->size;

  alpha = R/Bpar;
  alpha_1 = Bpar/R;

  // x = sqrt(E)
  // x tg x(R-a) = 1/alpha

  xmin = 1e-20;
  xmax = (3.14159265358979-1e-200)/(2.*(R-a));
  ymin = xmin * tg(xmin*(R-a)) - alpha_1;
  ymax = xmax * tg(xmax*(R-a)) - alpha_1;

  if(ymin*ymax > 0) Error("Can't construct the grid : No solution found");

  while(search) {
    x = (xmin+xmax)/2.;
    y = x * tg(x*(R-a)) - alpha_1;

    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }

    if(fabs(y/alpha_1)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y = x * tg(x*(R-a)) - alpha_1;
      search = OFF;
    }
  }

  sqE = x;
  sqE2 = sqE*sqE;
  alpha2E = alpha*alpha*sqE2;

  G->min = Rmin;
  G->max = Rmax;
  dx = (G->max - G->min) / (DOUBLE) (size-1);
  G->step = dx;
  G->I_step = (DOUBLE) size / (G->max - G->min);
  G->max2 = G->max*G->max;

  A1SW = Sin(sqE*(R-a));
  A3SW = alpha2E / (alpha2E+1) * Exp(R*alpha_1);
  if(A3SW > 1e300)  Error("Divergence in A3SW");
}

/************************** Construct Grid Dark Soliton 12 *****************/
void ConstructGridDarkSoltion12(struct Grid *G) {
  int i;
  DOUBLE dx;
  int size = 3;
  DOUBLE crd;
  DOUBLE xi, g, mu;
  // hbar^2/(2 m xi^2) = mu
  g = -2. / aA;
  mu = fabs(g*n);
  xi = 1. / sqrt(2. * mu);

  Message("  TRIAL_DARK_SOLITON12 (1D) th(Apar12 crd(x,L)), Apar12 in units of xi where xi is the healing length\n");
  Message("  GP:  mu = %lf, xi = %lf\n", mu, xi);
  Message("  GP dark soliton: th(x/(Apar12*sqrt(2)*xi))\n  var. par, Apar12 = %lf -> ", Apar12);
  Apar12 = 1. / (Apar12*sqrt(2.)*xi);
  Message("%lf (in units of healing length)\n", Apar12);
   
  AllocateWFGrid(G, size);
  i = (int) G->size;

  G->min = 0;
  G->max = Lhalf;
  dx = (G->max - G->min) / (DOUBLE) (size-1);
  G->step = dx;
  G->I_step = (DOUBLE) size / (G->max - G->min);
  G->max2 = G->max*G->max;


  crd = L / PI * sin(PI*Lhalf / L); // chord length
  Bpar12 = -Log(fabs(tanh(Apar12*crd)));
}

#ifdef TRIAL_DARK_SOLITON12
DOUBLE InterpolateExactU12(DOUBLE x) {
  DOUBLE crd;

  crd = L/PI*sin(PI*x/L); // chord length
  return Bpar12 + Log(fabs(tanh(Apar12*crd)));
}

DOUBLE InterpolateExactFp12(DOUBLE x) {
  DOUBLE crd, crdp;
  crd = L / PI * sin(PI*x / L); // chord length
  crdp = cos(PI*x / L); // d crd(x) / dx

  return 2.*Apar12*crdp / sinh(2.*Apar12*crd);
}

/* Eloc1D = -[f"/f - (f'/f)^2]*/
DOUBLE InterpolateExactE12(DOUBLE x) {
  DOUBLE crd, crdp, crdpp;
  crd = L / PI * sin(PI*x / L); // chord length
  crdp = cos(PI*x / L); // d crd(x) / dx
  crdpp = -(PI/L)*sin(PI*x / L); // d^2 crd(x) / dx^2

  return 2.*Apar12 / sinh(2.*Apar12*crd) * (2.*Apar12/tanh(2.*Apar12*crd)*crdp*crdp - crdpp);
}
#endif

/************************** Construct Grid 1D ******************************/
#ifdef TRIAL_TONKS12
DOUBLE sqEAB, sqEAB2;
#endif

void ConstructGridTonks12(struct Grid *G, const int size, const DOUBLE Rmin, const DOUBLE Rmax) {
  int i;
  DOUBLE dx;

  AllocateWFGrid(G, size);
  i = (int) G->size;

  G->min = Rmin;
  G->max = Rmax;
  dx = (G->max - G->min) / (DOUBLE) (size-1);
  G->step = dx;
  G->I_step = (DOUBLE) size / (G->max - G->min);
  G->max2 = G->max*G->max;

#ifdef TRIAL_TONKS12
  sqEAB = 0.5*PI/(Lhalf-a);
  sqEAB2 = sqEAB*sqEAB;
#endif
}

/* executed only if TRIAL_TONKS is defined */
#ifdef TRIAL_TONKS12
DOUBLE InterpolateExactU12(DOUBLE x) {
#ifdef HARD_SPHERE12
  if(x<a) return -100;
#endif
  return Log(fabs(Sin(sqEAB*(x-a))));
}

DOUBLE InterpolateExactFp12(DOUBLE x) {
#ifdef HARD_SPHERE12
  if(x<a) return 100;
#endif
  return sqEAB / tg(sqEAB*(x-a));
}

/* Eloc1D = -[f"/f - (f'/f)^2]*/
DOUBLE InterpolateExactE12(DOUBLE x) {
  DOUBLE g;

#ifdef HARD_SPHERE12
  if(x<a) return 0;
#endif

  g = sqEAB/tg(sqEAB*(x-a));
  //g = 0.; //Real local energy
  return sqEAB2 + g*g;
}
#endif

#ifdef TRIAL_TONKS11
  DOUBLE sqEA, sqEA2;
#endif

void ConstructGridTonks11(struct Grid *G, const int size, const DOUBLE Rmin, const DOUBLE Rmax) {
  int i;
  DOUBLE dx;

  AllocateWFGrid(G, size);
  i = (int) G->size;

  G->min = Rmin;
  G->max = Rmax;
  dx = (G->max - G->min) / (DOUBLE) (size-1);
  G->step = dx;
  G->I_step = (DOUBLE) size / (G->max - G->min);
  G->max2 = G->max*G->max;

#ifdef TRIAL_TONKS11
  sqEA = 0.5*PI/(Lhalf-aA);
  sqEA2 = sqEA*sqEA;
#endif
}

#ifdef TRIAL_TONKS11
DOUBLE InterpolateExactU11(DOUBLE x) {
#ifdef HARD_SPHERE11
  if(x<aA) return -100;
#endif
  return Log(fabs(Sin(sqEA*(x-aA))));
}

DOUBLE InterpolateExactFp11(DOUBLE x) {
  return sqEA / tg(sqEA*(x-aA));
}

DOUBLE InterpolateExactE11(DOUBLE x) {
  DOUBLE g;

  g = sqEA/tg(sqEA*(x-aA));
  return sqEA2 + g*g;
}
/*#ifdef TRIAL_1D
  return sqE2 / (Sin(sqE*x)*Sin(sqE*x));
  //return sqE2 + fp*fp;
#endif
#ifdef TRIAL_2D
  fp = sqE/tg(sqE*x);
//  return sqE2 + fp*fp-fp/x;
  return sqE2 / (Sin(sqE*x)*Sin(sqE*x))-fp/x;
#endif
#ifdef TRIAL_3D
  fp = sqE/tg(sqE*x);
//  return sqE2 + fp*fp-2.*fp/x;
  return sqE2 / (Sin(sqE*x)*Sin(sqE*x))-2.*fp/x;
#endif*/
#endif

#ifdef TRIAL_TONKS22
  DOUBLE sqEB, sqEB2;
#endif

void ConstructGridTonks22(struct Grid *G, const int size, const DOUBLE Rmin, const DOUBLE Rmax) {
  int i;
  DOUBLE dx;

  AllocateWFGrid(G, size);
  i = (int) G->size;

  G->min = Rmin;
  G->max = Rmax;
  dx = (G->max - G->min) / (DOUBLE) (size-1);
  G->step = dx;
  G->I_step = (DOUBLE) size / (G->max - G->min);
  G->max2 = G->max*G->max;

#ifdef TRIAL_TONKS22
  sqEB = 0.5*PI/(Lhalf-aB);
  sqEB2 = sqEB*sqEB;
#endif
}

#ifdef TRIAL_TONKS22
DOUBLE InterpolateExactU22(DOUBLE x) {
#ifdef HARD_SPHERE22
  if(x<aB) return -100;
#endif
  return Log(fabs(Sin(sqEB*(x-aB))));
}

DOUBLE InterpolateExactFp22(DOUBLE x) {
  return sqEB / tg(sqEB*(x-aB));
}

DOUBLE InterpolateExactE22(DOUBLE x) {
  DOUBLE g;

  g = sqEB/tg(sqEB*(x-aB));
  return sqEB2 + g*g;
}
#endif

/************************** Construct Grid Lieb ****************************/
void ConstructGridLieb(struct Grid *G, const DOUBLE R, const DOUBLE Bpar, const int size, const DOUBLE Rmin, const DOUBLE Rmax) {
  DOUBLE x;
  DOUBLE xmin, xmax;
  DOUBLE y, ymin, ymax;
  DOUBLE precision = 1e-10;
  int search = ON;
  int i;
  DOUBLE dx;
  DOUBLE alpha;

  AllocateWFGrid(G, size);
  i = (int) G->size;

  Warning("Rpar -> Rpar [L/2], %lf -> %lf\n", Rpar, Rpar*Lhalf);
  Rpar *= Lhalf;

#ifdef TRIAL_LIEB_RENORMALIZED
  Warning("Using renormalized value of the 1D scat. length %"LF" -> %"LF"\n  ", a, -1.4603545088095877/sqrt(2)+1./a);
  a = -1.4603545088095877/sqrt(2)+1./a;
#endif

if(a<0) {
  Warning("Negative scattering length ! %"LG" -> %"LG"\n  ", a, -a);
  a = -a;
  Warning("Wavefunction will have node at distance |a|\n");

  a2 = a*a;

  // x tg xR = -1/a1D 
  alpha = -1./a;
  xmin = (PI+1e-10)/(2.*R);
  xmax = (2*PI-1e-10)/(2.*R);
  ymin = xmin * tg(xmin*R) - alpha;
  ymax = xmax * tg(xmax*R) - alpha;
} 
else {
#ifdef TRIAL_LIEB_RENORMALIZED
  // x tg xR = -1/a1D
  alpha = 1./a;
#else
  // x tg xR = a3D
  alpha = a;
#endif
  xmin = 1e-20;
  xmax = (PI-1e-10)/(2.*R);
  ymin = xmin * tg(xmin*R) - alpha;
  ymax = xmax * tg(xmax*R) - alpha;
}

  if(ymin*ymax > 0) Error("Can't construct the grid : No solution found");

  while(search) {
    x = (xmin+xmax)/2.;
    y = x * tg(x*R) - alpha;

    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }

    if(fabs(y/alpha)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y = x * tg(x*R) - alpha;
      search = OFF;
    }
  }

  sqE = x;
  Message("  k = %"LG"\n", sqE);
  sqE2 = sqE*sqE;

  G->min = 0;
  G->max = 1e10;
  dx = (G->max - G->min) / (DOUBLE) (size-1);
  G->step = dx;
  G->I_step = (DOUBLE) size / (G->max - G->min);
  G->max2 = G->max*G->max;
}

/* executed only if TRIAL_LIEB is defined */
#ifdef TRIAL_LIEB
DOUBLE InterpolateExactU11(DOUBLE x) {
#ifdef HARD_SPHERE
  if(x<a) return -100;
#endif
  if(x<Rpar) 
    return Log(fabs(Cos(sqE*(x-Rpar))));
  else 
    return 0;
}

DOUBLE InterpolateExactFp11(DOUBLE x) {
  if(x<Rpar)
    return -sqE * tg(sqE*(x-Rpar));
  else 
    return 0;
}

DOUBLE InterpolateExactE11(DOUBLE x) {
  DOUBLE g;

  if(x<Rpar) {
    g = tg(sqE*(x-Rpar));
    return sqE2*(1+g*g);
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactU22(DOUBLE x) {
#ifdef HARD_SPHERE
  if(x<a) return -100;
#endif
  if(x<Rpar)
    return Log(fabs(Cos(sqE*(x-Rpar))));
  else 
    return 0;
}

DOUBLE InterpolateExactFp22(DOUBLE x) {
  if(x<Rpar)
    return -sqE * tg(sqE*(x-Rpar));
  else 
    return 0;
}

DOUBLE InterpolateExactE22(DOUBLE x) {
  DOUBLE g;

  if(x<Rpar) {
    g = tg(sqE*(x-Rpar));
    return sqE2*(1+g*g);
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactU12(DOUBLE x) {
#ifdef HARD_SPHERE
  if(x<a) return -100;
#endif
  if(x<Rpar) 
    return Log(fabs(Cos(sqE*(x-Rpar))));
  else 
    return 0;
}

DOUBLE InterpolateExactFp12(DOUBLE x) {
  if(x<Rpar)
    return -sqE * tg(sqE*(x-Rpar));
  else
    return 0;
}

DOUBLE InterpolateExactE12(DOUBLE x) {
  DOUBLE g;

  if(x<Rpar) {
    g = tg(sqE*(x-Rpar));
    return sqE2*(1+g*g);
  }
  else {
    return 0;
  }
}
#endif

/************************** Construct Grid Phonons *************************/
void ConstructGridLiebPhonons11(struct Grid *G, const DOUBLE a, const DOUBLE R) {
  DOUBLE k, kmin, kmax;
  DOUBLE y, ymin, ymax;
  DOUBLE precision = 1e-12;
  int search = ON;
  DOUBLE C;

  Message("Jastrow11: GridLiebPhonons11\n");
  if(R>Lhalf) Error("Rpar>L/2\n");

  AllocateWFGrid(G, 3);
  C = -L/(2*PI)*Sin(2*PI*R/L);

  k = kmin = 1e-3;
  ymin = (a*k*Cos(k*R)+Sin(k*R))*(a*k*Sin(k*R)-Cos(k*R))/(k*(a*a*k*k+1));
  while(search) {
    k = kmax = kmin + 0.1*kmin;
    y = (a*k*Cos(k*R)+Sin(k*R))*(a*k*Sin(k*R)-Cos(k*R))/(k*(a*a*k*k+1));

    if((ymin-C)*(y-C)<0) {
      search = OFF;
    }
    else {
      kmin = k;
      ymin = y;
    }
  }

  search = ON;

  while(search) {
    k = (kmin+kmax)/2.;
    y = (a*k*Cos(k*R)+Sin(k*R))*(a*k*Sin(k*R)-Cos(k*R))/(k*(a*a*k*k+1));

    if((y-C)*(ymin-C) < 0) {
      kmax = k;
      ymax = y;
    } else {
      kmin = k;
      ymin = y;
    }

    if(fabs((y-C)/C)<precision) {
      k = (kmax*(ymin-C)-kmin*(ymax-C)) / (ymin-ymax);
      y = (a*k*Cos(k*R)+Sin(k*R))*(a*k*Sin(k*R)-Cos(k*R))/(k*(a*a*k*k+1));
      search = OFF;
    }
  }

  A3SW = arctg(1/(k*a))/k;
  alpha_1 = 1 + tg(PI*R/L)*(k*L/PI/tg(k*(R-A3SW))+tg(PI*R/L));
  A1SW = Sin(PI*R/L);
  A1SW = pow(Sin(PI*R/L), alpha_1) / Cos(k*(R-A3SW));

  Message("    k = %"LE", error %"LE"\n", k, fabs((y-C)/C));
  Message("    A = %"LE"\n", A1SW);
  Message("    B = %"LE"\n", A3SW);
  Message("    alpha = %"LE"\n\n", alpha_1);

  sqE = k;
  sqE2 = sqE*sqE;

  G->min = 0;
  G->max = L/2;
  G->max2 = G->max*G->max;
}

/* executed only if TRIAL_LIEB_PHONON is defined */
#ifdef TRIAL_LIEB_PHONON11
DOUBLE InterpolateExactU11(DOUBLE x) {
  if(x<Rpar11)
    return Log(fabs(A1SW*Cos(sqE*(x-A3SW))));
  else
    //return Log(pow(Sin(PI*x/L), alpha_1));
    return alpha_1*Log(Sin(PI*x/L));
}

DOUBLE InterpolateExactFp11(DOUBLE x) {
#ifdef HARD_SPHERE12
#ifdef SECURE
      if(x<a) Error("InterpolateExactFp : (r<a) call\n");
#endif
#endif

  if(x<Rpar11)
    return -sqE * tg(sqE*(x-A3SW));
  else 
    return alpha_1*PI/(L*tg(PI*x/L));
}

DOUBLE InterpolateExactE11(DOUBLE x) {
  DOUBLE c;

  if(x<Rpar11) {
    c = tg(sqE*(x-A3SW));
    return sqE2*(1.+c*c);
  }
  else {
    c = 1. / tg(PI*x/L);
    return alpha_1*(PI*PI/(L*L))*(1.+c*c);
  }
}

/*DOUBLE InterpolateExactU22(DOUBLE x) {
  if(x<Rpar11)
    return Log(A1SW*Cos(sqE*(x-A3SW)));
  else
    return Log(pow(Sin(PI*x/L), alpha_1));
}

DOUBLE InterpolateExactFp22(DOUBLE x) {
#ifdef HARD_SPHERE12
#ifdef SECURE
      if(x<a) Error("InterpolateExactFp : (r<a) call\n");
#endif
#endif

  if(x<Rpar11)
    return -sqE * tg(sqE*(x-A3SW));
  else
    return alpha_1*PI/(L*tg(PI*x/L));
}

DOUBLE InterpolateExactE22(DOUBLE x) {
  DOUBLE c;

  if(x<Rpar11) {
    c = tg(sqE*(x-A3SW));
    return sqE2*(1.+c*c);
  }
  else {
    c = 1 / tg(PI*x/L);
    return alpha_1*(PI*PI/(L*L))*(1+c*c);
  }
}*/
#endif

/************************** Construct Grid Phonon Luttinger *************************/
void ConstructGridPhononLuttinger11(struct Grid *G) {
  DOUBLE k, kmin, kmax;
  DOUBLE y, ymin, ymax;
  DOUBLE precision = 1e-12;
  int search = ON;
  DOUBLE C;
  DOUBLE R;
  DOUBLE a;
  int iter = 1;

  a = aA;

  if(Rpar11<0) Error("Negative Rpar11 = %lf, adjust to Rpar11>0\n", Rpar11);
  if(Rpar11>Lhalf) Error("Rpar11 = %lf, adjust to 0<Rpar11<L/2=%lf\n", Rpar11, Lhalf);

  Message("\n  Jastrow term: ConstructGridPhononLuttinger11 trial wavefunction\n");
  Message("  Rpar11=%"LG"  is used as it is, without rescaling\n", Rpar11);
  R = Rpar11;

  if(a>0) {
    Warning(" assuming repulsive case, with aA = %lf < 0\n", -a);
    a = -a;
  }

  AllocateWFGrid(G, 3);

  Message("  Kpar11= %"LE " \n", Kpar11);
  alpha_1 = 1./Kpar11;

  C = -PI*alpha_1/L/tan(PI*R/L);
  k = kmin = 1e-8;

  Message("  Fixing kR (1)... ");
  ymin = k*tan(k*R + atan(1./(k*a))) - C;
  while(search) {
    k = kmax = kmin + 0.01*kmin;
    y = k*tan(k*R + atan(1./(k*a))) - C;
    if(ymin*y<0) {
      ymax = y;
      search = OFF;
    }
    else {
      kmin = k;
      ymin = y;
    }
  }
  search = ON;
  Message("done\n  Fixing kR (2)... ");

  // Solve k*tan(k*R + atan(1./(k*a))) = -PI*alpha_1/L/tan(PI*R/L);
  // i.e. f'(R)/f(R)
  while(search) {
    k = (kmin+kmax)/2.;
    y = k*tan(k*R + atan(1./(k*a))) - C;

    if(y*ymin < 0) {
      kmax = k;
      ymax = y;
    } else {
      kmin = k;
      ymin = y;
    }

    if(fabs(y)<precision) {
      k = (kmax*ymin-kmin*ymax) / (ymin-ymax);
      y = k*tan(k*R + atan(1./(k*a)));
      search = OFF;
    }

    if(iter++ == 1000) {
      Warning("  try Rpar closer to L/2");
    }
  }
  Message("done\n");

  Btrial = - Arctg(1/(k*a))/k;
  Atrial = Sin(PI*R/L);
  Atrial = pow(Sin(PI*R/L), alpha_1) / Cos(k*(R-Btrial));

  Message("    k = %"LE " , error %"LE " \n", k, fabs((y-C)/C));
  Message("    A = %"LE " \n", Atrial);
  Message("    B = %"LE " \n", Btrial);
  Message("    Equivalent Luttiner parameter K = 1/alpha = %"LE " \n", 1./alpha_1);

  sqE = k;
  sqE2 = sqE*sqE;

  G->min = 0;
  G->max = L/2;
  G->max2 = G->max*G->max;
}

/* executed only if TRIAL_PHONON is defined */
#ifdef TRIAL_PHONON_LUTTINGER11
// ideal bosons K -> \infty, alpha_1 = 1/K = 0
// ideal fermions K = 1, alpha_1 = 1/K = 1
DOUBLE InterpolateExactU11(DOUBLE x) {
  if(x<Rpar11) 
    return Log(fabs(Atrial*Cos(sqE*(x-Btrial))));
  else 
    return Log(pow(Sin(PI*x/L), alpha_1));
}

DOUBLE InterpolateExactFp11(DOUBLE x) {
#ifdef HARD_SPHERE
#ifdef SECURE
      if(x<a) Error("InterpolateExactFp : (r<a) call\n");
#endif
#endif
  if(x<Rpar11)
    return -sqE * Tg(sqE*(x-Btrial));
  else
    return alpha_1*PI/(L*Tg(PI*x/L));
}

/* executed only if TRIAL_PHONON is defined */
DOUBLE InterpolateExactE11(DOUBLE x) {
  DOUBLE c;

  if(x<Rpar11) {
    c = Tg(sqE*(x-Btrial));
    return sqE2*(1.+c*c);
    //return sqE2; // Local energy
  }
  else {
    c = 1. / Tg(PI*x/Lwf);
    return alpha_1*(PI*PI/(L*L))*(1.+c*c);
    //return -alpha_1*(PI*PI/(L*L))*((alpha_1-1)*c*c-1); // Local energy
  }
}
#endif

/************************** Construct Grid Phonon Luttinger *************************/
DOUBLE Dtrial12, Etrial12;
void ConstructGridPhononLuttinger12(struct Grid *G) {
  DOUBLE k, kmin, kmax;
  DOUBLE y, ymin, ymax;
  DOUBLE precision = 1e-12;
  int search = ON;
  DOUBLE C;
  DOUBLE R;
  int iter = 1;

  //a = aAB;
  if(Rpar12<0) Error("Negative Rpar12 = %lf, adjust to Rpar>0\n", Rpar12);
  if(Rpar12>Lhalf) Error("Rpar12 = %lf, adjust to 0<Rpar12<L/2=%lf\n", Rpar12, Lhalf);

  Message("\n  Jastrow term: ConstructGridPhononLuttinger12 trial wavefunction\n");
  Message("  Rpar12=%"LG"  is used as it is, without rescaling\n", Rpar12);
  R = Rpar12;

  if(a>0) {
    //Warning(" assuming repulsive case, with a = %lf < 0\n", -a);
    //a = -a;
    Warning(" assuming sTG case, with a node at a = %lf > 0\n", a);
  }

  AllocateWFGrid(G, 3);

  Message("  Kpar12= %"LE " \n", Kpar12);
  Etrial12 = 1./Kpar12; //   alpha_1 = 1./Kpar11;

  C = -PI*Etrial12/L/tan(PI*R/L);
  k = kmin = 1e-8;

  Message("  Fixing kR (1)... ");
  ymin = k*tan(k*R + atan(1./(k*a))) - C;
  while(search) {
    k = kmax = kmin + 0.01*kmin;
    y = k*tan(k*R + atan(1./(k*a))) - C;
    if(ymin*y<0) {
      ymax = y;
      search = OFF;
    }
    else {
      kmin = k;
      ymin = y;
    }
  }
  search = ON;
  Message("done\n  Fixing kR (2)... ");

  // Solve k*tan(k*R + atan(1./(k*a))) = -PI*Etrial12/L/tan(PI*R/L);
  // i.e. f'(R)/f(R)
  while(search) {
    k = (kmin+kmax)/2.;
    y = k*tan(k*R + atan(1./(k*a))) - C;

    if(y*ymin < 0) {
      kmax = k;
      ymax = y;
    } else {
      kmin = k;
      ymin = y;
    }

    if(fabs(y)<precision) {
      k = (kmax*ymin-kmin*ymax) / (ymin-ymax);
      y = k*tan(k*R + atan(1./(k*a)));
      search = OFF;
    }

    if(iter++ == 1000) {
      Warning("  try Rpar closer to L/2");
    }
  }
  Message("done\n");

  Btrial12 = - Arctg(1./(k*a))/k;
  Atrial12 = Sin(PI*R/L);
  Atrial12 = pow(Sin(PI*R/L), Etrial12) / Cos(k*(R-Btrial12));

  Message("    k = %"LE " , error %"LE " \n", k, fabs((y-C)/C));
  Message("    A = %"LE " \n", Atrial12);
  Message("    B = %"LE " \n", Btrial12);
  Message("    Equivalent Luttiner parameter K = 1/alpha = %"LE " \n", 1./Etrial12);

  Ctrial12 = k;
  Dtrial12 = Ctrial12*Ctrial12;
  
  G->min = 0;
  G->max = L/2;
  G->max2 = G->max*G->max;
}

/* executed only if TRIAL_PHONON is defined */
#ifdef TRIAL_PHONON_LUTTINGER12
// ideal bosons K -> \infty, Etrial12 = 1/K = 0
// ideal fermions K = 1, Etrial12 = 1/K = 1
DOUBLE InterpolateExactU12(DOUBLE x) {
  if(x<Rpar12)
    return Log(fabs(Atrial12*Cos(Ctrial12*(x-Btrial12))));
  else 
    return Log(pow(Sin(PI*x/L), Etrial12));
}

DOUBLE InterpolateExactFp12(DOUBLE x) {
#ifdef HARD_SPHERE
#ifdef SECURE
      if(x<a) Error("InterpolateExactFp : (r<a) call\n");
#endif
#endif
  if(x<Rpar12)
    return -Ctrial12 * Tg(Ctrial12*(x-Btrial12));
  else
    return Etrial12*PI/(L*Tg(PI*x/L));
}

/* executed only if TRIAL_PHONON is defined */
DOUBLE InterpolateExactE12(DOUBLE x) {
  DOUBLE c;

  if(x<Rpar12) {
    c = Tg(Ctrial12*(x-Btrial12));
    return Dtrial12*(1.+c*c);
    //return Dtrial12; // Local energy
  }
  else {
    c = 1. / Tg(PI*x/L);
    return Etrial12*(PI*PI/(L*L))*(1.+c*c);
    //return -Etrial12*(PI*PI/(L*L))*((Etrial12-1)*c*c-1); // Local energy
  }
}
#endif

DOUBLE alpha12_1, A1SW12, A3SW12, sqE12, sqE212;
void ConstructGridLiebPhonons12(struct Grid *G, const DOUBLE a, const DOUBLE R) {
  DOUBLE k, kmin, kmax;
  DOUBLE y, ymin, ymax;
  DOUBLE precision = 1e-12;
  int search = ON;
  DOUBLE C;

  //a = fabs(a1D); // only for repulsion

  Message("  Jastrow12: GridLiebPhonons12\n");
  if(R>Lhalf) Error("Rpar>L/2\n");

  AllocateWFGrid(G, 3);
  C = -L/(2*PI)*Sin(2*PI*R/L);

  k = kmin = 1e-3;
  ymin = (a*k*Cos(k*R)+Sin(k*R))*(a*k*Sin(k*R)-Cos(k*R))/(k*(a*a*k*k+1));
  while(search) {
    k = kmax = kmin + 0.1*kmin;
    y = (a*k*Cos(k*R)+Sin(k*R))*(a*k*Sin(k*R)-Cos(k*R))/(k*(a*a*k*k+1));

    if((ymin-C)*(y-C)<0) {
      search = OFF;
    }
    else {
      kmin = k;
      ymin = y;
    }
  }

  search = ON;

  while(search) {
    k = (kmin+kmax)/2.;
    y = (a*k*Cos(k*R)+Sin(k*R))*(a*k*Sin(k*R)-Cos(k*R))/(k*(a*a*k*k+1));

    if((y-C)*(ymin-C) < 0) {
      kmax = k;
      ymax = y;
    } else {
      kmin = k;
      ymin = y;
    }

    if(fabs((y-C)/C)<precision) {
      k = (kmax*(ymin-C)-kmin*(ymax-C)) / (ymin-ymax);
      y = (a*k*Cos(k*R)+Sin(k*R))*(a*k*Sin(k*R)-Cos(k*R))/(k*(a*a*k*k+1));
      search = OFF;
    }
  }

  A3SW12 = arctg(1/(k*a))/k;
  alpha12_1 = 1 + tg(PI*R/L)*(k*L/PI/tg(k*(R-A3SW12))+tg(PI*R/L));
  A1SW12 = Sin(PI*R/L);
  A1SW12 = pow(Sin(PI*R/L), alpha12_1) / Cos(k*(R-A3SW12));

  Message("  k = %"LE", error %"LE"\n", k, fabs((y-C)/C));
  Message("    A = %"LE"\n", A1SW12);
  Message("    B = %"LE"\n", A3SW12);
  Message("    alpha = %"LE"\n", alpha12_1);

  sqE12 = k;
  sqE212 = sqE12*sqE12;

  G->min = 0;
  G->max = L/2;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_LIEB_PHONON12
DOUBLE InterpolateExactU12(DOUBLE x) {
  if(x<Rpar12)
    return Log(fabs(A1SW12*Cos(sqE12*(x-A3SW12))));
  else
    return Log(pow(Sin(PI*x/L), alpha12_1));
}

DOUBLE InterpolateExactFp12(DOUBLE x) {
#ifdef HARD_SPHERE12
#ifdef SECURE
      if(x<a) Error("InterpolateExactFp : (r<a) call\n");
#endif
#endif

  if(x<Rpar12)
    return -sqE12 * tg(sqE12*(x-A3SW12));
  else
    return alpha12_1*PI/(L*tg(PI*x/L));
}

DOUBLE InterpolateExactE12(DOUBLE x) {
  DOUBLE c;

  if(x<Rpar12) {
    c = tg(sqE12*(x-A3SW12));
    return sqE212*(1.+c*c);
  }
  else {
    c = 1. / tg(PI*x/L);
    return alpha12_1*(PI*PI/(L*L))*(1.+c*c);
  }
}

#endif

/************************** Construct Grid Sutherland ******************/
void ConstructGridSutherland(struct Grid *G) {
  AllocateWFGrid(G, 3);

  Warning("  Input parameters lambda1, lambda2, lambda12 mean lambda of the CSM\n");
  Message("  Sutherland parameter lambda12 = %.15"LE "\n", lambda12);
  Message("  Sutherland parameter lambda1  = %.15"LE "\n", lambda1);
  Message("  Sutherland parameter lambda2  = %.15"LE "\n", lambda2);

  sqE = PI/L;
  sqE2 = sqE*sqE;

  G->min = 0;
  G->max = L/2.;
  G->max2 = G->max*G->max;
}

/* executed only if TRIAL_SUTHERLAND12 is defined */
#ifdef TRIAL_SUTHERLAND12
DOUBLE InterpolateExactU12(DOUBLE x) {
  return lambda12*Log(fabs(Sin(sqE*x)));
}

DOUBLE InterpolateExactFp12(DOUBLE x) {
  return lambda12*sqE/(tg(sqE*x));
}

DOUBLE InterpolateExactE12(DOUBLE x) {
  DOUBLE c;

  c = 1./Sin(sqE*x);
  return lambda12*sqE2*c*c;
}
#endif

/* executed only if TRIAL_SUTHERLAND11 is defined */
#ifdef TRIAL_SUTHERLAND11
DOUBLE InterpolateExactU11(DOUBLE x) {
  return lambda1*Log(fabs(Sin(sqE*x)));
}

DOUBLE InterpolateExactFp11(DOUBLE x) {
  return lambda1*sqE/(tg(sqE*x));
}

DOUBLE InterpolateExactE11(DOUBLE x) {
  DOUBLE c;
  c = 1./tg(sqE*x);
  return lambda1*sqE2*(1.+c*c);
}
#endif

#ifdef TRIAL_SUTHERLAND22
DOUBLE InterpolateExactU22(DOUBLE x) {
  return lambda2*Log(fabs(Sin(sqE*x)));
}

DOUBLE InterpolateExactFp22(DOUBLE x) {
  return lambda2*sqE/(tg(sqE*x));
}

DOUBLE InterpolateExactE22(DOUBLE x) {
  DOUBLE c;

  c = 1./Sin(sqE*x);
  return lambda2*sqE2*c*c;
}
#endif

#ifdef TRIAL_SUTHERLAND_CHANGING_LAMBDA1122
// f(x) = sin[PI x/L]^(1+Apar r + Bpar r^2)
DOUBLE InterpolateExactU11(DOUBLE x) {
  return (1.+Apar*x + Bpar*x*x)*Log(fabs(Sin(sqE*x)));
}

DOUBLE InterpolateExactFp11(DOUBLE x) {
  return (1. + x*(Apar + Bpar*x))*sqE/tan(x*sqE) + (Apar + 2.*Bpar*x)*Log(Sin(sqE*x));
}

DOUBLE InterpolateExactE11(DOUBLE x) {
  DOUBLE c;

  c = 1./Sin(sqE*x);
  return -2.*(Apar + 2.*Bpar*x)*sqE/tan(sqE*x) + (1. + x*(Apar + Bpar*x))*sqE2*c*c - 2.*Bpar*Log(Sin(sqE*x));
}

DOUBLE InterpolateExactU22(DOUBLE x) {
  return (1.+Apar*x + Bpar*x*x)*Log(fabs(Sin(sqE*x)));
}

DOUBLE InterpolateExactFp22(DOUBLE x) {
  return (1. + x*(Apar + Bpar*x))*sqE/tan(x*sqE) + (Apar + 2.*Bpar*x)*Log(Sin(sqE*x));
}

DOUBLE InterpolateExactE22(DOUBLE x) {
  DOUBLE c;

  c = 1./Sin(sqE*x);
  return -2.*(Apar + 2.*Bpar*x)*sqE/tan(sqE*x) + (1. + x*(Apar + Bpar*x))*sqE2*c*c - 2.*Bpar*Log(Sin(sqE*x));
}
#endif

/************************** Construct Grid Coulomb 1D Phonon **************/
void ConstructGridCoulomb1DPhonon(struct Grid *G) {
  AllocateWFGrid(G, 3);

  //Message("  Jastrow term: Coulomb 1D phonon.\n");
  //Warning("  Changing Rpar %lf -> %lf [L/2] units\n", Rpar, Rpar*Lhalf);
  //Rpar *= Lhalf;

  Btrial = (L*(Sqrt(Rpar)*BesselI0(2*Sqrt(Rpar))+BesselI1(2*Sqrt(Rpar))+Sqrt(Rpar)*BesselIN(2,2*Sqrt(Rpar)))*Tg((PI*Rpar)/L))/(2.*PI*Rpar*BesselI1(2*Sqrt(Rpar)));
  Atrial = pow(Sin(PI*Rpar/L),Btrial)/(Sqrt(Rpar)*BesselI1(2*Sqrt(Rpar)));
  Message("Atrial = %"LG" , Btrial = %"LG"\n", Atrial, Btrial);
  Message("  Rpar / [L/2] = %"LG"\n", Rpar/Lhalf);

  if(Rpar>Lhalf) Error("  Rpar must be smaller than L/2!\n");

  //Warning("  Changing Rpar -> Rpar Lhalf (%"LG"  -> %"LG" )\n", Rpar, Rpar*Lhalf);
  //Rpar *= Lhalf; // do not change Rpar earlier!

  G->max = Lhalf;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_COULOMB_1D_PHONON
DOUBLE InterpolateExactU11(DOUBLE r) {
  if(r>Lhalf) 
    return 0;
  else if(r<Rpar)
    return Log(Atrial*Sqrt(r)*BesselI1(2*Sqrt(r)));
  else
    return Log(Sin(PI/L*r))*Btrial;
}

DOUBLE InterpolateExactFp11(DOUBLE r) {
  DOUBLE I0,I1,I2,arg;
  if(r>Lhalf) 
    return 0.;
  else if(r<Rpar) {
    arg = 2.*Sqrt(r);
    I0 = BesselI0(arg);
    I1 = BesselI1(arg);
    I2 = BesselIN(2,arg);
    return (Sqrt(r)*(I0+I2)+I1)/(2.*r*I1);
  }
  else {
    return Btrial*PI/(L*Tg(PI/L*r));
  }
}

DOUBLE InterpolateExactE11(DOUBLE r) {
  //1D Eloc = [-f"/f] + (f'/f)^2
  DOUBLE c;
  if(r>Lhalf)
    return 0.;
  else if(r<Rpar) {
    c = InterpolateExactFp11(r);
    return -1/r + c*c;
  }
  else {
    c = 1 / Tg(PI*r/L);
    return Btrial*PI*PI/(L*L)*(1+c*c);
  }
}

DOUBLE InterpolateExactU22(DOUBLE r) {
  if(r>Lhalf) 
    return 0;
  else if(r<Rpar)
    return Log(Atrial*Sqrt(r)*BesselI1(2*Sqrt(r)));
  else
    return Log(Sin(PI/L*r))*Btrial;
}

DOUBLE InterpolateExactFp22(DOUBLE r) {
  DOUBLE I0,I1,I2,arg;
  if(r>Lhalf) 
    return 0.;
  else if(r<Rpar) {
    arg = 2.*Sqrt(r);
    I0 = BesselI0(arg);
    I1 = BesselI1(arg);
    I2 = BesselIN(2,arg);
    return (Sqrt(r)*(I0+I2)+I1)/(2.*r*I1);
  }
  else {
    return Btrial*PI/(L*Tg(PI/L*r));
  }
}

DOUBLE InterpolateExactE22(DOUBLE r) {
  //1D Eloc = [-f"/f] + (f'/f)^2
  DOUBLE c;
  if(r>Lhalf)
    return 0.;
  else if(r<Rpar) {
    c = InterpolateExactFp22(r);
    return -1/r + c*c;
  }
  else {
    c = 1 / Tg(PI*r/L);
    return Btrial*PI*PI/(L*L)*(1+c*c);
  }
}
#endif

#ifdef TRIAL_COULOMB_1D_PHONON12
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r>Lhalf) 
    return 0;
  else if(r<Rpar)
    return Log(Atrial*Sqrt(r)*BesselI1(2*Sqrt(r)));
  else
    return Log(Sin(PI/L*r))*Btrial;
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  DOUBLE I0,I1,I2,arg;
  if(r>Lhalf) 
    return 0.;
  else if(r<Rpar) {
    arg = 2.*Sqrt(r);
    I0 = BesselI0(arg);
    I1 = BesselI1(arg);
    I2 = BesselIN(2,arg);
    return (Sqrt(r)*(I0+I2)+I1)/(2.*r*I1);
  }
  else {
    return Btrial*PI/(L*Tg(PI/L*r));
  }
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  //1D Eloc = [-f"/f] + (f'/f)^2
  DOUBLE c;
  if(r>Lhalf)
    return 0.;
  else if(r<Rpar) {
    c = InterpolateExactFp11(r);
    return -1/r + c*c;
  }
  else {
    c = 1 / Tg(PI*r/L);
    return Btrial*PI*PI/(L*L)*(1+c*c);
  }
}
#endif
/************************** Construct Grid Coulomb 1D Phonon **************/
DOUBLE Atrial2, Btrial2;
void ConstructGridCoulomb1DPhononEx(struct Grid *G) {
  AllocateWFGrid(G, 3);

  //Message("  Jastrow 12 term : Attractive Coulomb 1D phonon.\n");
  //Warning("  Changing Rpar2 %lf -> %lf [L/2] units\n", Rpar2, Rpar2*Lhalf);
  //Rpar2 *= Lhalf;

  Btrial2 = L*(2*excitonRadius-Rpar2)/(2*pi*Rpar2*excitonRadius)*Tg(PI*Rpar2/L);
  Atrial2 = pow(Sin(PI*Rpar2/L),Btrial2)/(Rpar2*Exp(-Rpar2/2.));
  Message("  Atrial2 = %"LG" , Btrial2 = %"LG"\n", Atrial2, Btrial2);

  if(Rpar2>Lhalf) Error("  Rpar must be smaller than L/2!\n");

  //Warning("  Changing Rpar -> Rpar Lhalf (%"LG"  -> %"LG" )\n", Rpar, Rpar*Lhalf);
  //Rpar *= Lhalf; // do not change Rpar earlier!

  G->max = Lhalf;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_COULOMB12exPHONON
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r>Lhalf) 
    return 0;
  else if(r<Rpar2)
    return Log(Atrial2*r)-r/(2.*excitonRadius);
  else
    return Log(Sin(PI/L*r))*Btrial2;
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  DOUBLE I0,I1,I2,arg;
  if(r>Lhalf) 
    return 0.;
  else if(r<Rpar2) {
    return 1./r-0.5/excitonRadius;
  }
  else {
    return Btrial2*PI/(L*Tg(PI/L*r));
  }
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  //1D Eloc = [-f"/f] + (f'/f)^2
  DOUBLE c;
  if(r>Lhalf)
    return 0.;
  else if(r<Rpar2) {
    return 1./(r*r);
  }
  else {
    c = 1 / Tg(PI*r/L);
    return Btrial2*PI*PI/(L*L)*(1+c*c);
  }
}
#endif

/************************** Construct Grid Coulomb 3D Phonon **************/
void ConstructGridCoulombPhonon11(struct Grid *G) {
  DOUBLE arg,r,I0,I1,I2;
  AllocateWFGrid(G, 3);

  Message("  Jastrow term 11,22: 3D repulsive Coulomb + phonons\n");
  Warning("  Changing Rpar %lf -> %lf [L/2] units\n", Rpar, Rpar*Lhalf);
  Rpar *= Lhalf;

  // matching f'(Rpar)/f(Rpar)
  //Btrial = -(-Rpar*Rpar*(Rpar-L)*(Rpar-L)*(Rpar-L)*(Sqrt(Rpar)*BesselI0(2*Sqrt(Rpar)) - BesselI1(2*Sqrt(Rpar)) + Sqrt(Rpar)*BesselIN(2,2*Sqrt(Rpar))))/(4.*(L*L*L - 3.*L*L*Rpar + 3.*L*Rpar*Rpar - 2.*Rpar*Rpar*Rpar)*BesselI1(2*Sqrt(Rpar)));
  r = Rpar;
  arg = 2.*Sqrt(r);
  I0 = BesselI0(arg);
  I1 = BesselI1(arg);
  I2 = BesselIN(2,arg);
  Btrial = (Sqrt(r)*(I0+I2)-I1)/(2.*r*I1)/(2.*(1./(r*r*r)-1./((L-r)*(L-r)*(L-r))));
  // matching f(Rpar)
  Atrial = Btrial*(2./Lhalf2-1./(Rpar*Rpar)-1./((L-Rpar)*(L-Rpar))) - Log(BesselI1(2*Sqrt(Rpar)))+0.5*Log(Rpar);

  Message("  Atrial = %"LG" , Btrial = %"LG"\n", Atrial, Btrial);

  if(Rpar>Lhalf) Error("  Rpar must be smaller than L/2!\n");

  G->max = Lhalf;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_COULOMB_3D_PHONON11
DOUBLE InterpolateExactU11(DOUBLE r) {
  if(r>Lhalf)
    return 0;
  else if(r<Rpar)
    return Atrial+Log(BesselI1(2*Sqrt(r)))-0.5*Log(r);
  else
    return Btrial*(2./Lhalf2-1./(r*r)-1./((L-r)*(L-r)));
}

DOUBLE InterpolateExactFp11(DOUBLE r) {
  DOUBLE I0,I1,I2,arg;
  if(r>Lhalf) 
    return 0.;
  else if(r<Rpar) {
    arg = 2.*Sqrt(r);
    I0 = BesselI0(arg);
    I1 = BesselI1(arg);
    I2 = BesselIN(2,arg);
    return (Sqrt(r)*(I0+I2)-I1)/(2.*r*I1);
  }
  else {
    return 2.*Btrial*(1./(r*r*r)-1./((L-r)*(L-r)*(L-r)));
  }
}

DOUBLE InterpolateExactE11(DOUBLE r) {
  //3D Eloc = [-f"/f - 2/r f'/f] + (f'/f)^2
  DOUBLE c;
  if(r>Lhalf)
    return 0.;
  else if(r<Rpar) {
    c = InterpolateExactFp11(r);
    return -1/r + c*c;
  }
  else {
    return 2.*Btrial*(L*L*L*L - 4.*L*L*L*r + 6.*L*L*r*r - 2.*L*r*r*r + 2.*r*r*r*r)/(r*r*r*r*(L-r)*(L-r)*(L-r)*(L-r));
  }
}

DOUBLE InterpolateExactU22(DOUBLE r) {
  if(r>Lhalf)
    return 0;
  else if(r<Rpar)
    return Atrial+Log(BesselI1(2*Sqrt(r)))-0.5*Log(r);
  else
    return Btrial*(2./Lhalf2-1./(r*r)-1./((L-r)*(L-r)));
}

DOUBLE InterpolateExactFp22(DOUBLE r) {
  DOUBLE I0,I1,I2,arg;
  if(r>Lhalf) 
    return 0.;
  else if(r<Rpar) {
    arg = 2.*Sqrt(r);
    I0 = BesselI0(arg);
    I1 = BesselI1(arg);
    I2 = BesselIN(2,arg);
    return (Sqrt(r)*(I0+I2)-I1)/(2.*r*I1);
  }
  else {
    return 2.*Btrial*(1./(r*r*r)-1./((L-r)*(L-r)*(L-r)));
  }
}

DOUBLE InterpolateExactE22(DOUBLE r) {
  //3D Eloc = [-f"/f - 2/r f'/f] + (f'/f)^2
  DOUBLE c;
  if(r>Lhalf)
    return 0.;
  else if(r<Rpar) {
    c = InterpolateExactFp11(r);
    return -1/r + c*c;
  }
  else {
    return 2.*Btrial*(L*L*L*L - 4.*L*L*L*r + 6.*L*L*r*r - 2.*L*r*r*r + 2.*r*r*r*r)/(r*r*r*r*(L-r)*(L-r)*(L-r)*(L-r));
  }
}
#endif

/************************** Construct Grid Coulomb 3D Phonon **************/
void ConstructGridCoulombPhonon12(struct Grid *G) {
  DOUBLE r;
  AllocateWFGrid(G, 3);

  Message("  Jastrow term 12: 3D attractive Coulomb + phonons\n");
  Warning("  Changing Rpar2 %lf -> %lf [L/2] units\n", Rpar2, Rpar2*Lhalf);
  Rpar2 *= Lhalf;

  r = Rpar2;
  // matching f'(Rpar)/f(Rpar)
  Btrial2 = -0.5/excitonRadius/(2.*(1./(r*r*r)-1./((L-r)*(L-r)*(L-r))));
  // matching f(Rpar)
  Atrial2 = Btrial2*(2./Lhalf2-1./(r*r)-1./((L-r)*(L-r)))+r/(2.*excitonRadius);
  Message("  Atrial2 = %"LG" , Btrial2 = %"LG"\n", Atrial2, Btrial2);

  if(Rpar2>Lhalf) Error("  Rpar2 must be smaller than L/2!\n");

  G->max = Lhalf;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_COULOMB_3D_PHONON12
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r>Lhalf)
    return 0;
  else if(r<Rpar2)
    return Atrial2-r/(2.*excitonRadius);
  else
    return Btrial2*(2./Lhalf2-1./(r*r)-1./((L-r)*(L-r)));
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  DOUBLE I0,I1,I2,arg;
  if(r>Lhalf) 
    return 0.;
  else if(r<Rpar2) {
    return -0.5/excitonRadius;
  }
  else {
    return 2.*Btrial2*(1./(r*r*r)-1./((L-r)*(L-r)*(L-r)));
  }
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  //3D Eloc = [-f"/f - 2/r f'/f] + (f'/f)^2
  DOUBLE c;
  if(r>Lhalf)
    return 0.;
  else if(r<Rpar2) {
    return 1./(excitonRadius*r);
  }
  else {
    return 2.*Btrial2*(L*L*L*L - 4.*L*L*L*r + 6.*L*L*r*r - 2.*L*r*r*r + 2.*r*r*r*r)/(r*r*r*r*(L-r)*(L-r)*(L-r)*(L-r));
  }
}
#endif

/************************** Construct Grid Dipole **************************/
void ConstructGridDipole(struct Grid *G, const DOUBLE Apar, const DOUBLE Bpar, const DOUBLE n) {

  AllocateWFGrid(G, 3);

#ifdef TRIAL_1D
  A1SW = pow(Apar/n, Bpar);
#endif
#ifdef TRIAL_3D
  A1SW = 0.5*pow(Apar, Bpar);
#endif
  A4SW = A1SW * pow(0.5*L, -Bpar);

  Message("  Dipole trial wavefunction\n");

  G->min = 0;
  G->max = L/2;
  G->max2 = G->max*G->max;
}


/************************** Construct Grid HS *****************************/
void ConstructGridPseudopotentialFreeState(struct Grid *G, const DOUBLE a, const DOUBLE Rpar) {
  DOUBLE x, xmin, xmax;
  DOUBLE y, ymin, ymax;
  DOUBLE V;
  DOUBLE precision = 1e-12;
  int search = ON;
  int i;
  DOUBLE dx;
  int size = 3;

  AllocateWFGrid(G, size);
  i = (int) G->size;

  V = a/Rpar;

  // x = kR
  // tg (kR - arctg kR)/kR = a/R
  xmin = 1e-10;
  xmax = 2.79838;

  ymin = tg(xmin-arctg(xmin))/xmin - V;
  ymax = tg(xmax-arctg(xmax))/xmax - V;

  if(ymin*ymax > 0) Error("Can't construct the grid : No solution found");

  while(search) {
    x = (xmin+xmax)/2.;
    y = tg(x-arctg(x))/x - V;

    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }

    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y = tg(x-arctg(x))/x - V;
      search = OFF;
    }
  }
  Message("Solution is k = %"LF", error %"LE"\n", x/Rpar, y);

  sqE = x/Rpar;
  A3SW = arctg(x) - x;

  //sqE = 0.5*PI/(Lhalf-a); // cos k(Lhalf-a) = 0!
  //A3SW = -sqE*a;

  A1SW = L/2./Sin(sqE*L/2.+A3SW);
  sqE2 = sqE*sqE;
  if(fabs(tg(A3SW+x)-x)>1e-10) Error(" Boundary condition violated.");
  if(fabs(tg(A3SW) + sqE*a)>1e-10) Error(" Matching condition violated.");

  G->min = 0;
  G->max = L;
  dx = (G->max - G->min) / (DOUBLE) (size-1);
  G->step = dx;
  G->I_step = (DOUBLE) size / (G->max - G->min);
  G->max2 = G->max*G->max;
}

/* executed only if TRIAL_PSEUDOPOT_FS is defined */
#ifdef TRIAL_PSEUDOPOT_FS
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r<Lhalf)
    return Log(fabs(A1SW/r *Sin(sqE*r+A3SW)));
  else
    return 0.;
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  if(r<Lhalf)
    return sqE / tg(sqE*r + A3SW) - 1./r;
  else
    return 0.;
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  DOUBLE g;

  if(r<Lhalf) {
    g = sqE/tg(sqE*r + A3SW) - 1./r;
    return sqE2 + g*g;
  }
  else
    return 0.;
}

DOUBLE InterpolateExactU11(DOUBLE r) {
  return 0.;
}
DOUBLE InterpolateExactFp11(DOUBLE r) {
  return 0.;
}
DOUBLE InterpolateExactE11(DOUBLE r) {
  return 0.;
}
#endif

/************************** Construct Grid HS *****************************/
void ConstructGridPseudopotentialBoundState(struct Grid *G) {
  Message("  Pseudopotential bound state wavefunction");

  AllocateWFGrid(G, 3);
  sqE = 1./a;
  A1SW = Lhalf*(sqE*Lhalf);
}

/************************** Construct Grid Fermion ************************/
void ConstructGridSoftSphereSquareWell(struct Grid *G) {
  DOUBLE kappaR, kR, KappaR;
  DOUBLE V,y,ymin,ymax,x,xmin,xmax;
  DOUBLE precision = 1e-10;
  DOUBLE trial_delta_check;
  int search;
  static int first_time = ON;

  if(first_time) Message("Soft sphere potential, parameters: 0<Cpar<1, 0<Apar<1; Cpar=1 -> Cpar=a\n");

  if(Cpar == 1) {
    if(first_time) Warning("  Range of the fictitious potential: Cpar = a = %"LE"\n", a);
    Cpar = a;
  }
  else {
    if(first_time) Message("  Range of the fictitious potential: Cpar = %"LE" [R]\n", Cpar);
  }

  if(Apar>=1) Error("  Apar must be smaller than 1");
  if(first_time) Warning("  Scattering length on the fictitous potential Apar %"LE" [a] -> %"LE" [R]\n", Apar, Apar*Cpar);
  Apar *= Cpar;

  // define phase shift
  search = ON;
  // aDD/R = 1 - th kappaR / kappaR
  // x = kappaR
  V = Apar/ Cpar; // dimer-dimer scattering length
  xmin = 1e-14;
  xmax = 1e100;

  ymin = 1-th(xmin)/xmin - V;
  ymax = 1-th(xmax)/xmax - V;

  if(first_time) Message("  Fixing the depth of the ficticious potential...");
  if(ymin*ymax > 0) Error("Can't construct the grid : No solution found");

  while(search) {
    x = (xmin+xmax)/2.;
    y = 1-th(x)/x - V;
    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }

    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y = 1-th(x)/x - V;
      search = OFF;
    }
  }
  kappaR = x;

  if(first_time) Message("  done\n  Solution is kappa R = %"LF", error %"LE"\n", x, y);
  if(first_time) Message("  Height is %.2e [h^2/mR^2] = %.2e\n", x*x/(Cpar*Cpar), x*x/(Cpar*Cpar)*energy_unit);
  if(first_time) Message("  Matching the solutions...");

  // arctg x - x - arctg (k th KappaR /KappaR) + kR = 0
  // kR = kL/2 R/(L/2)
  // x = kL/2
  V = 0;
  xmin = 1e-10;
  xmax = kappaR*Lhalf/Cpar*(1.-1e-10);
  //xmax = sqrt(kappaR*kappaR-PI*PI/4.)*Lhalf/Cpar;
  //if(PI/2. - 1e-10<xmax) xmax = PI/2. - 1e-10;

  kR = xmin * Cpar / Lhalf;
  if(kappaR<kR) Error("kappaR < kR\n");
  KappaR = sqrt(kappaR*kappaR-kR*kR);
  ymin = arctg(xmin) - xmin - arctg(kR * th(KappaR)/KappaR) + kR - V;
  kR = xmax * Cpar / Lhalf;
  if(kappaR<kR) Error("kappaR < kR\n");
  KappaR = sqrt(kappaR*kappaR-kR*kR);
  ymax = arctg(xmax) - xmax - arctg(kR * th(KappaR)/KappaR) + kR - V;

  if(ymin*ymax > 0) Error("Can't construct the grid : No solution found");

  search = ON;
  while(search) {
    x = (xmin+xmax)/2.;
    kR = x * Cpar / Lhalf;
    KappaR = sqrt(kappaR*kappaR-kR*kR);
    y = arctg(x) - x - arctg(kR * th(KappaR)/KappaR) + kR - V;

    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }

    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      kR = x * Cpar / Lhalf;
      KappaR = sqrt(kappaR*kappaR-kR*kR);
      y = arctg(x) - x - arctg(kR * th(KappaR)/KappaR) + kR - V;
      search = OFF;
    }
  }

  trial_delta = arctg(x) - x;
  trial_delta_check = arctg(kR * th(KappaR)/KappaR) - kR;
  if(fabs(trial_delta-trial_delta_check)/trial_delta>1e-6) Error("Wrong solution found");
  if(first_time) Message(" done\n  xmat RR = %"LE", error %"LE"\n", x, y);
  if(first_time) Message("  delta = %"LE"\n", trial_delta);

  sqE = kR/Cpar;
  sqE2 = sqE*sqE;
  if(first_time) Message("  Energy of the scattering state %"LE" [h^2/mR^2] = %"LE"\n", sqE2, sqE2*energy_unit);

  A3SWSS = Lhalf/Sin(sqE*Lhalf+trial_delta);
  A1SWSS = A3SWSS*Sin(kR+trial_delta)/sinh(KappaR);
  A4SWSS = KappaR/Cpar;
  G->min = 0;
  G->max = Lhalf;
  G->max2 = G->max*G->max;

  first_time = OFF;
}

#ifdef TRIAL_SS_SW
// up - down: Gauss
DOUBLE InterpolateExactU12(DOUBLE r) {
  return Log(1-Gamma*Exp(-beta*r*r));
}

// Fp = f'/f
DOUBLE InterpolateExactFp12(DOUBLE r) {
  return 2*beta*Gamma*r/(Exp(beta*r*r)-Gamma);
}

// Eloc = -(f" +2/r f')/f+(f'/f)^2
DOUBLE InterpolateExactE12(DOUBLE r) {
  DOUBLE ex;

  ex = Exp(beta*r*r);
  return 2*beta*Gamma*(ex*(2*beta*r*r-3)+3*Gamma)/((ex-Gamma)*(ex-Gamma));
}

/*DOUBLE InterpolateExactU12(DOUBLE r) {
  return Log(1-Gamma*Exp(-beta*r*r)-Gamma2*Exp(-beta2*r*r));
}

// Fp = f'/f
DOUBLE InterpolateExactFp12(DOUBLE r) {
  DOUBLE ex1, ex2;

  ex1 = Exp(-beta*r*r);
  ex2 = Exp(-beta2*r*r);
  return 2*r*(Gamma*beta*ex1+Gamma2*beta2*ex2)/(1-Gamma*ex1-Gamma2*ex2);
}

// Eloc = -(f" +2/r f')/f+(f'/f)^2
DOUBLE InterpolateExactE12(DOUBLE r) {
  DOUBLE ex1, ex2, F, Fp;

  ex1 = Exp(-beta*r*r);
  ex2 = Exp(-beta2*r*r);

  F = 1-Gamma*ex1-Gamma2*ex2;
  Fp = 2*r*(Gamma*beta*ex1+Gamma2*beta2*ex2)/F;

  return (4*r*r*(Gamma*beta*beta*ex1+Gamma2*beta2*beta2*ex2)-6*(Gamma*beta*ex1+Gamma2*beta2*ex2))/F + Fp*Fp;
}*/

// up - up: SS
DOUBLE InterpolateExactU11(DOUBLE r) {
  if(r<Cpar) {
    if(r == 0.)
      return Log(fabs(A1SWSS*A4SWSS));
    else
      return Log(fabs(A1SWSS*sh(A4SWSS*r)/r));
  }
  else if(r<Lhalf)
    return Log(fabs(A3SWSS*Sin(sqE*r+trial_delta)/r));
  else
    return 0;
}

DOUBLE InterpolateExactFp11(DOUBLE r) {
  if(r<Cpar) {
    if(r==0.) return 0.;
    return A4SWSS/th(A4SWSS*r) - 1./r;
  }
  else if(r<Lhalf)
    return sqE/tg(sqE*r+trial_delta) - 1./r;
  else
    return 0;
}

DOUBLE InterpolateExactE11(DOUBLE r) {
  DOUBLE fp=0;

  if(r<Cpar) {
    if(r == 0.)
      return 0.;
    else {
      fp = A4SWSS/th(A4SWSS*r) - 1./r;
      return -A4SWSS*A4SWSS + fp*fp;
    }
  }
  else if(r<Lhalf){
    fp = sqE/tg(sqE*r+trial_delta) - 1./r;
    return sqE2 + fp*fp;
  }
  else
    return 0;
}

// dn - dn: SS
DOUBLE InterpolateExactU22(DOUBLE r) {
  if(r<Cpar) {
    if(r == 0.)
      return Log(fabs(A1SWSS*A4SWSS));
    else
      return Log(fabs(A1SWSS*sh(A4SWSS*r)/r));
  }
  else if(r<Lhalf)
    return Log(fabs(A3SWSS*Sin(sqE*r+trial_delta)/r));
  else
    return 0;
}

DOUBLE InterpolateExactFp22(DOUBLE r) {
  if(r<Cpar) {
    if(r==0.) return 0.;
    return A4SWSS/th(A4SWSS*r) - 1./r;
  }
  else if(r<Lhalf)
    return sqE/tg(sqE*r+trial_delta) - 1./r;
  else
    return 0;
}

DOUBLE InterpolateExactE22(DOUBLE r) {
  DOUBLE fp=0;

  if(r<Cpar) {
    if(r == 0.)
      return 0.;
    else {
      fp = A4SWSS/th(A4SWSS*r) - 1./r;
      return -A4SWSS*A4SWSS + fp*fp;
    }
  }
  else if(r<Lhalf){
    fp = sqE/tg(sqE*r+trial_delta) - 1./r;
    return sqE2 + fp*fp;
  }
  else
    return 0;
}
#endif

/************************** Construct Grid Dipole **************************/
void ConstructAbsent(struct Grid *G) {
  AllocateWFGrid(G, 3);
}

/************************ Interpolate U ************************************/
/* U = ln(f) */

DOUBLE InterpolateGridU(struct Grid *Grid, DOUBLE r) {
  int i;
  DOUBLE dx;

  if(r<=Grid->min) r = Grid->min; //return InterpolateExactU(r);

  i = (int) ((r-Grid->min)/Grid->step + 0.5);
  if(i >= Grid->size-1) i = Grid->size-1; //return InterpolateExactU(r);

  dx = r - Grid->x[i];

  // linear interpolation 
  return Grid->lnf[i] + (Grid->lnf[i+1]-Grid->lnf[i]) * Grid->I_step * dx;
}

/* executed only if TRIAL_LIM is defined */
#ifdef TRIAL_LIM
DOUBLE InterpolateExactU(DOUBLE r) {

  if(r>a) {
    return Log(1. - a / r);
  }
  else {
    return -100.;
  }
}
#endif

/* executed only if TRIAL_POWER is defined */
#ifdef TRIAL_POWER
DOUBLE InterpolateExactU(DOUBLE r) {

  if(r>a) {
    return Log(1. - a *pow(r, -Rpar));
  }
  else {
    return -100.;
  }
}
#endif

/* Pseudopotential */
#ifdef TRIAL_PSEUDOPOT_SIMPLE
DOUBLE InterpolateExactU12(DOUBLE x) {
  return Log(fabs(A1SW/x*Sin(sqE*(x-a))));
}

DOUBLE InterpolateExactFp12(DOUBLE x) {
  DOUBLE fp;

  fp = sqE / tg(sqE*(x-a)) - 1./x;

  /*if(x<a)
    return -fp;
  else*/
    return fp;
}

DOUBLE InterpolateExactE12(DOUBLE x) {
  DOUBLE fp;

  fp = sqE / tg(sqE*(x-a))-1./x;
  return sqE2 + fp*fp;
}
#endif


/* executed only if TRIAL_SS is defined */
#ifdef TRIAL_SS
DOUBLE InterpolateExactU(DOUBLE x) {
  DOUBLE f;

  if(x<Apar) {
    if(high_energy) f = A1SW * Sin(sqEkappa*x) / x;
    else f = A1SW * sinh(sqEkappa*x) / x;
  }
  else if(x<Rpar) {
    f = A3SW * Sin(sqE*x+trial_delta) / x;
  }
  else {
    f = 1. - A4SW*Exp(-alpha_1*x);
  }
#ifndef __WATCOMC__
    if(f<1e-10) return -100.;
#endif
  return Log(f);
}
#endif

/* executed only if TRIAL_SS is defined */
#ifdef TRIAL_SS
DOUBLE InterpolateExactU(DOUBLE x) {
  DOUBLE f;

  if(x<Apar) {
    if(high_energy) f = A1SW * Sin(sqEkappa*x) / x;
    else f = A1SW * sinh(sqEkappa*x) / x;
  }
  else if(x<Rpar) {
    f = A3SW * Sin(sqE*x+trial_delta) / x;
  }
  else {
    f = 1. - A4SW*Exp(-alpha_1*x);
  }
#ifndef __WATCOMC__
    if(f<1e-10) return -100.;
#endif
  return Log(f);
}
#endif

/* executed only if TRIAL_MCGUIRE11_DIMER is defined */
#ifdef TRIAL_MCGUIRE11_DIMER
DOUBLE InterpolateExactU11(DOUBLE r) {
  return -r/aA + sqrt(Bpar*a*a+r*r)*(1./aA + 1./a - 1./(2.*Apar));
}

DOUBLE InterpolateExactFp11(DOUBLE r) {
  return -1./aA + (1./aA + 1./a - 1./(2.*Apar)) * r / sqrt(Bpar*a*a+r*r);
}

DOUBLE InterpolateExactE11(DOUBLE r) {
// Eloc1D = -[f"/f - (f'/f)^2]
  return (a*(a*aA - 2*(a + aA)*Apar)*Bpar)/(2.*aA*Apar*(a*a*Bpar + r*r)*sqrt(a*a*Bpar + r*r));
}
#endif

/* executed only if TRIAL_MCGUIRE11_SH is defined */
#ifdef TRIAL_MCGUIRE11_SH
DOUBLE InterpolateExactU11(DOUBLE r) {
  return log(sinh(r*(1./a - 0.5/Apar)) - aA*(1./a-0.5/Apar));
}

DOUBLE InterpolateExactFp11(DOUBLE r) {
  double f,fp;

  f = sinh(r*(1./a - 0.5/Apar)) - aA*(1./a-0.5/Apar);
  fp = cosh(r*(1./a - 0.5/Apar)) * (1./a - 0.5/Apar);

  return fp/f;
}

DOUBLE InterpolateExactE11(DOUBLE r) {
// Eloc1D = -[f"/f - (f'/f)^2]
  double f,fp,fpp;

  f = sinh(r*(1./a - 0.5/Apar)) - aA*(1./a-0.5/Apar);
  fp = cosh(r*(1./a - 0.5/Apar)) * (1./a - 0.5/Apar);
  fpp = sinh(r*(1./a - 0.5/Apar)) * (1./a - 0.5/Apar)* (1./a - 0.5/Apar);
  fp /= f;
  fpp /= f;
  return -(fpp - fp*fp);
}
#endif

/* executed only if TRIAL_MCGUIRE is defined */
#ifdef TRIAL_MCGUIRE12
// f(x) = Exp(-x/aAB)
DOUBLE InterpolateExactU12(DOUBLE r) {
#ifdef TRIAL_1D
  return -r/a;
#endif
#ifdef TRIAL_3D
  //return -r/a - Log(r);
  return -(r-Lhalf)/a - Log(r/Lhalf); // f(Lhalf) = 1
#endif
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
#ifdef TRIAL_1D
  return -1./a;
#endif
#ifdef TRIAL_3D
  return -1./r - 1./a;
#endif
}

/* Eloc1D = -[f"/f - (f'/f)^2]*/
DOUBLE InterpolateExactE12(DOUBLE r) {
#ifdef TRIAL_1D
  return 0.;
#endif
#ifdef TRIAL_3D
  return (1./r + 2./a)/r;
#endif
}
#endif

/* executed only if TRIAL_MCGUIRE is defined */
#ifdef TRIAL_MCGUIRE11
// f(x) = Exp(-x/aAA)
DOUBLE InterpolateExactU11(DOUBLE r) {
#ifdef TRIAL_1D
  return -r/aA;
#endif
#ifdef TRIAL_3D
  //return -r/aA - Log(r);
  return -(r-Lhalf)/aA - Log(r/Lhalf); // f(Lhalf) = 1
#endif
}

DOUBLE InterpolateExactFp11(DOUBLE r) {
#ifdef TRIAL_1D
  return -1./aA;
#endif
#ifdef TRIAL_3D
  return -1./r - 1./aA;
#endif
}

/* Eloc1D = -[f"/f - (f'/f)^2]*/
DOUBLE InterpolateExactE11(DOUBLE r) {
#ifdef TRIAL_1D
  return 0.;
#endif
#ifdef TRIAL_3D
  return (1./r + 2./aA)/r;
#endif
}
#endif

void ConstructGridMcGuireGaussian1D(void) {
  Message("  TRIAL_MCGUIRE12_GAUSSIAN: (1D) exp(-r / Atrial) + Apar*exp(-(1/2) r^2 / Bpar^2), Atrial defined from the s-wave scattering length a\n");
  //exp(-0.5*r^2/Bpar^2) = exp( - Bparnew r^2)
  Bpar = 0.5 / (Bpar*Bpar);
  Atrial = a / (1. + Apar);
}
/* executed only if TRIAL_MCGUIRE is defined */
#ifdef TRIAL_MCGUIRE12_GAUSSIAN1D
DOUBLE InterpolateExactU12(DOUBLE r) {
  return log(exp(-r / Atrial) + Apar*exp(-Bpar*r*r));
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  double f,fp;

  f = exp(-r / Atrial) + Apar*exp(-Bpar*r*r);
  fp = - exp(-r / Atrial)/Atrial - 2.*r*Apar*Bpar*exp(-Bpar*r*r);

  return fp/f;
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  // Eloc1D = -[f"/f - (f'/f)^2]
  double f,fp,fpp;

  f = exp(-r / Atrial) + Apar*exp(-Bpar*r*r);
  fp = - exp(-r / Atrial)/Atrial - 2.*r*Apar*Bpar*exp(-Bpar*r*r);
  //fpp = exp(-r / Atrial)/(Atrial*Atrial) - 2.*Apar*Bpar*exp(-Bpar*r*r) + 4.*r*r*Apar*Bpar*Bpar*exp(-Bpar*r*r);
  fpp = exp(-r / Atrial)/(Atrial*Atrial) + (- 1. + 2.*r*r*Bpar)*2.*Apar*Bpar*exp(-Bpar*r*r);

  return -fpp/f + fp*fp/(f*f);
}
#endif

void ConstructGridMcGuireGaussian3D(void) {
  Message("  TRIAL_MCGUIRE12_GAUSSIAN3D // (3D) exp(-r/aAB - Apar12 r^2) / r\n");
}

#ifdef TRIAL_MCGUIRE12_GAUSSIAN3D
DOUBLE InterpolateExactU12(DOUBLE r) {
  return -r/a - Apar12*r*r - log(r);
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
   //Fp = u'
  return -1./a - 2.*Apar12*r -1./r;
}

DOUBLE InterpolateExactE12(DOUBLE r) {
   //Eloc = -u'' - (D-1)u'/r
  //return (a+2.*r)/(a*r*r) - 6.*Apar12;
  return 6.*Apar12 + (a + 2.*r)/(a*r*r);
}
#endif

void ConstructGridMcGuireFermi(void) {
  Message("  TRIAL_MCGUIRE12_FERMI: (1D) exp(-r / Atrial) + Apar/(1. + exp((r-Epar)/T)), Atrial defined from the s-wave scattering length a\n");
  // Atrial = a / f(0)
  Atrial = a / (1. + Apar/(1. + exp(-Epar/T)));
}

/* executed only if TRIAL_MCGUIRE is defined */
#ifdef TRIAL_MCGUIRE12_FERMI
DOUBLE InterpolateExactU12(DOUBLE r) {
  return log(exp(-r / Atrial) + Apar/(1. + exp((r-Epar)/T)));
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  double f, fp, fexp, fexpinv;
  double f1, f2, fp1, fp2;

  f1 = exp(-r / Atrial);
  fp1 = -f1 / Atrial;

  fexp = exp((r - Epar) / T);
  fexpinv = 1. / (1. + fexp);
  f2 = Apar * fexpinv;
  fp2 = - f2 * fexp * fexpinv / T;

  return (fp1+fp2)/(f1+f2);
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  // Eloc1D = -[f"/f - (f'/f)^2]
  double f, fp, fpp, fexp, fexpinv;
  double f1, f2, fp1, fp2, fpp1, fpp2;

  f1 = exp(-r / Atrial);
  fp1 = -f1 / Atrial;
  fpp1 = f1 / (Atrial*Atrial);

  fexp = exp((r - Epar) / T);
  fexpinv = 1. / (1. + fexp);
  f2 = Apar * fexpinv;
  fp2 = - f2 * fexp * fexpinv / T;
  fpp2 = f2 * fexp * fexpinv / (T*T)*(2.*fexp * fexpinv-1.);

  f = f1 + f2;
  fp = fp1 + fp2;
  fpp = fpp1 + fpp2;

  return -fpp/f + fp*fp/(f*f);
}
#endif

/************************** Construct Grid Mc Guire PBC *******************/
void ConstructGridMcGuirePBC(void) {
  static int first_time = ON;
  DOUBLE xmin, xmax, ymin, ymax, x, y;
  int search = ON;
  DOUBLE precision = 1e-8;
  DOUBLE er, eLr;

  Message("  ConstructGridMcGuirePBC: finding adjusted value of b\n");
  Message("  f12: (1D) Exp(x/B) + Exp((2*Rpar12-x)/B) +  Apar12 - 2.*Exp(Rpar12/B), parameters b>0, Apar12>0, 0<Rpar12<L/2: f'(0)/f(0) = 1/b \n");

  if(Rpar12 > Lhalf) {
    Warning("  Rpar12 = %"LG" > L/2 = %"LG", setting Rpar12 = L/2\n", Rpar12, Lhalf);
    Rpar12 = Lhalf;
  }

  // define trial_k
  // Solve: f'(0)/f(0) = 1/b

  xmin = 0.0001*b;
  xmax = b;
  x = xmin;
  ymin = (1-Exp(2.*Rpar12/x)) / (1 + Exp(2.*Rpar12/x) + Apar12 -2.*Exp(Rpar12/x))/x - 1/b;
  x = xmax;
  ymax = (1-Exp(2.*Rpar12/x)) / (1 + Exp(2.*Rpar12/x) + Apar12 -2.*Exp(Rpar12/x))/x - 1/b;
  Message("  Solving equation ... \n");
  if(ymin*ymax > 0) Error("Can't construct the grid : No solution found");
  while(search) {
    x = (xmin+xmax)/2.;
    y = (1-Exp(2.*Rpar12/x)) / (1 + Exp(2.*Rpar12/x) + Apar12 -2.*Exp(Rpar12/x))/x - 1/b;
    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }
    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y = (1-Exp(2.*Rpar12/x)) / (1 + Exp(2.*Rpar12/x) + Apar12 -2.*Exp(Rpar12/x))/x - 1/b;
      search = OFF;
    }
  }
  Message("  initial (thermodynamic) value of b %"LF"\n", b);
  Message("  new value (consistent with PBC) of b %"LF", error %"LE"\n", x, y);
  Message("  1/b = %"LF"\n", 1/b);
  b = x;

  Apar12 = Apar12 - 2.*Exp(Rpar12/x);

  // checking boundary condition
  x = 0.;
  er = Exp(x/b);
  eLr = Exp((2.*Rpar12-x)/b);
  er = (er-eLr)/(b*(er+eLr+Apar12));
  Message("  f'(0)/f(0) = %"LF"\n", er);

  Warning("  Changing Apar12 -> %"LF"\n", Apar12);
  Message("  By subtracting minimal allowed value %"LF"\n", -2.*Exp(Rpar12/b));

  Message("done\n");
}

#ifdef TRIAL_MCGUIRE_PBC12
// f(x) = Exp(-x/a)
// f(x) = C + Exp(-x/a)
DOUBLE InterpolateExactU12(DOUBLE x) {
  if(x>Rpar12) x = Rpar12;
  return Log(Exp(x/b) + Exp((2.*Rpar12-x)/b) + Apar12);
}

DOUBLE InterpolateExactFp12(DOUBLE x) {
  DOUBLE er, eLr;
  if(x>Rpar12) x = Rpar12;
  er = Exp(x/b); // b<0
  eLr = Exp((2.*Rpar12-x)/b);
  return (er-eLr)/(b*(er+eLr+Apar12));
}

/* Eloc1D = -[f"/f - (f'/f)^2]*/
DOUBLE InterpolateExactE12(DOUBLE x) {
  DOUBLE er, eLr, F, Fp, Fpp;

  if(x>Rpar12) x = Rpar12;
  er = Exp(x/b);
  eLr = Exp((2.*Rpar12-x)/b);
  F = er+eLr+Apar12;
  Fp = (er-eLr)/(b*F);
  Fpp = (er+eLr)/(b*b*F);

  return -Fpp + Fp*Fp;
}
#endif

#ifdef TRIAL_MCGUIRE_PBC22_EPAR
// f(x) = Exp(-x/a)
// f(x) = C + Exp(-x/a)
DOUBLE InterpolateExactU22(DOUBLE x) {
  return Log(0.5*(Exp(-Epar*(x-Lhalf)) + Exp(-Epar*(Lhalf-x))));
}

DOUBLE InterpolateExactFp22(DOUBLE x) {
  return -Epar*tanh(Epar*(Lhalf-x));
}

/* Eloc1D = -[f"/f - (f'/f)^2]*/
DOUBLE InterpolateExactE22(DOUBLE x) {
  DOUBLE Fp, Fpp;

  Fp = -Epar*tanh(Epar*(Lhalf-x));
  Fpp = -Epar*Epar;

  return -Fpp + Fp*Fp;
}
#endif

/************************** Construct Grid Mc Guire PBC *******************/
void ConstructGridMcGuirePBCRpar(void) {
  DOUBLE r;

  Message(" McGuire PBC Rpar Jastrow terms, f(r) = Exp(-r/a -Apar12 r^2 + Ctrial r^3), Apar12 - decay rate, Rpar12 - matching point to 1 in units of L/2\n");
  Message(" decay rate: Apar12 = %"LF"\n", Apar12);
  Btrial = -Apar12;
  //Message(" matching point: Rpar12 = %"LF" [in untis of L/2]\n", Rpar12);
  //Rpar12 *= Lhalf;
  Message(" matching point: Rpar12 = %"LF"\n", Rpar12);

  if(Rpar12 > Lhalf) {
    Warning("  Rpar12>Lhalf, setting to Lhalf\n");
    Rpar = Lhalf;
  }
  r = Rpar12;

  Ctrial = -(-1. / a - 1. / r + 2 * Btrial*r) / (3.*r*r); // f'(Rpar) = 0;
  Atrial = -(-r / a + Btrial*r*r + Ctrial*r*r*r - Log(r)); // f(Rpar) = 1
}

#ifdef TRIAL_MCGUIRE_PBC12_RPAR
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r < Rpar12)
    return Atrial - r / a + Btrial*r*r + Ctrial*r*r*r - Log(r);
  else
    return 0.;
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  if(r < Rpar12)
    return -1. / a - 1. / r + 2 * Btrial*r + 3.*Ctrial*r*r;
  else
    return 0.;
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  if(r < Rpar12)
#ifdef TRIAL_3D
    return -6.*Btrial + 1. / (r*r) + 2. / (r*a) - 12.*Ctrial*r;
#endif
#ifdef TRIAL_2D
    return -4.*Btrial + 1. / (r*a) - 9.*Ctrial*r;
#endif
#ifdef TRIAL_1D
    return -2.*Btrial - 1. / (r*r) - 6.*Ctrial*r;
#endif
  else
    return 0.;
}
#endif

/************************** Construct Grid Mc Guire PBC *******************/
void ConstructGridMcGuirePhonon12(void) {
  DOUBLE x;

  Message("  ConstructGridMcGuirePhonon12: finding adjusted value of b\n");
  Message("  f12: ((1D) Exp(x/b); (x<Rpar12), sin^a(pi*x/L) otherwise\n");

  if(Rpar12 > Lhalf) {
    Warning("  Rpar12 = %"LG" > L/2 = %"LG", setting Rpar12 = L/2\n", Rpar12, Lhalf);
    Rpar12 = Lhalf;
  }

  //Atrial12 = -(L*tg(PI*Rpar12/L))/b/PI;
  x = Rpar12;
  Atrial12 = -1./b/(PI/(L*tg(PI*x/L)));
  Btrial12 = Atrial12*Log(Sin(PI*x/L)) + x/b;

  Message("done\n");
}

#ifdef TRIAL_MCGUIRE_PHONON12 // (1D) Exp(x/b); (x<Rpar12), sin^a(pi*x/L) otherwise
DOUBLE InterpolateExactU12(DOUBLE x) {
  if(x<Rpar12)
    return -x/b + Btrial12;
  else
    //return Log(pow(Sin(PI*x/L), Atrial12));
    return Atrial12*Log(Sin(PI*x/L));
}

DOUBLE InterpolateExactFp12(DOUBLE x) {
  if(x<Rpar12)
    return -1./b;
  else
    return Atrial12*PI/(L*tg(PI*x/L));
}

// Eloc1D = -[f"/f - (f'/f)^2]
DOUBLE InterpolateExactE12(DOUBLE x) {
  DOUBLE c;

  if(x<Rpar12) {
    return 0;
  }
  else {
    c = 1. / tg(PI*x/L);
    return Atrial12*(PI*PI/(L*L))*(1.+c*c);
  }
}
#endif

void ConstructGridMcGuirePhonon11(void) {
  DOUBLE x;

  Message("  ConstructGridMcGuirePhonon11:\n");
  Message("  f12: ((1D) Exp(x/aA); (x<Rpar12), sin^a(pi*x/L) otherwise\n");

  if(Rpar11 > Lhalf) {
    Warning("  Rpar12 = %"LG" > L/2 = %"LG", setting Rpar12 = L/2\n", Rpar11, Lhalf);
    Rpar11 = Lhalf;
  }

  x = Rpar11;
  Atrial11 = -1./aA/(PI/(L*tg(PI*x/L)));
  Btrial11 = Atrial11*Log(Sin(PI*x/L)) + x/aA;

  Message("done\n");
}

#ifdef TRIAL_MCGUIRE_PHONON11 // (1D) Exp(x/aA); (x<Rpar11), sin^a(pi*x/L) otherwise
DOUBLE InterpolateExactU11(DOUBLE x) {
  if(x<Rpar11)
    return -x/aA + Btrial11;
  else
    return Atrial11*Log(Sin(PI*x/L));
}

DOUBLE InterpolateExactFp11(DOUBLE x) {
  if(x<Rpar11)
    return -1./aA;
  else
    return Atrial11*PI/(L*tg(PI*x/L));
}

// Eloc1D = -[f"/f - (f'/f)^2]
DOUBLE InterpolateExactE11(DOUBLE x) {
  DOUBLE c;

  if(x<Rpar11) {
    return 0;
  }
  else {
    c = 1. / tg(PI*x/L);
    return Atrial11*(PI*PI/(L*L))*(1.+c*c);
  }
}
#endif

/* executed only if TRIAL_DIPOLE is defined */
#ifdef TRIAL_DIPOLE
DOUBLE InterpolateExactU(DOUBLE z) {
  //if(fabs(z)<0.3*sigma_lj) z = 0.3*sigma_lj; // Pederiva truncates two-mody term !
  return -A1SW*pow(fabs(z), -Bpar) + A4SW;
}
#endif

#ifdef TRIAL_SQUARE_WELL_BS
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r<Ro)
    return Log(fabs(A1SW*Sin(trial_Kappa*r)/r));
  else
    return Log(A3SW/r)-sqE*r;
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  if(r<Ro)
    return trial_Kappa/tg(trial_Kappa*r) - 1./r;
  else
    return - sqE - 1./r;
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  DOUBLE fp;

  if(r<Ro) {
    fp = trial_Kappa/tg(trial_Kappa*r) - 1./r;
    return trial_Kappa2 + fp*fp;
  }
  else {
    fp = -sqE - 1./r;
    return -sqE2 + fp*fp;
  }
}
#endif

#ifdef TRIAL_SH
DOUBLE InterpolateExactU(DOUBLE r) {
  return Log(A1SW*tanh(r)/r);
}
#endif

#ifdef TRIAL_PSEUDOPOT_BS
DOUBLE InterpolateExactU(DOUBLE r) {
  return -sqE*r + Log(A1SW/r);
}
#endif

/* executed only if TRIAL_ABSENT is defined */
#ifdef TRIAL_ABSENT11
  DOUBLE InterpolateExactU11(DOUBLE z) {
  return 0;
}
DOUBLE InterpolateExactFp11(DOUBLE z) {
  return 0;
}
DOUBLE InterpolateExactE11(DOUBLE z) {
  return 0;
}
#endif

#ifdef TRIAL_ABSENT22
DOUBLE InterpolateExactU22(DOUBLE z) {
  return 0;
}
DOUBLE InterpolateExactFp22(DOUBLE z) {
  return 0;
}
DOUBLE InterpolateExactE22(DOUBLE z) {
  return 0;
}
#endif

#ifdef TRIAL_ABSENT12
DOUBLE InterpolateExactU12(DOUBLE z) {
  return 0;
}
DOUBLE InterpolateExactFp12(DOUBLE z) {
  return 0;
}
DOUBLE InterpolateExactE12(DOUBLE z) {
  return 0;
}
#endif

/************************ InterpolateFp ************************************/
/* fp = f'/f */
DOUBLE InterpolateGridFp(struct Grid *Grid, DOUBLE r) {
  int i;
  DOUBLE dx;

  if(r<=Grid->min) r = Grid->min; //return InterpolateExactFp(r);

  i = (int) ((r-Grid->min)/Grid->step + 0.5);
  if(i >= Grid->size-1) i = Grid->size-1; //return InterpolateExactFp(r);

  dx = r - Grid->x[i];

  return Grid->fp[i] + (Grid->fp[i+1] - Grid->fp[i]) * Grid->I_step * dx;
}

/* executed only if TRIAL_LIM is defined */
#ifdef TRIAL_LIM
DOUBLE InterpolateExactFp(DOUBLE r) {
  if(r>a)
    return a / (r*(r-a));
  else
    return 0.;
}
#endif

/* executed only if TRIAL_POWER is defined */
#ifdef TRIAL_POWER
DOUBLE InterpolateExactFp(DOUBLE r) {
  if(r>a)
    return Rpar*a / (r*(pow(r,Rpar)-a));
  else
    return 0.;
}
#endif

/* executed only if TRIAL_SS is defined */
#ifdef TRIAL_SS
DOUBLE InterpolateExactFp(DOUBLE x) {
  if(x<Apar) {
    if(high_energy) return (sqEkappa / tg(sqEkappa*x) - 1./x);
    else return (sqEkappa / tanh(sqEkappa*x) - 1./x);
  }
  else if(x<Rpar) {
    return sqE  / tg(sqE*x+trial_delta) - 1./x;
  }
  else {
    return A4SW*alpha_1 / (Exp(x*alpha_1) - A4SW);
  }
}
#endif

/* executed only if TRIAL_HS1D is defined */
#ifdef TRIAL_HS1D
DOUBLE InterpolateExactFp(DOUBLE x) {

#ifdef SECURE
  if(x<a) Error("InterpolateExactFp : (r<a) call\n");
#endif

  if(x<Rpar) {
    return sqE / tg(sqE*(x-a));
  }
  else {
#ifdef __WATCOMC__
    if(x*alpha_1<700)
      return A3SW*alpha_1 / (Exp(x*alpha_1) - A3SW);
    else
      return A3SW*alpha_1 / (Exp(700) - A3SW);
#endif
    return A3SW*alpha_1 / (Exp(x*alpha_1) - A3SW);
  }
}
#endif

/* executed only if TRIAL_DIPOLE is defined */
#ifdef TRIAL_DIPOLE
DOUBLE InterpolateExactFp(DOUBLE z) {
  //if(fabs(z)<0.3*sigma_lj) z = 0.3*sigma_lj;
  return Bpar*A1SW*pow(fabs(z), -Bpar-1);
}
#endif

#ifdef TRIAL_SH
DOUBLE InterpolateExactFp(DOUBLE r) {
  return 2./sinh(2*r)-1./r;
}
#endif

#ifdef TRIAL_PSEUDOPOT_BS
DOUBLE InterpolateExactFp(DOUBLE r) {
  return -(sqE + 1./r);
}
#endif

/************************ InterpolateE ************************************/
/* E =  -(f"/f + f'/f*(2/x - f'/f)); */

DOUBLE InterpolateEG11(DOUBLE r) {
#ifdef TRIAL_3D
  return 4.*PI*r*r*InterpolateE11(&G11, r);
#endif

#ifdef TRIAL_2D
  return 2*PI*r*InterpolateE11(&G11, r);
#endif

#ifdef TRIAL_1D
  return 2*InterpolateE11(&G11, r);
#endif
}

DOUBLE InterpolateEG22(DOUBLE r) {
#ifdef TRIAL_3D
  return 4.*PI*r*r*InterpolateE22(&G11, r);
#endif

#ifdef TRIAL_2D
  return 2*PI*r*InterpolateE22(&G11, r);
#endif

#ifdef TRIAL_1D
  return 2*InterpolateE22(&G11, r);
#endif
}

DOUBLE InterpolateEG12(DOUBLE r) {
#ifdef TRIAL_3D
  return 4.*PI*r*r*InterpolateE12(&G12, r);
#endif

#ifdef TRIAL_2D
  return 2*PI*r*InterpolateE12(&G12, r);
#endif

#ifdef TRIAL_1D
  return 2*InterpolateE12(&G12, r);
#endif
}

DOUBLE InterpolateGridE(struct Grid *Grid, DOUBLE r) {
  int i;
  DOUBLE dx;

  if(r<=Grid->min) r = Grid->min; //return InterpolateExactE(r);

  i = (int) ((r-Grid->min)/Grid->step);

  if(i>= Grid->size-2) i = Grid->size-2;//return InterpolateExactE(r);

  dx = r - Grid->x[i];
  return Grid->E[i]+(Grid->E[i+1] - Grid->E[i])*Grid->I_step * dx;
}

/* executed only if TRIAL_LIM is defined */
#ifdef TRIAL_LIM
DOUBLE InterpolateExactE(DOUBLE r) {
  DOUBLE e;

  if(r>a) {
#ifdef TRIAL_3D // 3D case: Eloc = (a / (x(x-a)))^2
    e = a/(r*(r-a));
    return (e*e);
#endif

#ifdef TRIAL_2D // 2D case: Eloc = a / (x (x-a)^2)
    e = r-a;
    return a/(r*e*e);
#endif

#ifdef TRIAL_1D // 1D case: Eloc = a (2x-a) / (x(x-a))^2
    e = r*(r-a);
    return a*(2*r-a)/(e*e);
#endif

  }
  else { // i.e. r <= a
    return 0.;
  }
}
#endif

/* executed only if TRIAL_POWER is defined */
#ifdef TRIAL_POWER
DOUBLE InterpolateExactE(DOUBLE r) {
// E = 1/x^2  na/(x^n-a) [n+2-D + na/(x^n-a)], D=1,2,3
  DOUBLE e;

  if(r>a) {
    e = Rpar*a/(pow(r,Rpar)-a);
#ifdef TRIAL_3D // 3D case:
    return e*(Rpar-1+e)/(r*r);
#endif
#ifdef TRIAL_2D // 2D case:
    return e*(Rpar+e)/(r*r);
#endif
#ifdef TRIAL_1D // 1D case:
    return e*(Rpar+1+e)/(r*r);
#endif
  }
  else { // i.e. r <= a
    return 0.;
  }
}
#endif

/* executed only if TRIAL_SS is defined */
#ifdef TRIAL_SS
DOUBLE InterpolateExactE(DOUBLE x) {
  DOUBLE g, fexp;
  if(x<Apar) {
    if(high_energy) g = sqEkappa / tg(sqEkappa*x) - 1./x;
    else g = sqEkappa / tanh(sqEkappa*x) - 1./x;
    return sqE2 + g *g;
  }
  else if(x<Rpar) {
    g =  sqE  / tg(sqE*x+trial_delta) - 1./x;
    return sqE2 + g*g;
  }
  else {
#ifdef __WATCOMC__
    if(x*alpha_1<700)
      fexp = 1. / (Exp(x*alpha_1)-A4SW);
    else
      fexp = 1. / (Exp(700)-A4SW);
#else
    fexp = 1. / (Exp(x*alpha_1)-A4SW);
#endif
    return A4SW*alpha_1*((1+A4SW*fexp)*alpha_1-2./x)* fexp;
  }
}
#endif

/* executed only if TRIAL_HS1D is defined */
#ifdef TRIAL_HS1D
DOUBLE InterpolateExactE(DOUBLE x) {
/* Eloc1D = -[f"/f - (f'/f)^2]*/
  DOUBLE g, fexp;

#ifdef SECURE
  if(x<a) Error("InterpolateExact E : (r<a) call\n");
#endif

  if(x<Rpar) {
    g = sqE/tg(sqE*(x-a));
    //g = 0.; //Real local energy
    return sqE2 + g*g;
  }
  else {
#ifdef __WATCOMC__
    if(x*alpha_1<700)
      fexp = 1. / (Exp(x*alpha_1)-A3SW);
    else
      fexp = 1. / (Exp(700)-A3SW);
#else
    fexp = 1. / (Exp(x*alpha_1)-A3SW);
#endif
    //return A3SW*alpha_1*alpha_1*Exp(-x*alpha_1)/((1.-A3SW*Exp(-x*alpha_1)));//Real local energy
    return A3SW*alpha_1*alpha_1*(1.+A3SW*fexp)* fexp;
  }
}
#endif

/* Eloc1D = -[f"/f - (f'/f)^2]*/
/* executed only if TRIAL_DIPOLE is defined */
#ifdef TRIAL_DIPOLE
DOUBLE InterpolateExactE(DOUBLE z) {
#ifdef TRIAL_1D
  //if(fabs(z)<0.3*sigma_lj) z = 0.3*sigma_lj; //!
  return Bpar*(Bpar+1)*A1SW*pow(fabs(z), -Bpar-2);
#endif
#ifdef TRIAL_3D
  return Bpar*(Bpar-1)*A1SW*pow(fabs(z), -Bpar-2);
#endif
}
#endif

#ifdef TRIAL_SH
DOUBLE InterpolateExactE(DOUBLE r) {
  DOUBLE fp,e;

  fp = 2./sinh(2*r)-1./r;
  e = 1./cosh(r);
  return 2*e*e + fp*fp;
}
#endif

#ifdef TRIAL_PSEUDOPOT_BS
DOUBLE InterpolateExactE(DOUBLE r) {
  return (2.*sqE + 1./r)/r;
}
#endif

/************************** Construct Grid Fermion ************************/
//  note that a>0 is actually -a
void ConstructGridSquareWellZeroConst(void) {
  static int first_time = ON;
  DOUBLE r;
  DOUBLE mu;
  DOUBLE precision = 1e-8;

  // reduced mass
  if(fabs(m_dn_inv) < precision) {
    mu = m_up;
  }
  else if(fabs(m_up_inv) < precision) {
    mu = m_dn;
  }
  else {
    mu = m_up*m_dn / (m_up + m_dn);
  }

  // kappa = sqrt(2 mu Vo ) / hbar
  trial_Kappa = sqrt(2.*mu*trial_Vo);
  trial_Kappa2 = 2.*mu*trial_Vo;

  if(first_time) {
    Message("  BCS square well zero energy + const, var. par.: Rpar and Bpar\n");
    Warning("  Scattering length must be negative in order to use this w.f.\n");
    Warning("  Sin(K r)/r [r<RoSW], 1+a/r [r<Rpar], 1. + A*(Exp(-Bpar*r)+Exp(Bpar*(r-L))) [r<L/2]\n");
    Warning("  Changing Rpar %"LE" -> %"LE"\n", Rpar, Rpar*Lhalf);
    first_time = OFF;
    Rpar *= Lhalf;
  }

  if(a < 0) {
    Warning("  Automatically changing scattering length to positive value\n");
    a = -a;
  }

  if(RoSW > Rpar) Error(" RoSW = %lf > Rpar = %lf = %lf L/2, must be otherwise, i.e. Rpar/[L/2] > %lf\n", RoSW, Rpar, Rpar/Lhalf, RoSW/Lhalf);

  A3SW = 1./(1.+a/Rpar*(1.-1./(Rpar*Bpar)*(Exp(Bpar*(L-2*Rpar))+1.)/(Exp(Bpar*(L-2*Rpar))-1.)));
  r = RoSW;
  A1SW = A3SW*(1.+a/r) / (Sin(trial_Kappa*r)/r);
  A4SW = A3SW*a/(Rpar*Rpar*Bpar)/(Exp(-Bpar*Rpar)-Exp(Bpar*(Rpar-L)));

  //Message("  Check a = %"LE", goal value %"LE"\n", RoSW*(1.-tg(kappa*R)/(kappa*R)), a);

  r = RoSW;
  r = (trial_Kappa / tan(trial_Kappa*r) - 1. / r) / (-a / (r*r) / (1. + a / r)); // testing continuity of the first derivative
  if(fabs(r-1)>0.1) Error("ConstructGridSquareWellZeroConst: first derivative is discontinuous");
}

#ifdef TRIAL_SQ_WELL_ZERO_CONST
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r<RoSW)
    return Log(A1SW*Sin(trial_Kappa*r)/r);
  else if(r<Rpar)
    return Log(A3SW*(1.+a/r));
  else if(r<Lhalf)
    return Log(1. + A4SW*(Exp(-Bpar*r)+Exp(Bpar*(r-L))));
  else
    return 0;
}

// Fp = f'/f
DOUBLE InterpolateExactFp12(DOUBLE r) {
  if(r<RoSW)
    return trial_Kappa/tan(trial_Kappa*r) - 1./r;
  else if(r<Rpar)
    return -a/(r*r)/(1.+a/r);
  else if(r<Lhalf)
    return A4SW*Bpar*(-Exp(-Bpar*r)+Exp(Bpar*(r-L)))/(1. + A4SW*(Exp(-Bpar*r)+Exp(Bpar*(r-L))));
  else
    return 0.;
}

// Eloc = -(f" +2/r f')/f+(f'/f)^2
DOUBLE InterpolateExactE12(DOUBLE r) {
  DOUBLE fp;

  fp = InterpolateExactFp12(r);
  if(r<RoSW)
    return trial_Kappa2 + fp*fp;
  else if(r<Rpar)
    return fp*fp;
  else if(r<Lhalf)
    return -((Bpar*A4SW*(Exp(Bpar*L)*(-2 + Bpar*r) + Exp(2*Bpar*r)*(2 + Bpar*r)))/((Exp(Bpar*(L + r)) + A4SW*(Exp(Bpar*L) + Exp(2*Bpar*r)))*r)) + fp*fp;
  else
    return 0;
}
#endif

/************************** Construct Grid Square Well Trap ***************/
//  note that a>0 is actually -a
void ConstructGridSquareWellTrap(void) {
  static int first_time = ON;
  DOUBLE r;
  DOUBLE mu;
  DOUBLE precision = 1e-8;

  // reduced mass
  if(fabs(m_dn_inv) < precision) {
    mu = m_up;
  }
  else if(fabs(m_up_inv) < precision) {
    mu = m_dn;
  }
  else {
    mu = m_up*m_dn / (m_up + m_dn);
  }

  // kappa = sqrt(2 mu Vo ) / hbar
  trial_Kappa = sqrt(2.*mu*trial_Vo);
  trial_Kappa2 = 2.*mu*trial_Vo;

  if(first_time) {
    Warning("  Scattering length must be negative in order to use this w.f.\n");
    Warning("  Sin(K r)/r [r<RoSW], 1+a/r\n");
    first_time = OFF;
  }

  if(a < 0) {
    Warning("  Automatically changing scattering length to positive value\n");
    a = -a;
  }

  r = RoSW;
  A3SW = 1.;
  A1SW = A3SW*(r+a)/Sin(trial_Kappa*r); // f(r)
  r = (trial_Kappa / tan(trial_Kappa*r) - 1. / r) / (-a / (r*r) / (1. + a / r)); // testing continuity of the first derivative
  if(fabs(r-1)>0.1) Error("ConstructGridSquareWellZeroConst: first derivative is discontinuous");
}

#ifdef TRIAL_SQ_WELL_TRAP
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r<RoSW)
    return Log(A1SW*Sin(trial_Kappa*r)/r);
  else
    return Log(A3SW*(1.+a/r));
}

// Fp = f'/f
DOUBLE InterpolateExactFp12(DOUBLE r) {
  if(r<RoSW)
    return trial_Kappa/tan(trial_Kappa*r) - 1./r;
  else
    return -a/(r*r)/(1.+a/r);
}

// Eloc = -(f" +2/r f')/f+(f'/f)^2
DOUBLE InterpolateExactE12(DOUBLE r) {
  DOUBLE fp;

  fp = InterpolateExactFp12(r);
  if(r<RoSW)
    return trial_Kappa2 + fp*fp;
  else
    return fp*fp;
}
#endif

/*********************** Construct Grid Square Well BS Trap ***************/
//  note that a>0 is actually -a
void ConstructGridSquareWellBSTrap(void) {
  static int first_time = ON;
  DOUBLE r;
  DOUBLE mu;
  DOUBLE precision = 1e-8;
  DOUBLE xmin,xmax,x,ymin,ymax,y;
  int search = ON;

  if(first_time) {
    Message("  Scattering length must be positive in order to use this w.f.\n");
    Message("  Sin(K r)/r [r<RoSW], exp(-kr)/r\n");
    first_time = OFF;
  }

  // reduced mass
  if(fabs(m_dn_inv) < precision) {
    mu = m_up;
  }
  else if(fabs(m_up_inv) < precision) {
    mu = m_dn;
  }
  else {
    mu = m_up*m_dn / (m_up + m_dn);
  }
  if(fabs(trial_Vo)<1e-8) Warning("  Suspicious SW depth %lf\n", trial_Vo);
  // kappa = sqrt(2 mu Vo) / hbar
  trial_Kappa = sqrt(2.*mu*trial_Vo);
  trial_Kappa2 = 2.*mu*trial_Vo;

  // k - K ctg[K R] = 0  where K = sqrt(kappa^2-k^2)
  // Solve  x/sqrt(1-x^2) * tg[kappa R sqrt(1-x^2)]  + 1 = 0
  //        x = k/kappa
  //        kappa = sqrt(2 mu Vo ) / hbar
  xmin = 1e-6; //1e-6
  if(1. - (0.5*PI / (trial_Kappa*RoSW))*(0.5*PI / (trial_Kappa*RoSW)) > 0.) {
    //trial_Kappa*RoSW*sqrt(1.-x*x) = PI/2  if(1. - (0.5*PI / (trial_Kappa*RoSW))*(0.5*PI / (trial_Kappa*RoSW)) > 0.) {
   xmax = sqrt(1. - (0.5*PI / (trial_Kappa*RoSW))*(0.5*PI / (trial_Kappa*RoSW)));
   xmax *= 1. - 1e-8;
  }
  else {
    Warning("  cannot initialize xmax");
    xmax = 1.-1e-6;
  }

  x = xmin;
  ymin = x/sqrt(1.-x*x)*tg(trial_Kappa*RoSW*sqrt(1.-x*x)) + 1.;
  x = xmax;
  ymax = x/sqrt(1.-x*x)*tg(trial_Kappa*RoSW*sqrt(1.-x*x)) + 1.;
  if(ymin*ymax > 0) 
    Error("R = %"LF" Can't construct solution for the b.s. of square well", RoSW);
  while(search) {
    x = (xmin+xmax)/2.;
    y = x/sqrt(1.-x*x)*tg(trial_Kappa*RoSW*sqrt(1.-x*x)) + 1.;
    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }
    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y = x*tg(trial_Kappa*RoSW*sqrt(1.-x*x))/sqrt(1.-x*x) + 1;
      search = OFF;
    }
  }
  Message("  Solution is k/kappa  = %"LF"  error %"LE"\n", x, y);
  sqEkappa2 = trial_Kappa2*sqrt(1.-x*x);
  sqEkappa = sqrt(sqEkappa2);

  // k - K ctg[K R] = 0  where K = sqrt(kappa^2-k^2)
  // Solve  x tg[kappa R sqrt(1-x^2)] / sqrt(1-x^2) + 1 = 0
  //        x = k/kappa
  //        kappa = sqrt(2 mu Vo ) / hbar

  trial_k = x*trial_Kappa;
  sqEkappa = trial_Kappa*sqrt(1 - x*x);
  Message("  verifying solution: k = %lf, -K ctg[K R] = %lf\n", trial_k, -sqEkappa/ tan(sqEkappa*RoSW));

  sqEkappa2 = trial_Kappa2*(1-x*x);
  r = RoSW;
  if(Sin(trial_k*r)<0) Warning("  Negative w.f.\n");
  A1SW = exp(-trial_k*r - Log(r)) / fabs(Sin(sqEkappa*r)/r);
}

#ifdef TRIAL_SQ_WELL_BS_TRAP
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r<RoSW)
    return Log(A1SW*fabs(Sin(sqEkappa*r))/r);
  else
    return -trial_k*r - Log(r);
}

// Fp = f'/f
DOUBLE InterpolateExactFp12(DOUBLE r) {
  if(r<RoSW)
    return sqEkappa/tan(sqEkappa*r) - 1./r;
  else
    return -(1.+trial_k*r)/r; // -(a+r)/(a*r);
}

// Eloc = -(f" +2/r f')/f+(f'/f)^2
DOUBLE InterpolateExactE12(DOUBLE r) {
  DOUBLE fp;

  if(r<RoSW) {
    fp = InterpolateExactFp12(r);
    return sqEkappa2 + fp*fp;
  }
  else
    return (1.+2.*trial_k*r)/(r*r); //(a+2.*r)/(a*r*r);
}
#endif

/*********************** Construct Grid Square Well BS Trap ***************/
void ConstructGridSquareWellBSLatticePolaron(void) {
  static int first_time = ON;
  DOUBLE r;
  DOUBLE mu;
  DOUBLE precision = 1e-8;
  DOUBLE xmin,xmax,x,ymin,ymax,y;
  DOUBLE rhs;
  int search = ON;

  if(first_time) {
    Message("  sin(K r)/r [r<RoSW], exp(-kr)/r [r < Rpar12] exp(-r/Apar12) +symm \n");
    first_time = OFF;
    if(Rpar12<RoSW) Error("  Rpar12<RoSW, should be other way around");
  }

  // reduced mass
  if(fabs(m_dn_inv) < precision) {
    mu = m_up;
  }
  else if(fabs(m_up_inv) < precision) {
    mu = m_dn;
  }
  else {
    mu = m_up*m_dn / (m_up + m_dn);
  }
  if(fabs(trial_Vo)<1e-8) Warning("  Suspicious SW depth %lf\n", trial_Vo);
  // kappa = sqrt(2 mu Vo) / hbar
  trial_Kappa = sqrt(2.*mu*trial_Vo);
  trial_Kappa2 = 2.*mu*trial_Vo;

  // matching of f'(r)/r(r) at r = Rpar12
  r = Rpar12;
  trial_k = tanh((Lhalf-r)/Apar12)/Apar12 - 1./r;

  // matching of f'(r)/r(r) at r = RoSW
  r = RoSW;
  // sqEkappa*r/tan(sqEkappa*r) = -trial_k*r
  // kappa / tan(kappa r) = - k
  // x / tan(x) = - k*r, x = kappa*r
  rhs = - trial_k * r;
  xmin = 1e-6;
  xmax = PI-1e-6;
  x = xmin;
  ymin = x/tan(x) - rhs;
  x = xmax;
  ymax = x/tan(x) - rhs;
  if(ymin*ymax > 0) 
    Error("Can't construct solution for f'(Rpar12)");
  while(search) {
    x = (xmin+xmax)/2.;
    y = x/tan(x) - rhs;
    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }
    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y = x/tan(x) - rhs;
      search = OFF;
    }
  }
  Message("  Solution is x = k Rpar12  = %"LF"  error %"LE"\n", x, y);
  sqEkappa = x/r;
  sqEkappa2 = sqEkappa*sqEkappa;

  //f(L/2)
  r = Lhalf;
  Ctrial12 = -Log(Exp(-r/Apar12) + Exp(-(L-r)/Apar12));

  r = Rpar12;
  //f (r)
  Btrial12 = Ctrial12 + Log(Exp(-r/Apar12) + Exp(-(L-r)/Apar12)) - (- trial_k*r - Log(r));

  r = RoSW;
  if(Sin(trial_k*r)<0) Warning("  Negative w.f.\n");
  Atrial12 = - Log(fabs(Sin(sqEkappa*r))/r) + Btrial12 - trial_k*r - Log(r);
}

#ifdef TRIAL_SQ_WELL_BS_LATTICE_POLARON
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r<RoSW)
    return Atrial12 + Log(fabs(Sin(sqEkappa*r))/r);
  else if(r<Rpar12)
    return Btrial12 - trial_k*r - Log(r);
  else if(r<Lhalf)
    return Ctrial12 + Log(Exp(-r/Apar12) + Exp(-(L-r)/Apar12));
  else
    return 0.;
}

// Fp = f'/f
DOUBLE InterpolateExactFp12(DOUBLE r) {
  if(r<RoSW)
    return sqEkappa/tan(sqEkappa*r) - 1./r;
  else if(r<Rpar12)
    return -(1.+trial_k*r)/r;
  else if(r<Lhalf)
    return -tanh((Lhalf-r)/Apar12)/Apar12;
  else
    return 0.;
}

// Eloc = -(f" +2/r f')/f+(f'/f)^2
DOUBLE InterpolateExactE12(DOUBLE r) {
  DOUBLE fp;

  if(r<RoSW) {
    fp = InterpolateExactFp12(r);
    return sqEkappa2 + fp*fp;
  }
  else if(r<Rpar12)
    return (1.+2.*trial_k*r)/(r*r);
  else if(r<Lhalf) {
    DOUBLE x;
    x = 1./(cosh((Lhalf-r)/Apar12)*Apar12);
    return 2.*tanh((Lhalf-r)/Apar12)/(Apar12*r) - x*x;
  }
  else
    return 0.;
}
#endif

/************************** Construct Grid Fermion ************************/
void ConstructGridSquareWellUnitarity(void) {
  static int first_time = ON;
  DOUBLE r;

  //trial_Kappa = sqrt(trial_Vo);
  trial_Kappa = sqrt(2.*m_mu*trial_Vo);
  trial_Kappa2 = 2.*m_mu*trial_Vo;

  if(first_time) {
    Message("  square well zero energy at unitarity + exponents: Rpar and Apar\n");
    Warning("  Scattering length must be infinite\n");
    Warning("  A1 Sin(K r)/r [r<Ro], A2 1/r [r<Rpar], A3 + A4 (Exp(-Apar*r) + Exp(Apar*(r-L))) [r<L/2]\n");
    Warning("  Changing Rpar %"LE" -> %"LE"\n", Rpar, Rpar*Lhalf);
    first_time = OFF;
    Rpar *= Lhalf;
  }

  Message("  at unitarity Kappa = %lf = %lf", trial_Kappa, PI/(2.*Ro));
  if(fabs(PI/(2.*Ro) - trial_Kappa) > 1e-3) Error("  for TRIAL_SQ_WELL_UNITARITY wave function 'a' should be infinite");

  A4SW = 1./(2./Exp((Apar*L)/2.) + (-1 + Apar*Rpar)/Exp(Apar*Rpar) - Exp(Apar*(-L + Rpar))*(1 + Apar*Rpar));
  A3SW = (A4SW*(Exp(Apar*L)*(-1. + Apar*Rpar) - Exp(2.*Apar*Rpar)*(1. + Apar*Rpar)))/Exp(Apar*(L + Rpar));
  r = Rpar;
  A2SW = (A3SW + A4SW*(Exp(-Apar*r)+Exp(Apar*(r-L))))*r;
  r = Ro;
  A1SW = A2SW / Sin(trial_Kappa*r);

  if(A1SW<0) Error("negative Jastrow w.f.");
}

#ifdef TRIAL_SQ_WELL_UNITARITY
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r<Ro)
    return Log(A1SW*Sin(trial_Kappa*r)/r);
  else if(r<Rpar)
    return Log(A2SW/r);
  else if(r<Lhalf)
    return Log(A3SW + A4SW*(Exp(-Apar*r)+Exp(Apar*(r-L))));
  else
    return 0;
}

// Fp = f'/f
DOUBLE InterpolateExactFp12(DOUBLE r) {
  if(r<Ro)
    return trial_Kappa/tan(trial_Kappa*r) - 1./r;
  else if(r<Rpar)
    return -1./r;
  else if(r<Lhalf)
    return (A4SW*Apar*(-Exp(Apar*L) + Exp(2.*Apar*r)))/(A3SW*Exp(Apar*(L + r)) + A4SW*(Exp(Apar*L) + Exp(2.*Apar*r)));
  else
    return 0.;
}

// Eloc = -(f" +2/r f')/f+(f'/f)^2
DOUBLE InterpolateExactE12(DOUBLE r) {
  DOUBLE fp;

  fp = InterpolateExactFp12(r);
  if(r<Ro)
    return trial_Kappa2 + fp*fp;
  else if(r<Rpar)
    return fp*fp;
  else if(r<Lhalf)
    return -((A4SW*Apar*(A3SW*Exp(Apar*(2.*L + r))*(-2. + Apar*r) + A3SW*Exp(Apar*(L + 3.*r))*(2. + Apar*r) + 2.*A4SW*(-Exp(2.*Apar*L) + Exp(4.*Apar*r) + 2.*Apar*Exp(Apar*(L + 2.*r))*r)))/((A3SW*Exp(Apar*(L + r)) + A4SW*(Exp(Apar*L) + Exp(2.*Apar*r)))*(A3SW*Exp(Apar*(L + r)) + A4SW*(Exp(Apar*L) + Exp(2.*Apar*r)))*r));
  else
    return 0;
}
#endif

/************************** Construct Grid Fermion ************************/
DOUBLE Gauss1122_A;
void ConstructGridGauss1122(void) {
  Gauss1122_A = 1.+Gamma*2.*Exp(-beta*Lhalf*Lhalf);
}

#ifdef TRIAL_GAUSS1122
DOUBLE InterpolateExactU11(DOUBLE r) {
  if(r>Lhalf)
    return 1.;
  else
    return Log(Gauss1122_A-Gamma*(Exp(-beta*r*r)+Exp(-beta*(L-r)*(L-r))));
}

// Fp = f'/f
DOUBLE InterpolateExactFp11(DOUBLE r) {
  DOUBLE e1,e2,f;
  if(r>Lhalf)
    return 0.;
  else {
    e1 = Exp(-beta*r*r);
    e2 = Exp(-beta*(L-r)*(L-r));
    f = Gauss1122_A-Gamma*(e1+e2);
    //return 2*beta*Gamma*r/(Exp(beta*r*r)-Gamma);
    return 2.*beta*Gamma*(r*e1-(L-r)*e2)/f;
  }
}

// Eloc = -(f" +2/r f')/f+(f'/f)^2
DOUBLE InterpolateExactE11(DOUBLE r) {
  DOUBLE e1,e2,f,fp,fpp;

  if(r>Lhalf)
    return 0;
  else {
    //ex = Exp(beta*r*r);
    //return 2*beta*Gamma*(ex*(2*beta*r*r-3)+3*Gamma)/((ex-Gamma)*(ex-Gamma));
    e1 = Exp(-beta*r*r);
    e2 = Exp(-beta*(L-r)*(L-r));
    f = Gauss1122_A-Gamma*(e1+e2);
    fp = 2.*beta*Gamma*(r*e1-(L-r)*e2);
    fpp = 2.*beta*Gamma*(e1+e2-2.*beta*(r*r*e1+(L-r)*(L-r)*e2));
    fp /= f;
    fpp /= f;
    return -(fpp +2./r*fp)+fp*fp;
  }
}

DOUBLE InterpolateExactU22(DOUBLE r) {
  if(r>Lhalf) r = Lhalf;
  return Log((1.-Gamma*Exp(-beta*r*r))/(1.-Gamma*Exp(-beta*Lhalf2)));
}

// Fp = f'/f
DOUBLE InterpolateExactFp22(DOUBLE r) {
  if(r>Lhalf) r = Lhalf;
  return 2*beta*Gamma*r/(Exp(beta*r*r)-Gamma);
}

// Eloc = -(f" +2/r f')/f+(f'/f)^2
DOUBLE InterpolateExactE22(DOUBLE r) {
  DOUBLE ex;

  if(r>Lhalf) r = Lhalf;
  ex = Exp(beta*r*r);
  return 2*beta*Gamma*(ex*(2*beta*r*r-3)+3*Gamma)/((ex-Gamma)*(ex-Gamma));
}
#endif

#ifdef TRIAL_GAUSS12
DOUBLE InterpolateExactU12(DOUBLE r) {
  return Log((1.-Gamma*Exp(-beta*r*r))/(1.-Gamma*Exp(-beta*Lhalf2)));
}

// Fp = f'/f
DOUBLE InterpolateExactFp12(DOUBLE r) {
  return 2.*beta*Gamma*r/(Exp(beta*r*r)-Gamma);
}

// Eloc = -(f" +2/r f')/f+(f'/f)^2
DOUBLE InterpolateExactE12(DOUBLE r) {
  DOUBLE ex;

  ex = Exp(beta*r*r);
  return 2.*beta*Gamma*(ex*(2.*beta*r*r-3)+3*Gamma)/((ex-Gamma)*(ex-Gamma));
}
#endif
 
#ifdef TRIAL_GAUSSIAN12 // Exp(-beta r^2)
DOUBLE InterpolateExactU12(DOUBLE r) {
  return -beta*r*r;
}

// Fp = f'/f
DOUBLE InterpolateExactFp12(DOUBLE r) {
  return -2.*beta*r;
}

// Eloc = -(f" + (D-1)/r f')/f+(f'/f)^2
DOUBLE InterpolateExactE12(DOUBLE r) {
#ifdef TRIAL_3D
  return 6.*beta;
#endif
#ifdef TRIAL_2D
  return 4.*beta;
#endif
#ifdef TRIAL_1D
  return 2.*beta;
#endif
}
#endif

/***************************** Simpson Integration ***************************/
DOUBLE IntegrateSimpson(DOUBLE xmin, DOUBLE xmax, DOUBLE (*f)(DOUBLE x), long int Npoints, int dimensionality) {
// 1/3, 4/3, 2/3, ..., 2/3, 4/3, 1/3
  DOUBLE dx, x;
  DOUBLE I = 0.;
  long int i;

  long int N;

#ifdef HARD_SPHERE12
  if(xmin<a) {
    Warning(" Integrate Simpson xmin<a call");
    xmin = a*1.01;
  }
#endif

  N = (Npoints/2)*2+1; // N should be even
  dx = (xmax-xmin)/ (DOUBLE) (N-1);

  if(dimensionality == 1)
    I = f(xmin) + 4. * f(xmin+dx);
  else if(dimensionality == 2)
    I = xmin*f(xmin) + 4.*(xmin+dx)*f(xmin+dx);
  else 
    I = xmin*xmin*f(xmin) + 4.*(xmin+dx)*(xmin+dx)*f(xmin+dx);

  for(i=2; i<N-2; i += 2) {
    x = xmin + dx*(DOUBLE) i;

    if(dimensionality == 1) {
      I += 2. * f(x);
      I += 4. * f(x+dx);
    }
    else if(dimensionality == 2) {
      I += 2. * x*f(x);
      I += 4. * (x+dx)*f(x+dx);
    }
    else {
      I += 2. * x*x*f(x);
      I += 4. * (x+dx)*(x+dx)*f(x+dx);
    }
  }

  if(dimensionality == 1) {
    I += f(x+2.*dx);
  }
  else if(dimensionality == 2) {
    I += (x+2.*dx)*f(x+2.*dx);
  }
  else {
    I += (x+2.*dx)*(x+2.*dx)*f(x+2.*dx);
  }

  I *= dx / 3.;

  if(dimensionality == 1) {
  }
  else if(dimensionality == 2) {
    I *= 2.*PI;
  }
  else {
    I *= 4.*PI;
  }
  return I;
}

/************************** Construct Grid Fermion ************************/
void ConstructGridSquareWell2D(void) {
  static int first_time = ON;
  DOUBLE f1,f2;
  DOUBLE xmin, xmax, ymin, ymax, x, y;
  int search = ON;
  DOUBLE precision = 1e-8;
  DOUBLE precision_in_wf = 1e-6;

  //trial_Kappa = sqrt(trial_Vo);
  trial_Kappa = sqrt(2.*m_mu*trial_Vo);
  trial_Kappa2 = trial_Vo;

  if(first_time) {
    Message("  2D square well \n");
    first_time = OFF;
  }

  // define trial_k
  // Solve: k K1(k)/K0(k) -  ... = 0
  xmin = 1e-5;
  xmax = sqrt(trial_Vo)-1e-5;
  x = xmin;
  ymin = x*BesselK1(x)/BesselK0(x) - sqrt(trial_Vo-x*x)*BesselY1(sqrt(trial_Vo-x*x))/BesselY0(sqrt(trial_Vo-x*x));
  x = xmax;
  ymax = x*BesselK1(x)/BesselK0(x) - sqrt(trial_Vo-x*x)*BesselY1(sqrt(trial_Vo-x*x))/BesselY0(sqrt(trial_Vo-x*x));

  Message("  Solving equation for Bpar ... \n");
  if(ymin*ymax > 0) Error("Can't construct the grid : No solution found");
  while(search) {
    x = (xmin+xmax)/2.;
    y =  x*BesselK1(x)/BesselK0(x) - sqrt(trial_Vo-x*x)*BesselY1(sqrt(trial_Vo-x*x))/BesselY0(sqrt(trial_Vo-x*x));
    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }
    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y =  x*BesselK1(x)/BesselK0(x) - sqrt(trial_Vo-x*x)*BesselY1(sqrt(trial_Vo-x*x))/BesselY0(sqrt(trial_Vo-x*x));
      search = OFF;
    }
  }
  Message("  Solution is scattering momentum k = %"LF", error %"LE"\n", x, y);
  trial_k = x;
  trial_k2 = trial_k*trial_k;
  //trial_Kappa = sqrt(trial_Vo-trial_k2);
  trial_Kappa = sqrt(2.*m_mu*trial_Vo - trial_k2);
  trial_Kappa2 = 2.*m_mu*trial_Vo - trial_k2;

  if(BesselK0(trial_k*Lhalf) == 0) Error(" K0(%lf) = 0", trial_k*Lhalf);

  A2SW = 1./BesselK0(trial_k*Lhalf);
  A1SW = A2SW*BesselK0(trial_k) / BesselY0(trial_Kappa);

  Message("  Testing continuity ... ");
  f1 = InterpolateU12(&G12, 1.-1e-8);
  f2 = InterpolateU12(&G12, 1.+1e-8);
  if(fabs(f1/f2-1.)>precision_in_wf) Error("  discontinuity at f(1): %"LG" and %"LG"\n", f1, f2);

  f1 = InterpolateFp12(&G12, 1.-1e-8);
  f2 = InterpolateFp12(&G12, 1.+1e-8);
  if(fabs(f1/f2-1.)>precision_in_wf) Error("  discontinuity at f'(1): %"LG" and %"LG"\n", f1, f2);

  f1 = InterpolateU12(&G12, 1.-1e-8);
  f2 = InterpolateU12(&G12, 1.+1e-8);
  if(fabs(f1/f2-1.)>precision_in_wf) Error("  discontinuity at f(1): %"LG" and %"LG"\n", f1, f2);

  f1 = InterpolateFp12(&G12, Lhalf-1e-8);
  f2 = InterpolateFp12(&G12, Lhalf+1e-8);
  if(fabs(f1/f2-1.)>precision_in_wf) Warning("  discontinuity at f'(L/2): %"LG" and %"LG"\n", f1, f2);
  Message("done\n");
}

#ifdef TRIAL_SQUARE_WELL2D
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r<Ro)
    return Log(A1SW*BesselY0(trial_Kappa*r));
#ifdef SYMMETRIZE_TRIAL_WF12
  return Log(A2SW*BesselK0(trial_k*r));
#else
  else if(r<Lhalf)
    return Log(A2SW*BesselK0(trial_k*r));
  else
    return 0;
#endif
}

// Fp = f'/f
DOUBLE InterpolateExactFp12(DOUBLE r) {
  if(r<Ro)
    return -trial_Kappa*BesselY1(trial_Kappa*r)/BesselY0(trial_Kappa*r);
#ifdef SYMMETRIZE_TRIAL_WF12
    return -trial_k*BesselK1(trial_k*r)/BesselK0(trial_k*r);
#else
  else if(r<Lhalf)
    return -trial_k*BesselK1(trial_k*r)/BesselK0(trial_k*r);
  else
    return 0;
#endif
}

// Eloc = -(f" +1/r f')/f+(f'/f)^2
DOUBLE InterpolateExactE12(DOUBLE r) {
  DOUBLE fp;

  if(r<Ro) {
    fp = InterpolateExactFp12(r);
    return trial_Kappa2 + fp*fp;
  }
#ifdef SYMMETRIZE_TRIAL_WF12
  fp = InterpolateExactFp12(r);
  return -trial_k2 + fp*fp;
#else
  else if(r<Lhalf) {
    fp = InterpolateExactFp12(r);
    return -trial_k2 + fp*fp;
  }
  else
    return 0;
#endif
}
#endif

#ifdef TRIAL_COULOMB12
// Exp(-r/2a)
DOUBLE InterpolateExactU12(DOUBLE r) {
  return -r/(2.*excitonRadius);
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  return -0.5/excitonRadius;
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  return 0.5*(DIMENSION-1.)/(excitonRadius*r);
}
#endif

#ifdef TRIAL_COULOMB12ex
// r Exp(-r/2a)
DOUBLE InterpolateExactU12(DOUBLE r) {
  return Log(r)-r/(2.*excitonRadius);
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  return 1./r-0.5/excitonRadius;
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  return 1./(r*r);
}
#endif

/************************** Check Trial Wave Function **********************/
void CheckTrialWaveFunction(void) {
  int Ngrid = 1000;
  DOUBLE tolerance_fp = 1e-3;
  DOUBLE tolerance_Eloc = 1e-2;
  DOUBLE r, dr, epsilon;
  DOUBLE fp, Fp_check, Fp_exact;
  DOUBLE fpp, Eloc_check, Eloc_exact;
  int i;

#ifdef HARD_SPHERE11
  G11.min = aA+1e-5;
#endif
#ifdef HARD_SPHERE22
  G22.min = aB+1e-5;
#endif
#ifdef HARD_SPHERE12
  G12.min = a+1e-5;
#endif
  if(G11.step == 0) {
    G11.step = 1e-5;
    G11.I_step = 1./G11.step;
  }
  if(G12.step == 0) {
    G12.step = 1e-5;
    G12.I_step = 1./G12.step;
  }

#ifndef DONT_CHECK_CONTINUITY_OF_WF
  Message("\nTesting grids (up-up)...\n    f(R=%"LE") ... ", Rpar);
  if(fabs(Exp(InterpolateU11(&G11, Rpar-1e-10))-Exp(InterpolateU11(&G11, Rpar+1e-10)))>1e-4*Exp(InterpolateU11(&G11, Rpar-1e-10)))
      Error("f(R-0) = %"LF", f(R+0) = %"LF"\n", Exp(InterpolateU11(&G11, Rpar-1e-10)),
      Exp(InterpolateU11(&G11, Rpar+1e-10)));
  else Message("done\n    f'(R) ... ");
  if(fabs(InterpolateFp11(&G11, Rpar-1e-10)-InterpolateFp11(&G11, Rpar+1e-10))>1e-2) 
      Error("f'(R-0) = %"LF", f'(R+0) = %"LF"\n", InterpolateFp11(&G11, Rpar-1e-10), InterpolateFp11(&G11, Rpar+1e-10));
  else Message("done\n");
#ifndef DONT_CHECK_CONTINUITY_OF_ELOC
  Message("    Eloc(R) ... ");
  if(fabs(InterpolateE11(&G11, Rpar-1e-10)+InteractionEnergy(Rpar-1e-10)-InterpolateE11(&G11, Rpar+1e-10)-InteractionEnergy(Rpar+1e-10))>1e-2*(InterpolateE(&G11, Rpar-1e-10)+InteractionEnergy11(Rpar-1e-10)))
    Error("Eloc(R-0) = %"LF", Eloc(R+0) = %"LF"\n", InterpolateE11(&G11, Rpar-1e-10)+InteractionEnergy11(Rpar-1e-10), InterpolateE(&G11, Rpar+1e-10)+InteractionEnergy11(Rpar+1e-10));
  else
     Message("done\n");
#endif
     Message("done\n\n");

  Message("\nTesting grids (up-dn)...\n    f(R) ... ");
  if(fabs(Exp(InterpolateU12(&G12, Rpar12-1e-10))-Exp(InterpolateU12(&G12, Rpar12+1e-10)))>1e-2)
      Error("f(R-0) = %"LF", f(R+0) = %"LF", Rpar12 = %"LF"\n", Exp(InterpolateU12(&G12, Rpar12-1e-10)), Exp(InterpolateU12(&G12, Rpar12+1e-10)), Rpar12);
  else Message("done\n    f'(R) ... ");
  if(fabs(InterpolateFp12(&G12, Rpar12-1e-10)-InterpolateFp12(&G12, Rpar12+1e-10))>1e-2) 
      Error("f'(R-0) = %"LF", f'(R+0) = %"LF"\n", InterpolateFp12(&G12, Rpar12-1e-10), InterpolateFp12(&G12, Rpar12+1e-10));
  else Message("done\n");
#ifndef DONT_CHECK_CONTINUITY_OF_ELOC
  Message("    Eloc(R) ... ");
  if(fabs(InterpolateE12(&G12, Rpar12-1e-10)+InteractionEnergy(Rpar12-1e-10)-InterpolateE12(&G12, Rpar12+1e-10)-InteractionEnergy(Rpar12+1e-10))>1e-2*(InterpolateE(&G12, Rpar12-1e-10)+InteractionEnergy12(Rpar12-1e-10)))
    Error("Eloc(R-0) = %"LF", Eloc(R+0) = %"LF"\n", InterpolateE12(&G12, Rpar12-1e-10)+InteractionEnergy(Rpar12-1e-10), InterpolateE(&G12, Rpar12+1e-10)+InteractionEnergy12(Rpar12+1e-10));
  else
     Message("done\n");
#endif
     Message("done\n\n");
#endif

  Message("\nTesting grids using discrete calculation of derivatives... ");

  dr = (Lhalf-G12.min)/(DOUBLE)Ngrid;
  Message("\n    orbital: first derivative (second order difference method), %"LG" <r<%"LG" ... ", G12.min+dr, G12.min+dr*(Ngrid+1));
  epsilon = 1e-4;
  for(i=0; i<Ngrid; i++) {
    r = G12.min+dr*(i+1);
    // f' = (f(i+1)-f(i-1)) / (2 dx)
    Fp_check = (orbitalF(r+epsilon)-orbitalF(r-epsilon)) / (2.*epsilon);
    Fp_exact = orbitalFp(r);
    if(fabs(Fp_check/Fp_exact-1.)>tolerance_fp) {
      Warning(" r=%.3"LG" (r/L=%.3"LG"): f'(r)=%.2"LG" (exact) %.2"LG" (test), Error %2.1"LG"%%\n", r, r/L, Fp_exact, Fp_check, fabs(Fp_check/Fp_exact-1.)*100);
    }
  }

  Message("\n    orbital: local energy, %"LG" <r<%"LG" ...", G12.min+dr, G12.min+dr*(Ngrid+1));
  for(i=0; i<Ngrid; i++) {
    r = G12.min+dr*(i+1);
    //fp = (orbitalF(r+epsilon)-orbitalF(r-epsilon)) / (2.*epsilon);
    //fpp = (orbitalF(r+epsilon)-2.*orbitalF(r)+orbitalF(r-epsilon)) / (epsilon*epsilon);
    fp = orbitalFp(r);
    fpp = (orbitalFp(r+epsilon)-orbitalFp(r-epsilon)) / (2*epsilon);
#ifdef TRIAL_1D // 1D Eloc = [-f"/f] + (f'/f)^2
    Eloc_check = fpp;
#endif
#ifdef TRIAL_2D // 2D Eloc = [-(f" + f'/r)/f] + (f'/f)^2
    Eloc_check = fpp + fp/r;
#endif
#ifdef TRIAL_3D // 3D Eloc = [-(f"+2f'/r)/f] + (f'/f)^2
    Eloc_check = fpp + 2.*fp/r;
#endif
    Eloc_exact = orbitalEloc(r);

    if(fabs(Eloc_check/Eloc_exact-1.)>tolerance_Eloc && Eloc_check!= 0. && Eloc_exact!= 0 && fabs(Eloc_check/fpp)>1e-4) {
      Warning("   r=%.3"LG" (r/L=%.3"LG"): E(r)=%.2"LG" (exact) %.2"LG" (test), Error %2.1g%%\n", r, r/L, Eloc_exact, Eloc_check, fabs(Eloc_check/Eloc_exact-1.)*100);
    }
  }

  Message("\n    Jastrow 11: first derivative (second order difference method), %"LG" <r<%"LG" ...", G11.min+dr, G11.min+dr*(Ngrid+1));
  dr = (Lhalf-G11.min)/(DOUBLE)Ngrid;
  epsilon = 1e-3;
  for(i=0; i<Ngrid; i++) {
    r = G11.min+dr*(i+1);
    // f' = (f(i+1)-f(i-1)) / (2 dx)
    Fp_check = (InterpolateU11(&G11, r+epsilon)-InterpolateU11(&G11, r-epsilon)) / (2.*epsilon);
    Fp_exact = InterpolateFp11(&G11, r);
    if(fabs(Fp_check/Fp_exact-1.)>tolerance_fp) {
      Warning(" r=%.3"LG" (r/L=%.3"LG"): f'(r)=%.2"LG" (exact) %.2"LG" (test), Error %2.1"LG"%%\n", r, r/L, Fp_exact, Fp_check, fabs(Fp_check/Fp_exact-1.)*100);
    }
  }

  Message("\n    Jastrow 11: local energy, %"LG" <r<%"LG" ...", G11.min+dr, G11.min+dr*(Ngrid+1));
  for(i=0; i<Ngrid; i++) {
    r = G11.min+dr*(i+1);
    fp = (InterpolateU11(&G11, r+epsilon)-InterpolateU11(&G11, r-epsilon)) / (2.*epsilon);
    fpp = (InterpolateU11(&G11, r+epsilon)-2.*InterpolateU11(&G11, r)+InterpolateU11(&G11, r-epsilon)) / (epsilon*epsilon) + fp*fp;
#ifdef TRIAL_1D // 1D Eloc = [-f"/f] + (f'/f)^2
    Eloc_check = -fpp + fp*fp;
#endif
#ifdef TRIAL_2D // 2D Eloc = [-(f" + f'/r)/f] + (f'/f)^2
    Eloc_check = -(fpp + fp/r) + fp*fp;
#endif
#ifdef TRIAL_3D // 3D Eloc = [-(f"+2f'/r)/f] + (f'/f)^2
    Eloc_check = -(fpp + 2*fp/r) + fp*fp;
#endif
    Eloc_exact = InterpolateE11(&G11, r);

    if(fabs(Eloc_check/Eloc_exact-1.)>tolerance_Eloc && Eloc_check!= 0. && Eloc_exact!= 0) {
      Warning("    r=%.3"LE " (r/L=%.3"LG"): E(r)=%.2"LE " (exact) %.2"LE " (test), Error %2.1g%%\n", r, r/L, Eloc_exact, Eloc_check, fabs(Eloc_check/Eloc_exact-1.)*100);
    }
  }

  dr = (Lhalf-G12.min)/(DOUBLE)Ngrid;
  Message("\n    Jastrow 12: first derivative (second order difference method), %"LG" <r<%"LG" ...", G12.min+dr, G12.min+dr*(Ngrid+1));
  epsilon = 1e-3;
  for(i=0; i<Ngrid; i++) {
    r = G12.min+dr*(i+1);
    // f' = (f(i+1)-f(i-1)) / (2 dx)
    Fp_check = (InterpolateU12(&G12, r+epsilon)-InterpolateU12(&G12, r-epsilon)) / (2.*epsilon);
    Fp_exact = InterpolateFp12(&G12, r);
    if(fabs(Fp_check/Fp_exact-1.)>tolerance_fp) {
      Warning("r=%.3"LG" (r/L=%.3"LG"): f'(r)=%.2"LG" (exact) %.2"LG" (test), Error %2.1"LG"%%\n", r, r/L, Fp_exact, Fp_check, fabs(Fp_check/Fp_exact-1.)*100);
    }
  }

  Message("\n    Jastrow 12: local energy, %"LG" <r<%"LG" ...", G12.min+dr, G12.min+dr*(Ngrid+1));
  for(i=0; i<Ngrid; i++) {
    r = G12.min+dr*(i+1);
    fp = (InterpolateU12(&G12, r+epsilon)-InterpolateU12(&G12, r-epsilon)) / (2.*epsilon);
    fpp = (InterpolateU12(&G12, r+epsilon)-2.*InterpolateU12(&G12, r)+InterpolateU12(&G12, r-epsilon)) / (epsilon*epsilon) + fp*fp;
#ifdef TRIAL_1D // 1D Eloc = [-f"/f] + (f'/f)^2
    Eloc_check = -fpp + fp*fp;
#endif
#ifdef TRIAL_2D // 2D Eloc = [-(f" + f'/r)/f] + (f'/f)^2
    Eloc_check = -(fpp + fp/r) + fp*fp;
#endif
#ifdef TRIAL_3D // 3D Eloc = [-(f"+2f'/r)/f] + (f'/f)^2
    Eloc_check = -(fpp + 2*fp/r) + fp*fp;
#endif
    Eloc_exact = InterpolateE12(&G12, r);

    if(fabs(Eloc_check/Eloc_exact-1.)>tolerance_Eloc && Eloc_check!= 0. && Eloc_exact!= 0) {
      Warning("    r=%.3"LE " (r/L=%.3"LG"): E(r)=%.2"LE " (exact) %.2"LE " (test), Error %2.1g%%\n", r, r/L, Eloc_exact, Eloc_check, fabs(Eloc_check/Eloc_exact-1.)*100);
    }
  }

  Message("\ndone\n\n");
}

/************************** Construct Grid Fermion ************************/
void ConstructGridSquareWellZeroConst11(void) {
  static int first_time = ON;

  //trial11_Kappa = sqrt(trial11_Vo);
  trial11_Kappa = sqrt(m_up*trial11_Vo);
  trial11_Kappa2 = trial11_Vo;

  if(first_time) {
    Message("  BCS square well zero energy + const, var. par.: Rpar and Bpar\n");
    Warning("  Scattering length must be negative in order to use this w.f.\n");
    Warning("  Sin(K r)/r [r<Ro], 1+a/r [r<Rpar], 1. + A*(Exp(-Bpar*r)+Exp(Bpar*(r-L))) [r<L/2]\n");
    Warning("  Changing Rpar %"LE" -> %"LE"\n", Rpar, Rpar*Lhalf);
    first_time = OFF;
    Rpar *= Lhalf;
  }

  if(ap<0) {
    Warning("  Jastrow11: inverting ap to ap>0\n");
    ap = -ap; 
  }

  A3SW11 = 1./(1.+ap/Rpar*(1.-1./(Rpar*Bpar)*(Exp(Bpar*(L-2*Rpar))+1.)/(Exp(Bpar*(L-2*Rpar))-1.)));
  A1SW11 = A3SW11*(1.+ap)/Sin(trial11_Kappa);
  A4SW11 = A3SW11*ap/(Rpar*Rpar*Bpar)/(Exp(-Bpar*Rpar)-Exp(Bpar*(Rpar-L)));
}

#ifdef TRIAL11_SQ_WELL_ZERO_CONST
DOUBLE InterpolateExactU11(DOUBLE r) {
  if(r<Ro)
    return Log(A1SW11*Sin(trial11_Kappa*r)/r);
  else if(r<Rpar)
    return Log(A3SW11*(1.+ap/r));
  else if(r<Lhalf)
    return Log(1. + A4SW11*(Exp(-Bpar*r)+Exp(Bpar*(r-L))));
  else
    return 0;
}

// Fp = f'/f
DOUBLE InterpolateExactFp11(DOUBLE r) {
  if(r<Ro)
    return trial11_Kappa/tan(trial11_Kappa*r) - 1./r;
  else if(r<Rpar)
    return -ap/(r*r)/(1.+ap/r);
  else if(r<Lhalf)
    return A4SW11*Bpar*(-Exp(-Bpar*r)+Exp(Bpar*(r-L)))/(1. + A4SW11*(Exp(-Bpar*r)+Exp(Bpar*(r-L))));
  else
    return 0.;
}

// Eloc = -(f" +2/r f')/f+(f'/f)^2
DOUBLE InterpolateExactE11(DOUBLE r) {
  DOUBLE fp;

  fp = InterpolateExactFp11(r);
  if(r<Ro)
    return trial11_Kappa2 + fp*fp;
  else if(r<Rpar)
    return fp*fp;
  else if(r<Lhalf)
    return -((Bpar*A4SW11*(Exp(Bpar*L)*(-2 + Bpar*r) + Exp(2*Bpar*r)*(2 + Bpar*r)))/((Exp(Bpar*(L + r)) + A4SW11*(Exp(Bpar*L) + Exp(2*Bpar*r)))*r)) + fp*fp;
  else
    return 0;
}

DOUBLE InterpolateExactU22(DOUBLE r) {
  if(r<Ro)
    return Log(A1SW11*Sin(trial11_Kappa*r)/r);
  else if(r<Rpar)
    return Log(A3SW11*(1.+ap/r));
  else if(r<Lhalf)
    return Log(1. + A4SW11*(Exp(-Bpar*r)+Exp(Bpar*(r-L))));
  else
    return 0;
}

// Fp = f'/f
DOUBLE InterpolateExactFp22(DOUBLE r) {
  if(r<Ro)
    return trial11_Kappa/tan(trial11_Kappa*r) - 1./r;
  else if(r<Rpar)
    return -ap/(r*r)/(1.+ap/r);
  else if(r<Lhalf)
    return A4SW11*Bpar*(-Exp(-Bpar*r)+Exp(Bpar*(r-L)))/(1. + A4SW11*(Exp(-Bpar*r)+Exp(Bpar*(r-L))));
  else
    return 0.;
}

// Eloc = -(f" +2/r f')/f+(f'/f)^2
DOUBLE InterpolateExactE22(DOUBLE r) {
  DOUBLE fp;

  fp = InterpolateExactFp22(r);
  if(r<Ro)
    return trial11_Kappa2 + fp*fp;
  else if(r<Rpar)
    return fp*fp;
  else if(r<Lhalf)
    return -((Bpar*A4SW11*(Exp(Bpar*L)*(-2 + Bpar*r) + Exp(2*Bpar*r)*(2 + Bpar*r)))/((Exp(Bpar*(L + r)) + A4SW11*(Exp(Bpar*L) + Exp(2*Bpar*r)))*r)) + fp*fp;
  else
    return 0;
}
#endif

/************************** Construct Grid Fermion ************************/
void ConstructGridSquareWellFreeStateGauss11(struct Grid *G) {
  DOUBLE kappa, k;
  DOUBLE V,y,ymin,ymax,x,xmin,xmax;
  DOUBLE precision = 1e-10;
  DOUBLE trial_delta_check;
  int search = ON;
  //DOUBLE Lhalf = 15;

  // matching conditions
  V = Lhalf - 1;
  //kappa = sqrt(trial_Vo);
  kappa = sqrt(m_up*trial_Vo);
  xmin = 1e-4;
  xmax = 4;
  ymin = (arctg(xmin*Lhalf) - arctg(xmin/sqrt(xmin*xmin+kappa*kappa)*tg(sqrt(xmin*xmin+kappa*kappa))))/xmin - V;
  ymax = (arctg(xmax*Lhalf) - arctg(xmax/sqrt(xmax*xmax+kappa*kappa)*tg(sqrt(xmax*xmax+kappa*kappa))))/xmax - V;

  if(ymin*ymax > 0) Error("Can't construct the grid : No solution found");
  while(search) {
    x = (xmin+xmax)/2.;
    y = (arctg(x*Lhalf) - arctg(x/sqrt(x*x+kappa*kappa)*tg(sqrt(x*x+kappa*kappa))))/x - V;

    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }

    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      y = (arctg(x*Lhalf) - arctg(x/sqrt(x*x+kappa*kappa)*tg(sqrt(x*x+kappa*kappa))))/x - V;
      search = OFF;
    }
  }
  k = x;

  Message("  Solution k = %"LE", error %"LE"\n", k, y);

  sqE = k;
  sqE2 = k*k;
  trial_Vo = kappa*kappa/m_up;
  trial_Kappa2 = k*k + kappa*kappa;
  trial_Kappa = sqrt(trial_Kappa2);

  trial_delta = arctg(k*Lhalf) - k*Lhalf;
  trial_delta_check = arctg(k/trial_Kappa*tg(trial_Kappa)) - k;
  if(fabs((trial_delta_check-trial_delta)/trial_delta)>1e-8) Error("  Invalid solution\n");

  A3SW = Lhalf/Sin(k*Lhalf+trial_delta);
  A1SW = A3SW*Sin(k+trial_delta)/Sin(trial_Kappa);

  //Xi = A1SW*Sin(trial_Kappa);  Xi = A3SW*Sin(k+trial_delta);
  Message(" Matching to one -> %"LF"", A3SW*Sin(k*Lhalf+trial_delta)/Lhalf);

  G->min = 0;
  G->max = Lhalf;
  G->max2 = G->max*G->max;
  Rpar = 1.;
}

#ifdef TRIAL11_SQUARE_WELL_FS_GAUSS
DOUBLE InterpolateExactU11(DOUBLE r) {
  if(r<Ro)
    return Log(fabs(A1SW*Sin(trial_Kappa*r)/r));
  else if(r<Lhalf)
    return Log(fabs(A3SW*Sin(sqE*r+trial_delta)/r));
  else
    return 0;
}

DOUBLE InterpolateExactFp11(DOUBLE r) {
  if(r<Ro)
    return trial_Kappa/tg(trial_Kappa*r) - 1./r;
  else if(r<Lhalf)
    return sqE/tg(sqE*r+trial_delta) - 1./r;
  else
    return 0;
}

DOUBLE InterpolateExactE11(DOUBLE r) {
  DOUBLE fp=0;

  if(r<Ro) {
    fp = trial_Kappa/tg(trial_Kappa*r) - 1./r;
    return trial_Kappa2 + fp*fp;
  }
  else if(r<Lhalf) {
    fp = sqE/tg(sqE*r+trial_delta) - 1./r;
    return sqE2 + fp*fp;
  }
  else
    return 0;
}

DOUBLE InterpolateExactU22(DOUBLE r) {
  if(r<Ro)
    return Log(fabs(A1SW*Sin(trial_Kappa*r)/r));
  else if(r<Lhalf)
    return Log(fabs(A3SW*Sin(sqE*r+trial_delta)/r));
  else
    return 0;
}

DOUBLE InterpolateExactFp22(DOUBLE r) {
  if(r<Ro)
    return trial_Kappa/tg(trial_Kappa*r) - 1./r;
  else if(r<Lhalf)
    return sqE/tg(sqE*r+trial_delta) - 1./r;
  else
    return 0;
}

DOUBLE InterpolateExactE22(DOUBLE r) {
  DOUBLE fp=0;

  if(r<Ro) {
    fp = trial_Kappa/tg(trial_Kappa*r) - 1./r;
    return trial_Kappa2 + fp*fp;
  }
  else if(r<Lhalf) {
    fp = sqE/tg(sqE*r+trial_delta) - 1./r;
    return sqE2 + fp*fp;
  }
  else
    return 0;
}

//# define TRIAL22_SAME_AS_11
//#  define InterpolateExactU22 InterpolateExactU11
//#  define InterpolateExactFp22 InterpolateExactFp11
//#  define InterpolateExactE22 InterpolateExactE11
#endif
//#ifdef TRIAL22_SAME_AS_11
//#endif

/************************** Tonks-Girardeau trapped 12 ********************/
#ifdef TRIAL_TONKS_TRAP12
#ifndef HARD_SPHERE12
//U = ln f
DOUBLE InterpolateExactU12(DOUBLE x) {
  return Log(fabs(x-a));
}

//Fp = f' / f
DOUBLE InterpolateExactFp12(DOUBLE x) {
  return 1./(x-a);
}

DOUBLE InterpolateExactE12(DOUBLE x) {
#ifdef TRIAL_1D // 1D Eloc = [-f"/f] + (f'/f)^2 = (f'/f)^2
  return 1./((x-a)*(x-a));
#endif
#ifdef TRIAL_2D // 2D Eloc = [-(f" + f'/r)/f] + (f'/f)^2
  return a/((x-a)*(x-a)*x);
#endif
#ifdef TRIAL_3D // 3D Eloc = [-(f"+2f'/r)/f] + (f'/f)^2
  return (2.*a-x)/((x-a)*(x-a)*x);
#endif
}
#else
//U = ln f
DOUBLE InterpolateExactU12(DOUBLE x) {
  if(x<a) return -100;
  return Log(fabs(x-a));
}

//Fp = f' / f
DOUBLE InterpolateExactFp12(DOUBLE x) {
  if(x<a) return 10;
  return 1./(x-a);
}

DOUBLE InterpolateExactE12(DOUBLE x) {
  if(x<a) return 0;
#ifdef TRIAL_1D // 1D Eloc = [-f"/f] + (f'/f)^2 = (f'/f)^2
  return 1./((x-a)*(x-a));
#endif
#ifdef TRIAL_2D // 2D Eloc = [-(f" + f'/r)/f] + (f'/f)^2
  return a/((x-a)*(x-a)*x);
#endif
#ifdef TRIAL_3D // 3D Eloc = [-(f"+2f'/r)/f] + (f'/f)^2
  return (2.*a-x)/((x-a)*(x-a)*x);
#endif
}
#endif // HARD_SPHERE
#endif // TRIAL_TONKS_TRAP

/************************** Tonks-Girardeau trapped 11 22 *****************/
#ifdef TRIAL_TONKS_TRAP1122
#ifndef HARD_SPHERE11
//U = ln f
DOUBLE InterpolateExactU11(DOUBLE x) {
  return Log(fabs(x-b));
}

//Fp = f' / f
DOUBLE InterpolateExactFp11(DOUBLE x) {
  return 1./(x-b);
}

DOUBLE InterpolateExactE11(DOUBLE x) {
#ifdef TRIAL_1D // 1D Eloc = [-f"/f] + (f'/f)^2 = (f'/f)^2
  return 1./((x-b)*(x-b));
#endif
#ifdef TRIAL_2D // 2D Eloc = [-(f" + f'/r)/f] + (f'/f)^2
  return b/((x-b)*(x-b)*x);
#endif
#ifdef TRIAL_3D // 3D Eloc = [-(f"+2f'/r)/f] + (f'/f)^2
  return (2.*b-x)/((x-b)*(x-b)*x);
#endif
}
#else // HS
//U = ln f
DOUBLE InterpolateExactU11(DOUBLE x) {
  if(x<aA) return -100;
  return Log(fabs(x-aA));
}

//Fp = f' / f
DOUBLE InterpolateExactFp11(DOUBLE x) {
  if(x<aA) return 10;
  return 1./(x-aA);
}

DOUBLE InterpolateExactE11(DOUBLE x) {
  if(x<b) return 0;
#ifdef TRIAL_1D // 1D Eloc = [-f"/f] + (f'/f)^2 = (f'/f)^2
  return 1./((x-aA)*(x-aA));
#endif
#ifdef TRIAL_2D // 2D Eloc = [-(f" + f'/r)/f] + (f'/f)^2
  return aA/((x-aA)*(x-aA)*x);
#endif
#ifdef TRIAL_3D // 3D Eloc = [-(f"+2f'/r)/f] + (f'/f)^2
  return (2.*aA-x)/((x-aA)*(x-aA)*x);
#endif
}
#endif // HARD_SPHERE
#endif // TRIAL_TONKS_TRAP1122

/************************** Tonks-Girardeau trapped 11 ********************/
#ifdef TRIAL_TONKS_TRAP11
#ifndef HARD_SPHERE11
//U = ln f
DOUBLE InterpolateExactU11(DOUBLE x) {
  return Log(fabs(x-aA));
}

//Fp = f' / f
DOUBLE InterpolateExactFp11(DOUBLE x) {
  return 1./(x-aA);
}

DOUBLE InterpolateExactE11(DOUBLE x) {
#ifdef TRIAL_1D // 1D Eloc = [-f"/f] + (f'/f)^2 = (f'/f)^2
  return 1./((x-aA)*(x-aA));
#endif
#ifdef TRIAL_2D // 2D Eloc = [-(f" + f'/r)/f] + (f'/f)^2
  return aA/((x-aA)*(x-aA)*x);
#endif
#ifdef TRIAL_3D // 3D Eloc = [-(f"+2f'/r)/f] + (f'/f)^2
  return (2.*aA-x)/((x-aA)*(x-aA)*x);
#endif
}
#else
//U = ln f
DOUBLE InterpolateExactU11(DOUBLE x) {
  if(x<aA) return -100;
  return Log(fabs(x-aA));
}

//Fp = f' / f
DOUBLE InterpolateExactFp11(DOUBLE x) {
  if(x<aA) return 10;
  return 1./(x-aA);
}

DOUBLE InterpolateExactE11(DOUBLE x) {
  if(x<aA) return 0;
#ifdef TRIAL_1D // 1D Eloc = [-f"/f] + (f'/f)^2 = (f'/f)^2
  return 1./((x-aA)*(x-aA));
#endif
#ifdef TRIAL_2D // 2D Eloc = [-(f" + f'/r)/f] + (f'/f)^2
  return aA/((x-aA)*(x-aA)*x);
#endif
#ifdef TRIAL_3D // 3D Eloc = [-(f"+2f'/r)/f] + (f'/f)^2
  return (2.*aA-x)/((x-aA)*(x-aA)*x);
#endif
}
#endif // HARD_SPHERE
#endif // TRIAL_TONKS_TRAP11

/************************** Tonks-Girardeau trapped 22 ********************/
#ifdef TRIAL_TONKS_TRAP22
#ifndef HARD_SPHERE22
//U = ln f
DOUBLE InterpolateExactU22(DOUBLE x) {
  return Log(fabs(x-aB));
}

//Fp = f' / f
DOUBLE InterpolateExactFp22(DOUBLE x) {
  return 1./(x-aB);
}

DOUBLE InterpolateExactE22(DOUBLE x) {
#ifdef TRIAL_1D // 1D Eloc = [-f"/f] + (f'/f)^2 = (f'/f)^2
  return 1./((x-aB)*(x-aB));
#endif
#ifdef TRIAL_2D // 2D Eloc = [-(f" + f'/r)/f] + (f'/f)^2
  return aB/((x-aB)*(x-aB)*x);
#endif
#ifdef TRIAL_3D // 3D Eloc = [-(f"+2f'/r)/f] + (f'/f)^2
  return (2.*aB-x)/((x-aB)*(x-aB)*x);
#endif
}
#else // Hard sphere
//U = ln f
DOUBLE InterpolateExactU22(DOUBLE x) {
  if(x<aB) return -100;
  return Log(fabs(x-aB));
}

//Fp = f' / f
DOUBLE InterpolateExactFp22(DOUBLE x) {
  if(x<aB) return 10;
  return 1./(x-aB);
}

DOUBLE InterpolateExactE22(DOUBLE x) {
  if(x<aB) return 0;
#ifdef TRIAL_1D // 1D Eloc = [-f"/f] + (f'/f)^2 = (f'/f)^2
  return 1./((x-aB)*(x-aB));
#endif
#ifdef TRIAL_2D // 2D Eloc = [-(f" + f'/r)/f] + (f'/f)^2
  return aB/((x-aB)*(x-aB)*x);
#endif
#ifdef TRIAL_3D // 3D Eloc = [-(f"+2f'/r)/f] + (f'/f)^2
  return (2.*aB-x)/((x-aB)*(x-aB)*x);
#endif
}
#endif // HARD_SPHERE
#endif // TRIAL_TONKS_TRAP22

/************************** Construct Grid Dipole **************************/
void ConstructGridDipoleKo11(struct Grid *G) {
  AllocateWFGrid(G, 3);

  Message("  2D Dipole Ko(r) trial wavefunction\n");
  Message("  Rpar11 %"LG"  [Lwf/2] -> %"LG"  [R]\n", Rpar11, Rpar11*Lhalf);
  Rpar11 *= Lhalf;

  Atrial = 1./K0opt(Rpar11);
  Ctrial = K1opt(Rpar11)*(L-Rpar11)*(L-Rpar11)*Sqrt(Rpar11)/(L*L*(L-2*Rpar11)*K0opt(Rpar11));
  Btrial = K1opt(Rpar11)/K0opt(Rpar11)*(L-2*Rpar11)*(L-Rpar11)/(L*L*Sqrt(Rpar11));

  G->min = 0;
  G->max = Lhalf;
  G->max2 = G->max*G->max;
}

/* executed only if TRIAL_DIPOLE_Ko is defined */
#ifdef TRIAL_DIPOLE_Ko11
DOUBLE InterpolateExactU11(DOUBLE r) {
  DOUBLE K0;
  if(r<Rpar11) {
    K0 = K0opt(r);
    return Log(Atrial*K0) - Btrial;
  }

  if(r>Lhalf) return 0; //r = Lhalfwf;
  return -Ctrial*(L-2*r)*(L-2*r)/((L-r)*r);
}

DOUBLE InterpolateExactFp11(DOUBLE r) {
  if(r<Rpar11) return K1opt(r)/(K0opt(r)*r*Sqrt(r));
  if(r>Lhalf) return 0.; //r = Lhalf;
  return Ctrial*(L-2*r)*L*L/((L-r)*(L-r)*r*r);
}

DOUBLE InterpolateExactE11(DOUBLE r) {
  DOUBLE t;
  if(r<Rpar11) {
    t = K1opt(r)/K0opt(r);
    return (t*t-1)/(r*r*r);
  }
  if(r>Lhalf) return 0.; //r = Lhalf;
  return Ctrial*L*L*(L*L-3*L*r+4*r*r)/((L-r)*(L-r)*(L-r)*r*r*r);
}
#endif

/************************** Construct Grid Dipole **************************/
void ConstructGridDipoleKo22(struct Grid *G) {
  AllocateWFGrid(G, 3);

  Message("  2D Dipole Ko(r) trial wavefunction\n");
  Message("  Rpar22 %"LG"  [Lwf/2] -> %"LG"  [R]\n", Rpar22, Rpar22*Lhalf);
  Rpar22 *= Lhalf;

  Atrial = 1./K0opt(Rpar22);
  Ctrial = K1opt(Rpar22)*(L-Rpar22)*(L-Rpar22)*Sqrt(Rpar22)/(L*L*(L-2*Rpar22)*K0opt(Rpar22));
  Btrial = K1opt(Rpar22)/K0opt(Rpar22)*(L-2*Rpar22)*(L-Rpar22)/(L*L*Sqrt(Rpar22));

  G->min = 0;
  G->max = Lhalf;
  G->max2 = G->max*G->max;
}

/* executed only if TRIAL_DIPOLE_Ko is defined */
#ifdef TRIAL_DIPOLE_Ko22
DOUBLE InterpolateExactU22(DOUBLE r) {
  DOUBLE K0;
  if(r<Rpar22) {
    K0 = K0opt(r);
    return Log(Atrial*K0) - Btrial;
  }

  if(r>Lhalf) return 0; //r = Lhalfwf;
  return -Ctrial*(L-2*r)*(L-2*r)/((L-r)*r);
}

DOUBLE InterpolateExactFp22(DOUBLE r) {
  if(r<Rpar22) return K1opt(r)/(K0opt(r)*r*Sqrt(r));
  if(r>Lhalf) return 0.; //r = Lhalf;
  return Ctrial*(L-2*r)*L*L/((L-r)*(L-r)*r*r);
}

DOUBLE InterpolateExactE22(DOUBLE r) {
  DOUBLE t;
  if(r<Rpar22) {
    t = K1opt(r)/K0opt(r);
    return (t*t-1)/(r*r*r);
  }
  if(r>Lhalf) return 0.; //r = Lhalf;
  return Ctrial*L*L*(L*L-3*L*r+4*r*r)/((L-r)*(L-r)*(L-r)*r*r*r);
}
#endif

/************************** Construct Grid Dipole **************************/
void ConstructGridDipolePhonon(struct Grid *G) {

  AllocateWFGrid(G, 3);

  Message("  2D Dipole phonon trial wavefunction\n");
  Warning("  Rpar %"LG"  [Lwf/2] -> %"LG"  [R]\n", Rpar, Rpar*Lhalfwf);
  Rpar *= Lhalfwf;

#ifdef TRIAL_1D
  Error(" Dipole-phonon w.f. is implemented in 2D only!");
#endif
#ifdef TRIAL_3D
  Error(" Dipole-phonon w.f. is implemented in 2D only!");
#endif

  Ctrial = Sqrt(Rpar)*(L-Rpar)*(L-Rpar)/(L-2*Rpar)/L;
  Btrial = Exp(2*Ctrial/Lhalf);
  Atrial = Btrial*Exp(2./Sqrt(Rpar)-Ctrial*(1/(Rpar)+1./(L-Rpar)));

  G->min = 0;
  G->max = L/2;
  G->max2 = G->max*G->max;
}

/* executed only if TRIAL_DIPOLES_PHONON is defined */
#ifdef TRIAL_DIPOLES_PHONON11
DOUBLE InterpolateExactU11(DOUBLE r) {
  if(r<Rpar)
    return -2./Sqrt(r) + Log(Atrial);
  else if(r>Lhalfwf) 
    return 0;
  return -Ctrial*(1./r+1./(Lwf-r))+Log(Btrial);
}

DOUBLE InterpolateExactFp11(DOUBLE r) {
  if(r<Rpar)
    return 1./(r*Sqrt(r));
  else if(r>Lhalfwf)
   return 0;
  return Ctrial*(1./(r*r)-1./((Lwf-r)*(Lwf-r)));
}

DOUBLE InterpolateExactE11(DOUBLE r) {
  if(r<Rpar)
    return 0.5/(r*r*Sqrt(r));
  else
    if(r>Lhalfwf) r = Lhalfwf;
  return Ctrial*Lwf*(Lwf*Lwf-3.*Lwf*r+4.*r*r)/(r*r*r*(Lwf-r)*(Lwf-r)*(Lwf-r));
}
#endif

/************************** Construct Grid Dipole **************************/
DOUBLE Lwf12, Lhalfwf12;
void ConstructGridDipoleGauss(struct Grid *G) {
  DOUBLE r;

  AllocateWFGrid(G, 3);

  Message("  12 2D Dipole Gauss w.f.: Atrial Exp(-Apar r^2), r<Rpar12*L/2; Btrial*exp (-Ctrial*(1./r + 1./(2*Bpar12 - r))), r < Bpar12*L/2; 1, r < L/2\n");
  Message("  Rpar12 %"LG"  [L/2] -> %"LG"  [R]\n", Rpar12, Rpar12*Lhalf);
  Rpar12 *= Lhalf;
  Message("  Bpar12 %"LG"  [L] -> %"LG"  [R]\n", Bpar12, Bpar12*L);
  Bpar12 *= Lhalf;
  Lhalfwf12 = Bpar12;
  Lwf12 = 2.*Lhalfwf12;

  // match f'(Rpar12)
  r = Rpar12;
  Ctrial12 = -2.*Apar12*r/(1./(r*r)-1./((Lwf12-r)*(Lwf12-r)));
  // match f(Lhalfwf12)
  r = Lhalfwf12;
  Btrial12 = Ctrial12*(1./r+1./(Lwf12-r));
  // match f(Rpar12)
  r = Rpar12;
  Atrial12 = Apar12*r*r + Btrial12-Ctrial12*(1./r+1./(Lwf12-r));

  G->min = 0;
  G->max = Lhalf;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_DIPOLE_GAUSS12
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r<Rpar12) {
    return Atrial12 - Apar12*r*r;
  }
  else if(r<Lhalfwf12) {
    return Btrial12-Ctrial12*(1./r+1./(Lwf12-r));
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  if(r<Rpar12) {
    return -2.*Apar12*r;
  }
  else if(r<Lhalfwf12) {
    return Ctrial12*(1./(r*r)-1./((Lwf12-r)*(Lwf12-r)));
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  if(r<Rpar12) {
    return 4.*Apar12;
  }
  else if(r<Lhalfwf12) {
    return Ctrial12*Lwf12*(Lwf12*Lwf12-3.*Lwf12*r+4.*r*r)/(r*r*r*(Lwf12-r)*(Lwf12-r)*(Lwf12-r));
  }
  else {
    return 0;
  }
}
#endif

/************************** Construct Grid SW BS phonons ******************/
void ConstructGridZeroRangeUnitaryPhonons(struct Grid *G) {
  DOUBLE r;

  Message(" wf 12: Zero range unitary\n");

#ifdef BC_ABSENT
  Message("  trapped geometry, f2(r) = 1/r\n");
#else
  // matching at Rpar
  Warning("  Matching distance Rpar = %lf [L/2] -> %lf [R]\n", Rpar, Rpar*Lhalf);
  if(Rpar>1 || Rpar <0) Error(" Rpar out of (0,1) range!\n");
  Rpar *= Lhalf;

  // f'(Rpar)
  r = Rpar;
  A5SW = - 1./r / (2.*(1./(r*r*r)-1./((Lwf-r)*(Lwf-r)*(Lwf-r))));

  // f(L/2)
  r = Lhalfwf;
  A2SW = A5SW*(1./(r*r)+1./((Lwf-r)*(Lwf-r)));

  // f(Rpar)
  r = Rpar;
  A1SW =Log(r) + A2SW - A5SW*(1./(r*r)+1./((Lwf-r)*(Lwf-r)));
#endif

  G->min = 0;
  G->max = Lhalf;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_ZERO_RANGE_UNITARY
DOUBLE InterpolateExactU12(DOUBLE r) {
#ifdef BC_ABSENT
  return -Log(r);
#else
  if(r<Rpar)
    return A1SW-Log(r);
  else
    return A2SW - A5SW*(1./(r*r)+1./((Lwf-r)*(Lwf-r)));
#endif
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
#ifdef BC_ABSENT
  return - 1./r;
#else
  if(r<Rpar)
    return - 1./r;
  else
    return 2.*A5SW*(1./(r*r*r)-1./((Lwf-r)*(Lwf-r)*(Lwf-r)));
#endif
}

DOUBLE InterpolateExactE12(DOUBLE r) {
#ifdef BC_ABSENT
  return 1./(r*r);
#else
  DOUBLE fp;

  if(r<Rpar) {
    return 1./(r*r);
  }
  else
    return 2.*A5SW*(Lwf*Lwf*Lwf*Lwf - 4.*Lwf*Lwf*Lwf*r + 6.*Lwf*Lwf*r*r - 2.*Lwf*r*r*r + 2.*r*r*r*r)/(r*r*r*r*(Lwf-r)*(Lwf-r)*(Lwf-r)*(Lwf-r));
#endif
}
#endif

/************************** Construct Grid HS 2D Finite Energy *************************/
// zero energy HD: f(r) = const ln r
void ConstructGridHS2DFiniteEnergy11(struct Grid *G) {
  DOUBLE k, kmin, kmax;
  DOUBLE y, ymin, ymax;
  DOUBLE precision = 1e-12;
  int search = ON;
  DOUBLE C=0.;
  DOUBLE R;

  Message("  Jastrow term: ConstructGridHS2DFiniteEnergy trial wavefunction\n");
  if(Rpar == 0.) {
    Warning("  Rpar -> Lwf/2\n", Lhalfwf);
    Rpar = Lhalfwf;
  }
  R = Rpar;

  AllocateWFGrid(G, 3);

  k = kmin = 1e-6/R;
  Atrial =  BesselY0(k*a)/(BesselJ0(k*R)*BesselY0(k*a)-BesselJ0(k*a)*BesselY0(k*R));
  Btrial = -BesselJ0(k*a)/(BesselJ0(k*R)*BesselY0(k*a)-BesselJ0(k*a)*BesselY0(k*R));

  // Solve J1(kR) Y0(ka) - J0(ka) Y0(kR) = 0
  Message("Fixing kR (1)... ");
  ymin = BesselJ1(k*R)*BesselY0(k*a) - BesselY1(k*R)*BesselJ0(k*a);
  while(search) {
    k = kmax = kmin + 0.1*kmin;
    Atrial =  BesselY0(k*a)/(BesselJ0(k*R)*BesselY0(k*a)-BesselJ0(k*a)*BesselY0(k*R));
    Btrial = -BesselJ0(k*a)/(BesselJ0(k*R)*BesselY0(k*a)-BesselJ0(k*a)*BesselY0(k*R));
    y = BesselJ1(k*R)*BesselY0(k*a) - BesselY1(k*R)*BesselJ0(k*a);

    if((ymin-C)*(y-C)<0) {
      search = OFF;
    }
    else {
      kmin = k;
      ymin = y;
    }
  }

  search = ON;
  Message("done\n  Fixing kR (2)... ");
  while(search) {
    k = (kmin+kmax)/2.;
    Atrial =  BesselY0(k*a)/(BesselJ0(k*R)*BesselY0(k*a)-BesselJ0(k*a)*BesselY0(k*R));
    Btrial = -BesselJ0(k*a)/(BesselJ0(k*R)*BesselY0(k*a)-BesselJ0(k*a)*BesselY0(k*R));
    y = BesselJ1(k*R)*BesselY0(k*a) - BesselY1(k*R)*BesselJ0(k*a);

    if((y-C)*(ymin-C) < 0) {
      kmax = k;
      ymax = y;
    } else {
      kmin = k;
      ymin = y;
    }

    if(fabs(y)<precision) {
      k = (kmax*(ymin-C)-kmin*(ymax-C)) / (ymin-ymax);
      Atrial =  BesselY0(k*a)/(BesselJ0(k*R)*BesselY0(k*a)-BesselJ0(k*a)*BesselY0(k*R));
      Btrial = -BesselJ0(k*a)/(BesselJ0(k*R)*BesselY0(k*a)-BesselJ0(k*a)*BesselY0(k*R));
      y = BesselJ1(k*R)*BesselY0(k*a) - BesselY1(k*R)*BesselJ0(k*a);
      search = OFF;
    }
  }
  Message("done\n");

  Atrial =  BesselY0(k*a)/(BesselJ0(k*R)*BesselY0(k*a)-BesselJ0(k*a)*BesselY0(k*R));
  Btrial = -BesselJ0(k*a)/(BesselJ0(k*R)*BesselY0(k*a)-BesselJ0(k*a)*BesselY0(k*R));

  Message("  k = %"LE " , error %"LE " \n", k, fabs(y));
  Message("    A = %"LE " \n", Atrial);
  Message("    B = %"LE " \n", Btrial);

  sqE = k;
  sqE2 = sqE*sqE;

  G->min = 0;
  G->max = L/2;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_HS2D_FINITE_ENERGY11
DOUBLE InterpolateExactU11(DOUBLE r) {
  if(r>Rpar)
    return 0.;
  else if(r>a) {
    return Log(Atrial*BesselJ0(sqE*r)+Btrial*BesselY0(sqE*r));
  }
  else
    return -100;
}

DOUBLE InterpolateExactFp11(DOUBLE r) {
  if(r>Rpar || r<a)
    return 0.;
  else
    return -sqE*(Atrial*BesselJ1(sqE*r)+Btrial*BesselY1(sqE*r))/(Atrial*BesselJ0(sqE*r)+Btrial*BesselY0(sqE*r));
}

DOUBLE InterpolateExactE11(DOUBLE r) {
  DOUBLE Fp;
  if(r<a)
    return -100;
  else
    if( r>Lhalf) return 0.;

  Fp = -sqE*(Atrial*BesselJ1(sqE*r)+Btrial*BesselY1(sqE*r))/(Atrial*BesselJ0(sqE*r)+Btrial*BesselY0(sqE*r));;
  return sqE*sqE+Fp*Fp;
}
#endif

/************************** Construct Grid HS 2D Zero Energy *************************/
// zero energy HD: f(r) = const ln r
void ConstructGridHS2DZeroEnergyPhonons11(struct Grid *G) {
  DOUBLE r;

  Message("  Jastrow term: TRIAL_HS2D_ZERO_ENERGY_PHONONS11 trial wavefunction\n");
  Message("     f(r) = A ln(r/aA), r<Rpar; B Exp(- C/r + D/r^2); Rpar - var. parameter\n");

  Warning("  Changing Rpar to units of L/2, Rpar = %lf -> %lf\n", Rpar, Rpar*Lhalfwf);
  Rpar *= Lhalfwf;

  if(Rpar == 0.) {
    Warning("  Rpar -> Lwf/2\n", Lhalfwf);
    Rpar = Lhalfwf;
  }
  if(aA<=0) Error("  parameter 'a' must be positive, a = %lf", aA);
  if(Rpar < aA) Error("  Rpar = %lf < a = %lf, must be otherwise", Rpar, aA);
  if(Rpar > Lhalfwf) Error("  Rpar = %lf > L/2 = %lf, must be otherwise", Rpar, Lhalfwf);

  AllocateWFGrid(G, 3);

  Ctrial = -Rpar*Rpar / ((Lhalfwf-Rpar)*Log(Rpar/aA));
  r = Lhalfwf;
  Btrial = Ctrial/r*(1. - 0.5*Lhalfwf/r);

  r = Rpar;
  Atrial = Exp(Btrial - Ctrial/r*(1. - 0.5*Lhalfwf/r)) / Log(r/aA);
  Message("    A = %"LE " \n", Atrial);
  Message("    B = %"LE " \n", Btrial);
  Message("    C = %"LE " \n", Ctrial);

  G->min = 0;
  G->max = Lhalfwf;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_HS2D_ZERO_ENERGY_PHONONS11
DOUBLE InterpolateExactU11(DOUBLE r) {
  if(r<aA) {
    return -100;
  }
  else if(r<Rpar) {
    return Log(Atrial*Log(r/aA));
  }
  else if(r<Lhalfwf) {
    return Btrial - Ctrial/r*(1. - 0.5*Lhalfwf/r);
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactFp11(DOUBLE r) {
  if(r<aA) {
    return 100;
  }
  else if(r<Rpar) {
    return 1./(r*Log(r/aA));
  }
  else if(r<Lhalfwf) {
    return Ctrial*(r-Lhalfwf)/(r*r*r);
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactE11(DOUBLE r) {
  DOUBLE logf1;

  if(r<aA) {
    return 100;
  }
  else if(r<Rpar) {
    logf1 = Log(r/aA);
#ifdef TRIAL_3D
    return (1.-logf1) / (r*r*logf1*logf1);
#endif
#ifdef TRIAL_2D
    return 1. / (r*r*logf1*logf1);
#endif
#ifdef TRIAL_1D
    return (1.+logf1) / (r*r*logf1*logf1);
#endif
  }
  else if(r<Lhalfwf) {
#ifdef TRIAL_3D
    return -Ctrial*Lhalfwf/(r*r*r*r);
#endif
#ifdef TRIAL_2D
    return Ctrial*(r-Lwf)/(r*r*r*r);
#endif
#ifdef TRIAL_1D
    return Ctrial*(2.*r-3.*Lhalfwf)/(r*r*r*r);
#endif
  }
  else {
    return 0;
  }
}

#endif

/************************** Construct Grid Square well 2D Zero Energy *************************/
// zero energy HD: f(r) = const ln r
void ConstructGridSquareWell2DZeroEnergyPhonons12(struct Grid *G) {
  DOUBLE r;

  Message("  Jastrow term: attractive branch TRIAL_SQUARE_WELL2D_ZERO_ENERGY_PHONONS12 trial wavefunction\n");
  Message("     f(r) = Jo(kappa r), r<Ro; A ln(r/aA), r<Rpar; B exp(-C/r + D/r^2), Rpar12 - var. parameter\n");

  Warning("  Changing Rpar12 to units of L/2, Rpar12 = %lf -> %lf\n", Rpar12, Rpar12*Lhalfwf);
  Rpar12 *= Lhalfwf;

  if(Rpar12 == 0.) {
    Warning("  Rpar12 -> Lwf/2\n", Lhalfwf);
    Rpar12 = Lhalfwf;
  }
  if(a<=0) Error("  parameter 'a' must be positive, a = %lf", a);
  if(Rpar12 > Lhalfwf) Error("  Rpar12 = %lf > L/2 = %lf, must be otherwise", Rpar12, Lhalfwf);
  if(Rpar12 < Ro) Error("  Rpar12 = %lf < Ro = %lf, must be otherwise", Rpar12, Ro);

  AllocateWFGrid(G, 3);

  A4SW = -Rpar12*Rpar12 / ((Lhalfwf-Rpar12)*Log(Rpar12/a));
  r = Lhalfwf;
  A3SW = A4SW/r*(1. - 0.5*Lhalfwf/r);

  r = Rpar12;
  A2SW = Exp(A3SW - A4SW/r*(1. - 0.5*Lhalfwf/r)) / Log(r/a);

  //trial_Kappa = sqrt(trial_Vo);
  trial_Kappa = sqrt(2.*m_mu*trial_Vo);
  trial_Kappa2 = 2.*m_mu*trial_Vo;
  r = Ro;
  A1SW = A2SW*Log(r/a)/BesselJ0(trial_Kappa*r);

  Message("    A1 = %"LE " \n", A1SW);
  Message("    A2 = %"LE " \n", A2SW);
  Message("    A3 = %"LE " \n", A3SW);
  Message("    A4 = %"LE " \n", A4SW);

  G->min = 0;
  G->max = Lhalfwf;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_SQUARE_WELL2D_ZERO_ENERGY_PHONONS12
DOUBLE InterpolateExactU12(DOUBLE r) {
  DOUBLE f;

  if(r<Rpar12) { // only first two regions can be negative
    if(r < Ro) {
      f = A1SW*BesselJ0(trial_Kappa*r);
    }
    else {
      f = A2SW*Log(r/a);
    }
#ifdef TRIAL_SET_NEGATIVE_WF_TO_ZERO
    if(f<0) 
      return -100;
#endif
    return 
      log(fabs(f));
  }
  if(r<Lhalfwf) {
    return A3SW - A4SW/r*(1. - 0.5*Lhalfwf/r);
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  DOUBLE f, Fp;

  if(r<Rpar12) { // only first two regions can be negative
    if(r < Ro) {
      f = A1SW*BesselJ0(trial_Kappa*r);
      Fp = -trial_Kappa*BesselJ1(trial_Kappa*r)*A1SW/f;
    }
    else {
      f = Log(r/a);
      Fp = 1./(r*f);
      //Fp = 1./(r*Log(r/a));
    }
#ifdef TRIAL_SET_NEGATIVE_WF_TO_ZERO
    if(f<0) 
      return 100;
#endif
    return Fp;
  }
  else if(r<Lhalfwf) {
    return A4SW*(r-Lhalfwf)/(r*r*r);
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  DOUBLE logf1,fp;
  DOUBLE f;

  if(r<Rpar12) { // only first two regions can be negative
    if(r < Ro) {
      f = A1SW*BesselJ0(trial_Kappa*r);
    }
    else {
      f = Log(r/a);
    }
#ifdef TRIAL_SET_NEGATIVE_WF_TO_ZERO
    if(f<0) return 100;
#endif
  }

  if(r<Ro) {
    fp = InterpolateExactFp12(r);
    return trial_Kappa2 + fp*fp;
  }
  else if(r<Rpar12) {
    logf1 = Log(r/a);
    return 1. / (r*r*logf1*logf1);
  }
  else if(r<Lhalfwf) {
    return A4SW*(r-Lwf)/(r*r*r*r);
  }
  else {
    return 0;
  }
}
#endif

/************************** Construct Grid Square well ****************************************/
void ConstructGridSquareWell_JO_Phonons12(struct Grid *G) {
  DOUBLE r;

  Message("  Jastrow term: TRIAL_SQUARE_WELL2D_BS_PHONONS12 trial wavefunction\n");
  Message("     f(r) = A Jo(kappa r), r<Ro; B Jo(k r), r<Rpar12; C Exp(- D/r + E/r^2); Rpar12 - var. parameter\n");

  Warning("  Changing Rpar12 to units of L/2, Rpar12 = %lf -> %lf\n", Rpar12, Rpar12*Lhalfwf);
  Rpar12 *= Lhalfwf;

  if(Rpar12 == 0.) {
    Warning("  Rpar12 -> Lwf/2\n", Lhalfwf);
    Rpar12 = Lhalfwf;
  }
  if(a<=0) Error("  parameter 'a' must be positive, a = %lf", a);
  if(Rpar12 > Lhalfwf) Error("  Rpar12 = %lf > L/2 = %lf, must be otherwise", Rpar12, Lhalfwf);
  if(Rpar12 < Ro) Error("  Rpar12 = %lf < Ro = %lf, must be otherwise", Rpar12, Ro);

  AllocateWFGrid(G, 3);

  trial_k = 2./(exp(0.57721566490153286060651209008240243104215933593992)*a);
  trial_k2 = trial_k*trial_k;
  if(2.*m_mu*trial_Vo - trial_k2*(aA/a)*(aA/a) < 0) Error("  cannot construct Jastrow, increase a");
  //trial_Kappa = sqrt(trial_Vo - trial_k2*(aA/a)*(aA/a));
  trial_Kappa = sqrt(2.*m_mu*trial_Vo - trial_k2*(aA/a)*(aA/a));
  trial_Kappa2 = trial_Kappa*trial_Kappa;

  // f'(Rpar12)
  r = Rpar12;
  A4SW = -trial_k*BesselJ1(trial_k*r) / BesselJ0(trial_k*r) / ((r - Lhalfwf) / (r*r*r));

  // f(L/2) = 1
  r = Lhalfwf;
  A3SW = A4SW/r*(1. - 0.5*Lhalfwf/r);

  // f(Rpar12)
  r = Rpar12;
  A2SW = exp(A3SW - A4SW / r*(1. - 0.5*Lhalfwf / r)) / BesselJ0(trial_k*r);

  // f(Ro)
  r = Ro;
  A1SW = A2SW*BesselJ0(trial_k*r) / BesselJ0(trial_Kappa*r);

  Message("    A1 = %"LE " \n", A1SW);
  Message("    A2 = %"LE " \n", A2SW);
  Message("    A3 = %"LE " \n", A3SW);
  Message("    A4 = %"LE " \n", A4SW);

  G->min = 0;
  G->max = Lhalfwf;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_SQUARE_WELL2D_J0_PHONONS12
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r<Ro) {
    return Log(fabs(A1SW*BesselJ0(trial_Kappa*r)));
  }
  else if(r<Rpar12) {
    return Log(fabs(A2SW*BesselJ0(trial_k*r)));
  }
  else if(r<Lhalfwf) {
    return A3SW - A4SW/r*(1. - 0.5*Lhalfwf/r);
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  if(r<Ro) {
    return -trial_Kappa*BesselJ1(trial_Kappa*r)/BesselJ0(trial_Kappa*r);
  }
  else if(r<Rpar12) {
    return -trial_k*BesselJ1(trial_k*r)/BesselJ0(trial_k*r);
  }
  else if(r<Lhalfwf) {
    return A4SW*(r-Lhalfwf)/(r*r*r);
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  DOUBLE logf1,fp;

  if(r<Ro) {
    fp = InterpolateExactFp12(r);
    return trial_Kappa2 + fp*fp;
  }
  else if(r<Rpar12) {
    fp = InterpolateExactFp12(r);
    return trial_k2 + fp*fp;
  }
  else if(r<Lhalfwf) {
    return A4SW*(r-Lwf)/(r*r*r*r);
  }
  else {
    return 0;
  }
}
#endif

/************************** Construct Grid Square well ****************************************/
void ConstructGridSquareWell2D_K0_Phonons12(struct Grid *G) {
  DOUBLE r;
  DOUBLE kappa, k;
  DOUBLE x, xmin, xmax, y, ymin, ymax;
  DOUBLE precision = 1e-8;
  int search = ON;

  // solve 
  xmin = 1e-4; //1e-4
  if(xmin > 0.1 / (a*a)) xmin = 0.1 / (a*a);

  Message("  Jastrow term: TRIAL_SQUARE_WELL2D_K0_PHONONS12 trial wavefunction\n");
  Message("     f(r) = A Jo(kappa r), r<Ro; B Ko(k r), r<Rpar12; C Exp(- D/r + E/r^2); Rpar12 - var. parameter\n");

  if(a<=0) Error("  parameter 'a' must be positive, a = %lf", a);
  Warning("  Changing Rpar12 to units of L/2, Rpar12 = %lf -> %lf\n", Rpar12, Rpar12*Lhalfwf);
  Rpar12 *= Lhalfwf;

  if(Rpar12 == 0.) {
    Warning("  Rpar12 -> Lwf/2\n", Lhalfwf);
    Rpar12 = Lhalfwf;
  }

  if(Rpar12 > Lhalfwf) Error("  Rpar12 = %lf > L/2 = %lf, must be otherwise", Rpar12, Lhalfwf);
  if(Rpar12 < Ro) Error("  Rpar12 = %lf < Ro = %lf, must be otherwise", Rpar12, Ro);

  AllocateWFGrid(G, 3);

  Message("     Solving consistently equation for the scattering energy\n");

  // -x means energy, 0 < |x| < trial_Vo
  x = xmin;
  k = sqrt(x);
  kappa = sqrt(2.*m_mu*trial_Vo - x);
  ymin = k*BesselK1(k*Ro)*BesselJ0(kappa*Ro) - kappa*BesselK0(k*Ro)*BesselJ1(kappa*Ro);

  xmax = 2.*m_mu*trial_Vo*0.999999;
  x = xmax;
  k = sqrt(x);
  kappa = sqrt(2.*m_mu*trial_Vo - x);
  ymax = k*BesselK1(k*Ro)*BesselJ0(kappa*Ro) - kappa*BesselK0(k*Ro)*BesselJ1(kappa*Ro);

  if(ymin*ymax > 0) Error("ConstructGridSquareWell_KO_Phonons12: cannot construct the grid : No solution found");

  while(search) {
    x = (xmin+xmax)/2.;
    k = sqrt(x);
    kappa = sqrt(2.*m_mu*trial_Vo - x);
    y = k*BesselK1(k*Ro)*BesselJ0(kappa*Ro) - kappa*BesselK0(k*Ro)*BesselJ1(kappa*Ro);

    if(y*ymin < 0) {
      xmax = x;
      ymax = y;
    } else {
      xmin = x;
      ymin = y;
    }

    if(fabs(y)<precision) {
      x = (xmax*ymin-xmin*ymax) / (ymin-ymax);
      k = sqrt(x);
      kappa = sqrt(2.*m_mu*trial_Vo - x);
      y = k*BesselK1(k*Ro)*BesselJ0(kappa*Ro) - kappa*BesselK0(k*Ro)*BesselJ1(kappa*Ro);
      search = OFF;
    }
  }
  Message("  Solution k = %"LE", error %"LE"\n", k, y);

  trial_k = k;
  trial_k2 = k*k;
  trial_Kappa = kappa;
  trial_Kappa2 = kappa*kappa;

  // f'(Rpar12)
  r = Rpar12;
  A4SW = -trial_k*BesselK1(trial_k*r) / BesselK0(trial_k*r) / ((r - Lhalfwf) / (r*r*r));

  // f(L/2) = 1
  r = Lhalfwf;
  A3SW = A4SW/r*(1. - 0.5*Lhalfwf/r);

  // f(Rpar12)
  r = Rpar12;
  A2SW = exp(A3SW - A4SW / r*(1. - 0.5*Lhalfwf / r)) / BesselK0(trial_k*r);

  // f(Ro)
  r = Ro;
  A1SW = A2SW*BesselK0(trial_k*r) / BesselJ0(trial_Kappa*r);

  Message("    A1 = %"LE " \n", A1SW);
  Message("    A2 = %"LE " \n", A2SW);
  Message("    A3 = %"LE " \n", A3SW);
  Message("    A4 = %"LE " \n", A4SW);

  G->min = 0;
  G->max = Lhalfwf;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_SQUARE_WELL2D_K0_PHONONS12
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r<Ro) {
    return Log(fabs(A1SW*BesselJ0(trial_Kappa*r)));
  }
  else if(r<Rpar12) {
    return Log(fabs(A2SW*BesselK0(trial_k*r)));
  }
  else if(r<Lhalfwf) {
    return A3SW - A4SW/r*(1. - 0.5*Lhalfwf/r);
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  if(r<Ro) {
    return -trial_Kappa*BesselJ1(trial_Kappa*r)/BesselJ0(trial_Kappa*r);
  }
  else if(r<Rpar12) {
    return -trial_k*BesselK1(trial_k*r)/BesselK0(trial_k*r);
  }
  else if(r<Lhalfwf) {
    return A4SW*(r-Lhalfwf)/(r*r*r);
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  DOUBLE logf1,fp;

  if(r<Ro) {
    fp = InterpolateExactFp12(r);
    return trial_Kappa2 + fp*fp;
  }
  else if(r<Rpar12) {
    fp = InterpolateExactFp12(r);
    return -trial_k2 + fp*fp;
  }
  else if(r<Lhalfwf) {
    return A4SW*(r-Lwf)/(r*r*r*r);
  }
  else {
    return 0;
  }
}
#endif

/************************** Construct Grid Square well ****************************************/
void ConstructGridZeroRange_K0_Phonons12(struct Grid *G) {
  DOUBLE r;

  trial_k = 1./a;
  trial_k2 = trial_k*trial_k;
  Message("\n  ConstructGridZeroRange_K0_Phonons12: scattering momentum k=1/a = %lf\n", trial_k);
  Warning("  Changing Rpar12 to units of L/2, Rpar12 = %lf -> %lf\n", Rpar12, Rpar12*Lhalfwf);
  Rpar12 *= Lhalfwf;

  // f'(Rpar12)
  r = Rpar12;
  A4SW = -trial_k*BesselK1(trial_k*r) / BesselK0(trial_k*r) / ((r - Lhalfwf) / (r*r*r));

  // f(L/2) = 1
  r = Lhalfwf;
  A3SW = A4SW/r*(1. - 0.5*Lhalfwf/r);

  // f(Rpar12)
  r = Rpar12;
  A2SW = exp(A3SW - A4SW / r*(1. - 0.5*Lhalfwf / r)) / BesselK0(trial_k*r);

  Message("    A2 = %"LE " \n", A2SW);
  Message("    A3 = %"LE " \n", A3SW);
  Message("    A4 = %"LE " \n", A4SW);

  G->min = 0;
  G->max = Lhalfwf;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_ZERO_RANGE_K0_PHONONS12
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r<Rpar12) {
    return Log(fabs(A2SW*BesselK0(trial_k*r)));
  }
  else if(r<Lhalfwf) {
    return A3SW - A4SW/r*(1. - 0.5*Lhalfwf/r);
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
if(r<Rpar12) {
    return -trial_k*BesselK1(trial_k*r)/BesselK0(trial_k*r);
  }
  else if(r<Lhalfwf) {
    return A4SW*(r-Lhalfwf)/(r*r*r);
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  DOUBLE logf1,fp;

if(r<Rpar12) {
    fp = InterpolateExactFp12(r);
    return -trial_k2 + fp*fp;
  }
  else if(r<Lhalfwf) {
    return A4SW*(r-Lwf)/(r*r*r*r);
  }
  else {
    return 0;
  }
}
#endif

/************************** Construct Grid Square well ****************************************/
void ConstructGridZeroRange_K0_Exp12(struct Grid *G) {
  DOUBLE r;
  int search = ON;
  DOUBLE gamma = 0.57721566490153286060651209008240243104215933593992;

  trial_k = 1./a;
  //trial_k = 1./ (2. / exp(gamma) * a);
  trial_k2 = trial_k*trial_k;
  //Message("\n  ConstructGridZeroRange_K0_Exp12: scattering momentum k=e^gamma/(2*a) = %lf\n", trial_k);
  Message("\n  ConstructGridZeroRange_K0_Exp12: scattering momentum k=1/a = %lf\n", trial_k);
  Warning("  Changing Rpar12 to units of L/2, Rpar12 = %lf -> %lf\n", Rpar12, Rpar12*Lhalfwf);
  Rpar12 *= Lhalfwf;

  // f'(Rpar12)
  r = Rpar12;
  A4SW = -trial_k*BesselK1(trial_k*r)/BesselK0(trial_k*r) / (Kpar*pow(Lhalfwf-r, Kpar-1.));

  // f(L/2) = 1 automatic
  // f'(L/2) = 0 automatic

  // f(Rpar12)
  r = Rpar12;
  A2SW = exp(- A4SW*pow(Lhalfwf-r, Kpar)) / BesselK0(trial_k*r);

  Message("    Kpar = %"LE " \n", Kpar);
  Message("    A2 = %"LE " \n", A2SW);

  G->min = 0;
  G->max = Lhalfwf;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_ZERO_RANGE_K0_EXP12
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r<Rpar12) {
    return Log(fabs(A2SW*BesselK0(trial_k*r)));
  }
  else if(r<Lhalfwf) {
    return - A4SW*pow(Lhalfwf-r, Kpar);
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  if(r<Rpar12) {
    return -trial_k*BesselK1(trial_k*r)/BesselK0(trial_k*r);
  }
  else if(r<Lhalfwf) {
    return A4SW*Kpar*pow(Lhalfwf-r, Kpar-1.);
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  DOUBLE logf1,fp;

  if(r<Rpar12) {
    fp = InterpolateExactFp12(r);
    return -trial_k2 + fp*fp;
  }
  else if(r<Lhalfwf) {
    return A4SW*Kpar*pow(Lhalfwf-r, Kpar-2.)*(Kpar*r-Lhalfwf)/r;
  }
  else {
    return 0;
  }
}
#endif

/************************** Construct Grid Zero Range Log GPE Polaron **************************/
void ConstructGridZeroRange_Log_GPE_Polaron(struct Grid *G) {
  DOUBLE r, FFp1;
  DOUBLE gamma = 0.57721566490153286060651209008240243104215933593992;

  Message("\n  ConstructGridZeroRange_Log_GPE_Polaron\n");
  //Warning("  Changing Rpar12 to units of L/2, Rpar12 = %lf -> %lf\n", Rpar12, Rpar12*Lhalfwf);
  //Rpar12 *= Lhalfwf;
  Apar = 1. / Apar;

  if(Rpar12>Lhalf) Error("  decrease Rpar12 = %lf and make it smaller than L/2 = %lf", Rpar12, Lhalf);

  // f'(Rpar12)
  r = Rpar12;
  FFp1 = 1./(r*Log(r/a));
  A3SW = 1. + 1./(-1. + cosh((Apar*(L - 2.*r))/2.) + (Apar*sinh((Apar*(L - 2.*r))/2.))/FFp1);

  // f'(L/2) = 0 automatic
  // f(L/2) = 1
  r = L / 2.;
  //A4SW = - A3SW/(exp(-Apar*r) + exp(Apar*(r-L)));
  A4SW = (1. - A3SW) / (exp(-Apar*r) + exp(Apar*(r - L)));

  // f(Rpar12)
  r = Rpar12;
  A2SW = (A3SW + A4SW*(exp(-Apar*r) + exp(Apar*(r-L)))) / Log(r/a);

  Message("    A2 = %"LE " \n", A2SW);
  Message("    A3 = %"LE " \n", A3SW);
  Message("    A4 = %"LE " \n", A4SW);

  G->min = 0;
  G->max = Lhalfwf;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_ZERO_RANGE_LOG_GPE_POLARON
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r<Rpar12) {
    return Log(fabs(A2SW*Log(r/a)));
  }
  else if(r<Lhalfwf) {
    return Log(A3SW + A4SW*(exp(-Apar*r) + exp(Apar*(r-L))));
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  if(r<Rpar12) {
    return 1./(r*Log(r/a));
  }
  else if(r<Lhalfwf) {
    DOUBLE f, fp;

    f = A3SW + A4SW*(exp(-Apar*r) + exp(Apar*(r-L)));
    fp = A4SW*Apar*(-exp(-Apar*r) + exp(Apar*(r-L)));

    return fp/f;
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  if(r<Rpar12) {
    DOUBLE logf1;
    logf1 = Log(r/a);
    return 1. / (r*r*logf1*logf1);
  }
  else if(r<Lhalfwf) {
    DOUBLE f, fp, fpp;

    f = A3SW + A4SW*(exp(-Apar*r) + exp(Apar*(r-L)));
    fp = A4SW*Apar*(-exp(-Apar*r) + exp(Apar*(r-L)));
    fpp = A4SW*Apar*Apar*(exp(-Apar*r) + exp(Apar*(r-L)));

    // [-(f" + f'/r)/f] + (f'/f)^2
    return -(fpp+fp/r)/f + (fp/f)*(fp/f);
  }
  else {
    return 0;
  }
}
#endif

/************************** Construct Grid Zero Range K0 GPE Polaron **************************/
void ConstructGridZeroRange_K0_GPE_Polaron(struct Grid *G) {
  DOUBLE r;
  DOUBLE gamma = 0.57721566490153286060651209008240243104215933593992;

  trial_k = 1./a;
  //trial_k = 1./ (2. / exp(gamma) * a);
  trial_k2 = trial_k*trial_k;
  Message("\n  ConstructGridZeroRange_K0_GPE_Polaron: scattering momentum k=1/a = %lf\n", trial_k);
  //Warning("  Changing Rpar12 to units of L/2, Rpar12 = %lf -> %lf\n", Rpar12, Rpar12*Lhalfwf);
  //Rpar12 *= Lhalfwf;
  Apar = 1. / Apar;

  // f'(Rpar12)
  r = Rpar12;
  A3SW = 1./(1. + 1./(-cosh((Apar*(L - 2*r))/2.) + (Apar*BesselK0(trial_k*r)*sinh((Apar*(L - 2*r))/2.))/(trial_k*BesselK1(trial_k*r))));

  // f'(L/2) = 0 automatic
  // f(L/2) = 1
  r = L / 2.;
  //A4SW = - A3SW/(exp(-Apar*r) + exp(Apar*(r-L)));
  A4SW = (1. - A3SW) / (exp(-Apar*r) + exp(Apar*(r - L)));

  // f(Rpar12)
  r = Rpar12;
  A2SW = (A3SW + A4SW*(exp(-Apar*r) + exp(Apar*(r-L)))) / BesselK0(trial_k*r);

  Message("    A2 = %"LE " \n", A2SW);
  Message("    A3 = %"LE " \n", A3SW);
  Message("    A4 = %"LE " \n", A4SW);

  G->min = 0;
  G->max = Lhalfwf;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_ZERO_RANGE_K0_GPE_POLARON
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r<Rpar12) {
    return Log(fabs(A2SW*BesselK0(trial_k*r)));
  }
  else if(r<Lhalfwf) {
    return Log(A3SW + A4SW*(exp(-Apar*r) + exp(Apar*(r-L))));
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  if(r<Rpar12) {
    return -trial_k*BesselK1(trial_k*r)/BesselK0(trial_k*r);
  }
  else if(r<Lhalfwf) {
    DOUBLE f, fp;

    f = A3SW + A4SW*(exp(-Apar*r) + exp(Apar*(r-L)));
    fp = A4SW*Apar*(-exp(-Apar*r) + exp(Apar*(r-L)));

    return fp/f;
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  if(r<Rpar12) {
    DOUBLE fp;
    fp = InterpolateExactFp12(r);
    return -trial_k2 + fp*fp;
  }
  else if(r<Lhalfwf) {
    DOUBLE f, fp, fpp;

    f = A3SW + A4SW*(exp(-Apar*r) + exp(Apar*(r-L)));
    fp = A4SW*Apar*(-exp(-Apar*r) + exp(Apar*(r-L)));
    fpp = A4SW*Apar*Apar*(exp(-Apar*r) + exp(Apar*(r-L)));

    // [-(f" + f'/r)/f] + (f'/f)^2
    return -(fpp+fp/r)/f + (fp/f)*(fp/f);
  }
  else {
    return 0;
  }
}
#endif

/************************** Construct Grid Zero Range Unitary GPE Polaron *********************/
void ConstructGridZeroRange_Unitary_GPE_Polaron(struct Grid *G) {
  DOUBLE r;

  Message("\n  ConstructGridZeroRange_Unitary_GPE_Polaron:\n");
  //Rpar12 *= Lhalfwf;
  Apar = 1. / Apar;

  // f'(Rpar12)
  r = Rpar12;
  A3SW = 1./(1. + 1./(-cosh((Apar*(L - 2*r))/2.) + (Apar*r*sinh((Apar*(L - 2*r))/2.))));

  // f'(L/2) = 0 automatic
  // f(L/2) = 1
  r = L / 2.;
  //A4SW = - A3SW/(exp(-Apar*r) + exp(Apar*(r-L)));
  A4SW = (1. - A3SW) / (exp(-Apar*r) + exp(Apar*(r - L)));

  // f(Rpar12)
  r = Rpar12;
  A2SW = (A3SW + A4SW*(exp(-Apar*r) + exp(Apar*(r - L)))) * r;

  Message("    A2 = %"LE " \n", A2SW);
  Message("    A3 = %"LE " \n", A3SW);
  Message("    A4 = %"LE " \n", A4SW);

  G->min = 0;
  G->max = Lhalfwf;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_ZERO_RANGE_UNITARY_GPE_POLARON
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r<Rpar12) {
    return Log(fabs(A2SW/r));
  }
  else if(r<Lhalfwf) {
    return Log(A3SW + A4SW*(exp(-Apar*r) + exp(Apar*(r-L))));
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  if(r<Rpar12) {
    return -1. / r;
  }
  else if(r<Lhalfwf) {
    DOUBLE f, fp;

    f = A3SW + A4SW*(exp(-Apar*r) + exp(Apar*(r-L)));
    fp = A4SW*Apar*(-exp(-Apar*r) + exp(Apar*(r-L)));

    return fp/f;
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  if(r<Rpar12) {
    DOUBLE fp;
    return 1./(r*r);
  }
  else if(r<Lhalfwf) {
    DOUBLE f, fp, fpp;

    f = A3SW + A4SW*(exp(-Apar*r) + exp(Apar*(r-L)));
    fp = A4SW*Apar*(-exp(-Apar*r) + exp(Apar*(r-L)));
    fpp = A4SW*Apar*Apar*(exp(-Apar*r) + exp(Apar*(r-L)));

    // [-(f" + 2.*f'/r)/f] + (f'/f)^2
    return -(fpp+2.*fp/r)/f + (fp/f)*(fp/f);
  }
  else {
    return 0;
  }
}
#endif

/************************** Construct Grid Zero Range Log Phonons *************************/
void ConstructGridZeroRangeLogPhonons12(struct Grid *G) {
  DOUBLE r;

  Message("  Jastrow term: TRIAL_ZERO_RANGE_LOG_PHONONS12 trial wavefunction\n");
  Message("     f(r) = A ln(r/a), r<Rpar12; B Exp(- C/r + D/r^2); Rpar12 - var. parameter\n");

  Warning("  Changing Rpar12 to units of L/2, Rpar12 = %lf -> %lf\n", Rpar12, Rpar12*Lhalfwf);
  Rpar12 *= Lhalfwf;

  if(Rpar12 == 0.) {
    Warning("  Rpar12 -> Lwf/2\n", Lhalfwf);
    Rpar12 = Lhalfwf;
  }
  if(a<=0) Error("  parameter 'a' must be positive, a = %lf", a);
  if(Rpar12 > Lhalfwf) Error("  Rpar12 = %lf > L/2 = %lf, must be otherwise", Rpar12, Lhalfwf);
  if(Rpar12 < Ro) Error("  Rpar12 = %lf < Ro = %lf, must be otherwise", Rpar12, Ro);

  AllocateWFGrid(G, 3);

  A4SW = -Rpar12*Rpar12 / ((Lhalfwf-Rpar12)*Log(Rpar12/a));
  r = Lhalfwf;
  A3SW = A4SW/r*(1. - 0.5*Lhalfwf/r);

  r = Rpar12;
  A2SW = Exp(A3SW - A4SW/r*(1. - 0.5*Lhalfwf/r)) / Log(r/a);

  trial_Kappa = sqrt(2.*m_mu*trial_Vo);
  trial_Kappa2 = 2.*m_mu*trial_Vo;

  Message("    A2 = %"LE " \n", A2SW);
  Message("    A3 = %"LE " \n", A3SW);
  Message("    A4 = %"LE " \n", A4SW);

  G->min = 0;
  G->max = Lhalfwf;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_ZERO_RANGE_LOG_PHONONS12
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r<Rpar12) {
    return Log(fabs(A2SW*Log(r/a)));
  }
  else if(r<Lhalfwf) {
    return A3SW - A4SW/r*(1. - 0.5*Lhalfwf/r);
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  if(r<Rpar12) {
    return 1./(r*Log(r/a));
  }
  else if(r<Lhalfwf) {
    return A4SW*(r-Lhalfwf)/(r*r*r);
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  DOUBLE logf1,fp;

  if(r<Rpar12) {
    logf1 = Log(r/a);
    return 1. / (r*r*logf1*logf1);
  }
  else if(r<Lhalfwf) {
    return A4SW*(r-Lwf)/(r*r*r*r);
  }
  else {
    return 0;
  }
}
#endif

/************************** Construct Grid Square well 2D Zero Energy *************************/
// zero energy HD: f(r) = const ln r
void ConstructGridSoftDisks2DZeroEnergyPhonons12(struct Grid *G) {
  DOUBLE r;

  Message("  Jastrow term: TRIAL_SOFT_DISKS_2D_ZERO_ENERGY_PHONONS12 trial wavefunction\n");
  Message("     f(r) = Io(kappa r), r<Ro; A ln(r/a), r<Rpar; B Exp(-C/r + D/r^2); Rpar12 - var. parameter\n");

  Warning("  Changing Rpar12 to units of L/2, Rpar12 = %lf -> %lf\n", Rpar12, Rpar12*Lhalfwf);
  Rpar12 *= Lhalfwf;

  if(Rpar12 == 0.) {
    Warning("  Rpar12 -> Lwf/2\n", Lhalfwf);
    Rpar12 = Lhalfwf;
  }
  if(a<=0) Error("  parameter 'a' must be positive, a = %lf", a);
  if(Rpar12 > Lhalfwf) Error("  Rpar12 = %lf > L/2 = %lf, must be otherwise", Rpar12, Lhalfwf);
  if(Rpar12 < Ro) Error("  Rpar12 = %lf < Ro = %lf, must be otherwise", Rpar12, Ro);

  AllocateWFGrid(G, 3);

  A4SD12 = -Rpar12*Rpar12 / ((Lhalfwf-Rpar12)*Log(Rpar12/a));
  r = Lhalfwf;
  A3SD12 = A4SD12/r*(1. - 0.5*Lhalfwf/r);

  r = Rpar12;
  A2SD12 = Exp(A3SD12 - A4SD12/r*(1. - 0.5*Lhalfwf/r)) / Log(r/a);

  trial_Kappa = sqrt(2.*m_mu*trial_Vo);
  trial_Kappa2 = 2.*m_mu*trial_Vo;
  r = Ro;
  A1SD12 = A2SD12*Log(r/a)/BesselI0(trial_Kappa*r);

  Message("    A1 = %"LE " \n", A1SD12);
  Message("    A2 = %"LE " \n", A2SD12);
  Message("    A3 = %"LE " \n", A3SD12);
  Message("    A4 = %"LE " \n", A4SD12);

  G->min = 0;
  G->max = Lhalfwf;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_SOFT_DISKS_2D_ZERO_ENERGY_PHONONS12
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r<Ro) {
    return Log(fabs(A1SD12*BesselI0(trial_Kappa*r)));
  }
  else if(r<Rpar12) {
    return Log(fabs(A2SD12*Log(r/a)));
  }
  else if(r<Lhalfwf) {
    return A3SD12 - A4SD12/r*(1. - 0.5*Lhalfwf/r);
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  if(r<Ro) {
    return trial_Kappa*BesselI1(trial_Kappa*r)/BesselI0(trial_Kappa*r);
  }
  else if(r<Rpar12) {
    return 1./(r*Log(r/a));
  }
  else if(r<Lhalfwf) {
    return A4SD12*(r-Lhalfwf)/(r*r*r);
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactE12(DOUBLE r) {
// 2D Eloc = [-(f"/f + f'/r)/f] + (f'/f)^2
  DOUBLE logf1,fp;

  if(r<Ro) {
    fp = InterpolateExactFp12(r);
    return -trial_Kappa2 + fp*fp;
  }
  else if(r<Rpar12) {
    logf1 = Log(r/a);
    return 1. / (r*r*logf1*logf1);
  }
  else if(r<Lhalfwf) {
    return A4SD12*(r-Lwf)/(r*r*r*r);
  }
  else {
    return 0;
  }
}

#endif

/************************** Construct Grid Square well 2D Zero Energy *************************/
// zero energy HD: f(r) = const ln r
void ConstructGridSoftDisks2DZeroEnergyPhonons11(struct Grid *G) {
  DOUBLE r;

  Message("  Jastrow term: TRIAL_SOFT_DISKS_2D_ZERO_ENERGY_PHONONS11 trial wavefunction\n");
  Message("     f(r) = Io(kappa r), r<Ro; A ln(r/aA), r<Rpar; B Exp(-C/r + D/r^2); Rpar11 - var. parameter\n");

  Warning("  Changing Rpar11 to units of L/2, Rpar11 = %lf -> %lf\n", Rpar11, Rpar11*Lhalfwf);
  Rpar11 *= Lhalfwf;

  if(Rpar11 == 0.) {
    Warning("  Rpar11 -> Lwf/2\n", Lhalfwf);
    Rpar11 = Lhalfwf;
  }
  if(aA<=0) Error("  parameter 'aA' must be positive, aA = %lf", aA);
  if(Rpar11 > Lhalfwf) Error("  Rpar11 = %lf > L/2 = %lf, must be otherwise", Rpar11, Lhalfwf);
  if(Rpar11 < Ro) Error("  Rpar11 = %lf < Ro = %lf, must be otherwise", Rpar11, Ro);

  AllocateWFGrid(G, 3);

  A4SD11 = -Rpar11*Rpar11 / ((Lhalfwf-Rpar11)*Log(Rpar11/aA));
  r = Lhalfwf;
  A3SD11 = A4SD11/r*(1. - 0.5*Lhalfwf/r);

  r = Rpar11;
  A2SD11 = Exp(A3SD11 - A4SD11/r*(1. - 0.5*Lhalfwf/r)) / Log(r/aA);

  trial11_Kappa = sqrt(trial11_Vo);
  trial11_Kappa2 = trial11_Vo;
  r = Ro;
  A1SD11 = A2SD11*Log(r/aA)/BesselI0(trial11_Kappa*r);

  Message("    A1 = %"LE " \n", A1SD11);
  Message("    A2 = %"LE " \n", A2SD11);
  Message("    A3 = %"LE " \n", A3SD11);
  Message("    A4 = %"LE " \n", A4SD11);

  G->min = 0;
  G->max = Lhalfwf;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_SOFT_DISKS_2D_ZERO_ENERGY_PHONONS11
DOUBLE InterpolateExactU11(DOUBLE r) {
  if(r<Ro) {
    return Log(fabs(A1SD11*BesselI0(trial11_Kappa*r)));
  }
  else if(r<Rpar11) {
    return Log(fabs(A2SD11*Log(r/aA)));
  }
  else if(r<Lhalfwf) {
    return A3SD11 - A4SD11/r*(1. - 0.5*Lhalfwf/r);
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactFp11(DOUBLE r) {
  if(r<Ro) {
    return trial11_Kappa*BesselI1(trial11_Kappa*r)/BesselI0(trial11_Kappa*r);
  }
  else if(r<Rpar11) {
    return 1./(r*Log(r/aA));
  }
  else if(r<Lhalfwf) {
    return A4SD11*(r-Lhalfwf)/(r*r*r);
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactE11(DOUBLE r) {
// 2D Eloc = [-(f"/f + f'/r)/f] + (f'/f)^2
  DOUBLE logf1,fp;

  if(r<Ro) {
    fp = InterpolateExactFp11(r);
    return -trial11_Kappa2 + fp*fp;
  }
  else if(r<Rpar11) {
    logf1 = Log(r/aA);
    return 1. / (r*r*logf1*logf1);
  }
  else if(r<Lhalfwf) {
    return A4SD11*(r-Lwf)/(r*r*r*r);
  }
  else {
    return 0;
  }
}

#endif

/************************** Construct Grid Square well 2D Zero Energy *************************/
// zero energy HD: f(r) = const ln r
void ConstructGridSoftDisks2DZeroEnergyPhonons22(struct Grid *G) {
  DOUBLE r;

  Message("  Jastrow term: TRIAL_SOFT_DISKS_2D_ZERO_ENERGY_PHONONS22 trial wavefunction\n");
  Message("     f(r) = Io(kappa r), r<Ro; A ln(r/aB), r<Rpar; B Exp(-C/r + D/r^2); Rpar22 - var. parameter\n");

  Warning("  Changing Rpar22 to units of L/2, Rpar22 = %lf -> %lf\n", Rpar22, Rpar22*Lhalfwf);
  Rpar22 *= Lhalfwf;

  if(Rpar22 == 0.) {
    Warning("  Rpar22 -> Lwf/2\n", Lhalfwf);
    Rpar22 = Lhalfwf;
  }
  if(aB<=0) Error("  parameter 'aB' must be positive, aB = %lf", aB);
  if(Rpar22 > Lhalfwf) Error("  Rpar22 = %lf > L/2 = %lf, must be otherwise", Rpar22, Lhalfwf);
  if(Rpar22 < Ro) Error("  Rpar22 = %lf < Ro = %lf, must be otherwise", Rpar22, Ro);

  AllocateWFGrid(G, 3);

  A4SD22 = -Rpar22*Rpar22 / ((Lhalfwf-Rpar22)*Log(Rpar22/aB));
  r = Lhalfwf;
  A3SD22 = A4SD22/r*(1. - 0.5*Lhalfwf/r);

  r = Rpar22;
  A2SD22 = Exp(A3SD22 - A4SD22/r*(1. - 0.5*Lhalfwf/r)) / Log(r/aB);

  trial22_Kappa = sqrt(trial22_Vo);
  trial22_Kappa2 = trial22_Vo;
  r = Ro;
  A1SD22 = A2SD22*Log(r/aB)/BesselI0(trial22_Kappa*r);

  Message("    A1 = %"LE " \n", A1SD22);
  Message("    A2 = %"LE " \n", A2SD22);
  Message("    A3 = %"LE " \n", A3SD22);
  Message("    A4 = %"LE " \n", A4SD22);

  G->min = 0;
  G->max = Lhalfwf;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_SOFT_DISKS_2D_ZERO_ENERGY_PHONONS22
DOUBLE InterpolateExactU22(DOUBLE r) {
  if(r<Ro) {
    return Log(fabs(A1SD22*BesselI0(trial22_Kappa*r)));
  }
  else if(r<Rpar22) {
    return Log(fabs(A2SD22*Log(r/aB)));
  }
  else if(r<Lhalfwf) {
    return A3SD22 - A4SD22/r*(1. - 0.5*Lhalfwf/r);
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactFp22(DOUBLE r) {
  if(r<Ro) {
    return trial22_Kappa*BesselI1(trial22_Kappa*r)/BesselI0(trial22_Kappa*r);
  }
  else if(r<Rpar22) {
    return 1./(r*Log(r/aB));
  }
  else if(r<Lhalfwf) {
    return A4SD22*(r-Lhalfwf)/(r*r*r);
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactE22(DOUBLE r) {
// 2D Eloc = [-(f"/f + f'/r)/f] + (f'/f)^2
  DOUBLE logf1,fp;

  if(r<Ro) {
    fp = InterpolateExactFp22(r);
    return -trial22_Kappa2 + fp*fp;
  }
  else if(r<Rpar22) {
    logf1 = Log(r/aB);
    return 1. / (r*r*logf1*logf1);
  }
  else if(r<Lhalfwf) {
    return A4SD22*(r-Lwf)/(r*r*r*r);
  }
  else {
    return 0;
  }
}

#endif

/************************** Construct Grid Dipole **************************/
void ConstructGridDipoleKoTrap(struct Grid *G) {
  AllocateWFGrid(G, 3);

  Message("  2D Dipole Ko(r) Trap trial wavefunction\n");

  G->min = 0;
  G->max = Lhalf;
  G->max2 = G->max*G->max;
}

/* executed only if TRIAL_DIPOLE_Ko is defined */
#ifdef TRIAL_DIPOLE_Ko_TRAP1122
DOUBLE InterpolateExactU11(DOUBLE r) {
    return Log(K0opt(r));
}

DOUBLE InterpolateExactFp11(DOUBLE r) {
  return K1opt(r) / (K0opt(r)*r*Sqrt(r));
}

DOUBLE InterpolateExactE11(DOUBLE r) {
  DOUBLE t;
  t = K1opt(r) / K0opt(r);
  return (t*t - 1) / (r*r*r);
}

DOUBLE InterpolateExactU22(DOUBLE r) {
  return Log(K0opt(r));
}

DOUBLE InterpolateExactFp22(DOUBLE r) {
  return K1opt(r) / (K0opt(r)*r*Sqrt(r));
}

DOUBLE InterpolateExactE22(DOUBLE r) {
  DOUBLE t;
  t = K1opt(r) / K0opt(r);
  return (t*t - 1) / (r*r*r);
}
#endif


/************************** Construct Grid Schiff Verlett ******************/
void ConstructGridSchiffVerlett11_Trap(struct Grid *G) {
  AllocateWFGrid(G, 3);

  Message("  TRIAL_SCHIFF_VERLET11_TRAP wavefunction\n");

  G->min = 0;
  G->max = Lhalf;
  G->max2 = G->max*G->max;
}

/* executed only if TRIAL_SCHIFF_VERLET is defined */
#ifdef TRIAL_SCHIFF_VERLET11_TRAP
DOUBLE InterpolateExactU11(DOUBLE r) {
  return -Apar*pow(r, -Bpar);
}

DOUBLE InterpolateExactFp11(DOUBLE r) {
  return Apar*Bpar*pow(r, -Bpar-1.);
}

DOUBLE InterpolateExactE11(DOUBLE r) {
//1D Eloc = [-f"/f] + (f'/f)^2
#ifdef TRIAL_1D
  return Apar*Bpar*(Bpar+1.)*pow(r, -Bpar-2.);
#endif

//2D Eloc = [-(f"/f + f'/r)/f] + (f'/f)^2
#ifdef TRIAL_2D
  return Apar*Bpar*Bpar*pow(r, -Bpar-2.);
#endif

//3D Eloc = [-(f"/f + 2f'/r)/f] + (f'/f)^2
#ifdef TRIAL_3D
  return Apar*Bpar*(Bpar-1.)*pow(r, -Bpar-2.);
#endif
}
#endif

/************************** Construct Grid SW FS phonons ******************/
void ConstructGridSquareWellFreeStatePhonons12(struct Grid *G) {
  DOUBLE kappa, k, r;

  Message("\n Jastrow 12 term: SquareWellFreeStatePhonons\n");
  // trial_Vo = kappa*kappa/(2.*m_mu);
  kappa = sqrt(2.*m_mu*trial_Vo);

  k = Bpar12/(2.*PI*Lwf);
  Message(" Scattering energy parameter k = Bpar12/(2*pi*L) = %e\n", k);

  sqE = k;
  sqE2 = k*k;
  trial_Kappa2 = kappa*kappa + k*k;
  trial_Kappa = sqrt(trial_Kappa2);

  // matching at Rpar12
  if(Rpar12>1 || Rpar12 <0) Error(" Rpar12 out of (0,1) range!\n");
  Warning("  Matching distance Rpar12 = %lf [L/2] -> %lf [R]\n", Rpar12, Rpar12*Lhalf);
  Rpar12 *= Lhalf;
  if(RoSW>Rpar12) Error("  Rpar12 = %lf must be larger than the range of the potential Ro = %lf", Rpar12, RoSW);

  // f'(Ro)
  r = RoSW;
  trial_delta = atan(sqE*tg(trial_Kappa*r)/trial_Kappa) - sqE*r;
  if(trial_delta + sqE*Rpar12<0) {
    trial_delta += PI; // choose proper phase
    Warning("  Adjusting phase\n");
  }

  // f'(Rpar12)
  r = Rpar12;
  A5SW = (sqE/tg(trial_delta + sqE*r) - 1./r)/(2.*(1./(r*r*r)-1./((Lwf-r)*(Lwf-r)*(Lwf-r))));

  // f(L/2)
  r = Lhalfwf;
  A2SW = A5SW*(1./(r*r)+1./((Lwf-r)*(Lwf-r)));

  // f(Rpar12)
  r = Rpar12;
  A3SW = A2SW - A5SW*(1./(r*r)+1./((Lwf-r)*(Lwf-r))) - Log(Sin(trial_delta + sqE*r)/r);

  // f(Ro)
  r = RoSW;
  A1SW = A3SW + Log(Sin(trial_delta + sqE*r)/Sin(trial_Kappa*r));

  G->min = 0;
  G->max = Lhalf;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_SQUARE_WELL_FS_PHONONS12
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r<RoSW)
    return A1SW + Log(Sin(trial_Kappa*r)/r);
  else if(r<Rpar12)
    return A3SW + Log(Sin(trial_delta + sqE*r)/r);
  else
    return A2SW - A5SW*(1./(r*r)+1./((Lwf-r)*(Lwf-r)));
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  if(r<RoSW)
    return trial_Kappa/tg(trial_Kappa*r) - 1./r;
  else if(r<Rpar12)
    return sqE/tg(trial_delta + sqE*r) - 1./r;
  else
    return 2.*A5SW*(1./(r*r*r)-1./((Lwf-r)*(Lwf-r)*(Lwf-r)));
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  DOUBLE fp;

  if(r<RoSW) {
    fp = trial_Kappa/tg(trial_Kappa*r) - 1./r;
    return trial_Kappa2 + fp*fp;
  }
  else if(r<Rpar12){
    fp = sqE/tg(trial_delta + sqE*r) - 1./r;
    return sqE2 + fp*fp;
  }
  else
    return 2.*A5SW*(Lwf*Lwf*Lwf*L - 4.*Lwf*Lwf*Lwf*r + 6.*Lwf*Lwf*r*r - 2.*Lwf*r*r*r + 2.*r*r*r*r)/(r*r*r*r*(Lwf-r)*(Lwf-r)*(Lwf-r)*(Lwf-r));
}
#endif

/************************** Construct Grid SW FS phonons ******************/
void ConstructGridSquareWellFreeStateGPEPolaron12(struct Grid *G) {
  DOUBLE kappa, k, r, Fp2;

  Message("\n Jastrow 12 term: SquareWellFreeStateGPEPhonons\n");
  // trial_Vo = kappa*kappa/(2.*m_mu);
  kappa = sqrt(2.*m_mu*trial_Vo);

  k = Bpar12/(2.*PI*Lwf);
  Message(" Scattering energy parameter k = Bpar12/(2*pi*L) = %e\n", k);

  sqE = k;
  sqE2 = k*k;
  trial_Kappa2 = kappa*kappa + k*k;
  trial_Kappa = sqrt(trial_Kappa2);

  if(Rpar12>1 || Rpar12 <0) Error(" Rpar12 out of (0,1) range!\n");
  Warning("  Matching distance Rpar12 = %lf [L/2] -> %lf [R]\n", Rpar12, Rpar12*Lhalf);
  Rpar12 *= Lhalf;
  if(RoSW>Rpar12) Error("  Rpar12 = %lf must be larger than the range of the potential Ro = %lf", Rpar12, RoSW);

  Apar = 1. / Apar;
  Message("  Converting Apar -> 1/Apar = %lf\n", Apar);

  // f'(Ro)
  r = RoSW;
  trial_delta = atan(sqE*tg(trial_Kappa*r)/trial_Kappa) - sqE*r;
  if(trial_delta + sqE*Rpar12<0) {
    trial_delta += PI; // choose proper phase
    Warning("  Adjusting phase\n");
  }

  // f'(Rpar12)
  r = Rpar12;
  Fp2 = sqE/tg(trial_delta + sqE*r) - 1./r;
  A3SW = 1. + 1./(-1. + cosh((Apar*(L - 2.*r))/2.) + (Apar*sinh((Apar*(L - 2.*r))/2.))/Fp2);
  
  // f'(L/2) = 0 automatic
  // f(L/2) = 1
  r = L / 2.;
  A4SW = (1. - A3SW) / (exp(-Apar*r) + exp(Apar*(r - L)));

  // f(Rpar12)
  r = Rpar12;
  A2SW = Log(A3SW + A4SW*(exp(-Apar*r) + exp(Apar*(r-L)))) - Log(Sin(trial_delta + sqE*r)/r);

  // f(Ro)
  r = RoSW;
  A1SW = A2SW + Log(Sin(trial_delta + sqE*r)/Sin(trial_Kappa*r));

  G->min = 0;
  G->max = Lhalf;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_SQUARE_WELL_FS_GPE_POLARON12
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r<RoSW)
    return A1SW + Log(Sin(trial_Kappa*r)/r);
  else if(r<Rpar12)
    return A2SW + Log(Sin(trial_delta + sqE*r)/r);
  else if(r<Lhalfwf)
    return Log(A3SW + A4SW*(exp(-Apar*r) + exp(Apar*(r-L))));
  else
    return 0;
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  if(r<RoSW)
    return trial_Kappa/tg(trial_Kappa*r) - 1./r;
  else if(r<Rpar12)
    return sqE/tg(trial_delta + sqE*r) - 1./r;
  else if(r<Lhalfwf) {
    DOUBLE f, fp;

    f = A3SW + A4SW*(exp(-Apar*r) + exp(Apar*(r-L)));
    fp = A4SW*Apar*(-exp(-Apar*r) + exp(Apar*(r-L)));

    return fp/f;
  }
  else {
    return 0;
  }
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  DOUBLE fp;

  if(r<RoSW) {
    fp = trial_Kappa/tg(trial_Kappa*r) - 1./r;
    return trial_Kappa2 + fp*fp;
  }
  else if(r<Rpar12){
    fp = sqE/tg(trial_delta + sqE*r) - 1./r;
    return sqE2 + fp*fp;
  }
  else if(r<Lhalfwf) {
    DOUBLE f, fp, fpp;

    f = A3SW + A4SW*(exp(-Apar*r) + exp(Apar*(r-L)));
    fp = A4SW*Apar*(-exp(-Apar*r) + exp(Apar*(r-L)));
    fpp = A4SW*Apar*Apar*(exp(-Apar*r) + exp(Apar*(r-L)));

    // [-(f" + f'/r)/f] + (f'/f)^2
    return -(fpp+2.*fp/r)/f + (fp/f)*(fp/f);
  }
  else {
    return 0;
  }
}
#endif

/************************** Construct Grid SW FS phonons ******************/
void ConstructGridSquareWellFreeStatePhonons11(struct Grid *G) {
  DOUBLE kappa, k, r;

  Message("\n Jastrow 11 term: SquareWellFreeStatePhonons\n");
  kappa = sqrt(m_up*trial_Vo);

  k = Bpar11/(2.*PI*Lwf);
  Message(" Scattering energy parameter k = Bpar11/(2*pi*L) = %e\n", k);

  sqE = k;
  sqE2 = k*k;
  trial_Kappa2 = kappa*kappa + k*k;
  trial_Kappa = sqrt(trial_Kappa2);

  // matching at Rpar11
  if(Rpar11>1 || Rpar11 <0) Error(" Rpar11 out of (0,1) range!\n");
  Warning("  Matching distance Rpar11 = %lf [L/2] -> %lf [R]\n", Rpar11, Rpar11*Lhalf);
  Rpar11 *= Lhalf;

  // f'(1)
  r = 1.;
  trial_delta = atan(sqE*tg(trial_Kappa*r)/trial_Kappa) - sqE*r;
  if(trial_delta + sqE*Rpar11<0) trial_delta += PI; // choose proper phase

  // f'(Rpar11)
  r = Rpar11;
  A5SW11 = (sqE/tg(trial_delta + sqE*r) - 1./r)/(2.*(1./(r*r*r)-1./((Lwf-r)*(Lwf-r)*(Lwf-r))));

  // f(L/2)
  r = Lhalfwf;
  A2SW11 = A5SW11*(1./(r*r)+1./((Lwf-r)*(Lwf-r)));

  // f(Rpar11)
  r = Rpar11;
  A3SW11 = A2SW11 - A5SW11*(1./(r*r)+1./((Lwf-r)*(Lwf-r))) - Log(Sin(trial_delta + sqE*r)/r);

  // f(1)
  r = 1;
  A1SW11 = A3SW11 + Log(Sin(trial_delta + sqE*r)/r) - Log(Sin(trial_Kappa*r)/r);

  G->min = 0;
  G->max = Lhalf;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_SQUARE_WELL_FS_PHONONS11
DOUBLE InterpolateExactU11(DOUBLE r) {
  if(r<1)
    return A1SW11+Log(Sin(trial_Kappa*r)/r);
  else if(r<Rpar11)
    return A3SW11 + Log(Sin(trial_delta + sqE*r)/r);
  else
    return A2SW11 - A5SW11*(1./(r*r)+1./((Lwf-r)*(Lwf-r)));
}

DOUBLE InterpolateExactFp11(DOUBLE r) {
  if(r<1)
    return trial_Kappa/tg(trial_Kappa*r) - 1./r;
  else if(r<Rpar11)
    return sqE/tg(trial_delta + sqE*r) - 1./r;
  else
    return 2.*A5SW11*(1./(r*r*r)-1./((Lwf-r)*(Lwf-r)*(Lwf-r)));
}

DOUBLE InterpolateExactE11(DOUBLE r) {
  DOUBLE fp;

  if(r<1) {
    fp = trial_Kappa/tg(trial_Kappa*r) - 1./r;
    return trial_Kappa2 + fp*fp;
  }
  else if(r<Rpar11){
    fp = sqE/tg(trial_delta + sqE*r) - 1./r;
    return sqE2 + fp*fp;
  }
  else
    return 2.*A5SW11*(Lwf*Lwf*Lwf*L - 4.*Lwf*Lwf*Lwf*r + 6.*Lwf*Lwf*r*r - 2.*Lwf*r*r*r + 2.*r*r*r*r)/(r*r*r*r*(Lwf-r)*(Lwf-r)*(Lwf-r)*(Lwf-r));
}
#endif

/************************** Construct Grid SS ****************************/
//R is the size of a soft sphere
//Rpar is the matching distance 
void ConstructGridSSExp12(struct Grid *G) {
  DOUBLE x;
  int size = 3;

  Warning("Converting Rpar12 = %lf -> %lf using L/2\n", Rpar12, Rpar12*Lhalf);
  Rpar12 *= Lhalf;

  if(Rpar12>Lhalf) Error("Matching distance %"LE " is larger than L/2=%"LE " \n", Rpar12, Lhalf);

  AllocateWFGrid(G, size);

  sqEkappa2 = 2.*m_mu*trial_Vo;
  sqEkappa = sqrt(2.*m_mu*trial_Vo);
  Message("  potential height Vo = %lf\n", trial_Vo);
  Message("  potential size   Ro = %lf\n", Ro);
  Message("  s-wave scattering length  a/R = %lf\n", 1-tanh(sqEkappa*Ro)/(sqEkappa*Ro));
  Message("  Jastrow term Exp(-Bpar12 r^2) with Bpar12 = %lf\n", Bpar12);

  x = Rpar12;
  Ctrial12 = Bpar12*x/((1./(x*x*x)-1./((Lwf-x)*(Lwf-x)*(Lwf-x))));

  x = Lhalfwf;
  Btrial12 = Ctrial12/(x*x) + Ctrial12/((L-x)*(L-x));

  x = Rpar12;
  Atrial12 = -Bpar12*x*x + Btrial12 - Ctrial12/(x*x) - Ctrial12/((Lwf-x)*(Lwf-x));

  G->min = 0;
  G->max = Lhalfwf;
  G->step = (G->max - G->min) / (DOUBLE) (size-1);
  G->I_step = (DOUBLE) size / (G->max - G->min);
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_SS_EXP12
DOUBLE InterpolateExactU12(DOUBLE x) {
  if(x<Rpar12) {
    return Atrial12 + Bpar12*x*x;
  }
  else {
    return Btrial12 - Ctrial12/(x*x) - Ctrial12/((Lwf-x)*(Lwf-x));
  }
}

DOUBLE InterpolateExactFp12(DOUBLE x) {
  if(x<Rpar12) {
    return 2.*Bpar12*x;
  }
  else {
    return 2.*Ctrial12*(1./(x*x*x)-1./((Lwf-x)*(Lwf-x)*(Lwf-x)));
  }
}

DOUBLE InterpolateExactE12(DOUBLE x) {
  if(x<Rpar12) {
    return -6.*Bpar12;
  }
  else {
   return (2.*Ctrial12*(Lwf*Lwf*Lwf*Lwf - 4.*Lwf*Lwf*Lwf*x + 6.*Lwf*Lwf*x*x - 2.*Lwf*x*x*x + 2.*x*x*x*x))/((Lwf-x)*(Lwf-x)*(Lwf-x)*(Lwf-x)*x*x*x*x);
  }
}
#endif

/************************** Construct Grid SS ****************************/
//R is the size of a soft sphere
//Rpar is the matching distance 
void ConstructGridSSExp11(struct Grid *G) {
  DOUBLE x;
  int size = 3;

  Warning("Converting Rpar11 = %lf -> %lf using L/2\n", Rpar11, Rpar11*Lhalf);
  Rpar11 *= Lhalf;

  if(Rpar11>Lhalf) Error("Matching distance Rpar11 = %"LE " is larger than L/2=%"LE " \n", Rpar11, Lhalf);

  AllocateWFGrid(G, size);

  sqEkappa2 = trial11_Vo;
  sqEkappa = sqrt(trial11_Vo);
  Message("  potential height Vo = %lf\n", trial11_Vo);
  Message("  potential size   Ro = %lf\n", Ro);
  Message("  s-wave scattering length  a/R = %lf\n", 1-tanh(sqEkappa*Ro)/(sqEkappa*Ro));
  Message("  Jastrow term Exp(-Bpar11 r^2) with Bpar11 = %lf\n", Bpar11);

  x = Rpar11;
  Ctrial11 = Bpar11*x/((1./(x*x*x)-1./((Lwf-x)*(Lwf-x)*(Lwf-x))));

  x = Lhalfwf;
  Btrial11 = Ctrial11/(x*x) + Ctrial11/((L-x)*(L-x));

  x = Rpar11;
  Atrial11 = -Bpar11*x*x + Btrial11 - Ctrial11/(x*x) - Ctrial11/((Lwf-x)*(Lwf-x));

  G->min = 0;
  G->max = Lhalfwf;
  G->step = (G->max - G->min) / (DOUBLE) (size-1);
  G->I_step = (DOUBLE) size / (G->max - G->min);
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_SS_EXP11
DOUBLE InterpolateExactU11(DOUBLE x) {
  if(x<Rpar11) {
    return Atrial11 + Bpar11*x*x;
  }
  else {
    return Btrial11 - Ctrial11/(x*x) - Ctrial11/((Lwf-x)*(Lwf-x));
  }
}

DOUBLE InterpolateExactFp11(DOUBLE x) {
  if(x<Rpar11) {
    return 2.*Bpar11*x;
  }
  else {
    return 2.*Ctrial11*(1./(x*x*x)-1./((Lwf-x)*(Lwf-x)*(Lwf-x)));
  }
}

DOUBLE InterpolateExactE11(DOUBLE x) {
  if(x<Rpar11) {
    return -6.*Bpar11;
  }
  else {
   return (2.*Ctrial11*(Lwf*Lwf*Lwf*Lwf - 4.*Lwf*Lwf*Lwf*x + 6.*Lwf*Lwf*x*x - 2.*Lwf*x*x*x + 2.*x*x*x*x))/((Lwf-x)*(Lwf-x)*(Lwf-x)*(Lwf-x)*x*x*x*x);
  }
}
#endif

/************************** Construct Grid SS ****************************/
//R is the size of a soft sphere
//Rpar is the matching distance 
void ConstructGridSSExp22(struct Grid *G) {
  DOUBLE x;
  int size = 3;

  Warning("Converting Rpar22 = %lf -> %lf using L/2\n", Rpar22, Rpar22*Lhalf);
  Rpar22 *= Lhalf;

  if(Rpar22>Lhalf) Error("Matching distance Rpar22 = %"LE " is larger than L/2=%"LE " \n", Rpar22, Lhalf);

  AllocateWFGrid(G, size);

  sqEkappa2 = trial22_Vo;
  sqEkappa = sqrt(trial22_Vo);
  Message("  potential height Vo = %lf\n", trial22_Vo);
  Message("  potential size   Ro = %lf\n", Ro);
  Message("  s-wave scattering length  a/R = %lf\n", 1-tanh(sqEkappa*Ro)/(sqEkappa*Ro));
  Message("  Jastrow term Exp(-Bpar22 r^2) with Bpar22 = %lf\n", Bpar22);

  x = Rpar22;
  Ctrial22 = Bpar22*x/((1./(x*x*x)-1./((Lwf-x)*(Lwf-x)*(Lwf-x))));

  x = Lhalfwf;
  Btrial22 = Ctrial22/(x*x) + Ctrial22/((L-x)*(L-x));

  x = Rpar22;
  Atrial22 = -Bpar22*x*x + Btrial22 - Ctrial22/(x*x) - Ctrial22/((Lwf-x)*(Lwf-x));

  G->min = 0;
  G->max = Lhalfwf;
  G->step = (G->max - G->min) / (DOUBLE) (size-1);
  G->I_step = (DOUBLE) size / (G->max - G->min);
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_SS_EXP22
DOUBLE InterpolateExactU22(DOUBLE x) {
  if(x<Rpar22) {
    return Atrial22 + Bpar22*x*x;
  }
  else {
    return Btrial22 - Ctrial22/(x*x) - Ctrial22/((Lwf-x)*(Lwf-x));
  }
}

DOUBLE InterpolateExactFp22(DOUBLE x) {
  if(x<Rpar22) {
    return 2.*Bpar22*x;
  }
  else {
    return 2.*Ctrial22*(1./(x*x*x)-1./((Lwf-x)*(Lwf-x)*(Lwf-x)));
  }
}

DOUBLE InterpolateExactE22(DOUBLE x) {
  if(x<Rpar22) {
    return -6.*Bpar22;
  }
  else {
   return (2.*Ctrial22*(Lwf*Lwf*Lwf*Lwf - 4.*Lwf*Lwf*Lwf*x + 6.*Lwf*Lwf*x*x - 2.*Lwf*x*x*x + 2.*x*x*x*x))/((Lwf-x)*(Lwf-x)*(Lwf-x)*(Lwf-x)*x*x*x*x);
  }
}
#endif

/************************** Construct Grid SS phonons *********************/
void ConstructGridSoftSpherePhonons11(struct Grid *G, DOUBLE Ro) {
  DOUBLE kappa, k, r;

  Message(" Jastrow term: ConstructGridSoftSpherePhonons11\n");
  kappa = sqrt(trial11_Vo);

  k = Bpar11/(2.*PI*Lwf);
  Message(" Scattering energy parameter k = Bpar11/(2*pi*L) = %lf\n", k);

  sqE11 = k;
  sqE211 = k*k;
  trial11_Kappa2 = kappa*kappa - k*k;
  if(trial11_Kappa2<0) Error("  Scattering energy must be smaller than the height of the potential, reduce Bpar11\n");
  trial11_Kappa = sqrt(trial11_Kappa2);

  // matching at Rpar11
  if(Rpar11>=1 || Rpar11 <0) Error(" Rpar11 out of (0,1) range!\n");
  Warning("  Matching distance Rpar11 = %lf [L/2] -> %lf [R]\n", Rpar11, Rpar11*Lhalf);
  Rpar11 *= Lhalf;

  if(Rpar11<Ro) Error(" Rpar11 is below its minimal value %lf: Rpar * Lhalf > Ro\n", Ro/Lhalf);

  // f'(Ro)
  r = Ro;
  trial11_delta = atan(sqE11*tanh(trial11_Kappa*r) / trial11_Kappa) - sqE11*r;
  if(trial11_delta + sqE11*Rpar11<0) {
    trial11_delta += PI; // choose proper phase
    Warning("  choosing proper phase for the solution\n");
  }

  // f'(Rpar11)
  r = Rpar11;
  A5SW11 = (sqE11/tg(trial11_delta + sqE11*r) - 1./r)/(2.*(1./(r*r*r)-1./((Lwf-r)*(Lwf-r)*(Lwf-r))));

  // f(L/2)
  r = Lhalfwf;
  A2SW11 = A5SW11*(1./(r*r)+1./((Lwf-r)*(Lwf-r)));

  // f(Rpar11)
  r = Rpar11;
  A3SW11 = A2SW11 - A5SW11*(1./(r*r)+1./((Lwf-r)*(Lwf-r))) - Log(Sin(trial11_delta + sqE11*r)/r);

  // f(Rpar)
  r = Ro;
  A1SW11 = A3SW11 + Log(Sin(trial11_delta + sqE11*r) / sinh(trial11_Kappa*r));

  G->min = 0;
  G->max = Lhalf;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_SS_PHONONS11
DOUBLE InterpolateExactU11(DOUBLE r) {
  if(r<Ro11)
    return A1SW11 + Log(sinh(trial11_Kappa*r)/r);
  else if(r<Rpar11)
    return A3SW11 + Log(Sin(trial11_delta + sqE11*r)/r);
  else if(r<Lhalf)
    return A2SW11 - A5SW11*(1./(r*r)+1./((Lwf-r)*(Lwf-r)));
  else
    return 0;
}

DOUBLE InterpolateExactFp11(DOUBLE r) {
  if(r<Ro11)
    return trial11_Kappa/tanh(trial11_Kappa*r) - 1./r;
  else if(r<Rpar11)
    return sqE11/tg(trial11_delta + sqE11*r) - 1./r;
  else if(r<Lhalf)
    return 2.*A5SW11*(1./(r*r*r)-1./((Lwf-r)*(Lwf-r)*(Lwf-r)));
  else
    return 0;
}

DOUBLE InterpolateExactE11(DOUBLE r) {
  DOUBLE fp;

  if(r<Ro11) {
    fp = trial11_Kappa/tanh(trial11_Kappa*r) - 1./r;
    return -trial11_Kappa2 + fp*fp;
  }
  else if(r<Rpar11){
    fp = sqE11/tg(trial11_delta + sqE11*r) - 1./r;
    return sqE211 + fp*fp;
  }
  else if(r<Lhalf)
    return 2.*A5SW11*(Lwf*Lwf*Lwf*L - 4.*Lwf*Lwf*Lwf*r + 6.*Lwf*Lwf*r*r - 2.*Lwf*r*r*r + 2.*r*r*r*r)/(r*r*r*r*(Lwf-r)*(Lwf-r)*(Lwf-r)*(Lwf-r));
  else 
    return 0;
}
#endif

/************************** Construct Grid SS phonons *********************/
void ConstructGridSoftSpherePhonons12(struct Grid *G, DOUBLE Ro) {
  DOUBLE kappa, k, r;

  Message(" Jastrow term: ConstructGridSoftSpherePhonons12\n");
  kappa = sqrt(2.*m_mu*trial_Vo);

  Message(" Scattering energy parameter k = Bpar12/(2*pi*L)\n");
  k = Bpar12/(2.*PI*Lwf);

  sqE12 = k;
  sqE212 = k*k;
  trial12_Kappa2 = kappa*kappa - k*k;
  if(trial12_Kappa2<0) Error("  Scattering energy must be smaller than the height of the potential, reduce Bpar12\n");
  trial12_Kappa = sqrt(trial12_Kappa2);

  // matching at Rpar12
  if(Rpar12>=1 || Rpar12 <0) Error(" Rpar12 out of (0,1) range!\n");
  Warning("  Matching distance Rpar12 = %lf [L/2] -> %lf [R]\n", Rpar12, Rpar12*Lhalf);
  Rpar12 *= Lhalf;

  if(Rpar12<Ro) Error(" Rpar12 is below its minimal value %lf: Rpar * Lhalf > Ro\n", Ro/Lhalf);

  // f'(Ro)
  r = Ro;
  trial12_delta = atan(sqE12*tanh(trial12_Kappa*r) / trial12_Kappa) - sqE12*r;
  if(trial12_delta + sqE12*Rpar12<0) {
    trial12_delta += PI; // choose proper phase
    Warning("  choosing proper phase for the solution\n");
  }

  // f'(Rpar12)
  r = Rpar12;
  A5SW12 = (sqE12/tg(trial12_delta + sqE12*r) - 1./r)/(2.*(1./(r*r*r)-1./((Lwf-r)*(Lwf-r)*(Lwf-r))));

  // f(L/2)
  r = Lhalfwf;
  A2SW12 = A5SW12*(1./(r*r)+1./((Lwf-r)*(Lwf-r)));

  // f(Rpar12)
  r = Rpar12;
  A3SW12 = A2SW12 - A5SW12*(1./(r*r)+1./((Lwf-r)*(Lwf-r))) - Log(Sin(trial12_delta + sqE12*r)/r);

  // f(Rpar)
  r = Ro;
  A1SW12 = A3SW12 + Log(Sin(trial12_delta + sqE12*r) / sinh(trial12_Kappa*r));

  G->min = 0;
  G->max = Lhalf;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_SS_PHONONS12
DOUBLE InterpolateExactU12(DOUBLE r) {
  if(r<Ro12)
    return A1SW12 + Log(sinh(trial12_Kappa*r)/r);
  else if(r<Rpar12)
    return A3SW12 + Log(Sin(trial12_delta + sqE12*r)/r);
  else if(r<Lhalf)
    return A2SW12 - A5SW12*(1./(r*r)+1./((Lwf-r)*(Lwf-r)));
  else
    return 0;
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  if(r<Ro12)
    return trial12_Kappa/tanh(trial12_Kappa*r) - 1./r;
  else if(r<Rpar12)
    return sqE12/tg(trial12_delta + sqE12*r) - 1./r;
  else if(r<Lhalf)
    return 2.*A5SW12*(1./(r*r*r)-1./((Lwf-r)*(Lwf-r)*(Lwf-r)));
  else
    return 0;
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  DOUBLE fp;

  if(r<Ro12) {
    fp = trial12_Kappa/tanh(trial12_Kappa*r) - 1./r;
    return -trial12_Kappa2 + fp*fp;
  }
  else if(r<Rpar12){
    fp = sqE12/tg(trial12_delta + sqE12*r) - 1./r;
    return sqE212 + fp*fp;
  }
  else if(r<Lhalf)
    return 2.*A5SW12*(Lwf*Lwf*Lwf*L - 4.*Lwf*Lwf*Lwf*r + 6.*Lwf*Lwf*r*r - 2.*Lwf*r*r*r + 2.*r*r*r*r)/(r*r*r*r*(Lwf-r)*(Lwf-r)*(Lwf-r)*(Lwf-r));
  else 
    return 0;
}
#endif

/************************** Construct Grid SS phonons *********************/
void ConstructGridSoftSpherePhonons22(struct Grid *G, DOUBLE Ro) {
  DOUBLE kappa, k, r;

  Message(" Jastrow term: ConstructGridSoftSpherePhonons22\n");
  kappa = sqrt(trial22_Vo);

  Message(" Scattering energy parameter k = Bpar22/(2*pi*L)\n");
  k = Bpar22/(2.*PI*Lwf);

  sqE22 = k;
  sqE222 = k*k;
  trial22_Kappa2 = kappa*kappa - k*k;
  if(trial22_Kappa2<0) Error("  Scattering energy must be smaller than the height of the potential, reduce Bpar22\n");
  trial22_Kappa = sqrt(trial22_Kappa2);

  // matching at Rpar22
  if(Rpar22>=1 || Rpar22 <0) Error(" Rpar22 out of (0,1) range!\n");
  Warning("  Matching distance Rpar22 = %lf [L/2] -> %lf [R]\n", Rpar22, Rpar22*Lhalf);
  Rpar22 *= Lhalf;

  if(Rpar22<Ro) Error(" Rpar22 is below its minimal value %lf: Rpar * Lhalf > Ro\n", Ro/Lhalf);

  // f'(Ro)
  r = Ro;
  trial22_delta = atan(sqE22*tanh(trial22_Kappa*r) / trial22_Kappa) - sqE22*r;
  if(trial22_delta + sqE22*Rpar22<0) {
    trial22_delta += PI; // choose proper phase
    Warning("  choosing proper phase for the solution\n");
  }

  // f'(Rpar22)
  r = Rpar22;
  A5SW22 = (sqE22/tg(trial22_delta + sqE22*r) - 1./r)/(2.*(1./(r*r*r)-1./((Lwf-r)*(Lwf-r)*(Lwf-r))));

  // f(L/2)
  r = Lhalfwf;
  A2SW22 = A5SW22*(1./(r*r)+1./((Lwf-r)*(Lwf-r)));

  // f(Rpar22)
  r = Rpar22;
  A3SW22 = A2SW22 - A5SW22*(1./(r*r)+1./((Lwf-r)*(Lwf-r))) - Log(Sin(trial22_delta + sqE22*r)/r);

  // f(Rpar)
  r = Ro;
  A1SW22 = A3SW22 + Log(Sin(trial22_delta + sqE22*r) / sinh(trial22_Kappa*r));

  G->min = 0;
  G->max = Lhalf;
  G->max2 = G->max*G->max;
}

#ifdef TRIAL_SS_PHONONS22
DOUBLE InterpolateExactU22(DOUBLE r) {
  if(r<Ro22)
    return A1SW22 + Log(sinh(trial22_Kappa*r)/r);
  else if(r<Rpar22)
    return A3SW22 + Log(Sin(trial22_delta + sqE22*r)/r);
  else if(r<Lhalf)
    return A2SW22 - A5SW22*(1./(r*r)+1./((Lwf-r)*(Lwf-r)));
  else
    return 0;
}

DOUBLE InterpolateExactFp22(DOUBLE r) {
  if(r<Ro22)
    return trial22_Kappa/tanh(trial22_Kappa*r) - 1./r;
  else if(r<Rpar22)
    return sqE22/tg(trial22_delta + sqE22*r) - 1./r;
  else if(r<Lhalf)
    return 2.*A5SW22*(1./(r*r*r)-1./((Lwf-r)*(Lwf-r)*(Lwf-r)));
  else
    return 0;
}

DOUBLE InterpolateExactE22(DOUBLE r) {
  DOUBLE fp;

  if(r<Ro22) {
    fp = trial22_Kappa/tanh(trial22_Kappa*r) - 1./r;
    return -trial22_Kappa2 + fp*fp;
  }
  else if(r<Rpar22){
    fp = sqE22/tg(trial22_delta + sqE22*r) - 1./r;
    return sqE222 + fp*fp;
  }
  else if(r<Lhalf)
    return 2.*A5SW22*(Lwf*Lwf*Lwf*L - 4.*Lwf*Lwf*Lwf*r + 6.*Lwf*Lwf*r*r - 2.*Lwf*r*r*r + 2.*r*r*r*r)/(r*r*r*r*(Lwf-r)*(Lwf-r)*(Lwf-r)*(Lwf-r));
  else 
    return 0;
}
#endif

/************************** Construct Grid Phonon Luttinger *************************/
void ConstructGridPhononLuttingerAuto11(struct Grid *G) {
  DOUBLE k, kmin, kmax;
  DOUBLE y, ymin, ymax;
  DOUBLE precision = 1e-12;
  int search = ON;
  DOUBLE R, Rmin, Rmax;
  DOUBLE a;
  int iter = 1;

  a = aA;
  Message("\n  Jastrow term: ConstructGridPhononLuttingerAuto11 trial wavefunction\n");

  if(a>0) {
    Warning(" assuming repulsive case, with aA = %lf < 0\n", -a);
    a = -a;
  }

  AllocateWFGrid(G, 3);

  Message("  Kpar11= %"LE " \n", Kpar11);
  alpha11_1 = 1./Kpar11;

  // search for Rpar
  R = Rmin = 1e-8;
  // (2.59) - k ^ 2 = alpha11_1 * (PI / L)*(PI / L)* ((alpha11_1 - 1.)*cotan(PI*R/L)*cotan(PI*R/L)-1.)
  k = kmin = sqrt(-alpha11_1 * (PI / L)*(PI / L)* ((alpha11_1 - 1.)/tan(PI*R/L)/tan(PI*R/L)-1.));
  // (2.58) -k*tan(k*R + atan(1./(k*a))) = alpha11_1*PI/L*cotan(PI*R/L)
  ymin = -k*tan(k*R + atan(1. / (k*a))) - alpha11_1*PI / L/tan(PI*R / L);

  R = Rmax = Lhalf;
  k = kmax = sqrt(-alpha11_1 * (PI / L)*(PI / L)* ((alpha11_1 - 1.)/tan(PI*R/L)/tan(PI*R/L)-1.));
  ymax = -k*tan(k*R + atan(1. / (k*a))) - alpha11_1*PI / L/tan(PI*R / L);

  if(fabs(ymax) < precision) {
    Message("  no iterations are needed\n");
    R = Rmax;
    search = OFF;
  }
  else {
    if(ymin*ymax > 0) Error("  cannot construct w.f. (11)");
  }

  // Solve k*tan(k*R + atan(1./(k*a))) = -PI*alpha11_1/L/tan(PI*R/L);
  // i.e. f'(R)/f(R)
  while(search) {
    R = (Rmin+Rmax)/2.;
    k = sqrt(-alpha11_1 * (PI / L)*(PI / L)* ((alpha11_1 - 1.)/tan(PI*R/L)/tan(PI*R/L)-1.));
    y = -k*tan(k*R + atan(1. / (k*a))) - alpha11_1*PI / L/tan(PI*R / L);

    if(y*ymin < 0) {
      Rmax = R;
      ymax = y;
    } else {
      Rmin = R;
      ymin = y;
    }

    if(fabs(y)<precision) {
      R = (Rmax*ymin-Rmin*ymax) / (ymin-ymax);
      search = OFF;
    }

    if(iter++ == 1000) {
      Warning("  maximal number of iterations exceeded");
    }
  }
  k = sqrt(-alpha11_1 * (PI / L)*(PI / L)* ((alpha11_1 - 1.)/tan(PI*R/L)/tan(PI*R/L)-1.));
  y = -k*tan(k*R + atan(1. / (k*a))) - alpha11_1*PI / L/tan(PI*R / L);
  Message("done\n");

  Btrial11 = - Arctg(1/(k*a))/k;
  Atrial11 = Sin(PI*R/L);
  Atrial11 = pow(Sin(PI*R/L), alpha11_1) / Cos(k*(R-Btrial11));
  Rpar11 = R;
  sqE11 = k;
  sqE211 = sqE11*sqE11;

  Message("    k = %"LE " , error %"LE "\n", k, y);
  Message("    R = %"LE " = %lf L/2\n", R, R/Lhalf);
  Message("    A = %"LE " \n", Atrial11);
  Message("    B = %"LE " \n", Btrial11);
  Message("    Equivalent Luttiner parameter K = 1/alpha = %"LE " \n", 1./alpha11_1);
  Message("    [Eloc(R-0) -Eloc(R+0)] / Eloc(R) = %"LE "\n", (InterpolateE11(G,R*0.99999)-InterpolateE11(G,R*1.00001))/InterpolateE11(G,R));

  G->min = 0;
  G->max = L/2;
  G->max2 = G->max*G->max;
}

/* executed only if TRIAL_PHONON is defined */
#ifdef TRIAL_PHONON_LUTTINGER_AUTO11
// ideal bosons K -> \infty, alpha11_1 = 1/K = 0
// ideal fermions K = 1, alpha11_1 = 1/K = 1
DOUBLE InterpolateExactU11(DOUBLE x) {
#ifdef BC_ABSENT
  if(x>Lhalf) return 0;
#endif
  if(x<Rpar11) 
    return Log(fabs(Atrial11*Cos(sqE11*(x-Btrial11))));
  else 
    return Log(pow(Sin(PI*x/L), alpha11_1));
}

DOUBLE InterpolateExactFp11(DOUBLE x) {
#ifdef HARD_SPHERE
#ifdef SECURE
      if(x<aA) Error("InterpolateExactFp11 : (r<aA) call\n");
#endif
#endif
#ifdef BC_ABSENT
  if(x>Lhalf) return 0;
#endif
  if(x<Rpar11)
    return -sqE11 * Tg(sqE11*(x-Btrial11));
  else
    return alpha11_1*PI/(Lwf*Tg(PI*x/L));
}

/* executed only if TRIAL_PHONON is defined */
DOUBLE InterpolateExactE11(DOUBLE x) {
  DOUBLE c;
#ifdef BC_ABSENT
  if(x>Lhalf) return 0;
#endif
  if(x<Rpar11) {
    c = Tg(sqE11*(x-Btrial11));
    return sqE211*(1.+c*c);
    //return sqE211; // Local energy
  }
  else {
    c = 1 / Tg(PI*x/Lwf);
    return alpha11_1*(PI*PI/(Lwf*Lwf))*(1+c*c);
    //return -alpha11_1*(PI*PI/(L*L))*((alpha11_1-1)*c*c-1); // Local energy
  }
}
#endif

/************************** Construct Grid Phonon Luttinger *************************/
void ConstructGridPhononLuttingerAuto12(struct Grid *G) {
  DOUBLE k, kmin, kmax;
  DOUBLE y, ymin, ymax;
  DOUBLE precision = 1e-12;
  int search = ON;
  DOUBLE R, Rmin, Rmax;
  int iter = 1;

  Message("\n  Jastrow term: ConstructGridPhononLuttingerAuto12 trial wavefunction\n");

  if(a>0) {
    Warning(" assuming repulsive case, with aA = %lf < 0\n", -a);
    a = -a;
  }

  AllocateWFGrid(G, 3);

  Message("  Kpar12= %"LE " \n", Kpar12);
  alpha12_1 = 1./Kpar12;

  // search for Rpar
  R = Rmin = 1e-8;
  // (2.59) - k ^ 2 = alpha12_1 * (PI / L)*(PI / L)* ((alpha12_1 - 1.)*cotan(PI*R/L)*cotan(PI*R/L)-1.)
  k = kmin = sqrt(-alpha12_1 * (PI / L)*(PI / L)* ((alpha12_1 - 1.)/tan(PI*R/L)/tan(PI*R/L)-1.));
  // (2.58) -k*tan(k*R + atan(1./(k*a))) = alpha12_1*PI/L*cotan(PI*R/L)
  ymin = -k*tan(k*R + atan(1. / (k*a))) - alpha12_1*PI / L/tan(PI*R / L);

  R = Rmax = Lhalf;
  k = kmax = sqrt(-alpha12_1 * (PI / L)*(PI / L)* ((alpha12_1 - 1.)/tan(PI*R/L)/tan(PI*R/L)-1.));
  ymax = -k*tan(k*R + atan(1. / (k*a))) - alpha12_1*PI / L/tan(PI*R / L);

  if(ymin*ymax > 0) Error("  cannot construct w.f. (12)");

  // Solve k*tan(k*R + atan(1./(k*a))) = -PI*alpha12_1/L/tan(PI*R/L);
  // i.e. f'(R)/f(R)
  while(search) {
    R = (Rmin+Rmax)/2.;
    k = sqrt(-alpha12_1 * (PI / L)*(PI / L)* ((alpha12_1 - 1.)/tan(PI*R/L)/tan(PI*R/L)-1.));
    y = -k*tan(k*R + atan(1. / (k*a))) - alpha12_1*PI / L/tan(PI*R / L);

    if(y*ymin < 0) {
      Rmax = R;
      ymax = y;
    } else {
      Rmin = R;
      ymin = y;
    }

    if(fabs(y)<precision) {
      R = (Rmax*ymin-Rmin*ymax) / (ymin-ymax);
      k = sqrt(-alpha12_1 * (PI / L)*(PI / L)* ((alpha12_1 - 1.)/tan(PI*R/L)/tan(PI*R/L)-1.));
      y = -k*tan(k*R + atan(1. / (k*a))) - alpha12_1*PI / L/tan(PI*R / L);
      search = OFF;
    }

    if(iter++ == 1000) {
      Warning("  number of iterations exceeded");
    }
  }
  Message("done\n");

  Btrial12 = - Arctg(1/(k*a))/k;
  Atrial12 = Sin(PI*R/L);
  Atrial12 = pow(Sin(PI*R/L), alpha12_1) / Cos(k*(R-Btrial12));
  Rpar12 = R;
  sqE12 = k;
  sqE212 = sqE12*sqE12;

  Message("    k = %"LE " , error %"LE "\n", k, y);
  Message("    R = %"LE " = %lf L/2\n", R, R/Lhalf);
  Message("    A = %"LE " \n", Atrial12);
  Message("    B = %"LE " \n", Btrial12);
  Message("    Equivalent Luttiner parameter K = 1/alpha = %"LE " \n", 1./alpha12_1);
  Message("    [Eloc(R-0) -Eloc(R+0)] / Eloc(R) = %"LE "\n", (InterpolateE12(G,R*0.99999)-InterpolateE12(G,R*1.00001))/InterpolateE12(G,R));

  G->min = 0;
  G->max = L/2;
  G->max2 = G->max*G->max;
}

/* executed only if TRIAL_PHONON is defined */
#ifdef TRIAL_PHONON_LUTTINGER_AUTO12
// ideal bosons K -> \infty, alpha12_1 = 1/K = 0
// ideal fermions K = 1, alpha12_1 = 1/K = 1
DOUBLE InterpolateExactU12(DOUBLE x) {
  if(x<Rpar12) 
    return Log(fabs(Atrial12*Cos(sqE12*(x-Btrial12))));
  else 
    return Log(pow(Sin(PI*x/L), alpha12_1));
}

DOUBLE InterpolateExactFp12(DOUBLE x) {
#ifdef HARD_SPHERE
#ifdef SECURE
      if(x<a) Error("InterpolateExactFp12 : (r<a) call\n");
#endif
#endif
  if(x<Rpar12)
    return -sqE12 * Tg(sqE12*(x-Btrial12));
  else
    return alpha12_1*PI/(Lwf*Tg(PI*x/L));
}

/* executed only if TRIAL_PHONON is defined */
DOUBLE InterpolateExactE12(DOUBLE x) {
  DOUBLE c;

  if(x<Rpar12) {
    c = Tg(sqE12*(x-Btrial12));
    return sqE212*(1.+c*c);
    //return sqE212; // Local energy
  }
  else {
    c = 1. / Tg(PI*x/Lwf);
    return alpha12_1*(PI*PI/(Lwf*Lwf))*(1.+c*c);
    //return -alpha12_1*(PI*PI/(L*L))*((alpha12_1-1)*c*c-1); // Local energy
  }
}

#endif

/************************** Construct Grid Schiff Verlet **************************/
void ConstructGridSchiffVerlet(struct Grid *G, const DOUBLE Apar, const DOUBLE Bpar, DOUBLE Rpar, const DOUBLE n) {
  Message("  Schiff Verlett w.f. will be used\n");

  AllocateWFGrid(G, 3);

#ifdef TRIAL_1D //  1D
  //Atrial = pow(Apar/n, Bpar);
  Atrial = 0.5*pow(Apar, Bpar);
#ifndef TRIAL_SCHIFF_VERLET_PHONON // 2D trap
  Ctrial = Atrial * pow(0.5*Lwf, -Bpar);
#else
   alpha_1 = Bpar*Atrial*pow(Rpar, -Bpar-1) / (PI/(L*tan(PI*Rpar/L))); // first derivative at f'(Rpar)
   Ctrial =  Log(pow(Sin(PI*Rpar/L), alpha_1)) + Atrial*pow(Rpar, -Bpar); // f(Rpar)
#endif
#endif

#ifdef TRIAL_2D
  // cusp condition in 2D
  // Cpar = 0;
  // Atrial = 2;
#ifndef TRIAL_SCHIFF_VERLET_PHONON // 2D trap
  Atrial = pow(Apar, Bpar);
#else // 2D hom. system
  Atrial = pow(Apar*Lwf, Bpar);
  //Cpar = Cpar/L;
  Cpar = Atrial*Bpar*pow(Lhalfwf,-Bpar-1);
#endif
  Ctrial = Atrial * pow(0.5*Lwf, -Bpar);
#endif

#ifdef TRIAL_3D
  Warning("Rpar -> Rpar L/2 (%lf -> %lf)\n", Rpar, Rpar*Lhalfwf);
  Rpar *= Lhalfwf;
  G->Rpar = Rpar;
#ifdef TRIAL_SCHIFF_VERLET_PHONON
  Message("  phonon asymptotics in the w.f. will be used\n");
  //Atrial = 0.5*pow(Apar, Bpar);
  G->Ctrial = Ctrial = Apar*Bpar*(Lwf-Rpar)*(Lwf-Rpar)*(Lwf-Rpar)*pow(Rpar,2.-Bpar)/(2.*(Lwf-2.*Rpar)*(Lwf*Lwf-Lwf*Rpar+Rpar*Rpar));
  G->Btrial = Btrial = 8.*Ctrial/(Lwf*Lwf);
  G->Atrial = Atrial = Btrial + Apar*pow(Rpar,-Bpar) - Ctrial*(1./(Rpar*Rpar)+1./((Lwf-Rpar)*(Lwf-Rpar)));
#else
  Atrial = Apar*pow(Lhalf,-Bpar);
#endif
#endif

  Message("  Schiff Verlet trial wavefunction\n");

  G->Apar = Apar;
  G->Bpar = Bpar;
  G->Rpar = Rpar;

  G->min = 0;
  G->max = Lhalf;
  G->max2 = G->max*G->max;
}

/* executed only if TRIAL_SCHIFF_VERLET is defined */
#ifdef TRIAL_SCHIFF_VERLET
DOUBLE InterpolateExactU11(DOUBLE r) {
  //if(fabs(z)<0.3*sigma_lj) z = 0.3*sigma_lj; // Pederiva truncates two-mody term !
#ifdef TRIAL_1D // 1D
#ifndef TRIAL_SCHIFF_VERLET_PHONON // 2D trap
  return -Atrial*pow(fabs(r), -Bpar) + Ctrial;
#else // 2D hom. system
  if(r<Rpar)
    return -Atrial*pow(fabs(r), -Bpar) + Ctrial;
  else
    return Log(pow(Sin(PI*r/L), alpha_1));
#endif
#endif

#ifdef TRIAL_2D // 2D
#ifndef TRIAL_SCHIFF_VERLET_PHONON // 2D trap
  return -Atrial*pow(r, -Bpar);
#else // 2D hom. system
  static int first_time = ON;
  static DOUBLE shift;

  if(first_time) {
    shift = Atrial/pow(Lhalfwf,Bpar)-Log(0.5*(Exp(-Cpar*Lhalfwf)+Exp(-Cpar*(-Lwf+Lhalfwf))));
  }
  if(r>Lhalfwf) return 0.;
  return shift-Atrial/pow(r,Bpar)+Log(0.5*(Exp(-Cpar*r)+Exp(-Cpar*(-Lwf+r))));
#endif
#endif

#ifdef TRIAL_3D
#ifdef TRIAL_SCHIFF_VERLET_PHONON
  if(r<G11.Rpar)
    return G11.Atrial - G11.Apar*pow(r,-G11.Bpar);
  else if(r<Lhalfwf)
    return G11.Btrial-G11.Ctrial*(1./(r*r)+1./((Lwf-r)*(Lwf-r)));
  else
    return 0.;
#else
  return Atrial - Apar*pow(r,-Bpar);
#endif
#endif
}

DOUBLE InterpolateExactFp11(DOUBLE r) {
  //if(fabs(z)<0.3*sigma_lj) z = 0.3*sigma_lj;

#ifdef TRIAL_1D // 1D
#ifndef TRIAL_SCHIFF_VERLET_PHONON
  return Bpar*Atrial*pow(fabs(r), -Bpar-1);
#else // 2D hom. system
  if(r<Rpar)
    return Bpar*Atrial*pow(fabs(r), -Bpar-1);
  else
    return alpha_1*PI/(L*tan(PI*r/L));
#endif
#endif

#ifdef TRIAL_2D
#ifndef TRIAL_SCHIFF_VERLET_PHONON // 2D trap
  return Bpar*Atrial*pow(r, -Bpar-1);
#else // 2D hom. system
  if(r>Lhalf) return 0.;
  return Atrial*Bpar*pow(r,-Bpar-1)-Cpar;
#endif
#endif

#ifdef TRIAL_3D
#ifdef TRIAL_SCHIFF_VERLET_PHONON
  if(r<G11.Rpar)
    return G11.Apar*G11.Bpar*pow(r,-1.-G11.Bpar);
  else if(r<Lhalfwf)
    return 2.*G11.Ctrial*(1./(r*r*r)-1./((Lwf-r)*(Lwf-r)*(Lwf-r)));
  else
    return 0.;
#else
  return Apar*Bpar*pow(r,-1.-Bpar);
#endif
#endif
}

DOUBLE InterpolateExactE11(DOUBLE r) {
//1D Eloc = [-f"/f] + (f'/f)^2
#ifdef TRIAL_1D
  //if(fabs(z)<0.3*sigma_lj) z = 0.3*sigma_lj; //!
#ifndef TRIAL_SCHIFF_VERLET_PHONON
  return Bpar*(Bpar+1)*Atrial*pow(fabs(r), -Bpar-2);
#else // 2D hom. system
  DOUBLE c;

  if(r<Rpar)
    return Bpar*(Bpar+1)*Atrial*pow(fabs(r), -Bpar-2);
  else {
    c = 1. / tan(PI*r/L);
    return alpha_1*(PI*PI/(L*L))*(1+c*c);
  }
#endif
#endif


//2D Eloc = [-(f"/f + f'/r)/f] + (f'/f)^2
#ifdef TRIAL_2D
#ifndef TRIAL_SCHIFF_VERLET_PHONON // 2D trap
  return Bpar*Bpar*Atrial*pow(r, -Bpar-2);
#else // 2D hom. system
  if(r>Lhalf) return 0;
  return (Atrial*Bpar*Bpar+Cpar*pow(r,Bpar+1))*pow(r,-Bpar-2);
#endif
#endif

//3D Eloc = [-(f"/f + 2f'/r)/f] + (f'/f)^2
#ifdef TRIAL_3D
#ifdef TRIAL_SCHIFF_VERLET_PHONON
  if(r<G11.Rpar)
    return G11.Apar*G11.Bpar*(G11.Bpar-1.)*pow(r,-2.-G11.Bpar);
  else if(r<Lhalfwf)
    return 2.*G11.Ctrial*(Lwf*Lwf*Lwf*Lwf-4.*Lwf*Lwf*Lwf*r+6.*Lwf*Lwf*r*r-2.*Lwf*r*r*r+2.*r*r*r*r)/((Lwf-r)*(Lwf-r)*(Lwf-r)*(Lwf-r)*r*r*r*r);
  else
    return 0.;
#else
    return Apar*Bpar*(Bpar-1.)*pow(r,-2.-Bpar);
#endif
#endif
}
#endif

/* executed only if TRIAL_SCHIFF_VERLET is defined */
#ifdef TRIAL_SCHIFF_VERLET12
DOUBLE InterpolateExactU12(DOUBLE r) {
#ifdef TRIAL_3D
#ifdef TRIAL_SCHIFF_VERLET_PHONON
  if(r<G12.Rpar)
    return G12.Atrial - G12.Apar*pow(r,-G12.Bpar);
  else if(r<Lhalfwf)
    return G12.Btrial-G12.Ctrial*(1./(r*r)+1./((Lwf-r)*(Lwf-r)));
  else
    return 0.;
#endif
#endif
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
#ifdef TRIAL_3D
#ifdef TRIAL_SCHIFF_VERLET_PHONON
  if(r<G12.Rpar)
    return G12.Apar*G12.Bpar*pow(r,-1.-G12.Bpar);
  else if(r<Lhalfwf)
    return 2.*G12.Ctrial*(1./(r*r*r)-1./((Lwf-r)*(Lwf-r)*(Lwf-r)));
  else
    return 0.;
#endif
#endif
}

DOUBLE InterpolateExactE12(DOUBLE r) {
//3D Eloc = [-(f"/f + 2f'/r)/f] + (f'/f)^2
#ifdef TRIAL_3D
#ifdef TRIAL_SCHIFF_VERLET_PHONON
  if(r<G12.Rpar)
    return G12.Apar*G12.Bpar*(G12.Bpar-1.)*pow(r,-2.-G12.Bpar);
  else if(r<Lhalfwf)
    return 2.*G12.Ctrial*(Lwf*Lwf*Lwf*Lwf-4.*Lwf*Lwf*Lwf*r+6.*Lwf*Lwf*r*r-2.*Lwf*r*r*r+2.*r*r*r*r)/((Lwf-r)*(Lwf-r)*(Lwf-r)*(Lwf-r)*r*r*r*r);
  else
    return 0.;
#endif
#endif
}
#endif

/************************** Construct Grid SYM **************************/
void ConstructGridSYMGAUSS(struct Grid *G) {
  AllocateWFGrid(G, 3);

  Message("  Sum trial wavefunction\n");
  
  G->min = 0;
  G->max = Lhalf;
  G->max2 = G->max*G->max;
}

/* executed only if TRIAL_SYM_GAUSS is defined */
#ifdef TRIAL_SYM_GAUSS
DOUBLE InterpolateExactSymF(DOUBLE r) {
  return Exp(-a*r*r / (1. + b*r)); //f
}

DOUBLE InterpolateExactSymFp(DOUBLE r) {
  return -((a*r*(2. + b*r)) / (Exp((a*r*r) / (1. + b*r))*(1. + b*r)*(1. + b*r))); // f'
}

DOUBLE InterpolateExactSymE(DOUBLE r) {
  return -(a*(-4. + 4. * a*r*r - b*b*b * r*r*r + b*b * r*r * (-4. + a*r*r) + b*r*(-7. + 4. * a*r*r))) / (Exp((a*r*r) / (1. + b*r))*(1. + b*r)*(1. + b*r)*(1. + b*r)*(1. + b*r)); // - (f'' + f'/r)
}

#endif

/************************** Construct Grid SYMFERMI **************************/
void ConstructGridSYMFERMI(struct Grid *G) {
  AllocateWFGrid(G, 3);

  Message("  Sum trial wavefunction\n");

  G->min = 0;
  G->max = Lhalf;
  G->max2 = G->max*G->max;
}

/* executed only if TRIAL_SYM_FERMI is defined */
#ifdef TRIAL_SYM_FERMI
DOUBLE InterpolateExactSymF(DOUBLE r) {
  return 1. / (1. + Exp((-b + r) / a)); //f
}

DOUBLE InterpolateExactSymFp(DOUBLE r) {
  return -(Exp((b + r) / a) / (a*(Exp(b / a) + Exp(r / a))*(Exp(b / a) + Exp(r / a)))); // f'
}

DOUBLE InterpolateExactSymE(DOUBLE r) {
  return (Exp((b + r) / a)*(a*(Exp(b / a) + Exp(r / a)) + (Exp(b / a) - Exp(r / a))*r)) / (a*a*(Exp(b / a) + Exp(r / a))*(Exp(b / a) + Exp(r / a))*(Exp(b / a) + Exp(r / a))*r); // - (f'' + f'/r)
}

#endif

/************************** Construct Grid SYMYAROSLAV **************************/
void ConstructGridSYMYAROSLAV(struct Grid *G) {
  AllocateWFGrid(G, 3);

  Message("  Sum trial wavefunction\n");

  G->min = 0;
  G->max = Lhalf;
  G->max2 = G->max*G->max;
}

/* executed only if TRIAL_SYM_YAROSLAV is defined */
#ifdef TRIAL_SYM_YAROSLAV
DOUBLE InterpolateExactSymF(DOUBLE r) {
  return 1. - Exp( -pow ( a / r , b ) ); //f
}

DOUBLE InterpolateExactSymFp(DOUBLE r) {
  return -((b*pow(a / r, b)) / (Exp(pow(a / r, b))*r)); // f'
}

DOUBLE InterpolateExactSymE(DOUBLE r) {
  return (b*b* (-1. + pow(a / r,b))*pow(a / r, b)) / (Exp(pow(a / r, b))*r*r); // - (f'' + f'/r)
}

#endif

/************************** Construct Grid SUMEXP **************************/
void ConstructGridSUMEXP(struct Grid *G) {
  AllocateWFGrid(G, 3);

  Message("  Sum trial wavefunction\n");

  G->min = 0;
  G->max = Lhalf;
  G->max2 = G->max*G->max;
}

/* executed only if TRIAL_SUM_EXPONENTIALS is defined */
#ifdef TRIAL_SUM_EXPONENTIALS
DOUBLE ft;
DOUBLE ftp;
DOUBLE ftpp;

DOUBLE InterpolateExactSymF(DOUBLE r) {
  ft = c6 / Exp(6.*a*r) + c5 / Exp(5.*a*r) + c4 / Exp(4.*a*r) + c3 / Exp(3.*a*r) + c2 / Exp(2.*a*r) + c / Exp(a*r) + c7 / Exp(a1*(-b1 + r)*(-b1 + r)) + c8 / Exp(a3*(-b3 + r)*(-b3 + r)) + c9 / Exp(a4*(-b4 + r)*(-b4 + r)) + c10 / Exp(a5*(-b5 + r)*(-b5 + r)) + c11 / Exp(a6*(-b6 + r)*(-b6 + r)) + c12 / Exp(a7*(-b7 + r)*(-b7 + r)) + c13 / Exp(a8*(-b8 + r)*(-b8 + r)); //f  
  return (ft>1e-100) ? (ft) : (1e-100);
}

DOUBLE InterpolateExactSymFp(DOUBLE r) {
  ftp = (-6.*a*c6) / Exp(6.*a*r) - (5.*a*c5) / Exp(5.*a*r) - (4.*a*c4) / Exp(4.*a*r) - (3.*a*c3) / Exp(3.*a*r) - (2.*a*c2) / Exp(2.*a*r) - (a*c) / Exp(a*r) + (2.*a1*c7*(b1 - r)) / Exp(a1*(b1 - r)*(b1 - r)) + (2.*a3*c8*(b3 - r)) / Exp(a3*(b3 - r)*(b3 - r)) + (2.*a4*c9*(b4 - r)) / Exp(a4*(b4 - r)*(b4 - r)) + (2.*a5*c10*(b5 - r)) / Exp(a5*(b5 - r)*(b5 - r)) + (2.*a6*c11*(b6 - r)) / Exp(a6*(b6 - r)*(b6 - r)) + (2.*a7*c12*(b7 - r)) / Exp(a7*(b7 - r)*(b7 - r)) + (2.*a8*c13*(b8 - r)) / Exp(a8*(b8 - r)*(b8 - r)); // f'  
  return (fabs(ftp)>1e-100) ? (ftp) : (1e-100);
}

DOUBLE InterpolateExactSymE(DOUBLE r) {
  ftpp = (-36.*a*a*c6) / Exp(6.*a*r) - (25.*a*a*c5) / Exp(5.*a*r) - (16.*a*a*c4) / Exp(4.*a*r) - (9.*a*a*c3) / Exp(3.*a*r) - (4.*a*a*c2) / Exp(2.*a*r) - (a*a*c) / Exp(a*r) + (2.*a1*c7) / Exp(a1*(-b1 + r)*(-b1 + r)) + (2.*a3*c8) / Exp(a3*(-b3 + r)*(-b3 + r)) - (4.*a1*a1*c7*(-b1 + r)*(-b1 + r)) / Exp(a1*(-b1 + r)*(-b1 + r)) - (4.*a3*a3*c8*(-b3 + r)*(-b3 + r)) / Exp(a3*(-b3 + r)*(-b3 + r)) + (2.*a4*c9) / Exp(a4*(-b4 + r)*(-b4 + r)) + (2.*a5*c10) / Exp(a5*(-b5 + r)*(-b5 + r)) + (2.*a6*c11) / Exp(a6*(-b6 + r)*(-b6 + r)) + (2.*a7*c12) / Exp(a7*(-b7 + r)*(-b7 + r)) + (2.*a8*c13) / Exp(a8*(-b8 + r)*(-b8 + r)) - (4.*a4*a4*c9*(-b4 + r)*(-b4 + r)) / Exp(a4*(-b4 + r)*(-b4 + r)) - (4.*a5*a5*c10*(-b5 + r)*(-b5 + r)) / Exp(a5*(-b5 + r)*(-b5 + r)) - (4.*a6*a6*c11*(-b6 + r)*(-b6 + r)) / Exp(a6*(-b6 + r)*(-b6 + r)) - (4.*a7*a7*c12*(-b7 + r)*(-b7 + r)) / Exp(a7*(-b7 + r)*(-b7 + r)) - (4.*a8*a8*c13*(-b8 + r)*(-b8 + r)) / Exp(a8*(-b8 + r)*(-b8 + r)) - ((-6.*a*c6) / Exp(6.*a*r) - (5.*a*c5) / Exp(5.*a*r) - (4.*a*c4) / Exp(4.*a*r) - (3.*a*c3) / Exp(3.*a*r) - (2.*a*c2) / Exp(2.*a*r) - (a*c) / Exp(a*r) + (2.*a1*c7*(b1 - r)) / Exp(a1*(b1 - r)*(b1 - r)) + (2.*a3*c8*(b3 - r)) / Exp(a3*(b3 - r)*(b3 - r)) + (2.*a4*c9*(b4 - r)) / Exp(a4*(b4 - r)*(b4 - r)) + (2.*a5*c10*(b5 - r)) / Exp(a5*(b5 - r)*(b5 - r)) + (2.*a6*c11*(b6 - r)) / Exp(a6*(b6 - r)*(b6 - r)) + (2.*a7*c12*(b7 - r)) / Exp(a7*(b7 - r)*(b7 - r)) + (2.*a8*c13*(b8 - r)) / Exp(a8*(b8 - r)*(b8 - r))) / r;
  return (fabs(ftpp)>1e-100) ? (ftpp) : (1e-100);
}
#endif

/************************** Construct Grid SYMTWOBODY **************************/
void ConstructGridSYMTWOBODY(struct Grid *G) {
  AllocateWFGrid(G, 3);

  Message("  Sum trial wavefunction\n");

  G->min = 0;
  G->max = Lhalf;
  G->max2 = G->max*G->max;
}
/* executed only if TRIAL_SYM_TWO_BODY is defined */
#ifdef TRIAL_SYM_TWO_BODY
DOUBLE InterpolateExactSymF(DOUBLE r) {
  return Exp(InterpolateSplineU(&GSYM, r)); //f
}

DOUBLE InterpolateExactSymFp(DOUBLE r) {
  return InterpolateSplineFp(&GSYM, r)*Exp(InterpolateSplineU(&GSYM, r)); // f'
}

DOUBLE InterpolateExactSymE(DOUBLE r) {
  return (InterpolateSplineE(&GSYM, r) - InterpolateSplineFp(&GSYM, r)*InterpolateSplineFp(&GSYM, r))*Exp(InterpolateSplineU(&GSYM, r)); // - (f'' + f'/r)
}

#endif

/* executed only if TRIAL_SYM_MCGUIRE is defined */
#ifdef TRIAL_SYM_MCGUIRE
DOUBLE InterpolateExactSymF(DOUBLE r) {
  return Exp(-r / a);
}

DOUBLE InterpolateExactSymFp(DOUBLE r) {
  return -Exp(-r / a) / a;
}

DOUBLE InterpolateExactSymE(DOUBLE r) {
// - f''
  return - Exp(-r / a) / (a*a);
}
#endif

/* executed only if TRIAL_MCGUIRE is defined */
#ifdef TRIAL_SYM_MCGUIRE12_A2 // f(x) = exp[-x*(1+x)/(aAB+Ro*x)]
DOUBLE InterpolateExactSymF(DOUBLE r) {
  return exp(-r*(1.+r)/(a+Ro*r));
}

DOUBLE InterpolateExactSymFp(DOUBLE r) {
  return -(a + 2.*a*r + Ro*r*r) / ((a + Ro*r)*(a + Ro*r))*exp(-r*(1.+r)/(a+Ro*r));
}

DOUBLE InterpolateExactSymE(DOUBLE r) {
// - f''
  return (2*a*a*a - Ro*Ro*r*r*r*r - 2*a*Ro*r*(Ro + r + 2*r*r) + a*a*(2*Ro*(-1 + r) - (1 + 2*r)*(1 + 2*r)))/(exp((r*(1 + r))/(a + Ro*r))*(a + Ro*r)*(a + Ro*r)*(a + Ro*r)*(a + Ro*r));
}
#endif

#ifdef TRIAL_CALOGERO12
DOUBLE InterpolateExactU12(DOUBLE r) {
  return Log(pow(r, Rpar12));
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  return Rpar12/r;
}

DOUBLE InterpolateExactE12(DOUBLE r) {
#ifdef TRIAL_1D //1D Eloc = [-f"/f] + (f'/f)^2
  return Rpar12/(r*r);
#endif

#ifdef TRIAL_2D //2D Eloc = [-(f" + f'/r)/f] + (f'/f)^2
  return 0;
#endif

#ifdef TRIAL_3D //3D Eloc = [-(f"+2f'/r)/f] + (f'/f)^2
  return -Rpar12/(r*r);
#endif

}
#endif

#ifdef TRIAL_CALOGERO11
DOUBLE InterpolateExactU11(DOUBLE r) {
  return Log(pow(r, Rpar11));
}

DOUBLE InterpolateExactFp11(DOUBLE r) {
  return Rpar11/r;
}

DOUBLE InterpolateExactE11(DOUBLE r) {
#ifdef TRIAL_1D //1D Eloc = [-f"/f] + (f'/f)^2
  return Rpar11/(r*r);
#endif

#ifdef TRIAL_2D //2D Eloc = [-(f" + f'/r)/f] + (f'/f)^2
  return 0;
#endif

#ifdef TRIAL_3D //3D Eloc = [-(f"+2f'/r)/f] + (f'/f)^2
  return -Rpar11/(r*r);
#endif

}
#endif

/* executed only if TRIAL_FERMI12 is defined */
#ifdef TRIAL_FERMI12
DOUBLE InterpolateExactU12(DOUBLE r) {
  return log(Apar/(1. + exp((r-Epar)/T)));
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  double f, fp, fexp, fexpinv;

  fexp = exp((r - Epar) / T);
  fexpinv = 1. / (1. + fexp);

  return - fexp * fexpinv / T;
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  // Eloc1D = -[f"/f - (f'/f)^2]
  double g;

  g = 2.*T*cosh(0.5*(Epar-r) / T);
  return 1./(g*g);
}
#endif

/* executed only if TRIAL_FERMI12 is defined */
#ifdef TRIAL_FERMI11
DOUBLE InterpolateExactU11(DOUBLE r) {
  return log(Apar/(1. + exp((r-Epar)/T)));
}

DOUBLE InterpolateExactFp11(DOUBLE r) {
  double fexp, fexpinv;

  fexp = exp((r - Epar) / T);
  fexpinv = 1. / (1. + fexp);

  return - fexp * fexpinv / T;
}

DOUBLE InterpolateExactE11(DOUBLE r) {
  // Eloc1D = -[f"/f - (f'/f)^2]
#ifdef TRIAL_1D
  double g;

  g = 2.*T*cosh(0.5*(Epar-r) / T);
  return 1./(g*g);
#else
  DOUBLE fexp, fexpinv, Fp, Fpp;
  fexp = exp((r - Epar) / T);
  fexpinv = 1. / (1. + fexp);
  Fp = - fexp * fexpinv / T;
  Fpp = fexp * fexpinv / (T*T)*(2.*fexp * fexpinv-1.);
  return -Fpp -(DIMENSION-1.)/r*Fp + Fp*Fp;
#endif
}
#endif

#ifdef JASTROW_RIGHT_BOUNDARY_PHONONS12
#ifdef INTERPOLATE_SPLINE_WF12
DOUBLE InterpolateU12(struct Grid *Grid, DOUBLE r) {
  if(r>Grid->max-10*Grid->step) { 
#ifdef TRIAL_3D
    return - Apar12/(r*r) - Apar12/((L-r)*(L-r)) + 2.*Apar12/Lhalf2;
#endif
#ifdef TRIAL_2D
    return - Apar12/r - Apar12/(L-r) + 2.*Apar12/Lhalf;
#endif
#ifdef TRIAL_1D
    return Log(pow(Sin(PI*x / L), alpha12_1));
#endif
  }
  else {
    return InterpolateSplineU(Grid, r);
  }
}

DOUBLE InterpolateFp12(struct Grid *Grid, DOUBLE r) {
  if(r>Grid->max-10*Grid->step) { 
#ifdef TRIAL_3D
    return 2.*Apar12*(1./(r*r*r)-1./((L-r)*(L-r)*(L-r)));
#endif
#ifdef TRIAL_2D
    return Apar12*(1./(r*r)-1./((L-r)*(L-r)));
#endif
#ifdef TRIAL_1D
    return alpha12_1*PI/(L*Tg(PI*x/L));
#endif
  }
  else {
    return InterpolateSplineFp(Grid, r);
  }
}

DOUBLE InterpolateE12(struct Grid *Grid, DOUBLE r) {
  if(r>Grid->max-10*Grid->step) { 
#ifdef TRIAL_3D
    return (2.*Apar12*(L*L*L*L - 4.*L*L*L*r + 6.*L*L*r*r - 2.*L*r*r*r + 2.*r*r*r*r))/((L-r)*(L-r)*(L-r)*(L-r)*r*r*r*r);
#endif
#ifdef TRIAL_2D
    return (Apar12*L*(L*L - 3.*L*r + 4.*r*r))/((L - r)*(L - r)*(L - r)*r*r*r);
#endif
#ifdef TRIAL_1D
    c = 1. / Tg(PI*x/L);
    return alpha12_1*(PI*PI/(L*L))*(1.+c*c);
#endif
  }
  else {
    return InterpolateSplineE(Grid, r);
  }
}
#endif
#endif

/* executed only if TRIAL_ION_12 is defined */
#ifdef TRIAL_ION_12

//double Rmachp;
//double Apar12= 5.32034;
//6.50092
//
//double b0=0.020032;
//double c0=0.22556;
//double Xmach=0.7;

DOUBLE InterpolateExactU12(DOUBLE r) {
  DOUBLE Alpha;
  DOUBLE a11;
  DOUBLE f;
  static int first_time = ON;

  Alpha = Apar12/Lhalf;

  if(first_time) {
    double Ar = 29.7705;
    double Ebind=1.6053;
    double C;
    Rpar12 = 0.7*Lhalf;
    C  = (Ar*exp(-sqrt(Ebind)*(Rpar12))/(Rpar12))/((exp(-Alpha*(Rpar12))+exp(-Alpha*(L-Rpar12))));
    f = 2*C*exp(-Alpha*L/2);
    Atrial12 = 1./f;
    first_time = OFF;
  }

  a11 = r*r;
  if(r<0.106) {
    f = (0.2238910492307238*(r*r) - 47.098604647632584*(r*r*r) + 3838.509687238086*(r*r*r*r) - 150697.7242549212*(r*r*r*r*r) + 
    2.69322357560198e6*(r*r*r*r*r*r) - 2.942801925836761e6*(r*r*r*r*r*r*r) - 6.975120222973193e8*(r*r*r*r*r*r*r*r) + 1.130081951875596e10*(r*r*r*r*r*r*r*r*r) - 
    7.136402175948848e10*(r*r*r*r*r*r*r*r*r*r) + 1.638665671507462e11*(r*r*r*r*r*r*r*r*r*r*r));
 } 

 if(r>=0.106 && r<=0.8348736327591326) {
   f=(-20.85834882166504 + 798.2413559771197*(r) - 12391.728635581465*(r*r) + 100731.03487830551*(r*r*r) - 473675.41460224695*(r*r*r*r) + 
   1.4043482121980153e6*(r*r*r*r*r) - 2.7296869170774086e6*(r*r*r*r*r*r) + 3.4862098025732883e6*(r*r*r*r*r*r*r) - 2.82425862227257e6*(r*r*r*r*r*r*r*r) + 
   1.317742109025869e6*(r*r*r*r*r*r*r*r*r) - 269898.3944374754*(r*r*r*r*r*r*r*r*r*r));
 }

  if(r>=0.8348736327591326 && r<Rpar12) {
    double Ar= 29.7705;
    double Ebind=1.6053;
    f=Ar*exp(-sqrt(Ebind)*(r))/(r);
  }
  else {
    if(r>=Rpar12 && r<=Lhalf) {
      double Ar = 29.7705;
      double Ebind=1.6053;
      double C = (Ar*exp(-sqrt(Ebind)*(Rpar12))/(Rpar12))/((exp(-Alpha*(Rpar12))+exp(-Alpha*(L-Rpar12))));
      f = C*(exp(-Alpha*(r))+exp(-Alpha*(L-r)));
    }
    if(r>Lhalf) {
      return 0;
    }
  }
  return log(f*Atrial12);
}

DOUBLE InterpolateExactFp12(DOUBLE r) {
  DOUBLE Alpha;
  DOUBLE a11;
  DOUBLE f, fp;

  Alpha = Apar12/Lhalf;
  a11 = r*r;
  if(r<0.106) {
    f = (0.2238910492307238*(r*r) - 47.098604647632584*(r*r*r) + 3838.509687238086*(r*r*r*r) - 150697.7242549212*(r*r*r*r*r) + 
    2.69322357560198e6*(r*r*r*r*r*r) - 2.942801925836761e6*(r*r*r*r*r*r*r) - 6.975120222973193e8*(r*r*r*r*r*r*r*r) + 1.130081951875596e10*(r*r*r*r*r*r*r*r*r) - 
    7.136402175948848e10*(r*r*r*r*r*r*r*r*r*r) + 1.638665671507462e11*(r*r*r*r*r*r*r*r*r*r*r));
    fp = (0.4477820984614476*r - 141.29581394289775*(r*r) + 15354.038748952344*(r*r*r) - 753488.621274606*(r*r*r*r) + 
    1.615934145361188e7*(r*r*r*r*r) - 2.0599613480857328e7*(r*r*r*r*r*r) - 5.580096178378554e9*(r*r*r*r*r*r*r) + 1.0170737566880365e11*(r*r*r*r*r*r*r*r) - 
    7.136402175948848e11*(r*r*r*r*r*r*r*r*r) + 1.802532238658208e12*(r*r*r*r*r*r*r*r*r*r));
 } 

 if(r>=0.106 && r<=0.8348736327591326) {
   f=(-20.85834882166504 + 798.2413559771197*(r) - 12391.728635581465*(r*r) + 100731.03487830551*(r*r*r) - 473675.41460224695*(r*r*r*r) + 
   1.4043482121980153e6*(r*r*r*r*r) - 2.7296869170774086e6*(r*r*r*r*r*r) + 3.4862098025732883e6*(r*r*r*r*r*r*r) - 2.82425862227257e6*(r*r*r*r*r*r*r*r) + 
   1.317742109025869e6*(r*r*r*r*r*r*r*r*r) - 269898.3944374754*(r*r*r*r*r*r*r*r*r*r));
   fp=(798.2413559771197 - 24783.45727116293*(r) + 302193.1046349165*(r*r) - 1.8947016584089878e6*(r*r*r) + 7.0217410609900765e6*(r*r*r*r) - 
   1.637812150246445e7*(r*r*r*r*r) + 2.4403468618013017e7*(r*r*r*r*r*r) - 2.259406897818056e7*(r*r*r*r*r*r*r) + 1.1859678981232822e7*(r*r*r*r*r*r*r*r) - 
   2.698983944374754e6*(r*r*r*r*r*r*r*r*r));
 }

  if(r>=0.8348736327591326 && r<Rpar12) {
    double Ar= 29.7705;
    double Ebind=1.6053;
    f=Ar*exp(-sqrt(Ebind)*(r))/(r);
    fp=(Ar*exp(-sqrt(Ebind)*(r))/(r))*(-1/(r)-sqrt(Ebind));
  }
  else {
    if(r>=Rpar12 && r<=Lhalf) {
      double Ar = 29.7705;
      double Ebind=1.6053;
      double C = (Ar*exp(-sqrt(Ebind)*(Rpar12))/(Rpar12))/((exp(-Alpha*(Rpar12))+exp(-Alpha*(L-Rpar12))));
      f = C*(exp(-Alpha*(r))+exp(-Alpha*(L-r)));
      fp = (-Alpha*C)*(exp(-Alpha*(r))-exp(-Alpha*(L-r)));
    }
    if(r>Lhalf) {
      double Ar = 29.7705;
      double Ebind=1.6053;
      double C = (Ar*exp(-sqrt(Ebind)*(Rpar12))/(Rpar12))/((exp(-Alpha*(Rpar12))+exp(-Alpha*(L-Rpar12))));
      f = 2*C*exp(-Alpha*L/2);
      fp = 0.0;
    }
  }

  return fp/f;
}

DOUBLE InterpolateExactE12(DOUBLE r) {
  DOUBLE Alpha;
  DOUBLE a11;
  DOUBLE f, fp, fpp;

  Alpha = Apar12/Lhalf;
  a11 = r*r;
  if(r<0.106) {
    f = (0.2238910492307238*(r*r) - 47.098604647632584*(r*r*r) + 3838.509687238086*(r*r*r*r) - 150697.7242549212*(r*r*r*r*r) + 
    2.69322357560198e6*(r*r*r*r*r*r) - 2.942801925836761e6*(r*r*r*r*r*r*r) - 6.975120222973193e8*(r*r*r*r*r*r*r*r) + 1.130081951875596e10*(r*r*r*r*r*r*r*r*r) - 
    7.136402175948848e10*(r*r*r*r*r*r*r*r*r*r) + 1.638665671507462e11*(r*r*r*r*r*r*r*r*r*r*r));
    fp = (0.4477820984614476*r - 141.29581394289775*(r*r) + 15354.038748952344*(r*r*r) - 753488.621274606*(r*r*r*r) + 
    1.615934145361188e7*(r*r*r*r*r) - 2.0599613480857328e7*(r*r*r*r*r*r) - 5.580096178378554e9*(r*r*r*r*r*r*r) + 1.0170737566880365e11*(r*r*r*r*r*r*r*r) - 
    7.136402175948848e11*(r*r*r*r*r*r*r*r*r) + 1.802532238658208e12*(r*r*r*r*r*r*r*r*r*r));
    fpp = (0.4477820984614476 - 282.5916278857955*r + 46062.11624685703*(r*r) - 3.013954485098424e6*(r*r*r) + 
    8.07967072680594e7*(r*r*r*r) - 1.2359768088514397e8*(r*r*r*r*r) - 3.906067324864988e10*(r*r*r*r*r*r) + 8.136590053504292e11*(r*r*r*r*r*r*r) - 
    6.422761958353963e12*(r*r*r*r*r*r*r*r) + 1.802532238658208e13*(r*r*r*r*r*r*r*r*r));
 } 

 if(r>=0.106 && r<=0.8348736327591326) {
   f=(-20.85834882166504 + 798.2413559771197*(r) - 12391.728635581465*(r*r) + 100731.03487830551*(r*r*r) - 473675.41460224695*(r*r*r*r) + 
   1.4043482121980153e6*(r*r*r*r*r) - 2.7296869170774086e6*(r*r*r*r*r*r) + 3.4862098025732883e6*(r*r*r*r*r*r*r) - 2.82425862227257e6*(r*r*r*r*r*r*r*r) + 
   1.317742109025869e6*(r*r*r*r*r*r*r*r*r) - 269898.3944374754*(r*r*r*r*r*r*r*r*r*r));
   fp=(798.2413559771197 - 24783.45727116293*(r) + 302193.1046349165*(r*r) - 1.8947016584089878e6*(r*r*r) + 7.0217410609900765e6*(r*r*r*r) - 
   1.637812150246445e7*(r*r*r*r*r) + 2.4403468618013017e7*(r*r*r*r*r*r) - 2.259406897818056e7*(r*r*r*r*r*r*r) + 1.1859678981232822e7*(r*r*r*r*r*r*r*r) - 
   2.698983944374754e6*(r*r*r*r*r*r*r*r*r));
   fpp=(-24783.45727116293 + 604386.209269833*(r) - 5.684104975226963e6*(r*r) + 2.8086964243960306e7*(r*r*r) - 8.189060751232225e7*(r*r*r*r) + 
   1.464208117080781e8*(r*r*r*r*r) - 1.5815848284726393e8*(r*r*r*r*r*r) + 9.487743184986258e7*(r*r*r*r*r*r*r) - 2.4290855499372788e7*(r*r*r*r*r*r*r*r));
 }

  if(r>=0.8348736327591326 && r<Rpar12) {
    double Ar= 29.7705;
    double Ebind=1.6053;
    f=Ar*exp(-sqrt(Ebind)*(r))/(r);
    fp=(Ar*exp(-sqrt(Ebind)*(r))/(r))*(-1/(r)-sqrt(Ebind));
    fpp=(Ar*exp(-sqrt(Ebind)*(r))/(r))*(2/a11+2*sqrt(Ebind)/(r)+Ebind);
  }  
  else {
    if(r>=Rpar12 && r<=Lhalf) {
      double Ar = 29.7705;
      double Ebind=1.6053;
      double C = (Ar*exp(-sqrt(Ebind)*(Rpar12))/(Rpar12))/((exp(-Alpha*(Rpar12))+exp(-Alpha*(L-Rpar12))));
      f = C*(exp(-Alpha*(r))+exp(-Alpha*(L-r)));
      fp = (-Alpha*C)*(exp(-Alpha*(r))-exp(-Alpha*(L-r)));
      fpp = (Alpha*Alpha*C)*(exp(-Alpha*(r))+exp(-Alpha*(L-r)));
    }
    if(r>Lhalf) {
      double Ar = 29.7705;
      double Ebind=1.6053;
      double C = (Ar*exp(-sqrt(Ebind)*(Rpar12))/(Rpar12))/((exp(-Alpha*(Rpar12))+exp(-Alpha*(L-Rpar12))));
      f = 2*C*exp(-Alpha*L/2);
      fp = 0.0;
      fpp = 0.0;
    }
  }
  //2D: Eloc = [-(f" + f'/r)/f] + (f'/f)^2
  return -(fpp+2.*fp/r)/f + (fp/f)*(fp/f);
}
#endif
