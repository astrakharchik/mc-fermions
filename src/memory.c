/*memory.c*/

#include <malloc.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include "memory.h"
#include "trial.h"
#include "utils.h"
#include "main.h"
#include "fermions.h"
#include "trialf.h"

#define ON 1
#define OFF 0

/*************************** Allocate Grid **********************************/
void AllocateWFGrid(struct Grid *G, long int size) {

  if(verbosity) Message("\nMemory allocation : allocating grid of size %li...", size);

  G->x   = (DOUBLE*) Calloc("G->x   ", size, sizeof(DOUBLE));
  G->fp  = (DOUBLE*) Calloc("G->fp  ", size, sizeof(DOUBLE));
  G->lnf = (DOUBLE*) Calloc("G->lnf",  size, sizeof(DOUBLE));
  G->E   = (DOUBLE*) Calloc("G->E",    size, sizeof(DOUBLE));
  G->f   = (DOUBLE*) Calloc("G->f  ",  size+1, sizeof(DOUBLE));
  G->fpp = (DOUBLE*) Calloc("G->fpp",  size+1, sizeof(DOUBLE));

  G->size = size;

  if(verbosity) Message(" done\n");
}

/*************************** Allocate Walkers *******************************/
void AllocateSingleWalker(struct Walker* W) {
  int i,j;
  int N;

  N = Nmax;

  // initialize first scalar elements of the structure. Arrays are initialized later
  W->weight = 1.;
  W->PD_position = 0;
  W->varparindex = 0;
  W->status_end_of_step_initialized = OFF;

  // data which is copied by CopyWalker
  // 1)
  W->x = (DOUBLE*) Calloc("W->x", N, sizeof(DOUBLE));
  W->y = (DOUBLE*) Calloc("W->y", N, sizeof(DOUBLE));
  W->z = (DOUBLE*) Calloc("W->z", N, sizeof(DOUBLE));
  W->xdn = (DOUBLE*) Calloc("W->x", N, sizeof(DOUBLE));
  W->ydn = (DOUBLE*) Calloc("W->y", N, sizeof(DOUBLE));
  W->zdn = (DOUBLE*) Calloc("W->z", N, sizeof(DOUBLE));

  // 2)
  if(MC == DIFFUSION && (SmartMC == DMC_LINEAR_METROPOLIS || SmartMC == DMC_LINEAR_METROPOLIS_MIDDLE)) {
    ArrayCalloc2D(W->dR_drift_old, "dR_drift_old ", i, N, 6, DOUBLE, "DOUBLE");
  }

  if(MC == DIFFUSION) {
    // 3)
    if(measure_energy) {
      W->Epot_pure = (DOUBLE*) Calloc("W->Epot_pure", grid_pure_block, sizeof(DOUBLE));
    }

    // 4)
    if(measure_PairDistr) {
      ArrayCalloc2D(W->PD, "W->PD ", i, grid_pure_block, gridPD, int, "int");
      ArrayCalloc2D(W->PDup, "W->PD ", i, grid_pure_block, gridPD, int, "int");
      ArrayCalloc2D(W->PDdn, "W->PD ", i, grid_pure_block, gridPD, int, "int");
      if(measure_PairDistrMATRIX) { // allocate pure PD matrix
        ArrayCalloc3D(W->PD_MATRIX11, "W->PD_MATRIX11 ", i, grid_pure_block, j, gridPD_MATRIX, gridPD_MATRIX, int, "int");
        ArrayCalloc3D(W->PD_MATRIX12, "W->PD_MATRIX12 ", i, grid_pure_block, j, gridPD_MATRIX, gridPD_MATRIX, int, "int");
        ArrayCalloc3D(W->PD_MATRIX22, "W->PD_MATRIX22 ", i, grid_pure_block, j, gridPD_MATRIX, gridPD_MATRIX, int, "int");
      }
    }

    // 5)
    if(measure_RadDistr) {
      W->RD_wait = 1;
      ArrayCalloc2D(W->RDup, "W->RDup", i, grid_pure_block, gridRD, int, "int");
      ArrayCalloc2D(W->RDdn, "W->RDdn", i, grid_pure_block, gridRD, int, "int");
    }

    // 6)
    if(measure_SD) {
      ArrayCalloc2D(W->CM, "W->CM ", i, 3, SD.size, DOUBLE, "DOUBLE");
      ArrayCalloc2D(W->rreal, "W->rreal ", i, Nmax, 6, DOUBLE, "DOUBLE");
    }

    // 7)
    if(measure_Sk) {
      ArrayCalloc2D(W->Sk, "W->Sk ", i, grid_pure_block, gridSk, DOUBLE, "DOUBLE");
      ArrayCalloc2D(W->Sk12, "W->Sk ", i, grid_pure_block, gridSk, DOUBLE, "DOUBLE");
      ArrayCalloc2D(W->Sk_N, "W->Sk_N ", i, grid_pure_block, gridSk, DOUBLE, "DOUBLE");
    }

    // 8)
    if(measure_FormFactor) {
      ArrayCalloc2D(W->rhoRe1, "W->rhoRe1 ", i, gridSKT_t, gridSKT_k, DOUBLE, "DOUBLE");
      ArrayCalloc2D(W->rhoIm1, "W->rhoIm1 ", i, gridSKT_t, gridSKT_k, DOUBLE, "DOUBLE");
      ArrayCalloc2D(W->rhoRe2, "W->rhoRe2 ", i, gridSKT_t, gridSKT_k, DOUBLE, "DOUBLE");
      ArrayCalloc2D(W->rhoIm2, "W->rhoIm2 ", i, gridSKT_t, gridSKT_k, DOUBLE, "DOUBLE");
    }

    // 9)
    if(measure_pure_coordinates) {
      W->x_pure = (DOUBLE*) Calloc("W->x_pure", N, sizeof(DOUBLE));
      W->y_pure = (DOUBLE*) Calloc("W->y_pure", N, sizeof(DOUBLE));
      W->z_pure = (DOUBLE*) Calloc("W->z_pure", N, sizeof(DOUBLE));
      W->xdn_pure = (DOUBLE*) Calloc("W->xdn_pure", N, sizeof(DOUBLE));
      W->ydn_pure = (DOUBLE*) Calloc("W->ydn_pure", N, sizeof(DOUBLE));
      W->zdn_pure = (DOUBLE*) Calloc("W->zdn_pure", N, sizeof(DOUBLE));
    }
  } // DIFFUSION

  // data which is NOT copied by CopyWalker
  ArrayCalloc2D(W->F, "F", i, N, 6, DOUBLE, "DOUBLE");
  ArrayCalloc2D(W->Fup, "Fup", i, N, 3, DOUBLE, "DOUBLE");
  ArrayCalloc2D(W->Fdn, "Fdn", i, N, 3, DOUBLE, "DOUBLE");
  ArrayCalloc2D(W->Dup, "Dup", i, N, 3, DOUBLE, "DOUBLE");
  ArrayCalloc2D(W->Ddn, "Ddn", i, N, 3, DOUBLE, "DOUBLE");

#ifdef SYM_OPPOSITE_SPIN
  W->Sup = (DOUBLE*) Calloc("W->Sup", N, sizeof(DOUBLE));
  W->Sdn = (DOUBLE*) Calloc("W->Sdn", N, sizeof(DOUBLE));
  ArrayCalloc2D(W->P1up, "P1up", i, N, 3, DOUBLE, "DOUBLE");
  ArrayCalloc2D(W->P1dn, "P1dn", i, N, 3, DOUBLE, "DOUBLE");
  ArrayCalloc2D(W->P2up, "P2up", i, N, 3, DOUBLE, "DOUBLE");
  ArrayCalloc2D(W->P2dn, "P2dn", i, N, 3, DOUBLE, "DOUBLE");
#endif

  if(MC == DIFFUSION || (MC == VARIATIONAL && SmartMC)) {  // in case of DMC allocate arrays for jumps
    ArrayCalloc2D(W->R, "R", i, N, 6, DOUBLE, "DOUBLE");
    ArrayCalloc2D(W->Rp, "Rp", i, N, 6, DOUBLE, "DOUBLE");
    ArrayCalloc2D(W->Rpp, "Rpp", i, N, 6, DOUBLE, "DOUBLE");
    ArrayCalloc2D(W->Rppp, "Rppp", i, N, 6, DOUBLE, "DOUBLE");
    ArrayCalloc2D(W->Fp, "Fp", i, N, 6, DOUBLE, "DOUBLE");
    ArrayCalloc2D(W->Fpp, "Fpp", i, N, 6, DOUBLE, "DOUBLE");
    ArrayCalloc2D(W->dR, "dR", i, N, 6, DOUBLE, "DOUBLE");
  }

  if(!(MC == DIFFUSION && (SmartMC == DMC_LINEAR_METROPOLIS || SmartMC == DMC_LINEAR_METROPOLIS_MIDDLE))) {
    ArrayCalloc2D(W->dR_drift_old, "dR_drift_old ", i, N, 6, DOUBLE, "DOUBLE");
  }

  if(MC == DIFFUSION) {
    if(SmartMC == DMC_LINEAR_METROPOLIS || SmartMC == DMC_LINEAR_METROPOLIS_MIDDLE) { // for Metropolis linear move
      ArrayCalloc2D(W->dR_drift, "dR_drift ", i, N, 6, DOUBLE, "DOUBLE");
    }

    if(measure_spectral_weight) {
      W->psi_U = (DOUBLE*) Calloc("W->psi U", gridPhi_t, sizeof(DOUBLE));
      W->psi_r = (DOUBLE*) Calloc("W->psi r", gridPhi_t, sizeof(DOUBLE));
    }

    if(measure_PairDistr) {
      ArrayCalloc2D(W->PD, "W->PD ", i, grid_pure_block, gridPD, int, "int");
      ArrayCalloc2D(W->PDup, "W->PD ", i, grid_pure_block, gridPD, int, "int");
      ArrayCalloc2D(W->PDdn, "W->PD ", i, grid_pure_block, gridPD, int, "int");
      if(measure_PairDistrMATRIX) { // allocate pure PD matrix
        ArrayCalloc3D(W->PD_MATRIX11, "W->PD_MATRIX11 ", i, grid_pure_block, j, gridPD_MATRIX, gridPD_MATRIX, int, "int");
        ArrayCalloc3D(W->PD_MATRIX12, "W->PD_MATRIX12 ", i, grid_pure_block, j, gridPD_MATRIX, gridPD_MATRIX, int, "int");
        ArrayCalloc3D(W->PD_MATRIX22, "W->PD_MATRIX22 ", i, grid_pure_block, j, gridPD_MATRIX, gridPD_MATRIX, int, "int");
      }
    }
  } // DIFFUSION

  if(MC == MOLECULAR_DYNAMICS) {
    W->Vx = (DOUBLE*) Calloc("W->x", N, sizeof(DOUBLE));
    W->Vy = (DOUBLE*) Calloc("W->y", N, sizeof(DOUBLE));
    W->Vz = (DOUBLE*) Calloc("W->z", N, sizeof(DOUBLE));
    W->Vxdn = (DOUBLE*) Calloc("W->x", N, sizeof(DOUBLE));
    W->Vydn = (DOUBLE*) Calloc("W->y", N, sizeof(DOUBLE));
    W->Vzdn = (DOUBLE*) Calloc("W->z", N, sizeof(DOUBLE));
  }
}

/*************************** Copy Walker ************************************/
void CopyWalker(struct Walker *out, const struct Walker in) {
  int i, j, k;

  out->E = in.E;
  out->EFF = in.EFF;
  out->Epot = in.Epot;
  out->Ekin = in.Ekin;
  out->Eext = in.Eext;
  out->Eold = in.Eold;
  out->U = in.U;
  out->status = in.status;
  out->weight = in.weight;
  out->r2 = in.r2;
  out->z2 = in.z2;
  out->r2old  = in.r2old;
  out->z2old  = in.z2old;
  out->Determinant = in.Determinant;
  if(var_par_array) {
    out->varparindex = in.varparindex;
  }
#ifdef CRYSTAL
  out->UcrystalUp = in.UcrystalUp;
  out->UcrystalDn = in.UcrystalDn;
#endif

  // 1)
  CaseX(ArrayCopy1D(out->x, in.x, i, Nup));
  CaseY(ArrayCopy1D(out->y, in.y, i, Nup));
  CaseZ(ArrayCopy1D(out->z, in.z, i, Nup));
  CaseX(ArrayCopy1D(out->xdn, in.xdn, i, Ndn));
  CaseY(ArrayCopy1D(out->ydn, in.ydn, i, Ndn));
  CaseZ(ArrayCopy1D(out->zdn, in.zdn, i, Ndn));

  // 2)
  if(MC == DIFFUSION && (SmartMC == DMC_LINEAR_METROPOLIS || SmartMC == DMC_LINEAR_METROPOLIS_MIDDLE)) {
    ArrayCopy2D(out->dR_drift_old, in.dR_drift_old, i, Nup, j, 6);
    out->status_end_of_step_initialized = in.status_end_of_step_initialized;
  }

  if(MC == DIFFUSION) {
    // 3)
    if(measure_energy) {
      ArrayCopy1D(out->Epot_pure, in.Epot_pure, i, grid_pure_block);
    }

    // 4)
    if(measure_PairDistr) {
      out->PD_position = in.PD_position;
      out->g3_accum = in.g3_accum;
      out->g3_store = in.g3_store;
      ArrayCopy2D(out->PD, in.PD, i, grid_pure_block, j, gridPD);
      ArrayCopy2D(out->PDup, in.PDup, i, grid_pure_block, j, gridPD);
      ArrayCopy2D(out->PDdn, in.PDdn, i, grid_pure_block, j, gridPD);
      if(measure_PairDistrMATRIX) {
        ArrayCopy3D(out->PD_MATRIX11, in.PD_MATRIX11, i, grid_pure_block, j, gridPD_MATRIX, k, gridPD_MATRIX);
        ArrayCopy3D(out->PD_MATRIX12, in.PD_MATRIX12, i, grid_pure_block, j, gridPD_MATRIX, k, gridPD_MATRIX);
        ArrayCopy3D(out->PD_MATRIX22, in.PD_MATRIX22, i, grid_pure_block, j, gridPD_MATRIX, k, gridPD_MATRIX);
      }
    }

    // 5)
    if(measure_RadDistr) {
      out->RD_position = in.RD_position;
      out->RD_wait = in.RD_wait;
      ArrayCopy2D(out->RDup, in.RDup, i, grid_pure_block, j, gridRD);
      ArrayCopy2D(out->RDdn, in.RDdn, i, grid_pure_block, j, gridRD);
    }

    // 6)
    if(measure_SD) {
      ArrayCopy2D(out->CM, in.CM, i, 3, j, SD.size);
      ArrayCopy2D(out->rreal, in.rreal, i, Nup, j, 6);
    }

    // 7)
    if(measure_Sk) {
      out->Sk_count = in.Sk_count;
      out->Sk_position = in.Sk_position;
      ArrayCopy2D(out->Sk, in.Sk, i, grid_pure_block, j, gridSk);
      ArrayCopy2D(out->Sk12, in.Sk12, i, grid_pure_block, j, gridSk);
      ArrayCopy2D(out->Sk_N, in.Sk_N, i, grid_pure_block, j, gridSk);
    }

    // 8)
    if(measure_FormFactor) {
      ArrayCopy2D(out->rhoRe1, in.rhoRe1, i, gridSKT_t, j, gridSKT_k);
      ArrayCopy2D(out->rhoIm1, in.rhoIm1, i, gridSKT_t, j, gridSKT_k);
      ArrayCopy2D(out->rhoRe2, in.rhoRe2, i, gridSKT_t, j, gridSKT_k);
      ArrayCopy2D(out->rhoIm2, in.rhoIm2, i, gridSKT_t, j, gridSKT_k);
    }

    // 9)
    if(measure_pure_coordinates) {
      CaseX(ArrayCopy1D(out->x_pure, in.x_pure, i, Nup));
      CaseY(ArrayCopy1D(out->y_pure, in.y_pure, i, Nup));
      CaseZ(ArrayCopy1D(out->z_pure, in.z_pure, i, Nup));
      CaseX(ArrayCopy1D(out->xdn_pure, in.xdn_pure, i, Ndn));
      CaseY(ArrayCopy1D(out->ydn_pure, in.ydn_pure, i, Ndn));
      CaseZ(ArrayCopy1D(out->zdn_pure, in.zdn_pure, i, Ndn));
    }
  }
}

/*************************** Allocate Walkers *******************************/
void AllocateWalkers(void) {
  int w;
  int N;
  int NwalkersMaxDeterminant;

  if(verbosity) Message("Memory allocation : allocating walkers ...");
  N = Nmax;
  if(MC == DIFFUSION && SmartMC == DMC_PSEUDOPOTENTIAL) {
    N = Nup+Ndn;
    Message("\n  array size [Nup+Ndn][3]\n");
  }

  W = (struct Walker*) Calloc("W", NwalkersMax, sizeof(struct Walker));
  for(w=0; w<NwalkersMax; w++) {
    AllocateSingleWalker(&W[w]);
    W[w].w = w;
  }

  Wp = (struct Walker*) Calloc("Wp", NwalkersMax, sizeof(struct Walker));
  for(w=0; w<NwalkersMax; w++) {
    AllocateSingleWalker(&Wp[w]);
    Wp[w].w = w;
  }

  walkers_storage = (int*) Calloc("Walkers storage", NwalkersMax, sizeof(int));
  dead_walkers_storage = (int*) Calloc("Dead walkers storage", NwalkersMax, sizeof(int));

  if(optimization == 2)
    NwalkersMaxDeterminant = 1;
  else
    NwalkersMaxDeterminant = NwalkersMax;

#ifdef FERMIONS
  Determinant = (DOUBLE***) Calloc("Determinant ", NwalkersMaxDeterminant, sizeof(DOUBLE**));
  for(w=0; w<NwalkersMaxDeterminant; w++) {
    Determinant[w] = (DOUBLE**) Calloc("Determinant[w] ", N, sizeof(DOUBLE*));
    for(i=0; i<N; i++) Determinant[w][i] = (DOUBLE*) Calloc("Determinant[w][i] ", N, sizeof(DOUBLE));
  }
  DeterminantInv = (DOUBLE***) Calloc("DeterminantInv ", NwalkersMaxDeterminant, sizeof(DOUBLE**));
  for(w=0; w<NwalkersMaxDeterminant; w++) {
    DeterminantInv[w] = (DOUBLE**) Calloc("DeterminantInv[w] ", N, sizeof(DOUBLE*));
    for(i=0; i<N; i++) DeterminantInv[w][i] = (DOUBLE*) Calloc("DeterminantInv[w][i] ", N, sizeof(DOUBLE));
  }

  DeterminantT = (DOUBLE***) Calloc("Determinant ", NwalkersMaxDeterminant, sizeof(DOUBLE**));
  for(w=0; w<NwalkersMaxDeterminant; w++) {
    DeterminantT[w] = (DOUBLE**) Calloc("Determinant[w] ", N, sizeof(DOUBLE*));
    for(i=0; i<N; i++) DeterminantT[w][i] = (DOUBLE*) Calloc("Determinant[w][i] ", N, sizeof(DOUBLE));
  }
  DeterminantTInv = (DOUBLE***) Calloc("DeterminantInv ", NwalkersMaxDeterminant, sizeof(DOUBLE**));
  for(w=0; w<NwalkersMaxDeterminant; w++) {
    DeterminantTInv[w] = (DOUBLE**) Calloc("DeterminantInv[w] ", N, sizeof(DOUBLE*));
    for(i=0; i<N; i++) DeterminantTInv[w][i] = (DOUBLE*) Calloc("DeterminantInv[w][i] ", N, sizeof(DOUBLE));
  }

  DeterminantMoveBackUp = (DOUBLE**) Calloc("DeterminantMoveBackUp ", N, sizeof(DOUBLE*));
  for(i=0; i<N; i++) DeterminantMoveBackUp[i] = (DOUBLE*) Calloc("DeterminantMoveBackUp[i] ", N, sizeof(DOUBLE));

  DeterminantMoveInvBackUp = (DOUBLE**) Calloc("DeterminantMoveInvBackUp ", N, sizeof(DOUBLE*));
  for(i=0; i<N; i++) DeterminantMoveInvBackUp[i] = (DOUBLE*) Calloc("DeterminantMoveInvBackUp[i] ", N, sizeof(DOUBLE));
#endif // FERMIONS

  to_process = (int*) Calloc("ps ", N, sizeof(int));

  if(verbosity) Message(" done\n");
}

/*************************** Allocate Values ********************************/
void AllocateGrids(void) {
  int i,j;

  if(verbosity) Message("Memory allocation : allocating values ...");

  if(measure_OBDM) {
    OBDM.f = (DOUBLE*) Calloc("OBDM->f", gridOBDM, sizeof(DOUBLE));
    OBDM.N = (DOUBLE*) Calloc("OBDM->N", gridOBDM, sizeof(DOUBLE));
    OBDM.size = gridOBDM;
    if(OBDM.max == 0) OBDM.max = Lcutoff_pot;
    OBDM.min = 0;
    OBDM.step = OBDM.max/gridOBDM;

#ifdef TRIAL_1D
    if(OBDM.Nksize) {
      OBDM.k  = (DOUBLE*) Calloc("OBDM->k", OBDM.Nksize, sizeof(DOUBLE));
      OBDM.Nk = (DOUBLE*) Calloc("OBDM->Nk", OBDM.Nksize, sizeof(DOUBLE));
#ifdef BC_ABSENT
      OBDM.kmin = 0;
      OBDM.kmax = 2.*sqrt(Nmean); // approx kmax, 1D IFG
      Message("  OBDM.kmin = %"LE " \n", OBDM.kmin);
      for(i=0; i<OBDM.Nksize; i++) { // initialize the values of the momenta (1D)
        OBDM.k[i] = OBDM.kmin + (DOUBLE)(i)/(DOUBLE)OBDM.Nksize*OBDM.kmax;
      }
      Message("  OBDM.kmax = %"LE " \n", OBDM.kmax);
#else
      OBDM.kmin = 2.*PI/L;
      Message("  OBDM.kmin = %"LE " \n", OBDM.kmin);
      for(i=0; i<OBDM.Nksize; i++) { // initialize the values of the momenta (1D)
        OBDM.k[i] = OBDM.kmin * (DOUBLE)(i+1);
      }
      //OBDM.kmax = OBDM.kmin + (OBDM.Nksize-1)*2.*PI/L;
#endif
      OBDM.kmax = OBDM.k[OBDM.Nksize-1]; // actual kmax
#ifdef OBDM_BOSONS
      OBDMbosons.f = (DOUBLE*) Calloc("OBDM->f", gridOBDM, sizeof(DOUBLE));
      OBDMbosons.N = (DOUBLE*) Calloc("OBDM->N", gridOBDM, sizeof(DOUBLE));
      OBDMbosons.size = gridOBDM;
      OBDMbosons.max = OBDM.max;
      OBDMbosons.min = 0;
      OBDMbosons.step = OBDM.max/gridOBDM;
      OBDMbosons.k  = (DOUBLE*) Calloc("OBDM->k", OBDM.Nksize, sizeof(DOUBLE));
      OBDMbosons.Nk = (DOUBLE*) Calloc("OBDM->Nk", OBDM.Nksize, sizeof(DOUBLE));
      OBDMbosons.Nksize = OBDM.Nksize;
      OBDMbosons.kmin = 2.*PI/L;
      OBDMbosons.kmax = OBDMbosons.kmin + (OBDM.Nksize-1)*2.*PI/L;
      for(i=0; i<OBDM.Nksize; i++) OBDMbosons.k[i] = OBDM.kmin * (DOUBLE)(i+1);
#endif
    }

    OBDM22.f = (DOUBLE*) Calloc("OBDM->f", gridOBDM, sizeof(DOUBLE));
    OBDM22.N = (DOUBLE*) Calloc("OBDM->N", gridOBDM, sizeof(DOUBLE));
    OBDM22.size = gridOBDM;
    OBDM22.max = Lcutoff_pot;
    OBDM22.min = 0;
    OBDM22.step = OBDM.max/gridOBDM;
    OBDM22.Nksize = OBDM.Nksize;
    if(OBDM22.Nksize) {
      OBDM22.k  = (DOUBLE*) Calloc("OBDM->k", OBDM.Nksize, sizeof(DOUBLE));
      OBDM22.Nk = (DOUBLE*) Calloc("OBDM->Nk", OBDM.Nksize, sizeof(DOUBLE));
      OBDM22.kmin = 2.*PI/L;
      OBDM22.kmax = OBDM22.kmin + (OBDM22.Nksize-1)*2.*PI/L;
      for(i=0; i<OBDM22.Nksize; i++) { // initialize the values of the momenta (1D)
        OBDM22.k[i] = OBDM22.kmin * (DOUBLE)(i+1);
      }
    }
#ifdef OBDM_BOSONS
    OBDMbosons22.f = (DOUBLE*) Calloc("OBDM->f", gridOBDM, sizeof(DOUBLE));
    OBDMbosons22.N = (DOUBLE*) Calloc("OBDM->N", gridOBDM, sizeof(DOUBLE));
    OBDMbosons22.size = gridOBDM;
    OBDMbosons22.max = Lcutoff_pot;
    OBDMbosons22.min = 0;
    OBDMbosons22.step = OBDM.max/gridOBDM;
    OBDMbosons22.Nksize = OBDM.Nksize;
    if(OBDM22.Nksize) {
      OBDMbosons22.k  = (DOUBLE*) Calloc("OBDM->k", OBDM.Nksize, sizeof(DOUBLE));
      OBDMbosons22.Nk = (DOUBLE*) Calloc("OBDM->Nk", OBDM.Nksize, sizeof(DOUBLE));
      OBDMbosons22.kmin = 2.*PI/L;
      OBDMbosons22.kmax = OBDM22.kmin + (OBDM22.Nksize-1)*2.*PI/L;
      for(i=0; i<OBDM22.Nksize; i++) { // initialize the values of the momenta (1D)
        OBDMbosons22.k[i] = OBDM22.kmin * (DOUBLE)(i+1);
      }
    }
#endif
#endif
  }

  if(MC == PIGS && SmartMC == PIGS_PSEUDOPOTENTIAL_OBDM) {
    OBDMpath_integral.f = (DOUBLE*) Calloc("OBDM->f", gridOBDM, sizeof(DOUBLE));
    OBDMpath_integral.N = (DOUBLE*) Calloc("OBDM->N", gridOBDM, sizeof(DOUBLE));
    OBDMpath_integral.size = gridOBDM;
    if(OBDMpath_integral.max == 0) OBDMpath_integral.max = Lcutoff_pot;
    OBDMpath_integral.min = 0;
    OBDMpath_integral.step = OBDMpath_integral.max/gridOBDM;
  }

  if(measure_TBDM) {
    TBDM12.f = (DOUBLE*) Calloc("TBDM->f", gridTBDM, sizeof(DOUBLE));
    TBDM12.fcorrected = (DOUBLE*) Calloc("TBDM->fcorrected", gridTBDM, sizeof(DOUBLE));
    TBDM12.N = (DOUBLE*) Calloc("TBDM->N", gridTBDM, sizeof(DOUBLE));
    TBDM12.R2 = (DOUBLE*) Calloc("TBDM->R2", gridTBDM, sizeof(DOUBLE));
    TBDM12.R2corrected = (DOUBLE*) Calloc("TBDM->R2corrected", gridTBDM, sizeof(DOUBLE));
    TBDM12.OBDM = (DOUBLE*) Calloc("TBDM->OBDM", gridTBDM, sizeof(DOUBLE));
    TBDM12.fr = (DOUBLE*) Calloc("TBDM->f", gridTBDM, sizeof(DOUBLE));
    TBDM12.Nr = (DOUBLE*) Calloc("TBDM->N", gridTBDM, sizeof(DOUBLE));

    TBDM11.f = (DOUBLE*) Calloc("TBDM11->f", gridTBDM, sizeof(DOUBLE));
    TBDM11.N = (DOUBLE*) Calloc("TBDM11->N", gridTBDM, sizeof(DOUBLE));
    TBDM11.OBDM = (DOUBLE*) Calloc("TBDM11->OBDM", gridTBDM, sizeof(DOUBLE));
    TBDM11.fr = (DOUBLE*) Calloc("TBDM11->f", gridTBDM, sizeof(DOUBLE));
    TBDM11.Nr = (DOUBLE*) Calloc("TBDM11->N", gridTBDM, sizeof(DOUBLE));

    TBDM22.f = (DOUBLE*) Calloc("TBDM22->f", gridTBDM, sizeof(DOUBLE));
    TBDM22.N = (DOUBLE*) Calloc("TBDM22->N", gridTBDM, sizeof(DOUBLE));
    TBDM22.OBDM = (DOUBLE*) Calloc("TBDM22->OBDM", gridTBDM, sizeof(DOUBLE));
    TBDM22.fr = (DOUBLE*) Calloc("TBDM22->f", gridTBDM, sizeof(DOUBLE));
    TBDM22.Nr = (DOUBLE*) Calloc("TBDM22->N", gridTBDM, sizeof(DOUBLE));

    TBDM11.size = TBDM12.size = TBDM22.size = gridTBDM;
    if(TBDM12.max == 0) TBDM12.max = Lcutoff_pot;
    TBDM11.max = TBDM22.max = TBDM12.max;
    TBDM11.min = TBDM12.min = TBDM22.min = 0;
    TBDM11.step = TBDM12.step = TBDM22.step = TBDM12.max/TBDM12.size;

#ifdef OBDM_BOSONS
    TBDMbosons12.f = (DOUBLE*) Calloc("TBDM->f", gridTBDM, sizeof(DOUBLE));
    TBDMbosons12.fcorrected = (DOUBLE*) Calloc("TBDM->fcorrected", gridTBDM, sizeof(DOUBLE));
    TBDMbosons12.N = (DOUBLE*) Calloc("TBDM->N", gridTBDM, sizeof(DOUBLE));
    TBDMbosons12.R2 = (DOUBLE*) Calloc("TBDM->R2", gridTBDM, sizeof(DOUBLE));
    TBDMbosons12.R2corrected = (DOUBLE*) Calloc("TBDM->R2corrected", gridTBDM, sizeof(DOUBLE));
    TBDMbosons12.OBDM = (DOUBLE*) Calloc("TBDM->OBDM", gridTBDM, sizeof(DOUBLE));
    TBDMbosons12.size = gridTBDM;
    if(TBDMbosons12.max == 0) TBDMbosons12.max = Lcutoff_pot;
    TBDMbosons12.min = 0;
    TBDMbosons12.step = TBDMbosons12.max/TBDMbosons12.size;
    TBDMbosons12.fr = (DOUBLE*) Calloc("TBDM->f", gridTBDM, sizeof(DOUBLE));
    TBDMbosons12.Nr = (DOUBLE*) Calloc("TBDM->N", gridTBDM, sizeof(DOUBLE));

    TBDMbosons11.f = (DOUBLE*) Calloc("TBDMbosons11->f", gridTBDM, sizeof(DOUBLE));
    TBDMbosons11.N = (DOUBLE*) Calloc("TBDMbosons11->N", gridTBDM, sizeof(DOUBLE));
    TBDMbosons11.OBDM = (DOUBLE*) Calloc("TBDM->OBDM", gridTBDM, sizeof(DOUBLE));
    TBDMbosons11.fr = (DOUBLE*) Calloc("TBDMbosons11->f", gridTBDM, sizeof(DOUBLE));
    TBDMbosons11.Nr = (DOUBLE*) Calloc("TBDMbosons11->N", gridTBDM, sizeof(DOUBLE));

    TBDMbosons22.f = (DOUBLE*) Calloc("TBDMbosons22->f", gridTBDM, sizeof(DOUBLE));
    TBDMbosons22.N = (DOUBLE*) Calloc("TBDMbosons22->N", gridTBDM, sizeof(DOUBLE));
    TBDMbosons22.OBDM = (DOUBLE*) Calloc("TBDM->OBDM", gridTBDM, sizeof(DOUBLE));
    TBDMbosons22.fr = (DOUBLE*) Calloc("TBDMbosons22->f", gridTBDM, sizeof(DOUBLE));
    TBDMbosons22.Nr = (DOUBLE*) Calloc("TBDMbosons22->N", gridTBDM, sizeof(DOUBLE));
#endif
  }

  if(measure_TBDM_MATRIX || measure_TBDM) {
    TBDM_MATRIX.size = gridTBDM_MATRIX;
    TBDM_MATRIX.f = (DOUBLE**) Calloc("TBDM_MATRIX.f ", gridTBDM, sizeof(DOUBLE*));
    for(i=0; i<gridTBDM; i++) TBDM_MATRIX.f[i] = (DOUBLE*) Calloc("TBDM_MATRIX.f[i] ", TBDM_MATRIX.size, sizeof(DOUBLE));
    TBDM_MATRIX.N = (int**) Calloc("TBDM_MATRIX.N ", gridTBDM, sizeof(int*));
    for(i=0; i<gridTBDM; i++) TBDM_MATRIX.N[i] = (int*) Calloc("TBDM_MATRIX.N[i] ", TBDM_MATRIX.size, sizeof(int));
    if(TBDM_MATRIX.max == 0) TBDM_MATRIX.max = Lcutoff_pot;
    TBDM_MATRIX.min = 0;
    TBDM_MATRIX.step = TBDM_MATRIX.max/TBDM_MATRIX.size;
  }

  if(measure_PairDistrMATRIX) {
    if(PD_MATRIXmax == 0) PD_MATRIXmax = Lhalf;

    ArrayCalloc2D(PD_MATRIX11.N, "PD_MATRIX11->N ", i, gridPD_MATRIX, gridPD_MATRIX, int, "int");
    PD_MATRIX11.max = PD_MATRIXmax;
    PD_MATRIX11.step = PD_MATRIX11.max/(gridPD_MATRIX-1.);

    ArrayCalloc2D(PD_MATRIX12.N, "PD_MATRIX12->N ", i, gridPD_MATRIX, gridPD_MATRIX, int, "int");
    PD_MATRIX12.max = PD_MATRIXmax;
    PD_MATRIX12.step = PD_MATRIX12.max/(gridPD_MATRIX-1.);

    ArrayCalloc2D(PD_MATRIX22.N, "PD_MATRIX22->N ", i, gridPD_MATRIX, gridPD_MATRIX, int, "int");
    PD_MATRIX22.max = PD_MATRIXmax;
    PD_MATRIX22.step = PD_MATRIX22.max/(gridPD_MATRIX-1.);

    if(MC && measure_PairDistrMATRIX) {
      ArrayCalloc2D(PD_MATRIX11_pure.N, "PD_MATRIX11_pure->N ", i, gridPD_MATRIX, gridPD_MATRIX, int, "int");
      ArrayCalloc2D(PD_MATRIX12_pure.N, "PD_MATRIX11_pure->N ", i, gridPD_MATRIX, gridPD_MATRIX, int, "int");
      ArrayCalloc2D(PD_MATRIX22_pure.N, "PD_MATRIX11_pure->N ", i, gridPD_MATRIX, gridPD_MATRIX, int, "int");
    }
  }

  if(measure_Nk) {
    Nk.size = gridNk;
    Nk.k = (DOUBLE*) Calloc("Nk.k", Nk.size, sizeof(DOUBLE));
    Nk.f = (DOUBLE**) Calloc("Nk.f ", Nk.size, sizeof(DOUBLE*));
    for(i=0; i<Nk.size; i++) {
      Nk.f[i] = (DOUBLE*) Calloc("Nk.f[i] ", Nk.size, sizeof(DOUBLE));
      Nk.k[i] = (DOUBLE)(i)*2.*PI/L;
    }
  }

#ifdef BC_ABSENT
  OBDMtrap.f = (DOUBLE*) Calloc("OBDMtrap->f", gridOBDM, sizeof(DOUBLE));
  OBDMtrap.N = (DOUBLE*) Calloc("OBDMtrap->N", gridOBDM, sizeof(DOUBLE));
  OBDMtrap.size = gridOBDM;
  if(OBDMtrap.max == 0) OBDMtrap.max = Lcutoff_pot;
  OBDMtrap.min = 0;
  OBDMtrap.step = OBDMtrap.max/gridOBDM;
#endif

  if(measure_PairDistr) {
    PD.N = (int*) Calloc("PD", gridPD, sizeof(int));
    PD.size = gridPD;
    PD.width = 0.1;
    PD.min = 0;
    if(PD.max == 0) PD.max = Lcutoff_pot;
    PD.r2 = 0.;
    PD.step = PD.max/gridPD;
    PD.times_measured = 0;

    PDup.N = (int*) Calloc("PD", gridPD, sizeof(int));
    PDup.size = gridPD;
    PDup.width = 0.1;
    PDup.min = 0;
    if(PDup.max == 0) PDup.max = Lcutoff_pot;
    PDup.r2 = 0.;
    PDup.step = PDup.max/gridPD;
    PDup.times_measured = 0;

    PDdn.N = (int*) Calloc("PD", gridPD, sizeof(int));
    if(measure_effective_up_dn_potential) PDdn.f = (DOUBLE*) Calloc("PD", gridPD, sizeof(DOUBLE));
    PDdn.size = gridPD;
    PDdn.width = 0.1;
    PDdn.min = 0;
    if(PDdn.max == 0) PDdn.max = Lcutoff_pot;
    PDdn.r2 = 0.;
    PDdn.step = PDdn.max/gridPD;
    PDdn.times_measured = 0;
  }

  if(measure_g3) {
    HR.N = (int*) Calloc("HR", gridg3, sizeof(int));
    HR.f = (DOUBLE*) Calloc("HR", gridg3, sizeof(DOUBLE));
    HR.size = gridg3;
    HR.width = 0.1;
    HR.min = 0;
    if(HR.max == 0) HR.max = Lcutoff_pot;
    HR.r2 = 0.;
    HR.step = HR.max/gridg3;
    HR.times_measured = 0;
  }

  if(MC == DIFFUSION) { 
    Epot_pure.x = (DOUBLE*) Calloc("Epot_pure", grid_pure_block, sizeof(DOUBLE));
    Epot_pure.f = (DOUBLE*) Calloc("Epot_pure", grid_pure_block, sizeof(DOUBLE));
    Epot_pure.size = grid_pure_block;
    Epot_pure.min = 0;
    Epot_pure.step = dt * (DOUBLE) Nmeasure;
    Epot_pure.max = Epot_pure.step*(Epot_pure.size-1);
    for(i=0; i<Epot_pure.size; i++) Epot_pure.x[i] = Epot_pure.step * (DOUBLE) i;
  }

  if(MC == DIFFUSION && measure_PairDistr) {
    PD_pure.N =   (int*) Calloc("PD_pure", gridPD, sizeof(int));
    PDup_pure.N = (int*) Calloc("PDup_pure", gridPD, sizeof(int));
    PDdn_pure.N = (int*) Calloc("PDdn_pure", gridPD, sizeof(int));
    PD_pure.size = gridPD;
    PD_pure.width = 0.1;
    PD_pure.min = 0;
    PD_pure.max = PD.max;
    if(PD_pure.max == 0) PD_pure.max = Lcutoff_pot;
    PD_pure.r2 = 0.;
    PD_pure.step = PD_pure.max/gridPD;
    PD_pure.times_measured = 0;
  }

  if(measure_RadDistr) {
    RD.N    = (int*) Calloc("RD", gridRD, sizeof(int));
    RDxup.N = (int*) Calloc("RD", gridRD, sizeof(int));
    RDyup.N = (int*) Calloc("RD", gridRD, sizeof(int));
    RDzup.N = (int*) Calloc("RD", gridRD, sizeof(int));
    RDup.N = (int*) Calloc("RD", gridRD, sizeof(int));
    RDxdn.N = (int*) Calloc("RD", gridRD, sizeof(int));
    RDydn.N = (int*) Calloc("RD", gridRD, sizeof(int));
    RDzdn.N = (int*) Calloc("RD", gridRD, sizeof(int));
    RDdn.N = (int*) Calloc("RD", gridRD, sizeof(int));
    RD.size = gridRD;
    RD.min = 0;
    RD.r2 = 0.;
    if(RD.max == 0) RD.max = L;
    RD.step = RD.max/gridRD;
    RD.times_measured = 0;

    if(RDxup.max == 0) RDxup.max = L;
    if(RDyup.max == 0) RDyup.max = L;
    if(RDzup.max == 0) RDzup.max = L;
    if(RDup.max == 0) RDup.max = L;
    if(RDxdn.max == 0) RDxdn.max = L;
    if(RDydn.max == 0) RDydn.max = L;
    if(RDzdn.max == 0) RDzdn.max = L;
    if(RDdn.max == 0) RDdn.max = L;
    RDxup.step = RDxup.max/gridRD;
    RDyup.step = RDyup.max/gridRD;
    RDzup.step = RDzup.max/gridRD;
    RDup.step = RDup.max/gridRD;
    RDxdn.step = RDxdn.max/gridRD;
    RDydn.step = RDydn.max/gridRD;
    RDzdn.step = RDzdn.max/gridRD;
    RDdn.step = RDdn.max/gridRD;
  }

  if(MC == DIFFUSION && measure_RadDistr) {
    RDup_pure.N = (int*) Calloc("RDup_pure", gridRD, sizeof(int));
    RDup_pure.size = gridRD;
    RDup_pure.times_measured = 0;
    RDdn_pure.N = (int*) Calloc("RDdn_pure", gridRD, sizeof(int));
    RDdn_pure.size = gridRD;
    RDdn_pure.times_measured = 0;
  }

  if(measure_Sk) {
    Sk.f = (DOUBLE*) Calloc("Sk->f", gridSk, sizeof(DOUBLE));
    Sk.f12 = (DOUBLE*) Calloc("Sk->f12", gridSk, sizeof(DOUBLE));
    Sk.cos = (DOUBLE*) Calloc("Sk->cos", gridSk, sizeof(DOUBLE));
    Sk.N = (DOUBLE*) Calloc("Sk->N", gridSk, sizeof(DOUBLE));
    Sk.size = gridSk;
    Sk.k =  (DOUBLE*) Calloc("SK->k", gridSk, sizeof(DOUBLE));
    Sk.kx = (DOUBLE*) Calloc("SK->kx", gridSk, sizeof(DOUBLE));
    Sk.ky = (DOUBLE*) Calloc("SK->ky", gridSk, sizeof(DOUBLE));
    Sk.kz = (DOUBLE*) Calloc("SK->kz", gridSk, sizeof(DOUBLE));
    Sk.index = (int*) Calloc("SK->index", gridSk, sizeof(int));
    Sk.degeneracy = (int*) Calloc("SK->degeneracy", gridSk, sizeof(int));
    Sk.phi = (DOUBLE*) Calloc("SK->phi", gridSk, sizeof(DOUBLE));
    Sk.phiN = (DOUBLE*) Calloc("SK->phiN", gridSk, sizeof(DOUBLE));

    if(Sk.L == 0) {
      Sk.L = L;
      Message("  Sk max = %"LE"\n", Sk.L);
    }
    else
      Sk.L = 2*PI*Sk.size/Sk.L;

    for(i=0; i<Sk.size; i++) { // initialize the values of the momenta (1D)
      Sk.k[i] = 2*PI/Sk.L * (i+1);
    }
  }

  if(measure_Sk && MC == DIFFUSION) {
    Sk_pure.f = (DOUBLE*) Calloc("Sk_pure.f", gridSk, sizeof(DOUBLE));
    Sk_pure.f12 = (DOUBLE*) Calloc("Sk_pure.f", gridSk, sizeof(DOUBLE));
    Sk_pure.N = (DOUBLE*) Calloc("Sk_pure.N", gridSk, sizeof(DOUBLE));
    Sk_pure.times_measured = 0;
    Sk_pure.size = gridSk;
    Sk_pure.L = Sk.L;
  }

  if(measure_SD) {
    SD.CM2 = (DOUBLE*) Calloc("SD->CM2", SD.size, sizeof(DOUBLE));
    SD.N   = (int*) Calloc("SD->N", SD.size, sizeof(int));
  }

  if(measure_spectral_weight) {
    Phi.f = (DOUBLE**) Calloc("Phi.f", gridPhi_x, sizeof(DOUBLE*));
    for(j=0; j<gridPhi_x; j++) {
      Phi.f[j] = (DOUBLE*) Calloc("Phi.f", gridPhi_t, sizeof(DOUBLE));
    }
  }

  if(var_par_array) {
    varpar = (DOUBLE**) Calloc("varar", NwalkersMax, sizeof(DOUBLE*));
    for(j=0; j<NwalkersMax; j++) {
      varpar[j] = (DOUBLE*) Calloc("varpar", 10, sizeof(DOUBLE));
    }
  }

#ifdef CRYSTAL_SYMM_SUM_PARTICLE
  MoUp = (DOUBLE*) Calloc("MoUp", Nmax, sizeof(DOUBLE));
  MoDn = (DOUBLE*) Calloc("MoDn", Nmax, sizeof(DOUBLE));
#endif

  u_mi = (DOUBLE*) Calloc("u_mi", Nmax, sizeof(DOUBLE));
  u_ij = (DOUBLE*) Calloc("u_ij", Nmax, sizeof(DOUBLE));

  mu_k = (DOUBLE*) Calloc("mu_k", Nmax, sizeof(DOUBLE));
  mu_p_k =  (DOUBLE*) Calloc("mu_p_k", Nmax, sizeof(DOUBLE));
  mu_pp_k =  (DOUBLE*) Calloc("mu_pp_k", Nmax, sizeof(DOUBLE));

  if(MC == DIFFUSION && measure_FormFactor) {
    ArrayCalloc2D(SKT.f, "SKT->f ", i, gridSKT_t, gridSKT_k, DOUBLE, "DOUBLE");
    ArrayCalloc2D(SKT.f12, "SKT->f12 ", i, gridSKT_t, gridSKT_k, DOUBLE, "DOUBLE");
    ArrayCalloc2D(SKT.N, "SKT->N ", i, gridSKT_t, gridSKT_k, int, "int");
  }

  if(measure_XY2) ArrayCalloc2D(XY2_MATRIX.f, "XY2_MATRIX ", i, Nup, Nup, DOUBLE, "DOUBLE");
}

/************************** Calloc ******************************************/
void* Calloc(const char* name, unsigned length, size_t size) {
  void *p = calloc(length, size);

  if(p == NULL) Error("Not enough memory for %s", name);

  return p;
}

/********************************* Copy Walker *******************************/
void CopyWalkerCoord(struct Walker *out, const struct Walker in) {

#ifndef MEMORY_CONTIGUOUS
  int i;
#endif
  CaseX(ArrayCopy1D(out->x, in.x, i, Nup));
  CaseY(ArrayCopy1D(out->y, in.y, i, Nup));
  CaseZ(ArrayCopy1D(out->z, in.z, i, Nup));
  CaseX(ArrayCopy1D(out->xdn, in.xdn, i, Ndn));
  CaseY(ArrayCopy1D(out->ydn, in.ydn, i, Ndn));
  CaseZ(ArrayCopy1D(out->zdn, in.zdn, i, Ndn));
}

/********************************* Walker Compare ****************************/
int WalkerCompare(const struct Walker x, const struct Walker y) {
  int i;
  int ret = 0;

  for(i=0; i<Nup; i++) {
    if(x.x[i] != y.x[i]) ret = 1;
    if(x.y[i] != y.y[i]) ret = 1;
    if(x.z[i] != y.z[i]) ret = 1;
  }
  for(i=0; i<Ndn; i++) {
    if(x.xdn[i] != y.xdn[i]) ret = 1;
    if(x.ydn[i] != y.ydn[i]) ret = 1;
    if(x.zdn[i] != y.zdn[i]) ret = 1;
  }
  if(x.E != y.E) ret = 1;
  if(x.U != y.U) ret = 1;
  if(MC == VARIATIONAL && x.Determinant != y.Determinant) ret = 1;

  if(ret) {
    for(i=0; i<Nup; i++) Warning("%i   %"LF" %"LF" %"LF"  %"LF" %"LF" %"LF"\n", i+1, x.x[i], x.y[i], x.z[i], y.x[i], y.y[i], y.z[i]);
    for(i=0; i<Ndn; i++) Warning("%i   %"LF" %"LF" %"LF"  %"LF" %"LF" %"LF"\n", i+1, x.xdn[i], x.ydn[i], x.zdn[i], y.xdn[i], y.ydn[i], y.zdn[i]);
    Warning("%"LF" %"LF"\n", x.E, y.E);
    Warning("%"LF" %"LF"\n", x.U, y.U);
  }

  return ret;
}

/************************* CallocContiguous2D ********************************/
void* CallocContiguous2D(const char* name, unsigned dim1, unsigned dim2, char *type) {
// Allocate 2D array of size [dim1][dim2]
// type is a string: "DOUBLE", "DOUBLE", "float", "int"
  DOUBLE **pDOUBLE;
  double **pdouble;
  float **pfloat;
  int **pint;
  unsigned i;

  if(strcmp(type, "DOUBLE") == 0) {
    pDOUBLE = (DOUBLE**) Calloc(name, dim1, sizeof(DOUBLE*));
    pDOUBLE[0] = (DOUBLE*) Calloc(name, dim1*dim2, sizeof(DOUBLE)); // allocate contiguous memory
    for(i=1; i<dim1; i++) pDOUBLE[i] = pDOUBLE[0] + i*dim2; // set pointers
    return pDOUBLE;
  }
  else if(strcmp(type, "double") == 0) {
    pdouble = (double**) Calloc(name, dim1, sizeof(double*));
    pdouble[0] = (double*) Calloc(name, dim1*dim2, sizeof(double)); // allocate contiguous memory
    for(i=1; i<dim1; i++) pdouble[i] = pdouble[0] + i*dim2; // set pointers
    return pdouble;
  }
  else if(strcmp(type, "float") == 0) {
    pfloat = (float**) Calloc(name, dim1, sizeof(float*));
    pfloat[0] = (float*) Calloc(name, dim1*dim2, sizeof(float)); // allocate contiguous memory
    for(i=1; i<dim1; i++) pfloat[i] = pfloat[0] + i*dim2; // set pointers
    return pfloat;
  }
  else { //if(strcmp(type, "int") == 0) {
    pint = (int**) Calloc(name, dim1, sizeof(int*));
    pint[0] = (int*) Calloc(name, dim1*dim2, sizeof(int)); // allocate contiguous memory
    for(i=1; i<dim1; i++) pint[i] = pint[0] + i*dim2; // set pointers
    return pint;
  }
}

/************************* CallocContiguous3D ********************************/
void* CallocContiguous3D(const char* name, unsigned dim1, unsigned dim2, unsigned dim3, char *type) {
// Allocate 3D array of size [dim1][dim2][dim3]
// type is a string: "DOUBLE", "double", "float", "int"
  DOUBLE ***pDOUBLE;
  //double ***pdouble;
  //float ***pfloat;
  //int ***pint;
  unsigned i,j;

  Error("CallocContiguous3D not yet implemented\n");

  //if(strcmp(type, "DOUBLE") == 0) {
    pDOUBLE = (DOUBLE***) Calloc(name, dim1, sizeof(DOUBLE**));
    pDOUBLE[0][0] = (DOUBLE*) Calloc(name, dim1*dim2*dim3, sizeof(DOUBLE)); // allocate contiguous memory
    for(i=0; i<dim1; i++) for(j=0; j<dim2; j++) pDOUBLE[i][j] = pDOUBLE[0][0] + i*dim1*dim2 + j*dim1; // set pointers
    return pDOUBLE;
  //}
}
