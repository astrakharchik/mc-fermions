/*ewald.h*/

#ifndef _EWALD_H
#define _EWALD_H

#include "compatab.h"

DOUBLE InteractionEnergySumOverImages12(DOUBLE x, DOUBLE y, DOUBLE z);
DOUBLE InteractionEnergySumOverImages11(DOUBLE x, DOUBLE y, DOUBLE z);
DOUBLE InteractionEnergySumOverImages22(DOUBLE x, DOUBLE y, DOUBLE z);
DOUBLE InteractionSelfEnergy11(void);
DOUBLE InteractionSelfEnergy22(void);

#endif
