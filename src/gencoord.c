/*gencoord.c*/
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

#include "main.h"
#include "gencoord.h"
#include "move.h"
#include "randnorm.h"
#include "utils.h"
#include "trialf.h"
#include "memory.h"

/******************************* Generate Coordinates ************************/
int GenerateCoordinates(void) {
  int w,i;
  int go;
  int iter;
  DOUBLE angle = 0;

  Message("Generating particle coordinates ... ");

  Randomize();

  for(w=0; w<Nwalkers; w++) {
    go = ON;
    iter = 0;
    while(go) {
      for(i=0; i<Nup; i++) GenerateOneParticleCoordinates(&W[w].x[i],&W[w].y[i],&W[w].z[i],i,1);
      for(i=0; i<Ndn; i++) GenerateOneParticleCoordinates(&W[w].xdn[i],&W[w].ydn[i],&W[w].zdn[i],i,0);
#ifdef INTERACTION12_DIPOLE_BILAYER // generate coordinates in molecular regime as pairs
      for(i=0; i<Ndn; i++) if(i<Nup) {
        W[w].xdn[i] = fabs(W[w].x[i]-1.);
        W[w].ydn[i] = fabs(W[w].y[i]-1.);
      }
#endif

      if(MC == PIGS) CopyWalkerCoord(&W[w], W[0]); // for PIGS start from a classical configuration
      if(MC == PIMC) CopyWalkerCoord(&W[w], W[0]); // for PIMC start from a classical configuration

#ifdef BC_BILAYER
      for(i=0; i<Nup; i++) W[w].z[i] = bilayer_width*0.5;
      for(i=0; i<Ndn; i++) W[w].zdn[i] = -bilayer_width*0.5;
#endif

      if(!OverlappingWalker(&W[w])) go = OFF;

      if(iter++ > MAX_GEN_ITER) Error("Can't generate initial configuration\n");
    }
    //Message("  generated %i-th walker %i iterations\n", w+1,iter);
  }
  Nwalkersw = (DOUBLE) Nwalkers;

  if(CheckOverlapping()) Error(" generated configuration is wrong");
  Message("done\n");

  return 0;
}

/******************************* Generate Coordinates ************************/
void GenerateOneParticleCoordinates(DOUBLE *x, DOUBLE *y, DOUBLE *z, int i, int up) {
  DOUBLE epsilon = 1e-3; // avoid division by zero
#ifdef VEXT_COS2
  int nx,ny,nz;
#endif
#ifdef CRYSTAL
  DOUBLE spreading;

  //spreading = 0.;
  spreading = L / (10. * pow(Nup, 1./(DOUBLE) DIMENSION));
#endif

#ifdef TRIAL_3D //  i.e. 3D simulation
  if(boundary == ONE_BOUNDARY_CONDITION) {
    *x = (1-2. * RandomSys())*0.5;
    *y = (1-2. * RandomSys())*0.5;
    *z = L * RandomSys();
  }
  else if(boundary == TWO_BOUNDARY_CONDITIONS) {
    *x = L * RandomSys();
    *y = L * RandomSys();
    *z = (1-2. * RandomSys())*0.5;
  }
  else if(boundary == THREE_BOUNDARY_CONDITIONS) {
    *x = L * RandomSys();
    *y = L * RandomSys();
    *z = L * RandomSys();
  }
#endif

#ifdef TRIAL_2D //  i.e. 2D simulation
  if(boundary == TWO_BOUNDARY_CONDITIONS) {
    *x = L * RandomSys();
    *y = L * RandomSys();
    *z = (1-2. * RandomSys())*0.5/(alpha_z_up + epsilon);
  }
#endif

#ifdef TRIAL_1D //  i.e. 1D simulation
  if(boundary == ONE_BOUNDARY_CONDITION) { // there is a restriction in the z direction
    *z = L * RandomSys();
  }
#endif

  if(boundary == NO_BOUNDARY_CONDITIONS) {
#ifdef MOVE_IN_X
    if(up) {
      if(alpha_x_up > 0) {
        *x = 1 - 2.*RandomSys() / alpha_x_up;
      }
      else {
        *x = 1 - 2.*RandomSys();
      }
    }
    else {
      if(alpha_x_dn > 0) {
        *x = 1 - 2.*RandomSys() / alpha_x_dn;
      }
      else {
        *x = 1 - 2.*RandomSys();
      }
    }
#endif
#ifdef MOVE_IN_Y
    if(up) {
      if(alpha_y_up > 0) {
        *y = 1 - 2.*RandomSys() / alpha_y_up;
      }
      else {
        *y = 1 - 2.*RandomSys();
      }
    }
    else {
      if(alpha_y_dn > 0) {
        *y = 1 - 2.*RandomSys() / alpha_y_dn;
      }
      else {
        *y = 1 - 2.*RandomSys();
      }
    }
#endif
#ifdef MOVE_IN_Z
    if(up) {
      if(alpha_z_up > 0) {
        *z = 1 - 2.*RandomSys() / alpha_z_up;
      }
      else {
        *z = 1 - 2.*RandomSys();
      }
    }
    else {
      if(alpha_z_dn > 0) {
        *z = 1 - 2.*RandomSys() / alpha_z_dn;
      }
      else {
        *z = 1 - 2.*RandomSys();
      }
    }
#endif
  }

#ifdef CRYSTAL
  if(boundary == THREE_BOUNDARY_CONDITIONS) {
    if(up) {
      *x = Crystal.x[i] + (1-2.*RandomSys())*spreading;
      *y = Crystal.y[i] + (1-2.*RandomSys())*spreading;
      *z = Crystal.z[i] + (1-2.*RandomSys())*spreading;
    }
    else {
      *x = Crystal.xdn[i] + (1-2.*RandomSys())*spreading;
      *y = Crystal.ydn[i] + (1-2.*RandomSys())*spreading;
      *z = Crystal.zdn[i] + (1-2.*RandomSys())*spreading;
    }
  }
  else if(boundary == TWO_BOUNDARY_CONDITIONS) {
    if(up) {
      *x = Crystal.x[i] + (1-2.*RandomSys())*spreading;
      *y = Crystal.y[i] + (1-2.*RandomSys())*spreading;
    }
    else {
      *x = Crystal.xdn[i] + (1-2.*RandomSys())*spreading;
      *y = Crystal.ydn[i] + (1-2.*RandomSys())*spreading;
    }
  }
  else if(boundary == ONE_BOUNDARY_CONDITION) {
    if(up) {
      *z = Crystal.z[i] + (1-2.*RandomSys())*spreading;
    }
    else {
      *z = Crystal.zdn[i] + (1-2.*RandomSys())*spreading;
    }
   }
#endif

#ifdef VEXT_COS2
#ifdef BC_3DPBC_CUBE
    //Message("%i  ", i);
   // 0<= i < Nlattice^3
    nx = i/(Nlattice*Nlattice);
    i -= nx*Nlattice*Nlattice;
    // 0<= i < Nlattice^2
    ny = i/Nlattice;
    i -= ny*Nlattice;
    // 0<= i < Nlattice
    nz = i;
    //Message("%i %i %i\n", nx, ny, nz);
    *x = PI*(0.5+nx)/kL;
    *y = PI*(0.5+ny)/kL;
    *z = PI*(0.5+nz)/kL;
#endif
#ifdef BC_2DPBC_SQUARE
    // 0<= i < Nlattice^2
    nx = i/(Nlattice*Nlattice);
    i -= ny*Nlattice*Nlattice;
    // 0<= i < Nlattice
    ny = i;

    *x = PI*(0.5+nx)/kL;
    *y = PI*(0.5+ny)/kL;
    *z = 0.;
#endif
#ifdef BC_1DPBC_Z
    *x = 0.;
    *y = 0.;
    *z = PI*(0.5+i)/kL;
#endif
#endif

#ifdef ONE_BODY_TRIAL_LINEAR_CHAIN_OF_HARMONIC_OSCILLATORS_UP
  if(up) {
    *x = 1-2.*RandomSys();
    *y = 1-2.*RandomSys();
#ifdef BC_ABSENT // centered at 0
    *z = Lz *((DOUBLE)i - 0.5*((DOUBLE)Nup-1.))/(DOUBLE)Nup;
#else // PBC
    *z = Lz / (DOUBLE)Nup*((DOUBLE)i + 0.5);
#endif
  } else {
    *x = 1-2.*RandomSys();
    *y = 1-2.*RandomSys();
#ifdef BC_ABSENT // centered at 0
    *z = 1-2.*RandomSys();
#else // PBC
    *z = Lz * RandomSys();
#endif
  }
#endif

#ifdef COORDINATES_BORN_OPPENHEIMER_2COMPONENT
  if(!up) {
    *x = 0.5*Lx;
    *y = 0.5*Ly;
    if(Dpar<1e-6) Dpar = 1e-6; // protect from equal coordinates -> r=0 and division by zero
    *z = 0.25*Lz + 0.5*Lz*Dpar*(DOUBLE)i;
  }
#endif

#ifdef INFINITE_MASS_COMPONENT_DN
  if(Ndn == 1) { // single impurity problem, put it to the center
    if(!up) {
      *x = Lx/2.;
      *y = Ly/2.;
      *z = Lz/2.;
    }
  }
#endif

  // additional check
#ifndef MOVE_IN_X
  *x = 0.;
#endif
#ifndef MOVE_IN_Y
  *y = 0.;
#endif
#ifndef MOVE_IN_Z
  *z = 0.;
#endif
}
