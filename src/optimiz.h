/*optimiz.h*/

#ifndef _OPTIMIZ_H_
#define _OPTIMIZ_H_

#include "main.h"
#include "compatab.h"
#include "crystal.h"

// number of the parameter to be adjusted
#define VAR_PAR_NUMBER 0

void INLINE AdjustVariationalParameter(int varparindex) {
//#ifdef CRYSTAL
//  Crystal.R = varpar[varparindex][0];
//#endif
  //alpha = varpar[varparindex][0];!!!
  beta = varpar[varparindex][1];
}

void INLINE InitializeVariationalParameters(void) {
//#ifdef CRYSTAL
//  varpar[0][0] = Crystal.R;
//#endif
  varpar[0][1] = beta;
}

// output normalization factor
#define VAR_PAR_UNIT (L/Nlattice*L/Nlattice)
// number of the parameter to be adjusted
#define VAR_PAR_NUMBER 0

void Optimization(void);
void MeasureEnergyReweighting(void);
int SaveCoordinatesOptimization(void);
int LoadCoordinatesOptimization(void);
DOUBLE OptimizationE(void);
void OptimizationAnnealing(DOUBLE *par);
DOUBLE OptimizationGradient(DOUBLE *par);
void OptimizationEnergy(DOUBLE Etarget, DOUBLE *Emean, DOUBLE *sigma);

DOUBLE opt_Etarget;
DOUBLE opt_sigma;

#endif
