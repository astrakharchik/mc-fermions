/*md.c*/

#include <math.h>
#include "main.h"
#include "memory.h"
#include "move.h"
#include "vmc.h"
#include "randnorm.h"
#include "rw.h"

/***************************** Force 11 ********************************************/
// F(r) = -dV(r)/dr
DOUBLE Force11(DOUBLE r) {
  DOUBLE f;
//#ifdef INTERACTION1122_DIPOLE // dipoles, 1/r^3
  //return 3./(r*r*r*r);
//#endif

#ifdef INTERACTION11_CALOGERO // Dpar / r^2
  return 2.*Dpar/(r*r*r);
#endif

#ifdef INTERACTION11_LiLi
  return 2.6655e8/(r*r*r*r*r*r*r*r*r*r*r*r*r) + (50255.331905*Cos(1.84959*(-5.03762 + r)))*Exp(-1.20145*r) + (77366.315151*Sin(1.84959*(-5.03762 + r)))*Exp(-1.20145*r);
#endif

#ifdef INTERACTION11_COULOMB
  return 1./(r*r*excitonRadius);
#endif

#ifdef INTERACTION11_HAMILTONIAN_MEAN_FIELD
  return Dpar*PI/L*sin(PI*r/L);
#endif

#ifdef INTERACTION11_RYDBERG_SCREENED
  return 6.*r*r*r*r*r/((r*r*r*r*r*r+a*a*a*a*a*a)*(r*r*r*r*r*r+a*a*a*a*a*a));
#endif

  return 0.;
}

/***************************** Force 12 ********************************************/
DOUBLE Force12(DOUBLE r) {
  // dipoles, 1/r^3
  return 3./(r*r*r*r);
}

/***************************** Force 22 ********************************************/
DOUBLE Force22(DOUBLE r) {
  // dipoles, 1/r^3
  return 3./(r*r*r*r);
}

/***************************** Molecular dynamics Move *****************************/
void MolecularDynamicsMove(int w) {
  int i,j;
  DOUBLE dr[3] = {0.,0.,0.};
  DOUBLE r,r2;
  DOUBLE x,y,z;
  DOUBLE Force;

  m_up = 1.; //7.
  m_up_inv = 1./m_up;

  for(i=0; i<Nup; i++) { // A-A contribution to velocities
    CaseX(x = W[w].x[i]);
    CaseY(y = W[w].y[i]);
    CaseZ(z = W[w].z[i]);
#ifdef TRAP_POTENTIAL // V(r) = (x*x+y*y+z*z)/2
    CaseX(W[w].Vx[i] -= dt*m_up_inv*x);
    CaseY(W[w].Vy[i] -= dt*m_up_inv*y);
    CaseZ(W[w].Vz[i] -= dt*m_up_inv*z);
#endif
    for(j=i+1; j<Nup; j++) {
      CaseX(dr[0] = x - W[w].x[j]);
      CaseY(dr[1] = y - W[w].y[j]);
      CaseZ(dr[2] = z - W[w].z[j]);
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(CheckInteractionConditionOrbital(dr[2], r2)) {
        r = sqrt(r2);
        Force = Force11(r)/r;
        CaseX(W[w].Vx[i] += Force*dt*m_up_inv*dr[0]);
        CaseY(W[w].Vy[i] += Force*dt*m_up_inv*dr[1]);
        CaseZ(W[w].Vz[i] += Force*dt*m_up_inv*dr[2]);
        CaseX(W[w].Vx[j] -= Force*dt*m_up_inv*dr[0]);
        CaseY(W[w].Vy[j] -= Force*dt*m_up_inv*dr[1]);
        CaseZ(W[w].Vz[j] -= Force*dt*m_up_inv*dr[2]);
      }
    }
  }

  for(i=0; i<Ndn; i++) { // B-B contribution to velocities
    CaseX(x = W[w].xdn[i]);
    CaseY(y = W[w].ydn[i]);
    CaseZ(z = W[w].zdn[i]);
#ifdef TRAP_POTENTIAL // V(r) = (x*x+y*y+z*z)/2
    CaseX(W[w].Vxdn[i] -= dt*m_dn_inv*x);
    CaseY(W[w].Vydn[i] -= dt*m_dn_inv*y);
    CaseZ(W[w].Vzdn[i] -= dt*m_dn_inv*z);
#endif
    for(j=i+1; j<Ndn; j++) {
      CaseX(dr[0] = x - W[w].xdn[j]);
      CaseY(dr[1] = y - W[w].ydn[j]);
      CaseZ(dr[2] = z - W[w].zdn[j]);
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(CheckInteractionConditionOrbital(dr[2], r2)) {
        r = sqrt(r2);
        Force = Force22(r)/r;
        CaseX(W[w].Vxdn[i] += Force*dt*m_dn_inv*dr[0]);
        CaseY(W[w].Vydn[i] += Force*dt*m_dn_inv*dr[1]);
        CaseZ(W[w].Vzdn[i] += Force*dt*m_dn_inv*dr[2]);
        CaseX(W[w].Vxdn[j] -= Force*dt*m_dn_inv*dr[0]);
        CaseY(W[w].Vydn[j] -= Force*dt*m_dn_inv*dr[1]);
        CaseZ(W[w].Vzdn[j] -= Force*dt*m_dn_inv*dr[2]);
      }
    }
  }

  for(i=0; i<Nup; i++) { // A-B contribution to velocities
    CaseX(x = W[w].x[i]);
    CaseY(y = W[w].y[i]);
    CaseZ(z = W[w].z[i]);
    for(j=0; j<Ndn; j++) {
      CaseX(dr[0] = x - W[w].xdn[j]);
      CaseY(dr[1] = y - W[w].ydn[j]);
      CaseZ(dr[2] = z - W[w].zdn[j]);
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(CheckInteractionConditionOrbital(dr[2], r2)) {
        r = sqrt(r2);
        Force = Force12(r)/r;
        CaseX(W[w].Vx[i] += Force*dt*m_up_inv*dr[0]);
        CaseY(W[w].Vy[i] += Force*dt*m_up_inv*dr[1]);
        CaseZ(W[w].Vz[i] += Force*dt*m_up_inv*dr[2]);
        CaseX(W[w].Vxdn[j] -= Force*dt*m_dn_inv*dr[0]);
        CaseY(W[w].Vydn[j] -= Force*dt*m_dn_inv*dr[1]);
        CaseZ(W[w].Vzdn[j] -= Force*dt*m_dn_inv*dr[2]);
      }
    }
  }

  for(i=0; i<Nup; i++) {
    CaseX(W[w].x[i] += dt*W[w].Vx[i]);
    CaseY(W[w].y[i] += dt*W[w].Vy[i]);
    CaseZ(W[w].z[i] += dt*W[w].Vz[i]);
    if(measure_SD) {
      CaseX(W[w].rreal[i][0] += dt*W[w].Vx[i]);
      CaseY(W[w].rreal[i][1] += dt*W[w].Vy[i]);
      CaseZ(W[w].rreal[i][2] += dt*W[w].Vz[i]);
    }
    ReduceToTheBoxXYZ(&W[w].x[i], &W[w].y[i], &W[w].z[i]);
  }

  for(i=0; i<Ndn; i++) {
    CaseX(W[w].xdn[i] += dt*W[w].Vxdn[i]);
    CaseY(W[w].ydn[i] += dt*W[w].Vydn[i]);
    CaseZ(W[w].zdn[i] += dt*W[w].Vzdn[i]);
    /*if(measure_SD) {
      CaseX(W[w].rreal[i][3] += dt*W[w].Vxdn[i]);
      CaseY(W[w].rreal[i][4] += dt*W[w].Vydn[i]);
      CaseZ(W[w].rreal[i][5] += dt*W[w].Vzdn[i]);
    }*/
    ReduceToTheBoxXYZ(&W[w].xdn[i], &W[w].ydn[i], &W[w].zdn[i]);
  }
}

/****************************** Walker Kinetic Potential Energy ***********************/
DOUBLE WalkerKineticPotentialEnergy(struct Walker *W, DOUBLE *Ekin, DOUBLE *Eint) {
  int i,j,k;
  DOUBLE dr[3], r, r2;
  DOUBLE Force, dF;
  DOUBLE x,y,z;

  *Eint = 0.;
  *Ekin = 0.;

#ifdef EXTERNAL_POTENTIAL // add external potential energy
#pragma omp parallel for
  for(i=0; i<Nup; i++) *Eint += VextUp(W->x[i], W->y[i], W->z[i], i);
#pragma omp parallel for
  for(i=0; i<Ndn; i++) *Eint += VextDn(W->xdn[i], W->ydn[i], W->zdn[i], i);
#endif

#pragma omp parallel for
  for(i=0; i<Nup; i++) { // AB
    for(j=0; j<Ndn; j++) {
      CaseX(dr[0] = W->x[i] - W->xdn[j]);
      CaseY(dr[1] = W->y[i] - W->ydn[j]);
      CaseZ(dr[2] = W->z[i] - W->zdn[j]);
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(CheckInteractionCondition(dr[2], r2)) {
        r = sqrt(r2);
#pragma omp atomic
        *Eint += InteractionEnergy12(r);
      }
    }
  }

#pragma omp parallel for
  for(i=0; i<Nup; i++) { // AA
    for(j=i+1; j<Nup; j++) {
      CaseX(dr[0] = W->x[i] - W->x[j]); // spin up
      CaseY(dr[1] = W->y[i] - W->y[j]);
      CaseZ(dr[2] = W->z[i] - W->z[j]);
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(CheckInteractionCondition(dr[2], r2)) {
        r = sqrt(r2);
#pragma omp atomic
        *Eint += InteractionEnergy11(r);
      }
    }
  }

#pragma omp parallel for
  for(i=0; i<Ndn; i++) { //BB
    for(j=i+1; j<Ndn; j++) {
      CaseX(dr[0] = W->xdn[i] - W->xdn[j]); // spin down
      CaseY(dr[1] = W->ydn[i] - W->ydn[j]);
      CaseZ(dr[2] = W->zdn[i] - W->zdn[j]);
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(CheckInteractionCondition(dr[2], r2)) {
        r = sqrt(r2);
#pragma omp atomic
        *Eint += InteractionEnergy22(r);
      }
    }
  }

  for(i=0; i<Nup; i++) {
    CaseX(*Ekin += 0.5*m_up*W->Vx[i]*W->Vx[i]);
    CaseY(*Ekin += 0.5*m_up*W->Vy[i]*W->Vy[i]);
    CaseZ(*Ekin += 0.5*m_up*W->Vz[i]*W->Vz[i]);
  }

  for(i=0; i<Ndn; i++) {
    CaseX(*Ekin += 0.5*m_dn*W->Vxdn[i]*W->Vxdn[i]);
    CaseY(*Ekin += 0.5*m_dn*W->Vydn[i]*W->Vydn[i]);
    CaseZ(*Ekin += 0.5*m_dn*W->Vzdn[i]*W->Vzdn[i]);
  }

  return *Eint + *Ekin;
}

/**************************** Adjust velocities ***************************************/
void AdjustVelocitiesToTemperature(void) {
  int w,i;
  DOUBLE correction;
  DOUBLE CMx, CMy, CMz;

  // real: Ekin
  // goal: Ekin = 3/2 kT

  correction = sqrt(1.5*T/Ekin);

  for(w=0; w<Nwalkers; w++) {
    CMx = CMy = CMz = 0.; // find CM velocity
    for(i=0; i<Nup; i++) {
      CaseX(CMx += W[w].Vx[i]);
      CaseY(CMy += W[w].Vy[i]);
      CaseZ(CMz += W[w].Vz[i]);
    }
    CMx /= (DOUBLE) Nup;
    CMy /= (DOUBLE) Nup;
    CMz /= (DOUBLE) Nup;

    for(i=0; i<Nup; i++) {
      CaseX(W[w].Vx[i] -= CMx); // fix CM velocity to zero
      CaseY(W[w].Vy[i] -= CMy);
      CaseZ(W[w].Vz[i] -= CMz);
      CaseX(W[w].Vx[i] *= correction); // adjust temperature
      CaseY(W[w].Vy[i] *= correction);
      CaseZ(W[w].Vz[i] *= correction);
    }

    CMx = CMy = CMz = 0.; // find CM velocity
    for(i=0; i<Ndn; i++) {
      CaseX(CMx += W[w].Vxdn[i]);
      CaseY(CMy += W[w].Vydn[i]);
      CaseZ(CMz += W[w].Vzdn[i]);
    }
    CMx /= (DOUBLE) Ndn;
    CMy /= (DOUBLE) Ndn;
    CMz /= (DOUBLE) Ndn;

    for(i=0; i<Ndn; i++) {
      CaseX(W[w].Vxdn[i] -= CMx); // fix CM velocity to zero
      CaseY(W[w].Vydn[i] -= CMy);
      CaseZ(W[w].Vzdn[i] -= CMz);
      CaseX(W[w].Vxdn[i] *= correction); // adjust temperature
      CaseY(W[w].Vydn[i] *= correction);
      CaseZ(W[w].Vzdn[i] *= correction);
    }
  }
}

/*************************** Generate Velocities***************************************/
void GenerateVelocities(void) {
  int w,i;

  Message("Generating particle velocities ... ");

  for(w=0; w<Nwalkers; w++) {
    for(i=0; i<Nup; i++) RandomNormal3(&W[w].Vx[i], &W[w].Vy[i], &W[w].Vz[i], 0., Sqrt(m_up_inv*T));
    for(i=0; i<Ndn; i++) RandomNormal3(&W[w].Vxdn[i], &W[w].Vydn[i], &W[w].Vzdn[i], 0., Sqrt(m_dn_inv*T));
  }

  E = Ekin = Epot = 0.;
  for(w=0; w<Nwalkers; w++) {
    WalkerKineticPotentialEnergy(&W[w], &W[w].Ekin, &W[w].Epot);
    Epot += W[w].Epot;
    Ekin += W[w].Ekin;
  }
  E = Epot + Ekin;
  E /= (Nup+Ndn)* (DOUBLE) Nwalkers;
  Ekin /= (Nup+Ndn)* (DOUBLE) Nwalkers;
  Epot /= (Nup+Ndn)* (DOUBLE) Nwalkers;

  AdjustVelocitiesToTemperature();

  E = Ekin = Epot = 0.;
  for(w=0; w<Nwalkers; w++) {
    WalkerKineticPotentialEnergy(&W[w], &W[w].Ekin, &W[w].Epot);
    Epot += W[w].Epot;
    Ekin += W[w].Ekin;
  }
  E = Epot + Ekin;
  E /= (Nup+Ndn)* (DOUBLE) Nwalkers;
  Ekin /= (Nup+Ndn)* (DOUBLE) Nwalkers;
  Epot /= (Nup+Ndn)* (DOUBLE) Nwalkers;

  Message("done\n");

  return;
}

/******************* Center of mass not moved ********************************/
void AdjustCenterOfMass(void) {
  int i,w;
  DOUBLE CM[3],M;

  for(w=0; w<Nwalkers; w++) {
    CM[0] = CM[1] = CM[2] = 0.;
    for(i=0; i<Nup; i++) {
      CaseX(CM[0] += m_up*W[w].x[i]);
      CaseY(CM[1] += m_up*W[w].y[i]);
      CaseZ(CM[2] += m_up*W[w].z[i]);
    }
    for(i=0; i<Ndn; i++) {
      CaseX(CM[0] += m_dn*W[w].xdn[i]);
      CaseY(CM[1] += m_dn*W[w].ydn[i]);
      CaseZ(CM[2] += m_dn*W[w].zdn[i]);
    }
    M = m_up*Nup + m_dn*Ndn;
#ifdef BC_ABSENT
    CM[0] = CM[0]/M;
    CM[1] = CM[1]/M;
    CM[2] = CM[2]/M;
#else
    CM[0] = CM[0]/M - Lhalf;
    CM[1] = CM[1]/M - Lhalf;
    CM[2] = CM[2]/M - Lhalf;
#endif

    for(i=0; i<Nup; i++) {
      CaseX(W[w].x[i] -= CM[0]);
      CaseY(W[w].y[i] -= CM[1]);
      CaseZ(W[w].z[i] -= CM[2]);
    }
    for(i=0; i<Ndn; i++) {
      CaseX(W[w].xdn[i] -= CM[0]);
      CaseY(W[w].ydn[i] -= CM[1]);
      CaseZ(W[w].zdn[i] -= CM[2]);
    }

    ReduceWalkerToTheBox(&W[w]);
  }
}

/************************ Adjust Center Of Mass Walker ***********************************/
void AdjustCenterOfMassWalker(struct Walker *Walker) {
  int i;
  DOUBLE CM[3],M;

  CM[0] = CM[1] = CM[2] = 0.;
  for(i=0; i<Nup; i++) {
    CaseX(CM[0] += m_up*Walker->x[i]);
    CaseY(CM[1] += m_up*Walker->y[i]);
    CaseZ(CM[2] += m_up*Walker->z[i]);
  }
  for(i=0; i<Ndn; i++) {
    CaseX(CM[0] += m_dn*Walker->xdn[i]);
    CaseY(CM[1] += m_dn*Walker->ydn[i]);
    CaseZ(CM[2] += m_dn*Walker->zdn[i]);
   }
  M = m_up*Nup + m_dn*Ndn;
#ifdef BC_ABSENT
  CM[0] = CM[0]/M;
  CM[1] = CM[1]/M;
  CM[2] = CM[2]/M;
#else
  CM[0] = CM[0]/M - Lhalf;
  CM[1] = CM[1]/M - Lhalf;
  CM[2] = CM[2]/M - Lhalf;
#endif

  for(i=0; i<Nup; i++) {
    CaseX(Walker->x[i] -= CM[0]);
    CaseY(Walker->y[i] -= CM[1]);
    CaseZ(Walker->z[i] -= CM[2]);
  }
  for(i=0; i<Ndn; i++) {
    CaseX(Walker->xdn[i] -= CM[0]);
    CaseY(Walker->ydn[i] -= CM[1]);
    CaseZ(Walker->zdn[i] -= CM[2]);
  }
   //ReduceWalkerToTheBox(&W[w]);
}

void AdjustUpParticlesPinnedToAChain(void) {
  int i,w;

  for(w=0; w<Nwalkers; w++) {
    for(i=0; i<Nup; i++) {
      W[w].x[i] = 0.;
      W[w].y[i] = 0.;
      W[w].z[i] = Lz / (DOUBLE)Nup*((DOUBLE)i + 0.5);
    }
  }
}

void AdjustUpParticlesPinnedToAChainWalker(struct Walker *Walker) {
  int i;

  for(i=0; i<Nup; i++) {
    Walker->x[i] = 0.;
    Walker->y[i] = 0.;
    Walker->z[i] = Lz / (DOUBLE)Nup*((DOUBLE)i + 0.5);
  }
}
