/*parallel.c*/
#include "main.h"
#include "utils.h"

#ifdef MPI

#include <stdio.h>
#include <mpi.h>
#include "parallel.h"
#include "utils.h"
#include "memory.h"
#include "move.h"
#include "randnorm.h"

int MPI_myid;    // process identification
int MPI_Nslaves; // number of jobs
MPI_Status mpi_status; // error status
char *buffer; // buffer for collecting global data
char *buffer_local; // buffer for local data
int *buffer_int;
double *buffer_double;
int *buffer_displacement; // needed for SCATTER
int *buffer_elements; // needed for SCATTER
int buffer_size; // size of all packed walker in memory

double time_parallel_total = 0.;    // time of the calculation
double time_parallel_transfer = 0.; // time lost for transferring data
double time_parallel_transfer_pack = 0.; // lost due to packing / unpacking
double time_parallel_transfer_send = 0.;  //due to collect / gather
double time_done_in_parallel = 0;   // time of distributed calculation

int NwalkersAll; // total number of walkers, now Nwalkers corresponds to a given machine
int *walker_indices, *NwalkersSl; // index of the first element in the master array and number of walkers on each of the processors

// storing measurements
double *bufferE, *bufferEFF, *bufferErelease, *bufferEext;
int bufferEN;

/******************************* Parallel Initialize *************************/
// MPI initialization, number of particles is not yet known
void ParallelInitializeMPI(int argn, char **argv) {
  int namelen;
  char processor_name[MPI_MAX_PROCESSOR_NAME];

  Message("\nStarting MPI parallelization ...\n");

  MPI_Init(&argn, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &MPI_Nslaves);
  MPI_Comm_rank(MPI_COMM_WORLD, &MPI_myid);

  Message("parameters are %s\n", argv[0]);
  Message("total number of processors is %i\n", MPI_Nslaves);

  time_parallel_total = MPI_Wtime();

  Message("  time count started.\n");

  MPI_Get_processor_name(processor_name, &namelen);
  Message("  Machine name %s, MPI process id number %i\n", processor_name, MPI_myid);

  Message("done\n");
}

/******************************* Parallel Initialize 2 ***********************/
// now number of particles is known, alocate buffers
void ParallelInitialize2(void) {
  int i, w;

  // Allocate arrays
  Message("Parallel Initialize2: Allocating memory for the buffer\n");
  walker_indices = (int*) Calloc("walker_indices", MPI_Nslaves, sizeof(int));
  NwalkersSl = (int*) Calloc("NwalkersSl", MPI_Nslaves, sizeof(int));
  NwalkersAll = Nwalkers;

  //Allocate pack buffer 
  //  Check number of passed elements!
#ifdef ONE_COMPONENT_CODE
  buffer_size = NwalkersMax*(3*N*sizeof(double)+3*sizeof(int)); // walkers coord.
#else
  buffer_size = NwalkersMax*(3*(Nup+Ndn)*sizeof(double)+3*sizeof(int)); // walkers coord.
#endif

  buffer_size += Niter*2; //E
#ifdef CRYSTAL
  buffer_size += Niter*2; //E
#endif
  if(measure_OBDM || measure_Nk) buffer_size += NwalkersMax*2*OBDM.size*sizeof(double);
  if(measure_PairDistr) buffer_size += NwalkersMax*(1+3*PD.size)*sizeof(int);
  if(measure_Nk) buffer_size += NwalkersMax*(1+Nk.size*Nk.size)*sizeof(double);
  buffer_size += 10; // additional 10 spaces are added

  Message("  MPI: buffer size is %i\n", buffer_size);
  buffer = (char*) Calloc("buffer", buffer_size, sizeof(char)); // buffer used for SCATTER and GATHER

  buffer_local = (char*) Calloc("buffer_local", buffer_size/MPI_Nslaves+10, sizeof(char));  // bufer used localy on each node
  buffer_int = (int*) Calloc("buffer_int", buffer_size/MPI_Nslaves+10, sizeof(int));
  buffer_double = (double*) Calloc("buffer_int", buffer_size/MPI_Nslaves+10, sizeof(double));
  buffer_displacement = (int*) Calloc("buffer_displacement", MPI_Nslaves, sizeof(int)); // needed for SCATTER
  buffer_elements = (int*) Calloc("buffer_elements", MPI_Nslaves, sizeof(int));     // needed for SCATTER

  // allocate arrays for storing energy
  bufferE =   (double*) Calloc("bufferE",   Niter, sizeof(double));
  bufferEFF = (double*) Calloc("bufferEFF", Niter, sizeof(double));
#ifdef CRYSTAL
  bufferErelease = (double*) Calloc("bufferErelease", Niter, sizeof(double));
  bufferEext = (double*) Calloc("bufferEext", Niter, sizeof(double));
#endif
  bufferEN = 0;

  // Check the network
  Message("\nChecking that network spreading works correctly ... \n");
  if(MPI_myid == MASTER) for(w=0; w<NwalkersAll; w++) CopyWalker(&Wp[w], W[w]);//w
  Message("\ncopying walkers done ... \n");

  DistributeAllWalkers();

  Message("\ndistribuiting walkers done ... \n");

  if(MPI_myid == MASTER) {
    for(w=Nwalkers; w<NwalkersAll; w++) {
#ifdef ONE_COMPONENT_CODE
      for(i=0; i<N; i++) W[w].x[i] = W[w].y[i] = W[w].z[i] = 0.;
#else
      for(i=0; i<Nup; i++) W[w].x[i] = W[w].y[i] = W[w].z[i] = 0.;
      for(i=0; i<Ndn; i++) W[w].xdn[i] = W[w].ydn[i] = W[w].zdn[i] = 0.;
#endif
      W[w].E = W[w].U = 0.;
      //W[w].E = W[w].Determinant = W[w].U = 0.;
    }
  }

  CollectAllWalkers();

  if(MPI_myid == MASTER) {
    for(w=0; w<NwalkersAll; w++) {
      if(WalkerCompare(Wp[w], W[w])) {
        Message("Walker compare failed for walker W[%i] !\n", w);
        //Message("Local copy up(%f,%f,%f) dn(%f,%f,%f)\n", Wp[w].x[0], Wp[w].y[0], Wp[w].z[0], Wp[w].xdn[0], Wp[w].xdn[0], Wp[w].xdn[0]);
        //Message("Recieved   up(%f,%f,%f) dn(%f,%f,%f)\n", W[w].x[0], W[w].y[0], W[w].z[0], W[w].xdn[0], W[w].xdn[0], W[w].xdn[0]);
        Error("exiting");
      }
    }
  }

  Message("done\n\n");
}

/******************************* Parallel stop *******************************/
void ParallelStop(void) {
  double effective_single;

  MPI_Finalize();

  time_done_in_parallel += MPI_Wtime();
  time_parallel_total =  MPI_Wtime() - time_parallel_total;

  Message("\nMPI time statistics, time:\n");
  Message("    total:   : %.1f sec (100%%)\n", time_parallel_total);
  Message("    serial   : %.1f sec (%2.lf%%)\n", time_parallel_total-time_done_in_parallel, (time_parallel_total-time_done_in_parallel)/time_parallel_total*100);
  Message("    parallel : %.1f sec (%2.lf%%), effective %.1f sec of single CPU\n", time_done_in_parallel, time_done_in_parallel/time_parallel_total*100, time_done_in_parallel*MPI_Nslaves);
  Message("    transfer : %.1f sec (%2.lf%%)\n", time_parallel_transfer, time_parallel_transfer/time_parallel_total*100);
  Message("    transfer (pack) : %.1f sec (%2.lf%%)\n", time_parallel_transfer_pack, time_parallel_transfer_pack/time_parallel_total*100);
  Message("    transfer (send) : %.1f sec (%2.lf%%)\n", time_parallel_transfer_send, time_parallel_transfer_send/time_parallel_total*100);
  effective_single = time_parallel_total - time_parallel_transfer + (MPI_Nslaves-1)*time_done_in_parallel;
  Message("    speed gain : %.1f times\n", effective_single/time_parallel_total);
}
                                                      
/**************************** Distribute All Walkers ****************************/
void DistributeAllWalkers(void) {
  int w,i;

  if(MPI_myid == MASTER) { // the master code
    if(verbosity) Message("Distribution of %i walkers to %i nodes\n", Nwalkers, MPI_Nslaves);
    NwalkersAll = Nwalkers;
    //NwalkersSl[0] = ceiling((double)NwalkersAll / (double) MPI_Nslaves); // distribute walkers in a homogeneous way
    NwalkersSl[0] = floor((double)NwalkersAll / (double) MPI_Nslaves); // distribute walkers in a homogeneous way
    walker_indices[0] = 0; // initialize

    for(i=1; i<MPI_Nslaves; i++) {
      walker_indices[i] = walker_indices[i-1] + NwalkersSl[i-1]; // first index of node i
      if(i<MPI_Nslaves-1)
        //NwalkersSl[i] = ceiling((double)NwalkersAll / (double) MPI_Nslaves); // number of walkers on node i
        NwalkersSl[i] = floor((double)NwalkersAll / (double) MPI_Nslaves); // number of walkers on node i
      else
        NwalkersSl[i] = NwalkersAll - walker_indices[i]; // last node takes the rest
    }
    walker_indices[MPI_Nslaves] = NwalkersAll;
    
  }

  DistributeWalkersSend();
}

/**************************** Distribute Walkers Send ***************************/
void DistributeWalkersSend(void) {
// Send / Recieve functions are called
  static int tag = 1;
  int w,i;

  tag++;
  if(MPI_myid == MASTER) { // the master code
    if(verbosity) {
      Message("\nSending configuration coordinates to slaves...\n");
      Message("  => MASTER has %i walkers :", NwalkersSl[0]);
      Message("  [%i,%i]\n", walker_indices[0] + 1, walker_indices[0]+NwalkersSl[0]);
    }
    for(i=1; i<MPI_Nslaves; i++) {
      if(verbosity) Message("  => SL %i sending %i walkers :  [%i,%i]\n", i, NwalkersSl[i], walker_indices[i]+1, walker_indices[i]+NwalkersSl[i]);
      SendWalkers(i, walker_indices[i], NwalkersSl[i], tag);
     }
    Nwalkers = NwalkersSl[0];
    if(verbosity) Message("done\n");
  }
  else { // the slave code
    if(verbosity) Message("Receiving walkers ...\n");

    ReceiveWalkers(MASTER, 0, tag);

    for(w=0; w<Nwalkers; w++) W[w].w = w; // update local index

    if(verbosity) Message("%i walkers recieved. Done\n", Nwalkers);
  }
} 

/**************************** Distribute Walkers Send ***************************/
void DistributeWalkersScatter(void) {
// Send / Recieve functions are called
  static int tag = 1;
  int w,i;

  tag++;
  if(MPI_myid == MASTER) { // the master code
    if(verbosity) {
      Message("\nSending configuration coordinates to slaves (scatter) ...\n");
      Message("  => MASTER has %i walkers :", NwalkersSl[0]);
      Message("  [%i,%i]\n", walker_indices[0] + 1, walker_indices[0]+NwalkersSl[0]);
    }
    for(i=1; i<MPI_Nslaves; i++) {
      if(verbosity) Message("  => SL %i sending %i walkers :  [%i,%i]\n", i, NwalkersSl[i], walker_indices[i]+1, walker_indices[i]+NwalkersSl[i]);
      SendWalkers(i, walker_indices[i], NwalkersSl[i], tag);
     }
    Nwalkers = NwalkersSl[0];
    if(verbosity) Message("done\n");
  }
  else { // the slave code
    if(verbosity) Message("Receiving walkers ...\n");

    ReceiveWalkers(MASTER, 0, tag);

    for(w=0; w<Nwalkers; w++) W[w].w = w; // update local index

    if(verbosity) Message("%i walkers recieved. Done\n", Nwalkers);
  }
}

/**************************** Collect All Walkers ****************************/
void CollectAllWalkers(void) {
  int i;
  static int tag=1000;

  tag++;
  if(verbosity) Message("Collect all walkers: packing local %i walkers ... ", Nwalkers);

  if(MPI_myid == MASTER) {
    for(i=1; i<MPI_Nslaves; i++) {
      ReceiveWalkers(i, walker_indices[i], tag);
    }
    Nwalkers = NwalkersAll;
  }
  else {
    SendWalkers(MASTER, 0, Nwalkers, tag);
  }
}

/**************************** Collect All Walkers Data ***********************/
void CollectAllWalkersData(void) {
  int i;
  static int tag = 2000;

  time_parallel_transfer -= MPI_Wtime(); // store moment when the function was called

  if(MPI_myid == MASTER) {
    for(i=1; i<MPI_Nslaves; i++) {
      ReceiveWalkersData(i, walker_indices[i], tag);
    }
    Nwalkers = NwalkersAll;
  }
  else {
    SendWalkersData(MASTER, 0, Nwalkers, tag);
  }
  time_parallel_transfer += MPI_Wtime(); //update transfer time
}

/***************************** Send Walkers ***********************************/
// send walkers W[wmin],...,W[wmin+Nwalk] to machine No dest
int SendWalkers(int dest, int wmin, int Nwalk, int tag) {
  int w;
  int buffer_position = 0;

  time_parallel_transfer -= MPI_Wtime();
  time_parallel_transfer_pack -= MPI_Wtime();
  MPI_Pack(&Nwalk, 1, MPI_INT, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
  for(w=wmin; w<wmin+Nwalk; w++) {
#ifdef ONE_COMPONENT_CODE
    CaseX(MPI_Pack(W[w].x, N, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD));
    CaseY(MPI_Pack(W[w].y, N, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD));
    CaseZ(MPI_Pack(W[w].z, N, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD));
#else
    CaseX(MPI_Pack(W[w].x, Nup, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD));
    CaseY(MPI_Pack(W[w].y, Nup, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD));
    CaseZ(MPI_Pack(W[w].z, Nup, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD));
    CaseX(MPI_Pack(W[w].xdn, Ndn, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD));
    CaseY(MPI_Pack(W[w].ydn, Ndn, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD));
    CaseZ(MPI_Pack(W[w].zdn, Ndn, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD));
#endif
    MPI_Pack(&W[w].U, 1, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
    //MPI_Pack(&W[w].Determinant, 1, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
    MPI_Pack(&W[w].E, 1, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
  }
  time_parallel_transfer_pack += MPI_Wtime();

  time_parallel_transfer_send -= MPI_Wtime();

  MPI_Send(buffer, buffer_position, MPI_PACKED, dest, tag, MPI_COMM_WORLD);

  time_parallel_transfer_send += MPI_Wtime();
  time_parallel_transfer += MPI_Wtime();

  return buffer_position;
}

/**************************** Receive Walkers *********************************/
void ReceiveWalkers(int src, int wmin, int tag) {
  int w;
  int buffer_position = 0;

  if(verbosity) Message("recieving walkers...");
  time_parallel_transfer -= MPI_Wtime();
  time_parallel_transfer_send -= MPI_Wtime(); // store moment when the function was called

  MPI_Recv(buffer, buffer_size, MPI_PACKED, src, tag, MPI_COMM_WORLD, &mpi_status);

  time_parallel_transfer_send += MPI_Wtime();

  time_parallel_transfer_pack -= MPI_Wtime();
  MPI_Unpack(buffer, buffer_size, &buffer_position, &Nwalkers, 1, MPI_INT, MPI_COMM_WORLD);
  if(verbosity) Message("recieving %i walkers...", Nwalkers);
  for(w=wmin; w<wmin+Nwalkers; w++) {
#ifdef ONE_COMPONENT_CODE
    CaseX(MPI_Unpack(buffer, buffer_size, &buffer_position, W[w].x, N, MPI_DOUBLE, MPI_COMM_WORLD));
    CaseY(MPI_Unpack(buffer, buffer_size, &buffer_position, W[w].y, N, MPI_DOUBLE, MPI_COMM_WORLD));
    CaseZ(MPI_Unpack(buffer, buffer_size, &buffer_position, W[w].z, N, MPI_DOUBLE, MPI_COMM_WORLD));
#else
    CaseX(MPI_Unpack(buffer, buffer_size, &buffer_position, W[w].x, Nup, MPI_DOUBLE, MPI_COMM_WORLD));
    CaseY(MPI_Unpack(buffer, buffer_size, &buffer_position, W[w].y, Nup, MPI_DOUBLE, MPI_COMM_WORLD));
    CaseZ(MPI_Unpack(buffer, buffer_size, &buffer_position, W[w].z, Nup, MPI_DOUBLE, MPI_COMM_WORLD));
    CaseX(MPI_Unpack(buffer, buffer_size, &buffer_position, W[w].xdn, Ndn, MPI_DOUBLE, MPI_COMM_WORLD));
    CaseY(MPI_Unpack(buffer, buffer_size, &buffer_position, W[w].ydn, Ndn, MPI_DOUBLE, MPI_COMM_WORLD));
    CaseZ(MPI_Unpack(buffer, buffer_size, &buffer_position, W[w].zdn, Ndn, MPI_DOUBLE, MPI_COMM_WORLD));
#endif
    MPI_Unpack(buffer, buffer_size, &buffer_position, &W[w].U, 1, MPI_DOUBLE, MPI_COMM_WORLD);
    //MPI_Unpack(buffer, buffer_size, &buffer_position, &W[w].Determinant, 1, MPI_DOUBLE, MPI_COMM_WORLD);
    MPI_Unpack(buffer, buffer_size, &buffer_position, &W[w].E, 1, MPI_DOUBLE, MPI_COMM_WORLD);
    W[w].w = w; // update local index
  }
  time_parallel_transfer_pack += MPI_Wtime();
  time_parallel_transfer += MPI_Wtime();
  if(verbosity) Message("done\n");
}

/***************************** Send Walkers Data ******************************/
// send walkers W[wmin],...,W[wmin+Nwalk] and arrays to machine No dest
int SendWalkersData(int dest, int wmin, int Nwalk, int tag) {
  int w;
  int buffer_position = 0;
  int i;

  Message("Send %i\n", tag);
  if(verbosity) Message("Send walker data..");
  time_parallel_transfer -= MPI_Wtime();
  time_parallel_transfer_pack -= MPI_Wtime();
  MPI_Pack(&Nwalk, 1, MPI_INT, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
  for(w=wmin; w<wmin+Nwalk; w++) {
#ifdef ONE_COMPONENT_CODE
    MPI_Pack(W[w].x, N, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
    MPI_Pack(W[w].y, N, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
    MPI_Pack(W[w].z, N, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
#else
    MPI_Pack(W[w].x, Nup, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
    MPI_Pack(W[w].y, Nup, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
    MPI_Pack(W[w].z, Nup, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
    MPI_Pack(W[w].xdn, Ndn, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
    MPI_Pack(W[w].ydn, Ndn, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
    MPI_Pack(W[w].zdn, Ndn, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
#endif
    MPI_Pack(&W[w].U, 1, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
    //MPI_Pack(&W[w].Determinant, 1, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
    MPI_Pack(&W[w].E, 1, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
  }

  if(measure_energy) {
    MPI_Pack(bufferE,   bufferEN, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
    MPI_Pack(bufferEFF, bufferEN, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
#ifdef CRYSTAL
    MPI_Pack(bufferErelease, bufferEN, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
    MPI_Pack(bufferEext, bufferEN, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
#endif
  }
  // empty energy arrays
  bufferEN = 0;

  if(measure_OBDM || measure_Nk) { // SaveOBDM()
    MPI_Pack(OBDM.f, OBDM.size, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
    MPI_Pack(OBDM.N, OBDM.size, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
  }
  if(measure_PairDistr) { //SavePairDistribution()
    MPI_Pack(&PD.times_measured, 1, MPI_INT, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
    MPI_Pack(PD.N, PD.size, MPI_INT, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
    MPI_Pack(PDup.N, PD.size, MPI_INT, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
    MPI_Pack(PDdn.N, PD.size, MPI_INT, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
  }
  if(measure_Nk) { //SaveMomentumDistribution()
    MPI_Pack(&Nk.times_measured, 1, MPI_INT, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
    for(i=0; i<Nk.size; i++) MPI_Pack(Nk.f[i], Nk.size, MPI_DOUBLE, buffer, buffer_size, &buffer_position, MPI_COMM_WORLD);
  }

  /*if(measure_TBDM) SaveTBDM();
  if(measure_TBDM_MATRIX) SaveTBDMMatrix();
  if(measure_OBDM_MATRIX) SaveOBDMMatrix();
  if(measure_RadDistr && boundary != ONE_BOUNDARY_CONDITION) SaveRadialDistribution();
  if(measure_Sk) {
    SaveStaticStructureFactor();
    SaveStaticStructureFactorAngularPart();
  }
  if(measure_OP) SaveOrderParameter();
  if(measure_Lind) SaveLindemannRatio();
  if(var_par_array) SaveVarParWeights();*/
  time_parallel_transfer_pack += MPI_Wtime();

  time_parallel_transfer_send -= MPI_Wtime();
  MPI_Send(buffer, buffer_position, MPI_PACKED, dest, tag, MPI_COMM_WORLD);
  time_parallel_transfer_send += MPI_Wtime();
  time_parallel_transfer += MPI_Wtime();
  if(verbosity) Message(".done\n");

  return buffer_position;
}

/**************************** Receive Walkers Data ****************************/
void ReceiveWalkersData(int src, int wmin, int tag) {
  int w;
  int buffer_position = 0;
  int i,j;

  if(verbosity) Message("recieving walkers data, wmin= %i ...", wmin);
  time_parallel_transfer -= MPI_Wtime(); // store moment when the function was called
  time_parallel_transfer_send -= MPI_Wtime();
  MPI_Recv(buffer, buffer_size, MPI_PACKED, src, tag, MPI_COMM_WORLD, &mpi_status);
  time_parallel_transfer_send += MPI_Wtime();

  time_parallel_transfer_pack -= MPI_Wtime();
  MPI_Unpack(buffer, buffer_size, &buffer_position, &Nwalkers, 1, MPI_INT, MPI_COMM_WORLD);
  for(w=wmin; w<wmin+Nwalkers; w++) {
#ifdef ONE_COMPONENT_CODE
    MPI_Unpack(buffer, buffer_size, &buffer_position, W[w].x, N, MPI_DOUBLE, MPI_COMM_WORLD);
    MPI_Unpack(buffer, buffer_size, &buffer_position, W[w].y, N, MPI_DOUBLE, MPI_COMM_WORLD);
    MPI_Unpack(buffer, buffer_size, &buffer_position, W[w].z, N, MPI_DOUBLE, MPI_COMM_WORLD);
#else
    MPI_Unpack(buffer, buffer_size, &buffer_position, W[w].x, Nup, MPI_DOUBLE, MPI_COMM_WORLD);
    MPI_Unpack(buffer, buffer_size, &buffer_position, W[w].y, Nup, MPI_DOUBLE, MPI_COMM_WORLD);
    MPI_Unpack(buffer, buffer_size, &buffer_position, W[w].z, Nup, MPI_DOUBLE, MPI_COMM_WORLD);
    MPI_Unpack(buffer, buffer_size, &buffer_position, W[w].xdn, Ndn, MPI_DOUBLE, MPI_COMM_WORLD);
    MPI_Unpack(buffer, buffer_size, &buffer_position, W[w].ydn, Ndn, MPI_DOUBLE, MPI_COMM_WORLD);
    MPI_Unpack(buffer, buffer_size, &buffer_position, W[w].zdn, Ndn, MPI_DOUBLE, MPI_COMM_WORLD);
#endif
    MPI_Unpack(buffer, buffer_size, &buffer_position, &W[w].U, 1, MPI_DOUBLE, MPI_COMM_WORLD);
    //MPI_Unpack(buffer, buffer_size, &buffer_position, &W[w].Determinant, 1, MPI_DOUBLE, MPI_COMM_WORLD);
    MPI_Unpack(buffer, buffer_size, &buffer_position, &W[w].E, 1, MPI_DOUBLE, MPI_COMM_WORLD);
    W[w].w = w; // update local index
  }

  if(measure_energy) {
  MPI_Unpack(buffer, buffer_size, &buffer_position, buffer_double, bufferEN, MPI_DOUBLE, MPI_COMM_WORLD);
  for(i=0; i<bufferEN; i++) bufferE[i] += buffer_double[i];
    MPI_Unpack(buffer, buffer_size, &buffer_position, buffer_double, bufferEN, MPI_DOUBLE, MPI_COMM_WORLD);
    for(i=0; i<bufferEN; i++) bufferEFF[i] += buffer_double[i];
#ifdef CRYSTAL
    MPI_Unpack(buffer, buffer_size, &buffer_position, buffer_double, bufferEN, MPI_DOUBLE, MPI_COMM_WORLD);
    for(i=0; i<bufferEN; i++) bufferErelease[i] += buffer_double[i];
    MPI_Unpack(buffer, buffer_size, &buffer_position, buffer_double, bufferEN, MPI_DOUBLE, MPI_COMM_WORLD);
    for(i=0; i<bufferEN; i++) bufferEext[i] += buffer_double[i];
#endif
  }

  if(measure_OBDM || measure_Nk) { // SaveOBDM()
    MPI_Unpack(buffer, buffer_size, &buffer_position, buffer_double, OBDM.size, MPI_DOUBLE, MPI_COMM_WORLD);
    for(i=0; i<OBDM.size; i++) OBDM.f[i] += buffer_double[i];
    MPI_Unpack(buffer, buffer_size, &buffer_position, buffer_double, OBDM.size, MPI_DOUBLE, MPI_COMM_WORLD);
    for(i=0; i<OBDM.size; i++) OBDM.N[i] += buffer_double[i];
  }

  if(measure_PairDistr) { //SavePairDistribution();
    MPI_Unpack(buffer, buffer_size, &buffer_position, buffer_int, 1, MPI_INT, MPI_COMM_WORLD);
    PD.times_measured += buffer_int[0];
    MPI_Unpack(buffer, buffer_size, &buffer_position, buffer_int, PD.size, MPI_INT, MPI_COMM_WORLD);
    for(i=0; i<PD.size; i++) PD.N[i] += buffer_int[i];
    MPI_Unpack(buffer, buffer_size, &buffer_position, buffer_int, PD.size, MPI_INT, MPI_COMM_WORLD);
    for(i=0; i<PD.size; i++) PDup.N[i] += buffer_int[i];
    MPI_Unpack(buffer, buffer_size, &buffer_position, buffer_int, PD.size, MPI_INT, MPI_COMM_WORLD);
    for(i=0; i<PD.size; i++) PDdn.N[i] += buffer_int[i];
  }

  if(measure_Nk) {
    MPI_Unpack(buffer, buffer_size, &buffer_position, buffer_int, 1, MPI_INT, MPI_COMM_WORLD);
    Nk.times_measured += buffer_int[0];
    for(i=0; i<Nk.size; i++) {
      MPI_Unpack(buffer, buffer_size, &buffer_position, buffer_double, Nk.size, MPI_DOUBLE, MPI_COMM_WORLD);
      for(j=0; j<Nk.size; j++) Nk.f[i][j] += buffer_double[j];
    }
  }
  time_parallel_transfer_pack += MPI_Wtime();
  time_parallel_transfer += MPI_Wtime();
  if(verbosity) Message("done\n");
}

/******************************** Save Energy VMC ************************/
void ParallelSaveEnergyVMC(void) {
#ifdef CRYSTAL
  //fprintf(out, "%.15"LE" %.15"LE" %.15"LE" %.15"LE"\n", energy_unit*(E+energy_shift), energy_unit*(EFF+energy_shift),energy_unit*(Epot+Ekin+energy_shift), energy_unit*Eext);
  bufferErelease[bufferEN] = (Epot+Ekin+energy_shift);
  bufferEext[bufferEN] = energy_unit*Eext;
#endif
  //fprintf(out, "%.15"LE" %.15"LE"\n", (E+energy_shift)*energy_unit, (EFF+energy_shift)*energy_unit);
  bufferE[bufferEN]   = (E+energy_shift)*energy_unit;
  bufferEFF[bufferEN] = (EFF+energy_shift)*energy_unit;
  bufferEN++;

  return;
}

/*********************************** Save Data *******************************/
void ParallelSaveBlockDataStart(void) {
  CollectAllWalkersData();
}

void ParallelSaveBlockDataEnd(void) {
  static int file_open = OFF;
  FILE *out;
  int i;

  // save Energy
  if(MPI_myid == MASTER) {
    out = fopen(file_energy, file_open?"a":"w");
    if(file_open == OFF) file_open = ON; 
    if(out == NULL) {
      perror("\nError:");
      Warning("can't write to energy file %s\n", file_energy);
       return;
    }
    for(i=0; i<bufferEN; i++) {
#ifdef CRYSTAL
      // E EtotFF Erelease Eexttice
      fprintf(out, "%.15"LE" %.15"LE" %.15"LE" %.15"LE"\n", bufferE[i]/MPI_Nslaves, bufferEFF[i]/MPI_Nslaves, bufferErelease[i]/MPI_Nslaves, bufferEext[i]/MPI_Nslaves);
#else
      fprintf(out, "%.15"LE" %.15"LE"\n", bufferE[i]/MPI_Nslaves, bufferEFF[i]/MPI_Nslaves);
#endif
    }
    fclose(out);
  }
  bufferEN = 0;

  if(MPI_myid == MASTER) Nwalkers = NwalkersSl[0];
}
#endif

/********************************** Open MP ***********************************/
#ifdef _OPENMP

#include <stdio.h>
#include <math.h>
#include "omp.h"
#include "parallel.h"
#include "utils.h"
#include "memory.h"
#include "vmc.h"
#include "rw.h"
#include "randnorm.h"

int OPENMP_myid; // process identification
int OPENMP_Nslaves; // number of jobs

void ParallelInitializeOpenMP(int argn, char **argv) {

  Message("\nStarting OpenMP parallelization ...\n");
#pragma omp parallel
  {
    OPENMP_Nslaves = omp_get_num_threads();
  }
  if(OPENMP_Nslaves>OPENMP_MAX_NPROC) {
    Error(" Cannot use all nodes: available %i, used %i\n  Change OPENMP_MAX_NPROC and recompile\n", OPENMP_Nslaves, OPENMP_MAX_NPROC);
    //omp_set_num_threads(OPENMP_MAX_NPROC);
  }

#pragma omp parallel
  {
    OPENMP_myid = omp_get_thread_num();  // Obtain thread number
    Message("  Hello World from OpenMP thread = %d\n", OPENMP_myid);

    if(OPENMP_myid == 0) { // Only master thread does this
      Message("  Hello from OpenMP Master\n");
      OPENMP_Nslaves = omp_get_num_threads();
    }
  } // All threads join master thread and disband

  Message("  total number of processors is %i\n", OPENMP_Nslaves);
  Message("done\n\n");
}

/**************************** Open MP: Random Critical ************************/
DOUBLE RandomCritical(void) {
  DOUBLE xi;
#pragma omp critical
  {
  xi = ran2(&rand_seed);
  }
  return xi;
}
#endif

/**************************** ParallelRandInit ********************************/
void ParallelRandInit(void) {
#ifdef PARALLEL_SEED

  int total_number_of_threads;
  int local_thread_number;

#ifdef _OPENMP
  int OPENMP_myid;
#endif

#ifndef USE_RAN2_GENERATOR
  Error("define USE_RAN2_GENERATOR");
#endif

#ifdef _OPENMP
#pragma omp parallel
{
  OPENMP_myid = omp_get_thread_num();
}
#endif

#ifdef MPI_ONLY
   total_number_of_threads = MPI_Nslaves;
   local_thread_number = MPI_myid;
#endif
#ifdef OPENP_ONLY
   total_number_of_threads = OPENMP_myid;
   local_thread_number = omp_get_thread_num();
#endif
#ifdef MPI_WITH_OPENMP
   total_number_of_threads = OPENMP_myid*MPI_Nslaves;
   local_thread_number = MPI_myid*MPI_Nslaves + omp_get_thread_num();
#endif

  if(total_number_of_threads > OPENMP_MAX_NPROC) Error("  Not enough memory reserved for paralallel random number, increase OPENMP_MAX_NPROC to %i", total_number_of_threads);
  if(total_number_of_threads > rand_seed_parallel_length) Error("  Not enough seeds for parallel random number generator, %i > %i, recompile\n", total_number_of_threads, rand_seed_parallel_length);
  Message(" node %i, Ran2() random number generator started with seed %i\n", local_thread_number, rand_seed_parallel[local_thread_number]);

  if(RandomSys()<0) Error("bad implementation of RandomSys(). It should not produce negative numbers");
  if(RandomSys()>1) Error("bad implementation of RandomSys(). It should not produce numbers larger than 1");
#endif
}
