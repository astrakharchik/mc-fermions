/*move.h*/

#ifndef _FERMIONS_H
#define _FERMIONS_H

#include "main.h"
#include "compatab.h"
#include <math.h>

#ifdef USE_LAPACK
#include "mkl_lapack.h"
int CheckLapack(void);
int LUinvLapack(DOUBLE **A, DOUBLE **Ainv, int n);
#endif

//extern DOUBLE **LU;
extern DOUBLE **AI;
extern DOUBLE *scales;
extern DOUBLE *invb;
extern int *ps, *to_process;

extern DOUBLE ***Determinant, ***DeterminantInv;
extern DOUBLE ***DeterminantT, ***DeterminantTInv;
extern DOUBLE **DeterminantMove, **DeterminantMoveInv;
extern DOUBLE **DeterminantMoveBackUp, **DeterminantMoveInvBackUp;
extern DOUBLE determinantLU;

void AllocateFermionMatrices(void);

int  LUdecompose(DOUBLE **a, int n);
void LUsolve(DOUBLE *x, DOUBLE *b, int n);
int  LUinv(DOUBLE **A, DOUBLE **Ainv, int n);
int  LUdecomposeSparse(DOUBLE **a, int n);
void LUsolveSparse(DOUBLE *x, DOUBLE *b, int n);
int  LUinvSparse(DOUBLE **A, DOUBLE **Ainv, int n);

void CopyMatrix(DOUBLE **out, DOUBLE **in, int size);
int CompareMatrix(DOUBLE **out, DOUBLE **in, int size);
void SaveMatrix(char *fileout, DOUBLE **matrix, int size1, int size2);

#define ORBITALS_MAX 1000

#define k_min_box (_2PI*L_inv)
#ifdef ORBITAL_3BODY

INLINE DOUBLE orbital(DOUBLE z, int alpha) {
  if(alpha == 0)
    return 1;
  else
    return Cos(alpha*k_min_box*z);
}

INLINE DOUBLE orbital_dz(DOUBLE z, int alpha) {
  if(alpha == 0)
    return 0;
  else
    return -alpha*k_min_box*Sin(k_min_box*z);
}

INLINE DOUBLE orbital_d2z(DOUBLE z, int alpha) {
  if(alpha == 0)
    return 0;
  else
    return -alpha*k_min_box*k_min_box*Cos(k_min_box*z);
}
#else
INLINE DOUBLE orbital(DOUBLE z, int alpha) {
  if(alpha == 0)
    return 1.;
  else if(alpha % 2 == 1)
    return Cos(_2PI*z*L_inv *((alpha+1)/2));
  else
    return Sin(_2PI*z*L_inv *(alpha/2));
}

INLINE DOUBLE orbital_dz(DOUBLE z, int alpha) {
  DOUBLE k;

  if(alpha == 0)
    return 0;
  else if(alpha % 2 == 1) {
    k = _2PI*L_inv*((alpha+1)/2);
    return -k*Sin(k*z);
  }
  else {
    k = _2PI*L_inv * (alpha/2);
    return k*Cos(k*z);
  }
}

INLINE DOUBLE orbital_d2z(DOUBLE z, int alpha) {
  DOUBLE k;

  if(alpha == 0)
    return 0;
  else if(alpha % 2 == 1) {
    k = _2PI*L_inv *((alpha+1)/2);
    return -k*k*Cos(k*z);
  }
  else {
    k = _2PI*L_inv * (alpha/2);
    return -k*k*Sin(k*z);
  }
}
#endif

extern int orbital_numbers_IFG[ORBITALS_MAX][3]; // (nx, ny, nz) for given alpha
extern int orbital_numbers_IFG_Norbital[ORBITALS_MAX];
extern int orbital_numbers_unpaired[ORBITALS_MAX][3]; // (nx, ny, nz) for given alpha
extern DOUBLE orbital_weight[ORBITALS_MAX]; // weights (var. parameters)
extern int orbital_N; // number of orbitals used
extern int alpha_polarized; // index of first polarized orbital

#endif
