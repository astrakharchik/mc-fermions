 /*quantities.c*/

#include <math.h>
#include <stdio.h>
#include "quantities.h"
#include "main.h"
#include "utils.h"
#include "trialf.h"
#include "trial.h"
#include "move.h"
#include "randnorm.h"
#include "rw.h"
#include "compatab.h"
#include "crystal.h"
#include "fermions.h"
#include "spline.h"
#include "ewald.h"
#include "trial.h"


/********************************* Energy ************************************/
// returns energy of the system averaged over all walkers
DOUBLE Energy(DOUBLE *Epot, DOUBLE *Ekin, DOUBLE *Eff, DOUBLE *Eext) {
  DOUBLE dEff;
  int w;

  *Epot = *Ekin = *Eff = *Eext = 0.;
  for(w=0; w<Nwalkers; w++) {
    if(optimization != 2) W[w].w = w;
    WalkerEnergy(&W[w], &W[w].Epot, &W[w].Ekin, &dEff, &W[w].Eext);
    *Epot += W[w].Epot;
    *Ekin += W[w].Ekin;
    *Eff  += dEff;
    *Eext += W[w].Eext;
  /*if(W[w].Epot + W[w].Ekin < -1e-4*(double)Nup) {//!!!
      WalkerEnergy(&W[w], &W[w].Epot, &W[w].Ekin, &dEff, &W[w].Eext);
      SaveCoordinates();
      Error(" very negative energy");
      exit(1);
    }  */
  }

  *Epot /= Nmean * (DOUBLE) Nwalkers;
  *Ekin /= Nmean * (DOUBLE) Nwalkers;
  *Eff  /= Nmean * (DOUBLE) Nwalkers;
  *Eext /= Nmean * (DOUBLE) Nwalkers;

  return *Epot + *Ekin;
}

/************************ Walker Energy **************************************/
DOUBLE WalkerEnergy0(struct Walker *W) {
  DOUBLE Epot;
  DOUBLE Ekin;
  DOUBLE Eff;
  DOUBLE Eext;
  DOUBLE E;

  E = WalkerEnergy(W, &Epot, &Ekin, &Eff, &Eext);

  W->Epot = Epot;
  W->Ekin = Ekin;
  W->Eext = Eext;
  W->EFF = Eff / Nmean;

  return E;
}

// notation: F = (\nabla \Psi) / \Psi
DOUBLE WalkerEnergy(struct Walker *W, DOUBLE *Eint, DOUBLE *Ekin, DOUBLE *EFF, DOUBLE *Eext) {
  int i,j,k;
  DOUBLE dr[3] = {0.}, r, r2;
  DOUBLE Force, dF;
  DOUBLE x,y,z;
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
  DOUBLE dFx, dFy, dFz;
#endif
#ifdef ORBITAL_POLARIZED_DESCRETE_BCS
#ifndef ORBITAL_ANISOTROPIC_RELATIVE
  DOUBLE dFx, dFy, dFz;
#endif
#endif
#ifdef CRYSTAL_SYMM_SUM_LATTICE
  DOUBLE M0, M1[3], M2, M2p, e;
#endif
#ifdef ORBITAL_ANISOTROPIC_ABSOLUTE
  DOUBLE dFxup, dFyup, dFzup, dFxdn, dFydn, dFzdn, dFupup, dFdndn;
#endif
#ifdef SYM_OPPOSITE_SPIN
  DOUBLE p1;
  DOUBLE p2;
  DOUBLE s;
  DOUBLE si;
  DOUBLE sj;
#endif
  DOUBLE EkinJ = 0.; // kinetic energy from Jastrow terms
  DOUBLE EkinD = 0.; // kinetic energy from determinant
  DOUBLE EkinSym = 0.; // kinetic energy from symmetrized terms
  DOUBLE Orb[3];
  int alpha_int;
  char name[30];
#ifdef FERMIONS
  int w;

  w = W->w;
#endif
//#ifdef UP_PARTICLES_PINNED_TO_A_CHAIN
//  AdjustUpParticlesPinnedToAChainWalker(W);
//#endif

  if(var_par_array) AdjustVariationalParameter(W->varparindex);

#ifdef FERMIONS // Fill the Determinant Matrix
  for(i=0; i<Nup; i++) {
    x = W->x[i];
    y = W->y[i];
    z = W->z[i];
    for(j=0; j<Nup; j++) {
      Determinant[w][i][j] = 0.;
#ifdef ORBITAL_ANISOTROPIC_ABSOLUTE // for all j
      Determinant[w][i][j] += orbitalFewBody_F(x,y,z,W->xdn[j],W->ydn[j],W->zdn[j],i,j);
#endif
      if(j<Ndn) { // unpolarized
        dr[0] = x - W->xdn[j];
        dr[1] = y - W->ydn[j];
        dr[2] = z - W->zdn[j];
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef ORBITAL_ISOTROPIC
        Determinant[w][i][j] += orbitalF(sqrt(r2));
#endif
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
        Determinant[w][i][j] += orbitalDescreteBCS_F(dr[0], dr[1], dr[2]);
#endif
      }
      else { // polarized
#ifdef ORBITAL_POLARIZED_DESCRETE_BCS // fill with BCS orbitals
        Determinant[w][i][j] += orbitalDescreteBCS_F(dr[0], dr[1], dr[2]);
#endif
#ifdef ORBITAL_POLARIZED_ALPHA
        alpha_int = Nup - 1 - j;
        Determinant[w][i][j] = orbital(W->x[i], orbital_numbers_unpaired[alpha_int][0])*orbital(W->y[i], orbital_numbers_unpaired[alpha_int][1])*orbital(W->z[i], orbital_numbers_unpaired[alpha_int][2]);
#endif
#ifdef ORBITAL_POLARIZED_3BODY
        if(i==0)
          Determinant[w][i][j] = Cos(k_min_box*(W->x[0]-W->x[1]));
        else
          Determinant[w][i][j] = 0.;
#endif
      }
    }
  }

  if(LUinv(Determinant[w], DeterminantInv[w], Nup) == 0) {
    Warning("inverse: singular matrix, W[%i]!\n", w);
    sprintf(name, "%i.mat", w);
    SaveMatrix(name, Determinant[w], Nup, Nup);

    Warning("matrix saved in file %s\n", name);
    //SaveMatrix("A.dat", Determinant[w], Nup, Nup);
    if(MC == DIFFUSION) {
      Warning("killing this W!\n");
      W[w].status = DEAD;
      W[w].weight = 0.;
      overlaped++;
    }
  }
  W->Determinant = determinantLU;

  // initialize Force array and energy
  for(i=0; i<Nmax; i++) for(k=0; k<3; k++) W->Dup[i][k] = W->Ddn[i][k] = 0.;
#endif // end FERMIONS

  *Eint = *EFF = *Eext = *Ekin = 0.;
  for(i=0; i<Nup; i++) W->Fup[i][0] = W->Fup[i][1] = W->Fup[i][2] = 0.;
  for(i=0; i<Ndn; i++) W->Fdn[i][0] = W->Fdn[i][1] = W->Fdn[i][2] = 0.;
#ifdef SYM_OPPOSITE_SPIN
  for(i=0; i<Nup; i++) W->P1up[i][0] = W->P1up[i][1] = W->P1up[i][2] = 0.;
  for(i=0; i<Ndn; i++) W->P1dn[i][0] = W->P1dn[i][1] = W->P1dn[i][2] = 0.;
  for(i=0; i<Nup; i++) W->P2up[i][0] = W->P2up[i][1] = W->P2up[i][2] = 0.;
  for(i=0; i<Ndn; i++) W->P2dn[i][0] = W->P2dn[i][1] = W->P2dn[i][2] = 0.;
#endif

#ifdef EXTERNAL_POTENTIAL // add external potential energy
  for(i=0; i<Nup; i++) *Eext += VextUp(W->x[i], W->y[i], W->z[i], i);
  for(i=0; i<Ndn; i++) *Eext += VextDn(W->xdn[i], W->ydn[i], W->zdn[i], i);
#endif

#ifdef ONE_BODY_TRIAL_TERMS
  for(i=0; i<Nup; i++) {
    OneBodyFp(&W->Fup[i][0], &W->Fup[i][1], &W->Fup[i][2], W->x[i], W->y[i], W->z[i], i, ON);
    *Ekin += OneBodyE(W->x[i], W->y[i], W->z[i], i, ON);
  }
  for(i=0; i<Ndn; i++) {
    OneBodyFp(&W->Fdn[i][0], &W->Fdn[i][1], &W->Fdn[i][2], W->xdn[i], W->ydn[i], W->zdn[i], i, OFF);
    *Ekin += OneBodyE(W->xdn[i], W->ydn[i], W->zdn[i], i, OFF);
  }
#endif

#ifdef CRYSTAL
#ifdef CRYSTAL_SYMM_SUM_PARTICLE
  for(j=0; j<Crystal.size; j++) {// fill Mo array
    MoUp[j] = MoDn[j] = 0.;
    for(i=0; i<Nup; i++) {
      dr[0] = W->x[i] - Crystal.x[j]; 
      dr[1] = W->y[i] - Crystal.y[j];
      dr[2] = W->z[i] - Crystal.z[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      //if(CheckInteractionCondition(dr[2], r2)) {
        MoUp[j] += Exp(-Crystal.R*r2);
      //}
      dr[0] = W->xdn[i] - Crystal.x[j];
      dr[1] = W->ydn[i] - Crystal.y[j];
      dr[2] = W->zdn[i] - Crystal.z[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      //if(CheckInteractionCondition(dr[2], r2)) {
        MoDn[j] += Exp(-Crystal.R*r2);
      //}
    }
  }
  for(j=0; j<Crystal.size; j++) { // calculate contribution of the crystal w.f. (second type)
    M2 = M2p = 0.; // spin up
    for(i=0; i<Nup; i++) { 
      dr[0] = W->x[i] - Crystal.x[j];
      dr[1] = W->y[i] - Crystal.y[j];
      dr[2] = W->z[i] - Crystal.z[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      //if(CheckInteractionCondition(dr[2], r2)) {
        e = Exp(-Crystal.R*r2);
        for(k=0; k<3; k++) W->Fup[i][k] += -2*Crystal.R*dr[k]*e/MoUp[j];
        M2 += r2 * e;
        M2p += r2 * e*e;
      //}
    }
    EkinJ -= 4*Crystal.R*Crystal.R *(M2 - M2p/MoUp[j])/MoUp[j];
    M2 = M2p = 0.; // spin down
    for(i=0; i<Ndn; i++) { 
      dr[0] = W->xdn[i] - Crystal.x[j];
      dr[1] = W->ydn[i] - Crystal.y[j];
      dr[2] = W->zdn[i] - Crystal.z[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      //if(CheckInteractionCondition(dr[2], r2)) {
        e = Exp(-Crystal.R*r2);
        for(k=0; k<3; k++) W->Fdn[i][k] += -2*Crystal.R*dr[k]*e/MoDn[j];
        M2 += r2 * e;
        M2p += r2 * e*e;
       //}
    }
    EkinJ -= 4*Crystal.R*Crystal.R *(M2 - M2p/MoDn[j])/MoDn[j];
  }
#endif
#endif

  // Determinant contribution + Jastrow Mixed terms
  for(i=0; i<Nup; i++) {
    x = W->x[i];
    y = W->y[i];
    z = W->z[i];
#ifdef CRYSTAL // add contribution from the w.f. of crystal
    //EkinJ += 12*Crystal.R; // 2[unit] 3[dimension] 2[spin], i.e. in units of h^2/2mR^2
#ifdef CRYSTAL_SYMM_SUM_LATTICE
    M0 = M1[0] = M1[1] = M1[2] = M2 = 0.;
    for(j=0; j<Crystal.size; j++) {
      dr[0] = W->x[i] - Crystal.x[j]; // spin up
      dr[1] = W->y[i] - Crystal.y[j];
      dr[2] = W->z[i] - Crystal.z[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(CheckInteractionCondition(dr[2], r2)) {
        e = Exp(-Crystal.R*r2);
        M0 += e;
        for(k=0; k<3; k++) M1[k] += dr[k]*e;
        M2 += r2*e;
      }
    }
    for(k=0; k<3; k++) M1[k] *= -2*Crystal.R / M0;
    for(k=0; k<3; k++) W->Fup[i][k] += M1[k];
    EkinJ -= 4*Crystal.R*Crystal.R *M2/M0 - M1[0]*M1[0]-M1[1]*M1[1]-M1[2]*M1[2];

    M0 = M1[0] = M1[1] = M1[2] = M2 = 0.;
    for(j=0; j<Crystal.size; j++) {
      dr[0] = W->xdn[i] - Crystal.x[j]; // spin up
      dr[1] = W->ydn[i] - Crystal.y[j];
      dr[2] = W->zdn[i] - Crystal.z[j];
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(CheckInteractionCondition(dr[2], r2)) {
        e = Exp(-Crystal.R*r2);
        M0 += e;
        for(k=0; k<3; k++) M1[k] += dr[k]*e;
        M2 += r2*e;
      }
    }
    for(k=0; k<3; k++) M1[k] *= -2*Crystal.R/ M0;
    for(k=0; k<3; k++) W->Fdn[i][k] += M1[k];
    EkinJ -= 4*Crystal.R*Crystal.R *M2/M0 - M1[0]*M1[0]-M1[1]*M1[1]-M1[2]*M1[2];
#endif
#endif// end crystal

    for(j=0; j<Nmax; j++) { // particle-particle force
#ifdef ORBITAL_ANISOTROPIC_ABSOLUTE // for all j
      // first derivative
      orbitalFewBody_Fp(&dFxup, &dFyup, &dFzup, &dFxdn, &dFydn, &dFzdn, x, y, z, W->xdn[j], W->ydn[j], W->zdn[j], i, j);

      W->Dup[i][0] += DeterminantInv[w][i][j] * dFxup;
      W->Dup[i][1] += DeterminantInv[w][i][j] * dFyup;
      W->Dup[i][2] += DeterminantInv[w][i][j] * dFzup;
      W->Ddn[j][0] -= DeterminantInv[w][i][j] * dFxdn;
      W->Ddn[j][1] -= DeterminantInv[w][i][j] * dFydn;
      W->Ddn[j][2] -= DeterminantInv[w][i][j] * dFzdn;

      orbitalFewBody_Fpp(&dFupup, &dFdndn, x, y, z, W->xdn[j], W->ydn[j], W->zdn[j], i, j);

      // second derivative
      EkinD -= DeterminantInv[w][i][j]*(m_up_inv*dFupup + m_dn_inv*dFdndn);
#endif

      if(j<Ndn) { // unpolarized part
        CaseX(dr[0] = x - W->xdn[j]);
        CaseY(dr[1] = y - W->ydn[j]);
        CaseZ(dr[2] = z - W->zdn[j]);
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
        r = sqrt(r2);

#ifdef INTERACTION_SUM_OVER_IMAGES
        *Eint += InteractionEnergySumOverImages12(dr[0],dr[1],dr[2]);
#endif

//#ifdef INTERACTION_EWALD
//        *Eint += EwaldSumPair(dr[0],dr[1],dr[2]);
//#endif

#ifdef INTERACTION_WITHOUT_IMAGES
        if(CheckInteractionCondition(dr[2], r2)) 
          *Eint += InteractionEnergy12(r);
#endif

#ifdef FERMIONS
#ifdef ORBITAL_ISOTROPIC
        if(CheckInteractionConditionOrbital(dr[2], r2)) {
          r = sqrt(r2);
          Force = DeterminantInv[w][i][j]*orbitalFp(r)/r; // orbitalFp = f'
          W->Dup[i][0] += Force * dr[0];
          W->Dup[i][1] += Force * dr[1];
          W->Dup[i][2] += Force * dr[2];
          W->Ddn[j][0] -= Force * dr[0];
          W->Ddn[j][1] -= Force * dr[1];
          W->Ddn[j][2] -= Force * dr[2];
          EkinD -= (m_up_inv+m_dn_inv)*DeterminantInv[w][i][j] * orbitalEloc(r); // - 2 [+(f" +2/r f')]
        }
#endif // end ORBITAL_ISOTROPIC
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
        orbitalDescreteBCS_Fp(&dFx, &dFy, &dFz, dr[0], dr[1], dr[2]);
        W->Dup[i][0] += DeterminantInv[w][i][j] * dFx;
        W->Dup[i][1] += DeterminantInv[w][i][j] * dFy;
        W->Dup[i][2] += DeterminantInv[w][i][j] * dFz;
        W->Ddn[j][0] -= DeterminantInv[w][i][j] * dFx;
        W->Ddn[j][1] -= DeterminantInv[w][i][j] * dFy;
        W->Ddn[j][2] -= DeterminantInv[w][i][j] * dFz;
        EkinD -= (m_up_inv+m_dn_inv)*DeterminantInv[w][i][j] * orbitalDescreteBCS_Fpp(dr[0], dr[1], dr[2]);
#endif
#endif // end FERMIONS

#ifdef JASTROW_OPPOSITE_SPIN // Jastrow MIXED terms
        if(CheckInteractionCondition(dr[2], r2)) {
          Force = InterpolateFp12(&G12, r) / r;
          CaseX(W->Fup[i][0] += Force*dr[0]);
          CaseX(W->Fdn[j][0] -= Force*dr[0]);
          CaseY(W->Fup[i][1] += Force*dr[1]);
          CaseY(W->Fdn[j][1] -= Force*dr[1]);
          CaseZ(W->Fup[i][2] += Force*dr[2]);
          CaseZ(W->Fdn[j][2] -= Force*dr[2]);

          EkinJ += (m_up_inv + m_dn_inv)*InterpolateE12(&G12, r);
        }
#endif
      }
      else { // polarized
#ifdef ORBITAL_POLARIZED_DESCRETE_BCS
        orbitalDescreteBCS_Fp(&dFx, &dFy, &dFz, dr[0], dr[1], dr[2]);
        W->Dup[i][0] += DeterminantInv[w][i][j] * dFx;
        W->Dup[i][1] += DeterminantInv[w][i][j] * dFy;
        W->Dup[i][2] += DeterminantInv[w][i][j] * dFz;
        W->Ddn[j][0] -= DeterminantInv[w][i][j] * dFx;
        W->Ddn[j][1] -= DeterminantInv[w][i][j] * dFy;
        W->Ddn[j][2] -= DeterminantInv[w][i][j] * dFz;
        EkinD -= (m_up_inv+m_dn_inv)*DeterminantInv[w][i][j] * orbitalDescreteBCS_Fpp(dr[0], dr[1], dr[2]);
#endif
#ifdef ORBITAL_POLARIZED_ALPHA
        alpha_int = Nup - 1 - j;
        Orb[0] = orbital(x, orbital_numbers_unpaired[alpha_int][0]);
        Orb[1] = orbital(y, orbital_numbers_unpaired[alpha_int][1]);
        Orb[2] = orbital(z, orbital_numbers_unpaired[alpha_int][2]);
        // first derivative
        W->Dup[i][0] += DeterminantInv[w][i][j] * orbital_dz(x, orbital_numbers_unpaired[alpha_int][0]) * Orb[1] * Orb[2];
        W->Dup[i][1] += DeterminantInv[w][i][j] * Orb[0] * orbital_dz(y, orbital_numbers_unpaired[alpha_int][1]) * Orb[2];
        W->Dup[i][2] += DeterminantInv[w][i][j] * Orb[0] * Orb[1] * orbital_dz(z, orbital_numbers_unpaired[alpha_int][2]);
        // second derivative
        EkinD -= m_up_inv*DeterminantInv[w][i][j] * (
            orbital_d2z(x, orbital_numbers_unpaired[alpha_int][0]) * Orb[1] * Orb[2]
          + Orb[0] * orbital_d2z(y, orbital_numbers_unpaired[alpha_int][1]) * Orb[2]
          + Orb[0] * Orb[1] * orbital_d2z(z, orbital_numbers_unpaired[alpha_int][2])
          );
#endif
#ifdef ORBITAL_POLARIZED_3BODY
        if(i==0) {
          // first derivative
          W->Dup[0][0] += DeterminantInv[w][0][0] * (-k_min_box*Sin(k_min_box*(W->x[0]-W->x[1])));
          W->Dup[1][0] -= DeterminantInv[w][1][0] * (-k_min_box*Sin(k_min_box*(W->x[0]-W->x[1])));
          // second derivative
          EkinD -= m_up_inv*DeterminantInv[w][0][0] * (-k_min_box*k_min_box*Cos(k_min_box*(W->x[0]-W->x[1])));
          EkinD -= m_up_inv*DeterminantInv[w][1][0] * (-k_min_box*k_min_box*Cos(k_min_box*(W->x[0]-W->x[1])));
        }
#endif
      }
    }
  }

#ifdef JASTROW_SAME_SPIN // Jastrow contribution (same spin particle-particle force)
  for(i=0; i<Nup; i++) {
    for(j=i+1; j<Nup; j++) {
      CaseX(dr[0] = W->x[i] - W->x[j]); // spin up
      CaseY(dr[1] = W->y[i] - W->y[j]);
      CaseZ(dr[2] = W->z[i] - W->z[j]);
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      r = sqrt(r2);

#ifdef INTERACTION_SUM_OVER_IMAGES
      *Eint += InteractionEnergySumOverImages11(dr[0],dr[1],dr[2]);
#endif

      if(CheckInteractionCondition(dr[2], r2)) {
#ifdef INTERACTION_WITHOUT_IMAGES
        *Eint += InteractionEnergy11(r);
#endif
        EkinJ += 2.*m_up_inv*InterpolateE11(&G11, r); // [-(f"+2f'/r)/f] + (f'/f)^2
        Force = InterpolateFp11(&G11, r) / r; // Fp = f' / f
        CaseX(W->Fup[i][0] += Force*dr[0]);
        CaseX(W->Fup[j][0] -= Force*dr[0]);
        CaseY(W->Fup[i][1] += Force*dr[1]);
        CaseY(W->Fup[j][1] -= Force*dr[1]);
        CaseZ(W->Fup[i][2] += Force*dr[2]);
        CaseZ(W->Fup[j][2] -= Force*dr[2]);
      }
    }
  }

  for(i=0; i<Ndn; i++) {
    for(j=i+1; j<Ndn; j++) { 
      CaseX(dr[0] = W->xdn[i] - W->xdn[j]); // spin down
      CaseY(dr[1] = W->ydn[i] - W->ydn[j]);
      CaseZ(dr[2] = W->zdn[i] - W->zdn[j]);
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      r = sqrt(r2);

#ifdef INTERACTION_SUM_OVER_IMAGES
      *Eint += InteractionEnergySumOverImages22(dr[0],dr[1],dr[2]);
#endif

      if(CheckInteractionCondition(dr[2], r2)) {
#ifdef INTERACTION_WITHOUT_IMAGES
        *Eint += InteractionEnergy22(r);
#endif
        EkinJ += 2.*m_dn_inv*InterpolateE22(&G22, r);
        Force = InterpolateFp22(&G22, r) / r;
        CaseX(W->Fdn[i][0] += Force*dr[0]);
        CaseX(W->Fdn[j][0] -= Force*dr[0]);
        CaseY(W->Fdn[i][1] += Force*dr[1]);
        CaseY(W->Fdn[j][1] -= Force*dr[1]);
        CaseZ(W->Fdn[i][2] += Force*dr[2]);
        CaseZ(W->Fdn[j][2] -= Force*dr[2]);
      }
    }
  }
#endif

#ifdef SYM_OPPOSITE_SPIN
  p1 = 1.;
  for (i = 0; i < Nup; i++) {
    s = 0.;
    x = W->x[i];
    y = W->y[i];
    z = W->z[i];

    for (j = 0; j < Ndn; j++) {
      dr[0] = x - W->xdn[j];
      dr[1] = y - W->ydn[j];
      dr[2] = z - W->zdn[j];

      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if (CheckInteractionConditionOrbital(dr[2], r2)) {
        r = sqrt(r2);
        s += InterpolateSYMF(&GSYM, r);
      }
    }
    W->Sup[i] = s;
    p1 *= s;
  }

  //Calculate p2 = product_{j=1}^{Ndn} sum_{i=1}^{Nup}f(r_ij)
  p2 = 1.;
  for (j = 0; j < Ndn; j++) {
    s = 0.;
    x = W->xdn[j];
    y = W->ydn[j];
    z = W->zdn[j];

    for (i = 0; i < Nup; i++) {
      dr[0] = W->x[i] - x;
      dr[1] = W->y[i] - y;
      dr[2] = W->z[i] - z;

      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(CheckInteractionConditionOrbital(dr[2], r2)) {
        r = sqrt(r2);
        s += InterpolateSYMF(&GSYM, r);
      }
    }
    W->Sdn[j] = s;
    p2 *= s;
  }

  // Calculate Sup;    Si = sum_{j=1}^{Ndn}f(r_ij)
  //Calculate    p1 (A_i) / (p1 + p2)   and   p2 (B_i) / (p1 + p2)   contributions to |F|^2
  // A_i
  // A_i=sum_{j=1}^{Ndn}f�(r_ij)/S_i
  // S_i = sum_{j=1}^{Ndn}f(r_ij)
  for(i=0; i<Nup; i++) {
    x = W->x[i];
    y = W->y[i];
    z = W->z[i];
    si = W->Sup[i];
    for(j=0; j<Ndn; j++) { // Calculate Fix Fiy Fiz
      dr[0] = x - W->xdn[j];
      dr[1] = y - W->ydn[j];
      dr[2] = z - W->zdn[j];
      sj = W->Sdn[j];

      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      if(CheckInteractionConditionOrbital(dr[2], r2)) {
        r = sqrt(r2);
        Force = InterpolateSYMFp(&GSYM, r) / r;
        CaseX(W->Fup[i][0] += ((Force * dr[0]) * (p1 / si + p2 / sj)) / (p1 + p2));
        CaseX(W->Fdn[j][0] -= ((Force * dr[0]) * (p1 / si + p2 / sj)) / (p1 + p2));
        CaseY(W->Fup[i][1] += ((Force * dr[1]) * (p1 / si + p2 / sj)) / (p1 + p2));
        CaseY(W->Fdn[j][1] -= ((Force * dr[1]) * (p1 / si + p2 / sj)) / (p1 + p2));
        CaseZ(W->Fup[i][2] += ((Force * dr[2]) * (p1 / si + p2 / sj)) / (p1 + p2));
        CaseZ(W->Fdn[j][2] -= ((Force * dr[2]) * (p1 / si + p2 / sj)) / (p1 + p2));

        CaseX(W->P1up[i][0] += (Force * dr[0] / si) * (p1 / (p1 + p2)));
        CaseX(W->P1dn[j][0] -= (Force * dr[0] / si) * (p1 / (p1 + p2)));
        CaseY(W->P1up[i][1] += (Force * dr[1] / si) * (p1 / (p1 + p2)));
        CaseY(W->P1dn[j][1] -= (Force * dr[1] / si) * (p1 / (p1 + p2)));
        CaseZ(W->P1up[i][2] += (Force * dr[2] / si) * (p1 / (p1 + p2)));
        CaseZ(W->P1dn[j][2] -= (Force * dr[2] / si) * (p1 / (p1 + p2)));

        CaseX(W->P2up[i][0] += (Force * dr[0] / sj) * (p2 / (p1 + p2)));
        CaseX(W->P2dn[j][0] -= (Force * dr[0] / sj) * (p2 / (p1 + p2)));
        CaseY(W->P2up[i][1] += (Force * dr[1] / sj) * (p2 / (p1 + p2)));
        CaseY(W->P2dn[j][1] -= (Force * dr[1] / sj) * (p2 / (p1 + p2)));
        CaseZ(W->P2up[i][2] += (Force * dr[2] / sj) * (p2 / (p1 + p2)));
        CaseZ(W->P2dn[j][2] -= (Force * dr[2] / sj) * (p2 / (p1 + p2)));

        //EkinSym += (m_up_inv + m_dn_inv)*(InterpolateSYME(&GSYM, r) / si)* (p1 / (p1 + p2));
       EkinSym += ((m_up_inv + m_dn_inv)*(p1 / si + p2 / sj)*InterpolateSYME(&GSYM, r) 
                 + (m_dn_inv * p1 / (si*si) + m_up_inv * p2 / (sj*sj))*(InterpolateSYMFp(&GSYM, r)*InterpolateSYMFp(&GSYM, r)))/(p1 + p2);
      }
    }
  }
#endif

  // calculate modulus squared [F + D]^2
  for(i=0; i<Nmax; i++) {
#ifdef FERMIONS
    EkinD +=  m_up_inv*W->Dup[i][0]*W->Dup[i][0] + m_dn_inv*W->Ddn[i][0]*W->Ddn[i][0]
             +m_up_inv*W->Dup[i][1]*W->Dup[i][1] + m_dn_inv*W->Ddn[i][1]*W->Ddn[i][1]
             +m_up_inv*W->Dup[i][2]*W->Dup[i][2] + m_dn_inv*W->Ddn[i][2]*W->Ddn[i][2];
#endif

    *EFF +=   m_up_inv*(
      CaseX(  (W->Dup[i][0]+W->Fup[i][0])*(W->Dup[i][0]+W->Fup[i][0]))
      CaseY(+ (W->Dup[i][1]+W->Fup[i][1])*(W->Dup[i][1]+W->Fup[i][1]))
      CaseZ(+ (W->Dup[i][2]+W->Fup[i][2])*(W->Dup[i][2]+W->Fup[i][2]))
      )
      + m_dn_inv*(
      CaseX(  (W->Ddn[i][0]+W->Fdn[i][0])*(W->Ddn[i][0]+W->Fdn[i][0]))
      CaseY(+ (W->Ddn[i][1]+W->Fdn[i][1])*(W->Ddn[i][1]+W->Fdn[i][1]))
      CaseZ(+ (W->Ddn[i][2]+W->Fdn[i][2])*(W->Ddn[i][2]+W->Fdn[i][2]))
      );

    if(MC == DIFFUSION && SmartMC == DMC_LINEAR_GAUSSIAN) { // Linear DMC code optimization, remember forces
        CaseX(W->F[i][0] = W->Fup[i][0] + W->Dup[i][0]);
        CaseY(W->F[i][1] = W->Fup[i][1] + W->Dup[i][1]);
        CaseZ(W->F[i][2] = W->Fup[i][2] + W->Dup[i][2]);
        CaseX(W->F[i][3] = W->Fdn[i][0] + W->Ddn[i][0]);
        CaseY(W->F[i][4] = W->Fdn[i][1] + W->Ddn[i][1]);
        CaseZ(W->F[i][5] = W->Fdn[i][2] + W->Ddn[i][2]);
    }
  }

#ifdef SYM_OPPOSITE_SPIN
  for(i = 0; i < Nmax; i++) {
    EkinSym += 
      - m_dn_inv*(p2 / p1 + 1.)*((W->P1dn[i][0])*(W->P1dn[i][0]) + (W->P1dn[i][1])*(W->P1dn[i][1]) + (W->P1dn[i][2])*(W->P1dn[i][2]))
      - m_up_inv*(p1 / p2 + 1.)*((W->P2up[i][0])*(W->P2up[i][0]) + (W->P2up[i][1])*(W->P2up[i][1]) + (W->P2up[i][2])*(W->P2up[i][2]))
      + m_up_inv*((W->P1up[i][0] + W->P2up[i][0])*(W->P1up[i][0] + W->P2up[i][0]) + (W->P1up[i][1] + W->P2up[i][1])*(W->P1up[i][1] + W->P2up[i][1]) + (W->P1up[i][2] + W->P2up[i][2])*(W->P1up[i][2] + W->P2up[i][2]))
      + m_dn_inv*((W->P1dn[i][0] + W->P2dn[i][0])*(W->P1dn[i][0] + W->P2dn[i][0]) + (W->P1dn[i][1] + W->P2dn[i][1])*(W->P1dn[i][1] + W->P2dn[i][1]) + (W->P1dn[i][2] + W->P2dn[i][2])*(W->P1dn[i][2] + W->P2dn[i][2]));
  }
#endif

  *Ekin += EkinJ + EkinD + EkinSym;

#ifdef RESCALE_ENERGY_3D_HS // in a homogeneous 3D system energy is measured in units of [h^2/2ma^2]
  *V *= 2;
  *Eext *= 2;
#else // otherwise in units of [h^2/mr^2] or [hw], add 1/2 to the kinetic term
  *Ekin *= 0.5;
  *EFF  *= 0.5;
#endif
  *Ekin -= *EFF;
  *EFF += *Eint + *Eext;

  *Eint = *Eint + *Eext; // return Epot instead of Eint

  W->E = *Eint + *Ekin;

  return *Eint + *Ekin;
}

/************************ Interaction Energy *********************************/
DOUBLE InteractionEnergy12(DOUBLE r) {
#ifdef INTERACTION_ABSENT
  return 0.;
#endif

#ifdef INTERACTION12_COULOMB_ATTRACTION
#ifdef BC_BILAYER
  return -1./sqrt(r*r+bilayer_width2);
#else
  return -1./(r*excitonRadius);
#endif
#endif

#ifdef INTERACTION12_COULOMB_REPULSION
  return 1./r;
#endif

#ifdef INTERACTION_DIPOLE 
  return 1./(r*r*r);
#endif

#ifdef INTERACTION12_DIPOLE_BILAYER
  return (r*r-2.*bilayer_width2)*pow(r*r+bilayer_width2, -2.5);
#endif

#ifdef INTERACTION_LENNARD_JONES
  DOUBLE rt;

  rt = sigma_lj/r;
  rt *= rt;
  rt *= (rt*rt);

  return U_lj*(rt*rt - rt);
#endif

#ifdef INTERACTION12_SQUARE_WELL
  if(r<RoSW)
    return -trial_Vo;
  else
    return 0;
#endif

#ifdef INTERACTION12_SOFT_SPHERE
  if(r<Ro12)
    return trial_Vo;
  else
    return 0;
#endif

#ifdef INTERACTION_CH2_12
  DOUBLE e;

  e = 1./cosh(r);
  return -Epar*e*e; // -2
#endif

#ifdef INTERACTION12_SUTHERLAND
  static int first_time = ON;
  static DOUBLE sqE,sqE2;

  if(first_time) {
    sqE = PI/L;
    sqE2 = sqE*sqE;
    first_time = OFF;
  }
  return lambda12*(lambda12-1.)*sqE2/(Sin(sqE*r)*Sin(sqE*r));
#endif

#ifdef INTERACTION12_LiHe
  DOUBLE r2,r3,r4,r5,r6,r7,r8;
  r2 = r*r;
  r3 = r2*r;
  r4 = r2*r2;
  r5 = r2*r3;
  r6 = r4*r2;
  r7 = r3*r4;
  r8 = r4*r4;
  return 315774.6646073474*(9.0862 + 26.8695048*r + 40.5110055796*r2 + 41.059979938199476*r3 + 31.930901502125543*r4 + 20.711064754799857*r5 + 11.77122531844017*r6 + 5.987267606922694*r7 + 23.60361872556778*r8 + Exp(2.554*r)*(-9.0862 - 3.66335*r - 1.52055*r2 - 0.691596*r4))/ (Exp(2.5539999999999994*r)*r8);
#endif

#ifdef INTERACTION12_HeHe
  DOUBLE epsilon = 10.22; // in K
  DOUBLE sigma = 2.556; // A
  DOUBLE x;
  x = sigma/r;
  x = x*x*x; // 1/x^3
  x = x*x; // 1/x^6
  return 4.*epsilon*(x*x-x)/energy_unit;
#endif

#ifdef INTERACTION12_Aziz
  DOUBLE epsilon = 10.948; // in K
  DOUBLE C6 = 1.36745214; //K/A^6
  DOUBLE rm = 2.963; //A
  DOUBLE C8 = 0.42123807; //K/A^8
  DOUBLE D = 1.4826; //
  DOUBLE C10 = 0.17473318; //K/A^10
  DOUBLE alpha = 10.43329537; //A^-1
  DOUBLE A = 1.8443101e5; //K
  DOUBLE beta = -2.27965105; //A^-2
  DOUBLE x;
  DOUBLE F;

  r /= rm;
  if(r<D)
    F = Exp(-(D/r-1.)*(D/r-1.));
  else
    F = 1.;

  F *= (C6 + (C8 + C10/(r*r))/(r*r))/(r*r*r*r*r*r);

  return epsilon*( A*Exp(-alpha*r + beta*r*r) -F)/energy_unit;
#endif

#ifdef INTERACTION12_RYDBERG
  return 1./(r*r*r*r*r*r);
  //return 4.78927/(r*r*r*r*r*r); // scattering length 1
#endif

#ifdef INTERACTION12_RYDBERG_SCREENED
  return a/(r*r*r*r*r*r+b*b*b*b*b*b);
#endif

#ifdef INTERACTION12_LENNARD_JONES_6_10
  DOUBLE rt;

  rt = 1. / r; // 1
  rt *= rt; // 2
  return 14.73013*rt*rt*rt*(rt*rt - 1.); // (6-10) potetnail with a_s = -1
#endif

#ifdef INTERACTION12_ION
  //return -c4*(r*r - a4*a4) / ((r*r + a4*a4)*(r*r + b4*b4)*(r*r + b4*b4));
  return -a6*(r*r - c6*c6) / ((r*r + c6*c6)*(r*r + b6*b6)*(r*r + b6*b6));
#endif
}

DOUBLE InteractionEnergy11(DOUBLE r) {
#ifdef INTERACTION11_ABSENT
  return 0.;
#endif

#ifdef INTERACTION11_COULOMB
  return 1./(r*excitonRadius);
#endif

#ifdef INTERACTION11_HAMILTONIAN_MEAN_FIELD
  return Dpar*cos(PI*r/L);
#endif

#ifdef INTERACTION11_SUTHERLAND
  static int first_time = ON;
  static DOUBLE sqE,sqE2;

  if(first_time) {
    sqE = PI/L;
    sqE2 = sqE*sqE;
    first_time = OFF;
  }
  return lambda1*(lambda1-1.)*sqE2/(Sin(sqE*r)*Sin(sqE*r));
#endif

#ifdef INTERACTION11_CALOGERO
  return Dpar/(r*r);
#endif

#ifdef INTERACTION11_SQUARE_WELL
  if(r<Ro)
    return -trial11_Vo;
  else
    return 0;
#endif

#ifdef INTERACTION11_SOFT_SPHERE
  if(r<Ro11)
    return trial11_Vo;
  else
    return 0;
#endif

#ifdef INTERACTION11_DIPOLE
  return 1./(r*r*r);
#endif

#ifdef INTERACTION11_LiLi
  return 2.22125e7/(r*r*r*r*r*r*r*r*r*r*r*r) + 41828.9*Exp(-1.20145*r)*Cos(1.84959*(r-5.03762));
#endif

#ifdef INTERACTION11_HeHe
  DOUBLE epsilon = 10.22; // in K
  DOUBLE sigma = 2.556; // A
  DOUBLE x;
  x = sigma/r;
  x = x*x*x; // 1/x^3
  x = x*x; // 1/x^6
  return 4.*epsilon*(x*x-x)/energy_unit;
#endif

#ifdef INTERACTION11_Aziz
  DOUBLE epsilon = 10.948; // in K
  DOUBLE C6 = 1.36745214; //K/A^6
  DOUBLE rm = 2.963; //A
  DOUBLE C8 = 0.42123807; //K/A^8
  DOUBLE D = 1.4826; //
  DOUBLE C10 = 0.17473318; //K/A^10
  DOUBLE alpha = 10.43329537; //A^-1
  DOUBLE A = 1.8443101e5; //K
  DOUBLE beta = -2.27965105; //A^-2
  DOUBLE x;
  DOUBLE F;

  r /= rm;
  if(r<D)
    F = Exp(-(D/r-1.)*(D/r-1.));
  else
    F = 1.;

  F *= (C6 + (C8 + C10/(r*r))/(r*r))/(r*r*r*r*r*r);

  return epsilon*( A*Exp(-alpha*r + beta*r*r) -F)/energy_unit;
#endif

#ifdef INTERACTION_CH2_11
  DOUBLE e;

  e = 1./cosh(r);
  return -2.*e*e;
#endif

#ifdef INTERACTION11_LENNARD_JONES
  DOUBLE rt;

  rt = sigma_lj/r;
  rt *= rt; // 2
  rt *= (rt*rt); // 6

  return U_lj*(rt*rt - rt);
#endif

#ifdef INTERACTION11_LENNARD_JONES_6_10
  DOUBLE rt;

  rt = 1. / r; // 1
  rt *= rt; // 2
  return 14.73013*rt*rt*rt*(rt*rt - 1.); // (6-10) potetnail with a_s = -1
#endif

#ifdef INTERACTION11_RYDBERG
  //return 1./(r*r*r*r*r*r);
  return 4.78927/(r*r*r*r*r*r); // scattering length 1
#endif

#ifdef INTERACTION11_RYDBERG_SCREENED
  return 1./(r*r*r*r*r*r+a*a*a*a*a*a);
#endif

}

DOUBLE InteractionEnergy22(DOUBLE r) {
#ifdef INTERACTION22_ABSENT
  return 0.;
#endif

#ifdef INTERACTION11_COULOMB
  return 1./(r*excitonRadius);
#endif

#ifdef INTERACTION22_SUTHERLAND
  static int first_time = ON;
  static DOUBLE sqE,sqE2;

  if(first_time) {
    sqE = PI/L;
    sqE2 = sqE*sqE;
    first_time = OFF;
  }
  return lambda2*(lambda2-1.)*sqE2/(Sin(sqE*r)*Sin(sqE*r));
#endif

#ifdef INTERACTION11_SQUARE_WELL
  if(r<Ro)
    return -trial22_Vo;
  else
    return 0;
#endif

#ifdef INTERACTION22_SOFT_SPHERE
  if(r<Ro22)
    return trial22_Vo;
  else
    return 0;
#endif

#ifdef INTERACTION22_DIPOLE
  return 1./(r*r*r);
#endif

#ifdef INTERACTION22_HeHe
  DOUBLE epsilon = 10.22; // in K
  DOUBLE sigma = 2.556; // A
  DOUBLE x;
  x = sigma/r;
  x = x*x*x; // 1/x^3
  x = x*x; // 1/x^6
  return 4.*epsilon*(x*x-x)/energy_unit;
#endif

#ifdef INTERACTION22_Aziz
  DOUBLE epsilon = 10.948; // in K
  DOUBLE C6 = 1.36745214; //K/A^6
  DOUBLE rm = 2.963; //A
  DOUBLE C8 = 0.42123807; //K/A^8
  DOUBLE D = 1.4826; //
  DOUBLE C10 = 0.17473318; //K/A^10
  DOUBLE alpha = 10.43329537; //A^-1
  DOUBLE A = 1.8443101e5; //K
  DOUBLE beta = -2.27965105; //A^-2
  DOUBLE x;
  DOUBLE F;

  r /= rm;
  if(r<D)
    F = Exp(-(D/r-1.)*(D/r-1.));
  else
    F = 1.;

  F *= (C6 + (C8 + C10/(r*r))/(r*r))/(r*r*r*r*r*r);

  return epsilon*( A*Exp(-alpha*r + beta*r*r) -F)/energy_unit;

#endif
}

/****************************** pair distribution *****************************/
void MeasurePairDistribution(int w) { 
  int i, j, n, nx, ny;
  DOUBLE r, r2, dr[3];
  int PD_position;
  static int wait = 1;

#pragma omp atomic
  PD.times_measured++;

  if(MC == DIFFUSION && wait == OFF) { // do a pure measurement
#pragma omp atomic
    PD_pure.times_measured += Nwalkers;
    PD_position = W[w].PD_position;
    for(n=0; n<gridPD; n++) {
#pragma omp atomic
      PD_pure.N[n] += W[w].PD[PD_position][n]; // * W[w].weight;
      W[w].PD[PD_position][n] = 0;
#pragma omp atomic
      PDup_pure.N[n] += W[w].PDup[PD_position][n]; // * W[w].weight;
      W[w].PDup[PD_position][n] = 0;
#pragma omp atomic
      PDdn_pure.N[n] += W[w].PDdn[PD_position][n]; // * W[w].weight;
      W[w].PDdn[PD_position][n] = 0;
    }
    if(measure_PairDistrMATRIX) {
      for(nx=0; nx<gridPD_MATRIX; nx++) {
        for(ny=0; ny<gridPD_MATRIX; ny++) {
#pragma omp atomic
          PD_MATRIX11_pure.N[nx][ny] += W[w].PD_MATRIX11[PD_position][nx][ny];
          W[w].PD_MATRIX11[PD_position][nx][ny] = 0;
#pragma omp atomic
          PD_MATRIX12_pure.N[nx][ny] += W[w].PD_MATRIX12[PD_position][nx][ny];
          W[w].PD_MATRIX12[PD_position][nx][ny] = 0;
#pragma omp atomic
          PD_MATRIX22_pure.N[nx][ny] += W[w].PD_MATRIX22[PD_position][nx][ny];
          W[w].PD_MATRIX22[PD_position][nx][ny] = 0;
        }
      }
    }
  }

  // UP UP
  for(i=0; i<Nup; i++) {
    for(j=i+1; j<Nup; j++) {
      CaseX(dr[0] = W[w].x[i] - W[w].x[j]);
      CaseY(dr[1] = W[w].y[i] - W[w].y[j]);
      CaseZ(dr[2] = W[w].z[i] - W[w].z[j]);

      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef PD_MEASURE_ONLY_Z_DIRECTION
      r2 = dr[2]*dr[2];
#endif
      r = sqrt(r2);

      if(CheckInteractionCondition(dr[2], r2)) {
        n = (int) (r / PD.step);
        if(n>=PD.size) n = PD.size-1;
#pragma omp atomic
        PDup.N[n]++; // mixed estimator
        if(MC == DIFFUSION) W[w].PDup[W[w].PD_position][n]++; // pure estimator
      }

      if(measure_PairDistrMATRIX) {
#ifndef TRIAL_1D
        nx = (int) ((dr[0]+PD_MATRIX11.max) / (2.*PD_MATRIX11.step));
        ny = (int) ((dr[1]+PD_MATRIX11.max) / (2.*PD_MATRIX11.step));
        if(nx>=0 && ny>=0 && nx<gridPD_MATRIX && ny<gridPD_MATRIX) {
#pragma omp atomic
          PD_MATRIX11.N[nx][ny]++;
#pragma omp atomic
          PD_MATRIX11.times_measured++;
        }
        nx = (int) ((-dr[0]+PD_MATRIX11.max/2.) / (2.*PD_MATRIX11.step));
        ny = (int) ((-dr[1]+PD_MATRIX11.max/2.) / (2.*PD_MATRIX11.step));
        if(nx>=0 && ny>=0 && n<gridPD_MATRIX && ny<gridPD_MATRIX) {
#pragma omp atomic
          PD_MATRIX11.N[nx][ny]++;
#pragma omp atomic
          PD_MATRIX11.times_measured++;
        }
#else // trapped 1D in z (n(r_1) n(r_2))
        nx = (int) ((W[w].z[i]+PD_MATRIX11.max) / (2.*PD_MATRIX11.step));
        ny = (int) ((W[w].z[j]+PD_MATRIX11.max) / (2.*PD_MATRIX11.step));
        if(nx>=0 && ny>=0 && nx<gridPD_MATRIX && ny<gridPD_MATRIX) {
#pragma omp atomic
          PD_MATRIX11.N[nx][ny]++;
#pragma omp atomic
          PD_MATRIX11.times_measured++;
#pragma omp atomic
          PD_MATRIX11.N[ny][nx]++;
#pragma omp atomic
          PD_MATRIX11.times_measured++;
        }
#endif
      }
    }
  }

  // DOWN DOWN
  for(i=0; i<Ndn; i++) {
    for(j=i+1; j<Ndn; j++) {
      CaseX(dr[0] = W[w].xdn[i] - W[w].xdn[j]);
      CaseY(dr[1] = W[w].ydn[i] - W[w].ydn[j]);
      CaseZ(dr[2] = W[w].zdn[i] - W[w].zdn[j]);

      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef PD_MEASURE_ONLY_Z_DIRECTION
      r2 = dr[2]*dr[2];
#endif
      r = sqrt(r2);

      if(CheckInteractionCondition(dr[2], r2)) {
        n = (int) (r / PD.step);
        if(n>=PD.size) n = PD.size-1;
#pragma omp atomic
        PDdn.N[n]++; // mixed estimator
        if(MC == DIFFUSION) W[w].PDdn[W[w].PD_position][n]++; // pure estimator
        if(measure_effective_up_dn_potential) PDdn.f[n] += W[w].E; // store the effective potential
      }

      if(measure_PairDistrMATRIX) {
#ifndef TRIAL_1D
        nx = (int) ((dr[0]+PD_MATRIX22.max) / (2.*PD_MATRIX22.step));
        ny = (int) ((dr[1]+PD_MATRIX22.max) / (2.*PD_MATRIX22.step));
        if(nx>=0 && ny>=0 && nx<gridPD_MATRIX && ny<gridPD_MATRIX) {
#pragma omp atomic
          PD_MATRIX22.N[nx][ny]++;
#pragma omp atomic
          PD_MATRIX22.times_measured++;
        }
        nx = (int) ((-dr[0]+PD_MATRIX22.max/2.) / (2.*PD_MATRIX22.step));
        ny = (int) ((-dr[1]+PD_MATRIX22.max/2.) / (2.*PD_MATRIX22.step));
        if(nx>=0 && ny>=0 && n<gridPD_MATRIX && ny<gridPD_MATRIX) {
#pragma omp atomic
          PD_MATRIX22.N[nx][ny]++;
#pragma omp atomic
          PD_MATRIX22.times_measured++;
        }
#else // trapped 1D in z (n(r_1) n(r_2))
        nx = (int) ((W[w].zdn[i]+PD_MATRIX22.max) / (2.*PD_MATRIX22.step));
        ny = (int) ((W[w].zdn[j]+PD_MATRIX22.max) / (2.*PD_MATRIX22.step));
        if(nx>=0 && ny>=0 && nx<gridPD_MATRIX && ny<gridPD_MATRIX) {
#pragma omp atomic
          PD_MATRIX22.N[nx][ny]++;
#pragma omp atomic
          PD_MATRIX22.times_measured++;
#pragma omp atomic
          PD_MATRIX22.N[ny][nx]++;
#pragma omp atomic
          PD_MATRIX22.times_measured++;
        }
#endif
      }
    }
  }

  // UP DOWN
  for(i=0; i<Nup; i++) {
    for(j=0; j<Ndn; j++) {
      CaseX(dr[0] = W[w].x[i] - W[w].xdn[j]);
      CaseY(dr[1] = W[w].y[i] - W[w].ydn[j]);
      CaseZ(dr[2] = W[w].z[i] - W[w].zdn[j]);

      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef PD_MEASURE_ONLY_Z_DIRECTION
      r2 = dr[2]*dr[2];
#endif
      r = sqrt(r2);

      if(CheckInteractionCondition(dr[2], r2)) {
        n = (int) (r / PD.step);
        if(n>=PD.size) n = PD.size-1;
#pragma omp atomic
        PD.N[n]++; // mixed estimator
        if(MC == DIFFUSION) W[w].PD[W[w].PD_position][n]++; // pure estimator
      }
      if(measure_PairDistrMATRIX) {
#ifndef TRIAL_1D
        nx = (int) ((dr[0]+PD_MATRIX12.max) / (2.*PD_MATRIX12.step));
        ny = (int) ((dr[1]+PD_MATRIX12.max) / (2.*PD_MATRIX12.step));
        if(nx>=0 && ny>=0 && nx<gridPD_MATRIX && ny<gridPD_MATRIX) {
#pragma omp atomic
          PD_MATRIX12.N[nx][ny]++;
#pragma omp atomic
          PD_MATRIX12.times_measured++;
        }
        nx = (int) ((-dr[0]+PD_MATRIX12.max/2.) / (2.*PD_MATRIX12.step));
        ny = (int) ((-dr[1]+PD_MATRIX12.max/2.) / (2.*PD_MATRIX12.step));
        if(nx>=0 && ny>=0 && n<gridPD_MATRIX && ny<gridPD_MATRIX) {
#pragma omp atomic
          PD_MATRIX12.N[nx][ny]++;
#pragma omp atomic
          PD_MATRIX12.times_measured++;
        }
#else // trapped 1D in z (n(r_1) n(r_2))
        nx = (int) ((W[w].z[i]+PD_MATRIX12.max) / (2.*PD_MATRIX12.step));
        ny = (int) ((W[w].zdn[j]+PD_MATRIX12.max) / (2.*PD_MATRIX12.step));
        if(nx>=0 && ny>=0 && nx<gridPD_MATRIX && ny<gridPD_MATRIX) {
#pragma omp atomic
          PD_MATRIX12.N[nx][ny]++;
#pragma omp atomic
          PD_MATRIX12.times_measured++;
        }
#endif
      }
    }
  }

  if(MC == DIFFUSION) { // move current position
    W[w].PD_position++;
    if(W[w].PD_position == grid_pure_block) W[w].PD_position = 0;
  }

  if(MC == DIFFUSION && wait) { // skip the first block in PURE case
    wait++;
    if(wait == grid_pure_block +1) wait = OFF;
  }
}

/****************************** hyper radius **********************************/
void MeasureHyperRadius(void) {
  int w,i,j,k,n;
  DOUBLE r, r2, dr[3];
  int PD_position;
  static int wait = 1;
  DOUBLE x1,y1,z1,x2,y2,z2,x3,y3,z3;
  DOUBLE m1,m2,m3,mur1,mur2,mur3,l1,l2,l3;
  DOUBLE m_up, m_dn,mu,M;
  DOUBLE HR21, HR22, HR23;

  //HR.times_measured += Nwalkers;

  /*if(MC && wait == OFF) { // do a pure measurement
    HR_pure.times_measured += Nwalkers;
    for(w=0; w<Nwalkers; w++) {
      HR_position = W[w].HR_position;
      for(n=0; n<gridHR; n++) {
        HR_pure.N[n] += W[w].HR[HR_position][n]; // * W[w].weight;
        W[w].HR[HR_position][n] = 0;
        HRup_pure.N[n] += W[w].HRup[HR_position][n]; // * W[w].weight;
        W[w].HRup[HR_position][n] = 0;
        HRdn_pure.N[n] += W[w].HRdn[HR_position][n]; // * W[w].weight;
        W[w].HRdn[HR_position][n] = 0;
      }
    }
  }*/

  m_dn = 0.5*(1.+mass_ratio);   // m_dn = M/(2mu)
  m_up = 0.5*(1.+1./mass_ratio);// m_up = m/(2mu)
#ifdef UNIT_MASS_COMPONENT_UP
  m_dn /= m_up;
  m_up = 1.;
#endif
#ifdef UNIT_MASS_COMPONENT_DN
  m_up = mass_ratio;
  m_dn = 1.;
#endif
  m1 = m_up;
  m2 = m_dn;
  m3 = m_dn;

  M = m1+m2+m3;
  mu = sqrt(m1*m2*m3/M);
  mur1= m2*m3/(m2+m3);
  mur2= m1*m3/(m1+m3);
  mur3= m1*m2/(m1+m2);
  l1 = Sqrt(mu/mur1);
  l2 = Sqrt(mu/mur2);
  l3 = Sqrt(mu/mur3);

  for(w=0; w<Nwalkers; w++) {
    for(i=0; i<Nup; i++) { // 1 - light 1
      x1 = W[w].x[i];
      y1 = W[w].y[i];
      z1 = W[w].z[i];
      for(j=0; j<Ndn; j++) { // 2 - heavy 1
        x2 = W[w].xdn[j];
        y2 = W[w].ydn[j];
        z2 = W[w].zdn[j];
        for(k=j+1; k<Ndn; k++) { // 3 - heavy 2
          x3 = W[w].xdn[k];
          y3 = W[w].ydn[k];
          z3 = W[w].zdn[k];

          dr[0] = x1-x2;
          dr[1] = y1-y2;
          dr[2] = z1-z2;
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          //r2 = dr[0]*dr[0]+dr[1]*dr[1]+dr[2]*dr[2];
          HR23 = r2/(l3*l3);

          dr[0] = x2-x3;
          dr[1] = y2-y3;
          dr[2] = z2-z3;
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          //r2 = dr[0]*dr[0]+dr[1]*dr[1]+dr[2]*dr[2];
          HR21 = r2/(l1*l1);

          dr[0] = x1-x3;
          dr[1] = y1-y3;
          dr[2] = z1-z3;
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          //r2 = dr[0]*dr[0]+dr[1]*dr[1]+dr[2]*dr[2];
          HR22 = r2/(l2*l2);

          dr[0] = x1 - (m2*x2+m3*x3)/(m2+m3);
          dr[1] = y1 - (m2*y2+m3*y3)/(m2+m3);
          dr[2] = z1 - (m2*z2+m3*z3)/(m2+m3);
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          //r2 = dr[0]*dr[0]+dr[1]*dr[1]+dr[2]*dr[2];
          HR21 += r2*l1*l1;

          dr[0] = x2 - (m1*x1+m3*x3)/(m1+m3);
          dr[1] = y2 - (m1*y1+m3*y3)/(m1+m3);
          dr[2] = z2 - (m1*z1+m3*z3)/(m1+m3);
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          //r2 = dr[0]*dr[0]+dr[1]*dr[1]+dr[2]*dr[2];
          HR22 += r2*l2*l2;

          dr[0] = x3 - (m2*x2+m1*x1)/(m2+m1);
          dr[1] = y3 - (m2*y2+m1*y1)/(m2+m1);
          dr[2] = z3 - (m2*z2+m1*z1)/(m2+m1);
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          //r2 = dr[0]*dr[0]+dr[1]*dr[1]+dr[2]*dr[2];
          HR23 += r2*l3*l3;

          r = sqrt(HR21);
          r = sqrt(HR22);
          r = sqrt(HR23);
          n = (int) (r / HR.step);
          //if(n>=HR.size) n = HR.size-1;
          if(n<HR.size) {
            //if(fabs(HR21/HR22-1.)>1e-3)
            //    Message("%lf\n%lf\n", sqrt(HR21), sqrt(HR22), sqrt(HR23));
            HR.N[n]++; // mixed estimator
            HR.f[n] += 1./(r*r*r*r*r);
          }
          //if(MC) W[w].HRup[W[w].HR_position][n]++; // pure estimator
          HR.times_measured++;
        }
      }
    }
  }
  /*  if(MC) { // move current position
      W[w].HR_position++;
      if(W[w].HR_position == grid_pure_block) W[w].HR_position = 0;
    }
  }*/
  if(MC && wait) { // skip the first block in PURE case
    wait++;
    if(wait == grid_pure_block +1) wait = OFF;
  }
}

/**************************** Lindemann  *********************************/
void MeasureLindemannRatio(void) {
#ifdef CRYSTAL
  int w,i;
  DOUBLE dx,dy,dz;

  FILE *out;
  static int time = 0;

  for(w=0; w<Nwalkers; w++) {
    for(i=0; i<Nup; i++) {
      dx = W[w].x[i] - Crystal.x[i];
      dy = W[w].y[i] - Crystal.y[i];
      dz = W[w].z[i] - Crystal.z[i];
      LindemannRatioF += dx*dx+dy*dy+dz*dz;
      LindemannRatioN++;
    }
    for(i=0; i<Ndn; i++) {
      dx = W[w].xdn[i] - Crystal.x[i];
      dy = W[w].ydn[i] - Crystal.y[i];
      dz = W[w].zdn[i] - Crystal.z[i];
      LindemannRatioF += dx*dx+dy*dy+dz*dz;
      LindemannRatioN++;
    }
  }
#else
  Warning("  Can not measure Lindemann ratio in absence of a crystal");
#endif
}

/**************************** radial distribution *****************************/
void MeasureRadialDistribution(void) {
  int w;
  //static int i=0;
  static int first_time = ON;

  //if(++i != Nmeasure) return;

  if(first_time == OFF && MC == DIFFUSION) SaveMeanR2DMC();
  //i=0;

  RD.r2recent = 0.;

  for(w=0; w<Nwalkers; w++) {
    MeasureRadialDistributionWalker(&W[w]);
  }

  RD.r2recent = sqrt(RD.r2recent / (DOUBLE) (Nwalkers * Nmean));
}

void MeasureRadialDistributionWalker(struct Walker *W) {
  int i, n, index;
  DOUBLE r2, r, R2, Z2;
  DOUBLE CM[3];
  R2 = Z2 = 0.;
#pragma omp atomic
  RD.times_measured++;

  if(MC == DIFFUSION && W->RD_wait == OFF) { // do a pure measurement
#pragma omp atomic
    RDup_pure.times_measured++;
#pragma omp atomic
    RDdn_pure.times_measured++;
    for(n=0; n<RD.size; n++) {
#pragma omp atomic
      RDup_pure.N[n] += W->RDup[W->RD_position][n]; // * W[w].weight;
      W->RDup[W->RD_position][n] = 0;
#pragma omp atomic
      RDdn_pure.N[n] += W->RDdn[W->RD_position][n]; // * W[w].weight;
      W->RDdn[W->RD_position][n] = 0;
    }
  }

  CM[0] = CM[1] = CM[2] = 0.;
#ifdef CENTER_OF_MASS_SUBTRACTED_IN_RADIAL_DISTRIBUTION
  for(i=0; i<Nup; i++) {
    CaseX(CM[0] += W->x[i]);
    CaseY(CM[1] += W->y[i]);
    CaseZ(CM[2] += W->z[i]);
  }
  for(i=0; i<Ndn; i++) {
    CaseX(CM[0] += W->xdn[i]);
    CaseY(CM[1] += W->ydn[i]);
    CaseZ(CM[2] += W->zdn[i]);
  }
  CM[0] = CM[0]/(DOUBLE)(Nup+Ndn);
  CM[1] = CM[1]/(DOUBLE)(Nup+Ndn);
  CM[2] = CM[2]/(DOUBLE)(Nup+Ndn);
#endif

  // component A
  for(i=0; i<Nup; i++) {
    r2 = (W->x[i]-CM[0])*(W->x[i]-CM[0]) + (W->y[i]-CM[1])*(W->y[i]-CM[1]) + (W->z[i]-CM[2])*(W->z[i]-CM[2]);
    r = sqrt(r2);
#pragma omp atomic
    RD.r2 += r2;

    index = (int)(RD.size*fabs(W->x[i] - CM[0])/RDxup.max);
    if(index>=0 && index<RD.size) 
#pragma omp atomic
      RDxup.N[index]++;

    index = (int)(RD.size*fabs(W->y[i] - CM[1])/RDyup.max);
    if(index>=0 && index<RD.size) 
#pragma omp atomic
      RDyup.N[index]++;

    index = (int)(RD.size*fabs(W->z[i] - CM[2])/RDzup.max);
    if(index>=0 && index<RD.size) {
#pragma omp atomic
      RDzup.N[index]++;
    if(MC == DIFFUSION) 
#pragma omp atomic
      W->RDup[W->RD_position][index]++; // pure estimator
    }

#pragma omp atomic
    RD.r2recent += r2;
    R2 += r2;
    if(fabs(W->z[i])< RD.width) {
      n = (int) (r/RD.step);
      if(n>=RD.size) n = RD.size-1;
#pragma omp atomic
      RD.N[n]++;
    }
  }

  // component B
  for(i=0; i<Ndn; i++) {
    r2 = (W->xdn[i]-CM[0])*(W->xdn[i]-CM[0]) + (W->ydn[i]-CM[1])*(W->ydn[i]-CM[1]) + (W->zdn[i]-CM[2])*(W->zdn[i]-CM[2]);
    r = sqrt(r2);
#pragma omp atomic
    RD.r2 += r2;

    index = (int)(RD.size*fabs(W->xdn[i] - CM[0])/RDxdn.max);
    if(index>=0 && index<RD.size) 
#pragma omp atomic
    RDxdn.N[index]++;

    index = (int)(RD.size*fabs(W->ydn[i] - CM[1])/RDydn.max);
    if(index>=0 && index<RD.size) 
#pragma omp atomic
    RDydn.N[index]++;

    index = (int)(RD.size*fabs(W->zdn[i] - CM[2])/RDzdn.max);
    if(index>=0 && index<RD.size) {
#pragma omp atomic
      RDzdn.N[index]++;
      if(MC == DIFFUSION) {
#pragma omp atomic
        W->RDdn[W->RD_position][index]++; // pure estimator
      }
    }
  }

  if(measure_R2) {
    W->r2old = W->r2;
    W->z2old = W->z2;
    W->r2 = R2 / (DOUBLE) Nup;
    W->z2 = Z2 / (DOUBLE) Nup;
  }

  if(MC == DIFFUSION) { // move current position
    W->RD_position++;
    if(W->RD_position == grid_pure_block) W->RD_position = 0;
    if(W->RD_wait) { // skip the first block in PURE case
      W->RD_wait++;
      if(W->RD_wait == grid_pure_block+1) {
        W->RD_wait = OFF;
      }
    }
  }
}

void MeasureFluctuations(struct Walker *Walker) {
  int i;
  DOUBLE r2, r, R2, Z2;
  static int initialized = OFF;
  static DOUBLE kf;
  //FILE *out;
  FILE *out1;
  FILE *out2;

  R2 = Z2 = 0.;
#pragma omp atomic
  RD.times_measured++;

  out1 = fopen("outrdup.dat", (initialized || file_append)?"a":"w");
  out2 = fopen("outrddn.dat", (initialized || file_append)?"a":"w");
  for(i=0; i<Nup; i++) {
#pragma omp atomic
    RDxup.N[(int) (Walker->x[i] / RD.step)]++;
#pragma omp atomic
    RDyup.N[(int) (Walker->y[i] / RD.step)]++;
#pragma omp atomic
    RDzup.N[(int) (Walker->z[i] / RD.step)]++;
    r2 = (Walker->x[i]-Lhalf)*(Walker->x[i]-Lhalf) + (Walker->y[i]-Lhalf)*(Walker->y[i]-Lhalf) + (Walker->z[i]-Lhalf)*(Walker->z[i]-Lhalf);
    r = sqrt(r2);
    //if(r<Lhalf) 
    fprintf(out1, "%"LF"\n", kf*r);
  }
  for(i=0; i<Ndn; i++) {
#pragma omp atomic
    RDxdn.N[(int) (Walker->xdn[i]/RD.step)]++;
#pragma omp atomic
    RDydn.N[(int) (Walker->ydn[i]/RD.step)]++;
#pragma omp atomic
    RDzdn.N[(int) (Walker->zdn[i]/RD.step)]++;
    r2 = (Walker->xdn[i]-Lhalf)*(Walker->xdn[i]-Lhalf) + (Walker->ydn[i]-Lhalf)*(Walker->ydn[i]-Lhalf) + (Walker->zdn[i]-Lhalf)*(Walker->zdn[i]-Lhalf);
    r = sqrt(r2);
    //if(r<Lhalf) 
    fprintf(out2, "%"LF"\n", kf*r);
  }
  fclose(out1);
  fclose(out2);

  //out = fopen("rdtimes.dat", "w");
  //fprintf(out, "%li", times++);
  //fclose(out);
  initialized = ON;
}

/******************************** OBDM ***************************************/
// Note that only SPIN UP particle is moved!
// McMillan calculation
// the contribution of particle i is: Exp(u_m - u_im[i] - u_ij[i])
// u_ij(i) = sum_j u(|r_i - r_j|)
// u_im(i) = u(|r_M - r_i|)
// u_m = sum_j u(|r_m - r_j|)
int MeasureOBDM(void) {
  int w,i,j,m, index;
  DOUBLE u_m;
  DOUBLE xm = 0.;
  DOUBLE ym = 0.;
  DOUBLE zm = 0.;
  DOUBLE u, r, r2, dr[3];
  DOUBLE q;
#ifdef BC_ABSENT
  int N1, N2;
#endif
#ifdef ONE_BODY_TRIAL_TERMS
  DOUBLE U_one_body_old = 0;
  DOUBLE U_one_body_new = 0;
#endif
#ifdef CRYSTAL // symmetric crystal contribution
#ifdef CRYSTAL_SYMMETRIC
#  define OBDM_CALCULATE_CRYSTAL_SYMMETRIC
#endif
#endif

  int N;

  if(Ndn == 1) return 1; // Residue is measured in a different place

  if(Nup != Ndn) Error("Measurement of OBDM is not implemented in the unbalaned case!");
  N = Nup;

  // fill u_ij = sum_j u(|r_i - r_j|)
  for(w=0; w<Nwalkers; w++) {
    if(var_par_array) AdjustVariationalParameter(W[w].varparindex);

#ifdef CRYSTAL // symmetric crystal contribution
#ifdef CRYSTAL_SYMMETRIC
    for(i=0; i<N; i++) {
      wp.x[i] = W[w].x[i];
      wp.y[i] = W[w].y[i];
      wp.z[i] = W[w].z[i];
    }
    U_one_body_old = OneBodyUWalker(wp);
#endif
#endif

#ifdef FERMIONS
    if(MC==1) { // in VMC case the Inverse matrix is already updated!
      // Fill Determinant Matrix
      for(i=0; i<N; i++) {
        for(j=0; j<N; j++) {
          dr[0] = W[w].x[i] - W[w].xdn[j];
          dr[1] = W[w].y[i] - W[w].ydn[j];
          dr[2] = W[w].z[i] - W[w].zdn[j];
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          Determinant[w][i][j] = orbitalF(sqrt(r2));
#ifdef ORBITAL_ANISOTROPIC_ABSOLUTE
  Error("ORBITAL_ANISOTROPIC_ABSOLUTE not implemented!");
#endif
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
          Determinant[w][i][j] += orbitalDescreteBCS_F(dr[0], dr[1], dr[2]);
#endif
        }
      }
      LUinv(Determinant[w], DeterminantInv[w], N);
      W[w].Determinant = determinantLU;
    }
#endif

    // initialize arrays with zeros
    for(i=0; i<N; i++) u_ij[i] = 0;
    // Note that only SPIN UP particle is moved!
#ifdef JASTROW_SAME_SPIN
    for(i=0; i<N; i++) {
      for(j=i+1; j<N; j++) {
        dr[0] = W[w].x[i] - W[w].x[j];
        dr[1] = W[w].y[i] - W[w].y[j];
        dr[2] = W[w].z[i] - W[w].z[j];

        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);

        u = InterpolateU11(&G11, sqrt(r2));
        u_ij[i] += u;
        u_ij[j] += u;
      }
    }
#endif
#ifdef JASTROW_OPPOSITE_SPIN
    for(i=0; i<N; i++) {
      for(j=0; j<N; j++) {
        dr[0] = W[w].x[i] - W[w].xdn[j];
        dr[1] = W[w].y[i] - W[w].ydn[j];
        dr[2] = W[w].z[i] - W[w].zdn[j];

        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);

        u = InterpolateU12(&G12, sqrt(r2));
        u_ij[i] += u;
      }
    }
#endif

    // Generate McMillan point McMillan_points times
    for(m=0; m<McMillan_points; m++) {
#ifdef BC_ABSENT
      CaseX(xm = (1.-2.*Random())*OBDM.max);
      CaseY(ym = (1.-2.*Random())*OBDM.max);
      CaseZ(zm = (1.-2.*Random())*OBDM.max);
#else // PBC
      CaseX(xm = Random()*L);
      CaseY(ym = Random()*L);
      CaseZ(zm = Random()*L);
#endif
      u_m = 0;
      //u_m += UOneBodyParticle(xm, ym, zm);

      // fill u_m and u_im
      for(i=0; i<N; i++) {
#ifdef JASTROW_SAME_SPIN
        CaseX(dr[0] = W[w].x[i] - xm);
        CaseY(dr[1] = W[w].y[i] - ym);
        CaseZ(dr[2] = W[w].z[i] - zm);
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
        u_mi[i] = InterpolateU11(&G11, sqrt(r2));
        u_m += u_mi[i];
#endif
#ifdef JASTROW_OPPOSITE_SPIN // different spin
        CaseX(dr[0] = W[w].xdn[i] - xm);
        CaseY(dr[1] = W[w].ydn[i] - ym);
        CaseZ(dr[2] = W[w].zdn[i] - zm);
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef JASTROW_SAME_SPIN // avoid erasing data
        q = InterpolateU12(&G12, sqrt(r2));
        u_mi[i] += q;
        u_m += q;
#else
        u_mi[i] = InterpolateU12(&G12, sqrt(r2));
        u_m += u_mi[i];
#endif
#endif
      }

      for(i=0; i<N; i++) {
        // calculate the displacement
        CaseX(dr[0] = W[w].x[i] - xm);
        CaseY(dr[1] = W[w].y[i] - ym);
        CaseZ(dr[2] = W[w].z[i] - zm);
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
        r = sqrt(r2);
        index = (int) (r/OBDM.step);
        // we move i-th particle to the point rm
        // Jastrow and external field contribution
#ifdef CRYSTAL
        Wp[w].x[i] = xm;
        Wp[w].y[i] = ym;
        Wp[w].z[i] = zm;
        U_one_body_new = OneBodyUWalker(Wp[w], ON, OFF);
        U_one_body_old = OneBodyUWalker(W[w], ON, OFF);
        // calculate Jastrow and Crystal contribution
        u = Exp(u_m - u_mi[i] - u_ij[i] + U_one_body_new - U_one_body_old);
#else
        u = Exp(u_m - u_mi[i] - u_ij[i]);
#endif

#ifdef ONE_BODY_TRIAL_TERMS // one-body terms are present
#ifdef OBDM_CALCULATE_CRYSTAL_SYMMETRIC // cannot be presented as a one-body contribution
    // Note that only SPIN UP particle is moved!
          wp.x[i] = xm;
          wp.y[i] = ym;
          wp.z[i] = zm;
          U_one_body_new = OneBodyUWalker(wp);
          wp.x[i] = W[w].x[i];
          wp.y[i] = W[w].y[i];
          wp.z[i] = W[w].z[i];

          // calculate Jastrow and Crystal contribution
          u = Exp(u_m - u_mi[i] - u_ij[i] + U_one_body_new - U_one_body_old);
#else // can be presented as a one-body contribution
    // Note that only SPIN UP particle is moved!
          u = Exp(u_m - u_mi[i] - u_ij[i] + OneBodyU(xm, ym, zm, i, ON) - OneBodyU(W[w].x[i], W[w].y[i], W[w].z[i], i, ON));
#endif
#else // one-body terms are absent
          u = Exp(u_m - u_mi[i] - u_ij[i]);
#endif

#ifdef FERMIONS
        // Determinant contribution
        q = 0.;
        for(j=0; j<N; j++) {
          dr[0] = xm - W[w].xdn[j];
          dr[1] = ym - W[w].ydn[j];
          dr[2] = zm - W[w].zdn[j];
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
          q += DeterminantInv[w][i][j] * (orbitalF(sqrt(r2)) + orbitalDescreteBCS_F(dr[0], dr[1], dr[2]));
#else
          q += DeterminantInv[w][i][j] * orbitalF(sqrt(r2));
#endif
        }
        u *= q;
#endif

        if(index<OBDM.size) {
          if(W[w].status == ALIVE || W[w].status == KILLED) {
            // one-body correlation function
            OBDM.N[index] += W[w].weight;
            OBDM.f[index] += u * W[w].weight;

            if(MC == VARIATIONAL && W[w].weight != 1.) Error("wrong weight in variational calculation");
#ifdef TRIAL_1D
/*#ifdef BC_ABSENT // reduced one-body correlation function
            N1 = (int) (fabs(W[w].z[i]) / RDz.step);
            N2 = (int) (fabs(zm) / RDz.step);
            if(N1 < RDz.size && N2 < RDz.size) {
              OBDMtrap.N[index] += sqrt(RDz.f[N1]*RDz.f[N2]);
              OBDMtrap.f[index] += u;
            }
#endif*/
#endif
          }
        }
        if(W[w].status == ALIVE || W[w].status == KILLED) { // measure Nk
          for(j=0; j<OBDM.Nksize; j++) {
            OBDM.Nk[j] += Cos(OBDM.k[j]*r)*u*W[w].weight;
          }
          OBDM.Nktimes_measured++;
        }
      }
    }
  }
  return 0;
}

/******************************** OBDM ***************************************/
int MeasureOBDM1D(void) {
  int w,i,j,m, n;
  DOUBLE xm, ym, zm;
  DOUBLE u, r, r2, dr;
  int s;

  xm = ym = zm = 0.; // initialize with zeros

  for(w=0; w<Nwalkers; w++) { // fill u_ij
    for(m=0; m<McMillan_points; m++) { // Generate McMillan point McMillan_points times

#ifdef BC_ABSENT
      zm = 2.*OBDM.max*(0.5-Random());
#else
      zm = Random()*L;
#endif

      for(i=0; i<Nup; i++) { // do measurement for up -> up OBDM
        dr = W[w].z[i] - zm;
        FindNearestImage1D(r2, dr);
        r = sqrt(r2);
        n = (int) (r/OBDM.step);

        u = 0.;
        for(j=0; j<Nup; j++) {
          if(j != i) {
            dr = W[w].z[j] - zm;
            FindNearestImage1D(r2, dr);
            u += InterpolateU11(&G11, Sqrt(r2));
            dr = W[w].z[j] - W[w].z[i];
            FindNearestImage1D(r2, dr);
            u -= InterpolateU11(&G11, Sqrt(r2));
          }
        }
        for(j=0; j<Ndn; j++) {
          dr = W[w].zdn[j] - zm;
          FindNearestImage1D(r2, dr);
          u += InterpolateU12(&G12, Sqrt(r2));
          dr = W[w].zdn[j] - W[w].z[i];
          FindNearestImage1D(r2, dr);
          u -= InterpolateU12(&G12, Sqrt(r2));
       }
#ifdef ONE_BODY_TRIAL_TERMS // one-body terms are present
       u = Exp(u + OneBodyU(xm, ym, zm, i, ON) - OneBodyU(W[w].x[i], W[w].y[i], W[w].z[i], i, ON));
#else // one-body terms are absent
       u = Exp(u);
#endif

        s = 1;
        /*for(j=0; j<Ndn; j++) {??? IFG type 1-2 interaction
          if(W[w].z[i]<W[w].zdn[j]) s *= -1;
          if(zm<W[w].zdn[j]) s *= -1;
        }*/
        for(j=0; j<Nup; j++) {
          if(j!=i) {
            if(W[w].z[i]<W[w].z[j]) s *= -1;
            if(zm<W[w].z[j]) s *= -1;
          }
        }

        if(W[w].status == ALIVE || W[w].status == KILLED) { // measure Nk
          for(j=0; j<OBDM.Nksize; j++) {
            OBDM.Nk[j] += Cos(OBDM.k[j]*r)*u*s*W[w].weight;
          }
          OBDM.Nktimes_measured++;
#ifdef OBDM_BOSONS
          for(j=0; j<OBDM.Nksize; j++) {
            OBDMbosons.Nk[j] += Cos(OBDM.k[j]*r)*u*W[w].weight;
          }
          OBDMbosons.Nktimes_measured++;
#endif
        }

        if(n<OBDM.size && (W[w].status == ALIVE || W[w].status == KILLED)) { // measure OBDM
          OBDM.N[n] += W[w].weight;
          OBDM.f[n] += u * s * W[w].weight;
#ifdef OBDM_BOSONS
          OBDMbosons.N[n] += W[w].weight;
          OBDMbosons.f[n] += u * W[w].weight;
#endif
        }
      }
    }
  }

  // do measurement for down -> down OBDM
  for(w=0; w<Nwalkers; w++) { // fill u_ij
    for(i=0; i<Ndn; i++) u_ij[i] = 0; // initialize arrays with zeros
    for(i=0; i<Ndn; i++) {
      for(j=i+1; j<Ndn; j++) {
        dr = W[w].zdn[i] - W[w].zdn[j];
        FindNearestImage1D(r2, dr);
        u = InterpolateU22(&G22, sqrt(r2));
        u_ij[i] += u;
        u_ij[j] += u;
      }
    }

    for(m=0; m<McMillan_points; m++) { // Generate McMillan point McMillan_points times
#ifdef BC_ABSENT
      zm = 2.*OBDM.max*(0.5-Random());
#else
      zm = Random()*L;
#endif

      for(i=0; i<Ndn; i++) {
        dr = W[w].zdn[i] - zm;
        FindNearestImage1D(r2, dr);
        r = sqrt(r2);
        n = (int) (r/OBDM.step);

        u = 0.;
        for(j=0; j<Ndn; j++) {
          if(j != i) {
            dr = W[w].zdn[j] - zm;
            FindNearestImage1D(r2, dr);
            u += InterpolateU22(&G22, Sqrt(r2));
            dr = W[w].zdn[j] - W[w].zdn[i];
            FindNearestImage1D(r2, dr);
            u -= InterpolateU22(&G22, Sqrt(r2));
          }
        }
        for(j=0; j<Nup; j++) {
          dr = W[w].z[j] - zm;
          FindNearestImage1D(r2, dr);
          u += InterpolateU12(&G12, Sqrt(r2));
          dr = W[w].z[j] - W[w].zdn[i];
          FindNearestImage1D(r2, dr);
          u -= InterpolateU12(&G12, Sqrt(r2));
       }
#ifdef ONE_BODY_TRIAL_TERMS // one-body terms are present
       u = Exp(u + OneBodyU(xm, ym, zm, i, OFF) - OneBodyU(W[w].xdn[i], W[w].ydn[i], W[w].zdn[i], i, OFF));
#else // one-body terms are absent
       u = Exp(u);
#endif

        s = 1;
        /*for(j=0; j<Nup; j++) {
          if(W[w].zdn[i]<W[w].z[j]) s *= -1;
          if(zm<W[w].z[j]) s *= -1;
        }*/
        for(j=0; j<Ndn; j++) {
          if(j!=i) {
            if(W[w].zdn[i]<W[w].zdn[j]) s *= -1;
            if(zm<W[w].zdn[j]) s *= -1;
          }
        }

        if(W[w].status == ALIVE || W[w].status == KILLED) { // measure Nk
          for(j=0; j<OBDM.Nksize; j++) {
            OBDM22.Nk[j] += Cos(OBDM.k[j]*r)*u*s*W[w].weight;
          }
          OBDM22.Nktimes_measured++;
        }

        if(n<OBDM.size && (W[w].status == ALIVE || W[w].status == KILLED)) { // measure g1
          OBDM22.N[n] += W[w].weight;
          OBDM22.f[n] += u*s*W[w].weight;
#ifdef OBDM_BOSONS
          OBDMbosons22.N[n] += W[w].weight;
          OBDMbosons22.f[n] += u*W[w].weight;
          for(j=0; j<OBDM.Nksize; j++) {
            OBDMbosons22.Nk[j] += Cos(OBDM.k[j]*r)*u*W[w].weight;
          }
          OBDMbosons22.Nktimes_measured++;
#endif
        }
      }
    }
  }

  return 0;
}

int MeasureOBDMresidue(int w) {
// calculates OBDM for spin down particle (residue)
  int i,j,m,index;

  DOUBLE u_m;
  DOUBLE xm = 0.;
  DOUBLE ym = 0.;
  DOUBLE zm = 0.;
  DOUBLE r,u,r2, dr[3];
  DOUBLE u_old = 0.;

  j = 0; // Note that only SPIN DOWN 1st particle is moved
  //for(w=0; w<Nwalkers; w++) {
  u_old = 0.; //u_old += UOneBodyParticle(W[w].xdn[j], W[w].ydn[j], W[w].zdn[j]);
#ifdef JASTROW_OPPOSITE_SPIN
  for(i=0; i<Nup; i++) {
    dr[0] = W[w].x[i] - W[w].xdn[j];
    dr[1] = W[w].y[i] - W[w].ydn[j];
    dr[2] = W[w].z[i] - W[w].zdn[j];
    FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
    u_old += InterpolateU12(&G12, sqrt(r2));
  }
#endif

  // Generate McMillan point McMillan_points times
  for(m=0; m<McMillan_points; m++) { // we move 1st down particle to the point rm
    CaseX(xm = Random()*L);
    CaseY(ym = Random()*L);
    CaseZ(zm = Random()*L);

    u_m = 0; //u_m += UOneBodyParticle(xm, ym, zm);
    for(i=0; i<Nup; i++) {
#ifdef JASTROW_OPPOSITE_SPIN // different spin
      CaseX(dr[0] = W[w].x[i] - xm);
      CaseY(dr[1] = W[w].y[i] - ym);
      CaseZ(dr[2] = W[w].z[i] - zm);
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      u_m += InterpolateU12(&G12, sqrt(r2));
#endif
    }

    // calculate the displacement distance
    CaseX(dr[0] = W[w].xdn[j] - xm);
    CaseY(dr[1] = W[w].ydn[j] - ym);
    CaseZ(dr[2] = W[w].zdn[j] - zm);
    FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
    index = (int) (sqrt(r2)/OBDM.step);

    u = Exp(u_m - u_old);

    if(index<OBDM.size) {
      if(W[w].status == ALIVE || W[w].status == KILLED) { // one-body correlation function
#pragma omp atomic
        OBDM.N[index] += W[w].weight;
#pragma omp atomic
        OBDM.f[index] += u * W[w].weight;
      }
    }
  }

  return 0;
}

/************************* Measure Momentum Distribuion **********************/
int MeasureMomentumDistribution(void) {
  int w,i,j,m, n;
  DOUBLE du;
  DOUBLE xm, ym, zm;
  DOUBLE u, r2, dr[3];
#ifdef BC_ABSENT
  int N1, N2;
#endif
  int m_number;
  DOUBLE dr1,dr2;
  int up;
#ifdef CRYSTAL
  DOUBLE UCrystalOldUp, UCrystalOldDn;
#endif
#ifdef FERMIONS
  DOUBLE q;
#endif
#ifdef TRIAL_3D
  int plane;
#endif
  int N;

  N = Nup;

  if(Nup != Ndn) {
    Warning("unpolarized case in n(k)\n exiting\n");
    return 1;
  }

  xm = ym = zm = 0.; // initialize with zeros for low dimensions
  dr1 = dr2 = 0.;

  if(McMillanTBDM_points != 1) {
    Warning("Adjusting McMillanTBDM_points=1\n");
    McMillanTBDM_points = 1;
  }

#ifdef BC_ABSENT // effect of the external field
  Error("Measurement of the momentum distribution is not implemented in a trapped case!");
#endif

  for(w=0; w<Nwalkers; w++) {
    if(var_par_array) AdjustVariationalParameter(W[w].varparindex);

#ifdef FERMIONS
    if(MC==1) { // in VMC case the Inverse matrix is already updated!
      // Fill Determinant Matrix
      for(i=0; i<N; i++) {
        for(j=0; j<N; j++) {
          dr[0] = W[w].x[i] - W[w].xdn[j];
          dr[1] = W[w].y[i] - W[w].ydn[j];
          dr[2] = W[w].z[i] - W[w].zdn[j];
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          Determinant[w][i][j] = orbitalF(sqrt(r2));
#ifdef ORBITAL_ANISOTROPIC_ABSOLUTE
  Error("ORBITAL_ANISOTROPIC_ABSOLUTE not implemented!");
#endif
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
          Determinant[w][i][j] += orbitalDescreteBCS_F(dr[0], dr[1], dr[2]);
#endif
        }
      }
      LUinv(Determinant[w], DeterminantInv[w], N);
      W[w].Determinant = determinantLU;
    }
#endif // FERMIONS

    for(i=0; i<Nmax; i++) u_ij[i] = 0; // initialize arrays with zeros

#ifdef JASTROW_SAME_SPIN // fill u_ij = sum_j u(|r_i - r_j|)
    for(i=0; i<Nup; i++) {
      for(j=i+1; j<Nup; j++) {
        dr[0] = W[w].x[i] - W[w].x[j];
        dr[1] = W[w].y[i] - W[w].y[j];
        dr[2] = W[w].z[i] - W[w].z[j];
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
        u = InterpolateU11(&G11, sqrt(r2));
        u_ij[i] += u; // u_ij -> up
        u_ij[j] += u;
      }
    }
    for(i=0; i<Ndn; i++) {
      for(j=i+1; j<Ndn; j++) {
        dr[0] = W[w].xdn[i] - W[w].xdn[j];
        dr[1] = W[w].ydn[i] - W[w].ydn[j];
        dr[2] = W[w].zdn[i] - W[w].zdn[j];
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
        u = InterpolateU22(&G22, sqrt(r2));
        u_mi[i] += u; // u_mi -> down
        u_mi[j] += u;
      }
    }
#endif
#ifdef JASTROW_OPPOSITE_SPIN
    for(i=0; i<Nup; i++) {
      for(j=0; j<Ndn; j++) {
        dr[0] = W[w].x[i] - W[w].xdn[j];
        dr[1] = W[w].y[i] - W[w].ydn[j];
        dr[2] = W[w].z[i] - W[w].zdn[j];
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
        u = InterpolateU12(&G12, sqrt(r2));
        u_ij[i] += u;
        u_mi[j] += u;
      }
    }
#endif

#ifdef CRYSTAL
    for(i=0; i<N; i++) {
      Wp[w].x[i] = W[w].x[i];
      Wp[w].y[i] = W[w].y[i];
      Wp[w].z[i] = W[w].z[i];
      Wp[w].xdn[i] = W[w].xdn[i];
      Wp[w].ydn[i] = W[w].ydn[i];
      Wp[w].zdn[i] = W[w].zdn[i];
    }
    UCrystalOldUp = OneBodyUWalker(Wp[w], ON, OFF);
    UCrystalOldDn = OneBodyUWalker(Wp[w], OFF, ON);
#endif

    // Generate McMillan point McMillan_points times
    for(m=0; m<McMillan_points; m++) {
      up = rand() % 2;    // choose if particle (1) up or (0) down is displaced
      if(up) // choose particle to displace
        m_number = (int) (Random()*Nup);
      else
        m_number = (int) (Random()*Ndn);

#ifdef TRIAL_3D
      plane = rand() % 3; // choose which plane is measured

      if(plane == 1) { // xy plane
        plane = 2;
        xm = Random()*L;
        ym = Random()*L;
        if(up) {
          zm  = W[w].z[m_number];
          dr1 = W[w].x[m_number] - xm;
          dr2 = W[w].y[m_number] - ym;
        }
        else {
          zm  = W[w].zdn[m_number];
          dr1 = W[w].xdn[m_number] - xm;
          dr2 = W[w].ydn[m_number] - ym;
        }
      }
      else if(plane == 2) { // yz plane
        plane = 3;
        ym = Random()*L;
        zm = Random()*L;
        if(up) {
          xm  = W[w].x[m_number];
          dr1 = W[w].y[m_number] - ym;
          dr2 = W[w].z[m_number] - zm;
        }
        else {
          xm  = W[w].xdn[m_number];
          dr1 = W[w].ydn[m_number] - ym;
          dr2 = W[w].zdn[m_number] - zm;
        }
      }
      else { // xz plane
        plane = 1;
        xm = Random()*L;
        zm = Random()*L;
        if(up) {
          ym  = W[w].y[m_number];
          dr1 = W[w].x[m_number] - xm;
          dr2 = W[w].z[m_number] - zm;
        }
        else {
          ym  = W[w].ydn[m_number];
          dr1 = W[w].xdn[m_number] - xm;
          dr2 = W[w].zdn[m_number] - zm;
        }
      }
#endif // TRIAL_3D

#ifdef TRIAL_1D
      zm = Random()*L;
      if(up) {
        dr1 = W[w].z[m_number] - zm;
      }
      else {
        dr1 = W[w].zdn[m_number] - zm;
      }
#endif // TRIAL_1D

      if(up) {
        dr[0] = W[w].x[m_number] - xm;
        dr[1] = W[w].y[m_number] - ym;
        dr[2] = W[w].z[m_number] - zm;
      }
      else {
        dr[0] = W[w].xdn[m_number] - xm;
        dr[1] = W[w].ydn[m_number] - ym;
        dr[2] = W[w].zdn[m_number] - zm;
      }

      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      n = (int) (sqrt(r2)/OBDM.step);

      if(up)
        du = -u_ij[m_number];
      else
        du = -u_mi[m_number];

#ifdef JASTROW_SAME_SPIN // Jastrow contribution
      if(up) {
        for(i=0; i<Nup; i++) {
          if(i != m_number) {
            dr[0] = W[w].x[i] - xm;
            dr[1] = W[w].y[i] - ym;
            dr[2] = W[w].z[i] - zm;
          }
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          du += InterpolateU11(&G11, sqrt(r2));
        }
      }
      else{
        for(i=0; i<Ndn; i++) {
          if(i != m_number) {
            dr[0] = W[w].xdn[i] - xm;
            dr[1] = W[w].ydn[i] - ym;
            dr[2] = W[w].zdn[i] - zm;
          }
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          du += InterpolateU22(&G11, sqrt(r2));
        }
      }
#endif
#ifdef JASTROW_OPPOSITE_SPIN
      if(up) {
        for(i=0; i<Ndn; i++) {
          if(i != m_number) {
            dr[0] = xm - W[w].xdn[i];
            dr[1] = ym - W[w].ydn[i];
            dr[2] = zm - W[w].zdn[i];
          }
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          du += InterpolateU12(&G12, sqrt(r2));
        }
      }
      else {
        for(i=0; i<Nup; i++) {
          if(i != m_number) {
            dr[0] = xm - W[w].x[i];
            dr[1] = ym - W[w].y[i];
            dr[2] = zm - W[w].z[i];
          }
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          du += InterpolateU12(&G12, sqrt(r2));
        }
      }
#endif

#ifdef CRYSTAL // in the presence of a crystal
      if(up) {
        Wp[w].x[i] = xm;
        Wp[w].y[i] = ym;
        Wp[w].z[i] = zm;
        du += OneBodyUWalker(Wp[w], ON, OFF) - UCrystalOldUp;
        Wp[w].x[i] = W[w].x[i];
        Wp[w].y[i] = W[w].y[i];
        Wp[w].z[i] = W[w].z[i];
      }
      else {
        Wp[w].xdn[i] = xm;
        Wp[w].ydn[i] = ym;
        Wp[w].zdn[i] = zm;
        du += OneBodyUWalker(Wp[w], OFF, ON) - UCrystalOldDn;
        Wp[w].xdn[i] = W[w].xdn[i];
        Wp[w].ydn[i] = W[w].ydn[i];
        Wp[w].zdn[i] = W[w].zdn[i];
      }
#endif
      u = Exp(du);

#ifdef FERMIONS
      // Determinant contribution
      q = 0.;
      for(i=0; i<N; i++) {
        if(up) {
          dr[0] = xm - W[w].xdn[i];
          dr[1] = ym - W[w].ydn[i];
          dr[2] = zm - W[w].zdn[i];
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
          q += DeterminantInv[w][m_number][i] * (orbitalF(sqrt(r2)) + orbitalDescreteBCS_F(dr[0], dr[1], dr[2]));
#else
          q += DeterminantInv[w][m_number][i] * orbitalF(sqrt(r2)); 
#endif
        }
        else {
          dr[0] = xm - W[w].x[i];
          dr[1] = ym - W[w].y[i];
          dr[2] = zm - W[w].z[i];
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
          q += DeterminantInv[w][i][m_number] * (orbitalF(sqrt(r2)) + orbitalDescreteBCS_F(dr[0], dr[1], dr[2]));
#else
          q += DeterminantInv[w][i][m_number] * orbitalF(sqrt(r2)); 
#endif
        }
      }

      u *= q;
#endif // FERMIONS

      if(n<OBDM.size) {
         if(W[w].status == ALIVE || W[w].status == KILLED) {
          // one-body correlation function
          OBDM.N[n] += W[w].weight;
          OBDM.f[n] += u * W[w].weight;
        }
      }
      Nk.times_measured++;
      for(i=0; i<Nk.size; i++) {
        for(j=0; j<Nk.size; j++) {
          Nk.f[i][j] += u * Cos(Nk.k[i]*dr1+Nk.k[j]*dr2)*W[w].weight;
        }
      }
    }
  }
  return 0;
}

/******************************** TBDM ***************************************/
int MeasureTBDM(void) {
  DOUBLE UDetOld = 1, UDetNew = 1, UOneOld = 0, UOneNew = 0; // wf values
  DOUBLE CMshift[3]={0}; // shift of the center of the mass
  DOUBLE UpMcMillan[3], DnMcMillan[3]; // new coordinates of the pair
  int i,j,w,nr,nR;
  int index_up, index_down; // this pair will be moved
  DOUBLE x,y,z,r2,CM2;
  DOUBLE dr[3] = {0};
  DOUBLE du, dUpot, q;
  DOUBLE du_OBDM1, du_OBDM2, UDet1, UDet2; // calculate OBDM for displaced particles
  DOUBLE uTBDM, uOBDM1, uOBDM2;
  //DOUBLE min_displacement = 0.75;
  DOUBLE min_displacement = 0.;
  int m;
  DOUBLE phi, theta;

  if(Nup != Ndn) Warning("Measure TBDM is implemented only for Nup=Ndn case!\n");

  for(w=0; w<Nwalkers; w++) {// loop over walkers
    if(var_par_array) AdjustVariationalParameter(W[w].varparindex);
#ifdef FERMIONS
    // Fill Determinant Matrix (before the move)
    for(i=0; i<Nup; i++) {
      x = W[w].x[i];
      y = W[w].y[i];
      z = W[w].z[i];
      for(j=0; j<Ndn; j++) {
        dr[0] = x - W[w].xdn[j];
        dr[1] = y - W[w].ydn[j];
        dr[2] = z - W[w].zdn[j];
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
        DeterminantMove[i][j] = orbitalF(sqrt(r2));
#ifdef ORBITAL_ANISOTROPIC_ABSOLUTE
  Error("ORBITAL_ANISOTROPIC_ABSOLUTE not implemented!");
#endif
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
        DeterminantMove[i][j] += orbitalDescreteBCS_F(dr[0], dr[1], dr[2]);
#endif
      }
    }
    // Invert the matrix
    LUinv(DeterminantMove, DeterminantMoveInv, Nup);
    // determinant of the old configuration
    UDetOld = determinantLU;
    // store the matrix
    CopyMatrix(DeterminantMoveBackUp, DeterminantMove, Nup);
    //CopyMatrix(DeterminantMoveInvBackUp, DeterminantMoveInv);
#endif // FERMIONS

#ifdef CRYSTAL
    for(i=0; i<Nup; i++) {
      Wp[w].x[i] = W[w].x[i];
      Wp[w].y[i] = W[w].y[i];
      Wp[w].z[i] = W[w].z[i];
      Wp[w].xdn[i] = W[w].xdn[i];
      Wp[w].ydn[i] = W[w].ydn[i];
      Wp[w].zdn[i] = W[w].zdn[i];
    }
    UOneOld = OneBodyUWalker(Wp[w], ON, ON);
#endif

    for(m=0; m<McMillanTBDM_points; m++) {
      dUpot = du_OBDM1 = du_OBDM2 = 0.;
      UDet1 = UDet2 = 1.;
      // define a random pair to move:
      index_up   = rand() % Nup;
      index_down = rand() % Ndn;

      // calculate center of mass separation distance !
      if(measure_TBDM == 1) { // TBDM (OBDM of molecules)
        do {
          CaseX(CMshift[0] = (2.*Random()-1.)*TBDM12.max);
          CaseY(CMshift[1] = (2.*Random()-1.)*TBDM12.max);
          CaseZ(CMshift[2] = (2.*Random()-1.)*TBDM12.max);

          FindNearestImage3D(CM2, CMshift[0], CMshift[1], CMshift[2]);
          nR = (int) (sqrt(CM2)/TBDM12.step);
        }
        while(nR>=TBDM12.size || nR < TBDM12.size*min_displacement);
      }
      else { // phi (order parameter)
        theta = Random()*2.*PI;
        phi = Random()*PI;

        CMshift[0] = Cos(theta)*Sin(phi)*TBDM12.max;
        CMshift[1] = Sin(theta)*Sin(phi)*TBDM12.max;
        CMshift[2] = Cos(phi)*TBDM12.max;
        CM2 = TBDM12.max*TBDM12.max;
        nR = TBDM12.size-1;
      }

#ifdef JASTROW_OPPOSITE_SPIN
      for(i=0; i<Nup; i++) {
        x = W[w].x[i];
        y = W[w].y[i];
        z = W[w].z[i];
        for(j=0; j<Ndn; j++) {
          if(i == index_up) {
            dr[0] = x - W[w].xdn[j];
            dr[1] = y - W[w].ydn[j];
            dr[2] = z - W[w].zdn[j];
            FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
            du = InterpolateU12(&G12, sqrt(r2));
            dUpot -= du;
            du_OBDM1 -= du;
            if(j == index_down) du_OBDM2 -= du;
          }
          else if(j == index_down) {
            dr[0] = x - W[w].xdn[j];
            dr[1] = y - W[w].ydn[j];
            dr[2] = z - W[w].zdn[j];
            FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
            du = InterpolateU12(&G12, sqrt(r2));
            dUpot -= du;
            du_OBDM2 -= du;
          }
        }
      }
#endif

      // new coordinates of the pair
      UpMcMillan[0] = W[w].x[index_up] + CMshift[0];
      UpMcMillan[1] = W[w].y[index_up] + CMshift[1];
      UpMcMillan[2] = W[w].z[index_up] + CMshift[2];
      DnMcMillan[0] = W[w].xdn[index_down] + CMshift[0];
      DnMcMillan[1] = W[w].ydn[index_down] + CMshift[1];
      DnMcMillan[2] = W[w].zdn[index_down] + CMshift[2];

#ifdef FERMIONS
      // Determinant contribution to OBDM, 1st particle
      q = 0.;
      for(j=0; j<Nup; j++) {
        dr[0] = UpMcMillan[0] - W[w].xdn[j];
        dr[1] = UpMcMillan[1] - W[w].ydn[j];
        dr[2] = UpMcMillan[2] - W[w].zdn[j];
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
        q += DeterminantMoveInv[index_up][j] * (orbitalF(sqrt(r2)) + orbitalDescreteBCS_F(dr[0], dr[1], dr[2]));
#else
        q += DeterminantMoveInv[index_up][j] * orbitalF(sqrt(r2));
#endif
      }
      UDet1 *= q;
#endif

/*#ifdef SECURE // check fast det. update, 1st particle
      //CopyMatrix(DeterminantMoveBackUp, DeterminantMove, Nup);
      //CopyMatrix(DeterminantMoveInvBackUp, DeterminantMoveInv, Nup);
      for(i=0; i<Nup; i++) {
        if(i != index_up) {
          x = W[w].x[i];
          y = W[w].y[i];
          z = W[w].z[i];
        }
        else {
          x = UpMcMillan[0];
          y = UpMcMillan[1];
          z = UpMcMillan[2];
        }
        for(j=0; j<Ndn; j++) {
          dr[0] = x - W[w].xdn[j];
          dr[1] = y - W[w].ydn[j];
          dr[2] = z - W[w].zdn[j];
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          DeterminantMove[i][j] = orbitalF(sqrt(r2));
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
          DeterminantMove[i][j] += orbitalDescreteBCS_F(dr[0], dr[1], dr[2]);
#endif
        }
      }
      // Invert the matrix
      LUinv(DeterminantMove, DeterminantMoveInv, Nup);
      // determinant of the old configuration
      if(fabs(determinantLU/UDetOld/UDet1-1.)>1e-8) {
        Warning("  TBDM: bad updated value of determinant, up particle, %"LE" instead of %"LE"\n", determinantLU, UDetOld/UDet1);
      }
      else
        Message("+");
      //CopyMatrix(DeterminantMove, DeterminantMoveBackUp, Nup);
      //CopyMatrix(DeterminantMoveInv, DeterminantMoveInvBackUp, Nup);
#endif*/

#ifdef FERMIONS
      // Determinant contribution to OBDM, 2nd particle
      q = 0.;
      for(i=0; i<Nup; i++) {
        dr[0] =  W[w].x[i] - DnMcMillan[0];
        dr[1] =  W[w].y[i] - DnMcMillan[1];
        dr[2] =  W[w].z[i] - DnMcMillan[2];
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
        q += DeterminantMoveInv[i][index_down] * (orbitalF(sqrt(r2)) + orbitalDescreteBCS_F(dr[0], dr[1], dr[2]));
#else
        q += DeterminantMoveInv[i][index_down] * orbitalF(sqrt(r2));
#endif
        }
      UDet2 *= q;
#endif

/*//#ifdef SECURE //check fast det. update 2nd particle
      //CopyMatrix(DeterminantMoveBackUp, DeterminantMove, Nup);
      //CopyMatrix(DeterminantMoveInvBackUp, DeterminantMoveInv, Nup);
      for(i=0; i<Nup; i++) {
        x = W[w].x[i];
        y = W[w].y[i];
        z = W[w].z[i];
        for(j=0; j<Ndn; j++) {
          if(j != index_down) {
            dr[0] = x - W[w].xdn[j];
            dr[1] = y - W[w].ydn[j];
            dr[2] = z - W[w].zdn[j];
          }
          else {
            dr[0] = x - DnMcMillan[0];
            dr[1] = y - DnMcMillan[1];
            dr[2] = z - DnMcMillan[2];
          }
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          DeterminantMove[i][j] = orbitalF(sqrt(r2));
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
          DeterminantMove[i][j] += orbitalDescreteBCS_F(dr[0], dr[1], dr[2]);
#endif
        }
      }
      // Invert the matrix
      LUinv(DeterminantMove, DeterminantMoveInv, Nup);
      // determinant of the old configuration
      if(fabs(determinantLU/UDetOld/UDet2-1.)>1e-8) {
        Warning("  TBDM: bad det., down particle, %"LE" instead of %"LE"\n", determinantLU, UDetOld/UDet2);
      }
//      CopyMatrix(DeterminantMove, DeterminantMoveBackUp, Nup);
//      CopyMatrix(DeterminantMoveInv, DeterminantMoveInvBackUp, Nup);
//#endif*/

      // Move this pair to McMillan point
#ifdef CRYSTAL
      Wp[w].x[index_up] = UpMcMillan[0];
      Wp[w].y[index_up] = UpMcMillan[1];
      Wp[w].z[index_up] = UpMcMillan[2];
      Wp[w].xdn[index_down] = DnMcMillan[0];
      Wp[w].ydn[index_down] = DnMcMillan[1];
      Wp[w].zdn[index_down] = DnMcMillan[2];
      UOneNew = OneBodyUWalker(Wp[w], ON, ON);
      Wp[w].x[index_up] = W[w].x[index_up];
      Wp[w].y[index_up] = W[w].y[index_up];
      Wp[w].z[index_up] = W[w].z[index_up];
      Wp[w].xdn[index_down] = W[w].xdn[index_down];
      Wp[w].ydn[index_down] = W[w].ydn[index_down];
      Wp[w].zdn[index_down] = W[w].zdn[index_down];
#endif
      for(j=0; j<Ndn; j++) { // up[i] -> McMillan
        dr[0] = UpMcMillan[0] - W[w].xdn[j];
        dr[1] = UpMcMillan[1] - W[w].ydn[j];
        dr[2] = UpMcMillan[2] - W[w].zdn[j];

        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);

#ifdef FERMIONS
        DeterminantMove[index_up][j] = orbitalF(sqrt(r2));
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
        DeterminantMove[index_up][j] += orbitalDescreteBCS_F(dr[0], dr[1], dr[2]);
#endif
#endif // FERMIONS

#ifdef JASTROW_OPPOSITE_SPIN // Jastrow term
        du = InterpolateU12(&G12, sqrt(r2));
        du_OBDM1 += du;
        if(j != index_down) dUpot += du;
#endif
      }

      for(i=0; i<Nup; i++) { // dn[j] -> McMillan
        dr[0] = W[w].x[i] - DnMcMillan[0];
        dr[1] = W[w].y[i] - DnMcMillan[1];
        dr[2] = W[w].z[i] - DnMcMillan[2];

        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);

#ifdef FERMIONS
        DeterminantMove[i][index_down] = orbitalF(sqrt(r2));
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
        DeterminantMove[i][index_down] += orbitalDescreteBCS_F(dr[0], dr[1], dr[2]);
#endif
#endif

#ifdef JASTROW_OPPOSITE_SPIN // Jastrow term
        du = InterpolateU12(&G12, sqrt(r2));
        du_OBDM2 += du;
        if(i != index_up) dUpot += du;
#endif
      }
      // up[i] - dn[j]
      dr[0] = UpMcMillan[0] - DnMcMillan[0];
      dr[1] = UpMcMillan[1] - DnMcMillan[1];
      dr[2] = UpMcMillan[2] - DnMcMillan[2];

      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);

#ifdef FERMIONS
      DeterminantMove[index_up][index_down] = orbitalF(sqrt(r2));
#ifdef ORBITAL_ANISOTROPIC_RELATIVE
      DeterminantMove[index_up][index_down] += orbitalDescreteBCS_F(dr[0], dr[1], dr[2]);
#endif
#endif

#ifdef JASTROW_OPPOSITE_SPIN // Jastrow term
      dUpot += InterpolateU12(&G12, sqrt(r2));
#endif

#ifdef FERMIONS
      // Invert the matrix
      LUinv(DeterminantMove, DeterminantMoveInv, Nup);
      // determinant of the McMillan configuration
      UDetNew = determinantLU;
#endif

    // Effect of interactions
#ifdef JASTROW_SAME_SPIN // Jastrow contribution (same spin particle-particle force)
      for(j=0; j<Nup; j++) {
        if(j!= index_up) {
          dr[0] = UpMcMillan[0] - W[w].x[j];
          dr[1] = UpMcMillan[1] - W[w].y[j];
          dr[2] = UpMcMillan[2] - W[w].z[j];
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          du = InterpolateU11(&G11, sqrt(r2));
          dUpot += du;
          du_OBDM1 += du;

          dr[0] = W[w].x[index_up] - W[w].x[j];
          dr[1] = W[w].y[index_up] - W[w].y[j];
          dr[2] = W[w].z[index_up] - W[w].z[j];
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          du = InterpolateU11(&G11, sqrt(r2));
          dUpot -= du;
          du_OBDM1 -= du;
        }

        if(j!= index_down) {
          dr[0] = DnMcMillan[0] - W[w].xdn[j];
          dr[1] = DnMcMillan[1] - W[w].ydn[j];
          dr[2] = DnMcMillan[2] - W[w].zdn[j];
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          du = InterpolateU22(&G22, sqrt(r2));
          dUpot += du;
          du_OBDM2 += du;

          dr[0] = W[w].xdn[index_down] - W[w].xdn[j];
          dr[1] = W[w].ydn[index_down] - W[w].ydn[j];
          dr[2] = W[w].zdn[index_down] - W[w].zdn[j];
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          du = InterpolateU22(&G22, sqrt(r2));
          dUpot -= du;
          du_OBDM2 -= du;
        }
      }
#endif

/*   W[w].x[index_up] = UpMcMillan[0]; // check OBDM u, 1st particle
     W[w].y[index_up] = UpMcMillan[1];
     W[w].z[index_up] = UpMcMillan[2];
     q = U(W[w]);
     if(fabs((q-killme)/du_OBDM1-1)>1e-6) Message("-");
     W[w].x[index_up] -= CMshift[0];
     W[w].y[index_up] -= CMshift[1];
     W[w].z[index_up] -= CMshift[2];

     W[w].xdn[index_down] = DnMcMillan[0]; // check OBDM u, 1st particle
     W[w].ydn[index_down] = DnMcMillan[1];
     W[w].zdn[index_down] = DnMcMillan[2];
     q = U(W[w]);
     if(fabs((q-killme)/du_OBDM2-1)>1e-6) Message("-");
     W[w].xdn[index_down] -= CMshift[0];
     W[w].ydn[index_down] -= CMshift[1];
     W[w].zdn[index_down] -= CMshift[2];*/

      nR = (int) (sqrt(CM2)/TBDM12.step);
      if(nR>=TBDM12.size) nR = TBDM12.size-1;

      if(W[w].status == ALIVE || W[w].status == KILLED) {
        uTBDM = UDetNew/UDetOld*Exp(dUpot+UOneNew-UOneOld);
        uOBDM1 = UDet1*Exp(du_OBDM1);
        uOBDM2 = UDet2*Exp(du_OBDM2);
        TBDM12.N[nR] += 1.;
        TBDM12.fcorrected[nR] += uTBDM-uOBDM1*uOBDM2;
        //TBDM12.OBDM[nR] += 0.5*(uOBDM1+uOBDM2);
        TBDM12.OBDM[nR] += uOBDM1;

        // calculate size of the pair
        dr[0] = W[w].x[index_up] - W[w].xdn[index_down];
        dr[1] = W[w].y[index_up] - W[w].ydn[index_down];
        dr[2] = W[w].z[index_up] - W[w].zdn[index_down];

        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);

        TBDM12.f[nR] += uTBDM;
        TBDM12.R2[nR] += r2*uTBDM;
        //TBDM12.f[nR] += fabs(uTBDM);
        //TBDM12.R2[nR] += r2*fabs(uTBDM);
        TBDM12.R2corrected[nR] += r2*(uTBDM-uOBDM1*uOBDM2);
        //TBDM12.R2corrected[nR] += r2*(uTBDM*(1.-fabs(uOBDM1*uOBDM2/uTBDM)));

        nr = (int) (sqrt(r2)/TBDM_MATRIX.step);

        if(nr<TBDM_MATRIX.size) {
          TBDM_MATRIX.f[0][nr] += uTBDM;
          TBDM_MATRIX.N[0][0]++;
          TBDM_MATRIX.f[1][nr] += uTBDM*r2;
          TBDM_MATRIX.N[1][0]++;
          TBDM_MATRIX.f[2][nr] += uTBDM*r2*r2;
          TBDM_MATRIX.N[2][0]++;
        }
      }
#ifdef FERMIONS
      CopyMatrix(DeterminantMove, DeterminantMoveBackUp, Nup); //restore the matrix
#endif
    }
    //CopyMatrix(DeterminantMoveInv, DeterminantMoveInvBackUp);
  }
  return 0;
}

/******************************** TBDM ***************************************/
//int MeasureTBDMbosons12(void) {
int MeasureTBDM1D(void) {
  DOUBLE UpMcMillan[3], DnMcMillan[3]; // new coordinates of the pair
  int i,w,nR;
  int index_up, index_down; // this pair will be moved
  DOUBLE dr[3],r2,CM2;
  DOUBLE du;
  DOUBLE uTBDM, uOBDM1, uOBDM2;
  int m;
  int s; // parity for TBDM
  int so1, so2;// parity for OBDM
  int measure_molecular_wf; // defines direction in which TBDM is measured

  dr[0] = dr[1] = 0.; // displacement vector

  for(w=0; w<Nwalkers; w++) { // loop over walkers
    for(m=0; m<McMillanTBDM_points; m++) {
      index_up   = rand() % Nup; // define a random pair to move:
      index_down = rand() % Ndn;

      measure_molecular_wf = rand() % 2;

      // define distance to displace a pair
      if(measure_molecular_wf) { // nR - is relative size of a bound pair
        CM2 = 0.25*L;
      }
      else { // nR - is displacement distance of a bound pair
        nR = rand() % TBDM12.size;
        CM2 = (DOUBLE) (nR) * TBDM12.step;
      }

      UpMcMillan[2] = W[w].z[index_up] + CM2; // generate new coordinates of the pair
      DnMcMillan[2] = W[w].zdn[index_down] + CM2;
      UpMcMillan[2] -= L*(int)(UpMcMillan[2]/L); // put coordinates to the box
      DnMcMillan[2] -= L*(int)(DnMcMillan[2]/L);
      //if(UpMcMillan[2]>L || UpMcMillan[2]<0) Warning("UpMcMillan[2] out of the box\n");

      if(measure_molecular_wf) {
        dr[2] = UpMcMillan[2]-DnMcMillan[2];
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
        nR = (int) (sqrt(r2)/TBDM12.step);
      }

      if(nR>=TBDM12.size) nR = TBDM12.size-1;

      uTBDM = uOBDM1 = uOBDM2 = 0.;
      s = so1 = so2 = 1; // calculate parity due to FTG interaction
      for(i=0; i<Ndn; i++) { // [index_up] + all down
        if(W[w].z[index_up]-W[w].zdn[i]<0) { // old parity
          if(i!=index_down) s *= -1;
          so1 *= -1;
        }
        if(UpMcMillan[2]-W[w].zdn[i]<0) { // new parity
          if(i!=index_down) s *= -1;
          so1 *= -1;
        }
      }
      for(i=0; i<Nup; i++) { // [index_dn] + all up
        if(W[w].zdn[index_down]-W[w].z[i]<0) {
          if(i!=index_up) s *= -1;
          so2 *= -1;
        }
        if(DnMcMillan[2]-W[w].z[i]<0) {
          if(i!=index_up) s *= -1;
          so2 *= -1;
         }
       }

      for(i=0; i<Nup; i++) { // [index_up] + all up
        if(i!=index_up) {
          dr[2] = UpMcMillan[2]-W[w].z[i]; // new CSM1 weight
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          du = InterpolateU11(&G11, sqrt(r2));
          if(i!=index_down) uTBDM += du;
          uOBDM1 += du;

          dr[2] = W[w].z[index_up]-W[w].z[i]; // old CSM1 weight
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          du = InterpolateU11(&G11, sqrt(r2));
          if(i!=index_down) uTBDM -= du;
          uOBDM1 -= du;
        }
      }

      for(i=0; i<Ndn; i++) { // [index_dn] + all dn
        if(i!=index_down) {
          dr[2] = DnMcMillan[2]-W[w].zdn[i]; // new CSM1 weight
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          du = InterpolateU22(&G11, sqrt(r2));
          if(i!=index_up) uTBDM += du;
          uOBDM2 += du;

          dr[2] = W[w].zdn[index_down]-W[w].zdn[i]; // old CSM2 weight
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          du = InterpolateU22(&G22, sqrt(r2));
          if(i!=index_up) uTBDM -= du;
          uOBDM2 -= du;
        }
      }

      if(measure_molecular_wf) {
        TBDM12.fr[nR] += Exp(uTBDM)*s;
        TBDM12.Nr[nR] += 1.;
#ifdef OBDM_BOSONS
        TBDMbosons12.fr[nR] += Exp(uTBDM);
        TBDMbosons12.Nr[nR] += 1.;
#endif
      }
      else {
        TBDM12.f[nR] += Exp(uTBDM)*s;
        TBDM12.N[nR] += 1.;
        TBDM11.OBDM[nR] += Exp(uOBDM1)*so1;
        TBDM22.OBDM[nR] += Exp(uOBDM2)*so2;
#ifdef OBDM_BOSONS
        TBDMbosons12.f[nR] += Exp(uTBDM);
        TBDMbosons12.N[nR] += 1.;
#endif
      }

      s = 1; //TBDM up - up
      UpMcMillan[2] = W[w].z[index_up] + CM2; // generate new coordinates of the pair
      DnMcMillan[2] = W[w].z[index_down] + CM2;
      UpMcMillan[2] -= L*(int)(UpMcMillan[2]/L); // put coordinates to the box
      DnMcMillan[2] -= L*(int)(DnMcMillan[2]/L);

      if(measure_molecular_wf) {
        dr[2] = UpMcMillan[2]-DnMcMillan[2];
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
        nR = (int) (sqrt(r2)/TBDM12.step);
      }

      uTBDM = 0.;
      for(i=0; i<Ndn; i++) { // [index_up/dn] + all down (FTG parity)
        if(UpMcMillan[2]-W[w].zdn[i]<0) s *= -1;
        if(W[w].z[index_up]-W[w].zdn[i]<0) s *= -1;
        if(DnMcMillan[2]-W[w].zdn[i]<0) s *= -1;
        if(W[w].z[index_down]-W[w].zdn[i]<0) s *= -1;
      }

      for(i=0; i<Nup; i++) { // CSM1 contribution
        if(i != index_up && i != index_down) {
          dr[2] = UpMcMillan[2]-W[w].z[i]; // new weight 1
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          du = InterpolateU22(&G11, sqrt(r2));
          uTBDM += du;

          dr[2] = W[w].z[index_up]-W[w].z[i]; // old weight 1
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          du = InterpolateU22(&G11, sqrt(r2));
          uTBDM -= du;

          dr[2] = DnMcMillan[2]-W[w].z[i]; // new weight 2
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          du = InterpolateU22(&G11, sqrt(r2));
          uTBDM += du;

          dr[2] = W[w].z[index_down]-W[w].z[i]; // old weight 2
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          du = InterpolateU22(&G11, sqrt(r2));
          uTBDM -= du;
        }
      }
      if(index_up != index_down) {
        if(measure_molecular_wf) {
          TBDM11.Nr[nR] += 1.;
          TBDM11.fr[nR] += Exp(uTBDM)*s;
#ifdef OBDM_BOSONS
          TBDMbosons11.Nr[nR] += 1.;
          TBDMbosons11.fr[nR] += Exp(uTBDM);
#endif
        }
        else {
          TBDM11.N[nR] += 1.;
          TBDM11.f[nR] += Exp(uTBDM)*s;
#ifdef OBDM_BOSONS
          TBDMbosons11.N[nR] += 1.;
          TBDMbosons11.f[nR] += Exp(uTBDM);
#endif
        }
      }

      s = 1; //TBDM dn - dn
      UpMcMillan[2] = W[w].zdn[index_up] + CM2; // generate new coordinates of the pair
      DnMcMillan[2] = W[w].zdn[index_down] + CM2;
      UpMcMillan[2] -= L*(int)(UpMcMillan[2]/L); // put coordinates to the box
      DnMcMillan[2] -= L*(int)(DnMcMillan[2]/L);

      if(measure_molecular_wf) {
        dr[2] = UpMcMillan[2]-DnMcMillan[2];
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
        nR = (int) (sqrt(r2)/TBDM12.step);
      }

      uTBDM = 0.;
      for(i=0; i<Nup; i++) { // [index_up/dn] + all up (FTG parity)
        if(UpMcMillan[2]-W[w].z[i]<0) s *= -1;
        if(W[w].zdn[index_up]-W[w].z[i]<0) s *= -1;
        if(DnMcMillan[2]-W[w].z[i]<0) s *= -1;
        if(W[w].zdn[index_down]-W[w].z[i]<0) s *= -1;
      }

      for(i=0; i<Ndn; i++) { // CSM2 contribution
        if(i != index_up && i != index_down) {
          dr[2] = UpMcMillan[2]-W[w].zdn[i]; // new weight 1
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          du = InterpolateU22(&G11, sqrt(r2));
          uTBDM += du;

          dr[2] = W[w].zdn[index_up]-W[w].zdn[i]; // old weight 1
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          du = InterpolateU22(&G11, sqrt(r2));
          uTBDM -= du;

          dr[2] = DnMcMillan[2]-W[w].zdn[i]; // new weight 2
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          du = InterpolateU22(&G11, sqrt(r2));
          uTBDM += du;

          dr[2] = W[w].zdn[index_down]-W[w].zdn[i]; // old weight 2
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
          du = InterpolateU22(&G11, sqrt(r2));
          uTBDM -= du;
        }
      }
      if(index_up != index_down) {
        if(measure_molecular_wf) {
          TBDM22.Nr[nR] += 1.;
          TBDM22.fr[nR] += Exp(uTBDM)*s;
#ifdef OBDM_BOSONS
          TBDMbosons22.Nr[nR] += 1.;
          TBDMbosons22.fr[nR] += Exp(uTBDM);
#endif
        }
        else {
          TBDM22.N[nR] += 1.;
          TBDM22.f[nR] += Exp(uTBDM)*s;
#ifdef OBDM_BOSONS
          TBDMbosons22.N[nR] += 1.;
          TBDMbosons22.f[nR] += Exp(uTBDM);
#endif
        }
      }
    }
  }
  if(dr[0]*dr[0]+dr[1]*dr[1]>1e-5) Warning(" not zero dr[]!\n");
  return 0;
}

/******************************** OBDM ***************************************/
int MeasureOBDMMatrix(void) {
  /*
  int w,i,j,n;
  DOUBLE x, y, z, xp, yp, zp, xpp, ypp, zpp;
  DOUBLE phi, theta, h;
  DOUBLE mu, r2, dr[3];
  //extern DOUBLE mu_k[N], mu_p_k[N], mu_pp_k[N]
  DOUBLE mu_p, mu_pp;

  h = OBDM_MATRIX.step;

  for(w=0; w<Nwalkers; w++) {
    if(var_par_array) AdjustVariationalParameter(W[w].varparindex);
    // initialize arrays with zeros
    for(n=0; n<Nup; n++) mu_k[n] = 0.;

    // fill mu_k = -alpha r_k^2 + sum mu(r_i - r_k)
    for(i=0; i<Nup; i++) {
      x = W[w].x[i];
      y = W[w].y[i];
      z = W[w].z[i];

      mu_k[i] += -alpha*(x*x+y*y+z*z);

      for(j=i+1; j<Nup; j++) {
        dr[0] = x - W[w].x[j];
        dr[1] = y - W[w].y[j];
        dr[2] = z - W[w].z[j];
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);

#ifdef HARD_SPHERE12
        if(r2 > a2)
#endif
        {
          mu = InterpolateU(&G, sqrt(r2));
          mu_k[i] += mu;
          mu_k[j] += mu;
        }
      }
    }

    for(i=0; i<OBDM_MATRIX.size; i++) {
      phi = Random() * _2PI;

#ifdef TRIAL_2D
      theta = 0;
#else
      theta = Random() * PI;
#endif

      xp = yp = zp = h * i;
      xp *= Cos(phi) * Cos(theta);
      yp *= Sin(phi) * Cos(theta);
      zp *= Sin(theta);

      // fill mu_p = -alpha (r')^2 + sum mu(r_i - r')
      //mu_p += -alpha*(xp*xp+yp*yp) -lambda_beta * zp*zp;
      mu_p = -alpha*h*h*(i*i);
      for(n=0; n<Nup; n++) {
        dr[0] = xp - W[w].x[n];
        dr[1] = yp - W[w].y[n];
        dr[2] = zp - W[w].z[n];
        FindNearestImage3D(r2, dr[0], dr[1], dr[2]);

#ifdef HARD_SPHERE12
        if(r2 > a2) {
          mu = InterpolateU(&G, sqrt(r2));
          mu_p += mu;
          mu_p_k[n] = mu;
        }
        else{
          mu_p_k[n] = 0;
        }
#else
        mu = InterpolateU(&G, sqrt(r2));
        mu_p += mu;
        mu_p_k[n] = mu;
#endif
      }

      for(j=i; j<OBDM_MATRIX.size; j++) {
        phi = Random() * _2PI;

#ifdef TRIAL_2D
        theta = 0;
#else
        theta = Random() * PI;
#endif

        xpp = ypp = zpp = h * j;
        xpp *= Cos(phi) * Cos(theta);
        ypp *= Sin(phi) * Cos(theta);
        zpp *= Sin(theta);

        // fill mu_pp = -alpha (r")^2 + sum mu(r_i - r")
        //mu_pp += -alpha*(xp*xp+yp*yp) -lambda_beta * zp*zp;
        mu_pp = -alpha*h*h*(j*j);
        for(n=0; n<Nup; n++) {
          dr[0] = xpp - W[w].x[n];
          dr[1] = ypp - W[w].y[n];
          dr[2] = zpp - W[w].z[n];
          FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
#ifdef HARD_SPHERE12
          if(r2 > a2) {
            mu = InterpolateU(&G, sqrt(r2));
            mu_pp += mu;
            mu_pp_k[n] = mu;
          }
          else {
            mu_pp_k[n] = 0;
          }
#else
          mu = InterpolateU(&G, sqrt(r2));
          mu_pp += mu;
          mu_pp_k[n] = mu;
#endif
        }

        for(n=0; n<Nup; n++) {
          mu = Exp(mu_p + mu_pp - mu_p_k[n] - mu_pp_k[n] - 2.*mu_k[n]);
          OBDM_MATRIX.f[i][j] += mu;
          OBDM_MATRIX.N[i][j] ++;
          if(i != j) {
            OBDM_MATRIX.f[j][i] += mu;
            OBDM_MATRIX.N[j][i] ++;
          }
        }
      }
    }
  }*/

  return 0;
}

/************************ Empty Distribution Arrays **************************/
void EmptyDistributionArrays(void) {
  int i,j;

  if(measure_PairDistr) {
    PD.times_measured = 0;
    PD.r2 = 0.;
    PD.g3 = 0;
  }

  if(measure_RadDistr) {
    RD.times_measured = 0;
    RD.r2 = 0.;
    for(i=0; i<RD.size; i++) {
      RD.N[i] = 0;
      RDxup.N[i] = 0;
      RDyup.N[i] = 0;
      RDzup.N[i] = 0;
      RDup.N[i] = 0;
      RDxdn.N[i] = 0;
      RDydn.N[i] = 0;
      RDzdn.N[i] = 0;
      RDdn.N[i] = 0;
    }
  }

  if(measure_OBDM || measure_Nk) {
    for(i=0; i<OBDM.size; i++) {
      OBDM.f[i] = 0.;
      OBDM.N[i] = 0;
#ifdef TRIAL_1D
      OBDM22.f[i] = 0.;
      OBDM22.N[i] = 0;
#endif
#ifdef BC_ABSENT
      OBDMtrap.f[i] = 0;
      OBDMtrap.N[i] = 0;
#endif
#ifdef OBDM_BOSONS
      OBDMbosons.f[i] = 0.;
      OBDMbosons.N[i] = 0;
      OBDMbosons22.f[i] = 0.;
      OBDMbosons22.N[i] = 0;
#endif
    }
#ifdef TRIAL_1D
  for(i=0; i<OBDM.Nksize; i++) OBDM.Nk[i] = OBDM22.Nk[i] = 0.;
#ifdef OBDM_BOSONS
  for(i=0; i<OBDM.Nksize; i++) OBDMbosons.Nk[i] = OBDMbosons22.Nk[i] = 0.;
#endif
#endif
  }

  if(measure_Nk) {
    Nk.times_measured = 0;
    for(i=0; i<Nk.size; i++) {
      for(j=0; j<Nk.size; j++) {
        Nk.f[i][j] = 0;
      }
    }
  }

  if(MC && measure_PairDistr) {
    PD_pure.times_measured = 0;
    PD_pure.r2 = 0.;
    for(i=0; i<PD_pure.size; i++) {
      PD_pure.N[i] = 0;
    }
  }

  if(measure_SD && MC) {
    for(i=0; i<SD.size; i++) {
      SD.CM2[i] = 0;
      SD.N[i] = 0;
    }
  }

  if(measure_TBDM) {
    for(i=0; i<OBDM.size; i++) {
      TBDM12.f[i] = 0.;
      TBDM12.N[i] = 0;
      TBDM12.R2[i] = 0.;
      TBDM12.fcorrected[i] = 0.;
      TBDM12.R2corrected[i] = 0.;
      TBDM12.OBDM[i] = 0.;
#ifdef TRIAL_1D
      TBDM11.f[i] = 0.;
      TBDM11.N[i] = 0.;
      TBDM11.OBDM[i] = 0.;
      TBDM22.f[i] = 0.;
      TBDM22.N[i] = 0.;
      TBDM22.OBDM[i] = 0.;
      TBDM12.fr[i] = 0.;
      TBDM12.Nr[i] = 0;
      TBDM11.fr[i] = 0.;
      TBDM11.Nr[i] = 0.;
      TBDM22.fr[i] = 0.;
      TBDM22.Nr[i] = 0.;
#endif
#ifdef OBDM_BOSONS
      TBDMbosons11.f[i] = 0.;
      TBDMbosons11.N[i] = 0.;
      TBDMbosons11.OBDM[i] = 0.;
      TBDMbosons22.f[i] = 0.;
      TBDMbosons22.N[i] = 0.;
      TBDMbosons22.OBDM[i] = 0.;
      TBDMbosons12.fr[i] = 0.;
      TBDMbosons12.Nr[i] = 0;
      TBDMbosons11.fr[i] = 0.;
      TBDMbosons11.Nr[i] = 0.;
      TBDMbosons22.fr[i] = 0.;
      TBDMbosons22.Nr[i] = 0.;
#endif
    }
  }

  OrderParameter.cos = 0;
  OrderParameter.sin = 0;
  OrderParameter.times_measured = 0;
}

/**************************** Static Structure Factor *************************/
void MeasureStaticStructureFactor(int w) {
  int i,n;
#ifdef TRIAL_1D
  DOUBLE k;
#else
  int nk;
  DOUBLE k1,k2,k3;
#endif
  DOUBLE cosF1, sinF1, cosF2, sinF2;
  static int wait = ON;
  int Sk_position;

#pragma omp atomic
  Sk.times_measured++;

  if(MC == DIFFUSION && wait == OFF) { // do a pure measurement
#pragma omp atomic
    Sk_pure.times_measured++;
    Sk_position = W[w].Sk_position;
    for(n=0; n<gridSk; n++) {
#pragma omp atomic
      Sk_pure.f[n] += W[w].Sk[Sk_position][n]; // * W[w].weight;
#pragma omp atomic
      Sk_pure.f12[n] += W[w].Sk12[Sk_position][n]; // * W[w].weight;
      W[w].Sk[Sk_position][n] = 0.;
      W[w].Sk12[Sk_position][n] = 0.;
//#ifndef TRIAL_1D // i.e. 2D and 3D
#pragma omp atomic
      Sk_pure.N[n] += W[w].Sk_N[Sk_position][n]; // * W[w].weight;
      W[w].Sk_N[Sk_position][n] = 0;
//#endif
    }
  }

#ifdef TRIAL_1D
  for(n=0; n<Sk.size; n++) {
    k = Sk.k[n];
    sinF1 = cosF1 = 0;
    for(i=0; i<Nup; i++) {
      cosF1 += Cos(k*W[w].z[i]);
      sinF1 += Sin(k*W[w].z[i]);
    }
    sinF2 = cosF2 = 0;
    for(i=0; i<Ndn; i++) {
      cosF2 += Cos(k*W[w].zdn[i]);
      sinF2 += Sin(k*W[w].zdn[i]);
    }
#else // 2D and 3D cases

  for(nk=0; nk<Sk.size; nk++) {
    sinF1 = cosF1 = sinF2 = cosF2 = 0;

    k1 = Sk.kx[nk];
    k2 = Sk.ky[nk];
    k3 = Sk.kz[nk];

    for(i=0; i<Nup; i++) {
      cosF1 += Cos(k1*W[w].x[i] + k2*W[w].y[i] + k3*W[w].z[i]);
      sinF1 += Sin(k1*W[w].x[i] + k2*W[w].y[i] + k3*W[w].z[i]);
    }
    for(i=0; i<Ndn; i++) {
      cosF2 += Cos(k1*W[w].xdn[i]+k2*W[w].ydn[i]+k3*W[w].zdn[i]);
      sinF2 += Sin(k1*W[w].xdn[i]+k2*W[w].ydn[i]+k3*W[w].zdn[i]);
    }
    n = Sk.index[nk];
#endif // end 2D and 3D

#pragma omp atomic
    //Sk.f[n] += cosF1*cosF1 + sinF1*sinF1 + cosF2*cosF2 + sinF2*sinF2; // two contributions
#ifdef MEASURE_SK11
    Sk.f[n] += cosF1*cosF1 + sinF1*sinF1; // first contribution
#endif
#ifdef MEASURE_SK22
    Sk.f[n] += cosF2*cosF2 + sinF2*sinF2; // second contribution
#endif
#pragma omp atomic
    Sk.f12[n] += cosF1*cosF2 + sinF1*sinF2; // one contribution
#pragma omp atomic
    Sk.N[n]++;
    if(MC == DIFFUSION) { // accumulation of the pure estimator
      //W[w].Sk[W[w].Sk_position][n]   += cosF1*cosF1 + sinF1*sinF1 + cosF2*cosF2 + sinF2*sinF2;
#ifdef MEASURE_SK11
      W[w].Sk[W[w].Sk_position][n]   += cosF1*cosF1 + sinF1*sinF1;
#endif
#ifdef MEASURE_SK22
      W[w].Sk[W[w].Sk_position][n]   += cosF2*cosF2 + sinF2*sinF2;
#endif
      W[w].Sk12[W[w].Sk_position][n] += cosF1*cosF2 + sinF1*sinF2;
      W[w].Sk_N[W[w].Sk_position][n]++;
    }
  }
  if(MC == DIFFUSION) { // move current position
    W[w].Sk_position++;
    if(W[w].Sk_position == grid_pure_block) W[w].Sk_position = 0;
  }

  if(MC == DIFFUSION && wait) { // skip the first block in PURE case
    wait++;
    if(wait == grid_pure_block+1)
      wait = OFF;
  }
}

void MeasureStaticStructureFactorAngularPart(void) {
  int w,i;
  int n,n1,n2,n3,nk;
  int n1p,n2p,n3p;
  DOUBLE k1,k2,k3,k1p,k2p,k3p;
  DOUBLE cosF1, sinF1, cosF2, sinF2;
  DOUBLE K,Kp,dphi,KKp;
  DOUBLE phi_min;
  int max_harmonic=5;

  phi_min = 0.1;

  for(w=0; w<Nwalkers; w++) {
    for(nk=0; nk<Sk.size; nk++) {
      do {
        n1 = max_harmonic - rand() % (2*max_harmonic);
        n2 = max_harmonic - rand() % (2*max_harmonic);
        n3 = max_harmonic - rand() % (2*max_harmonic);
        n1p = max_harmonic - rand() % (2*max_harmonic);
        n2p = max_harmonic - rand() % (2*max_harmonic);
        n3p = max_harmonic - rand() % (2*max_harmonic);
        //n1p = -n1;
        //n2p = -n2;
        //n3p = -n3;
        k1 = PI/Lhalf*n1;
        k2 = PI/Lhalf*n2;
        k3 = PI/Lhalf*n3;
        k1p = PI/Lhalf*n1p;
        k2p = PI/Lhalf*n2p;
        k3p = PI/Lhalf*n3p;
        K = sqrt(k1*k1+k2*k2+k3*k3);
        Kp = sqrt(k1p*k1p+k2p*k2p+k3p*k3p);
        KKp = (k1*k1p+k2*k2p+k3*k3p)/(K*Kp);
        if(KKp<-1) KKp = -1;
        if(KKp>1) KKp = 1;
        dphi = acos(KKp);
      }
      while(dphi<pi-phi_min);

      sinF1 = cosF1 = sinF2 = cosF2 = 0;

      for(i=0; i<Nup; i++) {
        cosF1 += Cos(k1*W[w].x[i] + k2*W[w].y[i] + k3*W[w].z[i]);
        sinF1 += Sin(k1*W[w].x[i] + k2*W[w].y[i] + k3*W[w].z[i]);
      }
      for(i=0; i<Ndn; i++) {
        cosF2 += Cos(k1p*W[w].xdn[i]+k2p*W[w].ydn[i]+k3p*W[w].zdn[i]);
        sinF2 += Sin(k1p*W[w].xdn[i]+k2p*W[w].ydn[i]+k3p*W[w].zdn[i]);
      }

      //n = (int)(dphi / (2.*PI/Sk.size));
      n = (int)((dphi-pi+phi_min) / (phi_min/Sk.size));

      Sk.phi[n] += cosF1*cosF2 - sinF1*sinF2;
      Sk.phiN[n] += Nmean;
    }
  }
}

/************************** Measure Superfluid Density ************************/
void MeasureSuperfluidDensity(void) {
  int w,i,j;
  DOUBLE CM[3], dr[3];
  static int times_measured = 0;
  int maxi;

  for(w=0; w<Nwalkers; w++) {
    for(i=0; i<3; i++) CM[i] = 0.; // zero center of the mass coordinate

#ifdef SD_MEASURE_GLOBAL_WINDING
    for(i=0; i<Nup; i++) for(j=0; j<3; j++) CM[j] += W[w].rreal[i][j]; // find position of the CM
    for(i=0; i<Ndn; i++) for(j=0; j<3; j++) CM[j] += W[w].rreal[i][3+j]; // find position of the CM
#endif
#ifdef SD_MEASURE_W1_MINUS_W2
    for(i=0; i<Nup; i++) for(j=0; j<3; j++) CM[j] += W[w].rreal[i][j]; // find position of the CM
    for(i=0; i<Ndn; i++) for(j=0; j<3; j++) CM[j] -= W[w].rreal[i][3+j]; // find position of the CM
#endif
#ifdef SD_MEASURE_W1_TIMES_W2
    for(i=0; i<Nup; i++) for(j=0; j<3; j++) CM[j] += W[w].rreal[i][j]; // find position of the CM
    for(i=3; i<6; i++) CM[i] = 0.; // zero center of the mass coordinate, dn
    for(i=0; i<Ndn; i++) for(j=0; j<3; j++) CM[3+j] += W[w].rreal[i][3+j]; // find position of the CM
#endif
#ifdef SD_MEASURE_ONLY_SPIN_DOWN
    for(i=0; i<Ndn; i++) for(j=0; j<3; j++) CM[j] += W[w].rreal[i][3+j]; // find position of the CM
#endif
#ifdef SD_MEASURE_DIFFUSION_COEFFICIENT
    for(j=0; j<3; j++) CM[j] += W[w].rreal[0][j];
#endif

#ifdef SD_MEASURE_GLOBAL_WINDING
    CM[0] /= Ntot;
    CM[1] /= Ntot;
    CM[2] /= Ntot;
#endif
#ifdef SD_MEASURE_W1_MINUS_W2
    CM[0] /= Ntot;
    CM[1] /= Ntot;
    CM[2] /= Ntot;
#endif
#ifdef SD_MEASURE_W1_TIMES_W2
    CM[0] /= Nup;
    CM[1] /= Nup;
    CM[2] /= Nup;
    CM[3] /= Ndn;
    CM[4] /= Ndn;
    CM[5] /= Ndn;
#endif
#ifdef SD_MEASURE_ONLY_SPIN_DOWN
    CM[0] /= Ndn;
    CM[1] /= Ndn;
    CM[2] /= Ndn;
#endif

    if(times_measured<SD.size) { // i.e. is not yet filled
      W[w].CM[0][times_measured] = CM[0];
      W[w].CM[1][times_measured] = CM[1];
      W[w].CM[2][times_measured] = CM[2];
      maxi = times_measured;
    }
    else { // do the shift
      maxi = SD.size-1;
      for(i=0; i<maxi; i++) for(j=0; j<3; j++) W[w].CM[j][i] = W[w].CM[j][i+1];
      W[w].CM[0][maxi] = CM[0];
      W[w].CM[1][maxi] = CM[1];
      W[w].CM[2][maxi] = CM[2];
    }

    // accumulate diffusion
    for(i=0; i<maxi; i++) {
      for(j=0; j<3; j++) {
        dr[j] = W[w].CM[j][maxi] - W[w].CM[j][i];
        dr[j] *= dr[j]; // dr -> dr2
      }
#ifdef SD_MEASURE_ONLY_Z_DIRECTION
      SD.CM2[maxi-i-1] += dr[2];
#else
      SD.CM2[maxi-i-1] += dr[0] + dr[1] + dr[2];
#endif
      SD.N[maxi-i-1]++;
    }
  }
  times_measured++;
}

/************************ External potential energy *************************/
DOUBLE VextUp(DOUBLE x, DOUBLE y, DOUBLE z, int i) {
  DOUBLE E = 0.;

#ifdef SPECKLES
  E += SpecklesEpot(x, y);
#endif

#ifdef TRAP_POTENTIAL
#ifdef BC_ABSENT
#ifdef TRIAL_1D
  E += 0.5*m_up*omega_z2_up*z*z;
#endif
#ifdef TRIAL_2D
  E += 0.5*m_up*(omega_x2_up*x*x+omega_y2_up*y*y);
#endif
#ifdef TRIAL_3D
#ifdef BC_1DPBC_Z
  E += 0.5*m_up*(omega_x2_up*x*x + omega_y2_up*y*y);
#else
  E += 0.5*m_up*(omega_x2_up*x*x + omega_y2_up*y*y + omega_z2_up*z*z);
#endif
#endif
#endif

#ifdef BC_2DPBC
#ifdef TRIAL_3D
  E += 0.5*m_up*omega_z2_up*z*z;
#endif
#endif

// #ifdef BC_1DPBC_Z
//  return 0.5*(x*x + y*y)/m_up_inv;
#ifdef BC_1DPBC
#ifdef TRIAL_3D
  E += 0.5*m_up*(omega_x2_up*x*x + omega_y2_up*y*y);
#endif
#endif
#endif

#ifdef TRAP_DOUBLE_WELL
  //E += ((z-1)*(z-1)-1)*((z-1)*(z-1)-1)/m_up_inv;
   E += (0.5*z*z + alpha_latt*Exp(-0.5*z*z) - (Log(alpha_latt)+1))/m_up_inv;
#endif

#ifdef TRAP_LINEAR_CHAIN_OF_HARMONIC_OSCILLATORS_UP
#ifdef BC_ABSENT // centered at 0
  z -= Lz *((DOUBLE)i - 0.5*((DOUBLE)Nup-1.))/(DOUBLE)Nup;
#else // PBC
  z -= Lz / (DOUBLE)Nup*((DOUBLE)i + 0.5);
  if(z > L_half_z) {
    z -= Lz;
  }
  else if(z < -L_half_z) {
    z += Lz;
  }
#endif
#ifndef INFINITE_MASS_COMPONENT_UP // avoid division by zero
  E += 0.5*m_up*(omega_x2_up*x*x + omega_y2_up*y*y + omega_z2_up*z*z);
#endif
#endif

#ifdef VEXT_COS2 // lattice
#ifdef TRIAL_1D
  E += Srecoil*Cos(kL*z)*Cos(kL*z);
#endif
#ifdef BC_2DPBC_SQUARE
  E += Srecoil*(Cos(kL*x)*Cos(kL*x) + Cos(kL*y)*Cos(kL*y));
#endif
#ifdef BC_3DPBC_CUBE // set UNITS_SET_2M_TO_1 to get output in recoil energies
  E += Srecoil*(Cos(kL*x)*Cos(kL*x) + Cos(kL*y)*Cos(kL*y) + Cos(kL*z)*Cos(kL*z));
#endif
#endif

#ifdef OPTICAL_LATTICE // lattice
#ifdef OPTICAL_LATTICE_WITH_IMPURITY
  DOUBLE kL = 0.843607049836142; // experimental value
  return 13.677462673060681*(Sin(kL*x)*Sin(kL*x) + Sin(kL*y)*Sin(kL*y));
#else
#ifdef OPTICAL_LATTICE_WITH_IMPURITY_HARMONIC_TERM
  DOUBLE kL = 0.843607049836142; // experimental value
  return 13.677462673060681*kL*kL*(x*x + y*y); // only harmonic part of the potential
#else
  DOUBLE kL = PI*Nlattice/L;
  return Srecoil*0.5*kL*kL*(Cos(kL*x)*Cos(kL*x) + Cos(kL*y)*Cos(kL*y) + Cos(kL*z)*Cos(kL*z));
#endif
#endif
#endif

#ifdef VORTEX
  return 0.5*c*c/((x-L_half_x-0.5*lambda)*(x-L_half_x-0.5*lambda) + (y-L_half_y)*(y-L_half_y));
#endif

  return E;
}

DOUBLE VextDn(DOUBLE x, DOUBLE y, DOUBLE z, int i) {
  DOUBLE E = 0.;

#ifdef SPECKLES
  E += SpecklesEpot(x, y);
#endif

#ifdef TRAP_POTENTIAL
#ifdef BC_ABSENT
#ifdef TRIAL_1D
  E += 0.5*m_dn*omega_z2_dn*z*z;
#endif
#ifdef TRIAL_2D
  E += 0.5*m_dn*(omega_x2_dn*x*x+omega_y2_dn*y*y);
#endif
#ifdef TRIAL_3D
#ifdef BC_1DPBC_Z
  E += 0.5*m_dn*(omega_x2_dn*x*x + omega_y2_dn*y*y);
#else
  E += 0.5*m_dn*(omega_x2_dn*x*x + omega_y2_dn*y*y + omega_z2_dn*z*z);
#endif
#endif
#endif

#ifdef BC_2DPBC
#ifdef TRIAL_3D
  E += 0.5*m_dn*omega_z2_dn*z*z;
#endif
#endif

#ifdef BC_1DPBC
#ifdef TRIAL_3D
  E += 0.5*m_dn*(omega_x2_dn*x*x + omega_y2_dn*y*y);
#endif
#endif
#endif

#ifdef TRAP_DOUBLE_WELL
 // E += ((x-1)*(x-1)-1)*((x-1)*(x-1)-1)/m_dn_inv; // mexican hat
   E += (0.5*z*z + alpha_latt*Exp(-0.5*z*z) - (Log(alpha_latt)+1))/m_dn_inv;
#endif

#ifdef VEXT_COS2 // lattice
#ifdef TRIAL_1D
  E += Srecoil*Cos(kL*z)*Cos(kL*z);
#endif
#ifdef BC_2DPBC_SQUARE
  E += Srecoil*(Cos(kL*x)*Cos(kL*x) + Cos(kL*y)*Cos(kL*y));
#endif
#ifdef BC_3DPBC_CUBE // set UNITS_SET_2M_TO_1 to get output in recoil energies
  E += Srecoil*(Cos(kL*x)*Cos(kL*x) + Cos(kL*y)*Cos(kL*y) + Cos(kL*z)*Cos(kL*z));
#endif
#endif

#ifdef OPTICAL_LATTICE // lattice
#ifdef OPTICAL_LATTICE_WITH_IMPURITY
  DOUBLE kL = 0.843607049836142; // experimental value
  return 12.3757653854789*(Sin(kL*x)*Sin(kL*x) + Sin(kL*y)*Sin(kL*y));
#else
#ifdef OPTICAL_LATTICE_WITH_IMPURITY_HARMONIC_TERM
  DOUBLE kL = 0.843607049836142; // experimental value
  return 12.3757653854789*kL*kL*(x*x + y*y); // only harmonic part of the potential
#else
  DOUBLE kL = PI*Nlattice/L;
  return Srecoil*0.5*kL*kL*(Cos(kL*x)*Cos(kL*x) + Cos(kL*y)*Cos(kL*y) + Cos(kL*z)*Cos(kL*z));
#endif
#endif
#endif

#ifdef VORTEX
  return 0.5*c*c/((x-L_half_x+0.5*lambda)*(x-L_half_x+0.5*lambda) + (y-L_half_y)*(y-L_half_y));
#endif

  return E;
}

/*************************** Measure Spectral Weight **************************/
void MeasureSpectralWeight(void) {
  int w,i;
  static int times_measured = 0;
  static int index_of_current_position = 0;
  int index_displaced;
  DOUBLE r;
  int index_r;
  DOUBLE max_x = L/500;

  if(MC != DIFFUSION) return;

  Phi.step = (2.*max_x)/(DOUBLE) gridPhi_x;

  for(w=0; w<Nwalkers; w++) {
    W[w].U = U(W[w]);
    W[w].psi_U[index_of_current_position] = Exp(W[w].U);
    //W[w].psi_r[index_of_current_position] = W[w].zdn[0];
    W[w].psi_r[index_of_current_position] = W[w].rreal[0][5];
  }

  if(times_measured >= gridPhi_t) {
    for(w=0; w<Nwalkers; w++) {
      for(i=0; i<gridPhi_t; i++) {
        index_displaced = index_of_current_position - i;
        if(index_displaced<0) index_displaced += gridPhi_t;
        r = W[w].psi_r[index_of_current_position] - W[w].psi_r[index_displaced];
        //if(r>L) r -= L;
        //if(r<0) r += L;
        r += max_x;
        //if(r>2*L) 
        //  Warning("r>2L %lf %lf\n",W[w].psi_r[index_of_current_position],W[w].psi_r[index_displaced]);
        //if(r<0) 
        //  Warning("r<0 %lf %lf\n",W[w].psi_r[index_of_current_position],W[w].psi_r[index_displaced]);

        index_r = (int) (r/Phi.step);
        if(index_r >= gridPhi_x) 
          index_r = gridPhi_x - 1;
        else if(index_r>=0) {
          Phi.f[index_r][i] += W[w].psi_U[index_displaced]/W[w].psi_U[index_of_current_position];
          //Phi.f[index_r][i] += 1.;
        }
        Phi.times_measured++;
      }
    }
  }
  index_of_current_position++;
  if(index_of_current_position == gridPhi_t) index_of_current_position = 0;
  times_measured++;

  //if(times_measured < gridPhi_t) times_measured++;
}

/****************************** Laplace transform of Form factor **************/
// S(k,tau) = \int S(k,omega) Exp(-omega tau) d omega
// call with w=-1 moves internal counter
void MeasureFormFactor(int w, int final_measurement) {
  int i;
  DOUBLE rhoIm1, rhoRe1;
  DOUBLE rhoIm2, rhoRe2;
  int k;
  static int times_measured = -1;
  static int index_of_current_position = -1;
  int index_displaced;
  int k_with_degeneracy;
#ifndef TRIAL_1D //2D,3D
  DOUBLE k1,k2,k3;
#else //1D
  DOUBLE sign,kF,dk_min,k1;
  int j;

  kF = PI*n;
#endif

  if(MC != DIFFUSION) return;

  //if(w==0 && !final_measurement) {
  if(w==-1) {
    index_of_current_position++;
    if(index_of_current_position == gridSKT_t) index_of_current_position = 0;
    if(times_measured < gridSKT_t) times_measured++; // protect from overflow
    return;
  }

  for(k=0; k<gridSKT_k; k++) {
#ifdef TRIAL_1D
#ifdef BC_ABSENT // trap
      //if(SKT_kmin>0)
      k1 = (DOUBLE)(k)*SKT_dk;// + SKT_kmin;
      //else // avoid k=0
      //  k1 = (DOUBLE)(k+1)*SKT_dk + SKT_kmin;
#else // hom. system
      k1 = (DOUBLE)(k+1)*SKT_dk*kF;
#endif
#else // 2D and 3D cases
      k1 = Sk.kx[k]; // first [0,0,0] term is trivial
      k2 = Sk.ky[k];
      k3 = Sk.kz[k];
#endif

    rhoIm1 = rhoRe1 = 0.;
    for(i=0; i<Nup; i++) {
#ifdef TRIAL_1D
      rhoRe1 += Cos(k1*W[w].z[i]);
      rhoIm1 -= Sin(k1*W[w].z[i]);
#else // 2D and 3D cases
      rhoRe1 += Cos(k1*W[w].x[i] + k2*W[w].y[i] + k3*W[w].z[i]);
      rhoIm1 -= Sin(k1*W[w].x[i] + k2*W[w].y[i] + k3*W[w].z[i]);
#endif
    }

    rhoIm2 = rhoRe2 = 0.;
    for(i=0; i<Ndn; i++) {
#ifdef TRIAL_1D
      rhoRe2 += Cos(k1*W[w].zdn[i]);
      rhoIm2 -= Sin(k1*W[w].zdn[i]);
#else // 2D and 3D cases
      rhoRe2 += Cos(k1*W[w].xdn[i] + k2*W[w].ydn[i] + k3*W[w].zdn[i]);
      rhoIm2 -= Sin(k1*W[w].xdn[i] + k2*W[w].ydn[i] + k3*W[w].zdn[i]);
#endif
    }

    W[w].rhoRe1[index_of_current_position][k] = rhoRe1;
    W[w].rhoIm1[index_of_current_position][k] = rhoIm1;
    W[w].rhoRe2[index_of_current_position][k] = rhoRe2;
    W[w].rhoIm2[index_of_current_position][k] = rhoIm2;
  }

  if(times_measured>=gridSKT_t) {
    for(i=0; i<gridSKT_t; i++) {
      for(k=0; k<gridSKT_k; k++) {
        index_displaced = index_of_current_position - i;
        if(index_displaced<0) index_displaced += gridSKT_t;
        //k_with_degeneracy = Sk.index[k];
        k_with_degeneracy = k;

#pragma omp atomic
        SKT.f[i][k_with_degeneracy] += 
#ifdef MEASURE_SK11
           W[w].rhoRe1[index_of_current_position][k]*W[w].rhoRe1[index_displaced][k] + W[w].rhoIm1[index_of_current_position][k]*W[w].rhoIm1[index_displaced][k]
#endif
#ifdef MEASURE_SK11
           + W[w].rhoRe2[index_of_current_position][k]*W[w].rhoRe2[index_displaced][k] + W[w].rhoIm2[index_of_current_position][k]*W[w].rhoIm2[index_displaced][k]
#endif
      ;

#pragma omp atomic
        SKT.f12[i][k_with_degeneracy] += W[w].rhoRe1[index_of_current_position][k]*W[w].rhoRe2[index_displaced][k] + W[w].rhoIm1[index_of_current_position][k]*W[w].rhoIm2[index_displaced][k];
#pragma omp atomic
        SKT.N[i][k_with_degeneracy]++;
      }
      //PhiTau.f[i][0] += W[w].psi[index_displaced][0]/W[w].psi[index_of_current_position][0];
    }
  //if(final_measurement) 
#pragma omp atomic
    SKT.times_measured++;
  }
}

/****************************** Walker Potential Energy ***********************/
DOUBLE WalkerPotentialEnergy(struct Walker *W) {
  int i,j,k;
  DOUBLE dr[3], r, r2;

  Eint = 0.;

#ifdef EXTERNAL_POTENTIAL // add external potential energy
  for(i=0; i<Nup; i++) Eint += VextUp(W->x[i], W->y[i], W->z[i], i);
  for(i=0; i<Ndn; i++) Eint += VextDn(W->xdn[i], W->ydn[i], W->zdn[i], i);
#endif

  for(i=0; i<Nup; i++) { // AB
    for(j=0; j<Ndn; j++) {
      CaseX(dr[0] = W->x[i] - W->xdn[j]);
      CaseY(dr[1] = W->y[i] - W->ydn[j]);
      CaseZ(dr[2] = W->z[i] - W->zdn[j]);
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);

#ifdef INTERACTION_SUM_OVER_IMAGES
      *Eint += InteractionEnergySumOverImages12(dr[0],dr[1],dr[2]);
#endif

#ifdef INTERACTION_WITHOUT_IMAGES
      if(CheckInteractionCondition(dr[2], r2)) {
        r = sqrt(r2);
        Eint += InteractionEnergy12(r);
      }
#endif
    }
  }

  for(i=0; i<Nup; i++) { // AA
    for(j=i+1; j<Nup; j++) {
      CaseX(dr[0] = W->x[i] - W->x[j]); // spin up
      CaseY(dr[1] = W->y[i] - W->y[j]);
      CaseZ(dr[2] = W->z[i] - W->z[j]);
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);

#ifdef INTERACTION_SUM_OVER_IMAGES
      Eint += InteractionEnergySumOverImages11(dr[0],dr[1],dr[2]);
#endif

#ifdef INTERACTION_WITHOUT_IMAGES
      if(CheckInteractionCondition(dr[2], r2)) {
        r = sqrt(r2);
        Eint += InteractionEnergy11(r);
      }
#endif
    }
  }

  for(i=0; i<Ndn; i++) { // BB
    for(j=i+1; j<Ndn; j++) { 
      CaseX(dr[0] = W->xdn[i] - W->xdn[j]); // spin down
      CaseY(dr[1] = W->ydn[i] - W->ydn[j]);
      CaseZ(dr[2] = W->zdn[i] - W->zdn[j]);
      FindNearestImage3D(r2, dr[0], dr[1], dr[2]);

#ifdef INTERACTION_SUM_OVER_IMAGES
      Eint += InteractionEnergySumOverImages22(dr[0],dr[1],dr[2]);
#endif

#ifdef INTERACTION_WITHOUT_IMAGES
      if(CheckInteractionCondition(dr[2], r2)) {
        r = sqrt(r2);
        Eint += InteractionEnergy22(r);
      }
#endif
    }
  }

  return Eint;
}

/************************** Measure Pure Potential Energy *********************/
void MeasurePurePotentialEnergy(void) {
  int w, i;
  static int pure_position = 0;
  static int wait = 1;
  int inew, iold;

  for(w=0; w<Nwalkers; w++) {
    W[w].Epot_pure[pure_position] = W[w].Epot; // update the recent value

    if(!wait) {
      inew = pure_position;
      for(i=0; i<Epot_pure.size; i++) { // loop over time
        iold = inew - i; // position of a provious measurement
        if(iold<0) iold += Epot_pure.size; // check bounds

        Epot_pure.f[i] += W[w].Epot_pure[iold];
      }
      Epot_pure.times_measured++;
    }
  }

  pure_position++;
  if(pure_position == grid_pure_block) pure_position = 0;

  if(wait) {
    wait++;
    if(wait == Epot_pure.size) wait = OFF;
  }
}

/********************************** Drift Force ******************************/
// F = (\nabla \Psi) / \Psi
// f"/f = u" + (u')^2
DOUBLE KineticEnergyNumerically(struct Walker *walker) {
  int i, j;

  DOUBLE epsilon, epsilon2; // accuracy
  DOUBLE coord_store; // store coordinates
  DOUBLE Uo, Umax, Umin; // moving by epsilon
  DOUBLE Up; // U' = (dU/dx)
  DOUBLE Upp; // U" = (d2U/dx2)
  DOUBLE Ekin = 0;

  DOUBLE Rmax = 0.;

  for(i=0; i<Nup; i++) { // find largest position
    CaseX(if(fabs(W->x[i]) > Rmax) Rmax = fabs(W->x[i]));
    CaseY(if(fabs(W->y[i]) > Rmax) Rmax = fabs(W->y[i]));
    CaseZ(if(fabs(W->z[i]) > Rmax) Rmax = fabs(W->z[i]));
  }
  for(i=0; i<Ndn; i++) {
    CaseX(if(fabs(W->xdn[i]) > Rmax) Rmax = fabs(W->xdn[i]));
    CaseY(if(fabs(W->ydn[i]) > Rmax) Rmax = fabs(W->ydn[i]));
    CaseZ(if(fabs(W->zdn[i]) > Rmax) Rmax = fabs(W->zdn[i]));
  }
  //epsilon = Rmax*1e-4; // gol relative error of 1e-10
  epsilon = Rmax * 1e-5; // goal relative error of 1e-10
  epsilon2 = epsilon*epsilon;

  Uo = U(*walker);

  for(i=0; i<Nup; i++) {
#ifdef MOVE_IN_X
    coord_store = walker->x[i];
    walker->x[i] += epsilon;
    Umax = U(*walker);
    walker->x[i] = coord_store - epsilon;
    Umin = U(*walker);
    walker->x[i] = coord_store;
    Up = (Umax - Umin) / (2.*epsilon);
    Upp = (Umax - 2.*Uo + Umin) / epsilon2;
    Ekin -= Upp + Up*Up; 
    //walker->F[i][0] = Up;
#endif
#ifdef MOVE_IN_Y
    coord_store = walker->y[i];
    walker->y[i] += epsilon;
    Umax = U(*walker);
    walker->y[i] = coord_store - epsilon;
    Umin = U(*walker);
    walker->y[i] = coord_store;
    Up = (Umax - Umin) / (2.*epsilon);
    Upp = (Umax - 2.*Uo + Umin) / epsilon2;
    Ekin -= Upp + Up*Up; 
    //walker->F[i][1] = Up;
#endif
#ifdef MOVE_IN_Z
    coord_store = walker->z[i];
    //epsilon = fabs(coord_store)*1e-5;
    walker->z[i] += epsilon;
    Umax = U(*walker);
    walker->z[i] = coord_store - epsilon;
    Umin = U(*walker);
    walker->z[i] = coord_store;
    Up = (Umax - Umin) / (2.*epsilon);
    Upp = (Umax - 2.*Uo + Umin) / epsilon2;
    Ekin -= Upp + Up*Up; 
    //walker->F[i][2] = Up;
#endif
  }

  for(i=0; i<Ndn; i++) {
#ifdef MOVE_IN_X
    coord_store = walker->xdn[i];
    walker->xdn[i] += epsilon;
    Umax = U(*walker);
    walker->xdn[i] = coord_store - epsilon;
    Umin = U(*walker);
    walker->xdn[i] = coord_store;
    Up = (Umax - Umin) / (2.*epsilon);
    Upp = (Umax - 2.*Uo + Umin) / epsilon2;
    Ekin -= Upp + Up*Up;
    //walker->F[i][4] = Up;
#endif
#ifdef MOVE_IN_Y
    coord_store = walker->ydn[i];
    walker->ydn[i] += epsilon;
    Umax = U(*walker);
    walker->ydn[i] = coord_store - epsilon;
    Umin = U(*walker);
    walker->ydn[i] = coord_store;
    Up = (Umax - Umin) / (2.*epsilon);
    Upp = (Umax - 2.*Uo + Umin) / epsilon2;
    Ekin -= Upp + Up*Up;
    //walker->F[i][5] = Up;
#endif
#ifdef MOVE_IN_Z
    coord_store = walker->zdn[i];
    //epsilon = fabs(coord_store)*1e-5;
    walker->zdn[i] += epsilon;
    Umax = U(*walker);
    walker->zdn[i] = coord_store - epsilon;
    Umin = U(*walker);
    walker->zdn[i] = coord_store;
    Up = (Umax - Umin) / (2.*epsilon);
    Upp = (Umax - 2.*Uo + Umin) / epsilon2;
    Ekin -= Upp + Up*Up;
    //walker->F[i][6] = Up;
#endif
  }

  return 0.5*Ekin;
}

/****************************** pair distribution XY2 ****************************/
void MeasureXY2(int w) {
  int i, j;
  DOUBLE r2, dr[3]={0.,0.,0.};

#pragma omp atomic
  XY2_MATRIX.times_measured++;

  // UP UP
  for(i=0; i<Nup; i++) {
    for(j=0; j<Nup; j++) { // accumulate distance in xy plane
      CaseX(dr[0] = W[w].x[i] - W[w].x[j]);
      CaseY(dr[1] = W[w].y[i] - W[w].y[j]);
      //CaseZ(dr[2] = W[w].z[i] - W[w].z[j]);
      //FindNearestImage3D(r2, dr[0], dr[1], dr[2]);
      r2 = dr[0]*dr[0] + dr[1]*dr[1];
#pragma omp atomic
      XY2_MATRIX.f[i][j] += r2;
    }
  }
}
